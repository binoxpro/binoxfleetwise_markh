<?php
include_once('connect.php');
class UniversalConnection implements ConnectObject{
	 private $driver=ConnectObject::DSN;
	 private $user=ConnectObject::USERNAME;
	 private $password=ConnectObject::PASSWORD;
	 static $connection_string=NULL;
	 private $conn;
	 private $query;
	 private $select_query;
	 
	
	public function __construct(){
		try{
		$this->conn=new PDO($this->driver,$this->user,$this->password);
		$this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
		}catch(PDOException $e){
			//write the error to log file
			echo $e->getMessage();
		}
	}
	//get the instance of any object of this class
	public static function getInstance(){
		if($this->conn!=NULL){
			// $this->conn;
		}else{
			try{
				$this->conn=new PDO($this->driver,$this->user,$this->password);
				$this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
			}catch(PDOException $e){
			//write the error to log file
			echo $e->getMessage();
			}
		}
	}
	
    
    public function getConn()
    {
        
        if($this->conn!=NULL){
			// $this->conn;
		}else{
			try{
				$this->conn=new PDO($this->driver,$this->user,$this->password);
				$this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
			}catch(PDOException $e){
			//write the error to log file
			echo $e->getMessage();
			}
		}
        return $this->conn;
    }
    
	public function execute_query(){
		try{
			$rs=$this->conn->query($this->query);
			return array('success'=>true);
		}catch(PDOException $e){
			return array('msg'=>$e->getMessage());
		}
	}
	
	public function setQuery($query_string){
		$this->query=$query_string;
	}
	
	public function execute_query2($array){
		try{
			$rs=$this->conn->prepare($this->query);
			if(is_array($array)){
				for($i=0;$i<sizeof($array);$i++){
					$rs->bindValue('param'.$i,$array[$i]);
					
				}
				$rs->execute();
				return array('success'=>true);
			}else{
				return array('msg'=>'set array');
			}
		}catch(PDOException $e){
			return array('msg'=>$e->getMessage());
		}
	}
	//any not using json
	public function execute_query3($array){
		try{
			$rs=$this->conn->prepare($this->query);
			if(is_array($array)){
				for($i=0;$i<sizeof($array);$i++){
					$rs->bindValue('param'.$i,$array[$i]);
					
				}
				$rs->execute();
				return true;
			}else{
				return 'set array';
			}
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
	
	public function getResultSet($partSql){
		//$array=Array();
		$row=NULL;
		
		try{
			//$resultSet=$this->conn->query($partSql);
			$stmt=$this->conn->prepare($partSql);
			$stmt->execute();
			$row=$stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(PDOException $e2){
			echo"Failure to fetch records due to:".$e2->getMessage();
		}
		
		return $row;
	}
	public function getSelect_query() {
            return $this->select_query;
        }

        public function setSelect_query($select_query) {
            $this->select_query = $select_query;
        }

    public function sqlCount(){
		$no=0;
		try{
			$resultSet=$this->conn->query($this->select_query);
			$no=$resultSet->rowCount();
			return $no;
		}catch(PDOException $e){
			return $no;
		}
	}
	public function getId(){
		try{
			return $this->conn->lastInsertId();
		}catch(PDOException $e){
			return $e->getMessage();
		}
	}
}

?>