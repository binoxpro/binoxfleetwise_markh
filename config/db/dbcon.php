<?php
class Dbcon {
	const DS_NAME="mysql:dbname=kcc_db";//driver and database name
	const DB_USERNAME="root";
	const DB_PASSWORD="";
	private static $instance=NULL;
	public static $conn;
	private function __construct(){
		try{
		Dbcon::$conn=new PDO(Dbcon::DS_NAME,Dbcon::DB_USERNAME,Dbcon::DB_PASSWORD);
		Dbcon::$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	
	public function __destruct(){
		Dbcon::$instance==NULL;
	}
	//deployment of the singleton pattern 
	//we have to create one object for connection strings
	public static function getInstance(){
		if(Dbcon::$instance!=NULL){
			#echo "old instance returned";
			return Dbcon::$instance;
		}else{
			//echo "new instance returned";
			Dbcon::$instance=new Dbcon();
			return Dbcon::$instance;
		}
		
	}
	//run all my select query
	public function getResultSet($partSql){
		$array=Array();
		
		try{
			$resultSet=Dbcon::$conn->query($partSql);
			foreach($resultSet as $row){
				array_push($array,$row);
			}
		}catch(PDOException $e2){
			echo"Failure to fetch records due to:".$e2->getMessage();
		}
		
		return $array;
	}
	//insert into the database
	public function insert($sql,$sqlCheck){
		$check=$this->recordCount($sqlCheck);
		if($check==0){
		try{
			$resultSet=Dbcon::$conn->query($sql);
			return array("success"=>true);
		}catch(PDOException $e){
			return array("msg"=>$e->getMessage());
		}
		}else if($check==1){
			return array("msg"=>"Data already exists");
		}
		
	}
	
	public function insert2($sql){
		
		
		try{
			$resultSet=Dbcon::$conn->query($sql);
			return array("success"=>true);
		}catch(PDOException $e){
			return array("msg"=>$e->getMessage());
		}
		
		
	}
	//insert throught return of 1
	public function insert3($sql){
		
		
		try{
			$resultSet=Dbcon::$conn->query($sql);
			return 1;
		}catch(PDOException $e){
			return $e->getMessage();
		}
		
		
	}
	public function update($sql){
		try{
			$resultSet=Dbcon::$conn->query($sql);
			$no=$resultSet->rowCount();
			if($no>0){
				return array("success"=>true);
			}else{
				return array("msg"=>"No Update made because the data may not exit or its ready updated");
			}
			
		}catch(PDOException $e){
			return array("msg"=>$e->getMessage());
		}
	}
	
	public function update2($sql){
		try{
			$resultSet=Dbcon::$conn->query($sql);
			$no=$resultSet->rowCount();
			if($no>0){
				//return array("success"=>true);
			}else{
				//return array("msg"=>"No Update made because the data may not exit or its ready updated");
			}
			
		}catch(PDOException $e){
			//return array("msg"=>$e->getMessage());
		}
	}
	
	public function delete($sql){
		try{
			$resultSet=Dbcon::$conn->query($sql);
			$no=$resultSet->rowCount();
			if($no>0){
				return array("success"=>true);
			}else{
				return array("msg"=>"No Delete Made ");
			}
		}catch(PDOException $e){
			return array("msg"=>$e->getMessage());
		}
	}
	
	public function recordCount($sql){
		$no=0;
		try{
			$resultSet=Dbcon::$conn->query($sql);
			$no=$resultSet->rowCount();
			return $no;
		}catch(PDOException $e){
			return $no;
		}
	}
	//return the last inserted id
	public function getId(){
		try{
			$resultSet=Dbcon::$conn->lastInsertId();
			return $resultSet;
		}catch(PDOException $e){
			return  $e->getMessage();
		}
	}
}
?>