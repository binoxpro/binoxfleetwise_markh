<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MARKH INVESTMENTS|FMS.104</title>

    <!-- Bootstrap -->
    <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendor/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">

</head>

<body class="login">
    

    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form action="controller/systemUserController.php" method="post">
                        <h1>MARKH INVESTMENTS LIMITED</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" name="username" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" name="password" required="" />
                        </div>
                        <?php
                        if(isset($_REQUEST['returnUrl']))
                        {
                            echo '<div><input type="hidden" class="form-control"  name="returnUrl" value="'.$_REQUEST['returnUrl'].'" /></div>';
                        }
                        ?>
                        <div>
                            <input type="submit" name="action" value="LOGIN" class="btn btn-success submit" />
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">


                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-paw"></i> Binox FLEETWISE SOLUTION.</h1>
                                <p>©2013 All Rights Reserved. Binox Africa Software. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>


        </div>
    </div>

</body>

</html>