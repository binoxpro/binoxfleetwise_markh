<?php
ob_start();
session_start();
if(isset($_SESSION['userId'])){
	if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600))
	{
		// last request was more than 30 minutes ago
		session_unset();     // unset $_SESSION variable for the run-time
		session_destroy();
		// destroy session data in storage
		$url=$_SERVER[REQUEST_URI];
		//tokenization
		$urlToken=explode('/',$url);
		$urlNew="";
		for($i=2;$i<sizeof($urlToken);$i++)
		{

			$urlNew.=$urlToken[$i];
		}
		header('location:index.php?returnUrl='.$urlNew);
	}else
	{
		//session_unset();     // unset $_SESSION variable for the run-time
		//session_destroy();
		//header('location:../index.php?returnUrl='.$url);
	}
	
}else{
	header('location:index.php?msg=you are not logged in login now');
}
?>