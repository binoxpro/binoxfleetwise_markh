<?php

/**
 * Created by PhpStorm.
 * User: james
 * Date: 9/16/2016
 * Time: 1:15 PM
 */
ob_start();
session_start();
require_once('isubject.php');
require_once('../services/systemUserService.php');
class Proxy implements  Isubject
{
    private $log;
    function login(systemUserService $sus)
    {
        $sus->login();

        if(!is_null($sus->getid()))
        {
            $_SESSION['userId']=$sus->getid();
            $_SESSION['username']=$sus->getfirstName()." ".$sus->getlastName();


            $this->log=true;
        }else{
            $this->log=false;
        }
        $this->request();
    }

    function request()
    {
            if($this->log)
            {
                if(!isset($_REQUEST['returnUrl']))
                {
                    $_SESSION['LAST_ACTIVITY']=time();
                    session_write_close();
                    header('location:../admin.php');
                }else
                {
                    $_SESSION['LAST_ACTIVITY']=time();
                    session_write_close();
                    if(isset($_REQUEST['returnUrl']))
                    {

                        header('location:../' . $_REQUEST['returnUrl']);

                    }
                }

                //echo $this->log;

            }else
            {
                //echo $this->log;
                header('location:../index.php?msg=supply correct username and password');
            }
    }
}