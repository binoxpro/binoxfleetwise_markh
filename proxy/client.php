<?php

/**
 * Created by PhpStorm.
 * User: james
 * Date: 9/16/2016
 * Time: 1:58 PM
 */
require_once('../services/systemUserService.php');
require_once('proxy.php');
require_once('isubject.php');
class Client
{
    private $sus;

    public function __construct($sus)
    {
        $this->sus=$sus;
        $proxy=new Proxy();
        $this->getIface($proxy);
    }

    private function getIface(Isubject $proxy)
    {
            $proxy->login($this->sus);
    }


}