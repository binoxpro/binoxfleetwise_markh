

 function newtireInspectionHeader(){
$('#dlgtireInspectionHeader').dialog('open').dialog('setTitle','Enter tireInspectionHeader ');
$('#frmtireInspectionHeader').form('clear');
 url='controller/tireInspectionHeaderController.php?action=add'; 
}

function getCurrentMileage(x,contentId){
	$.post('controller/tyreController.php?action=getMileage',{id:x},function(res){
		if(!isNaN(parseFloat(res))){
		$(contentId).numberbox('setValue',res);
        getLastInspection(x);
		}else{
		$.messager.show({title:'System Error',msg:'Please Contact the System Administrator: '+res});	
		}
	});
}

function getLastInspection(x){
	$.post('controller/tireInspectionHeaderController.php?action=getLastInspection',{id:x},function(res){
		var ar=res.split('*');
		$('#llid').val(ar[0]);
        $('#lastReading').val(ar[1]);
		
	});
}

 function edittireInspectionHeader(){
 var row=$('#dgtireInspectionHeader').datagrid('getSelected');
 if(row)
{
 $('#dlgtireInspectionHeader').dialog('open').dialog('setTitle','Edit tireInspectionHeader');
 $('#frmtireInspectionHeader').form('load',row); 
 url='controller/tireInspectionHeaderController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

function completeInspection(){
 var row=$('#dgtireInspectionHeader').datagrid('getSelected');
 if(row)
{
 //$('#dlgtireInspectionHeader').dialog('open').dialog('setTitle','Edit tireInspectionHeader');
 //$('#frmtireInspectionHeader').form('load',row); 
 window.location='admin.php?view=inspect_vehicle_tire&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}
function printCopy(){
 var row=$('#dgtireInspectionHeader').datagrid('getSelected');
 if(row)
{
 //$('#dlgtireInspectionHeader').dialog('open').dialog('setTitle','Edit tireInspectionHeader');
 //$('#frmtireInspectionHeader').form('load',row); 
 	
	//var urlxv="controller/email2.php?idx="+$("#jobid3").val()+"&title="+title+"&tin="+tin;
	var urlxv="printCopy.php?id="+row.id;
	if(window.showModalessDialog){
	 window.showModalessDialog(urlxv,'','dialogWidth=800,dialogHeight=1800,scrollbar=no,location=no,resizable=no,dialog=yes,alwaysontop=yes,maximize=no,left=0,top=0');
	}else{
		window.open(urlxv,"Invoice","width=800, height=1800, alwaysRaised=yes");
	}
 //window.location='admin.php?view=inspect_vehicle_tire&id=';
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}
function printCopy2(){
 var row=$('#dgtireInspectionHeader').datagrid('getSelected');
 if(row)
{
 //$('#dlgtireInspectionHeader').dialog('open').dialog('setTitle','Edit tireInspectionHeader');
 //$('#frmtireInspectionHeader').form('load',row); 
 	
	//var urlxv="controller/email2.php?idx="+$("#jobid3").val()+"&title="+title+"&tin="+tin;
	var urlxv="printCopy2.php?id="+row.id;
	if(window.showModalessDialog){
	 window.showModalessDialog(urlxv,'','dialogWidth=800,dialogHeight=1800,scrollbar=no,location=no,resizable=no,dialog=yes,alwaysontop=yes,maximize=no,left=0,top=0');
	}else{
		window.open(urlxv,"Invoice","width=800, height=1800, alwaysRaised=yes");
	}
 //window.location='admin.php?view=inspect_vehicle_tire&id=';
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletetireInspectionHeader(){
 var row=$('#dgtireInspectionHeader').datagrid('getSelected');
 if(row)
{
$.post('controller/tireInspectionHeaderController.php?action=delete&id='+row.id,{},function(data){ $('#dgtireInspectionHeader').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savetireInspectionHeader(){
    saveFormUrl('#frmtireInspectionHeader','controller/tireInspectionHeaderController.php?action=add','admin.php?view=inspect_vehicle_tire')
 //saveForm('#frmtireInspectionHeader',url,'#dlgtireInspectionHeader','#dgtireInspectionHeader');
}

$(function(){
	$('#dgtireInspectionHeader').datagrid({
	rowStyler:function(index,row){
		var days=parseInt(row.days);
		var isActive=parseInt(row.isActive);
		
		if((days)<=5 && isActive==1 ){
			
			return 'background-color:#0F9;color:#FFF; font-weight:"bold"';
		}
	}
	
	});
	});