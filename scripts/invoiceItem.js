

 function newinvoiceItem(){
$('#dlginvoiceItem').dialog('open').dialog('setTitle','Enter invoiceItem ');
$('#frminvoiceItem').form('clear');
 url='controller/invoiceItemController.php?action=add'; 
}

 function editinvoiceItem(){
 var row=$('#dginvoiceItem').datagrid('getSelected');
 if(row)
{
 $('#dlginvoiceItem').dialog('open').dialog('setTitle','Edit invoiceItem');
 $('#frminvoiceItem').form('load',row); 
 url='controller/invoiceItemController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteinvoiceItem(){
 var row=$('#dginvoiceItem').datagrid('getSelected');
 if(row)
{
$.post('controller/invoiceItemController.php?action=delete&id='+row.id,{},function(data){ $('#dginvoiceItem').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveinvoiceItem(){
 saveForm('#frminvoiceItem',url,'#dlginvoiceItem','#dginvoiceItem');
}