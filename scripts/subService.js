

 function newsubService(){
$('#dlgsubService').dialog('open').dialog('setTitle','Enter subService ');
$('#frmsubService').form('clear');
 url='controller/subServiceController.php?action=add'; 
}

 function editsubService(){
 var row=$('#dgsubService').datagrid('getSelected');
 if(row)
{
 $('#dlgsubService').dialog('open').dialog('setTitle','Edit subService');
 $('#frmsubService').form('load',row); 
 url='controller/subServiceController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletesubService(){
 var row=$('#dgsubService').datagrid('getSelected');
 if(row)
{
$.post('controller/subServiceController.php?action=delete&id='+row.id,{},function(data){ $('#dgsubService').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savesubService(){
 saveForm('#frmsubService',url,'#dlgsubService','#dgsubService');
}