// JavaScript Document
function newNotification()
{
	$('#dlgNotification').dialog('open').dialog('setTitle','Add Notification');
	$('#frmNotification').form('clear');
	url='controller/notificationController.php?action=add';
	
}
function editNotification()
{
	var row=$("#dgNotification").datagrid("getSelected");
	if(row)
	{
		$('#dlgNotification').dialog('open').dialog('setTitle','Editing Notification Details');
		$('#frmNotification').form('load',row);
		url='controller/notificationController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a Notification to edit"});
		//alert('nnnn')
	}
}
function saveNotification(){
	saveForm("#frmNotification",url,"#dlgNotification","#dgNotification");
}