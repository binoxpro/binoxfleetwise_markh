

 function newfinancialYear(){
$('#dlgfinancialYear').dialog('open').dialog('setTitle','Enter financialYear ');
$('#frmfinancialYear').form('clear');
 url='controller/financialYearController.php?action=add'; 
}

 function editfinancialYear(){
 var row=$('#dgfinancialYear').datagrid('getSelected');
 if(row)
{
 $('#dlgfinancialYear').dialog('open').dialog('setTitle','Edit financialYear');
 $('#frmfinancialYear').form('load',row); 
 url='controller/financialYearController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletefinancialYear(){
 var row=$('#dgfinancialYear').datagrid('getSelected');
 if(row)
{
$.post('controller/financialYearController.php?action=delete&id='+row.id,{},function(data){ $('#dgfinancialYear').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savefinancialYear(){
 saveForm('#frmfinancialYear',url,'#dlgfinancialYear','#dgfinancialYear');
}

 function checkBoxFormatter(val,row)
 {
  if(val==0)
  {
   return "<input type='checkbox'  value='1' readonly='readonly'  />";
  }else
  {
   return "<input type='checkbox' value='0' readonly='readonly' checked/>";
  }
 }
