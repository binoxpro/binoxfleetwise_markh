/**
 * Created by james on 11/20/2016.
 */
function loadStatement(x)
{
    $('#dgSupplierStatement').datagrid('load',{supplierCode:x});
}
function paySupplier()
{
    $('#dlgExpenseVoucher').dialog('open').dialog('setTitle','Payment Voucher');
    $('#frmExpenseVoucher').form('clear');
    url="controller/paymentVoucherController.php?action=paySupplier";
    $('#individualNo').val($('#supplierId').combobox('getValue'));
    $('#accountCode').val('Payables/Creditors');
    loadingFinancialConstants();
    loadingVoucher();
}
function validateDate(date)
{
    var year=date.getFullYear();
    var month=(date.getMonth()+1)<9?'0'+(date.getMonth()+1):(date.getMonth()+1);
    var calVal=month+'/'+year;
    var fm=$('#monthCode').val();
    //var array=fm.split('/');
    //alert("cal:"+calVal+"fm:"+fm);
    if(calVal===fm)
    {

    }else
    {
        $.messager.alert('Refixed Fleet Solution','You can not Transact in a closed Financial month please first open the transaction month of '+fm,'warning');
        $(this).datebox('setValue','');
    }

}
function loadingFinancialConstants()
{
    $.post('controller/financialYearController.php',{action:'getActiveYear'},function(data)
    {
        var data2= $.parseJSON(data);
        //alert(data2.id);
        $('#yearCode').val(data2.yearId);
        $('#monthCode').val(data2.id);
    })
}

function loadingVoucher()
{
    $.post('controller/paymentVoucherController.php', {action: 'getVoucherNo'}, function (data) {

        $('#idV').val(data);

    });
}

function savePaymentVoucher()
{
    //var url='';
    saveForm('#frmExpenseVoucher',url,'#dlgExpenseVoucher','#dgSupplierStatement');
}