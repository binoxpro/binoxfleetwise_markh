// JavaScript Document
/*function newTyre()
{
	$('#dlgTyre').dialog('open').dialog('setTitle','Add Tyre');
	$('#frmTyre').form('clear');
	url='controller/tyreController.php?action=add';
	
}
function editTyre()
{
	var row=$("#dgTyre").datagrid("getSelected");
	if(row)
	{
		$('#dlgTyre').dialog('open').dialog('setTitle','Editing Tyre Details');
		$('#frmTyre').form('load',row);
		url='controller/tyreController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a Tyre to edit"});
		//alert('nnnn')
	}
}
function saveTyre(){
	saveForm("#frmTyre",url,"#dlgTyre","#dgTyre");
}*/
function searchTyreDetails(searchVal)
{
	$('#dgTyreManagement').datagrid('load',{numberPlate:searchVal});
}
function getCurrentMileage(x,contentId){
	$.post('controller/tyreController.php?action=getMileage',{id:x},function(res){
		if(!isNaN(parseFloat(res))){
		$(contentId).numberbox('setValue',res);
		}else{
		$.messager.show({title:'System Error',msg:'Please Contact the System Administrator: '+res});	
		}
	});
}
function newTyre(){
		
		
			$('#dlgTyre').dialog('open').dialog('setTitle','Tyre Management');
			
			$('#frmTyre').form('clear');
			
			url='controller/tyreController.php?action=add';
			
	}
	function updateMileage(){
		
		
			$('#dlgMileage').dialog('open').dialog('setTitle','Update Mileage');
			
			$('#frmMileage').form('clear');
			
			url='controller/maintainanceController.php?action=addWeekly';
				
	}
	
	
	function deleteTyre(){
		$.messager.confirm('Confirm', 'Are sure you would like to delete is tyre Information', function(r){
				if (r){
		
		var row=$('#dgTyreManagement').datagrid('getSelected');
		if(row){
			//$('#dlgTyre').dialog('open').dialog('setTitle','Edit Tyre Record');
			
			url='controller/tyreController.php?action=delete&id='+row.Id;
			$.post(url,{},function(data){
				$.messager.show({title:'Message',msg:'Tyre Information deleted'});
				$('#dgTyreManagement').datagrid('reload');
			})
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile Tyre to edit'});
		}
				}
		});
	}
	
	function editTyre(){
		var row=$('#dgTyreManagement').datagrid('getSelected');
		if(row){
			$('#dlgTyre').dialog('open').dialog('setTitle','Edit Tyre Record');
			$('#frmTyre').form('load',row);
			url='controller/tyreController.php?action=update&id='+row.Id;
			
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile Tyre to edit'});
		}
		
	}
	
	function saveTrack(){
		
		
		
		$.messager.progress();
	 $('#frmTrack').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Truck successfully added'});
						$('#dlgTrack').dialog('close'); // close the dialog
						 $('#dgTyreManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	$(function(){
	$('#dgTyreManagement').datagrid({
	rowStyler:function(index,row){
		var removal=parseInt(row.removalMileage);
		var current=parseInt(row.currentReading);
		var treadDepth=parseInt(row.treadDepth);
        var remaining=parseInt(row.remaining);
		//alert(nextR+"/"+current);
		if((current)>=(removal-1000)&&remaining!=0){
			
			return 'background-color:#0F9;color:#FFF; font-weight:"bold"';
		}else if((current)>=(removal)&&remaining!=0){
			return 'background-color:#9B0000;color:#FFF; font-weight:"italic"';
		}else if((remaining)==0){
		      return 'background-color:orange;color:#FFF; font-weight:"bold"';
		}
		
	}
	
	});
	});
	
function setRemovalMileage(odmeterReading,expectedKm)
{
	var num1=parseFloat(odmeterReading);
	var num2=parseFloat(expectedKm);
	$('#removalMileage').numberbox('setValue',(num2+num1));
}

function setBrand(id)
{
	$.post('controller/stockController.php?action=getStock',{id:id},function(res){
	var str=res.split('-');
	$('#cost').val(str[0]);
	$('#treadDepth').val(str[1]);
	$('#brand').val(str[2]);
	
		
		
	});
	
}


function saveTyreManagement()
{
		$.messager.progress();
	 $('#frmTyre').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Tyre information added'});
						$('#dlgTyre').dialog('close'); // close the dialog
						 $('#dgTyreManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}


function exportToExcel(){
	var vid=$('#numberPlateSearchTyre').combobox('getValue');
	if(vid==null ||vid==""){
		$.post('controller/tyreController.php?action=view',{},function(res){
		JSONToCSVConvertor(res,"Tyre Report",true);
	});
	}else{
		$.post('controller/tyreController.php?action=view',{numberPlate:vid},function(res){
		JSONToCSVConvertor(res,"Tyre Report",true);
	});
	}
	
	
}

function saveTyrePositionManagement()
{
	$.messager.progress();
	 $('#frmMileage').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Trye position Added'});
						$('#dlgMileage').dialog('close'); // close the dialog
						 $('#dgTyreManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}

	function treadDepth(){
		var row=$('#dgTyreManagement').datagrid('getSelected');
		if(row){
			if(typeof row.stockId==="undefined"){
				alert("Tyre is not in stock");
			}else{
				alert('StockNo:'+row.stockId);
			$('#dlgTreadDepth').dialog('open').dialog('setTitle','Edit Tyre Record');
			$('#frmTreadDepth').form('load',row);
			url='controller/stockController.php?action=treadDepth&id='+row.stockId;
			}
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile Tyre to edit'});
		}
			
	}
	function saveTreadDepth()
	{
 		saveForm('#frmTreadDepth',url,'#dlgTreadDepth','#dgTyreManagement');
	}
	
	function openExtenison(){
		var row=$('#dgTyreManagement').datagrid('getSelected');
		if(row){
			$('#dlgMileageExtension').dialog('open').dialog('setTitle','Edit Tyre Record');
			$('#frmExtenisonMileage').form('clear');
			$('#rm').val(row.removalMileage);
			
			url='controller/tyreController.php?action=updateExtension&id='+row.Id;
			
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile tyre to extend removal'});
		}
		
	}
	
	function saveExtension()
	{
 		saveForm('#frmExtenisonMileage',url,'#dlgMileageExtension','#dgTyreManagement');
	}


