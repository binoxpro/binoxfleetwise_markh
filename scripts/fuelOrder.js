
function prepare_fuelOrderItem()
{

 var id=$('#id').val();
 //prepare_fuelOrder();
 $('#dgfuelOrderItem').edatagrid({
  url: 'controller/fuelOrderItemController.php?action=view&fuelOrderId='+id,
  saveUrl: 'controller/fuelOrderItemController.php?action=add&fuelOrderId='+id,
  updateUrl: 'controller/fuelOrderItemController.php?action=edit&fuelOrderId='+id,
  destroyUrl:'controller/fuelOrderItemController.php?action=delete&fuelOrderId='+id,
  onSuccess:function()
  {
   $('#dgfuelOrderItem').datagrid('reload');
  }
 });

}
 function newfuelOrder(){
$('#dlgfuelOrder').dialog('open').dialog('setTitle','Enter fuelOrder ');
$('#frmfuelOrder').form('clear');
 url='controller/fuelOrderController.php?action=add'; 
}
function fuelOrderIssued()
{
  var row=$('#dgfuelOrder').datagrid('getSelected');
  if(row)
  {
    $('#dlgfuelOrder').dialog('open').dialog('setTitle','Edit fuelOrder');
    $('#frmfuelOrder').form('load',row);
    prepare_fuelOrderItem();
    url='controller/fuelOrderController.php?action=issued&id='+row.id;
  }
  else
  {
    $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
}

function fuelOrderInvoiced()
{
    var row=$('#dgfuelOrder').datagrid('getSelected');
    if(row)
    {
        $('#dlgfuelOrder').dialog('open').dialog('setTitle','Edit fuelOrder');
        $('#frmfuelOrder').form('load',row);
        prepare_fuelOrderItem();
        url='controller/fuelOrderController.php?action=invoiced&id='+row.id;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}

function payfuelOrder()
{
    var row=$('#dgfuelOrder').datagrid('getSelected');
    if(row)
    {
        $('#dlgfuelOrder').dialog('open').dialog('setTitle','Edit fuelOrder');
        $('#frmfuelOrder').form('load',row);
        prepare_fuelOrderItem();
        url='controller/fuelOrderController.php?action=invoiced&id='+row.id;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}

function validateDate(date)
{
    var year=date.getFullYear();
    var month=(date.getMonth()+1)<9?'0'+(date.getMonth()+1):(date.getMonth()+1);
    var calVal=month+'/'+year;
    var fm=$('#monthCode').val();
    //var array=fm.split('/');
    //alert("cal:"+calVal+"fm:"+fm);
    if(calVal===fm)
    {

    }else{
        $.messager.alert('Refixed Fleet Solution','You can not Transact in a closed Financial month please first open the transaction month of '+fm,'warning');
        $(this).datebox('setValue','');
    }

}

function payFuel()
{
    var row=$('#dgfuelOrder').datagrid('getSelected');
    if(row)
    {
        if(row.paid==0)
        {


            $('#dlgTripExpenseVoucher').dialog('open').dialog('setTitle', 'Expense Payment Voucher');
            $('#frmTripExpenseVoucher').form('clear');
            $('#tripNo3').val(row.tripNo);
            $('#accountCode').val("Payables/Creditors");
            $('#particular').val("Payment for Fuel Suppliers");
            $('#expectAmount').val((row.totalShs));
            $('#individualNo').val(row.supplierId);
            $('#idFuelOrder').val(row.id);
            loadingFinancialConstants();
            loadingVoucher();
        }else
        {

        }
        $.messager.show({title:'Warning!',msg:'The fuel Order is ready paided'});
        //$('#dgTripExpense').datagrid('load',{tripNo:row.TripNumber});
        //$('#tripNoExpense').val(row.TripNumber);
        //$('#frmTripExpenseVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&TripNumber='+row.TripNumber;
        //prepare_tripOtherExpense();
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}


function loadingFinancialConstants()
{
    $.post('controller/financialYearController.php',{action:'getActiveYear'},function(data)
    {
        var data2= $.parseJSON(data);
        //alert(data2.id);
        $('#yearCode').val(data2.yearId);
        $('#monthCode').val(data2.id);
    })
}

function loadingVoucher()
{
    $.post('controller/paymentVoucherController.php', {action: 'getVoucherNo'}, function (data) {

        $('#idV').val(data);

    })
}



 function editfuelOrder()
 {
 var row=$('#dgfuelOrder').datagrid('getSelected');
     if(row)
    {
     $('#dlgfuelOrder').dialog('open').dialog('setTitle','Edit fuelOrder');
     $('#frmfuelOrder').form('load',row);
     prepare_fuelOrderItem();
     url='controller/fuelOrderController.php?action=edit&id='+row.id;
    }
     else
     {
      $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
     }
}

 function deletefuelOrder()
 {
    var row=$('#dgfuelOrder').datagrid('getSelected');
     if(row)
    {
    $.post('controller/fuelOrderController.php?action=delete&id='+row.id,{},function(data){ $('#dgfuelOrder').datagrid('reload');});
    }
     else
     {
    $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
     }
 }

 function savefuelOrder(){
 saveForm('#frmfuelOrder',url,'#dlgfuelOrder','#dgfuelOrder');
}
 function checkBoxFormatter(val,row)
 {
  if(val==0)
  {
   return "<input type='checkbox'  value='1' readonly='readonly'  />";
  }else
  {
   return "<input type='checkbox' value='0' readonly='readonly' checked/>";
  }
 }
function savePaymentVoucher()
{
    var urlpayment="controller/paymentVoucherController.php?action=fuelPayment";
    saveForm('#frmTripExpenseVoucher',urlpayment,'#dlgTripExpenseVoucher','#dgfuelOrder');
}
