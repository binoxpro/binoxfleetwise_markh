// JavaScript Document
function newFuel()
{
	$('#dlgFuel').dialog('open').dialog('setTitle','Add Fuel');
	$('#frmFuel').form('clear');
	url='controller/fuelController.php?action=add';
	
}
function editFuel()
{
	var row=$("#dgFuel").datagrid("getSelected");
	if(row)
	{
		$('#dlgFuel').dialog('open').dialog('setTitle','Editing Fuel Details');
		$('#frmFuel').form('load',row);
		url='controller/fuelController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Message!',msg:"Please select a Fuel to edit"});
		//alert('nnnn')
	}
}
function saveFuel(){
	saveForm("#frmFuel",url,"#dlgFuel","#dgFuel");
}
function viewSearchC(){
	
	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#vehicleId').combobox('getValue');
	if(p1==null || p1==''){
		
		$('#dgFuel').datagrid('load',{startDate:d1,endDate:e1});
	}else if(d1==null||e1==null||d1==""||e1==""){
		$('#dgFuel').datagrid('load',{vehicleId:p1});
	}else{
		
		$('#dgFuel').datagrid('load',{startDate:d1,endDate:e1,vehicleId:p1});
	}
}