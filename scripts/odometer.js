// JavaScript Document
function newOdometer()
{
	$('#dlgOdometer').dialog('open').dialog('setTitle','Add Odometer');
	$('#frmOdometer').form('clear');
	url='controller/odometerController.php?action=add';
	
}
function editOdometer()
{
	var row=$("#dgOdometer").datagrid("getSelected");
	if(row)
	{
		$('#dlgOdometer').dialog('open').dialog('setTitle','Editing Odometer Details');
		$('#frmOdometer').form('load',row);
		url='controller/odometerController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a Odometer to edit"});
		//alert('nnnn')
	}
}
function saveOdometer(){
	saveForm("#frmOdometer",url,"#dlgOdometer","#dgOdometer");
}