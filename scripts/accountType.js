

 function newaccountType(){
$('#dlgaccountType').dialog('open').dialog('setTitle','Enter accountType ');
$('#frmaccountType').form('clear');
 url='controller/accountTypeController.php?action=add'; 
}

 function editaccountType(){
 var row=$('#dgaccountType').datagrid('getSelected');
 if(row)
{
 $('#dlgaccountType').dialog('open').dialog('setTitle','Edit accountType');
 $('#frmaccountType').form('load',row); 
 url='controller/accountTypeController.php?action=edit&PrefixCode='+row.PrefixCode;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteaccountType(){
 var row=$('#dgaccountType').datagrid('getSelected');
 if(row)
{
$.post('controller/accountTypeController.php?action=delete&PrefixCode='+row.PrefixCode,{},function(data){ $('#dgaccountType').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveaccountType()
 {
 saveForm('#frmaccountType',url,'#dlgaccountType','#dgaccountType');
  $('#dgaccountType').treegrid('reload');
}
 $(function(){
  $('#dgaccountType').treegrid('collapseAll');
 });