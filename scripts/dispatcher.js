// JavaScript Document
$(function(){
	$("#dgDispatcher").edatagrid({
				method:'get',
				url:'controller/truckDispatcherController.php?action=view',
				saveUrl:'controller/truckDispatcherController.php?action=add',
				updateUrl:'controller/truckDispatcherController.php?action=update',
				deleteUrl:'controller/truckDispatcherController.php?action=delete',
				onSuccess:function(index,row){
					$("#dgDispatcher").datagrid('reload');
					
				}
			});	
	
})

function refreshGird()
{
$("#dgDispatcher").edatagrid('reload');	
}

function saveDispatcher(){
try{
javascript:$('#dgDispatcher').edatagrid('saveRow');
refreshGird();	
}catch(err)
{
	
}
}
function exportToExcel(){
	$.messager.prompt("Export","Enter Date?",function(r){
	if(r){
		$.post('controller/allocationController.php?action=excelExport',{date:r},function(res){
		JSONToCSVConvertor(res,"Orders for:"+r,true);
	});
	}
	}
	);
}