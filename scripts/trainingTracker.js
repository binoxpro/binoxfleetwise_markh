

 function newtrainingTracker(){
$('#dlgtrainingTracker').dialog('open').dialog('setTitle','Enter trainingTracker ');
$('#frmtrainingTracker').form('clear');
 url='controller/trainingTrackerController.php?action=add'; 
}

 function edittrainingTracker(){
 var row=$('#dgtrainingTracker').datagrid('getSelected');
 if(row)
{
 $('#dlgtrainingTracker').dialog('open').dialog('setTitle','Edit trainingTracker');
 $('#frmtrainingTracker').form('load',row); 
 url='controller/trainingTrackerController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletetrainingTracker(){
 var row=$('#dgtrainingTracker').datagrid('getSelected');
 if(row)
{
$.post('controller/trainingTrackerController.php?action=delete&id='+row.id,{},function(data){ $('#dgtrainingTracker').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savetrainingTracker(){
 saveForm('#frmtrainingTracker',url,'#dlgtrainingTracker','#dgtrainingTracker');
}