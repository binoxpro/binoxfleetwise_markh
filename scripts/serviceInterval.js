

 function newserviceInterval(){
$('#dlgserviceInterval').dialog('open').dialog('setTitle','Enter serviceInterval ');
$('#frmserviceInterval').form('clear');
 url='controller/serviceIntervalController.php?action=add'; 
}

 function editserviceInterval(){
 var row=$('#dgserviceInterval').datagrid('getSelected');
 if(row)
{
 $('#dlgserviceInterval').dialog('open').dialog('setTitle','Edit serviceInterval');
 $('#frmserviceInterval').form('load',row); 
 url='controller/serviceIntervalController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteserviceInterval(){
 var row=$('#dgserviceInterval').datagrid('getSelected');
 if(row)
{
$.post('controller/serviceIntervalController.php?action=delete&id='+row.id,{},function(data){ $('#dgserviceInterval').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveserviceInterval(){
 saveForm('#frmserviceInterval',url,'#dlgserviceInterval','#dgserviceInterval');
}
function addComponentList(){
 var row=$('#dgserviceInterval').datagrid('getSelected');
 if(row)
{
 $('#dlgaddComponent').dialog('open').dialog('setTitle','Add Components to model');
 //load the Grids
 	$('#dgcomponentList').datagrid('load',{modelId:row.id,cid:row.makeName});
 	$('#dgcomponentMyList').datagrid('load',{modelId:row.id});
 	$('#mid').val(row.id);
	$('#mid2').val(row.makeName);
}
 else
 {
	$.messager.show({title:'Warning!',msg:'Please select a modal to be managed'});
	} 
}

function addMyComponent()
{
	var id="";
	var modelId=$('#mid').val();
	var modelId2=$('#mid2').val();
	//get the check alerts
	var rows=$('#dgcomponentList').datagrid('getSelections');	
	for(var i=0;i<rows.length;i++)
	{
		var row=rows[i];
		if(i==0)
		{
		id=row.id;	
		}else
		{
			id=id+"-"+row.id
		}
	}
	//alert(id);
	$.post('controller/componentScheduleController.php?action=add&cmId='+id+'&serviceIntervalId='+modelId,{cmIdx:id,serviceIntervalIdx:modelId},function(data){
		$('#dgcomponentList').datagrid('load',{modelId:modelId,cid:modelId2});
 		$('#dgcomponentMyList').datagrid('load',{modelId:modelId});
		
	})
}

function removeMyComponent()
{
	var id="";
	var modelId=$('#mid').val();
	var modelId2=$('#mid2').val();
	//get the check alerts
	var rows=$('#dgcomponentMyList').datagrid('getSelections');	
	for(var i=0;i<rows.length;i++)
	{
		var row=rows[i];
		if(i==0)
		{
		id=row.id;	
		}else
		{
			id=id+"-"+row.id
		}
	}
	//alert(id);
	$.post('controller/componentScheduleController.php?action=delete&id='+id,{cmId:id},function(data){
		$('#dgcomponentList').datagrid('load',{modelId:modelId,cid:modelId2});
 		$('#dgcomponentMyList').datagrid('load',{modelId:modelId});
		
	})
}

$(function(){	
		$('#dgserviceInterval').datagrid({
			//rowStyler:function(index,row){
				//if(parseFloat(row.balance2)<=0.00 || row.balance2==null){
					//return 'background-color:#FFD5D5;';
				//}else{
					
					//return 'background-color:#CAE4FF;';
				//}
			//},
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					//rowStyler:function(index,row){
//				if(row.account_type=='ACA'){
//					return 'background:#CAE4FF;';
//				}else{
//					
//				}
//			},
					url:'controller/componentScheduleController.php?action=view&id='+row.id,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'Please wait the service information is loading',
					height:'auto',
					columns:[[{field:'componentName',title:'Component',width:90},{field:'actionNote',title:'Action',width:70}]],
					onResize:function(){
						$('#dgserviceInterval').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgserviceInterval').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgserviceInterval').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						//$('#dgTransaction').datagrid('expandRow',0);	
						}
		});
	});
	
	function applyActionNote()
	{
		var id="";
		var modelId=$('#mid').val();
		var modelId2=$('#mid2').val();
		var apply=$('#ann').combobox('getValue');
		var rows=$('#dgcomponentMyList').datagrid('getSelections');	
			for(var i=0;i<rows.length;i++)
			{
				var row=rows[i];
				if(i==0)
				{
				id=row.id;	
				}else
				{
					id=id+"-"+row.id
				}
			}
	
	$.post('controller/componentScheduleController.php?action=edit&id='+id,{cmId:id,apply:apply},function(data){
		$('#dgcomponentList').datagrid('load',{modelId:modelId,cid:modelId2});
 		$('#dgcomponentMyList').datagrid('load',{modelId:modelId});
		
	});	
	}
