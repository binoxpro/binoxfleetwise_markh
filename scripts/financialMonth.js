

 function newfinancialMonth(){
$('#dlgfinancialMonth').dialog('open').dialog('setTitle','Enter financialMonth ');
$('#frmfinancialMonth').form('clear');
 url='controller/financialMonthController.php?action=add'; 
}

 function editfinancialMonth(){
 var row=$('#dgfinancialMonth').datagrid('getSelected');
 if(row)
{
 $('#dlgfinancialMonth').dialog('open').dialog('setTitle','Edit financialMonth');
 $('#frmfinancialMonth').form('load',row); 
 url='controller/financialMonthController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletefinancialMonth(){
 var row=$('#dgfinancialMonth').datagrid('getSelected');
 if(row)
{
$.post('controller/financialMonthController.php?action=delete&id='+row.id,{},function(data){ $('#dgfinancialMonth').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savefinancialMonth(){
 saveForm('#frmfinancialMonth',url,'#dlgfinancialMonth','#dgfinancialMonth');
}
 function checkBoxFormatter(val,row)
 {
  if(val==0)
  {
   return "<input type='checkbox'  value='1' readonly='readonly'  />";
  }else
  {
   return "<input type='checkbox' value='0' readonly='readonly' checked/>";
  }
 }