

 function newgeneralLedger(){
$('#dlggeneralLedger').dialog('open').dialog('setTitle','Enter generalLedger ');
$('#frmgeneralLedger').form('clear');
 url='controller/generalLedgerController.php?action=add'; 
}

 function editgeneralLedger(){
 var row=$('#dggeneralLedger').datagrid('getSelected');
 if(row)
{
 $('#dlggeneralLedger').dialog('open').dialog('setTitle','Edit generalLedger');
 $('#frmgeneralLedger').form('load',row); 
 url='controller/generalLedgerController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletegeneralLedger(){
 var row=$('#dggeneralLedger').datagrid('getSelected');
 if(row)
{
$.post('controller/generalLedgerController.php?action=delete&id='+row.id,{},function(data){ $('#dggeneralLedger').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savegeneralLedger(){
 saveForm('#frmgeneralLedger',url,'#dlggeneralLedger','#dggeneralLedger');
}