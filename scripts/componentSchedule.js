

 function newcomponentSchedule(){
$('#dlgcomponentSchedule').dialog('open').dialog('setTitle','Enter componentSchedule ');
$('#frmcomponentSchedule').form('clear');
 url='controller/componentScheduleController.php?action=add'; 
}

 function editcomponentSchedule(){
 var row=$('#dgcomponentSchedule').datagrid('getSelected');
 if(row)
{
 $('#dlgcomponentSchedule').dialog('open').dialog('setTitle','Edit componentSchedule');
 $('#frmcomponentSchedule').form('load',row); 
 url='controller/componentScheduleController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletecomponentSchedule(){
 var row=$('#dgcomponentSchedule').datagrid('getSelected');
 if(row)
{
$.post('controller/componentScheduleController.php?action=delete&id='+row.id,{},function(data){ $('#dgcomponentSchedule').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savecomponentSchedule(){
 saveForm('#frmcomponentSchedule',url,'#dlgcomponentSchedule','#dgcomponentSchedule');
}