// JavaScript Document
$(function(){
	$("#dgDelivery").edatagrid({
				method:'get',
				url:'controller/deliveryController.php?action=view',
				saveUrl:'controller/deliveryController.php?action=add',
				updateUrl:'controller/deliveryController.php?action=update',
				destroyUrl:'controller/deliveryController.php?action=delete',
				onSuccess:function(index,row){
					$("#dgDelivery").datagrid('reload');
					
				}
			});	
	
})

function refreshGird()
{
$("#dgDelivery").datagrid('reload');	
}
function saveDelivery(){
	try{
		javascript:$('#dgDelivery').edatagrid('saveRow');
		refreshGird();
	}catch(err)
	{
		
	}
}
function exportToExcel(){
	$.messager.prompt("Export","Enter Date?",function(r){
	if(r){
		$.post('controller/deliveryController.php?action=excelExport',{date:r},function(res){
		JSONToCSVConvertor(res,"Orders Delivered:"+r,true);
	});
	}
	}
	);
}

function viewSearchC(){
	var x =$('#startDate').datebox('getValue');
	var y =$('#endDate').datebox('getValue');
	$('#dgDelivery').datagrid('load',{startDate:x,endDate:y})
}