

 function newserviceHeader(){
$('#dlgserviceHeader').dialog('open').dialog('setTitle','Enter serviceHeader ');
$('#frmserviceHeader').form('clear');
 url='controller/serviceHeaderController.php?action=add'; 
}

 function editserviceHeader(){
 var row=$('#dgserviceHeader').datagrid('getSelected');
	 if(row)
	{
	 $('#dlgserviceHeader').dialog('open').dialog('setTitle','Edit serviceHeader');
	 $('#frmserviceHeader').form('load',row); 
	 url='controller/serviceHeaderController.php?action=edit&id='+row.id;
	}
	 else{
	   
	$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
	} 
}

 function deleteserviceHeader(){
 var row=$('#dgserviceHeader').datagrid('getSelected');
 if(row)
{
$.post('controller/serviceHeaderController.php?action=delete&id='+row.id,{},function(data){ $('#dgserviceHeader').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveserviceHeader(){
 saveForm('#frmserviceHeader',url,'#dlgserviceHeader','#dgserviceHeader');
}
function modelCombox(x)
{
	//var id=$('vehicleId').combobox('getValue');
	
$('#serviceIntervalId').combobox('reload','controller/serviceIntervalController.php?action=viewForModel&id='+x);
getServiceDue(x);	
}
function getServiceDue(x)
{
	$.post('controller/serviceHeaderController.php?action=getLatest',{id:x},function(data){
	$("#purposedService").val(data);	
	})
	{
	}
}
function getInterval(x)
{
	$.post('controller/serviceIntervalController.php?action=getInterval&id=',{id:x},function(data){
	$("#interval").val(data);	
	})
	{
	}
}
function getNextReading(x)
{
	var interval=parseFloat($("#interval").val()	);
	var xValue=parseFloat(x);
	var answer=interval+xValue;
	$("#nextReading").val(answer);
	$("#reminderReading").val(answer-1000);
}
function subService()
{
	var row=$("#dgserviceHeader").datagrid("getSelected");
	if(row){
			$('#dlgsubService').dialog('open').dialog('setTitle','Component to service');
			$('#dgsubService').datagrid('load',{serviceHeaderId:row.id});
            $('#subServiceHeaderIdx').val(row.id);	
	}else
    {
		$.messager.show({title:'Warning!',msg:'Please select a item to to '});
	}

}

function ActionsubService()
{
    var row=$("#dgsubService").datagrid("getSelected");
    if(row) {
        $('#dlgsubService2').dialog('open').dialog('setTitle', 'Service Action ');
        $('#subService2').form('clear');
        $('#id').val($('#subServiceHeaderIdx').val());
        url = "controller/subServiceController.php?action=edit&idx="+row.id;
    }else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to action '});
    }
				
}
function savesubService(){
 saveForm('#subService2',url,'#dlgsubService2','#dgsubService');
}

$(function(){	
		$('#dgserviceHeader').datagrid({
			rowStyler:function(index,row){
				if(parseFloat(row.currentReading)>=parseFloat(row.reminderReading) && row.status=="Service Due" ){
					return 'background-color:red;';
				}else{
					
					//return 'background-color:#CAE4FF;';
				}
			},
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					//rowStyler:function(index,row){
//				if(row.account_type=='ACA'){
//					return 'background:#CAE4FF;';
//				}else{
//					
//				}
//			},
					url:'controller/subServiceController.php?action=view&serviceHeaderId='+row.id,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'Please wait the service information is loading',
					height:'auto',
					columns:[[{field:'componentName',title:'Component',width:90},{field:'actionNote',title:'Action',width:70},{field:'comment',title:'Comment',width:70},{field:'done',title:'Action Status',width:70}]],
					onResize:function(){
						$('#dgserviceHeader').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgserviceHeader').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgserviceHeader').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						//$('#dgTransaction').datagrid('expandRow',0);	
						}
		});
	});
function loadDocument()
{
	 var row=$('#dgserviceHeader').datagrid('getSelected');
	 if(row)
	{
	 //$('#dlgserviceHeader').dialog('open').dialog('setTitle','Edit serviceHeader');
	 //$('#frmserviceHeader').form('load',row); 
	var urlv='controller/serviceDocument.php?serviceId='+row.id+'&vehicleId='+row.regNo+'&serviceDate='+row.creationDate+'&serviceKm='+row.kmReading+'&nextReading='+row.nextReading+'&serviceType='+row.serviceName+'&driver=';
		 if(window.showModalessDialog){
		 window.showModalessDialog(urlv,'','dialogWidth=800,dialogHeight=1800,scrollbar=no,location=no,resizable=no,dialog=yes,alwaysontop=yes,maximize=no,left=0,top=0');
		}else{
			window.open(urlv,'Document',"width=800, height=1800, alwaysRaised=yes");
		}
	}
	 else
     {
	   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  } 
}