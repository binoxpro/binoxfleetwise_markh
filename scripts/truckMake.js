

 function newtruckMake(){
$('#dlgtruckMake').dialog('open').dialog('setTitle','Enter truckMake ');
$('#frmtruckMake').form('clear');
 url='controller/truckMakeController.php?action=add'; 
}

 function edittruckMake(){
 var row=$('#dgtruckMake').datagrid('getSelected');
 if(row)
{
 $('#dlgtruckMake').dialog('open').dialog('setTitle','Edit truckMake');
 $('#frmtruckMake').form('load',row); 
 url='controller/truckMakeController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletetruckMake(){
 var row=$('#dgtruckMake').datagrid('getSelected');
 if(row)
{
$.post('controller/truckMakeController.php?action=delete&id='+row.id,{},function(data){ $('#dgtruckMake').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savetruckMake(){
 saveForm('#frmtruckMake',url,'#dlgtruckMake','#dgtruckMake');
}