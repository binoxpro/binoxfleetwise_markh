

 function newclient(){
$('#dlgclient').dialog('open').dialog('setTitle','Enter client ');
$('#frmclient').form('clear');
 url='controller/clientController.php?action=add'; 
}

 function editclient(){
 var row=$('#dgclient').datagrid('getSelected');
 if(row)
{
 $('#dlgclient').dialog('open').dialog('setTitle','Edit client');
 $('#frmclient').form('load',row); 
 url='controller/clientController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteclient(){
 var row=$('#dgclient').datagrid('getSelected');
 if(row)
{
$.post('controller/clientController.php?action=delete&id='+row.id,{},function(data){ $('#dgclient').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveclient(){
 saveForm('#frmclient',url,'#dlgclient','#dgclient');
}