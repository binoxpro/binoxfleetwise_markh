

 function newdebitor(){
$('#dlgdebitor').dialog('open').dialog('setTitle','Enter debitor ');
$('#frmdebitor').form('clear');
 url='controller/debitorController.php?action=add'; 
}

 function editdebitor(){
 var row=$('#dgdebitor').datagrid('getSelected');
 if(row)
{
 $('#dlgdebitor').dialog('open').dialog('setTitle','Edit debitor');
 $('#frmdebitor').form('load',row); 
 url='controller/debitorController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletedebitor(){
 var row=$('#dgdebitor').datagrid('getSelected');
 if(row)
{
$.post('controller/debitorController.php?action=delete&id='+row.id,{},function(data){ $('#dgdebitor').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savedebitor(){
 saveForm('#frmdebitor',url,'#dlgdebitor','#dgdebitor');
}
 function checkBoxFormatter(val,row)
 {
  if(val==0)
  {
   return "<input type='checkbox'  value='1' readonly='readonly'  />";
  }else
  {
   return "<input type='checkbox' value='0' readonly='readonly' checked/>";
  }
 }