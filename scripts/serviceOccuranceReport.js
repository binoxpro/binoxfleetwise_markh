$(function(){
    $.post("reportController/serviceOccuranceReport.php",{},function(data){
        $('#reportViewer').html(data);
    })

})

function searchData(){
    var startDate=$('#startDate').datebox('getValue');
    var endDate=$('#endDate').datebox('getValue');
    var vehicleId=$('#vehicleId').combobox('getValue');
    if(startDate!="" && endDate!="")
    {
        $.post("reportController/serviceOccuranceReport.php", {action: 'yes', startDate:startDate,endDate:endDate}, function (data) {
            $('#reportViewer').html(data);
        })
    }else
    {
        $.post("reportController/serviceOccuranceReport.php",{},function(data){
            $('#reportViewer').html(data);
        })
    }

}

function kmsLess(){
    var x=$('#km').val();
    $.post("reportController/mountedTireReport.php",{kmsx:x},function(data){
        $('#reportViewer').html(data);
    })

}