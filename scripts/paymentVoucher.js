

 function newpaymentVoucher(){
$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Enter paymentVoucher ');
$('#frmpaymentVoucher').form('clear');
 url='controller/paymentVoucherController.php?action=add'; 
}

 function editpaymentVoucher(){
 var row=$('#dgpaymentVoucher').datagrid('getSelected');
 if(row)
{
 $('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
 $('#frmpaymentVoucher').form('load',row); 
 url='controller/paymentVoucherController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletepaymentVoucher(){
 var row=$('#dgpaymentVoucher').datagrid('getSelected');
 if(row)
{
$.post('controller/paymentVoucherController.php?action=delete&id='+row.id,{},function(data){ $('#dgpaymentVoucher').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savepaymentVoucher(){
  url="controller/paymentVoucherController.php?action=add";
  var urlx="admin.php?view=payment_voucher_list";
 //saveForm('#frmpaymentVoucher',url,'#dlgpaymentVoucher','#dgpaymentVoucher');
  saveFormUrl2('#frmpaymentVoucher',url,urlx);
}
 $(function(){

  $.post('controller/financialYearController.php',{action:'getActiveYear'},function(data)
  {
   var data2= $.parseJSON(data);
    //alert(data2.id);
   $('#yearCode').val(data2.yearId);
   $('#monthCode').val(data2.id);
  })
 });
 $(function()
 {
  var id=qs('id');
  if(id!==undefined)
  {
    $.post('controller/paymentVoucherController.php', {action: 'getVoucherNo',id:id}, function (data) {
    var data2= $.parseJSON(data);
     //$('#id').val(id);
     $('#frmpaymentVoucher').form('load',data2);
     prepare_paymentVoucherItem();
    })
  }else {
   $.post('controller/paymentVoucherController.php', {action: 'getVoucherNo'}, function (data) {

    $('#id').val(data);
    prepare_paymentVoucherItem();
   })
  }
 });
 function prepare_paymentVoucherItem()
 {

  var id=$('#id').val();
  //prepare_fuelOrder();
  $('#dgpaymentVoucherItem').edatagrid({
   url: 'controller/paymentVoucherItemController.php?action=view&payVoucherId='+id,
   saveUrl: 'controller/paymentVoucherItemController.php?action=add&payVoucherId='+id,
   updateUrl: 'controller/paymentVoucherItemController.php?action=edit&payVoucherId='+id,
   destroyUrl:'controller/paymentVoucherItemController.php?action=delete&payVoucherId='+id,
   onSuccess:function()
   {
    $('#dgpaymentVoucherItem').datagrid('reload');
   }
  });

 }
 function validateDate(date)
 {
     var year=date.getFullYear();
     var month=(date.getMonth()+1)<9?'0'+(date.getMonth()+1):(date.getMonth()+1);
    var calVal=month+'/'+year;
     var fm=$('#monthCode').val();
     //var array=fm.split('/');
    //alert("cal:"+calVal+"fm:"+fm);
    if(calVal===fm)
    {

    }else{
     $.messager.alert('Refixed Fleet Solution','You can not Transact in a closed Financial month please first open the transaction month of '+fm,'warning');
     $(this).datebox('setValue','');
    }

 }

