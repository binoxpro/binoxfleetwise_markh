

 function newactivity(){
$('#dlgactivity').dialog('open').dialog('setTitle','Enter activity ');
$('#frmactivity').form('clear');
 url='controller/activityController.php?action=add'; 
}

 function editactivity(){
 var row=$('#dgactivity').datagrid('getSelected');
 if(row)
{
 $('#dlgactivity').dialog('open').dialog('setTitle','Edit activity');
 $('#frmactivity').form('load',row); 
 url='controller/activityController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteactivity(){
 var row=$('#dgactivity').datagrid('getSelected');
 if(row)
{
$.post('controller/activityController.php?action=delete&id='+row.id,{},function(data){ $('#dgactivity').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveactivity(){
 saveForm('#frmactivity',url,'#dlgactivity','#dgactivity');
}