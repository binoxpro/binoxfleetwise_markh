// JavaScript Document
$(function(){
	$.post("controller/orderController.php?action=dasbboard",{},function(data){
		$('#orderSummary').html(data);
	})		
	
})
$(function(){
	$.post("reportController/stockSummaryReport.php",{},function(data){
		$('#tireSummary').html(data);
	})		
	
})
$(function(){
	$.post("controller/serviceTrackController.php?action=serviceDashboard",{},function(data){
		$('#serviceSummary').html(data);
	})		
	
})
$(function(){
	var ctx = document.getElementById("canvas").getContext("2d");
	var myLine = new Chart(ctx);
	 
	
	var dataVal2;
	$.post('controller/capacityController.php',{action:'graphData'},function(dataVal){
		//alert(dataVal);
		var util=new Array();
		//alert(dataVal);
		var utilisation=dataVal.split('/');
		 for(var c=1;c<13;c++){
			 util[c-1]=utilisation[c];
		 }
		//alert(util.length)
		var lineChartData;
		 lineChartData = {
		labels : ["Jan","Feb","Mar","Apr","May","Jun","jul","Aug","Sept","Oct","Nov","Dec"],
		datasets : [
			{
				label:'Income',
				fillColor: "rgba(151,203,285,0.3)",
				strokeColor: "rgba(0,0,149,3)",
				pointColor: "rgba(0,0,149,3)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data:util
			}
		]

	}
		myLine.Line(lineChartData,{responsive: true,scaleGridLineColor:"rgba(53,154,255,0.7)",scaleGridLineWidth:2,bezierCurve:true,animation: false});
	 
	});
	
});