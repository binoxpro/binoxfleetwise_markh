var url2='';
var url3='';

var prfNo=0;
var lopNo=0;
var itemId=0;
var quantity=0;
var rate=0;
function issuePart()
{
    var row=$('#dgpartRequestForm').datagrid('getSelected');
    if(row)
    {
        prfNo=row.prfId;
        if(prfNo>0)
        {
            $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Edit partRequestForm');
            $('#frmpartRequestForm').form('load',row);
            //reloading..
            $('#dglocalpurchaseorder').datagrid('load',{id:row.prfId});
            url='controller/partRequestFormController.php?action=closeLocalpurchase&id='+row.id+'&prfId='+row.prfId+'&processId='+row.processId;
            loadingJobcard(row.jobcardId,row.prfId);
            //enableAndDisable(true);
        }else
        {
            $.messager.show({title:'Warning!',msg:'Part request number was not found'});
        }

        //url='controller/partRequestFormController.php?action=edit&id='+row.id+'&processId='+row.processId;

    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a Item to create local purchase order for.'});
    }

}

function issuePartRF()
{
    var row=$('#dgpartRequestFormItem').datagrid('getSelected');
    if(row)
    {
        $('#dlgpartRequestFormItem').dialog('open').dialog('setTitle','Edit partRequestForm');
        $('#frmpartRequestFormItem').form('load',row);
        //$('#dgpartRequestFormItem').datagrid('load',{id:row.prfId});
        url2='controller/partRequestFormItemController.php?action=issue&id='+row.id;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}


 function newpartRequestForm()
 {
     $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Enter partRequestForm ');
     $('#frmpartRequestForm').form('clear');
      url='controller/partRequestFormController.php?action=add';
 }

 function editpartRequestForm()
 {
       var row=$('#dgpartRequestForm').datagrid('getSelected');
       if(row)
       {
        $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Edit partRequestForm');
        $('#frmpartRequestForm').form('load',row);
        url='controller/partRequestFormController.php?action=edit&id='+row.id;
       }
       else
       {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
       }
}

 function deletepartRequestForm(){
 var row=$('#dgpartRequestForm').datagrid('getSelected');
 if(row)
{
$.post('controller/partRequestFormController.php?action=delete&id='+row.id,{},function(data){ $('#dgpartRequestForm').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savepartRequestForm()
 {
    // alert("Process Incomplete will be completed on 28th");
     //url='';
     $.messager.confirm('Warning','Are you sure you would to perform the action',function(r){
         if(r==true)
         {
             $.messager.progress();
             //saveForm('#frmpartRequestForm',url,'#dlgpartRequestForm','#dgpartRequestForm');
             $.post(url,{},function(result){
                 $.messager.progress('close');
                 var result = eval('('+result+')');
                 if (result.msg)
                 {
                     $.messager.alert('Error',
                         result.msg,'error');
                 }else
                 {
                     $.messager.show({title: 'Info',
                         msg: 'Successfully completed'});
                     $('#dlgpartRequestForm').dialog('close');
                     //close the dialog
                     $('#dgpartRequestForm').datagrid('reload');
                     // reload the user
                 }

             })

         }


     })

}
function savepartRequestFormItem()
{
    saveForm('#frmpartRequestFormItem',url2,'#dlgpartRequestFormItem','#dglocalpurchaseorderitem');
}
function newlocalpurchaseorder()
{
    $('#dlglocalpurchaseorder').dialog('open').dialog('setTitle','Enter localpurchaseorder ');
    $('#frmlocalpurchaseorder').form('clear');
    $('#prfIdNo').val(prfNo);
    generateLopNumber();
    enableAndDisable(true);


    url3='controller/localpurchaseorderController.php?action=add';

}


function gridView(x)
{

    $('#dglocalpurchaseorderitem').edatagrid({
        url:'controller/localpurchaseorderitemController.php?action=view&lopId='+x,
        saveUrl:'controller/localpurchaseorderitemController.php?action=add&lopId='+x,
        updateUrl:'controller/localpurchaseorderitemController.php?action=edit&lopId='+x,
        destroyUrl:'controller/localpurchaseorderitemController.php?action=delete',
        onSuccess:function(index,row)
        {
            $('#dglocalpurchaseorderitem').datagrid('reload');

        },
        onAdd:function (index,row) {
            set_values(index);
        },
        onEdit:function (index,row)
        {
            set_values(index);
        }
    });

}

function reloadItemCombo()
{
    $('#prfItemId').combobox('reload', 'controller/partRequestFormItemController.php?action=viewCombo3&prfNo='+prfNo)
}
function set_values(rowIndex)
{
    var editors=$('#dglocalpurchaseorderitem').edatagrid('getEditors',rowIndex);
    var partName=editors[0];

    $(partName.target).combobox('reload', 'controller/partRequestFormItemController.php?action=viewCombo3&prfNo='+prfNo)



}


function generateLopNumber()
{
    $.post('controller/localpurchaseorderController.php?action=generateLopNumber',{},function(data){
        lopNo=data;
        gridView(lopNo);
        $('#lopNo').val(lopNo);
    })

}
function savelocalpurchaseorder()
{
    saveForm('#frmlocalpurchaseorder',url3,'#dlglocalpurchaseorder','#dglocalpurchaseorder');
    resetVariable();
}
function resetVariable()
{
    prfNo=0;
    lopNo=0;
}
function editlocalpurchaseorder(){
    var row=$('#dglocalpurchaseorder').datagrid('getSelected');
    if(row)
    {
        $('#dlglocalpurchaseorder').dialog('open').dialog('setTitle','Edit localpurchaseorder');
        $('#frmlocalpurchaseorder').form('load',row);
        url3='controller/localpurchaseorderController.php?action=edit&id='+row.id;
        prfNo=row.prfIdNo;
        lopNo=row.lopNo;
        gridView(lopNo);
        enableAndDisable(true);
    }
    else{
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});}
}
function receiveDeliveryItems()
{

    var row=$('#dglocalpurchaseorder').datagrid('getSelected');
    if(row)
    {
        $('#dlglocalpurchaseorder').dialog('open').dialog('setTitle','Edit localpurchaseorder');
        $('#frmlocalpurchaseorder').form('load',row);
        url3='controller/localpurchaseorderController.php?action=edit&id='+row.id;
        prfNo=row.prfIdNo;
        lopNo=row.lopNo;
        gridView(lopNo);
        enableAndDisable(false);
        //disable editing of the grid.
        $('#dglocalpurchaseorderitem').edatagrid('disableEditing');

    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Local purchase order not selected'});
    }

}

function enableAndDisable(enableStatus)
{
    if (enableStatus)
    {
        $('#r1').removeClass("disabled").find("a").removeAttr("onclick");
        $('#r2').removeClass("disabled").find("a").removeAttr("onclick");
        $('#r3').removeClass("disabled").find("a").removeAttr("onclick");
        //my form elements

        //$("#supplieId").removeAttr('readonly');
        $('#supplieId').combobox('enable');
        $("#creationDate").datebox('enable','true');


    }else
    {
        $("#r1").addClass("disabled").find("a").attr("onclick", "return false;");
        $("#r2").addClass("disabled").find("a").attr("onclick", "return false;");
        $("#r3").addClass("disabled").find("a").attr("onclick", "return false;");
        //$("#supplieId").attr('readonly','readonly');
        $('#supplieId').combobox('disable');
        $("#creationDate").datebox('disable','true');

    }
}
function receivedItem()
{
    var row=$('#dglocalpurchaseorderitem').datagrid('getSelected');
    if(row)
    {
         quantity=0;
        itemId=row.prfItemId;
        quantity=parseInt(row.qty);
        rate=parseInt(row.rate);
        var deliveredQty=parseInt(row.qtyDelivery);

            $('#dlglopdelivery').dialog('open').dialog('setTitle', 'Require');
            $('#frmlopdelivery').form('load', row);
            url4 = 'controller/lopdeliveryController.php?action=add&purfitemId=' + row.prfItemId + '&rate=' + row.rate+'&id='+row.id;

            //prfNo=row.prfIdNo;
            //lopNo=row.lopNo;
            //gridView(lopNo);
            //enableAndDisable(false);
            //disable editing of the grid.
            $('#dglocalpurchaseorderitem').edatagrid('disableEditing');
        
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Local purchase order not selected'});
    }

}

function quantityTestPassed(x)
{
        var result=(parseInt(quantity)-parseInt(x));
        //alert(quantity)
        result=result==0?'YES':'NO';
        $('#passed1').val(result);
        quantity=0;

}


function savelopdelivery()
{
    saveForm('#frmlopdelivery',url4,'#dlglopdelivery','#dglopdelivery');
}

function loadingJobcard(x,y)
{
    $.post('controller/jobCardController.php?action=softcopy_jobcard',{jobcardId:x,prfId:y},function(data)
    {

        $('#loadJobcardArea').html(data)
    })
}