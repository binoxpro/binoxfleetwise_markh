$(function(){
    var id=qs('id');
    getLastInspection(id);
    
    
})

function getLastInspection(x){
	$.post('controller/tireInspectionHeaderController.php?action=getInspectionDetails',{id:x},function(res){
		$('#inspectionHeader').html(res);
		
	});
}

function printCopy(){
	var id=$("#inspectionId").val();
	//var urlxv="controller/email2.php?idx="+$("#jobid3").val()+"&title="+title+"&tin="+tin;
	var urlxv="printCopy.php?id="+id;
	if(window.showModalessDialog){
	 window.showModalessDialog(urlxv,'','dialogWidth=800,dialogHeight=1800,scrollbar=no,location=no,resizable=no,dialog=yes,alwaysontop=yes,maximize=no,left=0,top=0');
	}else{
		window.open(urlxv,"Invoice","width=800, height=1800, alwaysRaised=yes");
	}
}

 function newtireInspectionDetails(){
$('#dlgtireInspectionDetails').dialog('open').dialog('setTitle','Enter tireInspectionDetails ');
$('#frmtireInspectionDetails').form('clear');
 url='controller/tireInspectionDetailsController.php?action=add'; 
}

 function edittireInspectionDetails(){
 var row=$('#dgtireInspectionDetails').datagrid('getSelected');
 if(row)
{
 $('#dlgtireInspectionDetails').dialog('open').dialog('setTitle','Edit tireInspectionDetails');
 $('#frmtireInspectionDetails').form('load',row); 
 url='controller/tireInspectionDetailsController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletetireInspectionDetails(){
 var row=$('#dgtireInspectionDetails').datagrid('getSelected');
 if(row)
{
$.post('controller/tireInspectionDetailsController.php?action=delete&id='+row.id,{},function(data){ $('#dgtireInspectionDetails').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savetireInspectionDetails(){
 saveForm('#frmtireInspectionDetails',url,'#dlgtireInspectionDetails','#dgtireInspectionDetails');
}