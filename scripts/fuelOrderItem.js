

 function newfuelOrderItem(){
$('#dlgfuelOrderItem').dialog('open').dialog('setTitle','Enter fuelOrderItem ');
$('#frmfuelOrderItem').form('clear');
 url='controller/fuelOrderItemController.php?action=add'; 
}

 function editfuelOrderItem(){
 var row=$('#dgfuelOrderItem').datagrid('getSelected');
 if(row)
{
 $('#dlgfuelOrderItem').dialog('open').dialog('setTitle','Edit fuelOrderItem');
 $('#frmfuelOrderItem').form('load',row); 
 url='controller/fuelOrderItemController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletefuelOrderItem(){
 var row=$('#dgfuelOrderItem').datagrid('getSelected');
 if(row)
{
$.post('controller/fuelOrderItemController.php?action=delete&id='+row.id,{},function(data){ $('#dgfuelOrderItem').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savefuelOrderItem(){
 saveForm('#frmfuelOrderItem',url,'#dlgfuelOrderItem','#dgfuelOrderItem');
}