var urlItem='';
// JavaScript Document
function loadUserInterface(){
	var x=$('#checkTypeCombobox').combobox('getValue');
	$.post('controller/inspectionController.php',{id:x,action:'loadItem'},function(data){
		
		$('#inspection').html(data);
	});
	
}
//$(function(){
//	var x=qs('id');
//	$.post('controller/jobcardItemController.php',{action:'loadItem',id:x},function(data){
//		$('#jobcarditemDetails').html(data);
//	})
//});
$(function(){
	var x=qs('id');
	if(x!=undefined)
	{
		$.post('controller/jobcardItemController.php',{action:'loadItemHeader',id:x},function(data2){
		$('#jobcardHeader').html(data2);
            getprfid(x);
        gridView();
        gridView2();
	});
	}else{
		window.location='admin.php';
	}
});

//$(function(){
//	var x=qs('id');
//	var y=qs('other');
//	
//	$.post('controller/partsUsedController.php',{action:'loadParts',id:x},function(data2){
//		$('#partsUsed').html(data2);
//	})
//});
//function editItem(x){
//	$.post('controller/jobcardItemController.php',{action:'getItem',id:x},function(data2){
//		//alert(data2.details);
//		$('#details').html(data2.details);
//		$('#timetaken').val(data2.timeTaken);
//		$('#initials').val(data2.initials);
//		$('#id').val(x);
//		$('#action').val("Update");
//	},'json');
//}

function gridView()
{
    var x=qs('id');

    	$('#dgPartUsed').edatagrid({
				url:'controller/partsUsedController.php?action=view&jobCardId='+x,
				saveUrl:'controller/partsUsedController.php?action=add&jobCardId='+x,
				updateUrl:'controller/partsUsedController.php?action=edit&jobCardId='+x,
				destroyUrl:'controller/partsUsedController.php?action=delete',
				onSuccess:function(index,row)
				{
                    //alert(row.partRequest)
					//$('#partrequestformid').val(row.partRequest);
                    getprfid(x);
					$('#dgPartUsed').datagrid('reload');
					
				},
            onEdit:function(index,row){
			//alert(row.prfiStatus)
			if(row.prfiStatus!='NOT ISSUED')
			{

				$.messager.alert('Warning','The Item cannot be edited');
                //$('#dgPartUsed').edatagrid('enableEditing');
                $('#dgPartUsed').datagrid('reload');

			}else
			{
				//alert('stuff');
                //$('#dgPartUsed').edatagrid('disableEditing');

			}
            }
			});
    
}
function gridView2()
{
    var x=qs('id');
    	$('#dgJobcardItemDetails').edatagrid({
				url:'controller/jobcardItemController.php?action=view&jobCardId='+x,
				saveUrl:'controller/jobcardItemController.php?action=add&jobCardId='+x,
				updateUrl:'controller/jobcardItemController.php?action=edit&jobCardId='+x,
				destroyUrl:'controller/jobcardItemController.php?action=delete',
			
				onSuccess:function(index,row)
				{

					$('#dgJobcardItemDetails').datagrid('reload');
					
				}
			});
}

function formatterCellStyle(val,row)
{
	if(val==0)
	{
		return 'NO';
	}else if(val==1){
		return 'YES';
	}

}

function editpartSupply()
{
	var row=$('#dgPartUsed').datagrid('getSelected');
	if(row)
	{
		$('#dlgpartSupply').dialog('open').dialog('setTitle','Edit partSupply');
		$('#frmpartSupply').form('clear');
		$('#frmpartSupply').form('load',row);
		url='controller/partSupplyController.php?action=add';
	}
	else
	{
		$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
	}
}
function savepartSupply()
{
	saveForm('#frmpartSupply',url,'#dlgpartSupply','#dgPartUsed');
}
function workcarriedout()
{
	var row=$('#dgJobcardItemDetails').datagrid('getSelected');
	if(row)
	{
		$('#dlglabourPayment').dialog('open').dialog('setTitle','Work Carried Out');
		$('#frmlabourPayment').form('clear');
		$('#frmlabourPayment').form('load',row);
		//$('#labourId').val(row.id);

		url='controller/jobcardItemController.php?action=edit&id='+row.id;
	}
	else
	{
		$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
	}
}
function savelabourPayment()
{
	saveForm('#frmlabourPayment',url,'#dlglabourPayment','#dgJobcardItemDetails');
}

function newitem(){
    $('#dlgitem').dialog('open').dialog('setTitle','Enter item ');
    $('#frmitem').form('clear');
    urlItem='controller/itemController.php?action=add';
}


function saveitem(){
    saveForm('#frmitem',urlItem,'#dlgitem','#dgitem');
    gridView();
    gridView2();
}

function submittostore()
{


    var prfIdv=$('#partrequestformid').val();
    if(prfIdv!=''&& prfIdv!=null)
	{
		$.messager.confirm('Connect to Server','Are you sure that you would like to Submit this information.....(Yes/No)',function(r){

			if(r==true)
			{
                console.log(r)
                $.post('controller/partsUsedController.php?action=sendtostore',{prfId:prfIdv},function(data)
                {
                    //$.messager.alert('Please ',data);
                    var result = eval('('+data+')');
                    if(result.msg)
                    {
                        $.messager.alert('System Occurred ',result.msg);
                    }else
                    {
                        window.location='admin.php?view=Follow_Up_JobCard';
                    }
                    //window.location='admin.php?view=Follow_Up_JobCard';
                })
			}else
			{
				console.log(r)
			}



		})

       // window.location='admin.php?view=Follow_Up_JobCard';
	}else
	{
        $.messager.alert('Warning!','Part Request Form is not exit');
	}

}

function completeJobcard()
{
    $.messager.confirm('Connect to Server','Are you sure that you would like to Submit this information.....(Yes/No)',function(r){

        if(r==true)
        {
            $.messager.alert('Error System Occurred ','The Function has noot ddone');

        }else
		{
		}
    })

}
function getprfid(id)
{
	$.post('controller/partsUsedController.php?action=getprfid',{jobCardId:id},function(data){
        $('#partrequestformid').val(data);
	})
}


