
 function newTrip()
 {
     $('#dlgTrip').dialog('open').dialog('setTitle','Enter trip ');
     $('#frmTrip').form('clear');
      url='controller/tripController.php?action=add';
 }

 function editTrip(){
 var row=$('#dgTrip').datagrid('getSelected');
 if(row)
{
 $('#dlgTrip').dialog('open').dialog('setTitle','Edit trip');
 $('#frmTrip').form('load',row); 
 url='controller/tripController.php?action=edit&TripNumber='+row.TripNumber;
 prepare_tripOtherExpense();
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteTrip()
 {
   var row=$('#dgTrip').datagrid('getSelected');
   if(row)
  {
  $.post('controller/tripController.php?action=delete&TripNumber='+row.TripNumber,{},function(data){ $('#dgTrip').datagrid('reload');});
  }
   else
   {
  $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
   }
}

 function saveTrip(){
 saveForm('#frmTrip',url,'#dlgTrip','#dgTrip');
}
 function getTripNumber(tripTypeId)
 {
  $.post('controller/tripController.php?action=getTripNumber',{tripTypeId:tripTypeId},function(data){
    $('#TripNumber').val(data);
   //do the datagrids Loading
    prepare_tripOtherExpense();
   prepare_fuelOrder();


  });
 }
 function getLastAssignment(x)
 {
  $.post('controller/driverVehicleController.php?action=getLastDriver',{vehicleId:x},function(data){
   var data2= $.parseJSON(data);
   if(data2===null)
   {
    alert('null');
   }else
   {
    //data2.id;
    //alert(data2.vehicleId);
    //$('#activeId').val(data2.statusId);
    $('#trailerPlate').combobox('setValue',data2.trailer);
    $('#DriverId').val(data2.fullName);

   }
   //alert(data.vehicleId);

  });
 }
 function formatterCellStyle(val,row)
 {
    if(val==0)
    {
      return 'NO';
    }else{
      return 'YES';
    }

 }
 function formatterCellStyleStatus(val,row)
 {
  if(val==1)
  {
   return 'In Progress';
  }else{
   return 'Closed/Completed';
  }

 }

 function formatterCellStyleYear(val,row)
 {
  if(val=='0000-00-00')
  {
   return '';
  }else
  {
   return val;
  }

 }

 function prepare_tripOtherExpense()
 {

  var id=$('#TripNumber').val();
  prepare_fuelOrder();
  $('#dgTripOtherExpense').edatagrid({
   url: 'controller/tripOtherExpenseController.php?action=view&tripNo='+id,
   saveUrl: 'controller/tripOtherExpenseController.php?action=add&tripNo='+id,
   updateUrl: 'controller/tripOtherExpenseController.php?action=edit&tripNo='+id,
   destroyUrl:'controller/tripOtherExpenseController.php?action=delete&tripNo='+id,
   onSuccess:function()
   {
    $('#dgTripOtherExpense').datagrid('reload');
   }
  });

 }

 function prepare_fuelOrderItem()
 {

  var id=$('#id').val();
  prepare_fuelOrder();
  $('#dgfuelOrderItem').edatagrid({
   url: 'controller/fuelOrderItemController.php?action=view&fuelOrderId='+id,
   saveUrl: 'controller/fuelOrderItemController.php?action=add&fuelOrderId='+id,
   updateUrl: 'controller/fuelOrderItemController.php?action=edit&fuelOrderId='+id,
   destroyUrl:'controller/fuelOrderItemController.php?action=delete&fuelOrderId='+id,
   onSuccess:function()
   {
    prepare_fuelOrder();
    $('#dgfuelOrderItem').datagrid('reload');

   }
  });

 }



 function prepare_fuelOrder()
 {
  var id=$('#TripNumber').val();
  $('#dgfuelOrder').datagrid('load',{TripNo:id});
 }


 function getFuelOrderId()
 {
  $.post('controller/fuelOrderController.php',{action:'getId'},function(data){
   $('#id').val(data);
   $('#tripNo').val($('#TripNumber').val());
   prepare_fuelOrderItem();
  });
 }

var urlfuelOrder="";
 function newfuelOrder()
 {
  $('#dlgfuelOrder').dialog('open').dialog('setTitle','Enter fuelOrder ');
  $('#frmfuelOrder').form('clear');
  getFuelOrderId();

  urlfuelOrder='controller/fuelOrderController.php?action=add';
 }

 function editfuelOrder()
 {
  var row=$('#dgfuelOrder').datagrid('getSelected');
  if(row)
  {
   $('#dlgfuelOrder').dialog('open').dialog('setTitle','Edit fuelOrder');
   $('#frmfuelOrder').form('load',row);
   prepare_fuelOrderItem();
   urlfuelOrder='controller/fuelOrderController.php?action=edit&id='+row.id;
  }
  else
  {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
 }

 function deletefuelOrder()
 {
  var row=$('#dgfuelOrder').datagrid('getSelected');
  if(row)
  {
   $.post('controller/fuelOrderController.php?action=delete&id='+row.id,{},function(data){ $('#dgfuelOrder').datagrid('reload');});
  }
  else
  {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
 }

 function savefuelOrder(){
  prepare_fuelOrder();
  saveForm('#frmfuelOrder',urlfuelOrder,'#dlgfuelOrder','#dgfuelOrder');
  prepare_fuelOrder();
 }

 function checkBoxFormatter(val,row)
 {
   if(val==0)
   {
     return "<input type='checkbox'  value='1' readonly='readonly'  />";
   }else
   {
    return "<input type='checkbox' value='0' readonly='readonly' checked/>";
   }
 }
 function viewExpense()
 {
  var row=$('#dgTrip').datagrid('getSelected');
  if(row)
  {
    $('#dlgTripExpense').dialog('open').dialog('setTitle','Expense Payment Voucher');
    $('#dgTripExpense').datagrid('load',{tripNo:row.TripNumber});
    $('#tripNoExpense').val(row.TripNumber);
   //$('#frmTripExpenseVoucher').form('load',row);
   //url='controller/paymentVoucherController.php?action=edit&TripNumber='+row.TripNumber;
   //prepare_tripOtherExpense();
  }
  else
  {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
 }

 function payExpense()
 {
  var row=$('#dgTripExpense').datagrid('getSelected');
  if(row)
  {
   $('#dlgTripExpenseVoucher').dialog('open').dialog('setTitle','Expense Payment Voucher');
   $('#frmTripExpenseVoucher').form('clear');
   $('#tripNo3').val(row.tripNo);
   $('#accountCode').val(row.accountCode);
   $('#particular').val(row.expenseName);
   $('#expectAmount').val((row.Amount-row.Paid));
   loadingFinancialConstants();
   loadingVoucher();
   //$('#dgTripExpense').datagrid('load',{tripNo:row.TripNumber});
   //$('#tripNoExpense').val(row.TripNumber);
   //$('#frmTripExpenseVoucher').form('load',row);
   //url='controller/paymentVoucherController.php?action=edit&TripNumber='+row.TripNumber;
   //prepare_tripOtherExpense();
  }
  else
  {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
 }
 function validateDate(date)
 {
  var year=date.getFullYear();
  var month=(date.getMonth()+1)<9?'0'+(date.getMonth()+1):(date.getMonth()+1);
  var calVal=month+'/'+year;
  var fm=$('#monthCode').val();
  //var array=fm.split('/');
  //alert("cal:"+calVal+"fm:"+fm);
  if(calVal===fm)
  {

  }else{
   $.messager.alert('Refixed Fleet Solution','You can not Transact in a closed Financial month please first open the transaction month of '+fm,'warning');
   $(this).datebox('setValue','');
  }

 }
 function loadingVoucher()
 {
  $.post('controller/paymentVoucherController.php', {action: 'getVoucherNo'}, function (data) {

   $('#idV').val(data);

  })
 }
 function loadingFinancialConstants()
 {
  $.post('controller/financialYearController.php',{action:'getActiveYear'},function(data)
  {
   var data2= $.parseJSON(data);
   //alert(data2.id);
   $('#yearCode').val(data2.yearId);
   $('#monthCode').val(data2.id);
  })
 }

 function savePaymentVoucher()
 {
  var urlpayment="controller/paymentVoucherController.php?action=tripExpensePayment";
  saveForm('#frmTripExpenseVoucher',urlpayment,'#dlgTripExpenseVoucher','#dgTripExpense');
 }
 function attachWorkOrder()
 {
  var row=$('#dgTrip').datagrid('getSelected');
  if(row)
  {
    $('#dlgworkOrderTrip').dialog('open').dialog('setTitle','WorkOrder Completion');
    $('#dgworkOrder').datagrid('load',{tripNo:row.TripNumber});
    $('#tripNoWo').val(row.TripNumber);

  }
  else
  {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
  }
 }
 var urlWo="";
 function newworkOrder(){
  var tripNo=$('#tripNoWo').val();
  $('#dlgworkOrder').dialog('open').dialog('setTitle','Enter workOrder ');
  $('#frmworkOrder').form('clear');
 $('#tripNo4').val(tripNo);
  urlWo='controller/workorderController.php?action=add';
 }

 function editworkOrder(){
  var tripNo=$('#tripNoWo').val();
  var row=$('#dgworkOrder').datagrid('getSelected');
  if(row)
  {
   $('#dlgworkOrder').dialog('open').dialog('setTitle','Edit workOrder');
   $('#frmworkOrder').form('load',row);
   urlWo='controller/workorderController.php?action=edit&id='+row.id;
   $('#tripNo4').val(tripNo);
  }
  else{
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});}
 }

 function deleteworkOrder()
 {
  var row=$('#dgworkOrder').datagrid('getSelected');
  if(row)
  {
   $.post('controller/workorderController.php?action=delete&id='+row.id,{},function(data){ $('#dgworkOrder').datagrid('reload');});
  }
  else{
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});}
 }

 function saveworkOrder()
 {
   saveForm('#frmworkOrder',urlWo,'#dlgworkOrder','#dgworkOrder');
   $('#dgTrip').datagrid('reload');
 }

 function getRate(x)
 {
  //alert(x);
  $.post('controller/customerNewController.php',{customerCode:x,action:'getRate'},function(data)
  {
     $('#Rate').val(data);
 });
 }
 function getTotal()
 {
   var rate=$('#Rate').val();
   var qty=$('#Quantity').val();
  var distance=$('#distance').val();
   var amount=0;
  if(isNaN(rate) ||isNaN(qty))
  {

  }else
  {
    amount=rate*qty*distance;
    $('#Amount').val(amount);
  }
 }
 function getDistance(x)
 {
  //alert(x);
  $.post('controller/distanceController.php',{id:x,action:'getDistance'},function(data)
  {
   $('#distance').val(data);
  });
 }

 function closeTrip(){
  var row=$('#dgTrip').datagrid('getSelected');
   if(row)
   {
    $('#dlgTripClose').dialog('open').dialog('setTitle','Edit trip');
    $('#frmTripClose').form('load',row);
    url='controller/tripController.php?action=close&TripNumber='+row.TripNumber;
    //prepare_tripOtherExpense();
   }
   else
   {
    $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
   }
 }
 function savecloseTrip()
 {
    saveForm('#frmTripClose',url,'#dlgTripClose','#dgTrip');
    $('#dgTrip').datagrid('reload');
 }



