/**
 * Created by james on 10/17/2016.
 */
function checkBoxFormatter(val,row)
{
    if(val==0)
    {
        return "<input type='checkbox'  value='1' readonly='readonly'  />";
    }else
    {
        return "<input type='checkbox' value='0' readonly='readonly' checked/>";
    }
}
function editpaymentVoucher(){
    var row=$('#dgpaymentVoucher').datagrid('getSelected');
    if(row)
    {
        //$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
        //$('#frmpaymentVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&id='+row.id;
        //view=payment_voucher_list
        if(row.Posted==='0')
        {
            window.location = "admin.php?view=manage_payment_voucher&id=" + row.id;
        }else{
            $.messager.show({title:'Warning!',msg:'Please you can note Repost the Payment Voucher'});
        }
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}

function printVoucher()
{
    var row=$('#dgpaymentVoucher').datagrid('getSelected');
    if(row)
    {
        //$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
        //$('#frmpaymentVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&id='+row.id;
        //view=payment_voucher_list

        printCopy("reportController/paymentVoucherDocument.php?id="+row.id);
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}

