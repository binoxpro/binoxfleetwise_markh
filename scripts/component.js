

 function newcomponent(){
$('#dlgcomponent').dialog('open').dialog('setTitle','Enter Component');
$('#frmcomponent').form('clear');
 url='controller/componentController.php?action=add'; 
}

 function editcomponent(){
 		var row=$('#dgcomponent').datagrid('getSelected');
		 if(row)
		{
		 $('#dlgcomponent').dialog('open').dialog('setTitle','Edit Component');
		 $('#frmcomponent').form('load',row); 
		 url='controller/componentController.php?action=edit&id='+row.id;
		}
		 else{
		$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
			} 
}

 function deletecomponent(){
 var row=$('#dgcomponent').datagrid('getSelected');
 if(row)
{
$.post('controller/componentController.php?action=delete&id='+row.id,{},function(data){ $('#dgcomponent').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savecomponent(){
 saveForm('#frmcomponent',url,'#dlgcomponent','#dgcomponent');
}