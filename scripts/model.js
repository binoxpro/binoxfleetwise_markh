

 function newmodel(){
$('#dlgmodel').dialog('open').dialog('setTitle','Enter Capacity Loaded');
$('#frmmodel').form('clear');
 url='controller/truckMakeController.php?action=add';
}

 function editmodel()
 {
 var row=$('#dgmodel').datagrid('getSelected');
	 if(row)
	{
	 $('#dlgmodel').dialog('open').dialog('setTitle','Edit model');
	 $('#frmmodel').form('load',row);
	 url='controller/truckMakeController.php?action=edit&id='+row.id;
	}
	 else
	{
		$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
	}
}

 function deletemodel(){
 var row=$('#dgmodel').datagrid('getSelected');
 if(row)
{
$.post('controller/modelController.php?action=delete&modelName='+row.modelName,{},function(data){ $('#dgmodel').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savemodel()
 {
 saveForm('#frmmodel',url,'#dlgmodel','#dgmodel');
}

function addComponent(){
 var row=$('#dgmodel').datagrid('getSelected');
 if(row)
{
 $('#dlgaddComponent').dialog('open').dialog('setTitle','Add Components to model');
 //load the Grids
 $('#dgcomponentList').datagrid('load',{modelId:row.makeName});
 $('#dgcomponentMyList').datagrid('load',{modelId:row.makeName});
 $('#mid').val(row.makeName);
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a model to be managed '});} 
}
function addMyComponent()
{
	var id="";
	var modelId=$('#mid').val();
	//get the check alerts
	var rows=$('#dgcomponentList').datagrid('getSelections');	
	for(var i=0;i<rows.length;i++)
	{
		var row=rows[i];
		if(i==0)
		{
		id=row.id;	
		}else
		{
			id=id+"-"+row.id
		}
	}
	alert(id);
	$.post('controller/modelComponentController.php?action=add',{componentId:id,modelId:modelId},function(data){
		$('#dgcomponentList').datagrid('load',{modelId:modelId});
 		$('#dgcomponentMyList').datagrid('load',{modelId:modelId});
		
	})
}

function removeMyComponent()
{
	var id="";
	var modelId=$('#mid').val();
	//get the check alerts
	var rows=$('#dgcomponentMyList').datagrid('getSelections');	
	for(var i=0;i<rows.length;i++)
	{
		var row=rows[i];
		if(i==0)
		{
		id=row.id;	
		}else
		{
			id=id+"-"+row.id
		}
	}
	alert(id);
	$.post('controller/modelComponentController.php?action=delete&id='+id,{idx:id},function(data){
		$('#dgcomponentList').datagrid('load',{modelId:modelId});
 		$('#dgcomponentMyList').datagrid('load',{modelId:modelId});
		
	})
}