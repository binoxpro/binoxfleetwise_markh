

 function newdistance(){
$('#dlgdistance').dialog('open').dialog('setTitle','Enter distance ');
$('#frmdistance').form('clear');
 url='controller/distanceController.php?action=add'; 
}

 function editdistance(){
 var row=$('#dgdistance').datagrid('getSelected');
 if(row)
{
 $('#dlgdistance').dialog('open').dialog('setTitle','Edit distance');
 $('#frmdistance').form('load',row); 
 url='controller/distanceController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletedistance(){
 var row=$('#dgdistance').datagrid('getSelected');
 if(row)
{
$.post('controller/distanceController.php?action=delete&id='+row.id,{},function(data){ $('#dgdistance').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savedistance(){
 saveForm('#frmdistance',url,'#dlgdistance','#dgdistance');
}