

 function newdepartment(){
$('#dlgdepartment').dialog('open').dialog('setTitle','Enter department ');
$('#frmdepartment').form('clear');
 url='controller/departmentController.php?action=add'; 
}

 function editdepartment(){
 var row=$('#dgdepartment').datagrid('getSelected');
 if(row)
{
 $('#dlgdepartment').dialog('open').dialog('setTitle','Edit department');
 $('#frmdepartment').form('load',row); 
 url='controller/departmentController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletedepartment(){
 var row=$('#dgdepartment').datagrid('getSelected');
 if(row)
{
$.post('controller/departmentController.php?action=delete&id='+row.id,{},function(data){ $('#dgdepartment').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savedepartment(){
 saveForm('#frmdepartment',url,'#dlgdepartment','#dgdepartment');
}