/**
 * Created by james on 10/25/2016.
 */
function editInvoice(){
    var row=$('#dginvoice').datagrid('getSelected');
    if(row)
    {
        //$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
        //$('#frmpaymentVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&id='+row.id;
        //view=payment_voucher_list
        if(row.confirmed==='0')
        {
            window.location = "admin.php?view=create_invoice&id=" + row.id;
        }else
        {
            $.messager.show({title:'Warning!',msg:'Please you can note Edit the invoice Please Roll it back'});
        }
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}

function confirmInvoice()
{
    var row=$('#dginvoice').datagrid('getSelected');
    if(row)
    {
        //$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
        //$('#frmpaymentVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&id='+row.id;
        //view=payment_voucher_list
        if(row.confirmed==='0')
        {
            $('#dlgSaleConfirmation').dialog('open').dialog('setTitle','Confirmation of sales');
            $('#salefrm').form('load',row);

            url='controller/generalLedgerController.php?action=confirmSale';
            //window.location = "admin.php?view=create_invoice&id=" + row.id;
        }else
        {
            $.messager.show({title:'Warning!',msg:'Please you can note reconfirm the invoice Please Roll it back'});
        }
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select an Invoice to confirm'});
    }
}

function confirmation()
{
    saveForm('#salefrm',url,'#dlgSaleConfirmation','#dginvoice');
}
function receivePayment()
{
    var row=$('#dginvoice').datagrid('getSelected');
    if(row)
    {
        //$('#dlgpaymentVoucher').dialog('open').dialog('setTitle','Edit paymentVoucher');
        //$('#frmpaymentVoucher').form('load',row);
        //url='controller/paymentVoucherController.php?action=edit&id='+row.id;
        //view=payment_voucher_list
        if(row.confirmed==='1')
        {
            $('#dlgSaleRecepiting').dialog('open').dialog('setTitle','Receive Payment');
            $('#saleRecepitfrm').form('clear');
            $('#idx').val(row.id);
            $('#customerIdx').val(row.customerId);
            $('#customerNamex').val(row.companyName);
            //$('#').val()
            getBalanceBefore(row.id);
            url='controller/generalLedgerController.php?action=receiptSale';
            //window.location = "admin.php?view=create_invoice&id=" + row.id;
        }else
        {
            $.messager.show({title:'Warning!',msg:'Please you can not Receive payment for a porforma confirm invoice'});
        }
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select an Invoice to confirm'});
    }
}
//function get
function getBalanceBefore(x)
{
    $.post('controller/generalLedgerController.php?action=getBalance',{invoiceNo:x},function(data){
        $('#balanceBefore').numberbox('setValue',data);
        //echo


    });
}
function currentBalance()
{
    var balanceBefore=$('#balanceBefore').numberbox('getValue');
    var amount=$('#amount').numberbox('getValue');
    var balanceAfter=balanceBefore-amount;
    $('#balanceAfter').numberbox('setValue',balanceAfter);
}
function savePayment()
{
    saveForm('#saleRecepitfrm',url,'#dlgSaleRecepiting','#dginvoice');
}
function openInvoice()
{
    var row=$('#dginvoice').datagrid('getSelected');
    if(row)
    {
        printCopy('reportController/invoicePDF.php?id='+row.id+'&confirmed='+row.confirmed);
    }
}
function openClaim()
{
    var row=$('#dginvoice').datagrid('getSelected');
    if(row)
    {
        printCopy('reportController/transportClaimPDF.php?id='+row.id+'&confirmed='+row.confirmed+'&vat='+row.vat);
    }
}
