function newRole(){
	$('#dlgUser').dialog('open').dialog('setTitle','Add System User Role');
	$('#frmUser').form('clear');
	
	url='controller/usercontroller.php?action=addRole';
}
function saveRole(){
		
		
		
		$.messager.progress();
	 $('#frmUser').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Transaction Completed Successfully'});
						$('#dlgUser').dialog('close'); // close the dialog
						 $('#dgUser').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

	
	}
	
	function saveUser2(){
		
		
		
		$.messager.progress();
	 $('#frmManageUser').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Transaction Completed Successfully'});
						$('#dlgUser').dialog('close'); // close the dialog
						 $('#dgUser').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

	
	}
	
	function changePassword(){
		var row=$('#dgUser').datagrid('getSelected');
			if(row){
			$('#dlgChangePassword').dialog('open').dialog('setTitle','Change Password');
			url='controller/usercontroller.php?action=changePassword&id='+row.userID+'&email='+row.email;	
			}else{
				$.messager.show({title:'Info',msg:'select an employee to change Password'});
			}
		
	}
	
	function passwordStrength(){
	
}
function passwordMatch(){
	var newP=$("#passwordNew").val();
	var newO=$("#passwordOld").val();
	if(newP==newO){
		return true;
	}else{
		return false;
	}
}
function savePassword(){
		$.messager.progress();
	if(passwordMatch()){
	 $('#frmChangePassword').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Successfully'});
						$('#dlgChangePassword').dialog('close'); // close the dialog
						 $('#dgUser').datagrid('reload'); // reload the user
					}
		
	}
}); 
		
	}else{
		$.messager.progress('close');
		$.messager.show({title: 'Info',msg:'The password must change'});
	}
}