// JavaScript Document
function addSection()
{
	var row=$("#dgSection").datagrid('getSelected');
	if(row){
		$('#dlgSection').dialog('open').dialog('setTitle','Add Section to '+row.type);
		//$('#sectionTab').tabs('select','Section');
		$("#checkListTypeIdValue").val(row.id);
		loadCheckList(row.id);
	}else{
		$.messager.show({title:'Warning',msg:'Please select a check list type to manage'});
	}
}
function loadCheckList(id)
{
	$.post('controller/checklistTypeController.php?action=loadCheck&id='+id,{},function(data){
		$("#sectionView").html(data);
	})
}
function saveSection(){
	//
	//url='controller/sectionController.php?action=add';
	saveForm("#frmSection",url,"#dlgSection","#dgSection");
}
function editSection(){
	var row=$("#dgSection").datagrid("getSelected");
	if(row){
		$("#dlgSection").dialog('open').dialog("setTitle","Edit Section");
	$("#frmSection").form('load',row);
	
		url="controller/sectionController.php?action=edit&id="+row.id;
	}else{
			
	}
	
}
function deleteSection(){
	var row=$("#dgSection").datagrid("getSelected");
	if(row){
		$.post('controller/sectionController.php?action=delete',function(res){
			alert(res);
			
		});
	}else{
		
	}
}
	
function addSectionForm(){
	$('#dlgSectionAdd').dialog('open').dialog('setTitle','Add Section to ');
	$('#frmSectionAdd').form('clear');
	$("#checkListTypeId").val($("#checkListTypeIdValue").val());
	url="controller/sectionController.php?action=add";
		//$('#sectionTab').tabs('select','Section');
}
function saveSectionForm(){
	//
	//saveForm("#frmSectionAdd",url,"#dlgSectionAdd","#dgSection");
	$.messager.progress();
	 $("#frmSectionAdd").form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',msg: 'successfully completed'});
						$("#dlgSectionAdd").dialog('close');
						var id=$("#checkListTypeIdValue").val();
						loadCheckList(id);
						//$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


	
}

function addSectionItem(){
	$('#dlgSectionitem').dialog('open').dialog('setTitle','Add Section item to');
	//$("#checkListTypeId").val($("#checkListTypeIdValue").val());
	$('#frmSectionitem').form('clear');
	var id=$("#checkListTypeIdValue").val();
	$('#sectionCombobox').combobox('reload','controller/sectionController.php?action=view&checkListTypeId='+id);
	url="controller/sectionItemController.php?action=add";
		
}


function saveSectionItem(){
	//
	//saveForm("#frmSectionAdd",url,"#dlgSectionAdd","#dgSection");
	//$.messager.progress();
	 $("#frmSectionitem").form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title:'Warning',msg: result.msg});
					} else {
						//$.messager.show({title: 'Info',msg: 'successfully completed'});
						var id=$("#checkListTypeIdValue").val();
						loadCheckList(id);
						$("#dlgSectionitem").dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 //reload the user
		}
		
	}
}); 
		


	
}