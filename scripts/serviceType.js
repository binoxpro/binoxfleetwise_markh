

 function newserviceType(){
$('#dlgserviceType').dialog('open').dialog('setTitle','Enter Capacity Loaded');
$('#frmserviceType').form('clear');
 url='controller/serviceTypeController.php?action=add'; 
}

 function editserviceType(){
 var row=$('#dgserviceType').datagrid('getSelected');
 if(row)
{
 $('#dlgserviceType').dialog('open').dialog('setTitle','Edit serviceType');
 $('#frmserviceType').form('load',row); 
 url='controller/serviceTypeController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteserviceType(){
 var row=$('#dgserviceType').datagrid('getSelected');
 if(row)
{
$.post('controller/serviceTypeController.php?action=delete&id='+row.id,{},function(data){ $('#dgserviceType').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveserviceType(){
 saveForm('#frmserviceType',url,'#dlgserviceType','#dgserviceType');
}