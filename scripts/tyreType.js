// JavaScript Document
function newTyreType()
{
	$('#dlgTyreType').dialog('open').dialog('setTitle','Add TyreType');
	$('#frmTyreType').form('clear');
	url='controller/tyreTypeController.php?action=add';
	
}
function editTyreType()
{
	var row=$("#dgTyreType").datagrid("getSelected");
	if(row)
	{
		$('#dlgTyreType').dialog('open').dialog('setTitle','Editing TyreType Details');
		$('#frmTyreType').form('load',row);
		url='controller/tyreTypeController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Warning',msg:"Please select a TyreType to edit"});
		//alert('nnnn')
	}
}
function saveTyreType(){
	saveForm("#frmTyreType",url,"#dlgTyreType","#dgTyreType");
}
function deleteTyreType()
{
	var row=$("#dgTyreType").datagrid("getSelected");
	if(row)
	{
		//$('#dlgTyreType').dialog('open').dialog('setTitle','Editing TyreType Details');
		//$('#frmTyreType').form('load',row);
		url='controller/tyreTypeController.php?action=delete&id='+row.id;
        $.post(url,{},function(){
            
            	$.messager.show({title:'Warning',msg:"Action Done"});
                $("#dgTyreType").datagrid('reload');
        });
	}
	else{
		$.messager.show({title:'Warning',msg:"Please select a TyreType to edit"});
		//alert('nnnn')
	}
}