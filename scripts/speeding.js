// JavaScript Document

$(function () {
    $('#container').highcharts({
        xAxis: {
            min: 0,
            max: 38
        },
        yAxis: {
            min: 0
        },
        title: {
            text: 'Scatter plot with regression line'
        },
        series: [{
            type: 'line',
            name: 'Regression Line',
            data: [[0, 80], [38, 80]],
            marker: {
                enabled: false
            },
            states: {
                hover: {
                    lineWidth: 0
                }
            },
            enableMouseTracking: true
        }, {
            type: 'scatter',
            name: 'Observations',
            data: [80, 50, 75, 90, 70, 40],
            marker: {
                radius: 4
            }
        }]
    });
});