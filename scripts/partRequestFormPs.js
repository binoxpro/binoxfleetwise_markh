var url2='';
function reviewRequestForm()
{
    var row=$('#dgpartRequestForm').datagrid('getSelected');
    if(row)
    {
        $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Edit partRequestForm');
        $('#frmpartRequestForm').form('load',row);
        $('#dgpartRequestFormItem').datagrid('load',{id:row.prfId});
        loadingJobcard(row.jobcardId,row.prfId);
        url='controller/partRequestFormController.php?action=edit&id='+row.id+'&prfId='+row.prfId+'&processId='+row.processId;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }

}

function processPartRF()
{
    var row=$('#dgpartRequestFormItem').datagrid('getSelected');
    if(row)
    {
        $('#dlgpartRequestFormItem').dialog('open').dialog('setTitle','Edit partRequestForm');
        $('#frmpartRequestFormItem').form('load',row);
        //$('#dgpartRequestFormItem').datagrid('load',{id:row.prfId});
        url2='controller/partRequestFormItemController.php?action=edit&id='+row.id;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}


 function newpartRequestForm()
 {
     $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Enter partRequestForm ');
     $('#frmpartRequestForm').form('clear');
      url='controller/partRequestFormController.php?action=add';
 }

 function editpartRequestForm()
 {
       var row=$('#dgpartRequestForm').datagrid('getSelected');
       if(row)
       {
        $('#dlgpartRequestForm').dialog('open').dialog('setTitle','Edit partRequestForm');
        $('#frmpartRequestForm').form('load',row);
        url='controller/partRequestFormController.php?action=edit&id='+row.id;
       }
       else
       {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
       }
}

 function deletepartRequestForm(){
 var row=$('#dgpartRequestForm').datagrid('getSelected');
 if(row)
{
$.post('controller/partRequestFormController.php?action=delete&id='+row.id,{},function(data){ $('#dgpartRequestForm').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savepartRequestForm()
 {
     //confirm.
     //alert("Are you sure you want to complete the purchase");
     $.messager.confirm('Saving Confirmation','Are sure that you would like to submit this information. Click ok to process.',function(r){
         if(r==true)
         {
             saveForm('#frmpartRequestForm',url,'#dlgpartRequestForm','#dgpartRequestForm');
         }
     })

}
function savepartRequestFormItem()
{
    saveForm('#frmpartRequestFormItem',url2,'#dlgpartRequestFormItem','#dgpartRequestFormItem');
}

function loadingJobcard(x)
{
    $.post('controller/jobCardController.php?action=softcopy_jobcard',{jobcardId:x},function(data)
    {

        $('#loadJobcardArea').html(data)
    })
}
function lssueSparePart()
{
        //load the issue screen.


}
function purchasePart()
{
    //load the
}
function issuePartRF()
{
    var row=$('#dgpartRequestFormItem').datagrid('getSelected');
    if(row)
    {
        //Check the voilation
        if(row.status!='ISSUED')
        {
            alert(row.quantity +'/'+ row.quantityInStore)

            if (parseFloat(row.quantity) <= parseFloat(row.quantityInStore))
            {

                $('#dlgpartRequestFormItem').dialog('open').dialog('setTitle', 'Issue Request Item');
                $('#frmpartRequestFormItem').form('load', row);
                //$('#dgpartRequestFormItem').datagrid('load',{id:row.prfId});
            } else
            {
                $.messager.alert('Warning', 'Please place a purchase of the item its out of stock.');
            }
        }else
        {
            $.messager.alert('Warning', 'Please Item already issued');
        }


        url2='controller/partRequestFormItemController.php?action=issue&id='+row.id;
    }
    else
    {
        $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
    }
}
function purchaseItem()
{
    var row=$('#dgpartRequestFormItem').datagrid('getSelected');
    if(row)
    {
        var qty=0;
        var qtyStore=0;
        var qtyIndicator=0;
        //
        qty=parseInt(row.quantity);
        qtyStore=parseInt(row.quantityInStore);
        alert(row.status+'/'+qty+'/'+qtyStore)
        //var id=row.id;
        if(((row.status!='ISSUED') && (row.status!='PROCESS PURCHASE')) &&(qty>qtyStore) ) {

            $.messager.confirm('Confirmation', 'Please would you like to list is item for purchase.', function (r) {
                //alert(r);
                if (r == true)
                {
                    //the
                    //change item
                    $.post('controller/partRequestFormItemController.php?action=purchaseItems',{id:row.id},function(data){
                        var result = eval('('+data+')');
                        if (result.msg)
                        {
                            $.messager.alert('Warning', 'Please contact the support team an error occupied'+result.msg);
                        }else
                        {
                            $.messager.alert('Warning', 'Saved successfully');
                            $('#dgpartRequestFormItem').datagrid('reload');
                        }

                    })
                }else
                {
                    //alert('Cause');
                }
            })
        }else
        {
            $.messager.alert('Warning', 'Please Item already issued/ Already Purchased. You cannot please purchase for the item');
        }

    }else
    {
        $.messager.show({title:'Warning!',msg:''});
    }

}
function searchRange()
{
    var vehicleIds, startDate,endDate, jobids;
    vehicleIds=$('#vehicleIds').combobox('getValue');
    jobids=$('#jobids').val();
    startDate=$('#startDates').datebox('getValue');
    endDate=$('#endDates').datebox('getValue');
    //
    if(vehicleIds!=''&&jobids!=''&&(startDate!=''&&endDate!='')){
        $('#dgpartRequestForm').datagrid('reload',{vehicleId:vehicleIds,jobcardNo:jobids,startDate:startDate,endDate:endDate})
    }else if(vehicleIds!=''&&jobids!='')
    {
        $('#dgpartRequestForm').datagrid('reload',{vehicleId:vehicleIds,jobcardNo:jobids})
    }else if(vehicleIds!=''&&(startDate!=''&&endDate!=''))
    {
        $('#dgpartRequestForm').datagrid('reload',{vehicleId:vehicleIds,startDate:startDate,endDate:endDate})
    }else if(jobids!=''&&(startDate!=''&&endDate!=''))
    {
        $('#dgpartRequestForm').datagrid('reload',{jobcardNo:jobids,startDate:startDate,endDate:endDate})
    }else if(vehicleIds!='')
    {
        $('#dgpartRequestForm').datagrid('reload',{vehicleId:vehicleIds})
    }else if(jobids!='')
    {
        $('#dgpartRequestForm').datagrid('reload',{jobcardNo:jobids})
    }else if((startDate!=''&&endDate!=''))
    {
        $('#dgpartRequestForm').datagrid('reload',{startDate:startDate,endDate:endDate})
    }
}

function clearButton() {
    $('#vehicleIds').combobox('clear');
    $('#jobids').val('');
    $('#startDates').datebox('clear');
    $('#endDates').datebox('clear');
}

