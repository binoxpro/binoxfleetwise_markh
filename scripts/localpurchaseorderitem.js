

 function newlocalpurchaseorderitem(){
$('#dlglocalpurchaseorderitem').dialog('open').dialog('setTitle','Enter localpurchaseorderitem ');
$('#frmlocalpurchaseorderitem').form('clear');
 url='controller/localpurchaseorderitemController.php?action=add'; 
}

 function editlocalpurchaseorderitem(){
 var row=$('#dglocalpurchaseorderitem').datagrid('getSelected');
 if(row)
{
 $('#dlglocalpurchaseorderitem').dialog('open').dialog('setTitle','Edit localpurchaseorderitem');
 $('#frmlocalpurchaseorderitem').form('load',row); 
 url='controller/localpurchaseorderitemController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletelocalpurchaseorderitem(){
 var row=$('#dglocalpurchaseorderitem').datagrid('getSelected');
 if(row)
{
$.post('controller/localpurchaseorderitemController.php?action=delete&id='+row.id,{},function(data){ $('#dglocalpurchaseorderitem').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savelocalpurchaseorderitem()
 {
 saveForm('#frmlocalpurchaseorderitem',url,'#dlglocalpurchaseorderitem','#dglocalpurchaseorderitem');
}