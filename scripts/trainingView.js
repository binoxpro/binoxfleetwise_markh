// JavaScript Document
function saveLicenseManagement()
{
		$.messager.progress();
	 $('#frmLicenseMaintainance').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		},
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg)
				 	{
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Service information add'});
						$('#dlgLicenseManagement').dialog('close'); // close the dialog
						 $('#dgLicenseManagement').datagrid('reload'); // reload the user
					}
		
	}
}); 	
}


	function loadGrid()
	{
		$('#dgLicenseManagement').datagrid('load',{});
	}
	
	function searchLicenseDetails(searchVal)
	{
		$('#dgLicenseManagement').datagrid('load',{id:searchVal});
	}
	
	function newLicenseManagement()
	{
		
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Training Management');
			
			$('#frmLicenseMaintainance').form('clear');
			
			url='controller/trainingController.php?action=add';
				
	}
	
	
	function editLicenseMaintance()
	{
		var row=$('#dgLicenseManagement').datagrid('getSelected');
		if(row)
		{
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Training Management');
			$('#frmLicenseMaintainance').form('load',row);
			url='controller/trainingController.php?action=update&id='+row.id;
			
		}else
		{
			$.messager.show({title:'Warning',msg:'Please select the a training '});
		}
		
	}
	
	$(function(){
	$('#dgLicenseManagement').datagrid({
	rowStyler:function(index,row){
		
		if(row.name=="")
		{
			
				return 'background-color:#F00;';
				
				
		}
	}
	
	});
	});
		
$(function (){
	
	$('.tdate').datepicker({format:'yyyy-mm-dd'});
});

function exportToExcel(){
	
		$.post('controller/trainingController.php?action=viewJson',{},function(res){
		JSONToCSVConvertor(res,"Training Report FST ",true);
	});
	
}


