$(function(){
    $.post("reportController/truckRunningCostReport.php",{},function(data){
        $('#reportViewer').html(data);
    })

})

function searchData(){
    var startDate=$('#startDate').datebox('getValue');
    var endDate=$('#endDate').datebox('getValue');
    var vehicleId=$('#vehicleId').combobox('getValue');
    if(startDate!="" && endDate!=""&&vehicleId!="")
    {
        $.post("reportController/truckRunningCostReport.php", {action: 'yes', startDate:startDate,endDate:endDate,vehicleId:vehicleId}, function (data) {
            $('#reportViewer').html(data);
        })
    }else if(startDate!="" && endDate!="")
    {
        $.post("reportController/truckRunningCostReport.php", {action: 'yes', startDate:startDate,endDate:endDate}, function (data) {
            $('#reportViewer').html(data);
        })
    }else if(vehicleId!="")
    {
        $.post("reportController/truckRunningCostReport.php", {action: 'yes', vehicleId:vehicleId}, function (data) {
            $('#reportViewer').html(data);
        })
    }else
    {
        $.post("reportController/truckRunningCostReport.php",{},function(data){
            $('#reportViewer').html(data);
        })
    }

}

function kmsLess(){
    var x=$('#km').val();
    $.post("reportController/mountedTireReport.php",{kmsx:x},function(data){
        $('#reportViewer').html(data);
    })

}