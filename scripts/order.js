// JavaScript Document
$(function(){
	
	$("#dgOrder").edatagrid({
				method:'get',
				url:'controller/deliveryController.php?action=view',
				saveUrl:'controller/deliveryController.php?action=add',
				updateUrl:'controller/deliveryController.php?action=update',
				destroyUrl:'controller/deliveryController.php?action=delete',
				onSuccess:function(index,row){
					$("#dgOrder").datagrid('reload');
				}
			});	
	
})

function refreshGird()
{
//$("#dgOrder").edatagrid('reload');
	try{
	$("#dgOrder").edatagrid('reload');	
	}catch(err)
	{
		$.messager.show({title:'System Error',msg: err.name});
	}

}
function saveOrder(){
	try{
	javascript:$('#dgOrder').edatagrid('saveRow');
	$("#dgOrder").datagrid('reload');	
	}catch(err)
	{
		$.messager.show({title:'System Error',msg: err.name});
	}
}
function exportToExcel(){
	$.messager.prompt("Export","Enter Date?",function(r){
	if(r){
		$.post('controller/orderController.php?action=excelExport',{date:r},function(res){
		JSONToCSVConvertor(res,"Orders for:"+r,true);
	});
	}
	}
	);
}

function openCustomer()
{
	$('#dlgOrder').dialog('open').dialog('setTitle','Add Order');
	$('#frmOrder').form('clear');
	url='controller/orderController.php?action=addCustomer';
}
function saveCustomer(){
	saveForm("#frmOrder",url,"#dlgOrder","#dgOrder");
}
function viewSearchC(){
	var x =$('#startDate').datebox('getValue');
	var y =$('#endDate').datebox('getValue');
	$('#dgOrder').datagrid('load',{startDate:x,endDate:y})
}
