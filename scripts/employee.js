

 function newemployee(){
$('#dlgemployee').dialog('open').dialog('setTitle','Enter employee ');
$('#frmemployee').form('clear');
 url='controller/employeeController.php?action=add'; 
}

 function editemployee(){
 var row=$('#dgemployee').datagrid('getSelected');
 if(row)
{
 $('#dlgemployee').dialog('open').dialog('setTitle','Edit employee');
 $('#frmemployee').form('load',row); 
 url='controller/employeeController.php?action=edit&empNo='+row.empNo;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteemployee(){
 var row=$('#dgemployee').datagrid('getSelected');
 if(row)
{
$.post('controller/employeeController.php?action=delete&empNo='+row.empNo,{},function(data){ $('#dgemployee').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveemployee(){
 //saveForm('#frmemployee',url,'#dlgemployee','#dgemployee');
  //var formData=new FormData();
  var formdata=new FormData($("#frmemployee")[0]);
  //formdata.append('file',); //use get('files')[0]
  /*formdata.append('empNo',$('#empNo').val());//you can append it to formdata with a proper parameter name
  formdata.append('firstName',$('#firstName').val());
  formdata.append('lastName',$('#lastName').val());
  formdata.append('sex',$('#sex').val());
  formdata.append('martialStatus',$('#martialStatus').val());
  formdata.append('dob',$('#dob').val());
  formdata.append('town',$('#town').val());
  formdata.append('district',$('#district').val());
  formdata.append('nationID',$('#nationID').val());
  formdata.append('mobile1',$('#mobile1').val());
  formdata.append('mobile2',$('#mobile2').val());
  formdata.append('nikname',$('#nikname').val());
  formdata.append('contact',$('#contact').val());*/
  //formdata.append('photo',$('#photo').val());

  $.ajax({
   url:'controller/employeeController.php?action=add',
   cache : false,
   contentType : false,
   processData : false,
   data : formdata, //formdata will contain all the other details with a name given to parameters
   type : 'post',
   success: function(result) {
    $.messager.progress('close');
    var result = eval('(' + result + ')');
    if (result.msg) {
     $.messager.show({
      title: 'Warning',
      msg: result.msg
     });
    } else {
     $('#empNoGeneral').val($('#empNo').val());
     $.messager.show({
      title: 'Info',
      msg: 'successfully completed'
     });
     //$(dialogName).dialog('close');
     //$(datagridName).datagrid('reload'); // close the dialog
     // reload the user
    }
   }

  })
}
 function saveemployee2()
 {
  $("#frmemployee").submit(function() {

   var formData = new FormData($(this).serialize());

   /*$.post('controller/employeeController.php?action=add', formData, function(data) {
    alert(data);
   });*/
   $.ajax({
    url:'controller/employeeController.php?action=add',
    data:formData,
    processData:false,
    contentData:false

   }).done(function(param){
    //alert(param);
   });
   //return false;
  });
 }
 $(document).ready(function() {

  $('#wizard').smartWizard({onLeaveStep:leaveAStepCallback,onShowStep:showStepCallBack});

  $('#wizard_verticle').smartWizard({
   transitionEffect: 'slide'
  });

  $('.buttonNext').addClass('btn btn-success');
  $('.buttonPrevious').addClass('btn btn-primary');
  $('.buttonFinish').addClass('btn btn-default');
 });

function leaveAStepCallback(obj,context)
{
  leavingStep(context.fromStep);
  return validateStep(context.fromStep);
}
 function showStepCallBack(obj,context)
 {
  var stepNo=parseInt(context.toStep);
  switch (stepNo)
  {
   case 1:
    alert("showing step"+stepNo);
    //saveemployee();
    break;
   case 2:
    alert("showing step"+stepNo);
    prepare_employeeQualification();
    break;
   case 3:

    alert("showing step "+stepNo);
    prepare_employmentHistory();
    break;
   case 4:
    alert("showing step "+stepNo);
    prepare_employeeStatus();
    break;
   case 5:

    alert("showing step "+stepNo);
    prepare_employeeBank();
    break;
   case 6:
       prepare_employeeAllowance();
    alert("showing step "+stepNo);
    break;
  }
 }
 function leavingStep(stepNumber)
 {
   var stepNo=parseInt(stepNumber);
   switch (stepNo)
   {
    case 1:
       //alert(stepNo);
     saveemployee();
           break;
    case 2:
       //alert(stepNo);
           break;
    case 3:
       //alert(stepNo);
           break;
    case 4:
      //alert(stepNo);
           break;
    case 5:
      //alert(stepNo);
           break;
    case 6:
      //alert(stepNo);
           break;
   }

 }

 function validateStep(stepNumber)
 {
  var stepNo=parseInt(stepNumber);
  var val=null;
  var empNoGeneral=$('#empNoGeneral');
  var isValid=false;
  switch (stepNo)
  {
   case 1:
     val=empNoGeneral.val();
       if(val!=null&&val!="")
       {
        isValid=true;
       }
    break;
   case 2:
    val=empNoGeneral.val();
    if(val!=null&&val!="")
    {
     isValid=true;
    }
    break;
   case 3:
    val=empNoGeneral.val();
    if(val!=null&&val!="")
    {
     isValid=true;
    }
    break;
   case 4:
    val=empNoGeneral.val();
    if(val!=null&&val!="")
    {
     isValid=true;
    }
    break;
   case 5:
    val=empNoGeneral.val();
    if(val!=null&&val!="")
    {
     isValid=true;
    }
    break;
   case 6:
    val=empNoGeneral.val();
    if(val!=null&&val!="")
    {
     isValid=true;
    }
    break;
  }
  return isValid;
 }
 $(function()
     {
       var Id=qs('Id');
      if(Id!==undefined)
      {
       var filter=Id.split('#');

        //alert(Id);
       $('#empNoGeneral').val(filter[0]);
       $.post('controller/employeeController.php?action=getObject',{id:filter[0]},function(data){
        //alert(filter.length);
        var data2= $.parseJSON(data);
        $('#frmemployee').form('load',data2);
       })
      }
     }
 );

 function prepare_employeeQualification()
 {

  var id=$('#empNoGeneral').val();
  $('#dgemployeeQualification').edatagrid({
   url: 'controller/employeeQualificationController.php?action=view&empNo='+id,
   saveUrl: 'controller/employeeQualificationController.php?action=add&empNo='+id,
   updateUrl: 'controller/employeeQualificationController.php?action=update&empNo='+id,
   destroyUrl:'controller/employeeQualificationController.php?action=delete&empNo='+id,
   onSuccess:function()
   {
    $('#dgemployeeQualification').datagrid('reload');
   }
  });

 }

 function prepare_employmentHistory()
 {

  var id=$('#empNoGeneral').val();
  $('#dgemploymentHistory').edatagrid({
   url: 'controller/employmentHistoryController.php?action=view&empNo='+id,
   saveUrl: 'controller/employmentHistoryController.php?action=add&empNo='+id,
   updateUrl: 'controller/employmentHistoryController.php?action=update&empNo='+id,
   destroyUrl:'controller/employmentHistoryController.php?action=delete&empNo='+id,
   onSuccess:function()
   {
    $('#dgemploymentHistory').datagrid('reload');
   }
  });

 }


 function prepare_employeeStatus()
 {

  var id=$('#empNoGeneral').val();
  $('#dgemployeeStatus').edatagrid({
   url: 'controller/employeeStatusController.php?action=view&empNo='+id,
   saveUrl: 'controller/employeeStatusController.php?action=add&empNo='+id,
   updateUrl: 'controller/employeeStatusController.php?action=update&empNo='+id,
   destroyUrl:'controller/employeeStatusController.php?action=delete&empNo='+id,
   onSuccess:function()
   {
    $('#dgemployeeStatus').datagrid('reload');
   }
  });

 }

 function prepare_employeeBank()
 {

  var id=$('#empNoGeneral').val();
  $('#dgemployeeBank').edatagrid({
   url: 'controller/employeeBankController.php?action=view&empNo='+id,
   saveUrl: 'controller/employeeBankController.php?action=add&empNo='+id,
   updateUrl: 'controller/employeeBankController.php?action=update&empNo='+id,
   destroyUrl:'controller/employeeBankController.php?action=delete&empNo='+id,
   onSuccess:function()
   {
    $('#dgemployeeBank').datagrid('reload');
   }
  });

 }

 function prepare_employeeAllowance()
 {

  var id=$('#empNoGeneral').val();
  $('#dgemployeeAllowance').edatagrid({
   url: 'controller/employeeAllowanceController.php?action=view&empNo='+id,
   saveUrl: 'controller/employeeAllowanceController.php?action=add&empNo='+id,
   updateUrl: 'controller/employeeAllowanceController.php?action=update&empNo='+id,
   destroyUrl:'controller/employeeAllowanceController.php?action=delete&empNo='+id,
   onSuccess:function()
   {
    $('#dgemployeeAllowance').datagrid('reload');
   }
  });

 }
