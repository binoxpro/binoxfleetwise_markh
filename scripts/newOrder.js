
$(function(){
    
    prepare_datagrid();
});
 function newnewOrder(){
$('#dlgnewOrder').dialog('open').dialog('setTitle','Enter newOrder ');
$('#frmnewOrder').form('clear');
 url='controller/newOrderController.php?action=add'; 
}

 function editnewOrder(){
 var row=$('#dgnewOrder').datagrid('getSelected');
 if(row)
{
 $('#dlgnewOrder').dialog('open').dialog('setTitle','Edit newOrder');
 $('#frmnewOrder').form('load',row); 
 url='controller/newOrderController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletenewOrder(){
 var row=$('#dgnewOrder').datagrid('getSelected');
 if(row)
{
$.post('controller/newOrderController.php?action=delete&id='+row.id,{},function(data){ $('#dgnewOrder').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savenewOrder(){
 saveForm('#frmnewOrder',url,'#dlgnewOrder','#dgnewOrder');
}
function prepare_datagrid(){
			
			
		
			$('#dgnewOrder').edatagrid({
				url: 'controller/newOrderController.php?action=view',
				saveUrl: 'controller/newOrderController.php?action=add',
				updateUrl: 'controller/newOrderController.php?action=edit',
				destroyUrl:'controller/newOrderController.php?action=delete',
                onSuccess:function()
                {
                    $('#dgnewOrder').datagrid('reload');
                }
			});
			
		}
