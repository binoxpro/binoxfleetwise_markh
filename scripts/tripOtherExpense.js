

 function newTripOtherExpense(){
$('#dlgTripOtherExpense').dialog('open').dialog('setTitle','Enter tripOtherExpense ');
$('#frmTripOtherExpense').form('clear');
 url='controller/tripOtherExpenseController.php?action=add';
}

 function editTripOtherExpense(){
 var row=$('#dgTripOtherExpense').datagrid('getSelected');
 if(row)
{
 $('#dlgTripOtherExpense').dialog('open').dialog('setTitle','Edit tripOtherExpense');
 $('#frmTripOtherExpense').form('load',row); 
 url='controller/tripOtherExpenseController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteTripOtherExpense(){
 var row=$('#dgTripOtherExpense').datagrid('getSelected');
 if(row)
{
$.post('controller/tripOtherExpenseController.php?action=delete&id='+row.id,{},function(data){ $('#dgTripOtherExpense').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveTripOtherExpense(){
 saveForm('#frmTripOtherExpense',url,'#dlgTripOtherExpense','#dgTripOtherExpense');
}