// JavaScript Document
function loadGrid()
{
	$('#dgServiceManagement').datagrid('load',{});
}
function searchSeviceDetails(searchVal)
{
	$('#dgServiceManagement').datagrid('load',{numberPlate:searchVal});
}
function newServiceMaintance(){
		
		
			$('#dlgServiceManagement').dialog('open').dialog('setTitle','Service Management');
			
			$('#frmServiceMaintainance').form('clear');
			
			url='controller/serviceTrackController.php?action=addServiceMaintainance';
				
	}
	
	
	function editServiceMaintance(){
		var row=$('#dgServiceManagement').datagrid('getSelected');
		if(row){
			$('#dlgServiceManagement').dialog('open').dialog('setTitle','Edit service');
			$('#frmServiceMaintainance').form('load',row);
			url='controller/serviceTrackController.php?action=updateServiceMaintance&id='+row.serviceID;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile edit'});
		}
		
	}
	
	
		

		
	
	$(function(){
	$('#dgServiceManagement').datagrid({
	rowStyler:function(index,row){
		var nextR=parseInt(row.next_reading);
		var reminder=parseInt(row.Reminder);
		var current=parseInt(row.currentReading);
		//alert(nextR+"/"+current);
		var diff=nextR-current
		//alert(diff+":"+reminder);
		if((nextR-current)<=1000){
			return 'background-color:#9B0000;color:#FFF;';
		}else if((nextR-current)<=(reminder+500)&&(nextR-current)>(reminder)){
			return 'background-color:#FF702B; text-decoration:blink;';
		}
		
	}
	
	});
	});
	
	function nextReading(){
	var id=parseInt($('#km_readingx').numberbox('getValue'));
	var inter=parseInt($('#intervalx').val());
	var sumAns=id+inter;
	$('#next_readingx').numberbox('setValue',sumAns);
	
	
	}
function setPreviousReading(id)
{
	$.post('controller/odometerController.php?action=currentReading',{id:id},function(res){
	$('#km_readingx').numberbox('setValue',res);
		
		
	});
	
}
function saveServiceManagement()
{
		$.messager.progress();
	 $('#frmServiceMaintainance').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',msg: 'Service information add'});
						$('#dlgServiceManagement').dialog('close'); // close the dialog
						 $('#dgServiceManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

}
