

 function newworkOrder(){
$('#dlgworkOrder').dialog('open').dialog('setTitle','Enter workOrder ');
$('#frmworkOrder').form('clear');
 url='controller/workOrderController.php?action=add'; 
}

 function editworkOrder(){
 var row=$('#dgworkOrder').datagrid('getSelected');
 if(row)
{
 $('#dlgworkOrder').dialog('open').dialog('setTitle','Edit workOrder');
 $('#frmworkOrder').form('load',row); 
 url='controller/workOrderController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteworkOrder()
 {
 var row=$('#dgworkOrder').datagrid('getSelected');
   if(row)
  {
  $.post('controller/workOrderController.php?action=delete&id='+row.id,{},function(data){ $('#dgworkOrder').datagrid('reload');});
  }
   else
   {
   $.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
   }
}

 function saveworkOrder()
 {
 saveForm('#frmworkOrder',url,'#dlgworkOrder','#dgworkOrder');
}