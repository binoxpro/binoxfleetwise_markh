

 function newauditTrail(){
$('#dlgauditTrail').dialog('open').dialog('setTitle','Enter auditTrail ');
$('#frmauditTrail').form('clear');
 url='controller/auditTrailController.php?action=add'; 
}

 function editauditTrail(){
 var row=$('#dgauditTrail').datagrid('getSelected');
 if(row)
{
 $('#dlgauditTrail').dialog('open').dialog('setTitle','Edit auditTrail');
 $('#frmauditTrail').form('load',row); 
 url='controller/auditTrailController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteauditTrail(){
 var row=$('#dgauditTrail').datagrid('getSelected');
 if(row)
{
$.post('controller/auditTrailController.php?action=delete&id='+row.id,{},function(data){ $('#dgauditTrail').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveauditTrail(){
 saveForm('#frmauditTrail',url,'#dlgauditTrail','#dgauditTrail');
}