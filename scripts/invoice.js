
 function newinvoice(){
$('#dlginvoice').dialog('open').dialog('setTitle','Enter invoice ');
$('#frminvoice').form('clear');
 url='controller/invoiceController.php?action=add'; 
}

 function editinvoice(){
 var row=$('#dginvoice').datagrid('getSelected');
 if(row)
{
 $('#dlginvoice').dialog('open').dialog('setTitle','Edit invoice');
 $('#frminvoice').form('load',row); 
 url='controller/invoiceController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteinvoice(){
 var row=$('#dginvoice').datagrid('getSelected');
 if(row)
{
$.post('controller/invoiceController.php?action=delete&id='+row.id,{},function(data){ $('#dginvoice').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});
 }
}

 function saveinvoice(){
  //url='controller/invoiceController.php?action=add';
  //saveForm('#frminvoice',url,'#dlginvoice','#dginvoice');
  url='controller/invoiceController.php?action=add';
  var urlx="admin.php?view=manage_invoice";
  //saveForm('#frmpaymentVoucher',url,'#dlgpaymentVoucher','#dgpaymentVoucher');
  saveFormUrl2('#frminvoice',url,urlx);
}

 $(function(){
  var id=qs('id');
  if(id===undefined)
  {
   $.post('controller/invoiceController.php',{action:'getInvoiceNo'},function(data){
     $('#id').val(data);
    prepare_invoiceItem();
   })
  }else
  {
   $('#id').val(id);
   prepare_invoiceItem();
   getFormData(id)
   //get the invoice object

  }

 });

function getFormData(x)
{
  $.post('controller/invoiceController.php',{action:'getInvoiceObject',id:x},function(data)
  {

       var data2= $.parseJSON(data);
       $('#frminvoice').form('load',data2);
       getDebitorDetail(data2.customerId);
  });
}
 function prepare_invoiceItem()
 {

  var id=$('#id').val();
  $('#dginvoiceItem').edatagrid({
   url: 'controller/invoiceItemController.php?action=view&invoiceId='+id,
   saveUrl: 'controller/invoiceItemController.php?action=add&invoiceId='+id,
   updateUrl: 'controller/invoiceItemController.php?action=edit&invoiceId='+id,
   destroyUrl:'controller/invoiceItemController.php?action=delete&invoiceId='+id,
   onSuccess:function()
   {
     $('#dginvoiceItem').datagrid('reload');
   }
  });

 }
 function getDebitorDetail(x)
 {
  $.post('controller/debitorController.php',{action:'getDebitorDetail',id:x},function(data){
    $('#billTo').html(data);
  });
 }
 function attachWorkOrder()
 {
  $('#dlgworkOrderTrip').dialog('open').dialog('setTitle','Transport Claims ');
  $('#dgworkOrder').datagrid('reload');
  //$('#frminvoice').form('clear');
 }
 function checkBoxFormatter(val,row)
 {
  if(val==0)
  {
   return "<input type='checkbox'  value='1' readonly='readonly'  />";
  }else
  {
   return "<input type='checkbox' value='0' readonly='readonly' checked/>";
  }
 }
 function loadTemplateTo(){
  //$.messager.progress();
  var id="";
  var invoiceId=$('#id').val();
  //var class_code_value=$('#class_code2').val();
  var rows=$('#dgworkOrder').datagrid('getSelections');

  if(rows.length==0){
   //$.messager.progress('close');
   $.messager.show({title:'Help',msg:'Select at list one row'});
  }else{
   for(var i=0;i<rows.length;i++){
    if(i==0){
     id=rows[i].id;
    }else{
     id=id+"-"+(rows[i].id);
    }
   }
   $.post('controller/workOrderInvoiceController.php?workorderId='+id+'&invoiceId='+invoiceId,{action:'add'},function(data){
    alert(data);
    if(data=="ok"){
     //$.messager.progress('close');
     $.messager.alert('Info','Template loaded');
     //$('#dgActualExpense').datagrid('reload');

     $('#dlgworkOrderTrip').dialog('close');
     $('#dginvoiceItem').datagrid('reload');

    }else{
     //$.messager.progress('close');

     $('#dlgworkOrderTrip').dialog('close');
     $('#dginvoiceItem').datagrid('reload');
     $.messager.show({title:'Help',msg:'Not Loaded'});
    }
   });


  }
 }
 function searchRange()
 {
  var startDate=$('#startDate').datebox('getValue');
  var endDate=$('#endDate').datebox('getValue');
  $('#dgworkOrder').datagrid('load',{startDate:startDate,endDate:endDate});
 }

