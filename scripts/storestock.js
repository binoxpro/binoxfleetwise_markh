

 function newstorestock(){
$('#dlgstorestock').dialog('open').dialog('setTitle','Enter storestock ');
$('#frmstorestock').form('clear');
 url='controller/storestockController.php?action=add'; 
}

 function editstorestock(){
 var row=$('#dgstorestock').datagrid('getSelected');
 if(row)
{
 $('#dlgstorestock').dialog('open').dialog('setTitle','Edit storestock');
 $('#frmstorestock').form('load',row); 
 url='controller/storestockController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletestorestock(){
 var row=$('#dgstorestock').datagrid('getSelected');
 if(row)
{
$.post('controller/storestockController.php?action=delete&id='+row.id,{},function(data){ $('#dgstorestock').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savestorestock(){
 saveForm('#frmstorestock',url,'#dlgstorestock','#dgstorestock');
}
$(function(){

    $('#dgstorestock').datagrid({
        rowStyler: function (index, row) {
            if (((parseFloat(row.QuantityInStore)<=parseFloat(row.levelAlert)))) {
                return 'background-color:red;';
            }
        }
    });


})