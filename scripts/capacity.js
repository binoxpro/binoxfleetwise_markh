// JavaScript Document
function newCapacity()
{
	$('#dlgCapacity').dialog('open').dialog('setTitle','Enter Capacity Loaded');
	$('#frmCapacity').form('clear');
	newTrip();
	url='controller/capacityController.php?action=add';
	
}
function editCapacity()
{
	var row=$("#dgCapacity").datagrid("getSelected");
	if(row)
	{
		$('#dlgCapacity').dialog('open').dialog('setTitle','Edit Loading Information');
		$('#frmCapacity').form('load',row);
		url='controller/capacityController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a load to edit"});
		//alert('nnnn')
	}
}
function saveCapacity(){
	saveForm("#frmCapacity",url,"#dlgCapacity","#dgCapacity");
}
function newTrip(){
	$.post('controller/tripController.php',{action:'setupTrip'},function(data){
		$('#tripNo').val(data);
	})
}
function searchVehicle(x)
{
	$("#dgCapacity").datagrid("load",{id:x});
}
function searchTrip(){
	var regNo=$("#regNo").combobox('getValue');
	$.post('controller/tripController.php',{action:'searchTrip',id:regNo},function(data){
		$('#tripNo').val(data);
	})
}
