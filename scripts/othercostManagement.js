// JavaScript Document
function newOthercostManagement()
{
	$('#dlgOthercostManagement').dialog('open').dialog('setTitle','Add OthercostManagement');
	$('#frmOthercostManagement').form('clear');
	url='controller/othercostManagementController.php?action=add';
	
}
function editOthercostManagement()
{
	var row=$("#dgOthercostManagement").datagrid("getSelected");
	if(row)
	{
		$('#dlgOthercostManagement').dialog('open').dialog('setTitle','Editing OthercostManagement Details');
		$('#frmOthercostManagement').form('load',row);
		url='controller/othercostManagementController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a OthercostManagement to edit"});
		//alert('nnnn')
	}
}
function saveOthercostManagement()
{
	saveForm("#frmOthercostManagement",url,"#dlgOthercostManagement","#dgOthercostManagement");
}
function viewSearchC(){

	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#vehicleId').combobox('getValue');
	if(p1==null || p1==''){

		$('#dgOthercostManagement').datagrid('load',{startDate:d1,endDate:e1});
	}else if(d1==null||e1==null||d1==""||e1==""){
		$('#dgOthercostManagement').datagrid('load',{vehicleId:p1});
	}else{

		$('#dgOthercostManagement').datagrid('load',{startDate:d1,endDate:e1,vehicleId:p1});
	}
}