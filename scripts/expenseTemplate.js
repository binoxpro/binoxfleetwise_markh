

 function newexpenseTemplate(){
$('#dlgexpenseTemplate').dialog('open').dialog('setTitle','Enter expenseTemplate ');
$('#frmexpenseTemplate').form('clear');
 url='controller/expenseTemplateController.php?action=add'; 
}

 function editexpenseTemplate(){
 var row=$('#dgexpenseTemplate').datagrid('getSelected');
 if(row)
{
 $('#dlgexpenseTemplate').dialog('open').dialog('setTitle','Edit expenseTemplate');
 $('#frmexpenseTemplate').form('load',row); 
 url='controller/expenseTemplateController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteexpenseTemplate(){
 var row=$('#dgexpenseTemplate').datagrid('getSelected');
 if(row)
{
$.post('controller/expenseTemplateController.php?action=delete&id='+row.id,{},function(data){ $('#dgexpenseTemplate').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveexpenseTemplate(){
 saveForm('#frmexpenseTemplate',url,'#dlgexpenseTemplate','#dgexpenseTemplate');
}