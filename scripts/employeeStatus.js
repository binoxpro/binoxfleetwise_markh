

 function newemployeeStatus(){
$('#dlgemployeeStatus').dialog('open').dialog('setTitle','Enter employeeStatus ');
$('#frmemployeeStatus').form('clear');
 url='controller/employeeStatusController.php?action=add'; 
}

 function editemployeeStatus(){
 var row=$('#dgemployeeStatus').datagrid('getSelected');
 if(row)
{
 $('#dlgemployeeStatus').dialog('open').dialog('setTitle','Edit employeeStatus');
 $('#frmemployeeStatus').form('load',row); 
 url='controller/employeeStatusController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteemployeeStatus(){
 var row=$('#dgemployeeStatus').datagrid('getSelected');
 if(row)
{
$.post('controller/employeeStatusController.php?action=delete&id='+row.id,{},function(data){ $('#dgemployeeStatus').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveemployeeStatus(){
 saveForm('#frmemployeeStatus',url,'#dlgemployeeStatus','#dgemployeeStatus');
}