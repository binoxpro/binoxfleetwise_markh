

 function newvehicleConfiguration(){
$('#dlgvehicleConfiguration').dialog('open').dialog('setTitle','Enter vehicleConfiguration ');
$('#frmvehicleConfiguration').form('clear');
 url='controller/vehicleConfigurationController.php?action=add'; 
}

 function editvehicleConfiguration(){
 var row=$('#dgvehicleConfiguration').datagrid('getSelected');
 if(row)
{
 $('#dlgvehicleConfiguration').dialog('open').dialog('setTitle','Edit vehicleConfiguration');
 $('#frmvehicleConfiguration').form('load',row); 
 url='controller/vehicleConfigurationController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletevehicleConfiguration(){
 var row=$('#dgvehicleConfiguration').datagrid('getSelected');
 if(row)
{
$.post('controller/vehicleConfigurationController.php?action=delete&id='+row.id,{},function(data){ $('#dgvehicleConfiguration').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savevehicleConfiguration(){
 saveForm('#frmvehicleConfiguration',url,'#dlgvehicleConfiguration','#dgvehicleConfiguration');
}