// JavaScript Document
function newJobCard()
{
	$('#dlgJobCard').dialog('open').dialog('setTitle','Add JobCard');
	$('#frmJobCard').form('clear');
	url='controller/jobCardController.php?action=add';
}
function saveJobCard(){
	//
	saveForm("#frmJobCard",url,"#dlgJobCard","#dgJobCard");
}
function editJobCard(){
	var row=$("#dgJobCard").datagrid("getSelected");
	if(row){
		$("#dlgJobCard").dialog('open').dialog("setTitle","Edit JobCard");
	$("#frmJobCard").form('load',row);
	url="controller/jobCardController.php?action=edit&id="+row.id;
	}else{
		
	}
	
}
function deactiveJobCard(){
	var row=$("#dgJobCard").datagrid("getSelected");
	if(row){
		$.post('controller/jobCardController.php?action=delete',function(res){
			alert(res);
			
		});
	}else{
		
	} 


	
	}
	
	$(function(){
		$.post('controller/jobCardController.php',{action:'getAutoJobCardNumber'},function(data){
			$('#jobNo').val(data);
		});
		
		
		
	});