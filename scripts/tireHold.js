

 function newtireHold(){
$('#dlgtireHold').dialog('open').dialog('setTitle','Enter Mounting and Dismounting ');
$('#frmtireHold').form('clear');
 url='controller/tireHoldController.php?action=add'; 
 $('#tireId').combobox('reload','controller/tireController.php?action=view&combox2=Instock');
}
function getCurrentMileage(x,contentId){
	$.post('controller/tyreController.php?action=getMileage',{id:x},function(res){
		if(!isNaN(parseFloat(res))){
		$(contentId).numberbox('setValue',res);
		}else{
		$.messager.show({title:'System Error',msg:'Please Contact the System Administrator: '+res});	
		}
	});
}

function getCurrentTireOn(vehicleId,positionId){
	$.post('controller/tireHoldController.php?action=getTireHold',{vid:vehicleId,pid:positionId},function(res)
    {
        var str=res.split("*");
        $('#oldBrand').val(str[1]);
        $('#kmDone').val(str[0]);
        $('#oldTireId').val(str[2]);
	});
}

function getCurrentTireInfo(tireId){
	$.post('controller/tireController.php?action=getTire',{tireId:tireId},function(res)
    {
        var str=res.split("*");
        $('#oldBrand').val(str[1]);
        $('#kmDone').val(str[0]);
        $('#oldTireId').val(str[2]);
	});
}



 function edittireHold(){
 var row=$('#dgtireHold').datagrid('getSelected');
 if(row)
{
 $('#dlgtireHold').dialog('open').dialog('setTitle','Edit tireHold');
 $('#frmtireHold').form('load',row); 
 url='controller/tireHoldController.php?action=edit&id='+row.thid;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletetireHold(){
 var row=$('#dgtireHold').datagrid('getSelected');
 if(row)
{
$.post('controller/tireHoldController.php?action=delete&id='+row.thid,{},function(data){ $('#dgtireHold').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savetireHold()
 {
 saveForm('#frmtireHold',url,'#dlgtireHold','#dgtireHold');
 
 }

function setRemovalMileage(odmeterReading,expectedKm)
{
	var num1=parseFloat(odmeterReading);
	var num2=parseFloat(expectedKm);
	$('#removalKms').numberbox('setValue',(num2+num1));
}

function setBrand(id)
{
	$.post('controller/tireController.php?action=getStock',{id:id},function(res){
	var str=res.split('*');
	$('#cost').val(str[0]);
	$('#treadDepth').val(str[1]);
	$('#pressure').val(str[2]);
    $('#expectedKms').val(str[3]);
	
		
		
	});
	
}
$(function(){
	prepareGrid();
	$('#dgtireHold').datagrid({
	rowStyler:function(index,row){
		var removal=parseInt(row.removalKms);
		var current=parseInt(row.currentReading);
		var treadDepth=parseInt(row.treadDepthx);
        var treadWear=parseInt(row.treadWear);
        var pressure=parseInt(row.pressurex);
        var maxPressures=parseInt(row.maxmiumPressure);
        var remaining=10;
		//alert(nextR+"/"+current);
		if((current)>=(removal-1000)){
			
			return 'background-color:red;color:#FFF; font-weight:"bold"';
		}
        
        else if((treadDepth)<=3){
			
			return 'background-color:lime;color:black; font-weight:"bold"';
		}
        else if((treadWear)>=70){
			
			return 'background-color:yellow;color:black; font-weight:"bold"';
		}
        else if((maxPressures-pressure)>=40||(maxPressures-pressure)<0){
			
			return 'background-color:fuchsia;color:white; font-weight:"bold";';
		}
		
	}
	
	});
	});

 function prepareGrid()
 {

	 $('#dgtireHold').edatagrid({
		 url:'controller/tireHoldController.php?action=view2',
		 saveUrl:'controller/tireHoldController.php?action=extend',
		 updateUrl:'controller/tireHoldController.php?action=editExistion',
		 destroyUrl:'controller/tireHoldController.php?action=delete2',

		 onSuccess:function(index,row)
		 {
			 $('#dgtireHold').datagrid('reload');

		 }
	 });

 }