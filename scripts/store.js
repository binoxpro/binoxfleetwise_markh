

 function newstore(){
$('#dlgstore').dialog('open').dialog('setTitle','Enter store ');
$('#frmstore').form('clear');
 url='controller/storeController.php?action=add'; 
}

 function editstore(){
 var row=$('#dgstore').datagrid('getSelected');
 if(row)
{
 $('#dlgstore').dialog('open').dialog('setTitle','Edit store');
 $('#frmstore').form('load',row); 
 url='controller/storeController.php?action=edit&storeNo='+row.storeNo;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletestore(){
 var row=$('#dgstore').datagrid('getSelected');
 if(row)
{
$.post('controller/storeController.php?action=delete&storeNo='+row.storeNo,{},function(data){ $('#dgstore').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savestore(){
 saveForm('#frmstore',url,'#dlgstore','#dgstore');
}