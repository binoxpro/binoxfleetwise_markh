// JavaScript Document

function formatComplies(val,row)
{
	if(val=="Complies"){
		return "<span style='color:green;'>"+val+"</span>";
	}else{
		return "<span style='color:red;'>"+val+"</span>";
	}
}

function formatFitted(val,row)
{
	if(val=="Fitted"){
		return "<span style='color:green;'>"+val+"</span>";
	}else{
		return "<span style='color:red;'>"+val+"</span>";
	}
}

function formatInstalled(val,row)
{
	if(val=="Installed"){
		return "<span style='color:green;'>"+val+"</span>";
	}else{
		return "<span style='color:red;'>"+val+"</span>";
	}
}


function newStandard()
{
	
	url='controller/standardController.php?action=add';
	
}
function editStandard()
{
	var row=$("#dgStandard").datagrid("getSelected");
	if(row)
	{
		//$('#dlgVehicle').dialog('open').dialog('setTitle','Editing vehicle Details');
		$("#myModal").modal("show");
		$('#frmstandard').form('load',row);
		url='controller/standardController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Warning!',msg:"Please select a standard to edit"});
		//alert('nnnn')
	}
}
function saveStandard(){
	//saveForm("#frmVehicle",url,"#dlgVehicle","#dgVehicle");
	//url=url;
		$.messager.progress();
	 $("#frmstandard").form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						//$(dialogName).dialog('close');
						$("#myModal").modal("hide");
						$("#dgStandard").datagrid('reload'); 
		}
		
	}
}); 
		

}