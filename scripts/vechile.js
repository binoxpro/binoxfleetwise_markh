// JavaScript Document
function newVehicle()
{
	$('#dlgVehicle').dialog('open').dialog('setTitle','Add Vehicle');
	$('#frmVehicle').form('clear');
	url='controller/vehicleController.php?action=add';
	
}
function editVehicle()
{
	var row=$("#dgVehicle").datagrid("getSelected");
	if(row)
	{
		$('#dlgVehicle').dialog('open').dialog('setTitle','Editing vehicle Details');
		$('#frmVehicle').form('load',row);
		url='controller/vehicleController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a vehicle to edit"});
		//alert('nnnn')
	}
}
function saveVehicle(){
	saveForm("#frmVehicle",url,"#dlgVehicle","#dgVehicle");
}
function assignDriver()
{
	$('#dlgAssignVehicle').dialog('open').dialog('setTitle','Assign Driver');
	$('#frmAssign').form('clear');
	url='controller/vehicleController.php?action=assignVehicle';
	
}
function saveAssignVehicle(){
	saveForm("#frmAssign",url,"#dlgAssignVehicle","#dgVehicle");
}