

 function newposition(){
$('#dlgposition').dialog('open').dialog('setTitle','Enter position ');
$('#frmposition').form('clear');
 url='controller/positionController.php?action=add'; 
}

 function editposition(){
 var row=$('#dgposition').datagrid('getSelected');
 if(row)
{
 $('#dlgposition').dialog('open').dialog('setTitle','Edit position');
 $('#frmposition').form('load',row); 
 url='controller/positionController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteposition(){
 var row=$('#dgposition').datagrid('getSelected');
 if(row)
{
$.post('controller/positionController.php?action=delete&id='+row.id,{},function(data){ $('#dgposition').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveposition(){
 saveForm('#frmposition',url,'#dlgposition','#dgposition');
}