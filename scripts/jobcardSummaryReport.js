$(function(){
    $.post("reportController/jobcardSummaryReport.php",{},function(data){
        $('#reportViewer').html(data);
    })

})

function searchData(){
    var startDate=$('#startDate').datebox('getValue');
    var endDate=$('#endDate').datebox('getValue');
    var vehicleId=$('#vehicleId').combobox('getValue');
    var supplierId=$('#garage').combobox('getValue');
    if(startDate!="" && endDate!=""&&vehicleId!="" &&supplierId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', startDate:startDate,endDate:endDate,vehicleId:vehicleId,supplierId:supplierId}, function (data) {
            $('#reportViewer').html(data);
        });
    }
    else if(startDate!="" && endDate!=""&&vehicleId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', startDate:startDate,endDate:endDate,vehicleId:vehicleId}, function (data) {
            $('#reportViewer').html(data);
        });
    }else if(startDate!="" && endDate!=""&&supplierId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', startDate:startDate,endDate:endDate,supplierId:supplierId}, function (data) {
            $('#reportViewer').html(data);
        });
    }else if(startDate!="" && endDate!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', startDate:startDate,endDate:endDate}, function (data) {
            $('#reportViewer').html(data);
        });
    }else if(vehicleId!=""&&supplierId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', vehicleId:vehicleId,supplierId:supplierId}, function (data) {
            $('#reportViewer').html(data);
        });
    }else if(vehicleId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', vehicleId:vehicleId}, function (data) {
            $('#reportViewer').html(data);
        });
    }else if(supplierId!="")
    {
        $.post("reportController/jobcardSummaryReport.php", {action: 'yes', supplierId:supplierId}, function (data) {
            $('#reportViewer').html(data);
        });
    }else
    {
        $.post("reportController/jobcardSummaryReport.php",{},function(data){
            $('#reportViewer').html(data);
        })
    }

}

function kmsLess(){
    var x=$('#km').val();
    $.post("reportController/mountedTireReport.php",{kmsx:x},function(data){
        $('#reportViewer').html(data);
    })

}