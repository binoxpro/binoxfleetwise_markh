

 function newemploymentHistory(){
$('#dlgemploymentHistory').dialog('open').dialog('setTitle','Enter employmentHistory ');
$('#frmemploymentHistory').form('clear');
 url='controller/employmentHistoryController.php?action=add'; 
}

 function editemploymentHistory(){
 var row=$('#dgemploymentHistory').datagrid('getSelected');
 if(row)
{
 $('#dlgemploymentHistory').dialog('open').dialog('setTitle','Edit employmentHistory');
 $('#frmemploymentHistory').form('load',row); 
 url='controller/employmentHistoryController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteemploymentHistory(){
 var row=$('#dgemploymentHistory').datagrid('getSelected');
 if(row)
{
$.post('controller/employmentHistoryController.php?action=delete&id='+row.id,{},function(data){ $('#dgemploymentHistory').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveemploymentHistory(){
 saveForm('#frmemploymentHistory',url,'#dlgemploymentHistory','#dgemploymentHistory');
}