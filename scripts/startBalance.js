

 function newstartBalance(){
$('#dlgstartBalance').dialog('open').dialog('setTitle','Enter startBalance ');
$('#frmstartBalance').form('clear');
 url='controller/startBalanceController.php?action=add'; 
}

 function editstartBalance(){
 var row=$('#dgstartBalance').datagrid('getSelected');
 if(row)
{
 $('#dlgstartBalance').dialog('open').dialog('setTitle','Edit startBalance');
 $('#frmstartBalance').form('load',row); 
 url='controller/startBalanceController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletestartBalance(){
 var row=$('#dgstartBalance').datagrid('getSelected');
 if(row)
{
$.post('controller/startBalanceController.php?action=delete&id='+row.id,{},function(data){ $('#dgstartBalance').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savestartBalance(){
 saveForm('#frmstartBalance',url,'#dlgstartBalance','#dgstartBalance');
}