// JavaScript Document
//var util=new Array();
function newCapacity()
{
	$('#dlgCapacity').dialog('open').dialog('setTitle','Enter Capacity Loaded');
	$('#frmCapacity').form('clear');
	newTrip();
	url='controller/capacityController.php?action=add';
	
}
function viewSearchC(){
	
	var d1=$('#startDate').datebox('getValue');
	var e1=$('#endDate').datebox('getValue');
	var p1=$('#regNo').combobox('getValue');
	if(p1==null || p1==''){
		//alert("date Range");
		$.post('controller/capacityController.php',{action:'loadUtilisationRange',startDate:d1,endDate:e1},function(data){
		$('#reportViewer').html(data);
		})
	
	}else if(((d1==null || d1=='')&& (e1==null|| e1=='')) &&(p1!=null || p1!=''))
	{
		//alert("vechile only");
		$.post('controller/capacityController.php',{action:'loadUtilisationId',id:p1},function(data){
		$('#reportViewer').html(data);
		})
		
	}else if(((d1!=null || d1!='')&& (e1!=null|| e1!='')) &&(p1!=null || p1!='')){
		//alert("Not implemented");
		$.post('controller/capacityController.php',{action:'loadUtilisationIdRange',startDate:d1,endDate:e1,id:p1},function(data){
		$('#reportViewer').html(data);
		})
	}
}

function loadGraphy(){
	var ctx = document.getElementById("canvas").getContext("2d");
	 var myLine = new Chart(ctx);
	 
	//document.getElementById("canvas").innerHTML="";
	var p1=$('#regNo').combobox('getValue');
	var dataVal2;
	$.post('controller/capacityController.php',{action:'graphData',vehicleId:p1},function(dataVal){
		//alert(dataVal);
		var util=new Array();
		//alert(dataVal);
		var utilisation=dataVal.split('/');
		 for(var c=1;c<13;c++){
			 util[c-1]=utilisation[c];
		 }
		//alert(util.length)
		var lineChartData;
		 lineChartData = {
		labels : ["Jan","Feb","Mar","Apr","May","Jun","jul","Aug","Sept","Oct","Nov","Dec"],
		datasets : [
			{
				label:'Income',
				fillColor: "rgba(151,203,285,0.3)",
				strokeColor: "rgba(0,0,149,3)",
				pointColor: "rgba(0,0,149,3)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data:util
			}
		]

	}
	//myLineChart.removeData();
	 
	 //ctx.clearRect(0,0,500,250);
	
	 
	 //ctx.clearRect(0,0,500,250);
		myLine.Line(lineChartData,{
			responsive: true,scaleGridLineColor:"rgba(53,154,255,0.7)",scaleGridLineWidth:2,bezierCurve:false,animation: false });
	 
	});
	//myLine.update();
	$("#dlgUtilisationGraph").dialog('open').dialog('setTitle','Edit Loading Information');
}
function viewGraph()
{
	
	loadGraphy();
}
function editCapacity()
{
	var row=$("#dgCapacity").datagrid("getSelected");
	if(row)
	{
		$('#dlgCapacity').dialog('open').dialog('setTitle','Edit Loading Information');
		$('#frmCapacity').form('load',row);
		url='controller/capacityController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a load to edit"});
		//alert('nnnn')
	}
}
function saveCapacity(){
	saveForm("#frmCapacity",url,"#dlgCapacity","#dgCapacity");
}
function newTrip(){
	$.post('controller/tripController.php',{action:'setupTrip'},function(data){
		$('#tripNo').val(data);
	})
}
function searchVehicle(x)
{
	$("#dgCapacity").datagrid("load",{id:x});
}
function searchTrip(){
	var regNo=$("#regNo").combobox('getValue');
	$.post('controller/tripController.php',{action:'searchTrip',id:regNo},function(data){
		$('#tripNo').val(data);
	})
}

$(function(){
	$.post('controller/capacityController.php',{action:'timeUtilisation'},function(data){
		
		$('#reportViewer').html(data);
	})
});
