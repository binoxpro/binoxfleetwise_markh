

 function newprocesshistory(){
$('#dlgprocesshistory').dialog('open').dialog('setTitle','Enter processhistory ');
$('#frmprocesshistory').form('clear');
 url='controller/processhistoryController.php?action=add'; 
}

 function editprocesshistory(){
 var row=$('#dgprocesshistory').datagrid('getSelected');
 if(row)
{
 $('#dlgprocesshistory').dialog('open').dialog('setTitle','Edit processhistory');
 $('#frmprocesshistory').form('load',row); 
 url='controller/processhistoryController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteprocesshistory(){
 var row=$('#dgprocesshistory').datagrid('getSelected');
 if(row)
{
$.post('controller/processhistoryController.php?action=delete&id='+row.id,{},function(data){ $('#dgprocesshistory').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveprocesshistory(){
 saveForm('#frmprocesshistory',url,'#dlgprocesshistory','#dgprocesshistory');
}