// JavaScript Document
function addSectionItem()
{
	var row=$("#dgSectionItem").datagrid('getSelected');
	if(row){
		$('#dlgSectionItem').dialog('open').dialog('setTitle','Add SectionItem to '+row.type);
		//$('#SectionItemTab').tabs('select','SectionItem');
		$("#checkListTypeIdValue").val(row.id);
		loadCheckList(row.id);
	}else{
		$.messager.show({title:'Warning',msg:'Please select a check list type to manage'});
	}
}
function loadCheckList(id)
{
	$.post('controller/checklistTypeController.php?action=loadCheck&id='+id,{},function(data){
		$("#SectionItemView").html(data);
	})
}
function saveSectionItem(){
	//
	url="controller/";
	saveForm("#frmSectionItem",url,"#dlgSectionItem","#dgSectionItem");
}
function editSectionItem(){
	var row=$("#dgSectionItem").datagrid("getSelected");
	if(row){
		$("#dlgSectionItem").dialog('open').dialog("setTitle","Edit SectionItem");
	$("#frmSectionItem").form('load',row);
	
		url="controller/sectionItemController.php?action=edit&id="+row.id;
	}else{
			
	}
	
}
function deleteSectionItem(){
	var row=$("#dgSectionItem").datagrid("getSelected");
	if(row){
		$.post('controller/sectionItemController.php?action=delete',function(res){
			alert(res);
			
		});
	}else{
		
	}
}
	
function addSectionItemForm(){
	$('#dlgSectionItemAdd').dialog('open').dialog('setTitle','Add SectionItem to ');
	$("#checkListTypeId").val($("#checkListTypeIdValue").val());
	url="controller/sectionItemController.php?action=add";
		//$('#SectionItemTab').tabs('select','SectionItem');
}
function saveSectionItemForm(){
	//
	//saveForm("#frmSectionItemAdd",url,"#dlgSectionItemAdd","#dgSectionItem");
	$.messager.progress();
	 $("#frmSectionItemAdd").form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',msg: 'successfully completed'});
						$("#dlgSectionItemAdd").dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


	
}
/*
function editSectionItemItem(){
	$('#dlgSectionItemitem').dialog('open').dialog('setTitle','Add SectionItem item to');
	$("#checkListTypeId").val($("#checkListTypeIdValue").val());
	url="controller/SectionItemItemController.php?action=add";
		
}
*/
/*
function saveSectionItemItem(){
	//
	//saveForm("#frmSectionItemAdd",url,"#dlgSectionItemAdd","#dgSectionItem");
	$.messager.progress();
	 $("#frmSectionItemitem").form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title:'Warning',msg: result.msg});
					} else {
						$.messager.show({title: 'Info',msg: 'successfully completed'});
						$("#dlgSectionItemitem").dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 //reload the user
		}
		
	}
}); 
		


	
}*/