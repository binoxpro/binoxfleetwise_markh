

 function newledgerHead(){
$('#dlgledgerHead').dialog('open').dialog('setTitle','Enter ledgerHead ');
$('#frmledgerHead').form('clear');
 url='controller/ledgerHeadController.php?action=add'; 
}

 function editledgerHead(){
 var row=$('#dgledgerHead').datagrid('getSelected');
 if(row)
{
 $('#dlgledgerHead').dialog('open').dialog('setTitle','Edit ledgerHead');
 $('#frmledgerHead').form('load',row); 
 url='controller/ledgerHeadController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteledgerHead(){
 var row=$('#dgledgerHead').datagrid('getSelected');
 if(row)
{
$.post('controller/ledgerHeadController.php?action=delete&id='+row.id,{},function(data){ $('#dgledgerHead').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveledgerHead(){
 saveForm('#frmledgerHead',url,'#dlgledgerHead','#dgledgerHead');
}