

 function newitemcategory(){
$('#dlgitemcategory').dialog('open').dialog('setTitle','Enter itemcategory ');
$('#frmitemcategory').form('clear');
 url='controller/itemcategoryController.php?action=add'; 
}

 function edititemcategory(){
 var row=$('#dgitemcategory').datagrid('getSelected');
 if(row)
{
 $('#dlgitemcategory').dialog('open').dialog('setTitle','Edit itemcategory');
 $('#frmitemcategory').form('load',row); 
 url='controller/itemcategoryController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteitemcategory(){
 var row=$('#dgitemcategory').datagrid('getSelected');
 if(row)
{
$.post('controller/itemcategoryController.php?action=delete&id='+row.id,{},function(data){ $('#dgitemcategory').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveitemcategory(){
 saveForm('#frmitemcategory',url,'#dlgitemcategory','#dgitemcategory');
}