

 function newitem(){
$('#dlgitem').dialog('open').dialog('setTitle','Enter item ');
$('#frmitem').form('clear');
 url='controller/itemController.php?action=add'; 
}

 function edititem(){
 var row=$('#dgitem').datagrid('getSelected');
 if(row)
{
 $('#dlgitem').dialog('open').dialog('setTitle','Edit item');
 $('#frmitem').form('load',row); 
 url='controller/itemController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deleteitem(){
 var row=$('#dgitem').datagrid('getSelected');
 if(row)
{
$.post('controller/itemController.php?action=delete&id='+row.id,{},function(data){ $('#dgitem').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function saveitem(){
 saveForm('#frmitem',url,'#dlgitem','#dgitem');
}