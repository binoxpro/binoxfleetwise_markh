

 function newvehicleSystem(){
$('#dlgvehicleSystem').dialog('open').dialog('setTitle','Enter vehicleSystem ');
$('#frmvehicleSystem').form('clear');
 url='controller/vehicleSystemController.php?action=add'; 
}

 function editvehicleSystem(){
 var row=$('#dgvehicleSystem').datagrid('getSelected');
 if(row)
{
 $('#dlgvehicleSystem').dialog('open').dialog('setTitle','Edit vehicleSystem');
 $('#frmvehicleSystem').form('load',row); 
 url='controller/vehicleSystemController.php?action=edit&id='+row.id;
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function deletevehicleSystem(){
 var row=$('#dgvehicleSystem').datagrid('getSelected');
 if(row)
{
$.post('controller/vehicleSystemController.php?action=delete&id='+row.id,{},function(data){ $('#dgvehicleSystem').datagrid('reload');});
}
 else{
$.messager.show({title:'Warning!',msg:'Please select a item to to edit'});} 
}

 function savevehicleSystem(){
 saveForm('#frmvehicleSystem',url,'#dlgvehicleSystem','#dgvehicleSystem');
}