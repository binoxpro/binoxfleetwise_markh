// JavaScript Document
function newOtherCost()
{
	$('#dlgOtherCost').dialog('open').dialog('setTitle','Add Other Cost');
	$('#frmOtherCost').form('clear');
	url='controller/otherCostController.php?action=add';
	
}
function editOtherCost()
{
	var row=$("#dgOtherCost").datagrid("getSelected");
	if(row)
	{
		$('#dlgOtherCost').dialog('open').dialog('setTitle','Editing OtherCost Details');
		$('#frmOtherCost').form('load',row);
		url='controller/otherCostController.php?action=edit&id='+row.id;
	}
	else{
		$.messager.show({title:'Oooops!',msg:"Please select a OtherCost to edit"});
		//alert('nnnn')
	}
}
function saveOtherCost(){
	saveForm("#frmOtherCost",url,"#dlgOtherCost","#dgOtherCost");
}