<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/paymentVoucherService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../controller/utility.php');
//require_once('../reportPluggin/intial.php');
try{
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    $totalAmount=0;

    $tt=new TableTemplate();
    $tb=new myTable($tt);

    $array=array("Particular","Amount","Paid To","Cost Center");
    $tb->setCaption("<h3 align='center'>Payment Voucher</h3>");
    $tb->setTableHeader($array);


    $sql="";
    $paymentVoucher=new paymentVoucherService();
    $paymentVoucher->setid($_REQUEST['id']);
    $paymentVoucher->find();
    $paymentVoucherItem=new paymentVoucherItemService();
    $paymentVoucherItem->setpayVoucherId($_REQUEST['id']);
    //$paymentVoucherItem->
    //$_SESSION['sqlValue']="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";

    $sql="select pvi.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pvi.accountCode) AccountName,(select ia.AccountName from  individualaccounts ia  where ia.Id=pvi.individualCode) Individual,(select ia.AccountName from  individualaccounts ia  where ia.Id=pvi.costCenter) costCenterName  from  paymentvoucheritem pvi where pvi.payVoucherId='".$paymentVoucherItem->getpayVoucherId()."'";
    //building my table
    $i=0;
    foreach($paymentVoucherItem->view_query($sql) as $row)
    {


        $tb->setTableData($row['particular'],0,sizeof($array));
        $tb->setTableData(number_format($row['amount'],2),1,sizeof($array));
        $tb->setTableData($row['Individual'],2,sizeof($array));
        $tb->setTableData($row['costCenterName'],3,sizeof($array));
        $totalAmount=$totalAmount+$row['amount'];
        $i=$i+1;

    }

    for($count=0;$count<(15-$i);$count++)
    {
        $tb->setTableData("&nbsp;",0,sizeof($array));
        $tb->setTableData("&nbsp;",1,sizeof($array));
        $tb->setTableData("&nbsp;",2,sizeof($array));
        $tb->setTableData("&nbsp;",3,sizeof($array));
    }

    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));

    $tb->setTableFooter(number_format($totalAmount,2),1,sizeof($array));
    $tb->setTableFooter("",2,sizeof($array));
    $tb->setTableFooter("",3,sizeof($array));
    // 
    //CREATE THE CLOSING FOOTER
    $str= "<table width='50%' style='font-size: 12pt; border-collapse: collapse; ' cellpadding='8'><tr ><td class='total'>Serial No.</td><td class='total' style='color: #d95450; font-weight: bolder;'>" .utility::returnThreeFiguareString($_REQUEST['id'])."</td><td class='total'>Date</td><td>".$paymentVoucher->gettransactionDate()."</td></tr></table><p style='height:20px;'></p>";
    $tb->setCaption($str);

    //create a close note
    $tb->setNotes("<div style='padding-top:5px;'><b>Amount in figures:".ucfirst(utility::convert_number_to_words($totalAmount))." Shilings</b></div><p style='padding-top:5px;'>Prepared by:".$paymentVoucher->getpreparedBy()."&nbsp;&nbsp;&nbsp;&nbsp;Received By:................................<br/>Authorised By:...........................");


    $rp=new Report($tb);
    $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
    include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();




}catch(Exception $exc)
{

}
//get the table;


