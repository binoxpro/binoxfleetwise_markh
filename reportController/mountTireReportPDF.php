<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/tireService.php');
require_once('../services/tireHoldService.php');
//require_once('../reportPluggin/intial.php');
    try{
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    
    $tt=new TableTemplate();
    $tb=new myTable($tt);
    
    $array=array("Truck No","Position","Brand","S.No","Fitting Date","Fitting OBC","Expected kms","Removal Kms","Current OBC","KMS Done","Remaining Kms","TreadDepth","TreadWear%","Pressure","Cost","CPK","Current Cost");
    $tb->setCaption("<h4 align='center'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 align='center'>Mounted Tire Report</h4>");
    $tb->setTableHeader($array);
    
    $tire=new tireService();
    $sql="";
    //$_SESSION['sqlValue']="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
      
    if(isset($_SESSION['sqlValue']))
    {
        $sql=$_SESSION['sqlValue'];
    }
    //building my table
    foreach($tire->view_query($sql) as $row)
    {
        $currentCost=$currentCost+(($row['cost']/$row['expectedKms'])*$row['kmsDone']);
        $purchase=$purchase+$row['cost'];
        $remaining=$row['actualKms']-$row['currentReading'];
        $i=0;
        if($remaining<1000){
           $tb->setTableData2($row['regNo'],0,sizeof($array),'color:red'); 
           $warningTire=$warningTire+1;
        }else if(floatval($row['treadDepthx'])<4 && !is_null($row['treadDepthx']) )
        {
            
            $tb->setTableData2($row['regNo'],0,sizeof($array),'color:green'); 
             $warningTire=$warningTire+1;
        }else{
            $tb->setTableData($row['regNo'],0,sizeof($array));
            $okTires=$okTires+1;
        }
        
        $tb->setTableData($row['tyretypeName'],1,sizeof($array));
        $tb->setTableData($row['tireBand'],2,sizeof($array));
        $tb->setTableData($row['serialNumber'],3,sizeof($array));
        $tb->setTableData($row['fixingDate'],4,sizeof($array));
        $tb->setTableData($row['fixingOdometer'],5,sizeof($array));
        $tb->setTableData($row['expectedKms'],6,sizeof($array));
        $tb->setTableData($row['actualKms'],7,sizeof($array));
        $tb->setTableData($row['currentReading'],8,sizeof($array));
        $tb->setTableData($row['kmsDone'],9,sizeof($array));
        $tb->setTableData($row['actualKms']-$row['currentReading'],10,sizeof($array));
        $tb->setTableData($row['treadDepthx'],11,sizeof($array));
        $tb->setTableData($row['treadWear'],12,sizeof($array));
        $tb->setTableData($row['pressurex'],13,sizeof($array));
        $tb->setTableData(number_format($row['cost'],0),14,sizeof($array));
        $tb->setTableData(number_format( round($row['cost']/$row['expectedKms'],2),2),15,sizeof($array));
        $tb->setTableData(number_format(round(($row['cost']/$row['expectedKms'])*$row['kmsDone'],2),2),16,sizeof($array));
       
        
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));
     for($i=1;$i<sizeof($array)-3;$i++)
     {
        $tb->setTableFooter("",$i,sizeof($array));
     }
    $tb->setTableFooter(number_format($purchase,0),14,sizeof($array));
    $tb->setTableFooter("",15,sizeof($array));
    $tb->setTableFooter(number_format(round($currentCost,0),0),16,sizeof($array));
    // 
    $wt=0;
    $gt=0;
    if($warningTire>0){
    $wt=round(($warningTire/($warningTire+$okTires))*100,2);
    }
    if($okTires>0)
    {
        $gt=round(($okTires/($warningTire+$okTires))*100,2);
    }
    $str="<table class='items' width='100%' style='font-size: 9pt; border-collapse: collapse; ' cellpadding='8'><thead><tr ><td class='total'>Description</td><td class='total'>Value</td><td class='total'>%</td></tr></thead><tbody><tr class='total'><td>Warning Tires</td><td>".$warningTire."</td><td>".$wt."%</td></tr><tr><td>Good Condition Tire</td><td>".$okTires."</td><td>".$gt."%</td></tr></tbody></table><p style='height:20px;'></p>";
    $tb->setCaption($str);
       
    $rp=new Report($tb);
    $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    
    
    
    
    }catch(Exception $exc)
    {
        
    }
//get the table;


