<?php
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/tireService.php');
    require_once('../services/tireHoldService.php');
    require_once('../reportPluggin/intial.php');
    
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    $tireband=new tireService();
    $sqlOuter="Select * from tbltireband";
    
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    $array=array("Brand","Quantity","Total/Km","CPM","Average CPM","Average Km","1 KM","1000 KM");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Tire Brand Summary Report</h4>");
    $tb->setTableHeader($array);
    
    foreach($tireband->view_query($sqlOuter) as $row){
        $brand=$row['brand'];
        
        $sqlOuter2="Select count(id) noId from tbltire where tireBand='$brand'";
         foreach($tireband->view_query($sqlOuter2) as $row2)
         {
            $tb->setTableData($brand,0,sizeof($array));
            $tb->setTableData($row2['noId'],1,sizeof($array));
            //echo $brand."&nbsp;".$row2['noId']."&nbsp;&nbsp;";
         }
        // $sqlOuter3="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and t.tireBand='$brand' order by th.vehicleId,tt.id";      
        
        $sqlOuter3="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and t.tireBand='$brand' order by th.vehicleId,tt.id";      
        $kmsDone=0;
        $cpk=0;
        $i=0;
        foreach($tireband->view_query($sqlOuter3) as $row3)
         {
            //echo $row2['noId']."<br/>";
            $kmsDone=$kmsDone+$row3['kmsDone'];
            $i=$i+1;
            $cpk=$cpk+(($row3['cost']/$row3['expectedKms'])*$row3['kmsDone']);
            
         }
         if($cpk!=0){
        $tb->setTableData(number_format($kmsDone,2),2,sizeof($array));
        $tb->setTableData(number_format(round($cpk,2),2),3,sizeof($array));
        //$tb->setTableData($kmsDone,4,sizeof($array));
        $tb->setTableData(number_format(round(($cpk/$i),2),2),4,sizeof($array));
        $tb->setTableData(number_format(round(($kmsDone/$i),2),2),5,sizeof($array));
		$tb->setTableData(number_format(round(($cpk/$i)/($kmsDone/$i),2),2),6,sizeof($array));
        
        $tb->setTableData(number_format(round(((($cpk/$i)/($kmsDone/$i))*1000),2),2),6,sizeof($array));
        }else
        {
             $tb->setTableData(number_format($kmsDone,2),2,sizeof($array));
        $tb->setTableData(number_format(round($cpk,2),2),3,sizeof($array));
        //$tb->setTableData($kmsDone,4,sizeof($array));
        $tb->setTableData(number_format(round((0),2),2),4,sizeof($array));
        $tb->setTableData(number_format(round((0),2),2),5,sizeof($array));
		$tb->setTableData(number_format(round(0,2),2),6,sizeof($array));
        
        $tb->setTableData(number_format(round((((0))),2),2),6,sizeof($array));
            
        }
        
        //echo $kmsDone."&nbsp;&nbsp;".$cpk."&nbsp;&nbsp;".($cpk/$i)."&nbsp;&nbsp;".($kmsDone/$i)."<br/>";
         
    }
    $rb=new ReportBuilder($tb);
   
   echo $rb->getReport();
    
    
    //
//    
//    $array=array("Truck No","Position","Brand","S.No","Fitting Date","Fitting OBC","Expected kms","Removal Kms","Current OBC","KMS Done","Remaining Kms","TreadDepth","TreadWear%","Pressure","Cost","CPK","Current Cost");
//    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>FST Transporter Limited</h4><h4 class='text-center'>Mounted Tire Report</h4>");
//    $tb->setTableHeader($array);
//    
//    $tire=new tireService();
//    $sql="";
//    if(isset($_REQUEST['action']))
//    {
//        $id=$_REQUEST['id'];
//      $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and th.vehicleId='$id' order by th.vehicleId,tt.id";
//      
//        
//    }else{
//      $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
//      
//    }
//   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
//    foreach($tire->view_query($sql) as $row)
//    {
//        $currentCost=$currentCost+(($row['cost']/$row['expectedKms'])*$row['kmsDone']);
//        $purchase=$purchase+$row['cost'];
//        $remaining=$row['actualKms']-$row['currentReading'];
//        $i=0;
//        if($remaining<1000){
//           $tb->setTableData2($row['regNo'],0,sizeof($array),'color:red'); 
//           $warningTire=$warningTire+1;
//        }else if(floatval($row['treadDepthx'])<4 && !is_null($row['treadDepthx']) )
//        {
//            
//            $tb->setTableData2($row['regNo'],0,sizeof($array),'color:green'); 
//             $warningTire=$warningTire+1;
//        }else{
//            $tb->setTableData($row['regNo'],0,sizeof($array));
//            $okTires=$okTires+1;
//        }
//        
//        $tb->setTableData($row['tyretypeName'],1,sizeof($array));
//        $tb->setTableData($row['tireBand'],2,sizeof($array));
//        $tb->setTableData($row['serialNumber'],3,sizeof($array));
//        $tb->setTableData($row['fixingDate'],4,sizeof($array));
//        $tb->setTableData($row['fixingOdometer'],5,sizeof($array));
//        $tb->setTableData($row['expectedKms'],6,sizeof($array));
//        $tb->setTableData($row['actualKms'],7,sizeof($array));
//        $tb->setTableData($row['currentReading'],8,sizeof($array));
//        $tb->setTableData($row['kmsDone'],9,sizeof($array));
//        $tb->setTableData($row['actualKms']-$row['currentReading'],10,sizeof($array));
//        $tb->setTableData($row['treadDepthx'],11,sizeof($array));
//        $tb->setTableData($row['treadWear'],12,sizeof($array));
//        $tb->setTableData($row['pressurex'],13,sizeof($array));
//        $tb->setTableData(number_format($row['cost'],0),14,sizeof($array));
//        $tb->setTableData(number_format( round($row['cost']/$row['expectedKms'],2),2),15,sizeof($array));
//        $tb->setTableData(number_format(round(($row['cost']/$row['expectedKms'])*$row['kmsDone'],2),2),16,sizeof($array));
//       
//        
//    }
//    //$tire
//    $tb->setTableFooter("Total",0,sizeof($array));
//     for($i=1;$i<sizeof($array)-3;$i++)
//     {
//        $tb->setTableFooter("",$i,sizeof($array));
//     }
//    $tb->setTableFooter(number_format($purchase,0),14,sizeof($array));
//    $tb->setTableFooter("",15,sizeof($array));
//    $tb->setTableFooter(number_format(round($currentCost,0),0),16,sizeof($array));
//    // 
//    $wt=0;
//    $gt=0;
//    if($warningTire>0){
//    $wt=round(($warningTire/($warningTire+$okTires))*100,2);
//    }
//    if($okTires>0)
//    {
//        $gt=round(($okTires/($warningTire+$okTires))*100,2);
//    }
//    $str="<table class='table table-bordered'><tr><th>Description</th><th>Value</th><th></th></tr><tr class='danger'><th>Warning Tires</th><th>".$warningTire."</th><th>".$wt."%</th></tr><tr class='success'><th>Good Condition Tire</th><th>".$okTires."</th><th>".$gt."%</th></tr></table>";
//    $tb->setCaption($str);
//       
//    $rb=new ReportBuilder($tb);
//    
//    echo $rb->getReport();
    


?>