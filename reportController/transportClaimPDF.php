<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/workOrderInvoiceService.php');
require_once('../services/taxInvoiceService.php');

    $totalQty=0;
    $totalBags=0;
    $totalAmount=0;



    $tt=new TableTemplate();
    $tb=new myTable($tt);

    //$metre
   // "<table class='items' width='100%' style='font-size: 9pt; border-collapse: collapse; ' cellpadding='8'><tr><th>REG No.</th><th>".$row2['regNo']."</th><th>Model.</th><th>".$row2['model']."</th></tr>";
    //"<tr><th>AXEL</th><th>".$row2['axel']."</th><th>Odometer</th><th>".$row2['meter']."</th></tr></table>";
    
    
    
    
    
    $array=array("Date","Veh.No","Delivery No","PO No","Product","Quantity Tons","Bags","Rate","Amount","Destination");
    $tb->setCaption("<h4>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Transport Claims</h4>");
    $tb->setTableHeader($array);
    $id="";
    $workOrderInvoice=new workOrderInvoiceService();
    if(isset($_REQUEST['confirmed']))
    {
        if($_REQUEST['confirmed']==='0')
        {
            $id=$_REQUEST['id'];

        }else if($_REQUEST['confirmed']==='1')
        {
            $taxInvoice=new taxInvoiceService();
            $taxInvoice->setid($_REQUEST['id']);
            $taxInvoice->find();
            $id=$taxInvoice->getparentId();
        }

    }else{
        $id=$_REQUEST['id'];
    }


    $sql="Select wo.CreationDate,v.regNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,(select cust.destination from tbl_customer cust where cust.customercode=wo.CustomerId) destination,(select cust.customerName from tbl_customer cust where cust.customercode=wo.CustomerId) town from workorderinvoice woi inner join workorder wo on woi.workorderId=wo.id inner join  tripnumbergenerator tng on wo.tripNo=tng.TripNumber inner join tblvehicle v ON tng.VehicleId=v.id where woi.invoiceId='$id'";
        
    //"select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($workOrderInvoice->view_query($sql) as $row)
    {

        $totalBags=$totalBags+$row['Bags'];
        $totalQty=$totalQty+$row['Quantity'];
        $totalAmount=$totalAmount+$row['Amount'];

        $tb->setTableData($row['CreationDate'],0,sizeof($array));
        $tb->setTableData($row['regNo'],1,sizeof($array));
        $tb->setTableData($row['DeliveryNo'],2,sizeof($array));
        $tb->setTableData($row['PONo'],3,sizeof($array));
        $tb->setTableData($row['product'],4,sizeof($array));
        $tb->setTableData(number_format($row['Quantity']),5,sizeof($array));
        $tb->setTableData(number_format($row['Bags']),6,sizeof($array));
        $tb->setTableData(number_format($row['Rate']),7,sizeof($array));
        $tb->setTableData(number_format($row['Amount']),8,sizeof($array));
        $tb->setTableData($row['destination'],9,sizeof($array));


        
    }
    //$tire
    $tb->setTableFooter("",0,sizeof($array));
    $tb->setTableFooter("",1,sizeof($array));
    $tb->setTableFooter("",2,sizeof($array));
    $tb->setTableFooter("",3,sizeof($array));
    $tb->setTableFooter("SubTotal",4,sizeof($array));
    $tb->setTableFooter(number_format($totalQty),5,sizeof($array));
    $tb->setTableFooter(number_format($totalBags),6,sizeof($array));
    $tb->setTableFooter("",7,sizeof($array));
    $tb->setTableFooter(number_format($totalAmount),8,sizeof($array));
    $tb->setTableFooter("",9,sizeof($array));

if(intval($_REQUEST['vat'])>0)
{
    $tb->setTableFooter("",0,sizeof($array));
    $tb->setTableFooter("",1,sizeof($array));
    $tb->setTableFooter("",2,sizeof($array));
    $tb->setTableFooter("",3,sizeof($array));
    $tb->setTableFooter("VAT 18%",4,sizeof($array));
    $tb->setTableFooter("",5,sizeof($array));
    $tb->setTableFooter("",6,sizeof($array));
    $tb->setTableFooter("",7,sizeof($array));
    $tb->setTableFooter(number_format(0.18*$totalAmount),8,sizeof($array));
    $tb->setTableFooter("",9,sizeof($array));


    $tb->setTableFooter("",0,sizeof($array));
    $tb->setTableFooter("",1,sizeof($array));
    $tb->setTableFooter("",2,sizeof($array));
    $tb->setTableFooter("",3,sizeof($array));
    $tb->setTableFooter("Grand Total",4,sizeof($array));
    $tb->setTableFooter("",5,sizeof($array));
    $tb->setTableFooter("",6,sizeof($array));
    $tb->setTableFooter("",7,sizeof($array));
    $tb->setTableFooter(number_format($totalAmount+(0.18*$totalAmount),2),8,sizeof($array));
    $tb->setTableFooter("",9,sizeof($array));


}


    $rp=new Report($tb);
    $htmlx=$rp->getReport();
    
     define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    

