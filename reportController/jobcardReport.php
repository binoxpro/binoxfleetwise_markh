<?php
    session_start();
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/jobCardService.php');
    //('../services/tireHoldService.php');
    require_once('../reportPluggin/intial.php');
    $sparepartCost=0;
    $labourCost=0;
    $totalCost=0;
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Date","Serial No","Registration Number","Trailer No","Driver Name","Spare Parts Cost","Labour Cost","Total Cost");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>JOBCARD SUMMARY REPORT</h4>");
    $tb->setTableHeader($array);
    
    $jobCard=new jobCardService();
    $sql="";
    if(isset($_REQUEST['action']))
    {
       if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];
           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.vehicleId='$vehicleId' and  jc.creationDate BETWEEN '$startDate' and '$endDate' ";

       }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];

           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where  jc.creationDate BETWEEN '$startDate' and '$endDate' ";


       }else if(isset($_REQUEST['vehicleId']))
       {

           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.vehicleId='$vehicleId'  ";

       }
        //$sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id ";


    }else if(isset($_REQUEST['kmsx']))
	{
		$kms=$_REQUEST['kmsx'];
		$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and (th.actualKms-od.reading)<'$kms' order by th.vehicleId,tt.id";
	}else{
      $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id ";
      
    }
    $_SESSION['sqlValue']=$sql;
    session_write_close();
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($jobCard->view_query($sql) as $row)
    {
        if(!is_null($row['SparePartCost'])) {
            $sparepartCost = $sparepartCost + $row['SparePartCost'];
            $row['SparePartCost']=$row['SparePartCost'];
        }else{
            $sparepartCost = $sparepartCost + 0;
            $row['SparePartCost']=0;
        }
        if(!is_null($row['labourCost']))
        {
            $labourCost=$labourCost+$row['labourCost'];
            $row['labourCost']=$row['labourCost'];
        }else {
            $labourCost = $labourCost + $row['labourCost'];
            $row['labourCost']=0;
        }

        $totalCost=$totalCost+($row['labourCost']+$row['SparePartCost']);

        
        $tb->setTableData($row['creationDate'],0,sizeof($array));
        $tb->setTableData($row['jobNo'],1,sizeof($array));
        $tb->setTableData($row['regNo'],2,sizeof($array));
        $tb->setTableData("",3,sizeof($array));
        $tb->setTableData("",4,sizeof($array));
        $tb->setTableData(number_format($row['SparePartCost'],0),5,sizeof($array));
        $tb->setTableData(number_format($row['labourCost'],0),6,sizeof($array));
        $tb->setTableData(number_format(($row['SparePartCost']+$row['labourCost']),0),7,sizeof($array));

        
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));
     for($i=1;$i<5;$i++)
     {
        $tb->setTableFooter("",$i,sizeof($array));
     }
    $tb->setTableFooter(number_format($sparepartCost,0),4,sizeof($array));
    $tb->setTableFooter(number_format($labourCost,0),6,sizeof($array));
    $tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));

    $str="";
    $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>