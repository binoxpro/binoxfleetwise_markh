<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/jobCardService.php');
//require_once('../services/tireHoldService.php');
//require_once('../reportPluggin/intial.php');
    try{
        $sparepartCost=0;
        $labourCost=0;
        $totalCost=0;
    
    $tt=new TableTemplate();
    $tb=new myTable($tt);

        $array=array("Date","Serial No","Registration Number","Trailer No","Driver Name","Spare Parts Cost","Labour Cost","Total Cost");
        $tb->setCaption("<h4 align='center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED </h4><h4 align='center'>JOBCARD SUMMARY REPORT</h4>");
        $tb->setTableHeader($array);

        $jobCard=new jobCardService();
    $sql="";
    //$_SESSION['sqlValue']="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
      
    if(isset($_SESSION['sqlValue']))
    {
        $sql=$_SESSION['sqlValue'];
    }
    //building my table
        foreach($jobCard->view_query($sql) as $row)
        {
            if(!is_null($row['SparePartCost'])) {
                $sparepartCost = $sparepartCost + $row['SparePartCost'];
                $row['SparePartCost']=$row['SparePartCost'];
            }else{
                $sparepartCost = $sparepartCost + 0;
                $row['SparePartCost']=0;
            }
            if(!is_null($row['labourCost']))
            {
                $labourCost=$labourCost+$row['labourCost'];
                $row['labourCost']=$row['labourCost'];
            }else {
                $labourCost = $labourCost + $row['labourCost'];
                $row['labourCost']=0;
            }

            $totalCost=$totalCost+($row['labourCost']+$row['SparePartCost']);


            $tb->setTableData($row['creationDate'],0,sizeof($array));
            $tb->setTableData($row['jobNo'],1,sizeof($array));
            $tb->setTableData($row['regNo'],2,sizeof($array));
            $tb->setTableData("",3,sizeof($array));
            $tb->setTableData("",4,sizeof($array));
            $tb->setTableData(number_format($row['SparePartCost'],0),5,sizeof($array));
            $tb->setTableData(number_format($row['labourCost'],0),6,sizeof($array));
            $tb->setTableData(number_format(($row['SparePartCost']+$row['labourCost']),0),7,sizeof($array));


        }
        //$tire
        $tb->setTableFooter("Total",0,sizeof($array));
        for($i=1;$i<5;$i++)
        {
            $tb->setTableFooter("",$i,sizeof($array));
        }
        $tb->setTableFooter(number_format($sparepartCost,0),4,sizeof($array));
        $tb->setTableFooter(number_format($labourCost,0),6,sizeof($array));
        $tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));

       // $str="<table class='items' width='100%' style='font-size: 9pt; border-collapse: collapse; ' cellpadding='8'><thead><tr ><td class='total'>Description</td><td class='total'>Value</td><td class='total'>%</td></tr></thead><tbody><tr class='total'><td>Warning Tires</td><td>".$warningTire."</td><td>".$wt."%</td></tr><tr><td>Good Condition Tire</td><td>".$okTires."</td><td>".$gt."%</td></tr></tbody></table><p style='height:20px;'></p>";
        $str="";
        $tb->setCaption($str);
       
    $rp=new Report($tb);
    $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    
    
    
    
    }catch(Exception $exc)
    {
        
    }
//get the table;


