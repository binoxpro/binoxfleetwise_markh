<?php
    session_start();
ini_set('memory_limit', '6024M');
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/tireService.php');
    require_once('../services/tireHoldService.php');
    require_once('../services/fuelOrderItemService.php');
    require_once('../services/tripOtherExpenseService.php');
    require_once('../services/generalLedgerService.php');
    require_once('../services/jobCardService.php');
    require_once('../services/licenceManagerService.php');
    require_once('../reportPluggin/intial.php');

    $grandQty=0;
    $grandBags=0;
    $grandIncome=0;
    $grandLitres=0;
    $grandFuelAmount=0;
    $grandMlgeAmount=0;
    $grandWht=0;
    $grandExpense=0;
    $grandOther=0;
    $grandKms=0;

    $totalTripNo=0;
    $totalKms=0;
    $totalmtccost=0;
    $totalfLitre=0;
    $totalfAmount=0;
    $totalLlitre=0;
    $totalLAmount=0;
    $totalTireCost=0;
    $totalSalaryAndAllowance=0;
    $totalInsurance=0;
    $totalRunningCost=0;
    $totalInvoice=0;
    $totalProfit=0;

    $grandTotalTripNo=0;
    $grandTotalKms=0;
    $grandTotalmtccost=0;
    $grandTotalfLitre=0;
    $grandTotalfAmount=0;
    $grandTotalLlitre=0;
    $grandTotalLAmount=0;
    $grandTotalTireCost=0;
    $grandTotalSalaryAndAllowance=0;
    $grandTotalInsurance=0;
    $grandTotalRunningCost=0;
    $grandTotalInvoice=0;
    $grandTotalProfit=0;



    $fuelAmountTotal=0;
    $MlgeAmountTotal=0;
    $ExpenseTotal=0;
    $Balance=0;
    $AccBalance=0;
    $IncomeAmount=0;
    $expenditure=0;
    $monthArray=array('Jan','Feb','Mar','April','May','June','July','Aug','Sept','Oct','Nov','Dec');
    
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Month","No. Trips","Kms","MTC Cost","MTC/Km","Fuel Litre","Fuel Amount","Fuel/Km","Lubricants Litre","Lubricants Amount","Lube/Km","Tire Cost/Km","Tire Cost","Salary and Allowance","Insurance","Total Running Cost","Invoice","Profit","Operation Cost %");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Running Cost Report</h4>");
    $tb->setTableHeader($array);
    
    $tire=new tireService();
    $tireCosting=new tireHoldService();
    $fuel=new fuelOrderItemService();
    $oil=new fuelOrderItemService();
    $accounting=new generalLedgerService();
    $tripOtherExpense=new TripOtherExpenseService();
    $tripOtherExpense2=new TripOtherExpenseService();
    $jobcard=new jobCardService();
    $licence=new licenceManagerService();
    $sql="";
//    if(isset($_REQUEST['action']))
//    {
//        if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId']))
//        {
//            $startDate=$_REQUEST['startDate'];
//            $endDate=$_REQUEST['endDate'];
//            $vehicleId=$_REQUEST['vehicleId'];
//            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' and wo.CreationDate BETWEEN '$startDate' and '$endDate' order by wo.CreationDate,wo.id, tng.tripNumber";
//
//        }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
//        {
//            $startDate=$_REQUEST['startDate'];
//            $endDate=$_REQUEST['endDate'];
//
//            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where  wo.CreationDate BETWEEN '$startDate' and '$endDate' order by wo.CreationDate,wo.id, tng.tripNumber";
//
//
//        }else if(isset($_REQUEST['vehicleId']))
//        {
//
//            $vehicleId=$_REQUEST['vehicleId'];
//            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' order by wo.CreationDate,wo.id, tng.tripNumber";
//
//        }
//    }else if(isset($_REQUEST['kmsx']))
//	{
//		$kms=$_REQUEST['kmsx'];
//		$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and (th.actualKms-od.reading)<'$kms' order by th.vehicleId,tt.id";
//	}else
//    {
//        $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode order by wo.CreationDate,wo.id, tng.tripNumber";
//    }
//    if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
//    {
//        $_SESSION['startDate']=$_REQUEST['startDate'];
//        $_SESSION['endDate']=$_REQUEST['endDate'];
//        $_SESSION['sqlValue']=$sql;
//    }else
//    {
//        session_unset();
//        $_SESSION['sqlValue']=$sql;
//    }

    session_write_close();
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";

    $startYear=isset($_REQUEST['year'])?$_REQUEST['year']:date('Y');
    for($startYear;$startYear<=intval(date('Y'));$startYear++)
    {
        $starty=$startYear."-01-01";
        $endy=$startYear."-12-".cal_days_in_month(CAL_GREGORIAN, (12),$startYear);
        if($startYear==intval(date('Y')))
        {

            for($i=0;$i<intval(date('m'));$i++)
            {

                $mvalue=$i<9?'0'.($i+1):($i+1);
                $startDate=$startYear."-".$mvalue."-01";
                $endDate=$startYear."-".$mvalue."-".cal_days_in_month(CAL_GREGORIAN, ($i+1),$startYear);
                $tb->setTableData2($monthArray[$i],0,sizeof($array),'color:red'); 
                for($xv=1;$xv<18;$xv++)
                {
                    $tb->setTableData('',$xv,sizeof($array));
                }

                $sqlVehicle="select v.id,v.regNo from tblvehicle v inner join tblvehicletype vt on v.vehicleTypeId=vt.id where vt.vehicleType!='Trailer'";
                foreach($tire->view_query($sqlVehicle) as $rowVehicle) {

                    $vehicleId=$rowVehicle['id'];
                    $sqlInner = "Select count(tng.TripNumber) tripNo,sum(wo.Amount) Amt,sum(customer.distance) kms from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' and wo.creationDate BETWEEN  '$startDate' and '$endDate' ";
                    foreach ($tire->view_query($sqlInner) as $row)
                    {
                        $accounting->setaccountCode('1502');
                        //initalize the expenseTotal
                        $expenditure = 0;
                        $fuel->findTruckFuelDetailSummaryFuel($startDate, $endDate,$vehicleId);
                        $oil->findTruckFuelDetailSummaryOil($startDate, $endDate,$vehicleId);
                       // $fuel->findTripFuelDetailSummaryFuel($startDate, $endDate);
                        //$oil->findTripFuelDetailSummaryOil($startDate, $endDate);
                        $jobcard->truckJobCardCosting($startDate, $endDate,$vehicleId);
                        $licence->getVehicleLicenceCostTotal($starty, $endy,$vehicleId);


                        $tb->setTableData($rowVehicle['regNo'], 0, sizeof($array));
                        $tb->setTableData($row['tripNo'], 1, sizeof($array));
                        $totalTripNo=$totalTripNo+$row['tripNo'];

                        if (intval($row['kms']) > 0) {
                            $tb->setTableData(number_format($row['kms']), 2, sizeof($array));
                            $totalKms=$totalKms+$row['kms'];
                            $grandTotalKms=$grandTotalKms+$row['kms'];
                        } else {
                            $tb->setTableData($row['kms'], 2, sizeof($array));
                        }
                        if ($jobcard->getlabourCost() > 0 && intval($row['kms'])>0) {
                            $tb->setTableData(number_format($jobcard->getlabourCost()), 3, sizeof($array));
                            $tb->setTableData(number_format($jobcard->getlabourCost() / $row['kms'], 2), 4, sizeof($array));
                            $expenditure = $expenditure + $jobcard->getlabourCost();
                            $totalmtccost=$totalmtccost+$jobcard->getlabourCost();
                            $grandTotalmtccost=$grandTotalmtccost+$jobcard->getlabourCost();
                        } else {
                            $tb->setTableData('-', 3, sizeof($array));
                            $tb->setTableData('-', 4, sizeof($array));
                        }
                        // $tb->setTableData('',3,sizeof($array));
                        //$tb->setTableData('',4,sizeof($array));
                        if (intval($fuel->getlitres()) > 0)
                        {
                            $tb->setTableData(number_format($fuel->getlitres()), 5, sizeof($array));
                            $totalfLitre=$totalfLitre+$fuel->getlitres();
                        } else
                        {
                            $tb->setTableData($fuel->getlitres(), 6, sizeof($array));
                        }


                        if (intval($fuel->getrate()) > 0&& intval($row['kms'])>0) {
                            $tb->setTableData(number_format($fuel->getrate()), 7, sizeof($array));
                            $tb->setTableData(number_format($fuel->getlitres() / $row['kms'], 2), 8, sizeof($array));
                            $expenditure = $expenditure + $fuel->getrate();
                            $totalfAmount=$totalfAmount+$fuel->getrate();
                        } else {
                            $tb->setTableData($fuel->getrate(), 7, sizeof($array));
                            $tb->setTableData('-', 8, sizeof($array));
                            //$totalfAmount=$totalfAmount+$fuel->getrate();
                        }

                        if (intval($oil->getlitres()) > 0&& intval($row['kms'])>0) {
                            $tb->setTableData(number_format($oil->getlitres()), 9, sizeof($array));
                            $totalLlitre=$totalLlitre+$oil->getlitres();
                        } else {
                            $tb->setTableData($oil->getlitres(), 9, sizeof($array));
                        }

                        if (intval($oil->getrate()) > 0&& intval($row['kms'])>0)
                        {
                            $tb->setTableData(number_format($oil->getrate()), 10, sizeof($array));
                            $tb->setTableData(number_format($oil->getlitres() / $row['kms'], 2), 10, sizeof($array));
                            $expenditure = $expenditure + $oil->getlitres();
                            $totalLAmount=$totalLAmount+$oil->getrate();

                        } else
                        {
                            $tb->setTableData($oil->getrate(), 10, sizeof($array));
                            $tb->setTableData('-', 10, sizeof($array));
                        }

                        if (intval($tireCosting->getCostPerKmForVehicle($vehicleId)) > 0&& intval($row['kms'])>0)
                        {
                            $tb->setTableData(number_format($tireCosting->getCostPerKmForVehicle($vehicleId)), 11, sizeof($array));


                        }else
                        {
                            $tb->setTableData($tireCosting->getCostPerKmForVehicle($vehicleId), 11, sizeof($array));
                        }

                        if (intval($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']) > 0) {
                            $tb->setTableData(number_format($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']), 12, sizeof($array));
                            $expenditure = $expenditure + ($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']);
                            $totalTireCost=$totalTireCost+($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']);
                        } else
                        {
                            $tb->setTableData($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms'], 12, sizeof($array));
                        }

                        if (intval($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId)) > 0)
                        {
                            $tb->setTableData(number_format($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId)), 13, sizeof($array));
                            $expenditure = $expenditure + $accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId);
                            $totalSalaryAndAllowance=$totalSalaryAndAllowance+$accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId);
                        } else
                        {
                            $tb->setTableData($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId), 13, sizeof($array));
                        }


                        $tb->setTableData(number_format($licence->getlicenceCost()), 14, sizeof($array));
                        $expenditure = $expenditure + $licence->getlicenceCost();
                        $totalInsurance=$totalInsurance+$licence->getlicenceCost();
                        $totalRunningCost=$totalRunningCost+$expenditure;
                        $tb->setTableData(number_format($expenditure), 15, sizeof($array));
                        $tb->setTableData(number_format($row['Amt']), 16, sizeof($array));
                        $tb->setTableData(number_format($row['Amt'] - $expenditure), 17, sizeof($array));
                        $totalInvoice=$totalInvoice+$row['Amt'];
                        if ($expenditure > 0 && $row['Amt'] > 0)
                        {
                            $tb->setTableData(number_format(($expenditure / $row['Amt']) * 100) . "%", 18, sizeof($array));
                        } else
                        {
                            $tb->setTableData("-", 18, sizeof($array));
                        }


                    }
                }
                $tb->setTableData2('Total',0,sizeof($array),'background-color:sky-blue');
                $tb->setTableData('',1,sizeof($array));
                $tb->setTableData(number_format($totalKms),2,sizeof($array));
                $tb->setTableData(number_format($totalmtccost),3,sizeof($array));
                $tb->setTableData('',4,sizeof($array));
                $tb->setTableData(number_format($totalfLitre),5,sizeof($array));
                $tb->setTableData(number_format($totalfAmount),6,sizeof($array));
                $tb->setTableData('',7,sizeof($array));
                $tb->setTableData(number_format($totalLlitre),8,sizeof($array));
                $tb->setTableData(number_format($totalLAmount),9,sizeof($array));
                $tb->setTableData('',10,sizeof($array));
                $tb->setTableData('',11,sizeof($array));
                $tb->setTableData(number_format($totalTireCost),12,sizeof($array));
                $tb->setTableData(number_format($totalSalaryAndAllowance),13,sizeof($array));
                $tb->setTableData(number_format($totalInsurance),14,sizeof($array));
                $tb->setTableData(number_format($totalRunningCost),15,sizeof($array));
                $tb->setTableData(number_format($totalInvoice),16,sizeof($array));
                $tb->setTableData(number_format($totalInvoice-$totalRunningCost),17,sizeof($array));
                if($totalRunningCost>0&&$totalInvoice>0)
                {
                    $tb->setTableData(round(($totalRunningCost/$totalInvoice)*100)."%",18,sizeof($array));
                }else
                {
                    $tb->setTableData('',18,sizeof($array));
                }

                //$tb->setTableData('',19,sizeof($array));

                $totalTripNo=0;
                $totalKms=0;
                $totalmtccost=0;
                $totalfLitre=0;
                $totalfAmount=0;
                $totalLlitre=0;
                $totalLAmount=0;
                $totalTireCost=0;
                $totalSalaryAndAllowance=0;
                $totalInsurance=0;
                $totalRunningCost=0;
                $totalInvoice=0;
                $totalProfit=0;



            }



        }else
        {
            for($i=0;$i<12;$i++)
            {

                $mvalue=$i<9?'0'.($i+1):($i+1);
                $startDate=$startYear."-".$mvalue."-01";
                $endDate=$startYear."-".$mvalue."-".cal_days_in_month(CAL_GREGORIAN, ($i+1),$startYear);
                $tb->setTableData2($monthArray[$i],0,sizeof($array),'color:red');
                for($xv=1;$xv<18;$xv++)
                {
                    $tb->setTableData('',$xv,sizeof($array));
                }

                $sqlVehicle="select v.id,v.regNo from tblvehicle v inner join tblvehicletype vt on v.vehicleTypeId=vt.id where vt.vehicleType!='Trailer'";
                foreach($tire->view_query($sqlVehicle) as $rowVehicle) {

                    $vehicleId=$rowVehicle['id'];
                    $sqlInner = "Select count(tng.TripNumber) tripNo,sum(wo.Amount) Amt,sum(customer.distance) kms from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' and wo.creationDate BETWEEN  '$startDate' and '$endDate' ";
                    foreach ($tire->view_query($sqlInner) as $row)
                    {
                        $accounting->setaccountCode('1502');
                        //initalize the expenseTotal
                        $expenditure = 0;
                        $fuel->findTruckFuelDetailSummaryFuel($startDate, $endDate,$vehicleId);
                        $oil->findTruckFuelDetailSummaryOil($startDate, $endDate,$vehicleId);
                        // $fuel->findTripFuelDetailSummaryFuel($startDate, $endDate);
                        //$oil->findTripFuelDetailSummaryOil($startDate, $endDate);
                        $jobcard->truckJobCardCosting($startDate, $endDate,$vehicleId);
                        $licence->getVehicleLicenceCostTotal($starty, $endy,$vehicleId);


                        $tb->setTableData($rowVehicle['regNo'], 0, sizeof($array));
                        $tb->setTableData($row['tripNo'], 1, sizeof($array));
                        $totalTripNo=$totalTripNo+$row['tripNo'];

                        if (intval($row['kms']) > 0)
                        {
                            $tb->setTableData(number_format($row['kms']), 2, sizeof($array));
                            $totalKms=$totalKms+$row['kms'];
                            $grandTotalKms=$grandTotalKms+$row['kms'];
                        } else {
                            $tb->setTableData($row['kms'], 2, sizeof($array));
                        }
                        if ($jobcard->getlabourCost() > 0 && intval($row['kms'])>0) {
                            $tb->setTableData(number_format($jobcard->getlabourCost()), 3, sizeof($array));
                            $tb->setTableData(number_format($jobcard->getlabourCost() / $row['kms'], 2), 4, sizeof($array));
                            $expenditure = $expenditure + $jobcard->getlabourCost();
                            $totalmtccost=$totalmtccost+$jobcard->getlabourCost();
                            $grandTotalmtccost=$grandTotalmtccost+$jobcard->getlabourCost();
                        } else {
                            $tb->setTableData('-', 3, sizeof($array));
                            $tb->setTableData('-', 4, sizeof($array));
                        }
                        // $tb->setTableData('',3,sizeof($array));
                        //$tb->setTableData('',4,sizeof($array));
                        if (intval($fuel->getlitres()) > 0)
                        {
                            $tb->setTableData(number_format($fuel->getlitres()), 5, sizeof($array));
                            $totalfLitre=$totalfLitre+$fuel->getlitres();
                        } else
                        {
                            $tb->setTableData($fuel->getlitres(), 6, sizeof($array));
                        }


                        if (intval($fuel->getrate()) > 0&& intval($row['kms'])>0) {
                            $tb->setTableData(number_format($fuel->getrate()), 7, sizeof($array));
                            $tb->setTableData(number_format($fuel->getlitres() / $row['kms'], 2), 8, sizeof($array));
                            $expenditure = $expenditure + $fuel->getrate();
                            $totalfAmount=$totalfAmount+$fuel->getrate();
                        } else {
                            $tb->setTableData($fuel->getrate(), 7, sizeof($array));
                            $tb->setTableData('-', 8, sizeof($array));
                        }

                        if (intval($oil->getlitres()) > 0&& intval($row['kms'])>0) {
                            $tb->setTableData(number_format($oil->getlitres()), 9, sizeof($array));
                            $totalLlitre=$totalLlitre+$oil->getlitres();
                        } else {
                            $tb->setTableData($oil->getlitres(), 9, sizeof($array));
                        }

                        if (intval($oil->getrate()) > 0&& intval($row['kms'])>0)
                        {
                            $tb->setTableData(number_format($oil->getrate()), 10, sizeof($array));
                            $tb->setTableData(number_format($oil->getlitres() / $row['kms'], 2), 10, sizeof($array));
                            $expenditure = $expenditure + $oil->getlitres();
                            $totalLAmount=$totalLAmount+$oil->getrate();

                        } else
                        {
                            $tb->setTableData($oil->getrate(), 10, sizeof($array));
                            $tb->setTableData('-', 10, sizeof($array));
                        }

                        if (intval($tireCosting->getCostPerKmForVehicle($vehicleId)) > 0&& intval($row['kms'])>0)
                        {
                            $tb->setTableData(number_format($tireCosting->getCostPerKmForVehicle($vehicleId)), 11, sizeof($array));


                        }else
                        {
                            $tb->setTableData($tireCosting->getCostPerKmForVehicle($vehicleId), 11, sizeof($array));
                        }

                        if (intval($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']) > 0) {
                            $tb->setTableData(number_format($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']), 12, sizeof($array));
                            $expenditure = $expenditure + ($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']);
                            $totalTireCost=$totalTireCost+($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms']);
                        } else
                        {
                            $tb->setTableData($tireCosting->getCostPerKmForVehicle($vehicleId) * $row['kms'], 12, sizeof($array));
                        }

                        if (intval($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId)) > 0)
                        {
                            $tb->setTableData(number_format($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId)), 13, sizeof($array));
                            $expenditure = $expenditure + $accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId);
                            $totalSalaryAndAllowance=$totalSalaryAndAllowance+$accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId);
                        } else
                        {
                            $tb->setTableData($accounting->getAccountPostingVehicle($startDate, $endDate,$vehicleId), 13, sizeof($array));
                        }


                        $tb->setTableData(number_format($licence->getlicenceCost()), 14, sizeof($array));
                        $expenditure = $expenditure + $licence->getlicenceCost();
                        $totalInsurance=$totalInsurance+$licence->getlicenceCost();
                        $totalRunningCost=$totalRunningCost+$expenditure;
                        $tb->setTableData(number_format($expenditure), 15, sizeof($array));
                        $tb->setTableData(number_format($row['Amt']), 16, sizeof($array));
                        $tb->setTableData(number_format($row['Amt'] - $expenditure), 17, sizeof($array));
                        $totalInvoice=$totalInvoice+$row['Amt'];
                        if ($expenditure > 0 && $row['Amt'] > 0)
                        {
                            $tb->setTableData(number_format(($expenditure / $row['Amt']) * 100) . "%", 18, sizeof($array));
                        } else
                        {
                            $tb->setTableData("-", 18, sizeof($array));
                        }


                    }
                }
                $tb->setTableData2('Total',0,sizeof($array),'background-color:sky-blue');
                $tb->setTableData($totalTripNo,1,sizeof($array));
                $tb->setTableData(number_format($totalKms),2,sizeof($array));
                $tb->setTableData(number_format($totalmtccost),3,sizeof($array));
                $tb->setTableData('',4,sizeof($array));
                $tb->setTableData(number_format($totalfLitre),5,sizeof($array));
                $tb->setTableData(number_format($totalfAmount),6,sizeof($array));
                $tb->setTableData('',7,sizeof($array));
                $tb->setTableData(number_format($totalLlitre),8,sizeof($array));
                $tb->setTableData(number_format($totalLAmount),9,sizeof($array));
                $tb->setTableData('',10,sizeof($array));
                $tb->setTableData('',11,sizeof($array));
                $tb->setTableData(number_format($totalTireCost),12,sizeof($array));
                $tb->setTableData(number_format($totalSalaryAndAllowance),13,sizeof($array));
                $tb->setTableData(number_format($totalInsurance),14,sizeof($array));
                $tb->setTableData(number_format($totalRunningCost),15,sizeof($array));
                $tb->setTableData(number_format($totalInvoice),16,sizeof($array));
                $tb->setTableData(number_format($totalInvoice-$totalRunningCost),17,sizeof($array));
                if($totalRunningCost>0&&$totalInvoice>0)
                {
                    $tb->setTableData(round(($totalRunningCost/$totalInvoice)*100)."%",18,sizeof($array));
                }else
                {
                    $tb->setTableData('',18,sizeof($array));
                }

                //$tb->setTableData('',19,sizeof($array));

                $totalTripNo=0;
                $totalKms=0;
                $totalmtccost=0;
                $totalfLitre=0;
                $totalfAmount=0;
                $totalLlitre=0;
                $totalLAmount=0;
                $totalTireCost=0;
                $totalSalaryAndAllowance=0;
                $totalInsurance=0;
                $totalRunningCost=0;
                $totalInvoice=0;
                $totalProfit=0;



            }


        }

    }

    //$tire
//    $tb->setTableFooter("Total",0,sizeof($array));
//     for($i=1;$i<6;$i++)
//     {
//        $tb->setTableFooter("",$i,sizeof($array));
//     }
//    $tb->setTableFooter(number_format($grandQty,0),6,sizeof($array));
//    $tb->setTableFooter(number_format($grandBags,0),7,sizeof($array));
//    $tb->setTableFooter('-',8,sizeof($array));
//    $tb->setTableFooter(number_format($IncomeAmount,0),9,sizeof($array));
//    $tb->setTableFooter('-',10,sizeof($array));
//    $tb->setTableFooter(number_format($grandKms),11,sizeof($array));
//    $tb->setTableFooter(number_format($grandLitres,0),12,sizeof($array));
//    $tb->setTableFooter('-',13,sizeof($array));
//    $tb->setTableFooter(number_format($grandFuelAmount,0),14,sizeof($array));
//    $tb->setTableFooter(number_format($grandMlgeAmount,0),15,sizeof($array));
//    $tb->setTableFooter('-',16,sizeof($array));
//    $tb->setTableFooter(number_format($grandWht,0),17,sizeof($array));
//    $tb->setTableFooter(number_format($grandExpense,0),18,sizeof($array));
//    if($grandExpense>0)
//    {
//        $tb->setTableFooter(round(($grandExpense/$IncomeAmount)*100,2),19,sizeof($array));
//    }else{
//        $tb->setTableFooter('-',19,sizeof($array));
//    }
//
//    $tb->setTableFooter('-',20,sizeof($array));
//$tb->setTableFooter("-",21,sizeof($array));

    //$tb->setTableFooter(number_format($AccBalance),20,sizeof($array));

    $str="";
     $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>