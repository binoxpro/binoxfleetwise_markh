<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PdfreportStructure
{
    private $pdfHeader;
    private $pdfFooter;
    private $pdfBody;
    private $pdfNote;
    
    public function __construct() 
    {
        
    }
    
    public function getPdfHeader()
    {
        $this->pdfHeader= '<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.01mm solid #000000;
	border-radius:5px;
}
td { vertical-align: top;text-align: left; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items th {
	/*border-bottom: 0.1mm solid black;*/
	
}
table thead td { /*background-color: #EEEEEE;*/
	text-align: left;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {

	border: 0.1mm solid #000000;
	/*background-color: #FFFFFF;*/
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
.item2
{
    border-bottom: 0.1mm solid #000000;
}
.item3
{
     border-right: 0.1mm solid #000000;
     border-bottom: 0.1mm solid black;
     border-left: 0.1mm solid #000000;
}
.item4 th
{
    border:0mm solid white;
}
</style>
</head>
<body><htmlpageheader name="myheader"><table width="100%">
<tr><td  colspan="2"><img src="../images/logo.png" width="800px" height="80px" /> </td>
</tr><tr><td colspan="2" align="left">
<p>P.O BOX 22683, Kampala <br/>Ntinda junction, martrys way, Plot 139,<br/> Tel +256 (0) 772 366 390  |  Mobile +256 (0) 701 082 909  |  Email maria.n@markhinvestments.com| www.markhinvestments.com</p>
</tr>
</table>';
        return $this->pdfHeader.$this->getPdfFooter();
    }
    
    public function getPdfFooter()
    {
        $this->pdfFooter='<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 5pt; text-align: center; padding-top: 3mm; "><i>Ntinda junction, martrys way, Plot 139,P.O BOX 22683, Kampala Tel +256 (0) 772 366 390  |  Mobile +256 (0) 701 082 909  |  Email maria.n@markhinvestments.com| www.markhinvestments.com</i><br />
Page {PAGENO} of {nb}
</div>
</htmlpagefooter><sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />';
        return $this->pdfFooter;
    }
    
    public function setPdfBody($str)
    {
        $this->pdfBody=$str."</body></html>";
    }    
    
    public function getPdfBody()
    {
        return $this->pdfBody;
    }
    public function printOut($file)
    {
        $html=$this->getPdfHeader().$this->getPdfBody();
        $mpdf=new mPDF('c','A4','','',20,15,10,25,10,10);
        $mpdf->SetTitle('Report');
        $mpdf->SetAuthor("code360 data solution");
        $mpdf->SetWatermarkText("");
        $mpdf->showWatermarkText = false;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.01;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output($file); 
    }
    
    
    
    
}
