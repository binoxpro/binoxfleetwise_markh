<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of myTable
 *
 * @author user
 */
//require_once('tableTemplate.php');
class myTable {
    //put your code here
    public $tableTemplate;
    
    public function __construct(TableTemplate $tt)
    {
        $this->tableTemplate=$tt;
    }
    
    public function setCaption($str)
    {
        $this->tableTemplate->addCaption($str);
    }
    
    public function setTableHeader($array)
    {
        
        for($i=0;$i<sizeof($array);$i++)
        {
            if($i==0)
            {
              $this->tableTemplate->addHeader('<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8"><thead><tr><th>'.$array[$i].'</th>');  
            }else if($i>0 && $i<(sizeof($array)-1))
            {
                $this->tableTemplate->addHeader("<th>".$array[$i]."</th>");  
            }else 
            {
               $this->tableTemplate->addHeader("<th>".$array[$i]."</th></tr><thead><tbody>");  
            }
        }
    }
    
    public function setTableData($str,$i,$length)
    {
        
        
            if($i==0)
            {
                $this->tableTemplate->addData("<tr><td>".$str."</td>");  
            }else if($i > 0 && $i < $length-1)
            {
                $this->tableTemplate->addData("<td>".$str."</td>");  
            }else 
            {
               $this->tableTemplate->addData("<td>".$str."</td></tr>");  
            }
        
    }
    
     public function setTableData2($str,$i,$length,$style)
    {
        
        
            if($i==0)
            {
                $this->tableTemplate->addData("<tr style='".$style."'><td>".$str."</td>");  
            }else if($i > 0 && $i < $length-1)
            {
                $this->tableTemplate->addData("<td>".$str."</td>");  
            }else 
            {
               $this->tableTemplate->addData("<td>".$str."</td></tr>");  
            }
        
    }
    
    
    public function setTableFooter($str,$i,$length,$style='item2')
    {
        
        
            if($i==0)
            {
                $this->tableTemplate->addFooter("<tr ><th class='".$style."'>".$str."</th>");
            }else if($i > 0 && $i < $length-1)
            {
                $this->tableTemplate->addFooter("<th class='".$style."'>".$str."</th>");
            }else 
            {
               $this->tableTemplate->addFooter("<th class='".$style."'>".$str."</th></tr>");
            }
        
    }
    
    public function setTableClose()
    {
        $this->tableTemplate->addFooter("</table>"); 
    }

    public function setNotes($str)
    {
        $this->tableTemplate->addNotes($str);
    }
}
