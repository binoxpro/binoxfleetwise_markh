<?php
    session_start();
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/jobCardService.php');
    require_once('../services/jobcardItemService.php');
    require_once('../services/partsUsedService.php');
    //('../services/tireHoldService.php');
    require_once('../reportPluggin/intial.php');
    $sparepartCost=0;
    $labourCost=0;
    $totalCost=0;
    $jobCardId=-1;
    $jobCardNo=-1;
    $dateCreation="";
    $regNo="";

    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Date","Jobcard No ","Registration Number","Spare Part/Labour","Supplier","Quality","Rate","Total");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>JOBCARD SPARE PART REPORT</h4>");
    $tb->setTableHeader($array);
    
    $jobCard=new jobCardService();
    $sql="";
    if(isset($_REQUEST['action']))
    {
         if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];
           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.vehicleId='$vehicleId' and  jc.creationDate BETWEEN '$startDate' and '$endDate' ";

       }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];

           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where  jc.creationDate BETWEEN '$startDate' and '$endDate' ";


       }else if(isset($_REQUEST['vehicleId']))
       {

           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.vehicleId='$vehicleId'  ";

       }
        //$sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id ";


    }else if(isset($_REQUEST['kmsx']))
	{
		$kms=$_REQUEST['kmsx'];
		$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and (th.actualKms-od.reading)<'$kms' order by th.vehicleId,tt.id";
	}else{
      $sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id ";
      
    }
    $_SESSION['sqlValue']=$sql;
   if(isset($_REQUEST['supplierId']))
   {
       $_SESSION['supplierId']=$_REQUEST['supplierId'];
   }
    session_write_close();
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($jobCard->view_query($sql) as $row)
    {
        if(!is_null($row['SparePartCost'])) {
            $sparepartCost = $sparepartCost + $row['SparePartCost'];
            $row['SparePartCost']=$row['SparePartCost'];
        }else{
            $sparepartCost = $sparepartCost + 0;
            $row['SparePartCost']=0;
        }
        if(!is_null($row['labourCost']))
        {
            $labourCost=$labourCost+$row['labourCost'];
            $row['labourCost']=$row['labourCost'];
        }else {
            $labourCost = $labourCost + $row['labourCost'];
            $row['labourCost']=0;
        }

        //$totalCost=$totalCost+($row['labourCost']+$row['SparePartCost']);
        $jobCardId=$row['id'];
        $jobCardNo=$row['jobNo'];
        $regNo=$row['regNo'];
        $dateCreation=$row['creationDate'];
        
//        $tb->setTableData($row['creationDate'],0,sizeof($array));
//        $tb->setTableData($row['jobNo'],1,sizeof($array));
//        $tb->setTableData($row['regNo'],2,sizeof($array));
//        $tb->setTableData("",3,sizeof($array));
//        $tb->setTableData("",4,sizeof($array));
//        $tb->setTableData(number_format($row['SparePartCost'],0),5,sizeof($array));
//        $tb->setTableData(number_format($row['labourCost'],0),6,sizeof($array));
//        $tb->setTableData(number_format(($row['SparePartCost']+$row['labourCost']),0),7,sizeof($array));

        //generate a part used
        $sqlPartsUsed='';
        if(isset($_REQUEST['supplierId']))
        {
            $supplierId=$_REQUEST['supplierId'];
            $sqlPartsUsed="Select Concat(sp.partName,'-',sp.partNumber) partNaming,pu.quantity,pu.cost,(pu.quantity*pu.cost) total,sup.supplierName from tblpartsused pu inner join sparepart sp on pu.partusedId=sp.id inner join tblsupplier sup on pu.supplierId=sup.supplierCode where pu.jobcardId='$jobCardId' and pu.supplierId='$supplierId' and pu.supplied=1";
            //$_SESSION['sqlValue2']=$sqlPartsUsed;
        }else
        {
            $sqlPartsUsed="Select Concat(sp.partName,'-',sp.partNumber) partNaming,pu.quantity,pu.cost,(pu.quantity*pu.cost) total,sup.supplierName from tblpartsused pu inner join sparepart sp on pu.partusedId=sp.id inner join tblsupplier sup on pu.supplierId=sup.supplierCode where pu.jobcardId='$jobCardId' and  pu.supplied=1";
            //$_SESSION['sqlValue2']=$sqlPartsUsed;
        }


        foreach($jobCard->view_query($sqlPartsUsed) as $row2)
        {
            $tb->setTableData($dateCreation,0,sizeof($array));
            $tb->setTableData($jobCardNo,1,sizeof($array));
            $tb->setTableData($regNo,2,sizeof($array));
            $tb->setTableData($row2['partNaming'],3,sizeof($array));
            $tb->setTableData($row2['supplierName'],4,sizeof($array));
            $tb->setTableData(number_format(intval($row2['quantity']),0),5,sizeof($array));
            $tb->setTableData(number_format(intval($row2['cost']),0),6,sizeof($array));
            $tb->setTableData(number_format(intval($row2['total']),0),7,sizeof($array));
            $totalCost=$totalCost+$row2['total'];

        }
        //generate a labour used

        
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));
     for($i=1;$i<6;$i++)
     {
        $tb->setTableFooter("",$i,sizeof($array));
     }
    $tb->setTableFooter('',6,sizeof($array));
    $tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));


    $str="";
    $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>