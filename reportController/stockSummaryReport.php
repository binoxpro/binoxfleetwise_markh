<?php
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/tireService.php');
    require_once('../reportPluggin/intial.php');
    
    $total=0;
    $Instock=0;
    $retread=0;
    $scrapped=0;
    $repair=0;
    $retreadTire=0;
    $newTire=0;
    
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Description","Value","Percentage");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Stock Summary Report</h4>");
    $tb->setTableHeader($array);
    
    $tire=new tireService();
    $sql1="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Instock'";
    foreach($tire->view_query($sql1) as $row)
    {
        $Instock=$row['noTires'];
    }
    
    $sql2="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Taken for Retreading'";
    foreach($tire->view_query($sql2) as $row)
    {
        $retread=$row['noTires'];
    }
    
    
    $sql3="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Fitted'";
    foreach($tire->view_query($sql3) as $row)
    {
        $fitted=$row['noTires'];
    }
    
    $sql4="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Scrapped'";
    foreach($tire->view_query($sql4) as $row)
    {
        $scrapped=$row['noTires'];
    }
    
    $sql5="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Taken for Repair'";
    foreach($tire->view_query($sql5) as $row)
    {
        $repair=$row['noTires'];
    }
    
     $sql6="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Instock' and tireType='Retread'";
    foreach($tire->view_query($sql6) as $row)
    {
        $retreadTire=$row['noTires'];
    }
    
    $sql7="SELECT COUNT(tireBand) noTires FROM `tbltire` WHERE `status`='Instock' and tireType='New'";
    foreach($tire->view_query($sql7) as $row)
    {
        $newTire=$row['noTires'];
    }
    
    $total=$scrapped+$fitted+$repair+$retreadTire+$newTire;
    
    $tb->setTableData("Retread instock",0,3);
    $tb->setTableData($retreadTire,1,3);
    if($retreadTire>0)
    {
        $tb->setTableData(round(($retreadTire/$total)*100,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    $tb->setTableData("New Tire instock",0,3);
    $tb->setTableData($newTire,1,3);
    if($newTire>0)
    {
        $tb->setTableData(round(($newTire/$total)*100,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    
    $tb->setTableData("In Stock",0,3);
    $tb->setTableData($Instock,1,3);
    if($Instock>0)
    {
        $tb->setTableData(round($Instock/$total,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    $tb->setTableData("On Trucks",0,3);
    $tb->setTableData($fitted,1,3);
    if($fitted>0)
    {
        $tb->setTableData(round(($fitted/$total)*100,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    $tb->setTableData("Taken for Retread",0,3);
    $tb->setTableData($retread,1,3);
    if($retread>0)
    {
        $tb->setTableData(round(($retread/$total)*100,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    $tb->setTableData("Scrapped",0,3);
    $tb->setTableData($scrapped,1,3);
    if($scrapped>0)
    {
        $tb->setTableData(round(($scrapped/$total)*100,2),2,3);
    }else
    {
        $tb->setTableData(0,2,3);
    }
    
    

    $tb->setTableFooter("Total",0,3);
    $tb->setTableFooter($total,1,3);
    $tb->setTableFooter(100,2,3);
    
    
    
    $rb=new ReportBuilder($tb);
    echo $rb->getReport();
    


?>