<?php
    session_start();
//                <th field='regNo' width='90'>Truck</th>
//                <th field='creationDate' width='90'>Service Date.</th>
//                <th field='serviceName' width='90'>Service Type</th>
//                <th field='kmReading' width='90'>Service Km</th>
//                <th field='nextReading' width='90'>Next Service Reading</th>
//                <th field='currentReading' width='90'>Current Km</th>
//                <th field='reminderReading' width='90'>Reminder Reading</th>
//                <th field='status' width='90'>Status</th>
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/serviceHeaderService.php');
    require_once('../services/subServiceService.php');
    require_once('../services/componentScheduleService.php');
    //('../services/tireHoldService.php');
    require_once('../reportPluggin/intial.php');
    $sparepartCost=0;
    $labourCost=0;
    $totalCost=0;
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Registration Number","Service Date","Service Type","Serviced At Km","Next Service Reading At Km","Current Reading","Status");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Preventive Maintainance Service REPORT</h4>");
    $tb->setTableHeader($array);
    
   $serviceHeader=new serviceHeaderService();
    $sql="";
    if(isset($_REQUEST['action']))
    {
       if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];
           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId where sh.vehicleId='$vehicleId' and  sh.creationDate BETWEEN '$startDate' and '$endDate' order by sh.id asc ";

       }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
       {
           $startDate=$_REQUEST['startDate'];
           $endDate=$_REQUEST['endDate'];

           $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId where  sh.creationDate BETWEEN '$startDate' and '$endDate' order by sh.id asc ";


       }else if(isset($_REQUEST['vehicleId']))
       {

           $vehicleId=$_REQUEST['vehicleId'];
           $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId where sh.vehicleId='$vehicleId' order by sh.id asc ";

       }
        //$sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id ";


    }else if(isset($_REQUEST['kmsx']))
	{
		$kms=$_REQUEST['kmsx'];
		$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and (th.actualKms-od.reading)<'$kms' order by th.vehicleId,tt.id";
	}else{
      $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId order by sh.id asc ";

    }
    $_SESSION['sqlValue']=$sql;
    session_write_close();
    $remaining=0;
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($serviceHeader->view_query($sql) as $row)
    {
                //<th field='regNo' width='90'>Truck</th>
//                <th field='creationDate' width='90'>Service Date.</th>
//                <th field='serviceName' width='90'>Service Type</th>
//                <th field='kmReading' width='90'>Service Km</th>
//                <th field='nextReading' width='90'>Next Service Reading</th>
//                <th field='currentReading' width='90'>Current Km</th>
//                <th field='reminderReading' width='90'>Reminder Reading</th>
//                <th field='status' width='90'>Status</th>
        $remaining=intval($row['nextReading'])-intval($row['currentReading']);
        if($remaining<=1000){
            $tb->setTableData2($row['regNo'],0,sizeof($array),'color:red');
        }else {
            $tb->setTableData($row['regNo'], 0, sizeof($array));
        }
        $tb->setTableData($row['creationDate'],1,sizeof($array));
        $tb->setTableData($row['serviceName'],2,sizeof($array));
        $tb->setTableData($row['kmReading'],3,sizeof($array));
        $tb->setTableData($row['nextReading'],4,sizeof($array));
        $tb->setTableData($row['currentReading'],5,sizeof($array));
       // $tb->setTableData($row['serviceName'],6,sizeof($array));
        $tb->setTableData($row['status'],6,sizeof($array));

        
    }
    //$tire
//    $tb->setTableFooter("Total",0,sizeof($array));
//     for($i=1;$i<5;$i++)
//     {
//        $tb->setTableFooter("",$i,sizeof($array));
//     }
//    $tb->setTableFooter(number_format($sparepartCost,0),4,sizeof($array));
//    $tb->setTableFooter(number_format($labourCost,0),6,sizeof($array));
//    $tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));

    $str="";
    $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>