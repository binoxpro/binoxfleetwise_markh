<?php
    session_start();
 //require_once('../');
 //require_once('../controller/controller.php');
    require_once('../services/jobCardService.php');
    require_once('../services/jobcardItemService.php');
    require_once('../services/partsUsedService.php');
    require_once('../services/vehicleNewService.php');

    //('../services/tireHoldService.php');
    require_once('../reportPluggin/intial.php');
    $sparepartCost=0;
    $labourCost=0;
    $totalCost=0;
    $jobCardId=-1;
    $jobCardNo=-1;
    $dateCreation="";
    $regNo="";
    $makeId=0;
    $makeName="";
    $vehicle=new vehicleNewService();
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Model","No of Trucks ","Spare part Cost");
    if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
    {
        $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED </h4><h4 class='text-center'>Spare By Model Report</h4><h4 class='text-center'>From ".$_REQUEST['startDate']." To:".$_REQUEST['endDate']." </h4>");
    }else
    {
        $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Spare By Model Report</h4>");
    }

    $tb->setTableHeader($array);
    
    $jobCard=new jobCardService();
    $sql="";

    $sql="select DISTINCT(tm.modelName) makeName,tm.id id  from tblvehicle v inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id inner join tblvehicletype vt on v.vehicleTypeId=vt.id where vt.vehicleType!='Trailer' ";
      

    $_SESSION['sqlValue']=$sql;
   if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
   {
       $_SESSION['startDate']=$_REQUEST['startDate'];
       $_SESSION['endDate']=$_REQUEST['endDate'];
   }else
   {
       session_unset();
       $_SESSION['sqlValue']=$sql;
   }
    session_write_close();
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($jobCard->view_query($sql) as $row)
    {

            $makeId=$row['id'];
            $makeName=$row['makeName'];
        
//        $tb->setTableData($row['creationDate'],0,sizeof($array));
//        $tb->setTableData($row['jobNo'],1,sizeof($array));
//        $tb->setTableData($row['regNo'],2,sizeof($array));
//        $tb->setTableData("",3,sizeof($array));
//        $tb->setTableData("",4,sizeof($array));
//        $tb->setTableData(number_format($row['SparePartCost'],0),5,sizeof($array));
//        $tb->setTableData(number_format($row['labourCost'],0),6,sizeof($array));
//        $tb->setTableData(number_format(($row['SparePartCost']+$row['labourCost']),0),7,sizeof($array));

        //generate a part used
        $sqlPartsUsed='';
        if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
        {
            $startDate=$_REQUEST['startDate'];
            $endDate=$_REQUEST['endDate'];
            $sqlPartsUsed="Select sum(pu.quantity*pu.cost) total from tblpartsused pu inner join tbljobcard jc on pu.jobcardId=jc.id inner join tblvehicle v on jc.vehicleId=v.id where v.modelId='$makeId' and jc.creationDate BETWEEN '$startDate' and '$endDate'";
            //$_SESSION['sqlValue2']=$sqlPartsUsed;
        }else
        {
            $sqlPartsUsed="Select sum(pu.quantity*pu.cost) total from tblpartsused pu inner join tbljobcard jc on pu.jobcardId=jc.id inner join tblvehicle v on jc.vehicleId=v.id where v.modelId='$makeId'";
            //$_SESSION['sqlValue2']=$sqlPartsUsed;
        }


        foreach($jobCard->view_query($sqlPartsUsed) as $row2)
        {
            $tb->setTableData($makeName,0,sizeof($array));
            $tb->setTableData($vehicle->findNoMake($makeId),1,sizeof($array));
            $tb->setTableData(number_format($row2['total'],0),2,sizeof($array));

            $totalCost=$totalCost+$row2['total'];

        }
        //generate a labour used

        
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));

    $tb->setTableFooter("",1,sizeof($array));

    //$tb->setTableFooter('',2,sizeof($array));
    $tb->setTableFooter(number_format($totalCost,0),2,sizeof($array));


    $str="";
    $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>