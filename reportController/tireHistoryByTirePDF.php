<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
    require_once 'pdfController/tableTemplate.php';
    require_once 'pdfController/report.php';
    require_once 'pdfController/pdfReportStructure.php';
    require_once('../services/tireService.php');
    require_once('../services/tireHoldService.php');

    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    $tire2=new tireService();
	$sqlV="";
        //$_SESSION['sqlValue']=$sqlV;
        if(isset($_SESSION['sqlValue']))
        {
            $sqlV=$_SESSION['sqlValue'];
        }
	
    foreach($tire2->view_query($sqlV) as $row2){
    
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    
    
    
    $tt=new TableTemplate();
    $tb=new myTable($tt);
    
    $array=array("Vehicle","Position","Brand","S.No","Fitting Date","Fitting OBC","Expected kms","Removal Kms","Current OBC","KMS Done","Remaining Kms","TreadDepth","TreadWear%","Pressure","Cost","CPK","Current Cost","Maintanance Cost","Total Cost");
    //$tb->setCaption("<h4 class='text-center' style='font-weight:bold'>FST Transporter Limited</h4><h4 class='text-center'>Mounted Tire Report</h4>");
    $tb->setTableHeader($array);
    
    $tire=new tireService();
    $sql="";
    //if(isset($_REQUEST['action']))
//    {
        $id=$row2['id'];
      $sql="select *,th.isActive mounted ,od.reading currentReading,case when th.isActive='1' then (od.reading-th.fixingOdometer) else (th.actualKms-th.fixingOdometer) end kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex,(Select sum(tmc.cost) maintenancecost from tbltiremaintenancecost tmc where tmc.tireId=th.tireId ) mcost from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE  th.tireId='$id' or t.parentId='$id' order by th.vehicleId,tt.id";
      
        
    //}else{
//      $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
//      
//    }
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($tire->view_query($sql) as $row)
    {
        $currentCost=$currentCost+(($row['cost']/$row['expectedKms'])*$row['kmsDone']);
        $purchase=$purchase+$row['cost'];
        $remaining=$row['actualKms']-$row['currentReading'];
        $i=0;
        if(intval($row['mounted'])==1)
        {
            if($remaining<1000){
               $tb->setTableData2($row['regNo'],0,sizeof($array),'color:red'); 
               $warningTire=$warningTire+1;
            }else if(floatval($row['treadDepthx'])<4 && !is_null($row['treadDepthx']) )
            {
                
                $tb->setTableData2($row['regNo'],0,sizeof($array),'color:green'); 
                 $warningTire=$warningTire+1;
            }else{
                $tb->setTableData($row['regNo'],0,sizeof($array));
                $okTires=$okTires+1;
            }
        }else
        {
            $tb->setTableData2($row['regNo'],0,sizeof($array),'background-color:blue;'); 
        }
        
        $tb->setTableData($row['tyretypeName'],1,sizeof($array));
        $tb->setTableData($row['tireBand'],2,sizeof($array));
        $tb->setTableData($row['serialNumber'],3,sizeof($array));
        $tb->setTableData($row['fixingDate'],4,sizeof($array));
        $tb->setTableData($row['fixingOdometer'],5,sizeof($array));
        $tb->setTableData($row['expectedKms'],6,sizeof($array));
        $tb->setTableData($row['actualKms'],7,sizeof($array));
        $tb->setTableData($row['currentReading'],8,sizeof($array));
        $tb->setTableData($row['kmsDone'],9,sizeof($array));
        $tb->setTableData($row['actualKms']-$row['currentReading'],10,sizeof($array));
        $tb->setTableData($row['treadDepthx'],11,sizeof($array));
        $tb->setTableData($row['treadWear'],12,sizeof($array));
        $tb->setTableData($row['pressurex'],13,sizeof($array));
        
        $tb->setTableData(number_format($row['cost'],0),14,sizeof($array));
        $tb->setTableData(number_format( round($row['cost']/$row['expectedKms'],2),2),15,sizeof($array));
        $tb->setTableData(number_format(round(($row['cost']/$row['expectedKms'])*$row['kmsDone'],2),2),16,sizeof($array));
        $tb->setTableData($row['mcost'],17,sizeof($array));
        $tb->setTableData('',18,sizeof($array)); 
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));
     for($i=1;$i<sizeof($array)-5;$i++)
     {
        $tb->setTableFooter("",$i,sizeof($array));
     }
    $tb->setTableFooter(number_format($purchase,0),14,sizeof($array));
    $tb->setTableFooter("",15,sizeof($array));
    $tb->setTableFooter(number_format(round($currentCost,0),0),16,sizeof($array));
    $tb->setTableFooter('',17,sizeof($array)); 
    $tb->setTableFooter('',18,sizeof($array)); 
    // 
    // table 2
    $tt2=new TableTemplate();
    $tb2=new myTable($tt2);
    $sqlMaintanance="select * from tbltiremaintenancecost where tireId='$id'";
    
    $array2=array("Details","Cost","Date");
    //$tb->setCaption("<h4 class='text-center' style='font-weight:bold'>FST Transporter Limited</h4><h4 class='text-center'>Mounted Tire Report</h4>");
    
    $tb2->setTableHeader($array2);
    
    foreach($tire->view_query($sqlMaintanance) as $rowM)
    {
        $tb2->setTableData($rowM['details'],0,sizeof($array2));
        $tb2->setTableData($rowM['cost'],1,sizeof($array2));
        $tb2->setTableData($rowM['creationDate'],2,sizeof($array2));
        
    }
    
    for($i=0;$i<sizeof($array2);$i++)
     {
        $tb2->setTableFooter("",$i,sizeof($array));
     }
     
    $wt=0;
    $gt=0;
    if($warningTire>0){
    $wt=round(($warningTire/($warningTire+$okTires))*100,2);
    }
    if($okTires>0)
    {
        $gt=round(($okTires/($warningTire+$okTires))*100,2);
    }
    
    $str="";
    $str.="<h4 align='center'><u>Tire No:".$row2['serialNumber']."</u></h4><table class='items' width='100%' style='font-size: 9pt; border-collapse: collapse; ' cellpadding='8'><tr><td class='totals'>Serial.</td><td>".$row2['serialNumber']."</td><td>Size</td><td>".$row2['tireSize']."</td></tr>";
    $str.="<tr><td class='totals'>Brand</td><td class='totals'>".$row2['tireBand']."</td><td class='totals'>Tread Depth</td><td class='totals'>".$row2['retreadDepth']."</td></tr>";
    $str.="<tr><td class='totals'>Pattern</td><td class='totals'>".$row2['pattern']."</td><td class='totals'>Type:</td><td class='totals'>".$row2['tireType']."</td></tr>";
    $str.="<tr><td class='totals'>Cost</td><td class='totals'>".$row2['cost']."</td><td class='totals'>Expected Kms:</td><td class='totals'>".$row2['kms']."</td></tr>";
    $str.="<tr><td class='totals'>Status</td><td class='totals'>".$row2['status']."</td><td class='totals'>Pressure:</td><td class='totals'>".$row2['maxmiumPressure']."</td></tr></table><p style='height:30px;'></p>";
    
    //$str="<table class='table table-bordered'><tr><th>Description</th><th>Value</th><th></th></tr><tr class='danger'><th>Warning Tires</th><th>".$warningTire."</th><th>".$wt."%</th></tr><tr class='success'><th>Good Condition Tire</th><th>".$okTires."</th><th>".$gt."%</th></tr></table>";
    $tb->setCaption($str);
       
    $rb=new Report($tb);
    
    $htmlx.=$rb->getReport();
    $rb2=new Report($tb2);
    
    $htmlx.=$rb2->getReport();
    }
    define('_MPDF_PATH','../lib/mpdf60/');
    include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    


