<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
 require_once('../services/tireService.php');
 require_once('../services/tireHoldService.php');
 require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
$currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    $tire2=new tireService();
    $sqlV=$_SESSION['sqlValue'];
	
    $tt=new TableTemplate();
    $tb=new myTable($tt);
    $array=array("Brand","Serial","TireType","Cost","Expected Kms","Pattern","Size","Status");
    $tb->setCaption("<h4 align='center' style='font-weight:bold;'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 align='center'>Tire Inventory  Report</h4>");
    $tb->setTableHeader($array);
    
        foreach($tire2->view_query($sqlV) as $row)
        {
        
            $tb->setTableData($row['tireBand'],0,sizeof($array));
            $tb->setTableData($row['serialNumber'],1,sizeof($array));
            $tb->setTableData($row['tireType'],2,sizeof($array));
            $tb->setTableData($row['cost'],3,sizeof($array));
            $tb->setTableData($row['kms'],4,sizeof($array));
            $tb->setTableData($row['pattern'],5,sizeof($array));
            $tb->setTableData($row['tireSize'],6,sizeof($array));
            $tb->setTableData($row['status'],7,sizeof($array));
			
	}
    $rb=new Report($tb);
   
    $htmlx=$rb->getReport();

    define('_MPDF_PATH','../lib/mpdf60/');
    include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
