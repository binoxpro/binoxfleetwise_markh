<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');
require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/serviceHeaderService.php');
require_once('../services/subServiceService.php');
require_once('../services/componentScheduleService.php');
//('../services/tireHoldService.php');
//require_once('../reportPluggin/intial.php');
//require_once('../services/tireHoldService.php');
//require_once('../reportPluggin/intial.php');
    try{
        $sparepartCost=0;
        $labourCost=0;
        $totalCost=0;
    
    $tt=new TableTemplate();
    $tb=new myTable($tt);

        $array=array("Registration Number","Service Date","Service Type","Serviced At Km","Next Service Reading At Km","Current Reading","Status");
        $tb->setCaption("<h4  style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 align='center'>Preventive Maintainance Service REPORT</h4>");
        $tb->setTableHeader($array);

        $serviceHeader=new serviceHeaderService();
        $sql="";

        $sql=$_SESSION['sqlValue'];
        session_write_close();
        $remaining=0;
        // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
        foreach($serviceHeader->view_query($sql) as $row)
        {
            //<th field='regNo' width='90'>Truck</th>
//                <th field='creationDate' width='90'>Service Date.</th>
//                <th field='serviceName' width='90'>Service Type</th>
//                <th field='kmReading' width='90'>Service Km</th>
//                <th field='nextReading' width='90'>Next Service Reading</th>
//                <th field='currentReading' width='90'>Current Km</th>
//                <th field='reminderReading' width='90'>Reminder Reading</th>
//                <th field='status' width='90'>Status</th>
            $remaining=intval($row['nextReading'])-intval($row['currentReading']);
            if($remaining<=1000){
                $tb->setTableData2($row['regNo'],0,sizeof($array),'color:red');
            }else {
                $tb->setTableData($row['regNo'], 0, sizeof($array));
            }
            $tb->setTableData($row['creationDate'],1,sizeof($array));
            $tb->setTableData($row['serviceName'],2,sizeof($array));
            $tb->setTableData($row['kmReading'],3,sizeof($array));
            $tb->setTableData($row['nextReading'],4,sizeof($array));
            $tb->setTableData($row['currentReading'],5,sizeof($array));
            // $tb->setTableData($row['serviceName'],6,sizeof($array));
            $tb->setTableData($row['status'],6,sizeof($array));


        }
        //$tire
//    $tb->setTableFooter("Total",0,sizeof($array));
//     for($i=1;$i<5;$i++)
//     {
//        $tb->setTableFooter("",$i,sizeof($array));
//     }
//    $tb->setTableFooter(number_format($sparepartCost,0),4,sizeof($array));
//    $tb->setTableFooter(number_format($labourCost,0),6,sizeof($array));
//    $tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));

        $str="";
        $tb->setCaption($str);
       
    $rp=new Report($tb);
    $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    
    
    
    
    }catch(Exception $exc)
    {
        
    }
//get the table;


