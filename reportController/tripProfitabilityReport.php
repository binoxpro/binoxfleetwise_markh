<?php
    session_start();
 //require_once('../');
 //require_once('../controller/controller.php');
    //requiervices/tireService.php');
    require_once('../services/tireHoldService.php');
    require_once('../services/fuelOrderItemService.php');
    require_once('../services/tripOtherExpenseService.php');
    require_once('../reportPluggin/intial.php');
    
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;

    $grandQty=0;
    $grandBags=0;
    $grandIncome=0;
    $grandLitres=0;
    $grandFuelAmount=0;
    $grandMlgeAmount=0;
    $grandWht=0;
    $grandExpense=0;
    $grandOther=0;
    $grandKms=0;



    $fuelAmountTotal=0;
    $MlgeAmountTotal=0;
    $ExpenseTotal=0;
    $Balance=0;
    $AccBalance=0;
    $IncomeAmount=0;
    
    
    $tt=new TableTemplate();
    $tb=new TableBuilder($tt);
    
    $array=array("Date","TripNo","Veh .No","Delivery No.","P.O. No","Product","Quantity","Bags","Rate","Amount","Customer","KMS","Fuel","Rate","Amount","MLGE","Other","WHT","Total","EXP%","Bal","Acc Bal");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Trip Profitability</h4>");
    $tb->setTableHeader($array);
    
    $tire=new tireService();
    $fuelOrderItem=new fuelOrderItemService();
    $tripOtherExpense=new TripOtherExpenseService();
    $tripOtherExpense2=new TripOtherExpenseService();
    $sql="";
    if(isset($_REQUEST['action']))
    {
        if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId']))
        {
            $startDate=$_REQUEST['startDate'];
            $endDate=$_REQUEST['endDate'];
            $vehicleId=$_REQUEST['vehicleId'];
            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' and wo.CreationDate BETWEEN '$startDate' and '$endDate' order by wo.CreationDate,wo.id, tng.tripNumber";

        }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
        {
            $startDate=$_REQUEST['startDate'];
            $endDate=$_REQUEST['endDate'];

            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where  wo.CreationDate BETWEEN '$startDate' and '$endDate' order by wo.CreationDate,wo.id, tng.tripNumber";


        }else if(isset($_REQUEST['vehicleId']))
        {

            $vehicleId=$_REQUEST['vehicleId'];
            $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode where tng.vehicleId='$vehicleId' order by wo.CreationDate,wo.id, tng.tripNumber";

        }
    }else if(isset($_REQUEST['kmsx']))
	{
		$kms=$_REQUEST['kmsx'];
		$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and (th.actualKms-od.reading)<'$kms' order by th.vehicleId,tt.id";
	}else
    {
        $sql="Select wo.CreationDate date,tng.TripNumber,v.regNo vehNo,wo.DeliveryNo,wo.PONo,wo.product,wo.Quantity,wo.Bags,wo.Rate,wo.Amount,customer.customerName,customer.distance kms,wo.WhtApplied  from tripnumbergenerator tng inner join workorder wo on tng.TripNumber=wo.tripNo inner join tblvehicle v on tng.VehicleId=v.id inner join tbl_customer customer on wo.CustomerId=customer.customercode order by wo.CreationDate,wo.id, tng.tripNumber";
    }
    if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
    {
        $_SESSION['startDate']=$_REQUEST['startDate'];
        $_SESSION['endDate']=$_REQUEST['endDate'];
        $_SESSION['sqlValue']=$sql;
    }else
    {
        session_unset();
        $_SESSION['sqlValue']=$sql;
    }

    session_write_close();
   // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
    foreach($tire->view_query($sql) as $row)
    {
        $IncomeAmount=$IncomeAmount+$row['Amount'];
        $grandIncome=$grandIncome+$IncomeAmount;
        $grandQty=$grandQty+$row['Quantity'];
        $grandBags=$grandBags+$row['Bags'];
        $grandKms=$grandKms+$row['kms'];

        $tb->setTableData($row['date'],0,sizeof($array));
        $tb->setTableData($row['TripNumber'],1,sizeof($array));
        $tb->setTableData($row['vehNo'],2,sizeof($array));
        $tb->setTableData($row['DeliveryNo'],3,sizeof($array));
        $tb->setTableData($row['PONo'],4,sizeof($array));
        $tb->setTableData($row['product'],5,sizeof($array));
        $tb->setTableData(number_format($row['Quantity']),6,sizeof($array));
        $tb->setTableData(number_format($row['Bags']),7,sizeof($array));
        $tb->setTableData(number_format($row['Rate']),8,sizeof($array));
        $tb->setTableData(number_format($row['Amount']),9,sizeof($array));
        $tb->setTableData($row['customerName'],10,sizeof($array));
        $tb->setTableData($row['kms'],11,sizeof($array));
        $fuelOrderItem->findTripFuelDetail($row['TripNumber']);
        $tripOtherExpense->findMileageCost($row['TripNumber']);
        $tripOtherExpense2->findOtherCost($row['TripNumber']);

        //get total
        $grandLitres=$grandLitres+intval($fuelOrderItem->getlitres());
        $grandFuelAmount=$grandFuelAmount+intval($fuelOrderItem->getisActive());
        $grandMlgeAmount=$grandMlgeAmount+intval($tripOtherExpense->getAmount());
        $grandOther=$grandOther+intval($tripOtherExpense2->getAmount());
        //
        $ExpenseTotal=intval($fuelOrderItem->getisActive())+intval($tripOtherExpense->getAmount())+intval($tripOtherExpense2->getAmount())+round(($row['Amount']*0.06));
        if(intval($fuelOrderItem->getlitres())>0)
        {
            $tb->setTableData(number_format($fuelOrderItem->getlitres()),12,sizeof($array));
        }else{
            $tb->setTableData($fuelOrderItem->getlitres(),12,sizeof($array));
        }
        if(intval($fuelOrderItem->getrate())>0)
        {
            $tb->setTableData(number_format($fuelOrderItem->getrate()),13,sizeof($array));
        }else{
            $tb->setTableData($fuelOrderItem->getrate(),13,sizeof($array));
        }
        if(intval($fuelOrderItem->getisActive())>0)
        {
            $tb->setTableData(number_format($fuelOrderItem->getisActive()),14,sizeof($array));
        }else{
            $tb->setTableData($fuelOrderItem->getisActive(),14,sizeof($array));
        }
        if(intval($tripOtherExpense->getAmount())>0)
        {
            $tb->setTableData(number_format(($tripOtherExpense->getAmount())),15,sizeof($array));
        }else{
            $tb->setTableData($tripOtherExpense->getAmount(),15,sizeof($array));
        }
        if(intval($tripOtherExpense2->getAmount())>0)
        {
            $tb->setTableData(number_format($tripOtherExpense2->getAmount()),16,sizeof($array));
        }else{
            $tb->setTableData($tripOtherExpense2->getAmount(),16,sizeof($array));
        }






        if($row['WhtApplied']==1)
        {
            $tb->setTableData(number_format($row['Amount']*0.06),17,sizeof($array));
            $grandWht=$grandWht+round($row['Amount']*0.06);
        }else
        {
            $tb->setTableData('-',17,sizeof($array));
        }
        $tb->setTableData(number_format($ExpenseTotal),18,sizeof($array));
        $tb->setTableData(round(($ExpenseTotal/$row['Amount'])*100,2),19,sizeof($array));
        $tb->setTableData(number_format($row['Amount']-$ExpenseTotal),20,sizeof($array));
        $grandExpense=$grandExpense+$ExpenseTotal;
        $Balance=$row['Amount']-$ExpenseTotal;
        $AccBalance=$AccBalance+$Balance;
        $tb->setTableData(number_format($AccBalance),21,sizeof($array));

        
       
        
    }
    //$tire
    $tb->setTableFooter("Total",0,sizeof($array));
     for($i=1;$i<6;$i++)
     {
        $tb->setTableFooter("",$i,sizeof($array));
     }
    $tb->setTableFooter(number_format($grandQty,0),6,sizeof($array));
    $tb->setTableFooter(number_format($grandBags,0),7,sizeof($array));
    $tb->setTableFooter('-',8,sizeof($array));
    $tb->setTableFooter(number_format($IncomeAmount,0),9,sizeof($array));
    $tb->setTableFooter('-',10,sizeof($array));
    $tb->setTableFooter(number_format($grandKms),11,sizeof($array));
    $tb->setTableFooter(number_format($grandLitres,0),12,sizeof($array));
    $tb->setTableFooter('-',13,sizeof($array));
    $tb->setTableFooter(number_format($grandFuelAmount,0),14,sizeof($array));
    $tb->setTableFooter(number_format($grandMlgeAmount,0),15,sizeof($array));
    $tb->setTableFooter('-',16,sizeof($array));
    $tb->setTableFooter(number_format($grandWht,0),17,sizeof($array));
    $tb->setTableFooter(number_format($grandExpense,0),18,sizeof($array));
    if($grandExpense>0)
    {
        $tb->setTableFooter(round(($grandExpense/$IncomeAmount)*100,2),19,sizeof($array));
    }else{
        $tb->setTableFooter('-',19,sizeof($array));
    }

    $tb->setTableFooter('-',20,sizeof($array));
//$tb->setTableFooter("-",21,sizeof($array));

    $tb->setTableFooter(number_format($AccBalance),20,sizeof($array));
    if($grandKms>0)
    {
        $str="<table class='table table-bordered'><tr><th>Description</th><th>Per Km(Approximation)</th><th>% of Income</th></tr><tr><th>Distance</th><th>".number_format($grandKms,2)."</th><th></th></tr><tr><th>Revenue Per Km</th><th>".number_format(round(($IncomeAmount/$grandKms),2),2)."</th><th></th></tr><tr><th>Fuel Per Km</th><th>".number_format(round(($grandFuelAmount/$grandKms),2),2)."</th><th>".round((($grandFuelAmount/$grandKms)/($IncomeAmount/$grandKms))*100,2)."</th></tr><tr><th>Mileage Per Km</th><th>".number_format(round(($grandMlgeAmount/$grandKms),2),2)."</th><th>".round((($grandMlgeAmount/$grandKms)/($IncomeAmount/$grandKms))*100,2)."</th></tr><tr><th>Profit Per Km</th><th>".number_format(round(($AccBalance/$grandKms),2),2)."</th><th>".round((($AccBalance/$grandKms)/($IncomeAmount/$grandKms))*100,2)."</th></tr></table>";

    }else
    {
        $str="<table class='table table-bordered'><tr><th>Description</th><th>Per Km(Approximation)</th><th>% of Income</th></tr><tr><th>Distance</th><th>".number_format($grandKms,2)."</th><th></th></tr><tr><th>Revenue Per Km</th><th></th><th></th></tr><tr><th>Fuel Per Km</th><th></th><th></th></tr><tr><th>Mileage Per Km</th><th></th><th></th></tr><tr><th>Profit Per Km</th><th></th><th></th></tr></table>";

    }

     $tb->setCaption($str);
       
    $rb=new ReportBuilder($tb);
    
    echo $rb->getReport();
    


?>