<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
ini_set('memory_limit', '1024M');

require_once 'pdfController/tableTemplate.php';
require_once 'pdfController/report.php';
require_once 'pdfController/pdfReportStructure.php';
require_once('../services/invoiceItemService.php');
require_once('../services/debitorService.php');
require_once('../controller/utility.php');
//require_once('../services/tireHoldService.php');
//require_once('../reportPluggin/intial.php');
    try{
        $total=0;
        $subTotal=0;
        $vattotal=0;
        $invoiceItemCount=0;

        $tt=new TableTemplate();
        $tb=new myTable($tt);

        $array=array("PARTICULARS","QTY","RATE","AMOUNT");
        $tb->setCaption("<h4 align='left' style='font-weight:bold'>TIN:1007610963</h4>");
        $tb->setTableHeader($array);

        $invoiceItem=new invoiceItemService();
        $sql="";
        $invoiceId=$_REQUEST['id'];
       $sql= "select * from  invoiceitem where invoiceId='$invoiceId'";
       // session_write_close();
        // $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
        foreach($invoiceItem->view_query($sql) as $row)
        {
            $total=($row['quantity']*$row['unitPrice']);
            $subtotal=$subtotal+($row['quantity']*$row['unitPrice']);
            if($row['taxed']=='1')
            {
                $vattotal=$vattotal+(($row['quantity']*$row['unitPrice'])*0.18);
            }
            $row['total']=number_format($total,2);
            $row['unitPrice']=number_format($row['unitPrice'],2);

            //$row['quantity']=number_format($row['quantity'],2);

           $tb->setTableData(ucfirst($row['itemDescrpiton']),0,sizeof($array));
            if(intval($row['quantity'])>1)
            {
                $tb->setTableData($row['quantity'],1,sizeof($array));
                $tb->setTableData($row['unitPrice'],2,sizeof($array));
            }else{
                $tb->setTableData("",1,sizeof($array));
                $tb->setTableData("",2,sizeof($array));
            }


           $tb->setTableData(number_format($total),3,sizeof($array));
            $invoiceItemCount=$invoiceItemCount+1;
//        $tb->setTableData("",4,sizeof($array));
//        $tb->setTableData(number_format($row['SparePartCost'],0),5,sizeof($array));
//        $tb->setTableData(number_format($row['labourCost'],0),6,sizeof($array));
//        $tb->setTableData(number_format(($row['SparePartCost']+$row['labourCost']),0),7,sizeof($array));




        }
        if($invoiceItemCount<7)
        {
            for($i=0;$i<(7-$invoiceItemCount);$i++)
            {
                $tb->setTableData('.',0,sizeof($array));
                $tb->setTableData('',1,sizeof($array));
                $tb->setTableData('',2,sizeof($array));
                $tb->setTableData('',3,sizeof($array));
            }
        }

        //$tire
        $tb->setTableFooter('E&O.E',0,sizeof($array),'item4');
        $tb->setTableFooter('',1,sizeof($array),'item4');
        $tb->setTableFooter('SubTotal',2,sizeof($array),'item3');
        $tb->setTableFooter(number_format($subtotal),3,sizeof($array));
        $tb->setTableFooter('Accounts are due to demand',0,sizeof($array),'item4');
        $tb->setTableFooter('',1,sizeof($array),'item4');
        $tb->setTableFooter('VAT 18%',2,sizeof($array),'item3');
        if($vattotal>0)
        {
            $tb->setTableFooter(number_format($vattotal),3,sizeof($array));
        }else
        {
            $tb->setTableFooter('NIL',3,sizeof($array));
        }


        $tb->setTableFooter('',0,sizeof($array),'item4');
        $tb->setTableFooter('',1,sizeof($array),'item4');
        $tb->setTableFooter('Grand Total',2,sizeof($array),'item3');
        $tb->setTableFooter(number_format($subtotal+$vattotal),3,sizeof($array));


        //$tb->setTableFooter(number_format($totalCost,0),7,sizeof($array));
        $sqlHeader="SELECT inv.*,cur.id,deb.* FROM invoice inv inner join debitor deb on inv.customerId=deb.id inner join tblcurrency cur on inv.currencyId=cur.id where inv.id='$invoiceId'";

        $note="<p style='padding-top: 20px;'>Amount in figures: <u> ".ucfirst(utility::convert_number_to_words($subtotal+$vattotal))." Shilling Only</u></p>";
        $note.="<br/><br><p style='text-align:right;'>Prepared By: <u>".$_SESSION['username']."</u> &nbsp;</p><p style='text-align:right; padding-top:10px;'>Signature :........................</p><p style='text-align:right;'>On behalf of <br/>Pathmark Logistic Limited</p>";

        $tb->setNotes($note);
        $str="";
        foreach($invoiceItem->view_query($sqlHeader) as $rowHeader)
        {
            if($_REQUEST['confirmed']==='1')
            {
                $str.= "<h2 align='center' ><span style='border:1px solid #0e0b5c;border-radius:5px;padding:3px;'>TAX INVOICE</span></h2>";
            }else
            {
                $str.= "<h2 align='center' ><span style='border:1px solid #0e0b5c;border-radius:5px;padding:3px;'>PROFORMA INVOICE".$rowHeader['confirmed']."</span></h2>";
            }
            $str.="<table border='0' cellpadding='0' cellspacing='0' width='100%' style='margin-left:50%;margin-bottom:10px;'><tr ><td>INVOICE NO</td><td style='color:red;'>".$invoiceId."</td></tr>";
            $str.="<tr><td>DATE</td><td>".$rowHeader['creationDate']."</td></tr>";
            $str.="<tr><td></td><td></td></tr></table>";
            $debitor=new debitorService();
            $debitor->setid($rowHeader['customerId']);
            $str.="<table border='1' cellpadding='0' width='100%' cellspacing='0' style='margin-bottom:10px;'><tr><td style='padding:5px;'><u>Bill To</u></td></t></tr>";
            $str.="<tr><td style='padding:5px;'>".$debitor->formatAddress()."</td></tr></table>";


        }
        //$str="";
        $tb->setCaption($str);
       
        $rp=new Report($tb);
        $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();
    
    
    
    
    }catch(Exception $exc)
    {
        
    }
//get the table;


