<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'pdfController/tableTemplate.php';
    require_once 'pdfController/report.php';
    require_once 'pdfController/pdfReportStructure.php';
    require_once('../services/tireService.php');
    require_once('../services/tireHoldService.php');
    
    $currentCost=0;
    $purchase=0;
    $okTires=0;
    $warningTire=0;
    $tireband=new tireService();
    $sqlOuter="Select * from tbltireband";
    
    $tt=new TableTemplate();
    $tb=new myTable($tt);
    $array=array("Brand","Quantity","Total/Km","CPM","Average CPM","Average Km","1 KM","1000 KM");
    $tb->setCaption("<h4 class='text-center' style='font-weight:bold'>MARKH INVESTMENTS COMPANY LIMITED</h4><h4 class='text-center'>Tire Brand Summary Report</h4>");
    $tb->setTableHeader($array);
    
    foreach($tireband->view_query($sqlOuter) as $row){
        $brand=$row['brand'];
        
        $sqlOuter2="Select count(id) noId from tbltire where tireBand='$brand'";
         foreach($tireband->view_query($sqlOuter2) as $row2)
         {
            $tb->setTableData($brand,0,sizeof($array));
            $tb->setTableData($row2['noId'],1,sizeof($array));
            //echo $brand."&nbsp;".$row2['noId']."&nbsp;&nbsp;";
         }
        // $sqlOuter3="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and t.tireBand='$brand' order by th.vehicleId,tt.id";      
        
        $sqlOuter3="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and t.tireBand='$brand' order by th.vehicleId,tt.id";      
        $kmsDone=0;
        $cpk=0;
        $i=0;
        foreach($tireband->view_query($sqlOuter3) as $row3)
         {
            //echo $row2['noId']."<br/>";
            $kmsDone=$kmsDone+$row3['kmsDone'];
            $i=$i+1;
            $cpk=$cpk+(($row3['cost']/$row3['expectedKms'])*$row3['kmsDone']);
            
         }
         if($cpk!=0){
        $tb->setTableData(number_format($kmsDone,2),2,sizeof($array));
        $tb->setTableData(number_format(round($cpk,2),2),3,sizeof($array));
        //$tb->setTableData($kmsDone,4,sizeof($array));
        $tb->setTableData(number_format(round(($cpk/$i),2),2),4,sizeof($array));
        $tb->setTableData(number_format(round(($kmsDone/$i),2),2),5,sizeof($array));
		$tb->setTableData(number_format(round(($cpk/$i)/($kmsDone/$i),2),2),6,sizeof($array));
        
        $tb->setTableData(number_format(round(((($cpk/$i)/($kmsDone/$i))*1000),2),2),6,sizeof($array));
        }else
        {
             $tb->setTableData(number_format($kmsDone,2),2,sizeof($array));
        $tb->setTableData(number_format(round($cpk,2),2),3,sizeof($array));
        //$tb->setTableData($kmsDone,4,sizeof($array));
        $tb->setTableData(number_format(round((0),2),2),4,sizeof($array));
        $tb->setTableData(number_format(round((0),2),2),5,sizeof($array));
		$tb->setTableData(number_format(round(0,2),2),6,sizeof($array));
        
        $tb->setTableData(number_format(round((((0))),2),2),6,sizeof($array));
            
        }
        
        //echo $kmsDone."&nbsp;&nbsp;".$cpk."&nbsp;&nbsp;".($cpk/$i)."&nbsp;&nbsp;".($kmsDone/$i)."<br/>";
         
    }
   $rp=new Report($tb);
   $htmlx=$rp->getReport();
    //echo $html;
    //echo htmlspecialchars($htmlx);
    define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");
    $pdfReportStructure=new PdfreportStructure();
    $pdfReportStructure->setPdfBody($htmlx);
    $pdfReportStructure->printOut();