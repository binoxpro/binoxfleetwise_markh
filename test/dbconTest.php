<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
require('../config/db/universalconnection.php');
require('../config/db/insertBuilder.php');
require('../config/db/updateBuilder.php');
require('../config/db/deleteBuilder.php');
require('../config/db/director.php');
#testing the sinletone pattern
$con=UniversalConnection::getInstance();

$builder=new InsertBuilder();
$builder->setTable("tbl_test");
$builder->addColumnAndData("class_name","test");
$builder->addColumnAndData("attribute_string","my the coder and programmer");

$con->setQuery(Director::buildSql($builder));
echo Director::buildSql($builder);
echo json_encode($con->execute_query2($builder->getValues()));

/*
$builder=new UpdateBuilder();
$builder->setTable("tbl_test");
$builder->setCriteria("where id=1");
$builder->addColumnAndData("class_name","change_class");

$con->setQuery(Director::buildSql($builder));
echo Director::buildSql($builder);
echo json_encode($con->insert());
*/
/*
$builder=new DeleteBuilder();
$builder->setTable("tbl_test");
$builder->setCriteria("where id=1");
$con->setQuery(Director::buildSql($builder));
echo Director::buildSql($builder);
echo "<br/>";
echo json_encode($con->execute_query());

*/


?>
</body>
</html>