<?php 
 require_once('../model/localpurchaseorderitem.php');
 class localpurchaseorderitemService extends localpurchaseorderitem{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbllopitem');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('prfItemId',parent::getprfItemId());
 		 	 $builder->addColumnAndData('qty',parent::getqty());
 		 	 $builder->addColumnAndData('rate',parent::getrate());
 		 	 $builder->addColumnAndData('currencyCode',parent::getcurrencyCode());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
             $builder->addColumnAndData('lopId',parent::getlopId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbllopitem');

 	 	 if(!is_null(parent::getprfItemId())){
$builder->addColumnAndData('prfItemId',parent::getprfItemId()); 
}

 	 	 if(!is_null(parent::getqty())){
$builder->addColumnAndData('qty',parent::getqty()); 
}

 	 	 if(!is_null(parent::getrate())){
$builder->addColumnAndData('rate',parent::getrate()); 
}

 	 	 if(!is_null(parent::getcurrencyCode())){
$builder->addColumnAndData('currencyCode',parent::getcurrencyCode()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate());
}

             if(!is_null(parent::getlopId())){
                 $builder->addColumnAndData('lopId',parent::getlopId());
             }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tbllopitem";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="SELECT li.*,itm.itemName partName,pi.quantity,pi.quantityIssued,(li.qty*li.rate) total,(select sum(pd.quantity) from purfdelivery pd where pd.purfItemId=li.prfItemId) qtyDelivery FROM `tbllopitem` li  inner join prfitem pi on li.prfItemId=pi.id inner join tblpartsused pus on pi.itemId=pus.id inner join items itm on pus.partusedId=itm.id where li.lopId='".parent::getlopId()."' ";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$total=0.00;
		$sql="SELECT li.*,itm.itemName partName,pi.quantity,pi.quantityIssued,(li.qty*li.rate) total,(select sum(pd.quantity) from purfdelivery pd where pd.purfItemId=li.prfItemId) qtyDelivery FROM `tbllopitem` li  inner join prfitem pi on li.prfItemId=pi.id inner join tblpartsused pus on pi.itemId=pus.id inner join items itm on pus.partusedId=itm.id where li.lopId='".parent::getlopId()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
         {
             $total=$total+$row['total'];
             $row['total']=number_format($row['total'],2);
             $row['rate']=number_format($row['rate'],2);
             $row['qty']=number_format($row['qty'],2);
			 array_push($data2,$row);
         }
         $data["rows"]=$data2;
         $data3=array();
		 $footer=array();
		 $footer['qty']="Total";
		 $footer['total']=number_format($total,2);
		 array_push($data3,$footer);
         $data['footer']=$data3;

		 return $data;
 	 }
 	 public function delete()
     {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbllopitem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
     {
	 	 $sql="select * from  tbllopitem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
            parent::setid($row['id']);
            parent::setprfItemId($row['prfItemId']);
            parent::setqty($row['qty']);
            parent::setrate($row['rate']);
            parent::setcurrencyCode($row['currencyCode']);
            parent::setisActive($row['isActive']);
            parent::setcreationDate($row['creationDate']);
        }
	 }

 	 public function view_query($sql)
     {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>