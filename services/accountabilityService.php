<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/accountability.php';
class AccountabilityService extends Accountability {
  
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() {
       
            
            $builder=new InsertBuilder();
            $builder->setTable("tbl_accountability");
            $builder->addColumnAndData("internalBudgetItemId", parent::getinternalBudgetItemId());
            $builder->addColumnAndData("Amount", parent::getAmount());
            $builder->addColumnAndData("Date", parent::getDate());
            $builder->addColumnAndData("parentId", parent::getparentId());
            
			
            $this->con->setQuery(Director::buildSql($builder));
            
            return $this->con->execute_query2($builder->getValues());
			
            
       
       // parent::save();
    }


	
    public function update() {
      	 	 $builder=new UpdateBuilder();
            $builder->setTable("tbl_accountability");
            $builder->addColumnAndData("internalinternalBudgetItemIdItemId", parent::getinternalinternalBudgetItemIdItemId());
            $builder->addColumnAndData("Amount", parent::getAmount());
            $builder->addColumnAndData("Date", parent::getDate());
            $builder->addColumnAndData("parentId", parent::getparentId());
            $builder->addColumnAndData("account_balance", parent::getAccount_balance());
			$builder->setCriteria("where internalinternalBudgetItemIdItemId='".parent::getinternalinternalBudgetItemIdItemId()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view_account_analysis_status() {
		$this->setAccount_status(NULL);
       $sql="select * from tbl_accountability where internalinternalBudgetItemIdItemId='".parent::getinternalinternalBudgetItemIdItemId()."'";
	   foreach($this->con->getResultSet($sql) as $row){
		   $this->setAccount_status($row['parentId']);
	   }
	   
    }
	public function view(){
	}
	public function view_data($id) {
       $sql="select jobID,Id,itemName,(qty*unitPrice*nodays) Amount,(select CASE WHEN sum( Amount)IS NULL then 0 else sum(Amount) end  from tbl_accountability where internalBudgetItemId=tbl_internalBudget.Id) ActualDeposit,((qty*unitPrice*nodays)-(select CASE WHEN sum( Amount)IS NULL then 0 else sum(Amount) end  from tbl_accountability where internalBudgetItemId=tbl_internalBudget.Id)) BalanceDeposit from tbl_internalBudget where jobID='$id'";
	   return $this->con->getResultSet($sql);
    }
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbl_accountability");
		$builder->setCriteria("where internalinternalBudgetItemIdItemId='".parent::getinternalinternalBudgetItemIdItemId()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        parent::view_query($sql);
    }
	public function activity_term(){
	}

}
