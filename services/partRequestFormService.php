<?php 
 require_once('../model/partRequestForm.php');
 class partRequestFormService extends partRequestForm{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('prf');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('section',parent::getsection());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('requestedBy',parent::getrequestedBy());
 		 	 $builder->addColumnAndData('requestDate',parent::getrequestDate());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('jobcardId',parent::getjobcardId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('prf');

 	 	 if(!is_null(parent::getsection())){
$builder->addColumnAndData('section',parent::getsection()); 
}

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getrequestedBy())){
$builder->addColumnAndData('requestedBy',parent::getrequestedBy()); 
}

 	 	 if(!is_null(parent::getrequestDate())){
$builder->addColumnAndData('requestDate',parent::getrequestDate()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getjobcardId())){
$builder->addColumnAndData('jobcardId',parent::getjobcardId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  prf";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  prf";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  prf limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('prf');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  prf  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setsection($row['section']);
	 	parent::setvehicleId($row['vehicleId']);
	 	parent::setrequestedBy($row['requestedBy']);
	 	parent::setrequestDate($row['requestDate']);
	 	parent::setstatus($row['status']);
	 	parent::setjobcardId($row['jobcardId']);
}	 	}

     public function findWithJobcard(){
         parent::setid(null);
         $sql="select * from  prf  where jobcardId='".parent::getjobcardId()."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setsection($row['section']);
             parent::setvehicleId($row['vehicleId']);
             parent::setrequestedBy($row['requestedBy']);
             parent::setrequestDate($row['requestDate']);
             parent::setstatus($row['status']);
             parent::setjobcardId($row['jobcardId']);
         }	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>