<?php 
 require_once('../model/startBalance.php');
 class startBalanceService extends startBalance{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('accountbalance');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
			 $builder->addColumnAndData('individualNo',parent::getIndividualNo());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
			 $sql="";
			 if(is_null(parent::getIndividualNo()))
			 {
				 $sql="select * from accountbalance where accountCode='".parent::getaccountCode()."'";

			 }else
			 {
				 $sql="select * from accountbalance where accountCode='".parent::getaccountCode()."' and individualNo='".parent::getIndividualNo()."'";
			 }

			 $this->con->setSelect_query($sql);
			 if($this->con->sqlCount()>0)
			 {
				$this->loadId();
				 return $this->update();
			 }else{
				 return $this->con->execute_query2($builder->getValues());
			 }

 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('accountbalance');

 	 	 if(!is_null(parent::getaccountCode()))
		 {
			$builder->addColumnAndData('accountCode',parent::getaccountCode());
		 }

 	 	 if(!is_null(parent::getamount()))
		 {
		   $builder->addColumnAndData('amount',parent::getamount());
		}

 	 	 if(!is_null(parent::getcreationDate()))
		 {
		$builder->addColumnAndData('creationDate',parent::getcreationDate());
		}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  accountbalance";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select ab.*,coa.accountName from  accountbalance ab inner join chartofaccounts coa on ab.accountCode=coa.accountCode";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select ab.*,coa.accountName,(Select ino.AccountName from individualaccounts ino where ino.Id=ab.individualNo) individualName from  accountbalance ab inner join chartofaccounts coa on ab.accountCode=coa.accountCode  limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('accountbalance');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  accountbalance  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setaccountCode($row['accountCode']);
	 	parent::setamount($row['amount']);
	 	parent::setcreationDate($row['creationDate']);
		}
	 }
	 public function loadId()
	 {
		 $sql="";

		 if(is_null(parent::getIndividualNo()))
		 {
			 $sql="select * from accountbalance where accountCode='".parent::getaccountCode()."'";

		 }else
		 {
			 $sql="select * from accountbalance where accountCode='".parent::getaccountCode()."' and individualNo='".parent::getIndividualNo()."'";
		 }

		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 //parent::setaccountCode($row['accountCode']);
			 //parent::setamount($row['amount']);
			 //parent::setcreationDate($row['creationDate']);
		 }
	 }

 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>