<?php
include_once '../model/truckStatus.php';
class TruckStatusService extends TruckStatus
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbltruckstatus");
		$builder->addColumnAndData("truckNo", parent::gettruckNo());
		$builder->addColumnAndData("truckCapacity", parent::gettruckCapacity());
		$builder->addColumnAndData("status",parent::getstatus());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbltruckstatus");
			$builder->addColumnAndData("truckNo", parent::gettruckNo());
            $builder->addColumnAndData("truckCapacity", parent::gettruckCapacity());
			$builder->addColumnAndData("status", parent::getstatus());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tbltruckstatus";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbltruckstatus");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
