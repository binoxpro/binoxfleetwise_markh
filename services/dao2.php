<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author plussoft
 */
include_once('config/db/universalconnection.php');
require('lib/builderplus/insertBuilder.php');
require('lib/builderplus/updateBuilder.php');
require('lib/builderplus/deleteBuilder.php');
require('lib/builderplus/director.php');
abstract class DAO{
    public $con=NULL;
    public function __construct() {
        $this->con=new UniversalConnection();
    }
    public abstract function save();
    public abstract function delete();
    public abstract function update();
    public abstract function view();
    public abstract function view_query($sql);



   
}
?>