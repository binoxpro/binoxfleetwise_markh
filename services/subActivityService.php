<?php 

 require_once('../model/subActivity.php');
 class subActivityService extends subActivity{
	 	 public function save()
          {
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblsubactivity');
	 	 $builder->addColumnAndData('id',parent::getid());
	 	 $builder->addColumnAndData('name',parent::getname());
	 	 $builder->addColumnAndData('link',parent::getlink());
	 	 $builder->addColumnAndData('activityId',parent::getactivityId());
	 	 $builder->addColumnAndData('isActive',parent::getisActive());
         $builder->addColumnAndData('orderIndex',parent::getOrderIndex());
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsubactivity');

 	 	 if(parent::getname()!=" "){
$builder->addColumnAndData('name',parent::getname()); }

 	 	 if(parent::getlink()!=" "){
$builder->addColumnAndData('link',parent::getlink()); }

 	 	 if(parent::getactivityId()!=" "){
$builder->addColumnAndData('activityId',parent::getactivityId()); }

 	 	 if(parent::getisActive()!=""){
$builder->addColumnAndData('isActive',parent::getisActive()); }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	$sql="select sa.*,a.name nameActivity from  tblsubactivity sa inner join tblactivity a on sa.activityId=a.id";
	 	return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsubactivity');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
	 
	 public  function viewSideMenu(){
		 $sql="select * from tblactivity where isActive='1'";
		 $str='<li><a href="admin.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>';
		 
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 //check role;
			 
			 
			 $str.= '<li class="treeview">';
                            $str.='<a href="#"><i class="fa fa-folder"></i> <span>'.$row['name'].'</span><i class="fa fa-angle-left pull-right"></i></a>';
                            
             $str.='<ul class="treeview-menu">';
			 $id=$row['id'];
			 $sqlInner="select * from  tblsubactivity where activityId='$id' and isActive='1'";
			 $strInner="";
			 foreach($this->con->getResultSet($sqlInner) as $rowInner)
			 {
				$strInner.='<li><a href="?'.$rowInner['link'].'"><i class="fa fa-angle-double-right"></i>'.$rowInner['name'].'</a></li>';
			 }
			 $strInner.='</ul></li>';
		 	$str.=$strInner;
		 }
		 return $str."</li>";
	 	//$sql="select sa.*,a.name nameActivity from  tblsubactivity sa inner join tblactivity a on sa.activityId=a.id";
	 	//return $this->con->getResultSet($sql);
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
     function getIndexValue()
     {
        $sql="select * from tblsubactivity where activityId='".parent::getactivityId()."'";
        $this->con->setSelect_query($sql);
        return ($this->con->sqlCount())+1;
     }
 }
?>