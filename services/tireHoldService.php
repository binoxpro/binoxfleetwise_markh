<?php 
 require_once('../model/tireHold.php');
 class tireHoldService extends tireHold{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltirehold');
	 	 //$builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('positionId',parent::getpositionId());
 		 	 $builder->addColumnAndData('tireId',parent::gettireId());
 		 	 $builder->addColumnAndData('fixingDate',parent::getfixingDate());
 		 	 $builder->addColumnAndData('fixingOdometer',parent::getfixingOdometer());
 		 	 $builder->addColumnAndData('expectedKms',parent::getexpectedKms());
 		 	 $builder->addColumnAndData('removalKms',parent::getremovalKms());
 		 	 $builder->addColumnAndData('actualKms',parent::getactualKms());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltirehold');

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getpositionId())){
$builder->addColumnAndData('positionId',parent::getpositionId()); 
}

 	 	 if(!is_null(parent::gettireId())){
$builder->addColumnAndData('tireId',parent::gettireId()); 
}

 	 	 if(!is_null(parent::getfixingDate())){
$builder->addColumnAndData('fixingDate',parent::getfixingDate()); 
}

 	 	 if(!is_null(parent::getfixingOdometer())){
$builder->addColumnAndData('fixingOdometer',parent::getfixingOdometer()); 
}

 	 	 if(!is_null(parent::getexpectedKms())){
$builder->addColumnAndData('expectedKms',parent::getexpectedKms()); 
}

 	 	 if(!is_null(parent::getremovalKms())){
$builder->addColumnAndData('removalKms',parent::getremovalKms()); 
}

 	 	 if(!is_null(parent::getactualKms())){
$builder->addColumnAndData('actualKms',parent::getactualKms()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
     	  $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
    	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    	   $offset = ($page-1)*$rows;
     	  
	 	 $sql="select *,th.id thid from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId order by th.vehicleId,tt.id";
	       
           $this->con->setSelect_query($sql);
             $data2=array();
             $data=array();
             $data['total']=$this->con->sqlCount();
             $sql="select *,th.id thid from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId order by th.vehicleId,tt.id limit $offset,$rows";
	       
           foreach($this->con->getResultSet($sql) as $row)
     {
        
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
 	 }
     
     public function viewMonitior(){
 	  //$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId inner join tbltire tr on tr.id=th.tireId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
	      //$sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1'  order by th.vehicleId,tt.id";
	       //return $this->con->getResultSet($sql);
           $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
    	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    	   $offset = ($page-1)*$rows;
     	  
	 	 $sql="select *,th.id thid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
	        $this->con->setSelect_query($sql);
             $data2=array();
             $data=array();
             $data['total']=$this->con->sqlCount();
           $sql="select *,th.id thid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id limit $offset,$rows";
	      
            foreach($this->con->getResultSet($sql) as $row)
     {
        
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
           //select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id
	       
 	 }
     
     public function viewMonitiorRotation(){
 	      $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
    	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    	   $offset = ($page-1)*$rows;
	 	 $sql="select *,th.id thid,t.id tid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex,(select count(id) from tbltirerotation where tireId=t.id) rotationCycle from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
	       $this->con->setSelect_query($sql);
             $data2=array();
             $data=array();
             $data['total']=$this->con->sqlCount();
           $sql="select *,th.id thid,t.id tid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where (tih.vehicleId=th.vehicleId and tireHoldId=th.id) and tih.isActive='1' ) pressurex,(select count(id) from tbltirerotation where tireId=t.id) rotationCycle from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id limit $offset,$rows";
	       foreach($this->con->getResultSet($sql) as $row)
             {
                
                array_push($data2,$row);
             }
             $data['rows']=$data2;
             return $data;
 	 }
     
     
     
     public function viewMonitiorVehicle($id){
           $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
    	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    	   $offset = ($page-1)*$rows;
     	  
 	  
	 	 $sql="select *,th.id thid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and th.vehicleId='$id' order by th.vehicleId,tt.id limit $offset,$rows";
	       $this->con->setSelect_query($sql);
             $data2=array();
             $data=array();
             $data['total']=$this->con->sqlCount();
           $sql="select *,th.id thid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and th.vehicleId='$id' order by th.vehicleId,tt.id";
	      
           
           foreach($this->con->getResultSet($sql) as $row)
     {
        
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
 	 }
     
      public function viewMonitiorRotationVehicle($id){
 	  
	 	 $sql="select *,th.id thid,t.id tid,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select tih.id from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') tihid,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex,(select count(id) from tbltirerotation where tireId=t.id) rotationCycle from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' and th.vehicleId='$id' order by th.vehicleId,tt.id";
	       return $this->con->getResultSet($sql);
 	 }
     

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltirehold');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltirehold where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

        parent::setid($row["id"]);
 	 	 parent::setvehicleId($row["vehicleId"]); 


 	 	 parent::setpositionId($row["positionId"]); 


 	 	 parent::settireId($row["tireId"]); 


 	 	 parent::setfixingDate($row["fixingDate"]); 


 	 	 parent::setfixingOdometer($row["fixingOdometer"]); 


 	 	 parent::setexpectedKms($row["expectedKms"]); 


 	 	 parent::setremovalKms($row["removalKms"]); 


 	 	 parent::setactualKms($row["actualKms"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 


 	 	 parent::setcomment($row["comment"]); 


 	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 
 
 public function getObjectExistanceTireOn(){
	 	 $sql="select * from  tbltirehold where vehicleId='".parent::getvehicleId()."' and positionId='".parent::getpositionId()."' and isActive='1'";
	 	 foreach($this->con->getResultSet($sql) as $row){
	 	         parent::setid($row["id"]);

         	 	 parent::setvehicleId($row["vehicleId"]); 
        
        
         	 	 parent::setpositionId($row["positionId"]); 
        
        
         	 	 parent::settireId($row["tireId"]); 
        
        
         	 	 parent::setfixingDate($row["fixingDate"]); 
        
        
         	 	 parent::setfixingOdometer($row["fixingOdometer"]); 
        
        
         	 	 parent::setexpectedKms($row["expectedKms"]); 
        
        
         	 	 parent::setremovalKms($row["removalKms"]); 
        
        
         	 	 parent::setactualKms($row["actualKms"]); 
        
        
         	 	 parent::setcreationDate($row["creationDate"]); 
        
        
         	 	 parent::setcomment($row["comment"]); 
        
        
         	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 
 public function getObjectTireDistance(){
	 	 $sql="select * from  tbltirehold where tireId='".parent::gettireId()."'";
         $distanceE=0.0;
         $distance=0.0;
	 	 foreach($this->con->getResultSet($sql) as $row){
         	 	$distance=(floatval($row["removalKms"])-floatval($row["fixingOdometer"])); 
	 	         $distanceE=(floatval($row["expectedKms"])-$distance); 
	 	} 
        return $distanceE;
 }
 
 public function getObjectTireDistance2($currentReading){
	 	 $sql="select * from  tbltirehold where tireId='".parent::gettireId()."'";
         $distanceE=0.0;
         $distance=0.0;
	 	 foreach($this->con->getResultSet($sql) as $row){
	 	         $vehicleId=$row['vehicleId'];
                 $currentReading=$this->setCurrentReadingOn($vehicleId);
                 $removalKms=$row['removalKms'];
                 $expectedKms=$row['expectedKms'];
                 $fittingKms=$row['fixingOdometer'];
	 	         if($row['isActive']==0)
                {
             	 	$distance=$distance+(floatval($row["removalKms"])-floatval($row["fixingOdometer"])); 
                    //$distanceE=(floatval($row["expectedKms"])-$distance); 
                }else
                {
                   $distance=$distance+(floatval($row["fixingOdometer"])-$currentReading);
                }
	 	} 
        return $distance;
 }
 
 
 public function setCurrentReadingOn($vid) 
 {
       $sql="SELECT reading from tblodometer where vehicleId='$vid'";
	   $currentReading=0.0;
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $currentReading=floatval($row['reading']);
	   }
	   return $currentReading;
 }
	 public function getCostPerKm()
	 {
		 $costPerKm=0.00;
		 $sql="SELECT sum(tr.cost/tr.kms) costperkm FROM tbltirehold th inner JOIN tbltire tr on th.tireId=tr.id where th.isActive='1'";
		 foreach( $this->con->getResultSet($sql) as $row)
		 {
			 if(floatval($row['costperkm'])>0)
			 {
				 $costPerKm=floatval($row['costperkm']);
			 }


		 }
		 return  $costPerKm;
	 }

	 public function getCostPerKmForVehicle($vehicleId)
	 {
		 $costPerKm=0;
		 $sql="SELECT sum(tr.cost/tr.kms) costperkm FROM tbltirehold th inner JOIN tbltire tr on th.tireId=tr.id where th.vehicleId='$vehicleId' and th.isActive='1'";
		 foreach( $this->con->getResultSet($sql) as $row)
		 {
			 if(floatval($row['costperkm'])>0)
			 {
				 $costPerKm=$row['costperkm'];
			 }


		 }
		 return  $costPerKm;
	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>