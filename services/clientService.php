<?php 
 require_once('../model/client.php');
 class clientService extends client{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblclient');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('companyName',parent::getcompanyName());
 		 	 $builder->addColumnAndData('street',parent::getstreet());
 		 	 $builder->addColumnAndData('PostalAddress',parent::getPostalAddress());
 		 	 $builder->addColumnAndData('City',parent::getCity());
 		 	 $builder->addColumnAndData('Phone',parent::getPhone());
 		 	 $builder->addColumnAndData('Email',parent::getEmail());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblclient');

 	 	 if(!is_null(parent::getcompanyName())){
$builder->addColumnAndData('companyName',parent::getcompanyName()); 
}

 	 	 if(!is_null(parent::getstreet())){
$builder->addColumnAndData('street',parent::getstreet()); 
}

 	 	 if(!is_null(parent::getPostalAddress())){
$builder->addColumnAndData('PostalAddress',parent::getPostalAddress()); 
}

 	 	 if(!is_null(parent::getCity())){
$builder->addColumnAndData('City',parent::getCity()); 
}

 	 	 if(!is_null(parent::getPhone())){
$builder->addColumnAndData('Phone',parent::getPhone()); 
}

 	 	 if(!is_null(parent::getEmail())){
$builder->addColumnAndData('Email',parent::getEmail()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblclient";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblclient');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>