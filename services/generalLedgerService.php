<?php 
	 require_once('../model/generalLedger.php');
	 require_once('accountTypeService.php');
	require_once('vehicleNewService.php');
 class generalLedgerService extends generalLedger{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('generaledger');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('ledgerHeadId',parent::getledgerHeadId());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
 		 	 $builder->addColumnAndData('narrative',parent::getnarrative());
 		 	 $builder->addColumnAndData('reference',parent::getreference());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('transactionType',parent::gettransactionType());
 		 	 $builder->addColumnAndData('costCentreId',parent::getcostCentreId());
 		 	 $builder->addColumnAndData('individualNo',parent::getindividualNo());
 		 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('generaledger');

 	 	 if(!is_null(parent::getledgerHeadId())){
$builder->addColumnAndData('ledgerHeadId',parent::getledgerHeadId()); 
}

 	 	 if(!is_null(parent::getaccountCode())){
$builder->addColumnAndData('accountCode',parent::getaccountCode()); 
}

 	 	 if(!is_null(parent::getnarrative())){
$builder->addColumnAndData('narrative',parent::getnarrative()); 
}

 	 	 if(!is_null(parent::getreference())){
$builder->addColumnAndData('reference',parent::getreference()); 
}

 	 	 if(!is_null(parent::getamount())){
$builder->addColumnAndData('amount',parent::getamount()); 
}

 	 	 if(!is_null(parent::gettransactionType())){
$builder->addColumnAndData('transactionType',parent::gettransactionType()); 
}

 	 	 if(!is_null(parent::getcostCentreId())){
$builder->addColumnAndData('costCentreId',parent::getcostCentreId()); 
}

 	 	 if(!is_null(parent::getindividualNo())){
$builder->addColumnAndData('individualNo',parent::getindividualNo()); 
}

 	 	 if(!is_null(parent::gettripNo())){
$builder->addColumnAndData('tripNo',parent::gettripNo()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  generaledger";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  generaledger";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  generaledger limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('generaledger');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  generaledger  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
			parent::setid($row['id']);
			parent::setledgerHeadId($row['ledgerHeadId']);
			parent::setaccountCode($row['accountCode']);
			parent::setnarrative($row['narrative']);
			parent::setreference($row['reference']);
			parent::setamount($row['amount']);
			parent::settransactionType($row['transactionType']);
			parent::setcostCentreId($row['costCentreId']);
			parent::setindividualNo($row['individualNo']);
			parent::settripNo($row['tripNo']);
			parent::setisActive($row['isActive']);
		}
	 }

	 public function findDebit($ld){
		 $sql="select * from  generaledger  where ledgerHeadId='$ld' and transactionType='Dr' ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 parent::setledgerHeadId($row['ledgerHeadId']);
			 parent::setaccountCode($row['accountCode']);
			 parent::setnarrative($row['narrative']);
			 parent::setreference($row['reference']);
			 parent::setamount($row['amount']);
			 parent::settransactionType($row['transactionType']);
			 parent::setcostCentreId($row['costCentreId']);
			 parent::setindividualNo($row['individualNo']);
			 parent::settripNo($row['tripNo']);
			 parent::setisActive($row['isActive']);
		 }
	 }

	 public function getStatement($startDate,$endDate)
	 {
		 $data=array();
		 $data2=array();
		 $data2['narrative']='Balance Brought Foreward';
		 // $openDate='2016-01-01';
		 $crAmount=0;
		 $drAmount=0;
		 $balance=0;
		 $balanceType='NA';
		 $accountType=new accountTypeService();
		 $vehicle=new vehicleNewService();
		 $balance=$this->getAccountBalanceBeforeForIndividual($startDate);
		 $data2['balance']=number_format($balance,0);
		array_push($data,$data2);
		 $sql="select gl.*,lh.transactionDate date,coa.AccountTypeCode from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id inner join chartofaccounts coa on gl.accountCode=coa.AccountCode  where  gl.accountCode='".parent::getaccountCode()."'and gl.individualNo='".parent::getindividualNo()."' and lh.transactionDate between '$startDate' and '$endDate' order by lh.transactionDate";
		 foreach($this->con->getResultSet($sql) as $row)
		 {

			 $accountCode=$row['AccountTypeCode'];
			 $accountType->setPrefixCode($accountCode);
			 $accountType->find();
			 $balanceType=$accountType->getAccountBalanceType();
			 if($balanceType==='Dr')
			 {
				 if ($row['transactionType'] == 'Dr')
				 {
					 $balance=$balance+$row['amount'];
					 $row['Dr']=number_format($row['amount'],2);
					 $row['Cr']="";
				 } else if ($row['transactionType'] == 'Cr')
				 {
					 $balance=$balance-$row['amount'];
					 $row['Dr']='';
					 $row['Cr']=number_format($row['amount'],2);

				 }

			 }else if($balanceType=='Cr')
			 {
				 if ($row['transactionType'] == 'Dr')
				 {
					 $balance=$balance-$row['amount'];
					 $row['Dr']=number_format($row['amount'],2);
					 $row['Cr']='';
				 } else if ($row['transactionType'] == 'Cr')
				 {
					 $balance=$balance+$row['amount'];
					 $row['Dr']='';
					 $row['Cr']=number_format($row['amount'],2);
				 }

			 }
			 $row['balance']=number_format($balance,0);
			 $row['narrative']=$row['narrative'];
			 $this->findDebit($row['ledgerHeadId']);

			 if(!is_null($this->getcostCentreId()) || $this->getcostCentreId()!="" )
			 {
				 $vehicle->setid($this->getcostCentreId());
				 $vehicle->find();
				 $row['narrative']=$row['narrative']." for ".$vehicle->getregNo();
			 }
			 array_push($data,$row);


		 }
		 return $data;

	 }

	 public function getAccountBalance($startDate,$endDate)
	 {
		// $openDate='2016-01-01';
		 $crAmount=0;
		 $drAmount=0;
		 $balance=0;
		 $balanceType='NA';
		 $accountType='';
		 $sql="select gl.*,coa.AccountTypeCode from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id  where gl.accountCode='".parent::getaccountCode()."'and gl.individualNo='".parent::getindividualNo()."' and lh.transactionDate btn '$startDate' and '$endDate' ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {

			 $accountCode=$row['AccountTypeCode'];
			 $accountType->setPrefixCode($accountCode);
			 $accountType->find();
			 $balanceType=$accountType->getAccountBalanceType();
			 if($balanceType==='Dr')
			 {
				 if ($row['transactionType'] == 'Dr')
				 {
					 $balance=$balance+$row['amount'];
					 $row['Dr']=number_format($row['amount'],2);
					 $row['Cr']="";
				 } else if ($row['transactionType'] == 'Cr')
				 {
					 $balance=$balance-$row['amount'];
					 $row['Dr']='';
					 $row['Cr']=number_format($row['amount'],2);

				 }

			 }else if($balanceType==='Cr')
			 {
				 if ($row['transactionType'] == 'Dr')
				 {
					 $balance=$balance-$row['amount'];
					 $row['Dr']=number_format($row['amount'],2);
					 $row['Cr']='';
				 } else if ($row['transactionType'] == 'Cr')
				 {
					 $balance=$balance+$row['amount'];
					 $row['Dr']='';
					 $row['Cr']=number_format($row['amount'],2);
				 }

			 }

		 }


	 }
	 public function getAccountBalanceBefore($startDate)
	 {
		 // $openDate='2016-01-01';
		 $crAmount=0;
		 $drAmount=0;
		 $balance=0;
		 $balanceType='NA';
		 $accountType=new accountTypeService();
		 $sql="select gl.*,coa.AccountTypeCode from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id inner join chartofaccounts coa on gl.accountCode=coa.AccountCode  where gl.accountCode='".parent::getaccountCode()."' and lh.transactionDate < '$startDate' ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {

				 $accountCode=$row['AccountTypeCode'];
			 $accountType->setPrefixCode($accountCode);
			 $accountType->find();
			 $balanceType=$accountType->getAccountBalanceType();
			 if($balanceType==='Dr') {
				 if ($row['transactionType'] == 'Dr') {
							$balance=$balance+$row['amount'];
				 } else if ($row['transactionType'] == 'Cr') {
					 $balance=$balance-$row['amount'];
				 }
			 }else if($balanceType==='Cr'){
				 if ($row['transactionType'] == 'Dr') {
					 $balance=$balance-$row['amount'];
				 } else if ($row['transactionType'] == 'Cr') {
					 $balance=$balance+$row['amount'];
				 }
			 }
		 }
		 return $balance;

	 }
	 public function getAccountBalanceBeforeForIndividual($startDate)
	 {
		 // $openDate='2016-01-01';
		 $crAmount=0;
		 $drAmount=0;
		 $balance=0;
		 $balanceType='NA';
		 $accountType=new accountTypeService();
		 $sql="select gl.*,coa.AccountTypeCode from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id inner join chartofaccounts coa on gl.accountCode=coa.AccountCode  where gl.accountCode='".parent::getaccountCode()."' and gl.individualNo='".parent::getindividualNo()."' and lh.transactionDate < '$startDate' ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {

			 $accountCode=$row['AccountTypeCode'];
			 $accountType->setPrefixCode($accountCode);
			 $accountType->find();
			 $balanceType=$accountType->getAccountBalanceType();
			 if($balanceType==='Dr') {
				 if ($row['transactionType'] == 'Dr') {
					 $balance=$balance+$row['amount'];
				 } else if ($row['transactionType'] == 'Cr') {
					 $balance=$balance-$row['amount'];
				 }
			 }else if($balanceType==='Cr'){
				 if ($row['transactionType'] == 'Dr') {
					 $balance=$balance-$row['amount'];
				 } else if ($row['transactionType'] == 'Cr') {
					 $balance=$balance+$row['amount'];
				 }
			 }
		 }
		 return $balance;

	 }

	 public function getInvoiceBalance()
	 {
		 $creditBalance=0;
		 $debtorBalance=0;
		 $sql="select * from  generaledger  where accountCode='".parent::getaccountCode()."' and reference='".parent::getreference()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 parent::setledgerHeadId($row['ledgerHeadId']);
			 parent::setaccountCode($row['accountCode']);
			 parent::setnarrative($row['narrative']);
			 parent::setreference($row['reference']);
			 parent::setamount($row['amount']);
			 parent::settransactionType($row['transactionType']);
			 parent::setcostCentreId($row['costCentreId']);
			 parent::setindividualNo($row['individualNo']);
			 parent::settripNo($row['tripNo']);
			 parent::setisActive($row['isActive']);

			 if($row['transactionType']=="Dr")
			 {
				 $debtorBalance=$debtorBalance+$row['amount'];

			 }else
			 {
				$creditBalance=$creditBalance+$row['amount'];
			 }


		 }

		 return ($debtorBalance-$creditBalance);

	 }

	 public function getAccountPosting($startDate,$endDate)
	 {
			$amount=0;
		 	$sql="select sum(gl.amount) Amount from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id inner join chartofaccounts coa on gl.accountCode=coa.AccountCode  where gl.accountCode='".parent::getaccountCode()."' and gl.transactionType='Dr' and lh.transactionDate BETWEEN '$startDate' and '$endDate'  ";
			 foreach($this->con->getResultSet($sql) as $row)
			 {
				 if(floatval($row['Amount'])>0)
				 {
					 $amount=$row['Amount'];
				 }

			 }

			return $amount;
	 }

	 public function getAccountPostingVehicle($startDate,$endDate,$vehicleId)
	 {
		 $amount=0;
		 $sql="select sum(gl.amount) Amount from generaledger gl inner join ledgehead lh on gl.ledgerHeadId=lh.id inner join chartofaccounts coa on gl.accountCode=coa.AccountCode  where gl.costCentreId='$vehicleId' AND gl.accountCode='".parent::getaccountCode()."' and gl.transactionType='Dr'  and lh.transactionDate BETWEEN '$startDate' and '$endDate'  ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if(floatval($row['Amount'])>0)
			 {
				 $amount=$row['Amount'];
			 }

		 }

		 return $amount;
	 }


 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>