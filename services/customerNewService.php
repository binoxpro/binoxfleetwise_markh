<?php 
 require_once('../model/customerNew.php');
 class customerNewService extends customerNew{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbl_customer');
	 	// $builder->addColumnAndData('customercode',parent::getcustomercode());
		 $builder->addColumnAndData('customerName',parent::getcustomerName());
		 $builder->addColumnAndData('distance',parent::getdistance());
		 $builder->addColumnAndData('destination',parent::getdistance());
		 $this->con->setQuery(Director::buildSql($builder));
        $this->find(parent::getcustomerName(),parent::getdistance(),parent::getdistance());
        if(is_null(parent::getcustomercode())) {

             $msg=$this->con->execute_query2($builder->getValues());
            $this->find(parent::getcustomerName(),parent::getdistance(),parent::getdistance());
            return $msg;
        }else{
            return array('msg'=>'The customer exist in the database');
        }
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbl_customer');

 	 	 if(!is_null(parent::getcustomerName())){
$builder->addColumnAndData('customerName',parent::getcustomerName()); 
}

 	 	 if(!is_null(parent::getdistance()))
		 {
		$builder->addColumnAndData('distance',parent::getdistance());
		}
		 if(!is_null(parent::getdestination()))
		 {
			 $builder->addColumnAndData('destination',parent::getdestination());
		 }
		$builder->setCriteria("where customercode='".parent::getcustomercode()."'");
		$this->con->setQuery(Director::buildSql($builder));
	 	return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbl_customer";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbl_customer');
	 	 $builder->setCriteria("where customercode='".parent::getcustomercode()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbl_customer where customercode='".parent::getcustomercode()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setcustomerName($row["customerName"]); 


 	 	 parent::setdistance($row["distance"]); 

	 	} 
 }


     public function find($cn,$d,$dis){
         $sql="select * from  tbl_customer where customerName='".$cn."' and distance='".$d."' and destination='".$dis."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setcustomercode($row['customercode']);
             parent::setcustomerName($row["customerName"]);
             parent::setdistance($row["distance"]);
             parent::setdestination($row['destination']);

         }
     }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>