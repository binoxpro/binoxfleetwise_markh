<?php 
 require_once('../model/invoiceItem.php');
 class invoiceItemService extends invoiceItem{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('invoiceitem');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('itemDescrpiton',parent::getitemDescrpiton());
 		 	 $builder->addColumnAndData('unitPrice',parent::getunitPrice());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('day',parent::getday());
 		 	 $builder->addColumnAndData('taxed',parent::gettaxed());
 		 	 $builder->addColumnAndData('invoiceId',parent::getinvoiceId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('invoiceitem');

 	 	 if(!is_null(parent::getitemDescrpiton())){
$builder->addColumnAndData('itemDescrpiton',parent::getitemDescrpiton()); 
}

 	 	 if(!is_null(parent::getunitPrice())){
$builder->addColumnAndData('unitPrice',parent::getunitPrice()); 
}

 	 	 if(!is_null(parent::getquantity())){
$builder->addColumnAndData('quantity',parent::getquantity()); 
}

 	 	 if(!is_null(parent::getday())){
$builder->addColumnAndData('day',parent::getday()); 
}

 	 	 if(!is_null(parent::gettaxed())){
$builder->addColumnAndData('taxed',parent::gettaxed()); 
}

 	 	 if(!is_null(parent::getinvoiceId())){
$builder->addColumnAndData('invoiceId',parent::getinvoiceId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  invoiceitem";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		 $subtotal=0;
		 $vattotal=0;

		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  invoiceitem where invoiceId='".parent::getinvoiceId()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  invoiceitem where invoiceId='".parent::getinvoiceId()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		{
			$total=0;
				$total=($row['quantity']*$row['unitPrice']);
			$subtotal=$subtotal+($row['quantity']*$row['unitPrice']);
			if($row['taxed']=='1')
			{
				$vattotal=$vattotal+(($row['quantity']*$row['unitPrice'])*0.18);
			}
			$row['total']=number_format($total,2);
			$row['unitPrice']=number_format($row['unitPrice'],2);
			$row['quantity']=number_format($row['quantity'],2);
		    array_push($data2,$row);
		}
		 $data["rows"]=$data2;

		 $data3=array();
		 $dataLine=array();
		 $dataLine['taxed']='SubTotal';
		 $dataLine['total']=number_format($subtotal,2);
		 array_push($data3,$dataLine);
		 $dataLine['taxed']='VAT 18%';
		 $dataLine['total']=$vattotal>0?number_format($vattotal,2):'NIL';
		 array_push($data3,$dataLine);
		 $dataLine['taxed']='Total';
		 $dataLine['total']=number_format(($subtotal+$vattotal),2);
		 array_push($data3,$dataLine);
		 $data['footer']=$data3;
		 return $data;
	 }
 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('invoiceitem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  invoiceitem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setitemDescrpiton($row['itemDescrpiton']);
	 	parent::setunitPrice($row['unitPrice']);
	 	parent::setquantity($row['quantity']);
	 	parent::setday($row['day']);
	 	parent::settaxed($row['taxed']);
	 	parent::setinvoiceId($row['invoiceId']);
		}
	 }

	 public function findTotalCost()
	 {
		 $data=array();
		 $subtotal=0;
		 $vatTotal=0;
		 $total=0;
		 $sql="select * from  invoiceitem  where invoiceId='".parent::getinvoiceId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $subtotal=$row['unitPrice']*$row['quantity'];
			 if($row['taxed']=='1')
			 {
				 $vatTotal=$subtotal*0.18;
			 }
			 $total=$total+$subtotal;

		 }
		 $data[]=$total;
		 $data[]=$vatTotal;
		 $data[]=$total+$vatTotal;
		 return $data;
	 }


 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>