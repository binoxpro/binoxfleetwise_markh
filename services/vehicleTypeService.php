<?php
include_once '../model/vehicleType.php';
class VehicleTypeService extends VehicleType
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblvehicletype");
		$builder->addColumnAndData("vehicleType", parent::getvehicleType());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblvehicletype");
			$builder->addColumnAndData("vehicleType", parent::getvehicleType());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblvehicletype";
	   $data=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["vehicleType"]=$row["vehicleType"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function setTypeId()
	{
		$sql="select * from tblvehicletype where vehicleType='".parent::getvehicleType()."'";
		//$data=array();
		foreach($this->con->getResultSet($sql) as $row)
		{
			//$data2=array();
			parent::setid($row["id"]);
			//$data2["vehicleType"]=$row["vehicleType"];
			//array_push($data,$data2);
		}
		//return $data;
	}
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblvehicletype");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
