<?php
include_once '../model/jobCard.php';
class jobCardService extends jobCard
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbljobcard");
		
		$builder->addColumnAndData("jobNo", parent::getjobNo());
		$builder->addColumnAndData("creationDate", parent::getcreationDate());
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		$builder->addColumnAndData("rectified", parent::getrectified());
		$builder->addColumnAndData("dateCompleted", parent::getdateCompleted());
		$builder->addColumnAndData("odometer", parent::getodometer());
		$builder->addColumnAndData("time", parent::gettime());
		$builder->addColumnAndData("checklistId", parent::getcheckListId());
		$builder->addColumnAndData("comment", parent::getcomment());
		$builder->addColumnAndData("garrage", parent::getgarrage());
		$builder->addColumnAndData("fuelLevel", parent::getfuelLevel());
		$builder->addColumnAndData("driverName", parent::getdriverName());
        $builder->addColumnAndData("createdBy", parent::getcreatedBy());
        $builder->addColumnAndData("approvedBy", parent::getapprovedBy());
        $builder->addColumnAndData("approvedDate", parent::getapprovedDate());
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query2($builder->getValues());
		 parent::setid($this->con->getId());
    }
	
	public function saveServiceJobCard($serviceId,$jobcardId) 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbljobcardservice");
		
		$builder->addColumnAndData("serviceId",$serviceId);
		$builder->addColumnAndData("jobcardId", $jobcardId);
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	 
		 //parent::setid($this->con->getId());
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbljobcard");
			if(parent::getjobNo()!=NULL){
			$builder->addColumnAndData("jobNo", parent::getjobNo());
			}
			if(parent::getcreationDate()!=NULL){
            $builder->addColumnAndData("date", parent::getcreationDate());
			}
			if(parent::getvehicleId()!=NULL){
            $builder->addColumnAndData("vehicleId", parent::getvehicleId());
			}
			if(parent::getrectified() !=NULL){
			$builder->addColumnAndData("rectified", parent::getrectified());
			}
			if(parent::getdateCompleted()!=NULL){
			$builder->addColumnAndData("dateCompleted", parent::getdateCompleted());
			}
			
			if(parent::getlabourCost()!=NULL){
			$builder->addColumnAndData("labourCost", parent::getlabourCost());
			}
			
			if(parent::getlabourHour()!=NULL){
			$builder->addColumnAndData("labourHour", parent::getlabourHour());
			}


			if(parent::getgarrage()!=NULL)
			{
				$builder->addColumnAndData("garrage", parent::getgarrage());
			}
			if(parent::getfuelLevel()!=NULL)
			{
				$builder->addColumnAndData("fuelLevel", parent::getfuelLevel());
			}

			if(parent::getdriverName()!=NULL)
			{
				$builder->addColumnAndData("driverName", parent::getdriverName());
			}

        if(parent::getcreatedBy()!=NULL)
        {
            $builder->addColumnAndData("createdBy", parent::getcreatedBy());
        }
        if(parent::getapprovedBy()!=NULL)
        {
            $builder->addColumnAndData("approvedBy", parent::getapprovedBy());
        }
        if(parent::getapprovedDate()!=NULL)
        {
            $builder->addColumnAndData("approvedDate", parent::getapprovedDate());
        }


			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tbljobcard";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbljobcard");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	public function find(){
		$sql="select * from  tbljobcard  where id='".parent::getid()."'";
		foreach($this->con->getResultSet($sql) as $row)
		{
			parent::setid($row['id']);
			parent::setjobNo($row['jobNo']);
			parent::setdate($row['creationDate']);
			parent::setvehicleId($row['vehicleId']);
			parent::setrectified($row['rectified']);
			parent::setdateCompleted($row['dateCompleted']);
			parent::setodometer($row['odometer']);
			parent::settime($row['time']);
			parent::setchecklistId($row['checklistId']);
			parent::setcomment($row['comment']);
			parent::setlabourCost($row['labourCost']);
			parent::setlabourHour($row['labourHour']);
			parent::setgarrage($row['garrage']);
			parent::setfuelLevel($row['fuelLevel']);
			parent::setdriverName($row['driverName']);
            parent::setapprovedBy($row['approvedBy']);
            parent::setcreatedBy($row['createdBy']);
            parent::setapprovedDate($row['approvedDate']);
		}

	}
	
	public function jobNoGeneration()
	{
		$sql="select * from tbljobcard";
		$this->con->setSelect_query($sql);
		$no=$this->con->sqlCount();
		$prefix="MARKH/";
		return $prefix.($no+1);
	}

	public function jobCardCosting($startDate,$endDate)
	{
		$mtcCost=0;
		$sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where  jc.creationDate BETWEEN '$startDate' and '$endDate' ";
		foreach($this->con->getResultSet($sql) as $row)
		{
			$mtcCost=$mtcCost+$row['SparePartCost']+$row['labourCost'];
		}
		parent::setLabourCost($mtcCost);
	}

	public function truckJobCardCosting($startDate,$endDate,$vehicleid)
	{
		$mtcCost=0;
		$sql="select jc.id,jc.jobNo,v.regNo,jc.creationDate,(select sum(pu.quantity*pu.cost) from tblpartsused pu where pu.jobcardId=jc.id and pu.supplied=1 ) SparePartCost,(select sum(jci.initials) from tbljobcarditem jci where jci.jobcardId=jc.id ) labourCost from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where  jc.vehicleId='$vehicleid' and   jc.creationDate BETWEEN '$startDate' and '$endDate' ";
		foreach($this->con->getResultSet($sql) as $row)
		{
			$mtcCost=$mtcCost+$row['SparePartCost']+$row['labourCost'];
		}
		parent::setLabourCost($mtcCost);
	}


	public function getSummaryWork($jobid)
	{

		$sql="SELECT * FROM `tbljobcarditem` where jobcardId='$jobid'";
		$str='<ul>';
		$i=0;
        foreach($this->con->getResultSet($sql) as $row)
        {
			$str.="<li> ".$row['details']."</li>";
			$i=$i+1;
        }

		$str.="</li>";
        if($i=0)
		{
			$str="";
		}
        return $str;
	}

    public function getSummaryWorkPending($jobid)
    {

        $sql="SELECT * FROM `tbljobcarditem` where jobcardId='$jobid' and status='pending'";
        $str='<ul>';
        $i=0;
        foreach($this->con->getResultSet($sql) as $row)
        {
            $str.="<li> ".$row['details']."</li>";
            $i=$i+1;
        }

        $str.="</li>";
        if($i=0)
        {
            $str="";
        }
        return $str;
    }

    public function getSummaryWorkDo($jobid)
    {

        $sql="SELECT * FROM `tbljobcarditem` where jobcardId='$jobid' and status='fixed'";
        $str='<ul>';
        $i=0;
        foreach($this->con->getResultSet($sql) as $row)
        {
            $str.="<li> ".$row['details']." (".$row['repairReview'].")</li>";
            $i=$i+1;
        }

        $str.="</li>";
        if($i=0)
        {
            $str="";
        }
        return $str;
    }

    public function getSummaryofmechanic($jobid)
    {

        $sql="SELECT * FROM `tbljobcarditem` where jobcardId='$jobid' and status='fixed'";
        $str='<ul>';
        $i=0;
        foreach($this->con->getResultSet($sql) as $row)
        {
            $str.="<li> ".$row['initials']."</li>";
            $i=$i+1;
        }

        $str.="</li>";
        if($i=0)
        {
            $str="No mechanic Assigned";
        }
        return $str;
    }

    public function getSummaryofpartRequested($jobid)
    {

        $sql="select it.itemName,pu.quantity,pi.quantityIssued,pi.status from prfitem pi inner join prf on pi.prfId=prf.jobcardId inner join tblpartsused pu on pi.itemId=pu.id inner join items it on pu.partusedId=it.id where prf.jobcardId='$jobid' ";
        $str='<ul>';
        $i=0;
        foreach($this->con->getResultSet($sql) as $row)
        {
            $str.="<li> ".$row['itemName']."(".$row['quantity'].")/<b>".$row['status']."</b></li>";
            $i=$i+1;
        }

        $str.="</li>";
        if($i=0)
        {
            $str="No Items Requested";
        }
        return $str;
    }




}

?>
