<?php 
 require_once('../model/chartOfaccount.php');
 class chartOfaccountService extends chartOfaccount{
	 private $data2=array();
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('chartofaccounts');
	 	 $builder->addColumnAndData('AccountCode',parent::getAccountCode());
 		 	 $builder->addColumnAndData('AccountName',parent::getAccountName());
 		 	 $builder->addColumnAndData('AccountTypeCode',parent::getAccountTypeCode());
 		 	 $builder->addColumnAndData('IsActive',parent::getIsActive());
			 if(!is_null(parent::getParentAccountCode())){
				 $builder->addColumnAndData('ParentAccountCode',parent::getParentAccountCode());
			 }
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('chartofaccounts');

 	 	 if(!is_null(parent::getAccountName())){
$builder->addColumnAndData('AccountName',parent::getAccountName()); 
}

 	 	 if(!is_null(parent::getAccountTypeCode())){
$builder->addColumnAndData('AccountTypeCode',parent::getAccountTypeCode()); 
}

 	 	 if(!is_null(parent::getIsActive())){
$builder->addColumnAndData('IsActive',parent::getIsActive()); 
}

 	 	 if(!is_null(parent::getParentAccountCode())){
$builder->addColumnAndData('ParentAccountCode',parent::getParentAccountCode()); 
}
$builder->setCriteria("where AccountCode='".parent::getAccountCode()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  chartofaccounts";
	 return $this->con->getResultSet($sql);
 	 }
	 public function viewConboxoftype(){
		 $sql="select * from  chartofaccounts where AccountTypeCode='".parent::getAccountTypeCode()."'";
		 return $this->con->getResultSet($sql);
	 }
	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select coa.* from  chartofaccounts coa where coa.ParentAccountCode=0 or coa.ParentAccountCode is NULL";
		$this->con->setSelect_query($sql);
		//$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select coa.*,(select ats.accountTypeName from accounttypes ats where ats.PrefixCode=coa.AccountTypeCode) accountTypeName from  chartofaccounts coa where coa.ParentAccountCode=0 or coa.ParentAccountCode is NULL  limit $offset,$rows";
		 foreach($this->con->getResultSet($sql) as $row)
		 {

			 array_push($this->data2,$row);
			 if($this->hasChildren($row['AccountCode']))
			 {
				 $this->viewTree($row['AccountCode']);
			 }
		 }
		 $data["rows"]=$this->data2;
		 return $data;
	 }

	 public function viewTree($parentId)
	 {
		 //$data=array();
		 $sql="select coa.*,(select ats.accountTypeName from accounttypes ats where ats.PrefixCode=coa.AccountTypeCode) accountTypeName from  chartofaccounts coa where coa.ParentAccountCode='$parentId'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $row['_parentId']=$row['ParentAccountCode'];
			 array_push($this->data2,$row);
			 if($this->hasChildren($row['AccountCode']))
			 {
				 $this->viewTree($row['AccountCode']);
			 }
		 }
		 //return $data;

	 }


	 public function hasChildren($parentId)
	 {
		 $val=false;
		 $sql="select * from  chartofaccounts where ParentAccountCode='$parentId'";
		 $this->con->setSelect_query($sql);
		 if($this->con->sqlCount()>0)
		 {
			 $val=true;
		 }
		 return $val;


	 }




 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('chartofaccounts');
	 	 $builder->setCriteria("where AccountCode='".parent::getAccountCode()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }




 	 public function find(){
	 	 $sql="select * from  chartofaccounts  where AccountCode='".parent::getAccountCode()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setAccountCode($row['AccountCode']);
	 	parent::setAccountName($row['AccountName']);
	 	parent::setAccountTypeCode($row['AccountTypeCode']);
	 	parent::setIsActive($row['IsActive']);
	 	parent::setParentAccountCode($row['ParentAccountCode']);
}	 	}


	 public function findAccountCode()
	 {
		 $sql="select * from  chartofaccounts  where AccountName='".parent::getAccountName()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setAccountCode($row['AccountCode']);
			 parent::setAccountName($row['AccountName']);
			 parent::setAccountTypeCode($row['AccountTypeCode']);
			 parent::setIsActive($row['IsActive']);
			 parent::setParentAccountCode($row['ParentAccountCode']);
		 }
	 }


	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>