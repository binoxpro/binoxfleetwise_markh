<?php 
 require_once('../model/expenseTemplate.php');
 class expenseTemplateService extends expenseTemplate{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('expensetemplate');
	 	 //$builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('expenseName',parent::getexpenseName());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
 		 	 $builder->addColumnAndData('Category',parent::getCategory());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('expensetemplate');

 	 	 if(!is_null(parent::getexpenseName())){
$builder->addColumnAndData('expenseName',parent::getexpenseName()); 
}

 	 	 if(!is_null(parent::getaccountCode())){
$builder->addColumnAndData('accountCode',parent::getaccountCode()); 
}

 	 	 if(!is_null(parent::getCategory())){
$builder->addColumnAndData('Category',parent::getCategory()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  expensetemplate";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select et.*,coa.accountName from  expensetemplate et inner join chartofaccounts coa on et.accountCode =coa.accountCode";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  expensetemplate limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('expensetemplate');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  expensetemplate  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setexpenseName($row['expenseName']);
	 	parent::setaccountCode($row['accountCode']);
	 	parent::setCategory($row['Category']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>