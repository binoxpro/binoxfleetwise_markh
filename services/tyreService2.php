<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/tyre.php';
class TyreService extends Tyre {
  
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbltyre");
		if(parent::getvehicleId()!=NULL){
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		}
		if(parent::gettyretypeId()!=NULL){
		$builder->addColumnAndData("tyretypeId", parent::gettyretypeId());
		}
		if(parent::getfixon()!=NULL){
		$builder->addColumnAndData("fixon",parent::getfixon());
		}
		if(parent::getfixDate()!=NULL){
		$builder->addColumnAndData("fixDate",parent::getfixDate());
		}
		if(parent::getremovalon()!=NULL){
		$builder->addColumnAndData("removalon",parent::getremovalon());
		}
		if(parent::getisActive()!=NULL){
		$builder->addColumnAndData("isActive",parent::getisActive());
		}
		
		if(parent::getTyreName()!=NULL){
		$builder->addColumnAndData("tyreName",parent::getTyreName());
		}
		
		if(parent::getCostTyre()!=NULL){
		$builder->addColumnAndData("CostTyre",parent::getCostTyre());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tbltyre");
			if(parent::getvehicleId()!=NULL){
			$builder->addColumnAndData("vehicleId", parent::getvehicleId());
			}
			
			if(parent::gettyretypeId()!=NULL){
			$builder->addColumnAndData("tyretypeId", parent::gettyretypeId());
			}
			
			if(parent::getfixon()!=NULL){
			$builder->addColumnAndData("fixon",parent::getfixon());
			}
			
			if(parent::getfixDate()!=NULL){
			$builder->addColumnAndData("fixDate",parent::getfixDate());
			}
			
			if(parent::getremovalon()!=NULL){
			$builder->addColumnAndData("removalon",parent::getremovalon());
			}
			
			if(parent::getisActive()!=NULL){
			$builder->addColumnAndData("isActive",parent::getisActive());
			}
			
			if(parent::getTyreName()!=NULL){
			$builder->addColumnAndData("tyreName",parent::getTyreName());
			}
		
			if(parent::getCostTyre()!=NULL){
			$builder->addColumnAndData("CostTyre",parent::getCostTyre());
			}
			
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
       $sql="select t.fixon,t.fixDate,t.removalon,t.isActive,v.regNo,tt.tyretypeName from tbltyre t inner join tblvehicle v on t.vehicleId=v.id inner join tbltyretype tt on t.tyretypeId=tt.id where t.isActive=1 ";
	   $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["vehicleId"]=$row["regNo"];
		   $data2["tyretypeId"]=$row["tyretypeName"];
		   $data2["fixon"]=$row["fixon"];
		   $data2["fixDate"]=$row["fixDate"];
		   $data2["removalon"]=$row["removalon"];
		    $data2["isActive"]=$row["isActive"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbltyre");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
	public function fixedNewTyre()
	{
		$sql="select * from tbltyre where vehicleId='".parent::getvehicleId()."' and tyretypeId='".parent::gettyretypeId()."'";
		$this->con->setSelect_query($sql);
		$no=$this->con->sqlCount();
		if($no>0)
		{
			//
		}
		else{
			//update 
			//$id=0;
			
			foreach( $this->con->getResultSet($sql) as $row)
	   		{
				
		   		parent::setid($row["id"]);
		   		parent::setvehicleId($row["regNo"]);
		   		parent::setTyreName($row["tyretypeName"]);
				parent::setCostTyre($row['CostTyre']);
		   		parent::setfixon($row["fixon"]);
		  		parent::setfixDate($row["fixDate"]);
		   		parent::setremovalon($row["removalon"]);
		    	parent::setisActive(false);
			}
			
			parent::update();
		}
	}
}

	
