<?php 
 require_once('../model/paymentVoucher.php');
 class paymentVoucherService extends paymentVoucher{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('paymentvoucher');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('sourceAccount',parent::getsourceAccount());
 		 	 $builder->addColumnAndData('preparedBy',parent::getpreparedBy());
 		 	 $builder->addColumnAndData('AuthorisedBy',parent::getAuthorisedBy());
 		 	 $builder->addColumnAndData('Posted',parent::getPosted());
 		 	 $builder->addColumnAndData('transactionDate',parent::gettransactionDate());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('sourceRef',parent::getsourceRef());
 		 	 $builder->addColumnAndData('approved',parent::getapproved());
 		 	 $this->con->setQuery(Director::buildSql($builder));
			 $sql="select * from paymentvoucher id='".parent::getid()."'";
			 $this->con->setSelect_query($sql);
			 if($this->con->sqlCount()>0)
			 {
				 return $this->update();
			 }else{
				 return $this->con->execute_query2($builder->getValues());
			 }




 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('paymentvoucher');

 	 	 if(!is_null(parent::getsourceAccount())){
$builder->addColumnAndData('sourceAccount',parent::getsourceAccount()); 
}

 	 	 if(!is_null(parent::getpreparedBy())){
$builder->addColumnAndData('preparedBy',parent::getpreparedBy()); 
}

 	 	 if(!is_null(parent::getAuthorisedBy())){
$builder->addColumnAndData('AuthorisedBy',parent::getAuthorisedBy()); 
}

 	 	 if(!is_null(parent::getPosted())){
$builder->addColumnAndData('Posted',parent::getPosted()); 
}

 	 	 if(!is_null(parent::gettransactionDate())){
$builder->addColumnAndData('transactionDate',parent::gettransactionDate()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getsourceRef())){
$builder->addColumnAndData('sourceRef',parent::getsourceRef()); 
}

 	 	 if(!is_null(parent::getapproved())){
$builder->addColumnAndData('approved',parent::getapproved()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  paymentvoucher";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select pv.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pv.sourceAccount) AccountName from  paymentvoucher pv";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select pv.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pv.sourceAccount) AccountName from  paymentvoucher pv limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('paymentvoucher');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  paymentvoucher  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setsourceAccount($row['sourceAccount']);
	 	parent::setpreparedBy($row['preparedBy']);
	 	parent::setAuthorisedBy($row['AuthorisedBy']);
	 	parent::setPosted($row['Posted']);
	 	parent::settransactionDate($row['transactionDate']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setsourceRef($row['sourceRef']);
	 	parent::setapproved($row['approved']);
}	 	}

	 public function findJson(){
		 $sql="select pv.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pv.sourceAccount) AccountName from  paymentvoucher pv  where pv.id='".parent::getid()."'";
		 $array=null;
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $array=$row;
		 }
		 return $array;
	 }


	 public function getVoucherNo()
	 {
		 $sql="select * from paymentvoucher";
		 $this->con->setSelect_query($sql);
		 return $this->con->sqlCount()+1;

	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>