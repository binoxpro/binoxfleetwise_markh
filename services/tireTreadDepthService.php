<?php 
 require_once('../model/tireTreadDepth.php');
 class tireTreadDepthService extends tireTreadDepth{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltiretreaddepth');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('treadDepth',parent::gettreadDepth());
 		 	 $builder->addColumnAndData('tireId',parent::gettireId());
 		 	 $builder->addColumnAndData('CreationDate',parent::getCreationDate());
 		 	 $builder->addColumnAndData('odometerreading',parent::getodometerreading());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltiretreaddepth');

 	 	 if(!is_null(parent::gettreadDepth())){
$builder->addColumnAndData('treadDepth',parent::gettreadDepth()); 
}

 	 	 if(!is_null(parent::gettireId())){
$builder->addColumnAndData('tireId',parent::gettireId()); 
}

 	 	 if(!is_null(parent::getCreationDate())){
$builder->addColumnAndData('CreationDate',parent::getCreationDate()); 
}

 	 	 if(!is_null(parent::getodometerreading())){
$builder->addColumnAndData('odometerreading',parent::getodometerreading()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltiretreaddepth";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltiretreaddepth');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltiretreaddepth where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settreadDepth($row["treadDepth"]); 


 	 	 parent::settireId($row["tireId"]); 


 	 	 parent::setCreationDate($row["CreationDate"]); 


 	 	 parent::setodometerreading($row["odometerreading"]); 

	 	} 
 }
 
 public function getObjectActual(){
	 	 $sql="select * from  tbltiretreaddepth where tireId='".parent::gettireId()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settreadDepth($row["treadDepth"]); 


 	 	 parent::settireId($row["tireId"]); 


 	 	 parent::setCreationDate($row["CreationDate"]); 


 	 	 parent::setodometerreading($row["odometerreading"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>