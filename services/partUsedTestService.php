<?php 
 require_once('../model/partUsedTest.php');
 class partUsedTestService extends partUsedTest{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblpartsused');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('numberplate',parent::getnumberplate());
 		 	 $builder->addColumnAndData('partused',parent::getpartused());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('cost',parent::getcost());
 		 	 $builder->addColumnAndData('jobcardId',parent::getjobcardId());
 		 	 $builder->addColumnAndData('dateofrepair',parent::getdateofrepair());
 		 	 $builder->addColumnAndData('typeofrepair',parent::gettypeofrepair());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValiues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblpartsused');

 	 	 if(parent::getnumberplate==""){
$builder->addColumnAndData('numberplate',parent::getnumberplate()); }

 	 	 if(parent::getpartused==""){
$builder->addColumnAndData('partused',parent::getpartused()); }

 	 	 if(parent::getquantity==""){
$builder->addColumnAndData('quantity',parent::getquantity()); }

 	 	 if(parent::getcost==""){
$builder->addColumnAndData('cost',parent::getcost()); }

 	 	 if(parent::getjobcardId==""){
$builder->addColumnAndData('jobcardId',parent::getjobcardId()); }

 	 	 if(parent::getdateofrepair==""){
$builder->addColumnAndData('dateofrepair',parent::getdateofrepair()); }

 	 	 if(parent::gettypeofrepair==""){
$builder->addColumnAndData('typeofrepair',parent::gettypeofrepair()); }
$builder->setCriteria("where id='".parent::getid."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblpartsused";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblpartsused');
	 	 $builder->setCriteria("where id='".parent::getid."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>