<?php 
 require_once('../model/trip.php');
 class TripService extends Trip{
	 	 public function save(){
			 $builder=new InsertBuilder();
			 $builder->setTable('tripnumbergenerator');
			 $builder->addColumnAndData('TripNumber',parent::getTripNumber());
			 if(!is_null(parent::getTripTypeId())){
				 $builder->addColumnAndData('TripTypeId',parent::getTripTypeId());
			 }

			 if(!is_null(parent::getVehicleId())){
				 $builder->addColumnAndData('VehicleId',parent::getVehicleId());
			 }

			 if(!is_null(parent::getDriverId())){
				 $builder->addColumnAndData('DriverId',parent::getDriverId());
			 }

			 if(!is_null(parent::getStatus())){
				 $builder->addColumnAndData('Status',parent::getStatus());
			 }

			 if(!is_null(parent::getTripStartDate())){
				 $builder->addColumnAndData('TripStartDate',parent::getTripStartDate());
			 }

			 if(!is_null(parent::getTripEndDate())){
				 $builder->addColumnAndData('TripEndDate',parent::getTripEndDate());
			 }

			 if(!is_null(parent::getWorkOrderAttached())){
				 $builder->addColumnAndData('WorkOrderAttached',parent::getWorkOrderAttached());
			 }

			 if(!is_null(parent::getApproved())){
				 $builder->addColumnAndData('Approved',parent::getApproved());
			 }
			 $sql="select * from trpnumbergenerator where TripNumber='".parent::getTripNumber()."'";
			 $this->con->setSelect_query($sql);
				 if($this->con->sqlCount()>0)
				 {
					return $this->update();

				 }else
				 {
					 $this->con->setQuery(Director::buildSql($builder));
					 return $this->con->execute_query2($builder->getValues());
				 }

 	  }
 
	 	 public function update()
         {
                 $builder=new UpdateBuilder();
                 $builder->setTable('tripnumbergenerator');

                if(!is_null(parent::getTripTypeId())){
                $builder->addColumnAndData('TripTypeId',parent::getTripTypeId());
                }

                if(!is_null(parent::getVehicleId())){
                $builder->addColumnAndData('VehicleId',parent::getVehicleId());
                }

                if(!is_null(parent::getDriverId())){
                $builder->addColumnAndData('DriverId',parent::getDriverId());
                }

                if(!is_null(parent::getStatus())){
                $builder->addColumnAndData('Status',parent::getStatus());
                }

                if(!is_null(parent::getTripStartDate())){
                $builder->addColumnAndData('TripStartDate',parent::getTripStartDate());
                }

                if(!is_null(parent::getTripEndDate())){
                $builder->addColumnAndData('TripEndDate',parent::getTripEndDate());
                }

                if(!is_null(parent::getWorkOrderAttached()))
                {
                $builder->addColumnAndData('WorkOrderAttached',parent::getWorkOrderAttached());
                }

                if(!is_null(parent::getApproved()))
                {
                $builder->addColumnAndData('Approved',parent::getApproved());
                }
                $builder->setCriteria("where TripNumber='".parent::getTripNumber()."'");
                $this->con->setQuery(Director::buildSql($builder));
                return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
        $sql="select * from  tripnumbergenerator";
        return $this->con->getResultSet($sql);
 	 }

	 public function view()
     {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select tng.*,v.regNo registrationNo,(select vt.regNo from tbltrailer tr inner join tblvehicle vt on tr.trailerId=vt.id where tr.isActive='1' and tr.tractorId=v.id) trailorPlate,concat(e.firstName,' ',e.lastName) driverName from  tripnumbergenerator tng inner join tblvehicle v on v.id=tng.VehicleId inner join employee e on e.empNo=tng.DriverId";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select tng.*,v.regNo registrationNo,(select vt.regNo from tbltrailer tr inner join tblvehicle vt on tr.trailerId=vt.id where tr.isActive='1' and tr.tractorId=v.id) trailorPlate,concat(e.firstName,' ',e.lastName) driverName from  tripnumbergenerator tng inner join tblvehicle v on v.id=tng.VehicleId inner join employee e on e.empNo=tng.DriverId order by tng.TripStartDate limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
            {
                 array_push($data2,$row);
            }
         $data["rows"]=$data2;
         return $data;
     }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tripnumbergenerator');
	 	 $builder->setCriteria("where TripNumber='".parent::getTripNumber()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tripnumbergenerator  where TripNumber='".parent::getTripNumber()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setTripNumber($row['TripNumber']);
	 	parent::setTripTypeId($row['TripTypeId']);
	 	parent::setVehicleId($row['VehicleId']);
	 	parent::setDriverId($row['DriverId']);
	 	parent::setStatus($row['Status']);
	 	parent::setTripStartDate($row['TripStartDate']);
	 	parent::setTripEndDate($row['TripEndDate']);
	 	parent::setWorkOrderAttached($row['WorkOrderAttached']);
	 	parent::setApproved($row['Approved']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }

     public function tripNumberGenerator($tripString)
     {
         $sql="select * from  tripnumbergenerator where TripTypeId='".parent::getTripTypeId()."'";
         $this->con->setSelect_query($sql);
         $no=0;
         $startOff=10;
         $prefix=$tripString."/".date('y')."/";
         $no=$this->con->sqlCount()+$startOff+1;
         return $prefix.$this->returnThreeFiguareString($no);

     }
     public function returnThreeFiguareString($figureVal)
     {
         $finger="";
         $myfingerLenght=strlen($figureVal);
         switch($myfingerLenght)
         {
             case 0:
                 $finger="000";
                 break;
             case 1:
                 $finger="00".$figureVal;
                 break;
             case 2:
                 $finger="0".$figureVal;
                 break;
             case 3:
                 $finger="".$figureVal;
                 break;
             default:
                 $finger=$figureVal;
                 break;

         }
         return $finger;
     }
 }
?>