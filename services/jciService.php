<?php 
 require_once('../model/jci.php');
 class jciService extends jci{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbljobcarditem');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('details',parent::getdetails());
 		 	 $builder->addColumnAndData('timeTaken',parent::gettimeTaken());
 		 	 $builder->addColumnAndData('initials',parent::getinitials());
 		 	 $builder->addColumnAndData('jobcardId',parent::getjobcardId());
 		 	 $builder->addColumnAndData('repairReview',parent::getrepairReview());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbljobcarditem');

 	 	 if(!is_null(parent::getdetails())){
$builder->addColumnAndData('details',parent::getdetails()); 
}

 	 	 if(!is_null(parent::gettimeTaken())){
$builder->addColumnAndData('timeTaken',parent::gettimeTaken()); 
}

 	 	 if(!is_null(parent::getinitials())){
$builder->addColumnAndData('initials',parent::getinitials()); 
}

 	 	 if(!is_null(parent::getjobcardId())){
$builder->addColumnAndData('jobcardId',parent::getjobcardId()); 
}

 	 	 if(!is_null(parent::getrepairReview())){
$builder->addColumnAndData('repairReview',parent::getrepairReview()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tbljobcarditem";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tbljobcarditem";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  tbljobcarditem limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbljobcarditem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tbljobcarditem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setdetails($row['details']);
	 	parent::settimeTaken($row['timeTaken']);
	 	parent::setinitials($row['initials']);
	 	parent::setjobcardId($row['jobcardId']);
	 	parent::setrepairReview($row['repairReview']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>