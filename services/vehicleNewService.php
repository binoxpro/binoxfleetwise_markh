<?php 
 require_once('../model/vehicleNew.php');
 class vehicleNewService extends vehicleNew{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblvehicle');
	 	 //$builder->addColumnAndData('id',parent::getid());
         $builder->addColumnAndData('regNo',parent::getregNo());
         $builder->addColumnAndData('yom',parent::getyom());
         $builder->addColumnAndData('modelId',parent::getmodelId());
         $builder->addColumnAndData('dop',parent::getdop());
         $builder->addColumnAndData('chaseNumber',parent::getchaseNumber());
         $builder->addColumnAndData('engineCapacityCC',parent::getengineCapacityCC());
         $builder->addColumnAndData('vehicleTypeId',parent::getvehicleTypeId());
         $builder->addColumnAndData('tonnage',parent::gettonnage());
         $builder->addColumnAndData('color',parent::getcolor());
         $builder->addColumnAndData('status',parent::getstatus());
         $builder->addColumnAndData('fuel',parent::getfuel());
         $builder->addColumnAndData('purpose',parent::getpurpose());
         $builder->addColumnAndData('hasTrailer',parent::getHasTrailer());
         $builder->addColumnAndData('isTrailer',parent::getIsTrailer());
         $this->con->setQuery(Director::buildSql($builder));
         $this->con->execute_query2($builder->getValues());
         parent::setid($this->con->getId());

 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblvehicle');

 	 	 if(!is_null(parent::getregNo())){
$builder->addColumnAndData('regNo',parent::getregNo()); 
}

 	 	 if(!is_null(parent::getyom())){
$builder->addColumnAndData('yom',parent::getyom()); 
}

 	 	 if(!is_null(parent::getmodelId())){
$builder->addColumnAndData('modelId',parent::getmodelId()); 
}

 	 	 if(!is_null(parent::getdop())){
$builder->addColumnAndData('dop',parent::getdop()); 
}

 	 	 if(!is_null(parent::getchaseNumber())){
$builder->addColumnAndData('chaseNumber',parent::getchaseNumber()); 
}

 	 	 if(!is_null(parent::getengineCapacityCC())){
$builder->addColumnAndData('engineCapacityCC',parent::getengineCapacityCC()); 
}

 	 	 if(!is_null(parent::getvehicleTypeId())){
$builder->addColumnAndData('vehicleTypeId',parent::getvehicleTypeId()); 
}

 	 	 if(!is_null(parent::gettonnage())){
$builder->addColumnAndData('tonnage',parent::gettonnage()); 
}

             if(!is_null(parent::getcolor())) {
                 $builder->addColumnAndData('color', parent::getcolor());
             }
             if(!is_null(parent::getstatus())) {
                 $builder->addColumnAndData('status', parent::getstatus());
             }
             if(!is_null(parent::getfuel())) {
                 $builder->addColumnAndData('fuel', parent::getfuel());
             }
             if(!is_null(parent::getpurpose()))
             {
                 $builder->addColumnAndData('purpose', parent::getpurpose());
             }

             if(!is_null(parent::getHasTrailer()))
             {
                 $builder->addColumnAndData('hasTrailer',parent::getHasTrailer());
             }
             if(!is_null(parent::getIsTrailer()))
             {
                 $builder->addColumnAndData('isTrailer',parent::getIsTrailer());
             }


        $builder->setCriteria("where id='".parent::getid()."'");
        $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }

	 public function viewCombo()
	 {
		 $sql=null;
		 if(!is_null(parent::getvehicleTypeId()))
		 {
			 $sql="SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,concat(v.regNo,'(',vm.makeName,'/',tm.modelName,'/',vt.vehicleType,')') vehicle FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id  where v.vehicleTypeId='".parent::getvehicleTypeId()."'";

		 }else
		 {
			 $sql = "SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,concat(v.regNo,'(',vm.makeName,'/',tm.modelName,'/',vt.vehicleType,')') vehicle FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id ";

		 }
			 return $this->con->getResultSet($sql);

	   }


     public function viewCombo2()
     {

            $sql="SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,concat(v.regNo,'(',vm.makeName,'/',tm.modelName,'/',vt.vehicleType,')') vehicle FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id  where (v.isTrailer='0' and v.hasTrailer='1') or (v.isTrailer='0' and v.hasTrailer='0') ";


         return $this->con->getResultSet($sql);

     }


 	 public function view()
     {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		//SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,vc.configurationName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id
	 	 $sql="SELECT v.*,tm.modelName,vm.makeName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id";
         $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql="SELECT v.*,tm.modelName,vm.makeName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id limit $offset,$rows";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;

 	 }

     public function viewSearch(){
         $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
         $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
         $offset = ($page-1)*$rows;
         //SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,vc.configurationName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id
         $sql="SELECT v.*,tm.modelName,vm.makeName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id";
         $this->con->setSelect_query($sql);
         $data2=array();
         $data=array();
         $data["total"]=$this->con->sqlCount();
         $sql="SELECT v.*,tm.modelName,vm.makeName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id limit $offset,$rows";
         foreach($this->con->getResultSet($sql) as $row)
         {
             array_push($data2,$row);
         }
         $data["rows"]=$data2;
         return $data;

     }


 	 public function delete()
     {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblvehicle');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tblvehicle  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setregNo($row['regNo']);
	 	parent::setyom($row['yom']);
	 	parent::setmodelId($row['modelId']);
	 	//parent::setdop($row['dop']);
	 	parent::setchaseNumber($row['chaseNumber']);
	 	//parent::setengineCapacityCC($row['engineCapacityCC']);
	 	parent::setvehicleTypeId($row['vehicleTypeId']);
	 	parent::settonnage($row['tonnage']);
	 	parent::setcolor($row['color']);
	 	parent::setfuel($row['fuel']);
	 	parent::setstatus($row['status']);
	 	parent::setpurpose($row['purpose']);
	 	parent::setHasTrailer($row['hasTrailer']);
	 	parent::setIsTrailer($row['isTrailer']);

}	 	}


     public function findCriteria($regNo)
     {
         $sql="select * from  tblvehicle  where regNo='".$regNo."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setregNo($row['regNo']);
             parent::setyom($row['yom']);
             parent::setmodelId($row['modelId']);
             parent::setdop($row['dop']);
             parent::setchaseNumber($row['chaseNumber']);
             parent::setengineCapacityCC($row['enginecapacitycc']);
             parent::setvehicleTypeId($row['vehicleTypeId']);
             parent::settonnage($row['tonnage']);
             parent::setcolor($row['color']);
             parent::setfuel($row['fuel']);
             parent::setstatus($row['status']);
             parent::setpurpose($row['purpose']);
             parent::setHasTrailer($row['hasTrailer']);
             parent::setIsTrailer($row['isTrailer']);
         }
 	 }

	 public function findNoMake($dop)
	 {
		 $sql = "select * from  tblvehicle  where dop='$dop' and vehicleTypeId!='2'";
		 $this->con->setSelect_query($sql);
		 return $this->con->sqlCount();
	 }
 	 public function view_query($sql)
     {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>