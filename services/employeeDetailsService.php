<?php
include_once '../model/employeeDetails.php';
class EmployeeDetailsService extends EmployeeDetails
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblemployeedetails");
		
		$builder->addColumnAndData("position", parent::getposition());
		$builder->addColumnAndData("employeeId", parent::getemployeeId());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblemployeedetails");
			$builder->addColumnAndData("position", parent::getposition());
            $builder->addColumnAndData("employeeId", parent::getemployeeId());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblemployeedetails";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblemployeedetails");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
