<?php
include_once '../model/section.php';
class SectionService extends Section
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblsection");
		$builder->addColumnAndData("name", parent::getname());
		$builder->addColumnAndData("isActive", parent::getisActive());
		$builder->addColumnAndData("checkTypeId",parent::getCheckListTypeId());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblsection");
			$builder->addColumnAndData("name", parent::getname());
            $builder->addColumnAndData("isActive", parent::getisActive());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblsection";
	   return $this->con->getResultSet($sql);
    }
	
	public function viewSection() {
       $sql="select * from tblsection where checkTypeId='".parent::getCheckListTypeId()."'";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblsection");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
