<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcademicYearService
 *
 * @author plussoft
 * 
 */
include_once '../model/tyre.php';

class TyreService extends Tyre{
    
   
    
    function __construct() {
        parent::__construct();        
    }


    public function save() {
            $this->updateIsActive();
       		
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_tyre");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("tyrePosition", parent::gettyrePosition());
			$builder->addColumnAndData("brand", parent::getbrand());
			$builder->addColumnAndData("serialNumber", parent::getserialNumber());
			$builder->addColumnAndData("fixingDate", parent::getfixingDate());
			$builder->addColumnAndData("Odometer", parent::getodometer());
			$builder->addColumnAndData("expectedKm", parent::getexpectedKm());
			$builder->addColumnAndData("CurretMileage", parent::getcurrentMileage());
			$builder->addColumnAndData("removalMileage", parent::getremovalMileage());
			$builder->addColumnAndData("comment", parent::getcomment());
			$builder->addColumnAndData("cost", parent::getCost());
			$builder->addColumnAndData("isActive", true);
			$builder->addColumnAndData("fixedAs", parent::getFixedAs());
			$builder->addColumnAndData("stockId", parent::getStockId());
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_tyre where numberPlate='".parent::getNumberPlate()."' and Odometer='".parent::getodometer()."' and tyrePosition='".parent::gettyrePosition()."' and serialNumber='".parent::getserialNumber()."'");
			if($this->con->sqlCount()<1)
			{
				
				return $this->con->execute_query2($builder->getValues());
				
			}else{
				return array('msg'=>'Tyre ready add to Truck');
			}
            
       
       // parent::save();
    }
	
	
	public function saveNotification($name,$email,$isActive) {
       		$this->updateIsActive();
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tblnotificationEmail");
            $builder->addColumnAndData("name",$name);
            $builder->addColumnAndData("email",$email);
			$builder->addColumnAndData("isActive",$isActive);
			
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_tyre where email='$email' and serialNumber='$name'");
			if($this->con->sqlCount()<1){
				return $this->con->execute_query2($builder->getValues());
				
			}else{
				return array('msg'=>'Notification email ready exits');
			}
            
       
       // parent::save();
    }
	
	public function saveStatus() {
       		
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service_status");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			//$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_service_status where numberPlate='".parent::getNumberPlate()."' ");
			if($this->con->sqlCount()<1){
				//$this->updateStatus();
            	return $this->con->execute_query2($builder->getValues());
				
			}else{
				return array('msg'=>'Service schedue ready saved');
			}
            
       
       // parent::save();
    }
	
    public function update() {
         	$builder=new UpdateBuilder();
           $builder->setTable("tbl_tyre");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("tyrePosition", parent::gettyrePosition());
			$builder->addColumnAndData("brand", parent::getbrand());
			$builder->addColumnAndData("serialNumber", parent::getserialNumber());
			$builder->addColumnAndData("fixingDate", parent::getfixingDate());
			$builder->addColumnAndData("Odometer", parent::getodometer());
			$builder->addColumnAndData("expectedKm", parent::getexpectedKm());
			$builder->addColumnAndData("CurretMileage", parent::getcurrentMileage());
			$builder->addColumnAndData("removalMileage", parent::getremovalMileage());
			$builder->addColumnAndData("comment", parent::getcomment());
			$builder->addColumnAndData("cost", parent::getCost());
			//$builder->addColumnAndData("isActive", );
			$builder->addColumnAndData("fixedAs", parent::getFixedAs());
			$builder->setCriteria("Where Id='".parent::getId()."'");
            $this->con->setQuery(Director::buildSql($builder));
			 return $this->con->execute_query();
			 //return $this->updateStatus();
		
    }
	
	 public function updateExtension() {
         	$builder=new UpdateBuilder();
           $builder->setTable("tbl_tyre");
           
			$builder->addColumnAndData("removalMileage", parent::getremovalMileage());
			
			//$builder->addColumnAndData("isActive", );
			
			$builder->setCriteria("Where Id='".parent::getId()."'");
            $this->con->setQuery(Director::buildSql($builder));
			 return $this->con->execute_query();
			 //return $this->updateStatus();
		
    }
	
	
	
	public function updateIsActive() {
		$sql="select * from tbl_tyre where numberPlate='".parent::getNumberPlate()."' and tyrePosition='".parent::gettyrePosition()."'";
		$id=-1;
		$stockId=-1;
		foreach($this->con->getResultSet($sql) as $row){
			$id=$row['Id'];
			//$stockId=$row['stockId'];
		}
		
		if($id>0)
		{
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_tyre");
			$builder->addColumnAndData("comment", parent::getcomment());
            $builder->addColumnAndData("removalDate", parent::getfixingDate());
			$builder->addColumnAndData("isActive", false);
			$builder->setCriteria("Where Id='$id'");
            $this->con->setQuery(Director::buildSql($builder));
			$this->con->execute_query();
			
			//$builder2=new UpdateBuilder();
           	//$builder2->setTable("tblstock");
            //$builder2->addColumnAndData("status", 'usedUp');
			//$builder2->addColumnAndData("isActive", false);
			//$builder2->setCriteria("Where id='$stockId'");
            //$this->con->setQuery(Director::buildSql($builder2));
			//$this->con->execute_query();
			 //return $this->updateStatus();
		}
		
    }
	#Obsolet method
	public function updateStatus(){
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			//$builder->addColumnAndData("reg_date", $datex);
			$builder->setCriteria("Where numberPlate='".parent::getNumberPlate()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    
	}
	public function updateStatusNew(){
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("PreviousDate", parent::getPreviousDate());
			$builder->addColumnAndData("NextDate", parent::getNextDate());
			//$builder->addColumnAndData("reg_date", $datex);
			$builder->setCriteria("Where numberPlate='".parent::getNumberPlate()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    
	}
	
    public function view2($id) {
       $sql="select * from tbl_class ";
	   return $this->con->getResultSet($sql);
    }

	public function view() {
       $sql="select tt.id positionId,ty.Id,ty.stockId,(select sk.treadDepth from tblstock sk where sk.id=ty.stockId) treadDepth,v.regNo, tt.tyretypeName position, ty.brand,ty.serialNumber, ty.cost , ty.fixedAs,ty.fixingDate, ty.Odometer,ty.expectedKm, 	od.reading currentReading,case when (ty.removalMileage-od.reading)>0 then (ty.removalMileage-od.reading)  else 0 end remaining ,round((ty.cost/ty.expectedKm),2) costperkm,ty.removalMileage,ty.comment,ty.numberPlate,ty.tyrePosition tyreDetails,case when (ty.removalMileage-od.reading)>0 then (od.reading-ty.Odometer)  else ty.expectedKm end kmsDone,FORMAT(round(((ty.cost/ty.expectedKm)*(case when (ty.removalMileage-od.reading)>0 then (od.reading-ty.Odometer)  else ty.expectedKm end)),2),0) currentCost from tbl_tyre ty inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId  where ty.isActive =1  order by ty.numberPlate,tt.id asc limit 0,50";
	  
       return $this->con->getResultSet($sql);
    }
	public function setCurrentReadingOn() {
       $sql="SELECT reading from tblodometer where vehicleId='".parent::getNumberPlate()."'";
	   parent::setcurrentMileage(NULL);
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   parent::setcurrentMileage($row['reading']);
	   }
	   if(parent::getcurrentMileage()==NULL)
	   {
		   parent::setcurrentMileage(0);
	   }
    }
    
    public function getTyreCostInRanage($startDate,$endDate,$vehicleId) {
        $reading=0.0;
        $sumCostPer=0.0;
        $sql="SELECT reading from tblodmeterreading where vehicleId='$vehicleId' and (creationDate between '$startDate' and '$endDate') order by creationDate asc";
      // $sql="SELECT reading from tblodometer where vehicleId='".parent::getNumberPlate()."'";
	   //parent::setcurrentMileage(NULL);
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $reading=$row['reading'];
	   }
       
       $sqlTire="select sum(ty.cost/ty.expectedKm) cpk from tbl_tyre ty  where ty.numberPlate='$vehicleId' and (ty.Odometer>='$reading' and ty.removalMileage<'$reading')";
       foreach($this->con->getResultSet($sqlTire) as $rowTire)
		{
			$sumCostPer=$rowTire['cpk'];
		}
        return $sumCostPer;
	   
    }
    
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	//the method is litte in complete
	public function searchFilterForService(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getInterval()==NULL?'null':"'".parent::getInterval()."'";
		$para4=parent::getReminder()==NULL?'null':"'".parent::getReminder()."'";
		$para5=parent::getService()==NULL?'null':"'".parent::getService()."'";
		$para6=parent::getTypeOfService()==NULL?'null':"'".parent::getTypeOfService()."'";
		$para7=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		$para8=parent::getServiceID()==NULL?'null':"'".parent::getServiceID()."'";
		$sql="select serviceID,numberPlate,PreviousDate,NextDate,IntervalPeriod,Reminder,Service,TypeOfService from tbl_service where serviceID=COALESCE(".$para8.",serviceID) AND numberPlate=COALESCE(".$para7.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate) AND IntervalPeriod=COALESCE(".$para3.",IntervalPeriod) AND Reminder=COALESCE(".$para4.",Reminder) AND Service=COALESCE(".$para5.",Service) AND TypeOfService=COALESCE(".$para6.",TypeOfService) ";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	
	public function searchFilterForServiceStatus(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		
		$sql="select * from tbl_service_status where numberPlate=COALESCE(".$para3.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate)";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbl_tyre");
		$builder->setCriteria("where Id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	

    //put your code here
}
?>