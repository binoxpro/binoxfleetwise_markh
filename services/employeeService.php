<?php 
 require_once('../model/employee.php');
 class employeeService extends employee{
	 	 public function save()
         {
             $builder = new InsertBuilder();
             $builder->setTable('employee');
             if (!is_null(parent::getempNo()&&parent::getempNo()!=" " &&parent::getempNo()!=null)) {
                 $builder->addColumnAndData('empNo', parent::getempNo());
                 $builder->addColumnAndData('firstName', parent::getfirstName());
                 $builder->addColumnAndData('lastName', parent::getlastName());
                 $builder->addColumnAndData('sex', parent::getsex());
                 $builder->addColumnAndData('martialStatus', parent::getmartialStatus());
                 $builder->addColumnAndData('dob', parent::getdob());
                 $builder->addColumnAndData('town', parent::gettown());
                 $builder->addColumnAndData('district', parent::getdistrict());
                 $builder->addColumnAndData('nationID', parent::getnationID());
                 $builder->addColumnAndData('mobile1', parent::getmobile1());
                 $builder->addColumnAndData('mobile2', parent::getmobile2());
                 $builder->addColumnAndData('nikname', parent::getnikname());
                 $builder->addColumnAndData('contact', parent::getcontact());
                 $builder->addColumnAndData('photo', parent::getphoto());
                 $this->con->setQuery(Director::buildSql($builder));

                 $this->con->setSelect_query("select * from employee where empNo='" . parent::getempNo() . "'");
                 if ($this->con->sqlCount() > 0)
                 {
                     return $this->update();
                 } else
                 {
                     return $this->con->execute_query2($builder->getValues());
                 }
             } else
             {
                 return array('msg'=>'Supply the Employee ID NO');
             }




 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('employee');

 	 	 if(!is_null(parent::getfirstName())){
$builder->addColumnAndData('firstName',parent::getfirstName()); 
}

 	 	 if(!is_null(parent::getlastName())){
$builder->addColumnAndData('lastName',parent::getlastName()); 
}

 	 	 if(!is_null(parent::getsex())){
$builder->addColumnAndData('sex',parent::getsex()); 
}

 	 	 if(!is_null(parent::getmartialStatus())){
$builder->addColumnAndData('martialStatus',parent::getmartialStatus()); 
}

 	 	 if(!is_null(parent::getdob())){
$builder->addColumnAndData('dob',parent::getdob()); 
}

 	 	 if(!is_null(parent::gettown())){
$builder->addColumnAndData('town',parent::gettown()); 
}

 	 	 if(!is_null(parent::getdistrict())){
$builder->addColumnAndData('district',parent::getdistrict()); 
}

 	 	 if(!is_null(parent::getnationID())){
$builder->addColumnAndData('nationID',parent::getnationID()); 
}

 	 	 if(!is_null(parent::getmobile1())){
$builder->addColumnAndData('mobile1',parent::getmobile1()); 
}

 	 	 if(!is_null(parent::getmobile2())){
$builder->addColumnAndData('mobile2',parent::getmobile2()); 
}

 	 	 if(!is_null(parent::getnikname())){
$builder->addColumnAndData('nikname',parent::getnikname()); 
}

 	 	 if(!is_null(parent::getcontact())){
$builder->addColumnAndData('contact',parent::getcontact()); 
}
			 if(!is_null(parent::getphoto())){
				 $builder->addColumnAndData('photo',parent::getphoto());
			 }
$builder->setCriteria("where empNo='".parent::getempNo()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select e.*,concat(e.empNo,':',e.firstName,' ',e.lastName) fullName  from  employee e inner join employeestatus es on es.empNo=e.empNo inner join tblposition p on es.positionId=p.id where p.position='Drivers'";
	    return $this->con->getResultSet($sql);
 	 }


     public function viewConboxposition($str){
         $sql="select e.*,concat(e.firstName,' ',e.lastName) fullName  from  employee e inner join employeestatus es on es.empNo=e.empNo inner join tblposition p on es.positionId=p.id where p.position='$str'";
         return $this->con->getResultSet($sql);
     }


	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  employee";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  employee limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('employee');
	 	 $builder->setCriteria("where empNo='".parent::getempNo()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  employee  where empNo='".parent::getempNo()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setempNo($row['empNo']);
	 	parent::setfirstName($row['firstName']);
	 	parent::setlastName($row['lastName']);
	 	parent::setsex($row['sex']);
	 	parent::setmartialStatus($row['martialStatus']);
	 	parent::setdob($row['dob']);
	 	parent::settown($row['town']);
	 	parent::setdistrict($row['district']);
	 	parent::setnationID($row['nationID']);
	 	parent::setmobile1($row['mobile1']);
	 	parent::setmobile2($row['mobile2']);
	 	parent::setnikname($row['nikname']);
	 	parent::setcontact($row['contact']);
}	 	}

     public function findWithName(){
         $sql="select * from  employee  where firstName='".parent::getfirstName()."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setempNo($row['empNo']);
             parent::setfirstName($row['firstName']);
             parent::setlastName($row['lastName']);
             parent::setsex($row['sex']);
             parent::setmartialStatus($row['martialStatus']);
             parent::setdob($row['dob']);
             parent::settown($row['town']);
             parent::setdistrict($row['district']);
             parent::setnationID($row['nationID']);
             parent::setmobile1($row['mobile1']);
             parent::setmobile2($row['mobile2']);
             parent::setnikname($row['nikname']);
             parent::setcontact($row['contact']);
         }	 	}

     public function findJson()
     {
         $data;
         $sql="select * from  employee  where empNo='".parent::getempNo()."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
                //array_push($data,$row);
             $data=$row;
         }
         return $data;
     }

     public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>