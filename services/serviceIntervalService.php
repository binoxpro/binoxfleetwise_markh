<?php 
 require_once('../model/serviceInterval.php');
 class serviceIntervalService extends serviceInterval{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblserviceinterval');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('intervalValue',parent::getintervalValue());
 		 	 $builder->addColumnAndData('serviceTypeId',parent::getserviceTypeId());
 		 	 $builder->addColumnAndData('modal',parent::getmodal());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $builder->addColumnAndData('nextServiceType',parent::getnextServiceType());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblserviceinterval');

 	 	 if(parent::getintervalValue()!=""){
$builder->addColumnAndData('intervalValue',parent::getintervalValue()); }

 	 	 if(parent::getserviceTypeId()!=""){
$builder->addColumnAndData('serviceTypeId',parent::getserviceTypeId()); }

 	 	 if(parent::getmodal()!=""){
$builder->addColumnAndData('modal',parent::getmodal()); }

 	 	 if(parent::getisActive()!=""){
$builder->addColumnAndData('isActive',parent::getisActive()); }

 	 	 if(parent::getnextServiceType()!=""){
$builder->addColumnAndData('nextServiceType',parent::getnextServiceType()); 
	}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select si.*,m.makeName,st.serviceName,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine from  tblserviceinterval si inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehiclemake m on si.modal=m.makeName order by si.modal asc,si.id asc";
	 return $this->con->getResultSet($sql);
 	 }
	 public function viewModel(){
	 	 $sql="select si.*,m.makeName,st.serviceName,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine from  tblserviceinterval si inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehiclemake m on si.modal=m.makeName  where si.modal='".parent::getmodal()."' order by si.modal asc,st.id asc";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblserviceinterval');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
	 
	 public function setObject(){
		 $sql="select si.*,m.makeName,st.serviceName,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine from  tblserviceinterval si inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehiclemake m on si.modal=m.makeName where si.id='".parent::getid()."' order by si.modal asc,st.id asc";
	 	 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setintervalValue($row['intervalValue']);
		 }
 	 }

     public function findCriteria($modal,$interval,$modalName)
     {
         //set for invalid
         parent::setid(-1);
         $sql = "select si.* from  tblserviceinterval si inner join tblservicetype st on si.serviceTypeId=st.id where st.serviceName like '%".$modalName."%' and si.intervalValue='" . $interval . "' and si.modal='" . $modal . "'   order by si.id limit 0,1";
         //echo $sql;
         foreach ($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setintervalValue($row['intervalValue']);
             parent::setmodal($row['modal']);
             parent::setnextServiceType($row['nextServiceType']);
             parent::setserviceTypeId($row['serviceTypeId']);
             parent::setisActive($row['isActive']);
         }


     }





 }
?>