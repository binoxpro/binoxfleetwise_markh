<?php
include_once '../model/driver.php';
class DriverService extends Driver
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbldriver");
		if(parent::getid()!=NULL){
		$builder->addColumnAndData("id", parent::getid());
		}
		if(parent::getfirstName()!=NULL){
		$builder->addColumnAndData("firstName", parent::getfirstName());
		}
		if(parent::getlastName()!=NULL){
		$builder->addColumnAndData("lastName",parent::getlastName());
		}
		if(parent::getdob()!=NULL){
		$builder->addColumnAndData("dob",parent::getdob());
		}
		if(parent::getcontact()!=NULL){
		$builder->addColumnAndData("contact",parent::getcontact());
		}
		if(parent::getisActive()!=NULL)
		{
		$builder->addColumnAndData("isActive",parent::getIsActive());
		}

		//check if the driver details exists
		$sql="select * from tbldriver where id='".parent::getid()."'";
		$this->con->setSelect_query($sql);
		//echo $this->con->sqlCount();
		if($this->con->sqlCount()>0)
		{

			return $this->update();
		}else
		{
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query2($builder->getValues());
		}
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbldriver");
			if(parent::getid()!=NULL){
			$builder->addColumnAndData("id", parent::getid());
			}
			if(parent::getfirstName()!=NULL){
			$builder->addColumnAndData("firstName", parent::getfirstName());
			}
			if(parent::getlastName()!=NULL){
			$builder->addColumnAndData("lastName",parent::getlastName());
			}
			if(parent::getdob()!=NULL){
			$builder->addColumnAndData("dob",parent::getdob());
			}
			if(parent::getcontact()!=NULL){
			$builder->addColumnAndData("contact",parent::getcontact());
			}
			if(parent::getisActive()!=NULL){
			$builder->addColumnAndData("isActive",parent::getIsActive());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tbldriver";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbldriver");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	public function driverIdGeneration()
	{
		$sql="select * from tbldriver";
		$this->con->setSelect_query($sql);
		$no=$this->con->sqlCount();
		$prefix="FST/D/";
		return $prefix.($no+100+1);
	}
	
}

?>
