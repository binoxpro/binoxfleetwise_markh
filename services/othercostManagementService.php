<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/othercostManagement.php';
class OthercostManagementService extends OtherCostManagement {
  
    function __construct() {
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblothercostmanagement");
		if(parent::getDescription()!=""){
		$builder->addColumnAndData("Destination", parent::getDestination());
		}
		if(parent::getamount()!=""){
		$builder->addColumnAndData("amount", parent::getamount());
		}
		if(parent::getvehicleId()!=""){
		$builder->addColumnAndData("vehicleId",parent::getvehicleId());
		}
		if(parent::getCreationDate()!=""){
		$builder->addColumnAndData("creationDate",parent::getCreationDate());
		}
		
		if(parent::getDescription()!=""){
		$builder->addColumnAndData("Description",parent::getDescription());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tblothercostmanagement");
			if(parent::getothercostId()!=NULL){
			$builder->addColumnAndData("othercostId", parent::getothercostId());
			}
			if(parent::getamount()!=NULL){
			$builder->addColumnAndData("amount", parent::getamount());
			}
			if(parent::getvehicleId()!=NULL){
			$builder->addColumnAndData("vehicleId",parent::getvehicleId());
			}
			if(parent::getCreationDate()!=NULL){
		$builder->addColumnAndData("creationDate",parent::getCreationDate());
		}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
		
	//$sqlView="select * from tblvehicle";
	$data=array();	
	
		
	//foreach($this->con->getResultSet($sqlView) as $row2){
		$sql="select ocm.*,v.regNo from tblothercostmanagement ocm inner join tblvehicle v on ocm.vehicleId=v.id";
	   
	   
	   $total=0;
		//array_push($data,$data3);
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["Truck"]=$row['regNo'];
			$data2["Destination"]=$row['Destination'];
			$data2["Description"]=$row['description'];
			$data2["Amount"]=number_format($row['amount'],0);
			$data2["Date"]=$row['creationDate'];
			
		   
		   $total=$total+$row['amount'];
		   array_push($data,$data2);
	   }
	  	 	 $data2=array();
		   	$data2["Truck"]="Total";
			$data2["Destination"]="";
			$data2["Description"]="";
			$data2["Amount"]=$total;
			$data2["Date"]="";
		   
		   array_push($data,$data2);
		   
		   
	   return $data;
    }


	public function viewVehicle() {

		//$sqlView="select * from tblvehicle";
		$data=array();


		//foreach($this->con->getResultSet($sqlView) as $row2){
		$sql="select ocm.*,v.regNo from tblothercostmanagement ocm inner join tblvehicle v on ocm.vehicleId=v.id where ocm.vehicleId='".parent::getvehicleId()."'";


		$total=0;
		//array_push($data,$data3);
		foreach( $this->con->getResultSet($sql) as $row)
		{
			$data2=array();
			$data2["Truck"]=$row['regNo'];
			$data2["Destination"]=$row['Destination'];
			$data2["Description"]=$row['description'];
			$data2["Amount"]=number_format($row['amount'],0);
			$data2["Date"]=$row['creationDate'];


			$total=$total+$row['amount'];
			array_push($data,$data2);
		}
		$data2=array();
		$data2["Truck"]="Total";
		$data2["Destination"]="";
		$data2["Description"]="";
		$data2["Amount"]=$total;
		$data2["Date"]="";

		array_push($data,$data2);


		return $data;
	}

	public function viewDate($sd,$ed) {

		//$sqlView="select * from tblvehicle";
		$data=array();


		//foreach($this->con->getResultSet($sqlView) as $row2){
		$sql="select ocm.*,v.regNo from tblothercostmanagement ocm inner join tblvehicle v on ocm.vehicleId=v.id where ocm.creationDate BETWEEN  '$sd' and '$ed' ";


		$total=0;
		//array_push($data,$data3);
		foreach( $this->con->getResultSet($sql) as $row)
		{
			$data2=array();
			$data2["Truck"]=$row['regNo'];
			$data2["Destination"]=$row['Destination'];
			$data2["Description"]=$row['description'];
			$data2["Amount"]=number_format($row['amount'],0);
			$data2["Date"]=$row['creationDate'];


			$total=$total+$row['amount'];
			array_push($data,$data2);
		}
		$data2=array();
		$data2["Truck"]="Total";
		$data2["Destination"]="";
		$data2["Description"]="";
		$data2["Amount"]=$total;
		$data2["Date"]="";

		array_push($data,$data2);


		return $data;
	}


	public function viewDateVehicle($sd,$ed) {

		//$sqlView="select * from tblvehicle";
		$data=array();


		//foreach($this->con->getResultSet($sqlView) as $row2){
		$sql="select ocm.*,v.regNo from tblothercostmanagement ocm inner join tblvehicle v on ocm.vehicleId=v.id where ocm.vehicleId='".parent::getvehicleId()."' and  ocm.creationDate BETWEEN  '$sd' and '$ed' ";


		$total=0;
		//array_push($data,$data3);
		foreach( $this->con->getResultSet($sql) as $row)
		{
			$data2=array();
			$data2["Truck"]=$row['regNo'];
			$data2["Destination"]=$row['Destination'];
			$data2["Description"]=$row['description'];
			$data2["Amount"]=number_format($row['amount'],0);
			$data2["Date"]=$row['creationDate'];


			$total=$total+$row['amount'];
			array_push($data,$data2);
		}
		$data2=array();
		$data2["Truck"]="Total";
		$data2["Destination"]="";
		$data2["Description"]="";
		$data2["Amount"]=$total;
		$data2["Date"]="";

		array_push($data,$data2);


		return $data;
	}


	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblothercostmanagement");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
}

	
