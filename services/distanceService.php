<?php 
 require_once('../model/distance.php');
 class distanceService extends distance{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbldistance');
	 	 $builder->addColumnAndData('id',parent::getid());
         $builder->addColumnAndData('districtFrom',parent::getdistrictFrom());
         $builder->addColumnAndData('districtTo',parent::getdistrictTo());
         $builder->addColumnAndData('distance',parent::getdistance());
         $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbldistance');

 	 	 if(!is_null(parent::getdistrictFrom())){
            $builder->addColumnAndData('districtFrom',parent::getdistrictFrom());
            }

 	 	 if(!is_null(parent::getdistrictTo())){
        $builder->addColumnAndData('districtTo',parent::getdistrictTo());
        }

 	 	 if(!is_null(parent::getdistance())){
        $builder->addColumnAndData('distance',parent::getdistance());
        }
        $builder->setCriteria("where id='".parent::getid()."'");
        $this->con->setQuery(Director::buildSql($builder));
                 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select *,concat(districtFrom,'-',districtTo) jounery from  tbldistance";
	 	return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tbldistance";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  tbldistance limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbldistance');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
     {
	 	 $sql="select * from  tbldistance  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
            parent::setid($row['id']);
            parent::setdistrictFrom($row['districtFrom']);
            parent::setdistrictTo($row['districtTo']);
            parent::setdistance($row['distance']);
	 	}
     }

 	 public function view_query($sql)
     {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>