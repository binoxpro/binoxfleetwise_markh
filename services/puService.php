<?php 
 require_once('../model/pu.php');
 class puService extends pu{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblpartsused');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('numberplate',parent::getnumberplate());
 		 	 $builder->addColumnAndData('partusedId',parent::getpartusedId());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('cost',parent::getcost());
 		 	 $builder->addColumnAndData('jobcardId',parent::getjobcardId());
 		 	 $builder->addColumnAndData('dateofrepair',parent::getdateofrepair());
 		 	 $builder->addColumnAndData('typeofrepair',parent::gettypeofrepair());
 		 	 $builder->addColumnAndData('supplierId',parent::getsupplierId());
 		 	 $builder->addColumnAndData('supplied',parent::getsupplied());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblpartsused');

 	 	 if(!is_null(parent::getnumberplate())){
$builder->addColumnAndData('numberplate',parent::getnumberplate()); 
}

 	 	 if(!is_null(parent::getpartusedId())){
$builder->addColumnAndData('partusedId',parent::getpartusedId()); 
}

 	 	 if(!is_null(parent::getquantity())){
$builder->addColumnAndData('quantity',parent::getquantity()); 
}

 	 	 if(!is_null(parent::getcost())){
$builder->addColumnAndData('cost',parent::getcost()); 
}

 	 	 if(!is_null(parent::getjobcardId())){
$builder->addColumnAndData('jobcardId',parent::getjobcardId()); 
}

 	 	 if(!is_null(parent::getdateofrepair())){
$builder->addColumnAndData('dateofrepair',parent::getdateofrepair()); 
}

 	 	 if(!is_null(parent::gettypeofrepair())){
$builder->addColumnAndData('typeofrepair',parent::gettypeofrepair()); 
}

 	 	 if(!is_null(parent::getsupplierId())){
$builder->addColumnAndData('supplierId',parent::getsupplierId()); 
}

 	 	 if(!is_null(parent::getsupplied())){
$builder->addColumnAndData('supplied',parent::getsupplied()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tblpartsused";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tblpartsused";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  tblpartsused limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblpartsused');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tblpartsused  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setnumberplate($row['numberplate']);
	 	parent::setpartusedId($row['partusedId']);
	 	parent::setquantity($row['quantity']);
	 	parent::setcost($row['cost']);
	 	parent::setjobcardId($row['jobcardId']);
	 	parent::setdateofrepair($row['dateofrepair']);
	 	parent::settypeofrepair($row['typeofrepair']);
	 	parent::setsupplierId($row['supplierId']);
	 	parent::setsupplied($row['supplied']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>