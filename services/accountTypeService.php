<?php 
 require_once('../model/accountType.php');
 class accountTypeService extends accountType{
 private $data2=array();
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('accounttypes');
	 	 $builder->addColumnAndData('PrefixCode',parent::getPrefixCode());
 		 	 $builder->addColumnAndData('AccountTypeName',parent::getAccountTypeName());
 		 	 $builder->addColumnAndData('AccountBalanceType',parent::getAccountBalanceType());
 		 	 $builder->addColumnAndData('ParentPrefixCode',parent::getParentPrefixCode());
 		 	 $builder->addColumnAndData('IsActive',parent::getIsActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('accounttypes');

 	 	 if(!is_null(parent::getAccountTypeName())){
$builder->addColumnAndData('AccountTypeName',parent::getAccountTypeName()); 
}

 	 	 if(!is_null(parent::getAccountBalanceType())){
$builder->addColumnAndData('AccountBalanceType',parent::getAccountBalanceType()); 
}

 	 	 if(!is_null(parent::getParentPrefixCode())){
$builder->addColumnAndData('ParentPrefixCode',parent::getParentPrefixCode()); 
}

 	 	 if(!is_null(parent::getIsActive())){
$builder->addColumnAndData('IsActive',parent::getIsActive()); 
}
$builder->setCriteria("where PrefixCode='".parent::getPrefixCode()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  accounttypes";
		 $data=array();
		 $data2=array();
		 $data2['PrefixCode']="0";
		 $data2['AccountTypeName']="None";
		 array_push($data,$data2);
	 	foreach($this->con->getResultSet($sql) as $row)
		{
			array_push($data,$row);
		}
		 return $data;
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  accounttypes where ParentPrefixCode=0";
		$this->con->setSelect_query($sql);

		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  accounttypes where ParentPrefixCode='0' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 //$row['ParentId']=$row['ParentPrefixCode'];
			 array_push($this->data2,$row);
			 if($this->hasChildren($row['PrefixCode']))
			 {

				 $this->viewTree($row['PrefixCode'],$this->data2);
			 }

		 }
		 $data["rows"]=$this->data2;
		 return $data;
	 }
	 public function viewTree($parentId)
	 {
		 //$data=array();
		 $sql="select * from  accounttypes where ParentPrefixCode='$parentId'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $row['_parentId']=$row['ParentPrefixCode'];
			 array_push($this->data2,$row);
			 if($this->hasChildren($row['PrefixCode']))
			 {
				 $this->viewTree($row['PrefixCode']);
			 }
		 }
		 //return $data;

	 }
	 public function hasChildren($parentId)
	 {
		 $val=false;
		 $sql="select * from  accounttypes where ParentPrefixCode='$parentId'";
		 $this->con->setSelect_query($sql);
		 if($this->con->sqlCount()>0)
		 {
			 $val=true;
		 }
		 return $val;


	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('accounttypes');
	 	 $builder->setCriteria("where PrefixCode='".parent::getPrefixCode()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  accounttypes  where PrefixCode='".parent::getPrefixCode()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setPrefixCode($row['PrefixCode']);
	 	parent::setAccountTypeName($row['AccountTypeName']);
	 	parent::setAccountBalanceType($row['AccountBalanceType']);
	 	parent::setParentPrefixCode($row['ParentPrefixCode']);
	 	parent::setIsActive($row['IsActive']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>