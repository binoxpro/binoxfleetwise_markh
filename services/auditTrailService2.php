<?php 
 require_once('model/auditTrail2.php');
 class auditTrailService extends auditTrail{
	 	 public function save()
		 {
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblaudit');
	 	 $builder->addColumnAndData('id',parent::getid());
		 $builder->addColumnAndData('userId',parent::getuserId());
		 $builder->addColumnAndData('url',parent::geturl());
		 $builder->addColumnAndData('creationdate',parent::getcreationdate());
		 $builder->addColumnAndData('timeCreation',parent::gettimeCreation());
		 $builder->addColumnAndData('Ipadderess',parent::getIpadderess());
		 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  	}
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblaudit');

 	 	 if(!is_null(parent::getuserId()))
		 {
			$builder->addColumnAndData('userId',parent::getuserId());
		 }

 	 	 if(!is_null(parent::geturl()))
		 {
			$builder->addColumnAndData('url',parent::geturl());
		 }

 	 	 if(!is_null(parent::getcreationdate()))
		 {
			$builder->addColumnAndData('creationdate',parent::getcreationdate());
		 }

 	 	 if(!is_null(parent::gettimeCreation())){
$builder->addColumnAndData('timeCreation',parent::gettimeCreation()); 
}

 	 	 if(!is_null(parent::getIpadderess())){
$builder->addColumnAndData('Ipadderess',parent::getIpadderess()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblaudit";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblaudit');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>