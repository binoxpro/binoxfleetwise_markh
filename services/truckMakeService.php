<?php 
 require_once('../model/truckMake.php');
 class truckMakeService extends truckMake{
	 	 public function save()
         {
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblvehiclemake');
	 	 //$builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('makeName',parent::getmakeName());
 		 	 $builder->addColumnAndData('Prefered',parent::getPrefered());
 		 	 $this->con->setQuery(Director::buildSql($builder));
             return $this->con->execute_query2($builder->getValues());
 	    }
 
	 	 public function update()
         {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblvehiclemake');

 	 	 if(!is_null(parent::getmakeName()))
 	 	 {
            $builder->addColumnAndData('makeName',parent::getmakeName());
         }

 	 	 if(!is_null(parent::getPrefered()))
 	 	 {
            $builder->addColumnAndData('Prefered',parent::getPrefered());
         }
            $builder->setCriteria("where id='".parent::getid()."'");
            $this->con->setQuery(Director::buildSql($builder));
	 	    return $this->con->execute_query();
 }
 

 	 public function view()
     {
	 	 $sql="select *,CASE when tblvehiclemake.Prefered =0 then 'Not' else 'Prefered' END preview from  tblvehiclemake ORDER BY tblvehiclemake.Prefered desc";
         return $this->con->getResultSet($sql);
 	 }

 	 public function delete()
     {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblvehiclemake');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
		 	$sql="select * from  tblvehiclemake  where id='".parent::getid()."'";
	 	 	foreach($this->con->getResultSet($sql) as $row)
			{
                parent::setid($row['id']);
                parent::setmakeName($row['makeName']);
                parent::setPrefered($row['Prefered']);
 			}
	 }

     public function findMakeId($str)
     {
         //set the id to invalid
 	     parent::setid(-1);
         $sql="select * from  tblvehiclemake  where makeName='".$str."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setmakeName($row['makeName']);
             parent::setPrefered($row['Prefered']);
         }

     }

 	 public function view_query($sql)
     {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>