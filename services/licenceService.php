<?php
include_once '../model/licence.php';
class LicenceService extends Licence
{
	public function __construct()
	{
		parent::__construct();
	}


	public function save()
	{
		$this->updateNewLicence();

		$builder = new InsertBuilder();
		$builder->setTable("tbllicence");
        //$builder->addColumnAndData("id", parent::getid());
		$builder->addColumnAndData("licenceNo", parent::getlicenceNo());
		$builder->addColumnAndData("issueDate", parent::getissueDate());
		$builder->addColumnAndData("expiryDate", parent::getexpiryDate());
		$builder->addColumnAndData("isActive", parent::getisActive());
		$builder->addColumnAndData("driverId", parent::getdriverId());
		$builder->addColumnAndData("parentId", parent::getparentId());
		$builder->addColumnAndData("licenseClass", parent::getlicenceClass());

		$this->con->setQuery(Director::buildSql($builder));
		echo Director::buildSql($builder);
		return $this->con->execute_query2($builder->getValues());
		//parent::setid($this->con->getId());
	}


	public function update()
	{
		$builder = new UpdateBuilder();
		$builder->setTable("tbllicence");

		$builder->addColumnAndData("licenceNo", parent::getlicenceNo());
		$builder->addColumnAndData("issueDate", parent::getissueDate());
		$builder->addColumnAndData("expiryDate", parent::getexpiryDate());
		if (parent::getisActive() != "") {
			$builder->addColumnAndData("isActive", parent::getisActive());
		}
		$builder->addColumnAndData("driverId", parent::getdriverId());
		if (parent::getparentId() != "") {
			$builder->addColumnAndData("parentId", parent::getparentId());
		}

		$builder->addColumnAndData("licenseClass", parent::getlicenceClass());

		$builder->setCriteria("where id='" . parent::getid() . "'");
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();

	}


	public function updateNewLicence()
	{
		$builder = new UpdateBuilder();
		$builder->setTable("tbllicence");


		$builder->addColumnAndData("isActive", false);


		$builder->setCriteria("where driverId='" . parent::getdriverId() . "'");
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();

	}

	public function view()
	{
		$sql = "select li.*, CONCAT(concat(dr.firstName,' '),dr.lastName) driverName,Case when li.isActive=1 then 'Current' else 'Not In Use' end IsActive   from tbllicence li inner join tbldriver dr on li.driverId=dr.id";
		$data = array();
		foreach ($this->con->getResultSet($sql) as $row) {
			$datev = strtotime(date('Y-m-d'));
			$sec = $datev + (3 * 3600 * 24);

			$data2 = array();
			$data2['driverName'] = $row['driverName'];
			$data2['licenceNo'] = $row['licenceNo'];
			$data2['issueDate'] = $row['issueDate'];
			$data2['expiryDate'] = $row['expiryDate'];
			$data2['licenseClass'] = $row['licenseClass'];
			if (!is_null($row['expiryDate'])) {
				$tp = strtotime($row['expiryDate']);
				if ($tp <= $sec) {
					$data2['Status'] = "ex";
				} else {
					$data2['Status'] = "on";
				}
			} else {
				$data2['Status'] = "ep";
			}

			array_push($data, $data2);
		}
		return $data;
	}


	public function delete()
	{
		$builder = new DeleteBuilder();
		$builder->setTable("tbllicence");
		$builder->setCriteria("where id='" . parent::getid() . "'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}

	public function view_query($sql)
	{
		return $this->con->getResultSet($sql);
	}

	public function getObject()
	{
		$sql = "select * from  tbllicence where driverId='" . parent::getid() . "' and isActive=1";
		$data=array();
		$row1;
		foreach ($this->con->getResultSet($sql) as $row)
		{
			array_push($data,$row);
			$row1=$row;
		}
		return $row1;


	}
    public function autoIncrement()
    {
        $sql = "select * from  tbllicence";
        $data=array();
        $row1=0;
        $this->con->setSelect_query($sql);
        $row1=$this->con->sqlCount() +1;
        return $row1;


    }

}

?>
