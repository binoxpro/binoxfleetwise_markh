<?php 
 require_once('../model/truckModel.php');
 class truckModelService extends truckModel{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltruckmodel');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('modelName',parent::getmodelName());
 		 	 $builder->addColumnAndData('makeId',parent::getmakeId());
 		 	 $builder->addColumnAndData('yearManufacture',parent::getyearManufacture());
 		 	 $builder->addColumnAndData('country',parent::getcountry());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltruckmodel');

 	 	 if(!is_null(parent::getmodelName())){
$builder->addColumnAndData('modelName',parent::getmodelName()); 
}

 	 	 if(!is_null(parent::getmakeId())){
$builder->addColumnAndData('makeId',parent::getmakeId()); 
}

 	 	 if(!is_null(parent::getyearManufacture())){
$builder->addColumnAndData('yearManufacture',parent::getyearManufacture()); 
}

 	 	 if(!is_null(parent::getcountry())){
$builder->addColumnAndData('country',parent::getcountry()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }


	 public function viewCombo($makeId)
	 {
		 $sql = "";
		 if(!is_null($makeId))
		 {
			 $sql = "select tm.*,vmk.makeName from  tbltruckmodel tm inner join tblvehiclemake vmk on vmk.id=tm.makeId where tm.makeId='$makeId'";

		 }else {
			 $sql = "select tm.*,vmk.makeName from  tbltruckmodel tm inner join tblvehiclemake vmk on vmk.id=tm.makeId";
		 }

		 return $this->con->getResultSet($sql);
	 }

 	 public function view()
	 {

		 $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		 $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
		 $offset = ($page-1)*$rows;

		 $sql="select tm.*,vmk.makeName from  tbltruckmodel tm inner join tblvehiclemake vmk on vmk.id=tm.makeId";

		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data['total']=$this->con->sqlCount();
		 $sql="select tm.*,vmk.makeName from  tbltruckmodel tm inner join tblvehiclemake vmk on vmk.id=tm.makeId limit $offset,$rows";

		 foreach($this->con->getResultSet($sql) as $row)
		 {

			 array_push($data2,$row);

		 }
		 $data['rows']=$data2;
		 return $data;

 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltruckmodel');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
     {
         $sql = "select * from  tbltruckmodel  where id='" . parent::getid() . "'";
         foreach ($this->con->getResultSet($sql) as $row) {
             parent::setid($row['id']);
             parent::setmodelName($row['modelName']);
             parent::setmakeId($row['makeId']);
             parent::setyearManufacture($row['yearManufacture']);
             parent::setcountry($row['country']);
             parent::setisActive($row['isActive']);
         }


     }

     //find
     public function findModel()
     {
         //set for invalid
         parent::setid(-1);
         $sql = "select * from  tbltruckmodel  where modelName='" . parent::getmodelName() . "' and makeId='" . parent::getmakeId() . "'";
         foreach ($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setmodelName($row['modelName']);
             parent::setmakeId($row['makeId']);
             parent::setyearManufacture($row['yearManufacture']);
             parent::setcountry($row['country']);
             parent::setisActive($row['isActive']);
         }


     }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>