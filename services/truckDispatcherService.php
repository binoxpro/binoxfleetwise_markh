<?php
include_once '../model/truckDispatcher.php';
class TruckDispatcherService extends TruckDispatcher
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbldispatcher");
		if(parent::gettimeinDepo()!=NULL){
		$builder->addColumnAndData("timeinDepo", parent::gettimeinDepo());
		}
		if(parent::gettimeoutDepo()!=NULL){
		$builder->addColumnAndData("timeoutDepo", parent::gettimeoutDepo());
		}
		if(parent::getdispatcher()!=NULL){
		$builder->addColumnAndData("dispatcher",parent::getdispatcher());
		}
		if(parent::getAllocationId()!=NULL){
		$builder->addColumnAndData("allocationId",parent::getAllocationId());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tbldispatcher");
			if(!is_null(parent::gettimeinDepo())){
			$builder->addColumnAndData("timeinDepo", parent::gettimeinDepo());
			}
			if(!is_null(parent::gettimeoutDepo())){
			$builder->addColumnAndData("timeoutDepo", parent::gettimeoutDepo());
			}
			if(!is_null(parent::getdispatcher())){
			$builder->addColumnAndData("dispatcher",parent::getdispatcher());
			}
			if(!is_null(parent::getAllocationId())){
			$builder->addColumnAndData("allocationId",parent::getAllocationId());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       
    	//$date=date('y-m-d');
       $sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId ,(select v.regNo from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) truckNo ,(select al.id from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) eid FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where os.name='Allocated'";
	   $data=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["mailingName"]=$row["mailingName"];
		   $data2["orderDate"]=$row["orderDate"];
		   $data2["promiseDate"]=$row["promiseDate"];
		   $data2["orderNumber"]=$row["orderNumber"];
		   $data2["ordertypeId"]=$row["ordertypeId"];
		   $data2["qtyOrder"]=$row["qtyOrder"];
		   $data2["statusorderId"]=$row["statusorderId"];
		   $data2["orderstatusId"]=$row["orderstatusId"];
		   $data2["itemcodeId"]=$row["itemcodeId"];
		   $data2["truck"]=$row["truckNo"];
		   $data2["eid"]=$row["eid"];
		   $eid=$data2["eid"];
		   $sqlInner="select dp.* from tbldispatcher dp where dp.AllocationId='$eid'";
		   $timeIn="";
		   $timeOut="";
		   $dispatcher="";
		   $allocationId="";
		   $did="";
		   foreach($this->con->getResultSet($sqlInner) as $row2)
		   {
			   $timeIn=$row2["timeinDepo"];
			   $timeOut=$row2["timeoutDepo"];
			   $dispatcher=$row2["dispatcher"];
			   $allocationId=$row2["AllocationId"];
			   $did=$row2['id'];
			   
		   }
		   	$data2['depotintime']=$timeIn;
			$data2['depotouttime']=$timeOut;
			$data2['dispatcher']=$dispatcher;
			//$data2['allocationId']=$allocationId; 
			$data2['did']=$did; 
		   
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($data,$data2);
	   }
	   return $data;
   
    
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbldispatcher");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
