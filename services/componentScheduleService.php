<?php 
 require_once('../model/componentSchedule.php');
 class componentScheduleService extends componentSchedule{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblcomponentschedule');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('serviceIntervalId',parent::getserviceIntervalId());
 		 	 $builder->addColumnAndData('componentModelId',parent::getcomponentModelId());
 		 	 $builder->addColumnAndData('actionNoteId',parent::getactionNoteId());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblcomponentschedule');

 	 	 if(!is_null(parent::getserviceIntervalId())){
				$builder->addColumnAndData('serviceIntervalId',parent::getserviceIntervalId()); }

 	 	 if(!is_null(parent::getcomponentModelId())){
			$builder->addColumnAndData('componentModelId',parent::getcomponentModelId()); }

 	 	 if(!is_null(parent::getactionNoteId())){
			$builder->addColumnAndData('actionNoteId',parent::getactionNoteId()); }

 	 	 if(!is_null(parent::getcreationDate())){
			$builder->addColumnAndData('creationDate',parent::getcreationDate()); }

 	 	 if(!is_null(parent::getisActive())){
			$builder->addColumnAndData('isActive',parent::getisActive());
	 }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="Select cs.id,c.componentName,(select an.detail from tblactionnote an where an.id=cs.actionNoteId ) actionNote from tblcomponentschedule cs inner join tblmodelcomponent mc on cs.componentModelId=mc.id inner join tblcomponent c on mc.componentId=c.id where cs.serviceIntervalId='".parent::getserviceIntervalId()."'";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete()
     {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblcomponentschedule');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>