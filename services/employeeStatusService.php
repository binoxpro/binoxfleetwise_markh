<?php 
 require_once('../model/employeeStatus.php');
 class employeeStatusService extends employeeStatus{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('employeestatus');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('empNo',parent::getempNo());
 		 	 $builder->addColumnAndData('positionId',parent::getpositionId());
 		 	 $builder->addColumnAndData('startDate',parent::getstartDate());
 		 	 $builder->addColumnAndData('endDate',parent::getendDate());
 		 	 $builder->addColumnAndData('contractType',parent::getcontractType());
 		 	 $builder->addColumnAndData('payrateId',parent::getpayrateId());
 		 	 $builder->addColumnAndData('NssfRequired',parent::getNssfRequired());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('employeestatus');

 	 	 if(!is_null(parent::getempNo())){
$builder->addColumnAndData('empNo',parent::getempNo()); 
}

 	 	 if(!is_null(parent::getpositionId())){
$builder->addColumnAndData('positionId',parent::getpositionId()); 
}

 	 	 if(!is_null(parent::getstartDate())){
$builder->addColumnAndData('startDate',parent::getstartDate()); 
}

 	 	 if(!is_null(parent::getendDate())){
$builder->addColumnAndData('endDate',parent::getendDate()); 
}

 	 	 if(!is_null(parent::getcontractType())){
$builder->addColumnAndData('contractType',parent::getcontractType()); 
}

 	 	 if(!is_null(parent::getpayrateId())){
$builder->addColumnAndData('payrateId',parent::getpayrateId()); 
}

 	 	 if(!is_null(parent::getNssfRequired())){
$builder->addColumnAndData('NssfRequired',parent::getNssfRequired()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select es.*,(select phr.position from tblposition phr where phr.id=es.positionId) positionName from  employeestatus es where es.empNo='".parent::getempNo()."'";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  employeestatus";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select es.*,(select phr.position from tblposition phr where phr.id=es.positionId) positionName from  employeestatus es where es.empNo='".parent::getempNo()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('employeestatus');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  employeestatus  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setempNo($row['empNo']);
	 	parent::setpositionId($row['positionId']);
	 	parent::setstartDate($row['startDate']);
	 	parent::setendDate($row['endDate']);
	 	parent::setcontractType($row['contractType']);
	 	parent::setpayrateId($row['payrateId']);
	 	parent::setNssfRequired($row['NssfRequired']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>