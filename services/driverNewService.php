<?php 
 require_once('../model/driverNew.php');
 class driverNewService extends driverNew{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbldriver');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('firstName',parent::getfirstName());
 		 	 $builder->addColumnAndData('lastName',parent::getlastName());
 		 	 $builder->addColumnAndData('dob',parent::getdob());
 		 	 $builder->addColumnAndData('contact',parent::getcontact());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 
 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbldriver');

 	 	 if(!is_null(parent::getfirstName())){
$builder->addColumnAndData('firstName',parent::getfirstName()); 
}

 	 	 if(!is_null(parent::getlastName())){
$builder->addColumnAndData('lastName',parent::getlastName()); 
}

 	 	 if(!is_null(parent::getdob())){
$builder->addColumnAndData('dob',parent::getdob()); 
}

 	 	 if(!is_null(parent::getcontact())){
$builder->addColumnAndData('contact',parent::getcontact()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select *,concat(firstName,'-',lastName) name from  tbldriver";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbldriver');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }

     public function findCriteria($firstName,$lastName)
     {
         $sql="select * from tbldriver where firstName like '%".$firstName."%' and lastName like '%".$lastName."%' or lastName like '%".$firstName."%' and firstName like '%".$lastName."%'";
         //$sql="select * from  tbldriver  where (firstName='".$firstName."' and lastName='".$lastName."')";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setfirstName($row['firstName']);
             parent::setlastName($row['lastName']);
             parent::setdob($row['dob']);
             parent::setcontact($row['contact']);
             parent::setisActive($row['isActive']);
             /*parent::setchaseNumber($row['chaseNumber']);
             parent::setengineCapacityCC($row['enginecapacitycc']);
             parent::setvehicleTypeId($row['vehicleTypeId']);
             parent::settonnage($row['tonnage']);
             parent::setcolor($row['color']);
             parent::setfuel($row['fuel']);
             parent::setstatus($row['status']);
             parent::setpurpose($row['purpose']);
             parent::setHasTrailer($row['hasTrailer']);
             parent::setIsTrailer($row['isTrailer']);*/
         }
     }




 }
?>