<?php
include_once '../model/vehicle.php';
class VehicleService extends Vehicle
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblvehicle");
		$builder->addColumnAndData("regNo", parent::getregNo());
		$builder->addColumnAndData("configuration", parent::getconfiguration());
		$builder->addColumnAndData("model", parent::getmodel());
		$builder->addColumnAndData("axel", parent::getaxel());
		$builder->addColumnAndData("cardNumber", parent::getcardNumber());
		$builder->addColumnAndData("countryMake", parent::getcountryMake());
		$builder->addColumnAndData("vehicletypeId", parent::getvehicletypeId());
		$builder->addColumnAndData("capacty", parent::getcapacity());
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query2($builder->getValues());
		return $this->con->getId();
		  
    }
	
	public function saveDriver($driverId,$vehicleId,$isActive) 
	{
		$this->updatevehicleDriver($driverId);
		$builder=new InsertBuilder();
		$builder->setTable("tbldrivervehicle");
		$builder->addColumnAndData("driverId", $driverId);
		$builder->addColumnAndData("vehicleId", $vehicleId);
		$builder->addColumnAndData("isActive", $isActive);
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());
		//return $this->con->getId();
		  
    }
	
    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblvehicle");
			$builder->addColumnAndData("regNo", parent::getregNo());
            $builder->addColumnAndData("configuration", parent::getconfiguration());
            $builder->addColumnAndData("model", parent::getmodel());
			$builder->addColumnAndData("axel", parent::getaxel());
			$builder->addColumnAndData("cardNumber", parent::getcardNumber());
			$builder->addColumnAndData("countryMake", parent::getcountryMake());
			$builder->addColumnAndData("vehicletypeId", parent::getvehicletypeId());
			$builder->addColumnAndData("Capacty", parent::getcapacity());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function updatevehicleDriver($driver) 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbldrivervehicle");
			$builder->addColumnAndData("isActive", false);
            
			$builder->setCriteria("where driverId='$driver'");
			$this->con->setQuery(Director::buildSql($builder));
			 $this->con->execute_query();
		
    }

	public function view() {
       //$sql="select v.*,vt.*,v.id idV from tblvehicle v inner join tblvehicletype vt on v.vehicletypeId=vt.id order by v.vehicletypeId";
        $sql="SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,concat(v.regNo,'(',vm.makeName,'/',tm.modelName,'/',vt.vehicleType,')') vehicle FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON tm.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id  where (v.isTrailer='0' and v.hasTrailer='1') or (v.isTrailer='0' and v.hasTrailer='0') ";


        $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {

		   array_push($data,$row);
	   }
	   return $data;
    }
	
	public function setVehicle() {
       $sql="select v.*,vt.*,v.id idV from tblvehicle v inner join tblvehicletype vt on v.vehicletypeId=vt.id where v.RegNo='"."' order by v.vehicletypeId";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   parent::setid($row["idV"]);
		   
	   }
	   //return $data;
    }
	public function setModelNow() {
       $sql="SELECT v.*,tm.modelName,vm.makeName,vt.vehicleType,vc.configurationName FROM  tblvehicle v INNER JOIN tbltruckmodel tm ON  v.modelId=tm.id INNER JOIN tblvehiclemake vm ON v.makeId=vm.id INNER JOIN tblvehicletype vt ON v.vehicleTypeId=vt.id INNER JOIN tblvehicleconfiguration vc ON v.configurationId=vc.id v.id='".parent::getid()."'";
	   
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   
		   parent::setmodel($row["makeName"]);
		   
	   }
	   
    }
	public function returnVehicle($regNo) {
		$id=-1;
       $sql="select id from tblvehicle where regNo='$regNo'";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   $id=$row['id'];
		   
	   }
	   return $id;
    }
	
	public function returnVehicleNumber($regNo) {
		$id=-1;
       $sql="select regNo from tblvehicle where id='$regNo'";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   $id=$row['regNo'];
		   
	   }
	   return $id;
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblvehicle");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
