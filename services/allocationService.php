<?php
include_once '../model/allocation.php';
class AllocationService extends Allocation
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblallocation");
		$builder->addColumnAndData("truckId", parent::gettruckId());
		$builder->addColumnAndData("orderId", parent::getorderId());
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query2($builder->getValues());	
		parent::setid($this->con->getId());	
		   
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblallocation");
			if(intval(parent::gettruckId())!=0){
			$builder->addColumnAndData("truckId", parent::gettruckId());
			}
			//$builder->addColumnAndData("orderId", parent::getorderId());
			
			//$builder->addColumnAndData("ordertypeId", parent::getordertypeId());
			$builder->setCriteria("where id='".parent::getid()."' and orderId='".parent::getorderId()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }


	public function view() {
    	//$date=date('y-m-d');
       $sql="SELECT (select al.truckId from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) truckId, o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId ,(select v.regNo from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) truckNo ,(select al.id from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) eid FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where  (os.name='Ready' or os.name='Allocated')";
	   $data=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["mailingName"]=$row["mailingName"];
		   $data2["orderDate"]=$row["orderDate"];
		   $data2["promiseDate"]=$row["promiseDate"];
		   $data2["orderNumber"]=$row["orderNumber"];
		   $data2["ordertypeId"]=$row["ordertypeId"];
		   $data2["qtyOrder"]=$row["qtyOrder"];
		   $data2["statusorderId"]=$row["statusorderId"];
		   $data2["orderstatusId"]=$row["orderstatusId"];
		   $data2["itemcodeId"]=$row["itemcodeId"];
		   $data2["truck"]=$row["truckNo"];
		   //$data2['truckid']=$row['truckId'];
		   $data2["eid"]=$row["eid"];
		   
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($data,$data2);
	   }
	   return $data;
   
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbltrailer");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
	}
}

?>
