<?php 
 require_once('../model/trailerAssigment.php');
 class trailerAssigmentService extends trailerAssigment{
	 	 public function save()
		 {
			 $builder=new InsertBuilder();
			 $builder->setTable('tbltrailer');
			 $builder->addColumnAndData('id',parent::getid());
			 $builder->addColumnAndData('tractorId',parent::gettractorId());
			 $builder->addColumnAndData('trailerId',parent::gettrailerId());
			 $builder->addColumnAndData('assignmentDate',parent::getassignmentDate());
			 $builder->addColumnAndData('addBy',parent::getaddBy());
			 $builder->addColumnAndData('driverId',parent::getdriverId());
			 $builder->addColumnAndData('isActive',parent::getIsActive());
			 $this->con->setQuery(Director::buildSql($builder));
			 return $this->con->execute_query2($builder->getValues());
 	  	}
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltrailer');

 	 	 if(!is_null(parent::gettractorId()))
 	 	 {
        $builder->addColumnAndData('tractorId',parent::gettractorId());
        }

 	 	 if(!is_null(parent::gettrailerId()))
 	 	 {
        $builder->addColumnAndData('trailerId',parent::gettrailerId());
        }

 	 	 if(!is_null(parent::getassignmentDate())){
$builder->addColumnAndData('assignmentDate',parent::getassignmentDate()); 
}

 	 	 if(!is_null(parent::getaddBy())){
$builder->addColumnAndData('addBy',parent::getaddBy()); 
}

 	 	 if(!is_null(parent::getdriverId())){
$builder->addColumnAndData('driverId',parent::getdriverId()); 
}
			 if(!is_null(parent::getIsActive())){
				 $builder->addColumnAndData('isActive',parent::getIsActive());
			 }
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
	 	 	return $this->con->execute_query();
 }


	 public function updateAllVehicle(){
		 $builder=new UpdateBuilder();
		 $builder->setTable('tbltrailer');



		 $builder->addColumnAndData('isActive',false);

		 $builder->setCriteria("where tractorId='".parent::gettractorId()."'");
		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }


 	 public function viewConbox(){
	 	 $sql="select * from  tbltrailer";
	 	return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
	 	$sql="select t.*,concat(e.firstName,' ',e.lastName) fullName,(select v.regNo from tblvehicle v where v.id=t.tractorId) tractorHead,(select v2.regNo from tblvehicle v2 where v2.id=t.trailerId) trailer from  tbltrailer t inner join employee e on e.empNo=t.driverId where t.isActive='1'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select t.*,concat(e.firstName,' ',e.lastName) fullName,(select v.regNo from tblvehicle v where v.id=t.tractorId) tractorHead,(select v2.regNo from tblvehicle v2 where v2.id=t.trailerId) trailer from  tbltrailer t inner join employee e on e.empNo=t.driverId where t.isActive='1' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
			}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltrailer');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  tbltrailer  where id='".parent::getid()."'";

	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
				parent::setid($row['id']);
				parent::settractorId($row['tractorId']);
				parent::settrailerId($row['trailerId']);
				parent::setassignmentDate($row['assignmentDate']);
				parent::setaddBy($row['addBy']);
				parent::setdriverId($row['driverId']);
				parent::setIsActive($row['isActive']);
		}
	 }

	 public function findActive()
	 {
		 $sql="select * from  tbltrailer  where id='".parent::gettractorId()."' and isActive='1'";

		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 parent::settractorId($row['tractorId']);
			 parent::settrailerId($row['trailerId']);
			 parent::setassignmentDate($row['assignmentDate']);
			 parent::setaddBy($row['addBy']);
			 parent::setdriverId($row['driverId']);
			 parent::setIsActive($row['isActive']);
		 }
	 }




 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>