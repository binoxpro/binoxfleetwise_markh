<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcademicYearService
 *
 * @author plussoft
 * 
 */
include_once '../model/maintainance.php';

class MaintainanceService extends Maintainance{
    
   
    
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() {
       
            
            $builder=new InsertBuilder();
            $builder->setTable("tbl_maintainance");
            $builder->addColumnAndData("workDone", parent::getWorkID());
            $builder->addColumnAndData("numberPlate", parent::getNumberPlate());
			$builder->addColumnAndData("reg_date",date('Y-m-d'));
            $this->con->setQuery(Director::buildSql($builder));
            //$this->con->setSelect_query("select * from tbl_maintainance where numberPlate='".parent::getNumberPlate()."'");
			//if($this->con->sqlCount()<1){
            return $this->con->execute_query2($builder->getValues());
			//}else{
				//return array('msg'=>'The class is ready set up');
			//}
            
       
       // parent::save();
    }
	
	
	public function saveDriver($n,$d,$e){
		
       
            
            $builder=new InsertBuilder();
            $builder->setTable("tbl_driver");
            $builder->addColumnAndData("names", $n);
            $builder->addColumnAndData("drivingClass", $d);
			$builder->addColumnAndData("expiry",$e);
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_driver where names='".$n."'");
			if($this->con->sqlCount()<1){
            return $this->con->execute_query2($builder->getValues());
			}else{
				return array('msg'=>'The driver is names is avaliable');
			}
            
       
       // parent::save();
    
	}
    public function update() {
        $builder=new UpdateBuilder();
		$builder->setTable("tbl_maintainance");
        $builder->addColumnAndData("workDone", parent::getWorkID());
        $builder->addColumnAndData("numberPlate", parent::getNumberPlate());
		$builder->addColumnAndData("reg_date",date('Y-m-d'));
		$builder->setCriteria("where mID=".parent::getMID());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();
		
    }

    public function view2() {
       $sql="select * from tbl_partsused where mID=".parent::getMID();
	   return $this->con->getResultSet($sql);
    }
	public function view3() {
       $sql="select * from tbl_driver";
	   return $this->con->getResultSet($sql);
    }

	public function view() {
       $sql="select * from tbl_maintainance ";
	   return $this->con->getResultSet($sql);
    }

	public function view_report() {
       $sql="select * from tbl_maintainance where numberPlate='".parent::getNumberPlate()."'";
	   return $this->con->getResultSet($sql);
    }
	public function getMIDNo() {
       $sql="select * from tbl_maintainance ";
	   
	   $this->con->setSelect_query($sql);
	   return $this->con->sqlCount()+1;
    }
    public function view_query($sql) {
        parent::view_query($sql);
    }
	
	public function savePart() {
       
            
            $builder=new InsertBuilder();
            $builder->setTable("tbl_partsused");
            $builder->addColumnAndData("partUsed", parent::getPartUsed());
            $builder->addColumnAndData("mID", parent::getMID());
			//$builder->addColumnAndData("reg_date",date('Y-m-d'));
            $this->con->setQuery(Director::buildSql($builder));
            //$this->con->setSelect_query("select * from tbl_maintainance where numberPlate='".parent::getNumberPlate()."'");
			//if($this->con->sqlCount()<1){
            return $this->con->execute_query2($builder->getValues());
			//}else{
				//return array('msg'=>'The class is ready set up');
			//}
            
       
       // parent::save();
    }


	public function updatePart() {
        $builder=new UpdateBuilder();
		 $builder->setTable("tbl_partsused");
		 $builder->addColumnAndData("partUsed", parent::getPartUsed());
        $builder->addColumnAndData("mID", parent::getMID());
		$builder->setCriteria("where partID=".parent::getPartID());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();
		
    }
    //put your code here
}
?>