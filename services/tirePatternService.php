<?php 
 require_once('../model/tirePattern.php');
 class tirePatternService extends tirePattern{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltirepattern');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('name',parent::getname());
 		 	 $builder->addColumnAndData('description',parent::getdescription());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltirepattern');

 	 	 if(!is_null(parent::getname())){
$builder->addColumnAndData('name',parent::getname()); 
}

 	 	 if(!is_null(parent::getdescription())){
$builder->addColumnAndData('description',parent::getdescription()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltirepattern";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltirepattern');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltirepattern where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setname($row["name"]); 


 	 	 parent::setdescription($row["description"]); 


 	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>