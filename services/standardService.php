<?php
include_once '../model/standards.php';
class StandardsService extends Standards
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblstandard");
		
		$builder->addColumnAndData("abs", parent::getabs());
		$builder->addColumnAndData("bmc", parent::getbmc());
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		$builder->addColumnAndData("obc", parent::getobc());
		$builder->addColumnAndData("sur", parent::getsur());
		$builder->addColumnAndData("rur", parent::getrur());
		$builder->addColumnAndData("fbsm", parent::getfbsm());
		$builder->addColumnAndData("sst", parent::getsst());
		$builder->addColumnAndData("sstt", parent::getsstt());
		//$builder->addColumnAndData("sstt", parent::getsstt());
		$builder->addColumnAndData("isActive", parent::getisActive());
		//$builder->addColumnAndData("sstt", parent::getsstt());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	 
		 //parent::setid($this->con->getId());
    }
	
	public function saveServiceJobCard($serviceId,$jobcardId) 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbljobcardservice");
		
		$builder->addColumnAndData("serviceId",$serviceId);
		$builder->addColumnAndData("jobcardId", $jobcardId);
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	 
		 //parent::setid($this->con->getId());
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblstandard");
		
			$builder->addColumnAndData("abs", parent::getabs());
			$builder->addColumnAndData("bmc", parent::getbmc());
			$builder->addColumnAndData("vehicleId", parent::getvehicleId());
			$builder->addColumnAndData("obc", parent::getobc());
			$builder->addColumnAndData("sur", parent::getsur());
			$builder->addColumnAndData("rur", parent::getrur());
			$builder->addColumnAndData("fbsm", parent::getfbsm());
			$builder->addColumnAndData("sst", parent::getsst());
			$builder->addColumnAndData("sstt", parent::getsstt());
			//$builder->addColumnAndData("sstt", parent::getsstt());
			$builder->addColumnAndData("isActive", parent::getisActive());			
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view(){
       $sql="select s.* ,(select regNo from tblvehicle v where v.id=s.vehicleId) regNo from tblstandard s";
	   return $this->con->getResultSet($sql);
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblstandard");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
	
	
}

?>
