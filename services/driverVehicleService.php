<?php 
 require_once('../model/driverVehicle.php');
 class driverVehicleService extends driverVehicle{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbldrivervehicle');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('driverId',parent::getdriverId());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('assignmentDate',parent::getassignmentDate());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbldrivervehicle');

 	 	 if(!is_null(parent::getdriverId())){
$builder->addColumnAndData('driverId',parent::getdriverId()); 
}

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getassignmentDate())){
$builder->addColumnAndData('assignmentDate',parent::getassignmentDate()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }


	 public function updateAllDriverRecord(){
		 $builder=new UpdateBuilder();
		 $builder->setTable('tbldrivervehicle');

		 $builder->addColumnAndData('isActive',false);

		 $builder->setCriteria("where driverId='".parent::getdriverId()."'");
		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tbldrivervehicle";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select dv.*,v.regNo vehicleNumber,concat(e.firstName,' ',e.lastName) fullName,concat(e.mobile1,'/',e.mobile2) contact from  tbldrivervehicle dv inner join tblvehicle v on dv.vehicleId=v.id inner join employee e on dv.driverId=e.empNo where dv.isActive='1'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select dv.*,v.regNo vehicleNumber,concat(e.firstName,' ',e.lastName) fullName,concat(e.mobile1,'/',e.mobile2) contact from  tbldrivervehicle dv inner join tblvehicle v on dv.vehicleId=v.id inner join employee e on dv.driverId=e.empNo where dv.isActive='1' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;return $data; }
 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbldrivervehicle');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tbldrivervehicle  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setdriverId($row['driverId']);
	 	parent::setvehicleId($row['vehicleId']);
	 	parent::setassignmentDate($row['assignmentDate']);
	 	parent::setisActive($row['isActive']);
}	 	}

     public function findDriver(){
         $sql="select * from  tbldrivervehicle  where vehicleId='".parent::getvehicleId()."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setdriverId($row['driverId']);
             parent::setvehicleId($row['vehicleId']);
             parent::setassignmentDate($row['assignmentDate']);
             parent::setisActive($row['isActive']);
         }	 	}

	 public function findActive()
	 {
		 $driverVehicle=new driverVehicleService();
		 $data=null;
		 $sql="select dv.*,(select v.regNo from tbltrailer t inner join tblvehicle v on t.trailerId=v.id WHERE t.tractorId=dv.vehicleId and t.isActive='1') trailer from  tbldrivervehicle dv  where dv.driverId='".parent::getdriverId()."' and dv.isActive='1'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			$data=$row;
		 }
		 return $data;
	 }
	 public function findActiveDrive()
	 {
		 $driverVehicle=new driverVehicleService();
		 $data=null;
		 $sql="select dv.*,concat(e.firstName,' ',e.lastName) fullName ,(select t.trailerId from tbltrailer t inner join tblvehicle v on t.trailerId=v.id WHERE t.tractorId=dv.vehicleId and t.isActive='1') trailer,(select v.regNo from tbltrailer t inner join tblvehicle v on t.trailerId=v.id WHERE t.tractorId=dv.vehicleId and t.isActive='1') trailerNo,(select t.id from tbltrailer t inner join tblvehicle v on t.trailerId=v.id WHERE t.tractorId=dv.vehicleId and t.isActive='1') statusId from  tbldrivervehicle dv inner join employee e on dv.driverId=e.empNo  where dv.vehicleId='".parent::getvehicleId()."' and dv.isActive='1'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $data=$row;
		 }
		 return $data;
	 }
 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>