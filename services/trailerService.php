<?php
include_once '../model/trailer.php';
class TrailerService extends Trailer
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbltrailer");
		$builder->addColumnAndData("tractorheadId", parent::getTractorHeadId());
		$builder->addColumnAndData("trailerId", parent::getTrailerId());
		$builder->addColumnAndData("isActive",parent::getIsActive());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbltrailer");
			$builder->addColumnAndData("tractorheadId", parent::getTractorHeadId());
            $builder->addColumnAndData("trailerId", parent::getTrailerId());
			$builder->addColumnAndData("isActive", parent::getIsActive());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select *,(select regNo from tblvehicle v where v.id=t.tractorheadId) tractorHead,(select regNo from tblvehicle v where v.id=t.trailerId) trailerNo from tbltrailer t";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbltrailer");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
