<?php 
 require_once('../model/sparePart.php');
 class sparePartService extends sparePart{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('sparepart');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('partName',parent::getpartName());
 		 	 $builder->addColumnAndData('partNumber',parent::getpartNumber());
 		 	 $builder->addColumnAndData('systemId',parent::getsystemId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('sparepart');

 	 	 if(!is_null(parent::getpartName())){
$builder->addColumnAndData('partName',parent::getpartName()); 
}

 	 	 if(!is_null(parent::getpartNumber())){
$builder->addColumnAndData('partNumber',parent::getpartNumber()); 
}

 	 	 if(!is_null(parent::getsystemId())){
$builder->addColumnAndData('systemId',parent::getsystemId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select sp.*, concat(sp.partName,'-',sp.partNumber) sparePartName from  sparepart sp";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select sp.*,vs.systemName from  sparepart sp inner join vehiclesystem vs on sp.systemId=vs.id order by vs.id DESC ";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select sp.*,vs.systemName from  sparepart sp inner join vehiclesystem vs on sp.systemId=vs.id order by vs.id DESC limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('sparepart');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  sparepart  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setpartName($row['partName']);
	 	parent::setpartNumber($row['partNumber']);
	 	parent::setsystemId($row['systemId']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>