
<?php
include_once '../model/checked.php';
class CheckedService extends Checked
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblchecked");
		if(parent::getvehicleId()!=NULL)
		{
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		}
		$builder->addColumnAndData("Date", parent::getdate());
		$builder->addColumnAndData("Time", parent::gettime());
		$builder->addColumnAndData("DriverName", parent::getdriverName());
		$builder->addColumnAndData("Representative", parent::getrepresentative());
		$builder->addColumnAndData("Comment", parent::getcomment());
		$builder->addColumnAndData("status", parent::getstatus());
		$builder->addColumnAndData("checklisytTypeId", parent::getcheckListTypeId());
		if(is_null(parent::getvehicleCoditionOk()))
		{
			$builder->addColumnAndData("vehicleConditionOk", parent::getvehicleCoditionOk());
		}
		if(is_null(parent::getdefectdonotcorrection()))
		{
			$builder->addColumnAndData("defectdonotcorrection", parent::getdefectdonotcorrection());
		}
		if(is_null(parent::getdefectscorrected()))
		{
			$builder->addColumnAndData("defectscorrected", parent::getdefectscorrected());
		}
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query2($builder->getValues());	
		parent::setid($this->con->getId());
    }

    public function update() 
	{
      	 $builder=new UpdateBuilder();
		$builder->setTable("tblchecked");
        if(parent::getvehicleId()!=NULL){
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		}
		$builder->addColumnAndData("Date", parent::getdate());
		$builder->addColumnAndData("Time", parent::gettime());
		$builder->addColumnAndData("DriverName", parent::getdriverName());
		$builder->addColumnAndData("Representative", parent::getrepresentative());
		$builder->addColumnAndData("Comment", parent::getcomment());
		if(is_null(parent::getvehicleCoditionOk()))
		{
			$builder->addColumnAndData("vehicleConditionOk", parent::getvehicleCoditionOk());
		}
		if(is_null(parent::getdefectdonotcorrection()))
		{
			$builder->addColumnAndData("defectdonotcorrection", parent::getdefectdonotcorrection());
		}
		if(is_null(parent::getdefectscorrected()))
		{
			$builder->addColumnAndData("defectscorrected", parent::getdefectscorrected());
		}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblchecked where status='failed'";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblchecked");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
	public function loadObject() {
       $sql="select * from tblchecked where id='".parent::getid()."'";
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   parent::setvehicleId($row['vehicleId']);
		   parent::setdate($row['Date']);
		   parent::settime($row['Time']);
		   parent::setdriverName($row['DriverName']);
		   parent::setrepresentative($row['Representative']);
		   parent::setcomment($row['Comment']);
		   parent::setstatus($row['status']);
		   parent::setCheckListTypeId($row['checklisytTypeId']);
		   
	   }
    }
	
	public function loadStatus($sql)
	{
		$id=-1;
		$this->con->setSelect_query($sql);
		$id=$this->con->sqlCount();
		return $id;
	}
	
	public function ListName() {
		$name="Check List";
       $sql="select * from tblchecklisttype where id='".parent::getcheckListTypeId()."'";
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $name=$row['type'];

		   
	   }
	   return $name;
    }
	
}

?>
