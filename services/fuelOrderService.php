<?php 
 require_once('../model/fuelOrder.php');
 class fuelOrderService extends fuelOrder{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('fuelorder');
	 	 $builder->addColumnAndData('id',parent::getid());
	 	 $builder->addColumnAndData('orderNo',parent::getorderNo());
	 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
	 	 $builder->addColumnAndData('invoiceNo',parent::getinvoiceNo());
	 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
	 	 $builder->addColumnAndData('paid',parent::getpaid());
	 	 $builder->addColumnAndData('checked',parent::getchecked());
	 	 $builder->addColumnAndData('posted',parent::getposted());
	 	 $builder->addColumnAndData('supplierId',parent::getsupplierId());
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('fuelorder');

 	 	 if(!is_null(parent::getorderNo())){
$builder->addColumnAndData('orderNo',parent::getorderNo()); 
}

 	 	 if(!is_null(parent::gettripNo())){
$builder->addColumnAndData('tripNo',parent::gettripNo()); 
}

 	 	 if(!is_null(parent::getinvoiceNo())){
$builder->addColumnAndData('invoiceNo',parent::getinvoiceNo()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getpaid())){
$builder->addColumnAndData('paid',parent::getpaid()); 
}

 	 	 if(!is_null(parent::getchecked())){
$builder->addColumnAndData('checked',parent::getchecked()); 
}

 	 	 if(!is_null(parent::getposted())){
$builder->addColumnAndData('posted',parent::getposted()); 
}

 	 	 if(!is_null(parent::getsupplierId())){
$builder->addColumnAndData('supplierId',parent::getsupplierId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  fuelorder";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="";

		 $sql="select fo.*,v.regNo,sup.supplierName,(select Group_Concat(fp.productName) from fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where foi.fuelOrderId=fo.id) product,(select sum(foi.litres) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalLitres,(select sum(foi.litres*foi.rate) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalShs from  fuelorder fo inner join tblsupplier sup on fo.supplierId=sup.supplierCode inner join tripnumbergenerator tr on fo.tripNo=tr.tripNumber INNER JOIN tblvehicle v on tr.vehicleId=v.id";


		 $this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		 $sql2=null;

		 	//parent::settripNo=$_REQUEST['TripNo'];
		 $sql2 = "select fo.*,v.regNo,sup.supplierName,(select Group_Concat(fp.productName) from fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where foi.fuelOrderId=fo.id) product,(select sum(foi.litres) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalLitres,(select sum(foi.litres*foi.rate) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalShs from  fuelorder fo inner join tblsupplier sup on fo.supplierId=sup.supplierCode inner join tripnumbergenerator tr on fo.tripNo=tr.tripNumber INNER JOIN tblvehicle v on tr.vehicleId=v.id  limit $offset,$rows ";



		 foreach($this->con->getResultSet($sql2) as $row)
		{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }

	 public function viewTrip()
	 {
		 $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		 $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		 $offset = ($page-1)*$rows;
		 //$sql="";


			 $sql="select fo.*,v.regNo,sup.supplierName,(select Group_Concat(fp.productName) from fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where foi.fuelOrderId=fo.id) product,(select sum(foi.litres) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalLitres,(select sum(foi.litres*foi.rate) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalShs from  fuelorder fo inner join tblsupplier sup on fo.supplierId=sup.supplierCode inner join tripnumbergenerator tr on fo.tripNo=tr.tripNumber INNER JOIN tblvehicle v on tr.vehicleId=v.id where fo.tripNo='".parent::gettripNo()."'";


		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql2=null;

		 $sql2 = "select fo.*,v.regNo,sup.supplierName,(select Group_Concat(fp.productName) from fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where foi.fuelOrderId=fo.id) product,(select sum(foi.litres) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalLitres,(select sum(foi.litres*foi.rate) from fuelorderitem foi where foi.fuelOrderId=fo.id) totalShs from  fuelorder fo inner join tblsupplier sup on fo.supplierId=sup.supplierCode inner join tripnumbergenerator tr on fo.tripNo=tr.tripNumber INNER JOIN tblvehicle v on tr.vehicleId=v.id where fo.tripNo='" . parent::gettripNo() . "'  limit $offset,$rows ";



		 foreach($this->con->getResultSet($sql2) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;
	 }

	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('fuelorder');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  fuelorder  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setorderNo($row['orderNo']);
	 	parent::settripNo($row['tripNo']);
	 	parent::setinvoiceNo($row['invoiceNo']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setpaid($row['paid']);
	 	parent::setchecked($row['checked']);
	 	parent::setposted($row['posted']);
	 	parent::setsupplierId($row['supplierId']);
		}
	 }
	 public function getObjectId()
	 {
		 $sql="select * from  fuelorder";
		 $this->con->setSelect_query($sql);
		 return intval($this->con->sqlCount())+1;
	 }

 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>