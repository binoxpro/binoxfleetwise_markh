<?php 
 require_once('../model/scrapped.php');
 class scrappedService extends scrapped{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblscrapped');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('stockId',parent::getstockId());
 		 	 $builder->addColumnAndData('kmDone',parent::getkmDone());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblscrapped');

 	 	 if(!is_null(parent::getstockId())){
$builder->addColumnAndData('stockId',parent::getstockId()); 
}

 	 	 if(!is_null(parent::getkmDone())){
$builder->addColumnAndData('kmDone',parent::getkmDone()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblscrapped";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblscrapped');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblscrapped where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setstockId($row["stockId"]); 


 	 	 parent::setkmDone($row["kmDone"]); 


 	 	 parent::setstatus($row["status"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>