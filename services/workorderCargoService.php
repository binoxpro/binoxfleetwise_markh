<?php 
 require_once('../model/workorderCargo.php');
 class workorderCargoService extends workorderCargo{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblworkordercargo');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('cargoTypeId',parent::getcargoTypeId());
 		 	 $builder->addColumnAndData('workorderId',parent::getworkorderId());
 		 	 $builder->addColumnAndData('cargoDetails',parent::getcargoDetails());
 		 	 $builder->addColumnAndData('qty',parent::getqty());
 		 	 $builder->addColumnAndData('Rate',parent::getRate());
 		 	 $builder->addColumnAndData('Total',parent::getTotal());
 		 	 $builder->addColumnAndData('Vat',parent::getVat());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblworkordercargo');

 	 	 if(!is_null(parent::getcargoTypeId())){
$builder->addColumnAndData('cargoTypeId',parent::getcargoTypeId()); 
}

 	 	 if(!is_null(parent::getworkorderId())){
$builder->addColumnAndData('workorderId',parent::getworkorderId()); 
}

 	 	 if(!is_null(parent::getcargoDetails())){
$builder->addColumnAndData('cargoDetails',parent::getcargoDetails()); 
}

 	 	 if(!is_null(parent::getqty())){
$builder->addColumnAndData('qty',parent::getqty()); 
}

 	 	 if(!is_null(parent::getRate())){
$builder->addColumnAndData('Rate',parent::getRate()); 
}

 	 	 if(!is_null(parent::getTotal())){
$builder->addColumnAndData('Total',parent::getTotal()); 
}

 	 	 if(!is_null(parent::getVat())){
$builder->addColumnAndData('Vat',parent::getVat()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblworkordercargo";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblworkordercargo');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>