<?php
include_once '../model/order.php';
class OrderService extends Order
{
	public function __construct() 
	{
        parent::__construct();
    }

	
    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblorder");
		$builder->addColumnAndData("mailingName", parent::getmailingName());
		//$builder->addColumnAndData("orderDate", parent::getorderDate());
		$builder->addColumnAndData("promiseDate", parent::getpromiseDate());
		$builder->addColumnAndData("ordertypeid", parent::getorderType());
		$builder->addColumnAndData("qtyOrder", parent::getqtyOrder());
		$builder->addColumnAndData("statusorderId", parent::getstatusOrder());
		$builder->addColumnAndData("orderstatusId", parent::getorderStatus());
		$builder->addColumnAndData("itemcodeId", parent::getitemCode());
		$builder->addColumnAndData("orderNumber", parent::getorderNumber());
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query2($builder->getValues());
		parent::setid($this->con->getId());	
		   
    }
//
    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblorder");
			$builder->addColumnAndData("mailingName", parent::getmailingName());
			$builder->addColumnAndData("orderDate", parent::getorderDate());
			$builder->addColumnAndData("promiseDate", parent::getpromiseDate());
			$builder->addColumnAndData("qtyOrder", parent::getqtyOrder());
			$builder->addColumnAndData("orderNumber", parent::getorderNumber());
			
			if(strlen(parent::getstatusOrder())==1){
			$builder->addColumnAndData("statusorderId", parent::getstatusOrder());
			}
			
			if(intval(parent::getorderStatus())!=0){
			$builder->addColumnAndData("orderstatusId", parent::getorderStatus());
			}
			
			if(intval(parent::getitemCode())!=0)
			{
			$builder->addColumnAndData("itemcodeId", parent::getitemCode());
			}
			
			if(intval(parent::getorderType())!=0)
			{
			$builder->addColumnAndData("ordertypeid", parent::getorderType());	
			}
			
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	
	    public function updateOrderStatus() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblorder");
			parent::setorderStatus($this->fixOrderStatus('Allocated'));
			$builder->addColumnAndData("orderstatusId",parent::getorderStatus() );
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	
	 public function dispatcherOrder() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblorder");
			parent::setorderStatus($this->fixOrderStatus('Dispatched'));
			$builder->addColumnAndData("orderstatusId",parent::getorderStatus() );
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	
	public function deliverOrder() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblorder");
			parent::setorderStatus($this->fixOrderStatus('Delivered'));
			$builder->addColumnAndData("orderstatusId",parent::getorderStatus() );
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where os.name='C1' or os.name='CS' or os.name='Ready'  order by o.orderDate desc ";
	   $data=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["mailingName"]=$row["mailingName"];
		   $data2["orderDate"]=$row["orderDate"];
		   $data2["promiseDate"]=$row["promiseDate"];
		   $data2["orderNumber"]=$row["orderNumber"];
		   $data2["ordertypeId"]=$row["ordertypeId"];
		   $data2["qtyOrder"]=$row["qtyOrder"];
		   $data2["statusorderId"]=$row["statusorderId"];
		   $data2["orderstatusId"]=$row["orderstatusId"];
		   $data2["itemcodeId"]=$row["itemcodeId"];
		   
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblorder");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	public function fixOrderStatus($str)
	{
		$sql="select id from tblorderstatus where name='$str'";
		$id=0;
		foreach($this->con->getResultSet($sql) as $row)
		{
			$id=$row['id'];	
		}
		return $id;
	}
	
	public function customerView()
	{
		try{
		$sql="select customerName from tbl_customer";
		$id=0;
		$oldName="";
		$newName="";
		$data=array();
		foreach($this->con->getResultSet($sql) as $row)
		{
			$data2=array();
			$newName=$row['customerName'];
			if($oldName!=$newName){
			$data2['cus']=$row['customerName'];
			array_push($data,$data2);
			}
			
			$oldName=$newName;
				
		}
		$data3=array();
		$data3['cus']="<a href='#' onclick='openCustomer()'><i class='fa fa-plus'></i>Add Customer</a>";
		array_push($data,$data3);
		return $data;
		}catch(PDOException $e)
		{
			return array('cus'=>" $e->getMessage()");
		}
		
	}
	
	public function saveCustomer($name) 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbl_customer");
		$builder->addColumnAndData("customerName", $name);
		
		$this->con->setQuery(Director::buildSql($builder));
		
		return $this->con->execute_query2($builder->getValues());	
		   
    }
	
	public function noworkedDays()
	{
		$order=new OrderService();
		$m=date('m');
		$countOrder=0;
		$i=1;
		$e=cal_days_in_month(CAL_GREGORIAN,$m,date('Y'));
		for($i;$i<=$e;$i++){
			$cv=$i<10?"0".$i:$i;
		$processDate=date('Y')."-".date('m')."-".$cv;
		$sqlPart="SELECT o.id   FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where deli.Deliverydate = '$processDate'  ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				//echo "<table class='table table-striped table-hovered table-bordered'>";
				foreach($order->view_query($sqlPart) as $rowPart)
				{	if($total==0){
					$countOrder=$countOrder+1;
					$total=$total+1;
					}
				}
				
		}
		return $countOrder;
	}
	public function noDaysLost()
	{
		$order=new OrderService();
		$m=date('m');
		$countOrder=0;
		$i=1;
		$e=cal_days_in_month(CAL_GREGORIAN,$m,date('Y'));
		for($i;$i<=intval(date('d'));$i++){
			$cv=$i<10?"0".$i:$i;
		$processDate=date('Y')."-".date('m')."-".$cv;
		$sqlPart="SELECT o.id   FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where deli.Deliverydate = '$processDate'  ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				//echo "<table class='table table-striped table-hovered table-bordered'>";
				foreach($order->view_query($sqlPart) as $rowPart)
				{	if($total==0){
					//$countOrder=$countOrder+1;
					$total=$total+1;
					}
				}
				if($total==0){
					$countOrder=$countOrder+1;
				}
		}
		return $countOrder;
	}
	
	public function returnId($colVal,$tableName)
	{
		$id=-1;
		$sql="select id from ".$tableName." where name='$colVal'";
		foreach ($this->con->getResultSet($sql) as $row)
		{
			$id=$row['id'];
		}
		return $id;
	}
}

?>
