<?php 
 require_once('../model/roleManagement.php');
 class roleManagementService extends roleManagement{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblrolemanagement');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('roleId',parent::getroleId());
 		 	 $builder->addColumnAndData('userId',parent::getuserId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblrolemanagement');

 	 	 if(!is_null(parent::getroleId())){
$builder->addColumnAndData('roleId',parent::getroleId()); 
}

 	 	 if(!is_null(parent::getuserId())){
$builder->addColumnAndData('userId',parent::getuserId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblrolemanagement";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblrolemanagement');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblrolemanagement where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setroleId($row["roleId"]); 


 	 	 parent::setuserId($row["userId"]); 

	 	} 
 }
 
 public function getObjectExistance(){
	 	 $sql="select * from  tblrolemanagement where roleId='".parent::getroleId()."' and userId='".parent::getuserId()."'";
	 	 $this->con->setSelect_query($sql) ;
		 return $this->con->sqlCount()>0?true:false;
 }
 
 public function getObjecttoDelete(){
	 	 $sql="select * from  tblrolemanagement where roleId='".parent::getroleId()."' and userId='".parent::getuserId()."'";
	 	foreach($this->con->getResultSet($sql) as $row){
	 	 parent::setid($row["id"]);
 	 	 parent::setroleId($row["roleId"]); 


 	 	 parent::setuserId($row["userId"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>