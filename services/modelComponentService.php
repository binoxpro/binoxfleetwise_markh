<?php 
 require_once('../model/modelComponent.php');
 class modelComponentService extends modelComponent{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblmodelcomponent');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('modelId',parent::getmodelId());
 		 	 $builder->addColumnAndData('componentId',parent::getcomponentId());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblmodelcomponent');

 	 	 if(parent::getmodelId()!=""){
$builder->addColumnAndData('modelId',parent::getmodelId()); }

 	 	 if(parent::getcomponentId()!=""){
$builder->addColumnAndData('componentId',parent::getcomponentId()); }

 	 	 if(parent::getisActive()!=""){
$builder->addColumnAndData('isActive',parent::getisActive()); }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblmodelcomponent";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblmodelcomponent');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
	 
	 public function viewMyComponent(){
	 	 $sql="select mc.*, c.componentName from  tblmodelcomponent mc inner join tblcomponent c on mc.componentId=c.id  where mc.modelId='".parent::getmodelId()."'";
		 $data=array();
	 foreach( $this->con->getResultSet($sql) as $row)
	 {
		 $data2=array();
		 $data2['id']=$row['id'];
		 $data2['componentName']=$row['componentName'];
		 array_push($data,$data2);
	 }
	 return $data;
 	 }
	 
	 public function viewMissingComponent(){
	 	 $sql="select * from tblcomponent";
		 $data=array();
	 foreach( $this->con->getResultSet($sql) as $row)
	 {
		 $id=$row['id'];
		 $sqlInner="select * from tblmodelcomponent where componentId='$id' and modelId='".parent::getmodelId()."'";
		 $count=-1;
		 $this->con->setSelect_query($sqlInner);
		 $count=$this->con->sqlCount();
		 if($count<=0){
		 $data2=array();
		 $data2['id']=$row['id'];
		 $data2['componentName']=$row['componentName'];
		 array_push($data,$data2);
		 }
	 }
	 return $data;
 	 }
	 
	 public function viewServiceListComponent(){
	 	 $sql="select mc.id,c.componentName from tblmodelcomponent mc inner join tblcomponent c on mc.componentId=c.id where mc.modelId='".parent::getcomponentId()."'";
		 $data=array();
	 foreach( $this->con->getResultSet($sql) as $row)
	 {
		 $id=$row['id'];
		 $sqlInner="select cs.id,c.componentName,cs.actionNoteId from tblcomponentschedule cs inner join tblmodelcomponent mc on cs.componentModelId=mc.id inner join tblcomponent c on mc.componentId=c.id where cs.serviceIntervalId='".parent::getmodelId()."' and cs.componentModelId='$id'";
		 $count=-1;
		 $this->con->setSelect_query($sqlInner);
		 $count=$this->con->sqlCount();
		 if($count<=0){
		 $data2=array();
		 $data2['id']=$row['id'];
		 $data2['componentName']=$row['componentName'];
		 array_push($data,$data2);
		 }
	 }
	 return $data;
 	 }
	 
	 public function viewServiceInternvalComponent(){
	 	 $sql="select cs.id,c.componentName,(select an.detail from tblactionnote an where an.id=cs.actionNoteId ) actionNote from tblcomponentschedule cs inner join tblmodelcomponent mc on cs.componentModelId=mc.id inner join tblcomponent c on mc.componentId=c.id where cs.serviceIntervalId='".parent::getmodelId()."' ";
		 $data=array();
	 foreach( $this->con->getResultSet($sql) as $row)
	 {
		 $data2=array();
		 $data2['id']=$row['id'];
		 $data2['componentName']=$row['componentName'];
		 $data2['actionNote']=$row['actionNote'];
		 array_push($data,$data2);
	 }
	 return $data;
 	 }
	 
 }
?>