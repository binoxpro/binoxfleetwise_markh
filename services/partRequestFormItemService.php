<?php 
 require_once('../model/partRequestFormItem.php');
 class partRequestFormItemService extends partRequestFormItem{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('prfitem');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('itemId',parent::getitemId());
 		 	 $builder->addColumnAndData('itemDescription',parent::getitemDescription());
 		 	 $builder->addColumnAndData('uomId',parent::getuomId());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('quantityIssued',parent::getquantityIssued());
 		 	 $builder->addColumnAndData('dateIssued',parent::getdateIssued());
 		 	 $builder->addColumnAndData('issuedBy',parent::getissuedBy());
 		 	 $builder->addColumnAndData('receviedBy',parent::getreceviedBy());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('prfId',parent::getprfId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('prfitem');

 	 	 if(!is_null(parent::getitemId())){
$builder->addColumnAndData('itemId',parent::getitemId()); 
}

 	 	 if(!is_null(parent::getitemDescription())){
$builder->addColumnAndData('itemDescription',parent::getitemDescription()); 
}

 	 	 if(!is_null(parent::getuomId())){
$builder->addColumnAndData('uomId',parent::getuomId()); 
}

 	 	 if(!is_null(parent::getquantity())){
$builder->addColumnAndData('quantity',parent::getquantity()); 
}

 	 	 if(!is_null(parent::getquantityIssued())){
$builder->addColumnAndData('quantityIssued',parent::getquantityIssued()); 
}

 	 	 if(!is_null(parent::getdateIssued())){
$builder->addColumnAndData('dateIssued',parent::getdateIssued()); 
}

 	 	 if(!is_null(parent::getissuedBy())){
$builder->addColumnAndData('issuedBy',parent::getissuedBy()); 
}

 	 	 if(!is_null(parent::getreceviedBy())){
$builder->addColumnAndData('receviedBy',parent::getreceviedBy()); 
}

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getprfId())){
$builder->addColumnAndData('prfId',parent::getprfId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  prfitem";
	 return $this->con->getResultSet($sql);
 	 }


     public function viewConboxForPrf(){
         $sql="SELECT prfi.*,concat(i.itemName,'/',i.partNo) partName FROM `prfitem` prfi inner join tblpartsused pus on prfi.itemId=pus.id  inner join items i on pus.partusedId=i.id where prfi.status  in ('PROCESS PURCHASE') and prfId='".parent::getprfId()."'";
         return $this->con->getResultSet($sql);
     }

	 public function view()
     {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		$sql="select * from  partvoucher where prfId='".parent::getprfId()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  partvoucher where prfId='".parent::getprfId()."' limit $offset,$rows ";
		foreach($this->con->getResultSet($sql) as $row)
        {
             array_push($data2,$row);
        }
		$data["rows"]=$data2;
		return $data;
 	 }





 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('prfitem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  prfitem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setitemId($row['itemId']);
	 	parent::setitemDescription($row['itemDescription']);
	 	parent::setuomId($row['uomId']);
	 	parent::setquantity($row['quantity']);
	 	parent::setquantityIssued($row['quantityIssued']);
	 	parent::setdateIssued($row['dateIssued']);
	 	parent::setissuedBy($row['issuedBy']);
	 	parent::setreceviedBy($row['receviedBy']);
	 	parent::setcomment($row['comment']);
	 	parent::setstatus($row['status']);
	 	parent::setprfId($row['prfId']);
}	 	}



     public function getNumberStatusItem($status)
     {
         $itemNo=0;
         $sql="select count(*) itemNo from  partvoucher where prfId='".parent::getprfId()."' and status='".$status."' ";
         foreach($this->con->getResultSet($sql) as $row)
         {
             $itemNo=$row['itemNo'];
         }

         return $itemNo;
     }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>