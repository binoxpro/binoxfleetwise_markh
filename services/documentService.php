<?php
include_once '../model/document.php';
class DocumentService extends Document
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbldocument");
		
		$builder->addColumnAndData("documentType", parent::getdocumentType());
		$builder->addColumnAndData("isActive", parent::getisActive());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbldocument");
			$builder->addColumnAndData("documentType", parent::getdocumentType());
            $builder->addColumnAndData("isActive", parent::getisActive());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tbldocument";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbldocument");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
