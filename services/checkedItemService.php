
<?php
include_once '../model/checkedItem.php';
class CheckedItemService extends CheckedItem
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblcheckedItem");
		
		$builder->addColumnAndData("checkListId", parent::getcheckListId());
		$builder->addColumnAndData("sectionItemId", parent::getsectionItemId());
		$builder->addColumnAndData("corrected", parent::getCorrected());
		$builder->addColumnAndData("inspectorsComment", parent::getinspectorsComment());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 $builder=new UpdateBuilder();
			$builder->setTable("tblcheckedItem");
			if(parent::getcheckListId()!=NULL){
			$builder->addColumnAndData("checkListId", parent::getcheckListId());
			}
			if(parent::getsectionItemId()!=NULL){
			$builder->addColumnAndData("sectionItemId", parent::getsectionItemId());
			}
			if(parent::getCorrected()!=NULL){
			$builder->addColumnAndData("corrected", parent::getCorrected());
			}
			if(is_null(parent::getinspectorsComment()))
			{
				$builder->addColumnAndData("inspectorsComment", parent::getinspectorsComment());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view()
	{
       $sql="select * from tblchecked where status='failed'";
	   return $this->con->getResultSet($sql);
    }
	
	public function delete()
	{
		$builder=new DeleteBuilder();
		$builder->setTable("tblchecked");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	
    public function view_query($sql)
	{
        return $this->con->getResultSet($sql);
    }
	
}

?>
