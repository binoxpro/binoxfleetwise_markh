<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/tyreType.php';
class TyreTypeService extends TyreType {
  
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbltyretype");
		if(parent::gettyretypeName()!=NULL){
		$builder->addColumnAndData("tyretypeName", parent::gettyretypeName());
		}
        if(parent::gettyreIndex()!=NULL){
		$builder->addColumnAndData("tyreIndex", parent::gettyreIndex());
		}
        if(parent::getrotationName()!=NULL){
		$builder->addColumnAndData("rotationPosition", parent::getrotationName());
		}
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tbltyretype");
			if(!is_null(parent::gettyretypeName())){
			$builder->addColumnAndData("tyretypeName", parent::gettyretypeName());
			}
            if(!is_null(parent::gettyreIndex())){
    		$builder->addColumnAndData("tyreIndex", parent::gettyreIndex());
    		}
            if(!is_null(parent::getrotationName())){
    		$builder->addColumnAndData("rotationPosition", parent::getrotationName());
    		}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
       $sql="select tt.*,(select tyretypeName from tbltyretype where id=tt.rotationPosition) rotationIndex from tbltyretype tt";
	   $data=array();
	   return $this->con->getResultSet($sql);
		   
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbltyretype");
		$builder->setCriteria("where id='".parent::getId()."'");
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	public function returnPosition($regNo) {
		$id=-1;
       $sql="select id from tbltyretype where tyretypeName='$regNo'";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   $id=$row['id'];
		   
	   }
	   return $id;
    }
	
	public function returnTyreType($regNo) {
		$id=-1;
       $sql="select id from tbltyretype where tyretypeName='$regNo'";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   $id=$row['id'];
		   
	   }
	   return $id;
    }
	
}

	
