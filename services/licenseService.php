<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcademicYearService
 *
 * @author plussoft
 * 
 */
include_once '../model/license.php';

class LicenseService extends License{
    
   private $school_no_init;
    
    function __construct() {
        parent::__construct();
     	
               
    }

#obslet method use save maintainance
    public function save() {
       
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_license");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("tpDoi", parent::getthirdPartyDOI());
			$builder->addColumnAndData("tpDoe", parent::getthirdPartyDOEg());
			if(!is_null(parent::getcaliberationDOI())) {
				$builder->addColumnAndData("cDoi", parent::getcaliberationDOI());
				$builder->addColumnAndData("cDoe", parent::getcaliberationDOE());
			}
			if(!is_null(parent::getFDoi())) {
				$builder->addColumnAndData("fDoi", parent::getFDoi());
				$builder->addColumnAndData("fDoe", parent::getFDoe());
			}
            $builder->addColumnAndData("isActive", parent::getIsActive());
			//$builder->addColumnAndData("pDoi", parent::getPDoi());
			//$builder->addColumnAndData("pDoe", parent::getPDoe());
            $this->con->setQuery(Director::buildSql($builder));
            //$this->con->setSelect_query("select * from tbl_service where numberPlate='".parent::getNumberPlate()."' and km_reading='".parent::getKmReading()."'");
			//if($this->con->sqlCount()<1){
			return $this->con->execute_query2($builder->getValues());
				//return $this->updateStatus();
			//}else{
				//return array('msg'=>'Service schedue ready saved');
			//}
            
       
       // parent::save();
    }
	
	public function saveStatus() {
       
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service_status");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			//$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_service_status where numberPlate='".parent::getNumberPlate()."' ");
			if($this->con->sqlCount()<1){
				//$this->updateStatus();
            	return $this->con->execute_query2($builder->getValues());
				
			}else{
				return array('msg'=>'Service schedue ready saved');
			}
            
       
       // parent::save();
    }
	
    public function update() {
         	$builder=new UpdateBuilder();
           //$builder->setTable("tbl_service");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->setTable("tbl_license");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("tpDoi", parent::getthirdPartyDOI());
			$builder->addColumnAndData("tpDoe", parent::getthirdPartyDOEg());
			if(!is_null(parent::getcaliberationDOI())) {
				$builder->addColumnAndData("cDoi", parent::getcaliberationDOI());
				$builder->addColumnAndData("cDoe", parent::getcaliberationDOE());
			}
			if(!is_null(parent::getFDoi())) {
				$builder->addColumnAndData("fDoi", parent::getFDoi());
				$builder->addColumnAndData("fDoe", parent::getFDoe());
			}
            $builder->addColumnAndData("isActive", parent::getIsActive());
			$builder->setCriteria("Where Id='".parent::getid()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
			 //return $this->updateStatus();
		
    }
	#Obsolet method
	public function updateStatus(){
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			
			$builder->setCriteria("Where numberPlate='".parent::getNumberPlate()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    
	}
	public function updateStatusNew()
	{
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
			$builder->addColumnAndData("isActive", parent::getIsActive());
			$builder->setCriteria("Where Id='".parent::getid()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
	}
    public function updateIsActive()
	{
         	$builder=new UpdateBuilder();
            $builder->setTable("tbl_license");
           	$builder->addColumnAndData("isActive", parent::getIsActive());
			$builder->setCriteria("Where Id='".parent::getid()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
	}
	public function saveMaintanance() {
       
          	 $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
			$builder->addColumnAndData("PreviousDate",parent::getPreviousDate());
			$builder->addColumnAndData("NextDate",parent::getNextDate());
			$builder->addColumnAndData("IntervalPeriod",parent::getInterval());
			$builder->addColumnAndData("Reminder",parent::getReminder());
			$builder->addColumnAndData("Service",parent::getService());
			$builder->addColumnAndData("TypeOfService",parent::getTypeOfService());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
			//this part is not needed in the save but if the client change requirement we will add it 
           // $this->con->setSelect_query("select * from tbl_service where PreviousDate='".parent::getPreviousDate()."' and km_reading='".parent::getKmReading()."'");
			///if($this->con->sqlCount()<1){
				$this->con->execute_query2($builder->getValues());
				return $this->updateStatusNew();
			//}else{
				//return array('msg'=>'Service schedue ready saved');
			//}
            
       
       // parent::save();
    }
    public function view2($id) {
       $sql="select * from tbl_class ";
	   return $this->con->getResultSet($sql);
    }

	public function view() {
       $sql="SELECT lic.*,v.regNo,lic.numberPlate numberPlate1 FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where isActive='1'";
	   $data=array();
	   foreach($this->con->getResultSet($sql) as $row){
		   	$datev=strtotime(date('Y-m-d'));
			$sec=$datev+(3*3600*24);
			
			//$expiryLimit=date('Y-m-d',$sec);

		   $data2=array();
           $data2['Id']=$row['Id'];
           $data2['numberPlate1']=$row['numberPlate1'];
		   $data2['regNo']=$row['regNo'];
		   $data2['tpDoi']=$row['tpDoi'];
		   $data2['tpDoe']=$row['tpDoe'];
		   if(!is_null($row['tpDoe'])){
			$tp=strtotime($row['tpDoe']);
			if($tp<=$sec)
			{
				$data2['tpStatus']="ex";
			}else{
				$data2['tpStatus']="on";
			}
			}else{
				$data2['tpStatus']="ep";
			}
		   
		   $data2['cDoi']=$row['cDoi'];
		   $data2['cDoe']=$row['cDoe'];
		    if(!is_null($row['cDoe'])){
			$tp=strtotime($row['cDoe']);
			if($tp<=$sec)
			{
				$data2['cStatus']="ex";
			}else{
				$data2['cStatus']="on";
			}
			}else{
				$data2['cStatus']="ep";
			}
		   $data2['fDoi']=$row['fDoi'];
		   $data2['fDoe']=$row['fDoe'];
		   if(!is_null($row['fDoe'])){
			$tp=strtotime($row['fDoe']);
			if($tp<=$sec)
			{
				$data2['fStatus']="ex";
			}else{
				$data2['fStatus']="on";
			}
			}else{
				$data2['fStatus']="ep";
			}
		   
		   $data2['pDoi']=$row['pDoi'];
		   $data2['pDoe']=$row['pDoe'];
		   if(!is_null($row['pDoe']))
		   {
				$tp=strtotime($row['pDoe']);
				if($tp<=$sec)
				{
					$data2['pStatus']="ex";
				}else{
					$data2['pStatus']="on";
				}
			}else
			{
				$data2['pStatus']="ep";
			}
		   //$data2['pStatus']="";
		   array_push($data,$data2);
		   
	   }
	   return $data;
	   }
	
	public function expireThirdParty() {
		$datev=strtotime(date('Y-m-d'));
		$sec=$datev+(2*3600*24);
		$datev=date('Y-m-d',$sec);
		$i=0;
		$emailText="";
       $sql="SELECT lic.*,v.regNo FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where lic.tpDoe<='$datev' and lic.tpDoe!='0000-00-00' ";
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   if($i==0)
		   {
			 $emailText.="<h4 style='color:#096;'>Third party to renew</h4><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><th>Vehicle</th><th>Expire Date</th></tr>"."<tr><td>".$row['regNo']."</td><td>".$row['tpDoe']."</td></tr>";  
		   }else{
			   $emailText.="<tr><td>".$row['regNo']."</td><td>".$row['tpDoe']."</td></tr>";  
		   }
		   
		   $i=$i+1;
	   }
	   if($i!=0)
	   {
		   $emailText.="</table>";
	   }
	   
	   return $emailText;
    }
	
	public function expireCaliberation() {
		$datev=strtotime(date('Y-m-d'));
		$sec=$datev+(2*3600*24);
		$datev=date('Y-m-d',$sec);
		$i=0;
		$emailText="";
       $sql="SELECT lic.*,v.regNo FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where lic.cDoe<='$datev' and lic.cDoe!='0000-00-00'  ";
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   if($i==0)
		   {
			 $emailText.="<h3 style='color:#096;'>Caliberation Warning </h3><p></p><p></p><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><th>Vehicle</th><th>Expire Date</th></tr>"."<tr><td>".$row['regNo']."</td><td>".$row['cDoe']."</td></tr>";  
		   }else{
			   $emailText.="<tr><td>".$row['regNo']."</td><td>".$row['cDoe']."</td></tr>";  
		   }
		   
		   $i=$i+1;
	   }
	   if($i!=0)
	   {
		   $emailText.="</table>";
	   }
	   
	   return $emailText;
    }
	
	
	public function expireFireExtinguisher() {
		$datev=strtotime(date('Y-m-d'));
		$sec=$datev+(2*3600*24);
		$datev=date('Y-m-d',$sec);
		$i=0;
		$emailText="";
       $sql="SELECT lic.*,v.regNo FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where lic.fDoe<='$datev' and lic.fDoe!='0000-00-00' ";
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   if($i==0)
		   {
			 $emailText.="<h4 style='color:#096;'>Fire Extinguisher to refill</h4><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><th>Vehicle</th><th>Expire Date</th></tr>"."<tr><td>".$row['regNo']."</td><td>".$row['fDoe']."</td></tr>";  
		   }else{
			   $emailText.="<tr><td>".$row['regNo']."</td><td>".$row['fDoe']."</td></tr>";  
		   }
		   
		   $i=$i+1;
	   }
	   if($i!=0)
	   {
		   $emailText.="</table>";
	   }
	   
	   return $emailText;
    }
	
	public function expirePressure() {
		$datev=strtotime(date('Y-m-d'));
		$sec=$datev+(2*3600*24);
		$datev=date('Y-m-d',$sec);
		$i=0;
		$emailText="";
       $sql="SELECT lic.*,v.regNo FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where lic.pDoe<='$datev' and lic.pDoe!='0000-00-00' ";
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   if($i==0)
		   {
			 $emailText.="<h3 style='color:#096;'>Pressure Test Alert </h3><p></p><p>This is to inform you that the vehicle's pressure test </p><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><th>Vehicle</th><th>Expire Date</th></tr>"."<tr><td>".$row['regNo']."</td><td>".$row['pDoe']."</td></tr>";  
		   }else{
			   $emailText.="<tr><td>".$row['regNo']."</td><td>".$row['pDoe']."</td></tr>";  
		   }
		   
		   $i=$i+1;
	   }
	   if($i!=0)
	   {
		   $emailText.="</table>";
	   }
	   
	   return $emailText;
    }

	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	//the method is litte in complete
	public function searchFilterForService(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getInterval()==NULL?'null':"'".parent::getInterval()."'";
		$para4=parent::getReminder()==NULL?'null':"'".parent::getReminder()."'";
		$para5=parent::getService()==NULL?'null':"'".parent::getService()."'";
		$para6=parent::getTypeOfService()==NULL?'null':"'".parent::getTypeOfService()."'";
		$para7=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		$para8=parent::getServiceID()==NULL?'null':"'".parent::getServiceID()."'";
		$sql="select serviceID,numberPlate,PreviousDate,NextDate,IntervalPeriod,Reminder,Service,TypeOfService from tbl_service where serviceID=COALESCE(".$para8.",serviceID) AND numberPlate=COALESCE(".$para7.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate) AND IntervalPeriod=COALESCE(".$para3.",IntervalPeriod) AND Reminder=COALESCE(".$para4.",Reminder) AND Service=COALESCE(".$para5.",Service) AND TypeOfService=COALESCE(".$para6.",TypeOfService) ";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	
	public function searchFilterForServiceStatus(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		
		$sql="select * from tbl_service_status where numberPlate=COALESCE(".$para3.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate)";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbl_license");
		$builder->setCriteria("where Id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}

    //put your code here
}
?>