<?php 
 require_once('../model/licenceManager.php');
 class licenceManagerService extends licenceManager{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('licenceamount');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('startDate',parent::getstartDate());
 		 	 $builder->addColumnAndData('expireDate',parent::getexpireDate());
 		 	 $builder->addColumnAndData('licenceCost',parent::getlicenceCost());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('licenceName',parent::getlicenceName());
 		 	 $builder->addColumnAndData('supplierId',parent::getsupplierId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('licenceamount');

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getstartDate())){
$builder->addColumnAndData('startDate',parent::getstartDate()); 
}

 	 	 if(!is_null(parent::getexpireDate())){
$builder->addColumnAndData('expireDate',parent::getexpireDate()); 
}

 	 	 if(!is_null(parent::getlicenceCost())){
$builder->addColumnAndData('licenceCost',parent::getlicenceCost()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getlicenceName())){
$builder->addColumnAndData('licenceName',parent::getlicenceName()); 
}

 	 	 if(!is_null(parent::getsupplierId())){
$builder->addColumnAndData('supplierId',parent::getsupplierId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }


	 public function updateAllIicence(){
		 $builder=new UpdateBuilder();
		 $builder->setTable('licenceamount');
		 $builder->addColumnAndData('status',0);
		 $builder->setCriteria("where licenceName='".parent::getlicenceName()."' and vehicleId='".parent::getvehicleId()."'");
		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }
 

 	 public function viewConbox()
	 {
	 	 $sql="select DISTINCT (licenceName) name from  licenceamount where licenceName like '".parent::getlicenceName()."%'";
	 return $this->con->getResultSet($sql);
 	 }


	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select la.*,v.regNo,concat(sp.supplierName,'/',sp.address) supplier from  licenceamount la inner join tblvehicle v on la.vehicleId=v.id inner join tblsupplier sp on la.supplierId=sp.supplierCode WHERE STATUS='1' ";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select la.*,v.regNo,concat(sp.supplierName,'/',sp.address) supplier from  licenceamount la inner join tblvehicle v on la.vehicleId=v.id inner join tblsupplier sp on la.supplierId=sp.supplierCode WHERE STATUS='1' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $balance=0;
			 $balance=(strtotime($row['expireDate'])-strtotime(date('Y-m-d')))/(24*60*60);
			 if($balance<0)
			 {
				 $row['balance']=0;
			 }else
			 {
				 $row['balance']=$balance;
			 }
			 $row['licenceCost']=number_format($row['licenceCost']);

			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('licenceamount');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  licenceamount  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setvehicleId($row['vehicleId']);
	 	parent::setstartDate($row['startDate']);
	 	parent::setexpireDate($row['expireDate']);
	 	parent::setlicenceCost($row['licenceCost']);
	 	parent::setstatus($row['status']);
	 	parent::setlicenceName($row['licenceName']);
	 	parent::setsupplierId($row['supplierId']);
}	 	}
	 public function getLicenceCostTotal($startDate, $endDate)
	 {
		 $licencefee=0;
		 $sql = "select sum(licenceCost) cost from  licenceamount  where  expireDate between '$startDate' and '$endDate' ";
		 foreach ($this->con->getResultSet($sql) as $row)
		 {
			 if($row['cost']>0)
			 {
				 $licencefee=$row['cost']/12;
			 }else
			 {
				 $licencefee=0;
			 }

		 }
		 parent::setlicenceCost(round($licencefee,2));
	 }


	 public function getVehicleLicenceCostTotal($startDate, $endDate,$vehicleId)
	 {
		 $licencefee=0;
		 $sql = "select sum(licenceCost) cost from  licenceamount  where vehicleId='$vehicleId' and  (expireDate <='$endDate' and expireDate >='$startDate')";
		 foreach ($this->con->getResultSet($sql) as $row)
		 {
			 if($row['cost']>0)
			 {
				 $licencefee=$row['cost']/12;
			 }else
			 {
				 $licencefee=0;
			 }

		 }
		parent::setlicenceCost(round($licencefee,2));
	 }

 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>