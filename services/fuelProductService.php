<?php 
 require_once('../model/fuelProduct.php');
 class fuelProductService extends fuelProduct{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('fuelproduct');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('productName',parent::getproductName());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('fuelproduct');

 	 	 if(!is_null(parent::getproductName())){
$builder->addColumnAndData('productName',parent::getproductName()); 
}

 	 	 if(!is_null(parent::getaccountCode())){
$builder->addColumnAndData('accountCode',parent::getaccountCode()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  fuelproduct";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select fp.*,coa.accountName from  fuelproduct fp inner join chartofaccounts coa on fp.accountCode=coa.accountCode";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select fp.*,coa.accountName from  fuelproduct fp inner join chartofaccounts coa on fp.accountCode=coa.accountCode limit $offset,$rows ";

		 foreach($this->con->getResultSet($sql) as $row)
		{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('fuelproduct');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  fuelproduct  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setproductName($row['productName']);
	 	parent::setaccountCode($row['accountCode']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>