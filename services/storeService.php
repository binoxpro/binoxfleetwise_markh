<?php 
 require_once('../model/store.php');
 class storeService extends store{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('store');
	 	 $builder->addColumnAndData('storeNo',parent::getstoreNo());
 		 	 $builder->addColumnAndData('storeName',parent::getstoreName());
 		 	 $builder->addColumnAndData('storeDescription',parent::getstoreDescription());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('store');

 	 	 if(!is_null(parent::getstoreName())){
$builder->addColumnAndData('storeName',parent::getstoreName()); 
}

 	 	 if(!is_null(parent::getstoreDescription())){
$builder->addColumnAndData('storeDescription',parent::getstoreDescription()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where storeNo='".parent::getstoreNo()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  store";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  store";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  store limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('store');
	 	 $builder->setCriteria("where storeNo='".parent::getstoreNo()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  store  where storeNo='".parent::getstoreNo()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setstoreNo($row['storeNo']);
	 	parent::setstoreName($row['storeName']);
	 	parent::setstoreDescription($row['storeDescription']);
	 	parent::setisActive($row['isActive']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>