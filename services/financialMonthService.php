<?php 
 require_once('../model/financialMonth.php');
 class financialMonthService extends financialMonth{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('financialmonth');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('yearId',parent::getyearId());
 		 	 $builder->addColumnAndData('startDate',parent::getstartDate());
 		 	 $builder->addColumnAndData('endDate',parent::getendDate());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('financialmonth');

 	 	 if(!is_null(parent::getyearId())){
$builder->addColumnAndData('yearId',parent::getyearId()); 
}

 	 	 if(!is_null(parent::getstartDate())){
$builder->addColumnAndData('startDate',parent::getstartDate()); 
}

 	 	 if(!is_null(parent::getendDate())){
$builder->addColumnAndData('endDate',parent::getendDate()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  financialmonth";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  financialmonth";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  financialmonth order by startDate ASC limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('financialmonth');
	 	 $builder->setCriteria("where yearId='".parent::getyearId()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }

	 public function updateAllFinancialMonth()
	 {
		 $builder=new UpdateBuilder();
		 $builder->setTable('financialmonth');
		 if(!is_null(parent::getisActive()))
		 {
			 $builder->addColumnAndData('isActive',parent::getisActive());
		 }

		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }


	 public function deleteForYear()
	 {
		 $builder=new DeleteBuilder;
		 $builder->setTable('financialmonth');
		 $builder->setCriteria("where id='".parent::getid()."'");
		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }
 

 	 public function find(){
	 	 $sql="select * from  financialmonth  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setyearId($row['yearId']);
	 	parent::setstartDate($row['startDate']);
	 	parent::setendDate($row['endDate']);
	 	parent::setisActive($row['isActive']);
}	 	}

	 public function findOpenMonth(){
		 $sql="select * from  financialmonth  where isActive='1'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 parent::setyearId($row['yearId']);
			 parent::setstartDate($row['startDate']);
			 parent::setendDate($row['endDate']);
			 parent::setisActive($row['isActive']);
		 }
	 }

	 public function findOpenMonthArray()
	 {
		 $array="";
		 $sql="select * from  financialmonth  where isActive='1'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $array=$row;
		 }
		 return $array;
	 }

 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>