<?php
include_once '../model/trainingCompliance.php';
class TrainingComplianceService extends TrainingCompliance
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$this->updateNewTraining();
		$builder=new InsertBuilder();
		$builder->setTable("tblcompliance");
		$builder->addColumnAndData("trainingId", parent::gettrainingId());
		$builder->addColumnAndData("issueDate", parent::getiusseDate());
		$builder->addColumnAndData("expiryDate", parent::getexpireDate());
		$builder->addColumnAndData("status", parent::getStatus());
		$builder->addColumnAndData("driverId", parent::getdriverId());
		$builder->addColumnAndData("isActive", parent::getisActive());
		$builder->addColumnAndData("parentId", parent::getparentId());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	 
		 //parent::setid($this->con->getId());
    }
	
    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblcompliance");
		
			$builder->addColumnAndData("trainingId", parent::gettrainingId());
			$builder->addColumnAndData("issueDate", parent::getiusseDate());
			$builder->addColumnAndData("expiryDate", parent::getexpireDate());
			$builder->addColumnAndData("isActive", parent::getisActive());
			$builder->addColumnAndData("status", parent::getStatus());
			$builder->addColumnAndData("driverId", parent::getdriverId());
			$builder->addColumnAndData("isActive", parent::getisActive());
			$builder->addColumnAndData("parentId", parent::getparentId());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	
	public function updateNewTraining() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblcompliance");
			$builder->addColumnAndData("isActive", false);
			$builder->setCriteria("where driverId='".parent::getdriverId()."' and trainingId='".parent::gettrainingId()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    	}

	public function view()
	{
       $sql="select d.id,d.firstName,d.lastName,d.dob,d.contact,li.licenceNo,li.issueDate,li.expiryDate,li.licenseClass from tbldriver d  inner join tbllicence li on d.id=li.driverId where li.isActive='1'";
	   $driverId=-1;
	   $data=array();
	  // echo "<table><tr><th>Driver ID</th><th>Name</th><th>DOB</th><th>CONTACT</th><th>LICENCE NO</th><th>";
	   foreach($this->con->getResultSet($sql) as $row){
		   $data2=array();
		   $driverId=$row['id'];
		   $data2["DriverId"]=$row['id'];
		   $data2["firstName"]=$row["firstName"];
		   $data2["lastName"]=$row["lastName"];
		   $data2["dob"]=$row["dob"];
		   $data2["contact"]=$row["contact"];
		   $data2["licenceNo"]=$row["licenceNo"];
		   $data2["issueDate"]=$row["issueDate"];
		   $data2["expiryDate"]=$row["expiryDate"];
		   $data2["licenseClass"]=$row["licenseClass"];
		   //$data2[""]=$row[""];
		   $sql2="select * from tbltrainingcourse where isActive='1'";
		   foreach($this->con->getResultSet($sql2) as $row2)
		   {
			   $arrayname=$row2["course"];
			   $courseId=$row2["id"];
			   $i=0;
			   $sql3="select * from tblcompliance where trainingId='$courseId' and driverId='$driverId' and isActive='1'";
			   $ed="";
			   $status="N/A";
			   foreach($this->con->getResultSet($sql3) as $row3)
		   		{
					$ed=$row3["expiryDate"];
					$issueD=$row3['issueDate'];
					$expiryDate=strtotime($row3['expiryDate']);
					$today=strtotime(date('Y-m-d'));
					if(is_null($row3['expiryDate'])){
						$status=$row3['status'];
					}else if(!is_null($row3['expiryDate'])){
						if($expiryDate>$today){
							$status="Valid";
						}
						else
						{
							$status="Invalid";
						}
					}
					//$data2[""]=$row3[""];
				}
				if(!is_null($ed)){
				$data2[$arrayname]=$ed;
				}
				$data2["status".$arrayname]=$status;
		   }
		   array_push($data,$data2);
	   }
	   return $data;
    	}
	
	public function viewJson() {
       
	   $driverId=-1;
	   $data=array();
	  
		   $sql2="select * from tbltrainingcourse where isActive='1'";
		   foreach($this->con->getResultSet($sql2) as $row2)
		   {
			   $heading=array();
			   $heading['name']="";
			   $heading['course']=$row2['course'];
			   $heading['issueDate']="";
			   $heading['expiryDate']="";
			   $heading['status']="";
			   $heading['id']="";
			   $heading['driverId']="";
			   $heading['trainingId']="";
			   array_push($data,$heading);
			   $arrayname=$row2["course"];
			   $courseId=$row2["id"];
			   $i=0;
			   $sql3="select cp.*,concat(concat(dr.firstName,' '),dr.lastName) name from tblcompliance cp inner join tbldriver dr on cp.driverId=dr.id where cp.trainingId='$courseId' and cp.isActive='1'";
			   $ed="";
			   $status="N/A";
			   foreach($this->con->getResultSet($sql3) as $row3)
		   		{
					$data2=array();
					$data2['name']=$row3['name'];
					$data2['course']=$arrayname;
					$data2['issueDate']=$row3['issueDate'];
					$data2['expiryDate']=$row3['expiryDate'];
					$data2['status']=$row3['isActive']==1?'VALID':'INVALID';
					$data2['id']=$row3['id'];
					$data2['driverId']=$row3['driverId'];
					$data2['trainingId']=$row3['trainingId'];
					array_push($data,$data2);
				}
				
				
		   }
		   
	  
	   return $data;
    }

	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblcompliance");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
	public function getWid()
	{
		$wid=-1;
		$sql="select * from tbltrainingcourse where id='".parent::gettrainingId()."'";
		foreach($this->con->getResultSet($sql) as $row)
		{
			$wid=$row['nextItem'];
		}
		return $wid;
	}
	
	public function getWidCourse($wid)
	{
		$widx="Nothing";
		$sql="select * from tbltrainingcourse where id=$wid";
		foreach($this->con->getResultSet($sql) as $row)
		{
			$widx=$row['course'];
		}
		return $widx;
	}
	
	
}

?>
