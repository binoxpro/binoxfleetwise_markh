<?php
include_once '../model/driverDocument.php';
class DriverDocumentService extends DriverDocument
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbldriverdocument");
		
		$builder->addColumnAndData("employeeId", parent::getemployeeId());
		$builder->addColumnAndData("documentType", parent::getdocumentType());
		$builder->addColumnAndData("issueDate", parent::getissueDate());
		$builder->addColumnAndData("expiryDate", parent::getexpiryDate());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbldriverdocument");
			$builder->addColumnAndData("employeeId", parent::getemployeeId());
            $builder->addColumnAndData("documentType", parent::getdocumentType());
            $builder->addColumnAndData("issueDate", parent::getissueDate());
			$builder->addColumnAndData("expiryDate", parent::getexpiryDate());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tbldriverdocument";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbldriverdocument");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
