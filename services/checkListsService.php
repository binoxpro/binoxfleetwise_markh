<?php
include_once '../model/checkLists.php';
class CheckListsService extends CheckLists
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblchecklists");
		
		$builder->addColumnAndData("driverId", parent::getdriverId());
		$builder->addColumnAndData("date", parent::getdate());
		$builder->addColumnAndData("employeeId", parent::getemployeeId());
		$builder->addColumnAndData("checklisttypeId", parent::getchecklisttypeId());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblchecklists");
			$builder->addColumnAndData("driverId", parent::getdriverId());
            $builder->addColumnAndData("date", parent::getdate());
            $builder->addColumnAndData("employeeId", parent::getemployeeId());
            $builder->addColumnAndData("checklisttypeId", parent::getchecklisttypeId());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblchecklists";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblchecklists");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
