<?php 
 require_once('../model/vehiclenewm.php');
 class vehiclenewmService extends vehiclenewm{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblvehicle');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('regNo',parent::getregNo());
 		 	 $builder->addColumnAndData('yom',parent::getyom());
 		 	 $builder->addColumnAndData('modelId',parent::getmodelId());
 		 	 $builder->addColumnAndData('dop',parent::getdop());
 		 	 $builder->addColumnAndData('chaseNumber',parent::getchaseNumber());
 		 	 $builder->addColumnAndData('enginecapacitycc',parent::getenginecapacitycc());
 		 	 $builder->addColumnAndData('vehicleTypeId',parent::getvehicleTypeId());
 		 	 $builder->addColumnAndData('tonnage',parent::gettonnage());
 		 	 $builder->addColumnAndData('color',parent::getcolor());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('fuel',parent::getfuel());
 		 	 $builder->addColumnAndData('purpose',parent::getpurpose());
 		 	 $builder->addColumnAndData('isTrailer',parent::getisTrailer());
 		 	 $builder->addColumnAndData('hasTrailer',parent::gethasTrailer());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblvehicle');

 	 	 if(!is_null(parent::getregNo())){
$builder->addColumnAndData('regNo',parent::getregNo()); 
}

 	 	 if(!is_null(parent::getyom())){
$builder->addColumnAndData('yom',parent::getyom()); 
}

 	 	 if(!is_null(parent::getmodelId())){
$builder->addColumnAndData('modelId',parent::getmodelId()); 
}

 	 	 if(!is_null(parent::getdop())){
$builder->addColumnAndData('dop',parent::getdop()); 
}

 	 	 if(!is_null(parent::getchaseNumber())){
$builder->addColumnAndData('chaseNumber',parent::getchaseNumber()); 
}

 	 	 if(!is_null(parent::getenginecapacitycc())){
$builder->addColumnAndData('enginecapacitycc',parent::getenginecapacitycc()); 
}

 	 	 if(!is_null(parent::getvehicleTypeId())){
$builder->addColumnAndData('vehicleTypeId',parent::getvehicleTypeId()); 
}

 	 	 if(!is_null(parent::gettonnage())){
$builder->addColumnAndData('tonnage',parent::gettonnage()); 
}

 	 	 if(!is_null(parent::getcolor())){
$builder->addColumnAndData('color',parent::getcolor()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getfuel())){
$builder->addColumnAndData('fuel',parent::getfuel()); 
}

 	 	 if(!is_null(parent::getpurpose())){
$builder->addColumnAndData('purpose',parent::getpurpose()); 
}

 	 	 if(!is_null(parent::getisTrailer())){
$builder->addColumnAndData('isTrailer',parent::getisTrailer()); 
}

 	 	 if(!is_null(parent::gethasTrailer())){
$builder->addColumnAndData('hasTrailer',parent::gethasTrailer()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tblvehicle";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tblvehicle";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  tblvehicle limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblvehicle');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tblvehicle  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setregNo($row['regNo']);
	 	parent::setyom($row['yom']);
	 	parent::setmodelId($row['modelId']);
	 	parent::setdop($row['dop']);
	 	parent::setchaseNumber($row['chaseNumber']);
	 	parent::setenginecapacitycc($row['enginecapacitycc']);
	 	parent::setvehicleTypeId($row['vehicleTypeId']);
	 	parent::settonnage($row['tonnage']);
	 	parent::setcolor($row['color']);
	 	parent::setstatus($row['status']);
	 	parent::setfuel($row['fuel']);
	 	parent::setpurpose($row['purpose']);
	 	parent::setisTrailer($row['isTrailer']);
	 	parent::sethasTrailer($row['hasTrailer']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>