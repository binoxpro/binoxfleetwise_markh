<?php 
 require_once('../model/tireRotation.php');
 class tireRotationService extends tireRotation{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltirerotation');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tireId',parent::gettireId());
 		 	 $builder->addColumnAndData('fromPosition',parent::getfromPosition());
 		 	 $builder->addColumnAndData('toPosition',parent::gettoPosition());
 		 	 $builder->addColumnAndData('tireHoldId',parent::gettireHoldId());
 		 	 $builder->addColumnAndData('kmsDone',parent::getkmsDone());
 		 	 $builder->addColumnAndData('inspectionDetailsId',parent::getinspectionDetailsId());
 		 	 $builder->addColumnAndData('CreationDate',parent::getCreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltirerotation');

 	 	 if(!is_null(parent::gettireId())){
$builder->addColumnAndData('tireId',parent::gettireId()); 
}

 	 	 if(!is_null(parent::getfromPosition())){
$builder->addColumnAndData('fromPosition',parent::getfromPosition()); 
}

 	 	 if(!is_null(parent::gettoPosition())){
$builder->addColumnAndData('toPosition',parent::gettoPosition()); 
}

 	 	 if(!is_null(parent::gettireHoldId())){
$builder->addColumnAndData('tireHoldId',parent::gettireHoldId()); 
}

 	 	 if(!is_null(parent::getkmsDone())){
$builder->addColumnAndData('kmsDone',parent::getkmsDone()); 
}

 	 	 if(!is_null(parent::getinspectionDetailsId())){
$builder->addColumnAndData('inspectionDetailsId',parent::getinspectionDetailsId()); 
}

 	 	 if(!is_null(parent::getCreationDate())){
$builder->addColumnAndData('CreationDate',parent::getCreationDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltirerotation";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltirerotation');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltirerotation where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settireId($row["tireId"]); 


 	 	 parent::setfromPosition($row["fromPosition"]); 


 	 	 parent::settoPosition($row["toPosition"]); 


 	 	 parent::settireHoldId($row["tireHoldId"]); 


 	 	 parent::setkmsDone($row["kmsDone"]); 


 	 	 parent::setinspectionDetailsId($row["inspectionDetailsId"]); 


 	 	 parent::setCreationDate($row["CreationDate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>