<?php 
 require_once('../model/department.php');
 class departmentService extends department{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbldepartment');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('Name',parent::getName());
 		 	 $builder->addColumnAndData('Prefix',parent::getPrefix());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbldepartment');

 	 	 if(!is_null(parent::getName())){
$builder->addColumnAndData('Name',parent::getName()); 
}

 	 	 if(!is_null(parent::getPrefix())){
$builder->addColumnAndData('Prefix',parent::getPrefix()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbldepartment";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbldepartment');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>