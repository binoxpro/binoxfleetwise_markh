<?php 
 require_once('../model/tireWarning.php');
 class tireWarningService extends tireWarning{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltirewarning');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('vthId',parent::getvthId());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('action',parent::getaction());
 		 	 $builder->addColumnAndData('sent',parent::getsent());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltirewarning');

 	 	 if(!is_null(parent::getvthId())){
$builder->addColumnAndData('vthId',parent::getvthId()); 
}

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); 
}

 	 	 if(!is_null(parent::getaction())){
$builder->addColumnAndData('action',parent::getaction()); 
}

 	 	 if(!is_null(parent::getsent())){
$builder->addColumnAndData('sent',parent::getsent()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltirewarning";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltirewarning');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltirewarning where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setvthId($row["vthId"]); 


 	 	 parent::setcomment($row["comment"]); 


 	 	 parent::setaction($row["action"]); 


 	 	 parent::setsent($row["sent"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>