<?php 
 require_once('../model/partSupply.php');
 class partSupplyService extends partSupply{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('partsupply');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('invoiceNo',parent::getinvoiceNo());
 		 	 $builder->addColumnAndData('deliveryNoteNo',parent::getdeliveryNoteNo());
 		 	 $builder->addColumnAndData('deliveryDate',parent::getdeliveryDate());
 		 	 $builder->addColumnAndData('partUsedId',parent::getpartUsedId());
 		 	 $builder->addColumnAndData('qty',parent::getqty());
 		 	 $builder->addColumnAndData('unitprice',parent::getunitprice());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('partsupply');

 	 	 if(!is_null(parent::getinvoiceNo())){
$builder->addColumnAndData('invoiceNo',parent::getinvoiceNo()); 
}

 	 	 if(!is_null(parent::getdeliveryNoteNo())){
$builder->addColumnAndData('deliveryNoteNo',parent::getdeliveryNoteNo()); 
}

 	 	 if(!is_null(parent::getdeliveryDate())){
$builder->addColumnAndData('deliveryDate',parent::getdeliveryDate()); 
}

 	 	 if(!is_null(parent::getpartUsedId())){
$builder->addColumnAndData('partUsedId',parent::getpartUsedId()); 
}

 	 	 if(!is_null(parent::getqty())){
$builder->addColumnAndData('qty',parent::getqty()); 
}

 	 	 if(!is_null(parent::getunitprice())){
$builder->addColumnAndData('unitprice',parent::getunitprice()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  partsupply";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  partsupply";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  partsupply limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('partsupply');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  partsupply  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setinvoiceNo($row['invoiceNo']);
	 	parent::setdeliveryNoteNo($row['deliveryNoteNo']);
	 	parent::setdeliveryDate($row['deliveryDate']);
	 	parent::setpartUsedId($row['partUsedId']);
	 	parent::setqty($row['qty']);
	 	parent::setunitprice($row['unitprice']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>