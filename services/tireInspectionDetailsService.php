<?php 
 require_once('../model/tireInspectionDetails.php');
 class tireInspectionDetailsService extends tireInspectionDetails{
	 	 public function save(){
	 	     $builder=new InsertBuilder();
	 	     $builder->setTable('tbltireinspectiondetails');
	 	     $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tireHoldId',parent::gettireHoldId());
 		 	 $builder->addColumnAndData('treadDepth',parent::gettreadDepth());
 		 	 $builder->addColumnAndData('pressure',parent::getpressure());
 		 	 $builder->addColumnAndData('sidewall',parent::getsidewall());
 		 	 $builder->addColumnAndData('tread',parent::gettread());
 		 	 $builder->addColumnAndData('mech',parent::getmech());
 		 	 $builder->addColumnAndData('mismatchedDual',parent::getmismatchedDual());
 		 	 $builder->addColumnAndData('valveCondition',parent::getvalveCondition());
 		 	 $builder->addColumnAndData('rimCondition',parent::getrimCondition());
 		 	 $builder->addColumnAndData('inspectionHeaderId',parent::getinspectionHeaderId());
 		 	 $builder->addColumnAndData('generalRemarks',parent::getgeneralRemarks());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltireinspectiondetails');

 	 	 if(!is_null(parent::gettireHoldId())){
$builder->addColumnAndData('tireHoldId',parent::gettireHoldId()); 
}

 	 	 if(!is_null(parent::gettreadDepth())){
$builder->addColumnAndData('treadDepth',parent::gettreadDepth()); 
}

 	 	 if(!is_null(parent::getpressure())){
$builder->addColumnAndData('pressure',parent::getpressure()); 
}

 	 	 if(!is_null(parent::getsidewall())){
$builder->addColumnAndData('sidewall',parent::getsidewall()); 
}

 	 	 if(!is_null(parent::gettread())){
$builder->addColumnAndData('tread',parent::gettread()); 
}

 	 	 if(!is_null(parent::getmech())){
$builder->addColumnAndData('mech',parent::getmech()); 
}

 	 	 if(!is_null(parent::getmismatchedDual())){
$builder->addColumnAndData('mismatchedDual',parent::getmismatchedDual()); 
}

 	 	 if(!is_null(parent::getvalveCondition())){
$builder->addColumnAndData('valveCondition',parent::getvalveCondition()); 
}

 	 	 if(!is_null(parent::getrimCondition())){
$builder->addColumnAndData('rimCondition',parent::getrimCondition()); 
}

 	 	 if(!is_null(parent::getinspectionHeaderId())){
$builder->addColumnAndData('inspectionHeaderId',parent::getinspectionHeaderId()); 
}

 	 	 if(!is_null(parent::getgeneralRemarks())){
$builder->addColumnAndData('generalRemarks',parent::getgeneralRemarks()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltireinspectiondetails tid inner join tbltirehold th on th.id=tid";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltireinspectiondetails');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltireinspectiondetails where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settireHoldId($row["tireHoldId"]); 


 	 	 parent::settreadDepth($row["treadDepth"]); 


 	 	 parent::setpressure($row["pressure"]); 


 	 	 parent::setsidewall($row["sidewall"]); 


 	 	 parent::settread($row["tread"]); 


 	 	 parent::setmech($row["mech"]); 


 	 	 parent::setmismatchedDual($row["mismatchedDual"]); 


 	 	 parent::setvalveCondition($row["valveCondition"]); 


 	 	 parent::setrimCondition($row["rimCondition"]); 


 	 	 parent::setinspectionHeaderId($row["inspectionHeaderId"]); 


 	 	 parent::setgeneralRemarks($row["generalRemarks"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>