<?php 
 require_once('../model/jbcardx.php');
 class jbcardxService extends jbcardx{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbljobcard');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('jobNo',parent::getjobNo());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('rectified',parent::getrectified());
 		 	 $builder->addColumnAndData('dateCompleted',parent::getdateCompleted());
 		 	 $builder->addColumnAndData('odometer',parent::getodometer());
 		 	 $builder->addColumnAndData('time',parent::gettime());
 		 	 $builder->addColumnAndData('checklistId',parent::getchecklistId());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('labourCost',parent::getlabourCost());
 		 	 $builder->addColumnAndData('labourHour',parent::getlabourHour());
 		 	 $builder->addColumnAndData('garrage',parent::getgarrage());
 		 	 $builder->addColumnAndData('fuelLevel',parent::getfuelLevel());
 		 	 $builder->addColumnAndData('driverName',parent::getdriverName());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbljobcard');

 	 	 if(!is_null(parent::getjobNo())){
$builder->addColumnAndData('jobNo',parent::getjobNo()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getrectified())){
$builder->addColumnAndData('rectified',parent::getrectified()); 
}

 	 	 if(!is_null(parent::getdateCompleted())){
$builder->addColumnAndData('dateCompleted',parent::getdateCompleted()); 
}

 	 	 if(!is_null(parent::getodometer())){
$builder->addColumnAndData('odometer',parent::getodometer()); 
}

 	 	 if(!is_null(parent::gettime())){
$builder->addColumnAndData('time',parent::gettime()); 
}

 	 	 if(!is_null(parent::getchecklistId())){
$builder->addColumnAndData('checklistId',parent::getchecklistId()); 
}

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); 
}

 	 	 if(!is_null(parent::getlabourCost())){
$builder->addColumnAndData('labourCost',parent::getlabourCost()); 
}

 	 	 if(!is_null(parent::getlabourHour())){
$builder->addColumnAndData('labourHour',parent::getlabourHour()); 
}

 	 	 if(!is_null(parent::getgarrage())){
$builder->addColumnAndData('garrage',parent::getgarrage()); 
}

 	 	 if(!is_null(parent::getfuelLevel())){
$builder->addColumnAndData('fuelLevel',parent::getfuelLevel()); 
}

 	 	 if(!is_null(parent::getdriverName())){
$builder->addColumnAndData('driverName',parent::getdriverName()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tbljobcard";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tbljobcard";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  tbljobcard limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbljobcard');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tbljobcard  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setjobNo($row['jobNo']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setvehicleId($row['vehicleId']);
	 	parent::setrectified($row['rectified']);
	 	parent::setdateCompleted($row['dateCompleted']);
	 	parent::setodometer($row['odometer']);
	 	parent::settime($row['time']);
	 	parent::setchecklistId($row['checklistId']);
	 	parent::setcomment($row['comment']);
	 	parent::setlabourCost($row['labourCost']);
	 	parent::setlabourHour($row['labourHour']);
	 	parent::setgarrage($row['garrage']);
	 	parent::setfuelLevel($row['fuelLevel']);
	 	parent::setdriverName($row['driverName']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>