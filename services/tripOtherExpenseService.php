<?php 
 require_once('../model/tripOtherExpense.php');
 class TripOtherExpenseService extends TripOtherExpense{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tripotherexpense');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
 		 	 $builder->addColumnAndData('expenseId',parent::getexpenseId());
 		 	 $builder->addColumnAndData('Amount',parent::getAmount());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update()
		 {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tripotherexpense');

 	 	 if(!is_null(parent::gettripNo()))
		{
		$builder->addColumnAndData('tripNo',parent::gettripNo());
		}

 	 	 if(!is_null(parent::getexpenseId()))
	 	{
			$builder->addColumnAndData('expenseId',parent::getexpenseId());
		}

 	 	 if(!is_null(parent::getAmount()))
	 	{
		$builder->addColumnAndData('Amount',parent::getAmount());
		}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tripotherexpense";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select toe.*,et.expenseName,et.accountCode from  tripotherexpense toe inner join  expensetemplate et on toe.expenseId=et.id where toe.tripNo='".parent::gettripNo()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select toe.*,et.expenseName,et.accountCode from  tripotherexpense toe inner join  expensetemplate et on toe.expenseId=et.id where tripNo='".parent::gettripNo()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
	 public function viewPayment()
	 {
		 $balance=0;
		 $totalPaid=0;
		 $totalBalance=0;
		 $totalAmount=0;
		 $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		 $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		 $offset = ($page-1)*$rows;
		 $sql="select toe.*,et.expenseName,et.accountCode,(select sum(gl.amount) from generaledger gl where gl.accountCode=et.accountCode and gl.tripNo=toe.tripNo ) Paid from  tripotherexpense toe inner join expensetemplate et on toe.expenseId=et.id where toe.tripNo='".parent::gettripNo()."'";
		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql="select toe.*,et.expenseName,et.accountCode,(select sum(gl.amount) from generaledger gl where gl.accountCode=et.accountCode and gl.tripNo=toe.tripNo ) Paid from  tripotherexpense toe inner join expensetemplate et on toe.expenseId=et.id where toe.tripNo='".parent::gettripNo()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['Paid']=='' || is_null($row['Paid']))
			 {
				 $row['Paid']=0;
			 }
			 $balance=$row['Amount']-$row['Paid'];
			 $totalBalance=$totalBalance+$balance;
			 $totalPaid=$totalPaid+$row['Paid'];
			 $totalAmount=$totalAmount+$row['Amount'];
			 $row['balance']=$balance;

			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 $data3=array();
		 $data3['expenseName']='Total';
		 $data3['Amount']=$totalAmount;
		 $data3['Paid']=$totalPaid;
		 $data3['balance']=$totalBalance;
		 $data4=array();
		 array_push($data4,$data3);
		 $data['footer']=$data4;
		 return $data;
	 }
 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tripotherexpense');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  tripotherexpense  where id='".parent::getid()."'";
			 foreach($this->con->getResultSet($sql) as $row)
			{
			parent::setid($row['id']);
			parent::settripNo($row['tripNo']);
			parent::setexpenseId($row['expenseId']);
			parent::setAmount($row['Amount']);
			}
	 }

	 public function findMileageCost($tripNo)
	 {
		 $sql="select sum(toe.Amount) total from  tripotherexpense toe inner join expensetemplate et on  toe.expenseId=et.id  where et.expenseName='Mileage' and toe.tripNo='$tripNo'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setAmount($row['total']);
		 }
	 }

	 public function findOtherCost($tripNo)
	 {

		 $sql="select sum(toe.Amount) total from  tripotherexpense toe inner join expensetemplate et on  toe.expenseId=et.id  where et.expenseName!='Mileage'and   toe.tripNo='$tripNo'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['total']=='null'||$row['total']=='')
			 {
				 parent::setAmount('-');
			 }else
			 {
				 parent::setAmount($row['total']);
			 }

		 }
	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>