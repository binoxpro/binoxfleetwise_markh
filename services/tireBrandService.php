<?php 
 require_once('../model/tireBrand.php');
 class tireBrandService extends tireBrand{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltireband');
	 	 $builder->addColumnAndData('id',parent::getid());
	 	 $builder->addColumnAndData('brand',parent::getbrand());
	 	 $builder->addColumnAndData('isActive',parent::getisActive());
	 	 $this->con->setQuery(Director::buildSql($builder));
         $this->con->setSelect_query("select * from  tbltireband where brand='".parent::getbrand()."'");
         $no=$this->con->sqlCount();
             
             if($no<=0){
    	 	     return $this->con->execute_query2($builder->getValues());
             }else{
                
             }
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltireband');

 	 	 if(!is_null(parent::getbrand())){
$builder->addColumnAndData('brand',parent::getbrand()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltireband";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltireband');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltireband where id='".parent::getid()."'";
         
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setbrand($row["brand"]); 


 	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>