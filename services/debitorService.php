<?php 
 require_once('../model/debitor.php');
 class debitorService extends debitor{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('debitor');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('contactPerson',parent::getcontactPerson());
 		 	 $builder->addColumnAndData('companyName',parent::getcompanyName());
 		 	 $builder->addColumnAndData('building',parent::getbuilding());
 		 	 $builder->addColumnAndData('town',parent::gettown());
 		 	 $builder->addColumnAndData('district',parent::getdistrict());
 		 	 $builder->addColumnAndData('country',parent::getcountry());
 		 	 $builder->addColumnAndData('tel',parent::gettel());
 		 	 $builder->addColumnAndData('email',parent::getemail());
 		 	 $builder->addColumnAndData('creditPeriod',parent::getcreditPeriod());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('debitor');

 	 	 if(!is_null(parent::getcontactPerson())){
$builder->addColumnAndData('contactPerson',parent::getcontactPerson()); 
}

 	 	 if(!is_null(parent::getcompanyName())){
$builder->addColumnAndData('companyName',parent::getcompanyName()); 
}

 	 	 if(!is_null(parent::getbuilding())){
$builder->addColumnAndData('building',parent::getbuilding()); 
}

 	 	 if(!is_null(parent::gettown())){
$builder->addColumnAndData('town',parent::gettown()); 
}

 	 	 if(!is_null(parent::getdistrict())){
$builder->addColumnAndData('district',parent::getdistrict()); 
}

 	 	 if(!is_null(parent::getcountry())){
$builder->addColumnAndData('country',parent::getcountry()); 
}

 	 	 if(!is_null(parent::gettel())){
$builder->addColumnAndData('tel',parent::gettel()); 
}

 	 	 if(!is_null(parent::getemail())){
$builder->addColumnAndData('email',parent::getemail()); 
}

 	 	 if(!is_null(parent::getcreditPeriod())){
$builder->addColumnAndData('creditPeriod',parent::getcreditPeriod()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  debitor";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  debitor";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  debitor limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('debitor');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  debitor  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setcontactPerson($row['contactPerson']);
	 	parent::setcompanyName($row['companyName']);
	 	parent::setbuilding($row['building']);
	 	parent::settown($row['town']);
	 	parent::setdistrict($row['district']);
	 	parent::setcountry($row['country']);
	 	parent::settel($row['tel']);
	 	parent::setemail($row['email']);
	 	parent::setcreditPeriod($row['creditPeriod']);
	 	parent::setisActive($row['isActive']);
}	 	}
	 public function formatAddress()
	 {
		 $strHtml="";
		 $this->find();
		 //AFTER THE CLIENT TO REMOVE NAME
		 //$strHtml.="<h5>".parent::getcontactPerson()."</h5>";
		 $strHtml.="<p>".parent::getcompanyName()."<br/>".parent::getbuilding()."<br/>".parent::gettown().", ".parent::getcountry()."<br/>".parent::gettel()."</p>";
		return $strHtml;
	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>