<?php 
 require_once('../model/vehicleConfiguration.php');
 class vehicleConfigurationService extends vehicleConfiguration{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblvehicleconfiguration');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('configurationName',parent::getconfigurationName());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblvehicleconfiguration');

 	 	 if(!is_null(parent::getconfigurationName())){
$builder->addColumnAndData('configurationName',parent::getconfigurationName()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblvehicleconfiguration";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblvehicleconfiguration');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tblvehicleconfiguration  where id='".parent::getid()."'";
		 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 		parent::setid($row['id']);
 		 	parent::setconfigurationName($row['configurationName']);
 		}


	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>