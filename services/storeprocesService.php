<?php 
 require_once('../model/storeproces.php');
 class storeprocesService extends storeproces{
	 	 public function save()
         {
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('storeprocess');
	 	 $builder->addColumnAndData('id',parent::getid());
         $builder->addColumnAndData('prfId',parent::getprfId());
         $builder->addColumnAndData('processHistoryId',parent::getprocessHistoryId());
         $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('storeprocess');

 	 	 if(!is_null(parent::getprfId())){
$builder->addColumnAndData('prfId',parent::getprfId()); 
}

 	 	 if(!is_null(parent::getprocessHistoryId())){
$builder->addColumnAndData('processHistoryId',parent::getprocessHistoryId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  storeprocess";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  storeprocess";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  storeprocess limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }

     public function viewProcess($processId){
         $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
         $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
         $offset = ($page-1)*$rows;
         $sql="select * from  storeprocesslist where processId='".$processId."' and IsActive='1'";
         $this->con->setSelect_query($sql);
         $data2=array();
         $data=array();
         $data["total"]=$this->con->sqlCount();
         $sql="select * from  storeprocesslist where processId='".$processId."' and IsActive='1' limit $offset,$rows ";
         foreach($this->con->getResultSet($sql) as $row)
         {
             array_push($data2,$row);
         }$data["rows"]=$data2;return $data; }
     public function viewProcessSearch($processId,$vehicleId=NULL,$jobcardNo=Null,$startDate=NULL,$endDate=null){
         $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
         $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
         $offset = ($page-1)*$rows;
         $sql='';
         //$sql="select * from  storeprocesslist where processId='".$processId."' and IsActive='1'";
         if(!is_null($vehicleId)&&!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and jobcardNo='".$jobcardNo."' and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1'";
         }else if(!is_null($vehicleId)&&!is_null($jobcardNo))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and jobcardNo='".$jobcardNo."'  and processId='".$processId."' and IsActive='1'";

         }else if(!is_null($vehicleId)&&(!is_null($startDate)&&!is_null($endDate))){
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."'  and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1'";


         }else if(!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where jobcardNo='".$jobcardNo."' and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1'";


         }else if(!is_null($vehicleId))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and processId='".$processId."' and IsActive='1'";

         }else if(!is_null($jobcardNo))
         {
             $sql="select * from  storeprocesslist where  jobcardNo='".$jobcardNo."'  and processId='".$processId."' and IsActive='1'";

         }else if((!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1'";

         }
         $this->con->setSelect_query($sql);
         $data2=array();
         $data=array();
         $data["total"]=$this->con->sqlCount();
         $sql='';
         //$sql="select * from  storeprocesslist where processId='".$processId."' and IsActive='1' limit $offset,$rows ";
         if(!is_null($vehicleId)&&!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and jobcardNo='".$jobcardNo."' and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1' limit $offset,$rows ";
         }else if(!is_null($vehicleId)&&!is_null($jobcardNo))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and jobcardNo='".$jobcardNo."'  and processId='".$processId."' and IsActive='1' limit $offset,$rows ";

         }else if(!is_null($vehicleId)&&(!is_null($startDate)&&!is_null($endDate))){
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."'  and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1' limit $offset,$rows ";


         }else if(!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where jobcardNo='".$jobcardNo."' and (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1' limit $offset,$rows ";


         }else if(!is_null($vehicleId))
         {
             $sql="select * from  storeprocesslist where vehicleId='".$vehicleId."' and processId='".$processId."' and IsActive='1' limit $offset,$rows ";

         }else if(!is_null($jobcardNo))
         {
             $sql="select * from  storeprocesslist where  jobcardNo='".$jobcardNo."'  and processId='".$processId."' and IsActive='1' limit $offset,$rows ";

         }else if((!is_null($startDate)&&!is_null($endDate)))
         {
             $sql="select * from  storeprocesslist where (requestDate between '".$startDate."' and '".$endDate."') and processId='".$processId."' and IsActive='1' limit $offset,$rows ";

         }
         foreach($this->con->getResultSet($sql) as $row)
         {
             array_push($data2,$row);
         }
         $data["rows"]=$data2;
         return $data;
     }



 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('storeprocess');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  storeprocess  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setprfId($row['prfId']);
	 	parent::setprocessHistoryId($row['processHistoryId']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>