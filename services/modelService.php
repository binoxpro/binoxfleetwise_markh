<?php 
 require_once('../model/model.php');
 class modelService extends model{
	 	 public function save(){
	 		 $builder=new InsertBuilder();
	 	 	$builder->setTable('tblmodel');
	 	 	$builder->addColumnAndData('modelName',parent::getmodelName());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblmodel');
			$builder->addColumnAndData('modelName',parent::getmodelName());
 	 	 if(parent::getisActive!=""){
$builder->addColumnAndData('isActive',parent::getisActive()); }
$builder->setCriteria("where modelName='".parent::getmodelName()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblmodel";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblmodel');
	 	 $builder->setCriteria("where modelName='".parent::getmodelName()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>