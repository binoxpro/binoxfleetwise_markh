<?php
include_once '../model/jobcardItem.php';
class JobcardItemService extends JobcardItem
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbljobcarditem");
		
		$builder->addColumnAndData("details", parent::getdetails());
		
		$builder->addColumnAndData("timeTaken", parent::gettimeTaken());
		$builder->addColumnAndData("initials", parent::getinitials());
		$builder->addColumnAndData("jobcardId", parent::getjobcardId());
        $builder->addColumnAndData("repairReview", parent::getrepairReview());
        $builder->addColumnAndData("status", parent::getstatus());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tbljobcarditem");
            if(!is_null(parent::getdetails()))
			{
                $builder->addColumnAndData("details", parent::getdetails());
			}
        if(!is_null(parent::getinitials())) {
            $builder->addColumnAndData("timeTaken", parent::gettimeTaken());
        }
        if(!is_null(parent::getinitials())) {
            $builder->addColumnAndData("initials", parent::getinitials());
        }
        if(!is_null(parent::getrepairReview())) {
            $builder->addColumnAndData("repairReview", parent::getrepairReview());
        }
        if(!is_null(parent::getstatus())) {
            $builder->addColumnAndData("status", parent::getstatus());
        }
			//$builder->addColumnAndData("jobcardId", parent::getjobcardId());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view()
	{
		$data=array();
		$total=0;
		$bal=0;
		$totalPaid=0;
      // $sql="select jci.*,(select sum(lp.amount) from labourpayment lp where lp.labourId=jci.id) paidAmount from tbljobcarditem jci where jci.jobcardId='".parent::getjobcardId()."'";

		$sql="Select * from tbljobcarditem where jobcardId='".parent::getjobcardId()."'";

		foreach($this->con->getResultSet($sql) as $row)
	   {

		   array_push($data,$row);
	   }

		return $data;
    }
	
	public function viewLabour()
	{
		//sql=
	}
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbljobcarditem");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
