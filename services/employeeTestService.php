<?php 
 require_once('../model/employeeTest.php');
 class employeeTestService extends employeeTest{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblemployee');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('firstName',parent::getfirstName());
 		 	 $builder->addColumnAndData('lastName',parent::getlastName());
 		 	 $builder->addColumnAndData('dob',parent::getdob());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $builder->addColumnAndData('regDate',parent::getregDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValiues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblemployee');

 	 	 if(parent::getfirstName==""){
$builder->addColumnAndData('firstName',parent::getfirstName()); }

 	 	 if(parent::getlastName==""){
$builder->addColumnAndData('lastName',parent::getlastName()); }

 	 	 if(parent::getdob==""){
$builder->addColumnAndData('dob',parent::getdob()); }

 	 	 if(parent::getisActive==""){
$builder->addColumnAndData('isActive',parent::getisActive()); }

 	 	 if(parent::getregDate==""){
$builder->addColumnAndData('regDate',parent::getregDate()); }
$builder->setCriteria("where id='".parent::getid."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblemployee";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblemployee');
	 	 $builder->setCriteria("where id='".parent::getid."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
