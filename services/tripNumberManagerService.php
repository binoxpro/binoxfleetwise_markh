<?php 
 require_once('../model/tripNumberManager.php');
 class tripNumberManagerService extends tripNumberManager{
	 	 /**
	 	  * tripNumberManagerService::save()
	 	  * 
	 	  * @return
	 	  */
	 	 public function save()
          {
    	 	 $builder=new InsertBuilder();
    	 	 $builder->setTable('tbltripnumbermanager');
    	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
 		 	 $builder->addColumnAndData('tripTypeId',parent::gettripTypeId());
 		 	   if(!is_null(parent::getOrderNo()))
               {
                $builder->addColumnAndData('OrderNo',parent::getOrderNo()); 
               }
 		 	 $this->con->setQuery(Director::buildSql($builder));
            $this->con->execute_query2($builder->getValues());
            return $this->con->getId();
 	      }
 
	 	 /**
	 	  * tripNumberManagerService::update()
	 	  * 
	 	  * @return
	 	  */
	 	 public function update()
          {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltripnumbermanager');

 	 	 if(!is_null(parent::gettripNo()))
           {
             $builder->addColumnAndData('tripNo',parent::gettripNo()); 
           }

 	 	 if(!is_null(parent::gettripTypeId()))
           {
            $builder->addColumnAndData('tripTypeId',parent::gettripTypeId()); 
           }

 	 	 if(!is_null(parent::getOrderNo()))
           {
            $builder->addColumnAndData('OrderNo',parent::getOrderNo()); 
           }
            $builder->setCriteria("where id='".parent::getid()."'");
            $this->con->setQuery(Director::buildSql($builder));
	 	     return $this->con->execute_query();
 }
 

 	 /**
 	  * tripNumberManagerService::view()
 	  * 
 	  * @return
 	  */
 	 public function view(){
	 	 $sql="select * from  tbltripnumbermanager";
	 return $this->con->getResultSet($sql);
 	 }

 	 /**
 	  * tripNumberManagerService::delete()
 	  * 
 	  * @return
 	  */
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltripnumbermanager');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 /**
	 	  * tripNumberManagerService::getObject()
	 	  * 
	 	  * @return
	 	  */
	 	 public function getObject(){
	 	 $sql="select * from  tbltripnumbermanager where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settripNo($row["tripNo"]); 


 	 	 parent::settripTypeId($row["tripTypeId"]); 


 	 	 parent::setOrderNo($row["OrderNo"]); 

	 	} 
 }
 /**
  * tripNumberManagerService::getLastTripNo()
  * 
  * @param mixed $name
  * @return
  */
 public function getLastTripNo($name){
	 	 $sql="select * from  tbltripnumbermanager where tripTypeId='".parent::gettripTypeId()."'";
         $this->con->setSelect_query($sql);
         $countNo=$this->con->sqlCount();
         $newTripCount=$countNo+1;
	 	 $tripNo=$name."/".$newTripCount."/".date('y');
         $this->settripNo($tripNo);
         return $this->save();
 }

 	 /**
 	  * tripNumberManagerService::view_query()
 	  * 
 	  * @param mixed $sql
 	  * @return
 	  */
 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>