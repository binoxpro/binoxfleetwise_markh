<?php 
 require_once('../model/newOrder.php');
 class newOrderService extends newOrder{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblneworder');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	  if(!is_null(parent::getcustomerName())){
                $builder->addColumnAndData('customerName',parent::getcustomerName()); 
                }

 	 	 if(!is_null(parent::getorderDate())){
            $builder->addColumnAndData('orderDate',parent::getorderDate()); 
            }

 	 	 if(!is_null(parent::getloadDate())){
        $builder->addColumnAndData('loadDate',parent::getloadDate()); 
        }

 	 	 if(!is_null(parent::getorderNo())){
        $builder->addColumnAndData('orderNo',parent::getorderNo()); 
        }

 	 	 if(!is_null(parent::getulg())){
        $builder->addColumnAndData('ulg',parent::getulg()); 
        }

 	 	 if(!is_null(parent::getago())){
        $builder->addColumnAndData('ago',parent::getago()); 
        }

 	 	 if(!is_null(parent::getbik())){
        $builder->addColumnAndData('bik',parent::getbik()); 
        }

 	 	 if(!is_null(parent::getvpower())){
        $builder->addColumnAndData('vpower',parent::getvpower()); 
        }

 	 	 if(!is_null(parent::getdepotTimeIn())){
        $builder->addColumnAndData('depotTimeIn',parent::getdepotTimeIn()); 
        }

 	 	 if(!is_null(parent::getdepotTimeOut())){
        $builder->addColumnAndData('depotTimeOut',parent::getdepotTimeOut()); 
        }

 	 	 if(!is_null(parent::getvehicleId())){
            $builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
            }

 	 	 if(!is_null(parent::getdeliveryDate())){
        $builder->addColumnAndData('deliveryDate',parent::getdeliveryDate()); 
        }

 	 	 if(!is_null(parent::getdeliveryTime())){
        $builder->addColumnAndData('deliveryTime',parent::getdeliveryTime()); 
        }

 	 	 if(!is_null(parent::gettripNo())){
        $builder->addColumnAndData('tripNo',parent::gettripNo()); 
        }

 	 	 if(!is_null(parent::getRevenue())){
        $builder->addColumnAndData('Revenue',parent::getRevenue()); 
        }

 	 	 if(!is_null(parent::getExpense())){
        $builder->addColumnAndData('Expense',parent::getExpense()); 
        }

 	 	 if(!is_null(parent::gettotalProduct())){
        $builder->addColumnAndData('totalProduct',parent::gettotalProduct()); 
        }

 	 	 if(!is_null(parent::getsystDate())){
        $builder->addColumnAndData('systDate',parent::getsystDate()); 
        }

 	 	 if(!is_null(parent::getisActive()))
        {
        $builder->addColumnAndData('isActive',parent::getisActive()); 
        }
        
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	  $this->con->execute_query2($builder->getValues());
          return $this->con->getId();
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblneworder');

 	 	 if(!is_null(parent::getcustomerName())){
$builder->addColumnAndData('customerName',parent::getcustomerName()); 
}

 	 	 if(!is_null(parent::getorderDate())){
            $builder->addColumnAndData('orderDate',parent::getorderDate()); 
            }

 	 	 if(!is_null(parent::getloadDate())){
        $builder->addColumnAndData('loadDate',parent::getloadDate()); 
        }

 	 	 if(!is_null(parent::getorderNo())){
        $builder->addColumnAndData('orderNo',parent::getorderNo()); 
        }

 	 	 if(!is_null(parent::getulg())){
        $builder->addColumnAndData('ulg',parent::getulg()); 
        }

 	 	 if(!is_null(parent::getago())){
        $builder->addColumnAndData('ago',parent::getago()); 
        }

 	 	 if(!is_null(parent::getbik())){
        $builder->addColumnAndData('bik',parent::getbik()); 
        }

 	 	 if(!is_null(parent::getvpower())){
        $builder->addColumnAndData('vpower',parent::getvpower()); 
        }

 	 	 if(!is_null(parent::getdepotTimeIn())){
        $builder->addColumnAndData('depotTimeIn',parent::getdepotTimeIn()); 
        }

 	 	 if(!is_null(parent::getdepotTimeOut())){
        $builder->addColumnAndData('depotTimeOut',parent::getdepotTimeOut()); 
        }

 	 	 if(!is_null(parent::getvehicleId())){
            $builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
            }

 	 	 if(!is_null(parent::getdeliveryDate())){
        $builder->addColumnAndData('deliveryDate',parent::getdeliveryDate()); 
        }

 	 	 if(!is_null(parent::getdeliveryTime())){
        $builder->addColumnAndData('deliveryTime',parent::getdeliveryTime()); 
        }

 	 	 if(!is_null(parent::gettripNo())){
        $builder->addColumnAndData('tripNo',parent::gettripNo()); 
        }

 	 	 if(!is_null(parent::getRevenue())){
        $builder->addColumnAndData('Revenue',parent::getRevenue()); 
        }

 	 	 if(!is_null(parent::getExpense())){
        $builder->addColumnAndData('Expense',parent::getExpense()); 
        }

 	 	 if(!is_null(parent::gettotalProduct())){
        $builder->addColumnAndData('totalProduct',parent::gettotalProduct()); 
        }

 	 	 if(!is_null(parent::getsystDate())){
        $builder->addColumnAndData('systDate',parent::getsystDate()); 
        }

 	 	 if(!is_null(parent::getisActive()))
        {
        $builder->addColumnAndData('isActive',parent::getisActive()); 
        }
        
        
        $builder->setCriteria("where id='".parent::getid()."'");
        $this->con->setQuery(Director::buildSql($builder));
        	 	 return $this->con->execute_query();
 }
 

 	  public function view()
      {
 	      $today=date('Y-m-d');
        $sql="select nod.*,(select tnm.tripNo from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripNo,(select v.regNo from tblvehicle v where v.id=nod.vehicleId) vehicleNumber,(select tnm.tripTypeId from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripType from  tblneworder nod where nod.loadDate = '$today'";
        return $this->con->getResultSet($sql);
 	  }
     
      public function view2()
      {
        $sql="select nod.*,(select tnm.tripNo from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripNo,(select v.regNo from tblvehicle v where v.id=nod.vehicleId) vehicleNumber,(select tnm.tripTypeId from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripType from  tblneworder nod where nod.deliveryTime is null and nod.deliveryDate is null";
        return $this->con->getResultSet($sql);
 	  }
      
      public function view3()
      {
        $sql="select nod.*,(select tnm.tripNo from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripNo,(select v.regNo from tblvehicle v where v.id=nod.vehicleId) vehicleNumber,(select tnm.tripTypeId from tbltripnumbermanager tnm where tnm.id=nod.tripNo) tripType from  tblneworder nod";
        return $this->con->getResultSet($sql);
 	  }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblneworder');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblneworder where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setcustomerName($row["customerName"]); 


 	 	 parent::setorderDate($row["orderDate"]); 


 	 	 parent::setloadDate($row["loadDate"]); 


 	 	 parent::setorderNo($row["orderNo"]); 


 	 	 parent::setulg($row["ulg"]); 


 	 	 parent::setago($row["ago"]); 


 	 	 parent::setbik($row["bik"]); 


 	 	 parent::setvpower($row["vpower"]); 


 	 	 parent::setdepotTimeIn($row["depotTimeIn"]); 


 	 	 parent::setdepotTimeOut($row["depotTimeOut"]); 


 	 	 parent::setvehicleId($row["vehicleId"]); 


 	 	 parent::setdeliveryDate($row["deliveryDate"]); 


 	 	 parent::setdeliveryTime($row["deliveryTime"]); 


 	 	 parent::settripNo($row["tripNo"]); 


 	 	 parent::setRevenue($row["Revenue"]); 


 	 	 parent::setExpense($row["Expense"]); 


 	 	 parent::settotalProduct($row["totalProduct"]); 


 	 	 parent::setsystDate($row["systDate"]); 


 	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>