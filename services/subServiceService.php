<?php 
 require_once('../model/subService.php');
 class subServiceService extends subService{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblsubservice');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('componentScheduleId',parent::getcomponentScheduleId());
 		 	 $builder->addColumnAndData('serviceHeaderId',parent::getserviceHeaderId());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('done',parent::getdone());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update()
		 {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsubservice');

 	 	 if(!is_null(parent::getcomponentScheduleId())){
$builder->addColumnAndData('componentScheduleId',parent::getcomponentScheduleId()); }

 	 	 if(!is_null(parent::getserviceHeaderId())){
$builder->addColumnAndData('serviceHeaderId',parent::getserviceHeaderId()); }

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); }

 	 	 if(!is_null(parent::getdone())){
$builder->addColumnAndData('done',parent::getdone());
 }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 
 
 public function updateNewChange()
		 {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsubservice');

 	 	 if(!is_null(parent::getcomponentScheduleId())){
$builder->addColumnAndData('componentScheduleId',parent::getcomponentScheduleId()); }

 	 	
 	 	 if(!is_null(parent::getcomment()))
           {
            $builder->addColumnAndData('comment',parent::getcomment()); 
           }

 	 	 if(!is_null(parent::getdone())){
            $builder->addColumnAndData('done',parent::getdone());
         }
        $builder->setCriteria("where serviceHeaderId='".parent::getserviceHeaderId()."'");
        $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="Select ss.id,c.componentName,(select an.detail from tblactionnote an where an.id=cs.actionNoteId ) actionNote,ss.comment,ss.done from tblsubservice ss inner join tblcomponentschedule cs on ss.componentScheduleId=cs.id inner join tblmodelcomponent mc on cs.componentModelId=mc.id inner join tblcomponent c on mc.componentId=c.id";
	 return $this->con->getResultSet($sql);
 	 }
	 public function viewHeader(){
	 	 $sql="Select ss.id,c.componentName,(select an.detail from tblactionnote an where an.id=cs.actionNoteId ) actionNote,ss.comment,ss.done from tblsubservice ss inner join tblcomponentschedule cs on ss.componentScheduleId=cs.id inner join tblmodelcomponent mc on cs.componentModelId=mc.id inner join tblcomponent c on mc.componentId=c.id where ss.serviceHeaderId='".parent::getserviceHeaderId()."'";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsubservice');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>