<?php 
 require_once('../model/serviceType.php');
 class serviceTypeService extends serviceType{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblservicetype');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('serviceName',parent::getserviceName());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblservicetype');

 	 	 if(parent::getserviceName()!=""){
$builder->addColumnAndData('serviceName',parent::getserviceName()); }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblservicetype";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblservicetype');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>