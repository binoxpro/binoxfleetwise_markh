<?php 
 require_once('../model/labourPayment.php');
 class labourPaymentService extends labourPayment{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('labourpayment');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('labourId',parent::getlabourId());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('paymentDate',parent::getpaymentDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('labourpayment');

 	 	 if(!is_null(parent::getlabourId())){
$builder->addColumnAndData('labourId',parent::getlabourId()); 
}

 	 	 if(!is_null(parent::getamount())){
$builder->addColumnAndData('amount',parent::getamount()); 
}

 	 	 if(!is_null(parent::getpaymentDate())){
$builder->addColumnAndData('paymentDate',parent::getpaymentDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  labourpayment";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  labourpayment";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  labourpayment limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('labourpayment');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  labourpayment  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setlabourId($row['labourId']);
	 	parent::setamount($row['amount']);
	 	parent::setpaymentDate($row['paymentDate']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>