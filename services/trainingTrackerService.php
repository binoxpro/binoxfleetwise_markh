<?php 
 require_once('../model/trainingTracker.php');
 class trainingTrackerService extends trainingTracker{
	 	 public function save(){
			 
			 if($this->getIdValue()!=-1)
			 {
				$training=new trainingTrackerService();
				$training->setisActive(false);
				$training->setstatus('Not In Use');
				$training->setid($this->getIdValue());
				parent::setparentId($training->getid());
			 }
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblcompliance');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('trainingId',parent::gettrainingId());
			if(!is_null(parent::getissueDate())){
			$builder->addColumnAndData('issueDate',parent::getissueDate()); 
			}
 		 	if(!is_null(parent::getexpiryDate())){
			$builder->addColumnAndData('expiryDate',parent::getexpiryDate()); 
			}
 		 	  
			$builder->addColumnAndData('driverId',parent::getdriverId()); 
				

 	 	 	if(!is_null(parent::getstatus())){
			$builder->addColumnAndData('status',parent::getstatus()); 
			}

 	 		 if(!is_null(parent::getparentId())){
			$builder->addColumnAndData('parentId',parent::getparentId()); 
			}
 		 	 $this->con->setQuery(Director::buildSql($builder));
			 
			
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 
 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblcompliance');

 	 	 if(!is_null(parent::gettrainingId())){
		$builder->addColumnAndData('trainingId',parent::gettrainingId()); 
		}

 	 	 if(!is_null(parent::getissueDate())){
		$builder->addColumnAndData('issueDate',parent::getissueDate()); 
		}

 	 	 if(!is_null(parent::getexpiryDate())){
		$builder->addColumnAndData('expiryDate',parent::getexpiryDate()); 
		}

 	 	 if(!is_null(parent::getisActive())){
		$builder->addColumnAndData('isActive',parent::getisActive()); 
		}

 	 	 if(!is_null(parent::getdriverId())){
		$builder->addColumnAndData('driverId',parent::getdriverId()); 
		}

 	 	 if(!is_null(parent::getstatus())){
		$builder->addColumnAndData('status',parent::getstatus()); 
		}

 	 	 if(!is_null(parent::getparentId())){
		$builder->addColumnAndData('parentId',parent::getparentId()); 
		}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select c.*,tc.course,concat(d.firstName,CONCAT('-',d.lastName)) dname from  tblcompliance c inner join tbltrainingcourse tc on c.trainingId=tc.id inner join tbldriver d on c.driverId=d.id order by d.id desc";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblcompliance');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
	 
	 public function getIdValue(){
		 $id=-1;
	 	 $sql="select * from  tblcompliance where trainingId='".parent::gettrainingId()."' and driverId='".parent::getdriverId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			$id=$row['id']; 
		 }
		 return $id;
 	 }
 }
?>