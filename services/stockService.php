<?php 
 require_once('../model/stock.php');
 class stockService extends stock{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblstock');
	 	 $builder->addColumnAndData('id',parent::getid());
		 $builder->addColumnAndData('brand',parent::getbrand());
		 $builder->addColumnAndData('serialNumber',parent::getserialNumber());
		 $builder->addColumnAndData('cost',parent::getcost());
		 $builder->addColumnAndData('status',parent::getstatus());
		 $builder->addColumnAndData('retreadstatus',parent::getretreadstatus());
		 $builder->addColumnAndData('creationDate',parent::getcreationDate());
		 $builder->addColumnAndData('treadDepth',parent::gettreadDepth()); 
			 
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
	  
	   public function saveNew(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblstock');
	 	 //$builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('brand',parent::getbrand());
 		 	 $builder->addColumnAndData('serialNumber',parent::getserialNumber());
 		 	 $builder->addColumnAndData('cost',parent::getcost());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('retreadstatus',parent::getretreadstatus());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
			 $builder->addColumnAndData('treadDepth',parent::gettreadDepth()); 
			 
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	  	$this->con->execute_query2($builder->getValues());
			return $this->con->getId();
			
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblstock');

 	 	 if(!is_null(parent::getbrand())){
$builder->addColumnAndData('brand',parent::getbrand()); 
}

 	 	 if(!is_null(parent::getserialNumber())){
$builder->addColumnAndData('serialNumber',parent::getserialNumber()); 
}

 	 	 if(!is_null(parent::getcost())){
$builder->addColumnAndData('cost',parent::getcost()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getretreadstatus())){
$builder->addColumnAndData('retreadstatus',parent::getretreadstatus()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}
		if(!is_null(parent::gettreadDepth())){
		$builder->addColumnAndData('treadDepth',parent::gettreadDepth()); 
		}
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblstock";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row) {
			 $row['retreadstatus']=$row['retreadstatus']==0?'Not':'Yes';
			 array_push($data,$row);
		 }
		 return $data;
 	 }
	 
	  public function viewStock2($id){
	 	 $sql="select * from  tblstock where status='$id'";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row) {
			 $row['retreadstatus']=$row['retreadstatus']==0?'Not':'Yes';
			 array_push($data,$row);
		 }
		 return $data;
 	 }
	 public function viewInStock(){
	 	 $sql="select * from  tblstock where status='instock'";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row) {
			 $row['retreadstatus']=$row['retreadstatus']==0?'Not':'Yes';
			 array_push($data,$row);
		 }
		 return $data;
 	 }
	 
	  public function viewUsed(){
	 	 $sql="select sk.*,tt.tyretypeName position,v.regNo,(od.reading-ty.Odometer) kmDone,case when(ty.removalMileage-od.reading)<0 then 0 else (ty.removalMileage-od.reading) end remaining,ty.removalDate,format(round(((od.reading-ty.Odometer)*(ty.cost/ty.expectedKm)),2),2) cpkTotal,round((ty.cost/ty.expectedKm),2) cpk   from  tblstock sk inner join tbl_tyre ty on ty.stockId=sk.id inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row) {
			 $row['retreadstatus']=$row['retreadstatus']==0?'Not':'Yes';
			 array_push($data,$row);
		 }
		 return $data;
 	 }
	 
	 public function updateTyreId($stockId,$id)
	 {
		 	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_tyre");
            $builder->addColumnAndData("stockId", $stockId);
			//$builder->addColumnAndData("isActive", false);
			$builder->setCriteria("Where Id='$id'");
            $this->con->setQuery(Director::buildSql($builder));
			$this->con->execute_query();
	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblstock');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblstock where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setbrand($row["brand"]); 


 	 	 parent::setserialNumber($row["serialNumber"]); 


 	 	 parent::setcost($row["cost"]); 


 	 	 parent::setstatus($row["status"]); 


 	 	 parent::setretreadstatus($row["retreadstatus"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 
		 
		 parent::settreadDepth($row["treadDepth"]); 

	 	} 
 }
 
 public function getObjectExtensance(){
	 	 $sql="select * from  tblstock where serialNumber='".parent::getserialNumber()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

		parent::setid($row['id']);
 	 	 parent::setbrand($row["brand"]); 


 	 	 parent::setserialNumber($row["serialNumber"]); 


 	 	 parent::setcost($row["cost"]); 


 	 	 parent::setstatus($row["status"]); 


 	 	 parent::setretreadstatus($row["retreadstatus"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 
		 
		 parent::settreadDepth($row["treadDepth"]); 

	 	} 
 }
 
 public function existanceOf(){
	 	 $sql="select * from  tblstock where serialNumber='".parent::getserialNumber()."'";
	 	 $this->con->setSelect_query($sql);
		 return $this->con->sqlCount()>0?true:false;
 }
 
 		public function transferStock(){
	 	 $sql="select * from tbl_tyre where stockId is null and tyrePosition !=-1";
		 //$data=array();
		 foreach($this->con->getResultSet($sql) as $row) {
			 //$row['retreadstatus']=$row['retreadstatus']==0?'Not':'Yes';
			 //array_push($data,$row);
			 if($row['fixedAS']=='NEW')
			 {
				 $stock=new stockService();
				 $stock->setbrand($row['brand']);
				 $stock->setserialNumber($row['serialNumber']);
				 $stock->setcost($row['cost']);
				 $stock->setretreadstatus(false);
				 $stock->setstatus('inUse');
				 $stock->setcreationDate($row['fixingDate']);
				 $id=$stock->saveNew();
				 $tyreId=$row['Id'];
				 $stock->updateTyreId($id,$tyreId);
				 
				 
			 }else if($row['fixedAS']=='RETREAD' || $row['fixedAS']=='retread'){
				 $stock=new stockService();
				 $stock->setbrand($row['brand']);
				 $stock->setserialNumber($row['serialNumber']);
				 $stock->setcost($row['cost']);
				 $stock->setretreadstatus(true);
				 $stock->setstatus('inUse/Retread');
				 $stock->setcreationDate($row['fixingDate']);
				 $id=$stock->saveNew();
				 $tyreId=$row['Id'];
				 $stock->updateTyreId($id,$tyreId);
				 
			 }
		 }
		 return $data;
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>