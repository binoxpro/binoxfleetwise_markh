<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/otherCost.php';
class OtherCostService extends OtherCost {
  
    function __construct() {
        parent::__construct();       
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblothercost");
		if(parent::getcostName()!=NULL){
		$builder->addColumnAndData("costName", parent::getcostName());
		}
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tblothercost");
			if(parent::getcostName()!=NULL){
			$builder->addColumnAndData("costName", parent::getcostName());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
       $sql="select * from tblothercost";
	   $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["costName"]=$row["costName"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblothercost");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
	//please i was under tension to implement is function it not allow here i apologise Yiga James
	public function getDistanceMoved($x,$startDate,$endDate){
		$oddReading=0;
		$latestReading=0;
		$previousReading=0;
		$distanceReading=0;
		//$distance=0;
		$sql="SELECT * FROM tblodmeterreading where vehicleId='$x' and creationDate < '$startDate' order by creationDate asc";
			//$i=0;
			//$reading=0;
			foreach($this->view_query($sql) as $row)
			{
				$oddReading=$row['reading'];
				$previousReading=$oddReading;
			}
			
			$sqlLatest="SELECT * FROM tblodmeterreading where vehicleId='$x' and (creationDate between '$startDate' and '$endDate') order by creationDate asc";
			//$i=0;
			//$latestReading=0;
			foreach($this->view_query($sqlLatest) as $rowLatest)
			{
				$latestReading=($rowLatest['reading']);
				$distanceReading=$distanceReading+($latestReading-$previousReading);
				$previousReading=$latestReading;
			}
			
			
			return $distanceReading;
		
	}
}

	
