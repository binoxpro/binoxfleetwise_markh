<?php
include_once '../checklistId/checklistDaily.php';
class ChecklistDailyService extends ChecklistDaily
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblchecklistdaily");
		
		$builder->addColumnAndData("sectionitemId", parent::getsectionitemId());
		$builder->addColumnAndData("status", parent::getstatus());
		$builder->addColumnAndData("checklistId", parent::getchecklistId());
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblchecklistdaily");
			$builder->addColumnAndData("sectionitemId", parent::getsectionitemId());
            $builder->addColumnAndData("status", parent::getstatus());
            $builder->addColumnAndData("checklistId", parent::getchecklistId());
			$builder->addColumnAndData("vehicleId", parent::getvehicleId());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblchecklistdaily";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblchecklistdaily");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
