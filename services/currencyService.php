<?php 
 require_once('../model/currency.php');
 class currencyService extends currency{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblcurrency');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('symbol',parent::getsymbol());
 		 	 $builder->addColumnAndData('default1',parent::getdefault());
 		 	 $builder->addColumnAndData('rate',parent::getrate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblcurrency');

 	 	 if(!is_null(parent::getsymbol())){
$builder->addColumnAndData('symbol',parent::getsymbol()); 
}

 	 	 if(!is_null(parent::getdefault())){
$builder->addColumnAndData('default1',parent::getdefault()); 
}

 	 	 if(!is_null(parent::getrate())){
$builder->addColumnAndData('rate',parent::getrate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblcurrency";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblcurrency');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblcurrency where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setsymbol($row["symbol"]); 


 	 	 parent::setdefault($row["default1"]); 


 	 	 parent::setrate($row["rate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>