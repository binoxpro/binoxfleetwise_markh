<?php 
 require_once('../model/invoice.php');
require_once('invoiceItemService.php');
require_once('taxInvoiceService.php');
 class invoiceService extends invoice{
	 	 public function save(){
			 $builder=new InsertBuilder();
			 $builder->setTable('invoice');
			 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('confirmed',parent::getconfirmed());
 		 	 $builder->addColumnAndData('currencyId',parent::getcurrencyId());
 		 	 $builder->addColumnAndData('customerId',parent::getcustomerId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
		 	$sql="select * from invoice where id='".parent::getid()."'";
			 $this->con->setSelect_query($sql);
			 if($this->con->sqlCount()>0)
			 {
				 return $this->update();
			 }else {
				 return $this->con->execute_query2($builder->getValues());
			 }
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('invoice');

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getconfirmed())){
$builder->addColumnAndData('confirmed',parent::getconfirmed()); 
}

 	 	 if(!is_null(parent::getcurrencyId())){
$builder->addColumnAndData('currencyId',parent::getcurrencyId()); 
}

 	 	 if(!is_null(parent::getcustomerId())){
$builder->addColumnAndData('customerId',parent::getcustomerId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  invoice";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		 $total=0;
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select inv.*,d.companyName from  invoice inv inner join debitor d on inv.customerId=d.id";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select inv.*,d.companyName from  invoice inv inner join debitor d on inv.customerId=d.id limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		{
			if($row['confirmed']==1)
			{
				$row['type']='Tax Invoice';
				$taxInvoice=new taxInvoiceService();
				$taxInvoice->setparentId($row['id']);
				$taxInvoice->findWithParent();
				if(is_null($taxInvoice->getid()))
				{

				}else{
					$row['id'] = $taxInvoice->getid();

				}

			}
			else
			{
				$row['type']='Proforma Invoice';
			}
			$invoiceItem=new invoiceItemService();
			$invoiceItem->setinvoiceId($row['id']);
			$dataTotal=$invoiceItem->findTotalCost();
			$row['subTotal']=number_format($dataTotal[0],2);
			$row['vat']=number_format($dataTotal[1],2);
			$row['total']=number_format($dataTotal[2],2);

			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('invoice');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  invoice  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setconfirmed($row['confirmed']);
	 	parent::setcurrencyId($row['currencyId']);
	 	parent::setcustomerId($row['customerId']);
}	 	}


	 public function findObject()
	 {
		 $sql="select * from  invoice  where id='".parent::getid()."'";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			$data=$row;
		 }
		 return $data;
	 }

	 public function invoiceNumbering()
	 {
		 $invoiceStart=0;
		 $sql="select * from invoice ";
		 $this->con->setSelect_query($sql);
		 return intval($this->con->sqlCount())+1+$invoiceStart;


	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>