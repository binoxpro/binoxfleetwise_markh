<?php
include_once '../model/capacity.php';
class capacityService extends CapacityUtilisation
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblcapacity");
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		$builder->addColumnAndData("tripId", parent::gettripId());
		$builder->addColumnAndData("vpower", parent::getvpower());
		$builder->addColumnAndData("ago", parent::getago());
		$builder->addColumnAndData("bik", parent::getbik());
		$builder->addColumnAndData("ugl", parent::getugl());
		$builder->addColumnAndData("creationDate", parent::getcreationDate());
		$builder->addColumnAndData("timeLoading", parent::gettimeLoading());
		//$builder->addColumnAndData("capacty", parent::getcapacity());
		$this->con->setQuery(Director::buildSql($builder));
		
		return $this->con->execute_query2($builder->getValues());	
		   
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblcapacity");
			$builder->addColumnAndData("vehicleId", parent::getvehicleId());
			$builder->addColumnAndData("tripId", parent::gettripId());
			$builder->addColumnAndData("vpower", parent::getvpower());
			$builder->addColumnAndData("ago", parent::getago());
			$builder->addColumnAndData("bik", parent::getbik());
			$builder->addColumnAndData("ugl", parent::getugl());
			$builder->addColumnAndData("creationDate", parent::getcreationDate());
			$builder->addColumnAndData("timeLoading", parent::gettimeLoading());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id order by c.id DESC";
	   $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["regNo"]=$row["regNo"];
		   $data2["vpower"]=$row["vpower"];
		   $data2["ago"]=$row["ago"];
		   $data2["bik"]=$row["bik"];
		   $data2["ugl"]=$row["ugl"];
		   $data2["creationDate"]=$row["creationDate"];
		   $data2["timeLoading"]=$row["timeLoading"];
		   $data2["tripNo"]=$row["tripNo"];
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function vehicleUtilisation()
	{
		$year=date('Y');
		$data=array();
		$totalago=0;
		$totalugl=0;
		$totalbik=0;
		$totalvp=0;
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table class='table table-striped table-hovered table-bordered'>";
		echo"<tr><th>Date</th><th>Vehicle</th><th>VPOWER</th><th>ULG</th><th>AGO</th><th>BIK</th><th>%</th></tr>";
		for($i=1;$i<=intval(date('m'));$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			echo"<tr class='success text-center'><th colspan='7'>".$month[$i-1]."</th>";
			//sql for order date;
			$sql="select o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where (os.name='Dispatched' or os.name='Delivered' ) and o.promiseDate between '$startDate' and '$endDate' ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   
			   echo"<tr><td>".$row['promiseDate']."</td><td>".$row['regNo']."</td>";
			   if($row['name2']=="VPOWER"){
				   $vpower=$vpower+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="ULG"){
				   $ugl=$ugl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="AGO"){
				   $agl=$agl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="BIK"){
				   $bik=$bik+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td>".$percentage."%</td></tr>";  
			   }
			   $count=$count+1;
		   }
		   $count=$count>0?$count:1;
		   $totalago=$totalago+$agl;
		   $totalvp=$totalvp+$vpower;
		   $totalugl=$totalugl+$ugl;
		   $totalbik=$totalbik+$bik;
		   
			echo "<tr><th>Total</th><th>".number_format($vpower+$agl+$bik+$ugl,0)."</th><th>".number_format($vpower,0)."</th><th>".number_format($ugl,0)."</th><th>".number_format($agl,0)."</th><th>".number_format($bik,0)."</th><th>".number_format(($totalPecent/$count),0)."%</th></tr>";
		}
		echo "<tr style='background-color:blue; color:white;'><th>Overall Total</th><th>".number_format($totalvp+$totalago+$totalbik+$totalugl,0)."</th><th>".number_format($totalvp,0)."</th><th>".number_format($totalugl,0)."</th><th>".number_format($totalago,0)."</th><th>".number_format($totalbik,0)."</th><th>".number_format(($totalbik+$totalago+$totalugl+$totalvp),0)."</th></tr>";
		echo "</table>";
		
	}
	
	public function vehicleUtilisationId($vid)
	{
		$year=date('Y');
		$data=array();
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table class='table table-striped table-hovered table-bordered'>";
		echo"<tr><th>Date</th><th>Vehicle</th><th>VPOWER</th><th>ULG</th><th>AGO</th><th>BIK</th><th>%</th></tr>";
		for($i=1;$i<=intval(date('m'));$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			echo"<tr class='success text-center'><th colspan='7'>".$month[$i-1]."</th>";
			//sql for order date;
			$sql="select o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where alo.truckId='$vid' and (os.name='Dispatched' or os.name='Delivered') and o.promiseDate between '$startDate' and '$endDate'  ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   
			   echo"<tr><td>".$row['promiseDate']."</td><td>".$row['regNo']."</td>";
			   if($row['name2']=="VPOWER"){
				   $vpower=$vpower+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="ULG"){
				   $ugl=$ugl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="AGO"){
				   $agl=$agl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="BIK"){
				   $bik=$bik+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td>".$percentage."%</td></tr>";  
			   }
			   $count=$count+1;
		   }
		   $count=$count>0?$count:1;
		   $totalago=$totalago+$agl;
		   $totalvp=$totalvp+$vpower;
		   $totalugl=$totalugl+$ugl;
		   $totalbik=$totalbik+$bik;
		   
			echo "<tr><th>Total</th><th>".number_format($vpower+$agl+$bik+$ugl,0)."</th><th>".number_format($vpower,0)."</th><th>".number_format($ugl,0)."</th><th>".number_format($agl,0)."</th><th>".number_format($bik,0)."</th><th>".number_format(($totalPecent/$count),0)."%</th></tr>";
		}
		echo "<tr><th>Total</th><th>".number_format($totalvp+$totalago+$totalbik+$totalugl,0)."</th><th>".number_format($totalvp,0)."</th><th>".number_format($totalugl,0)."</th><th>".number_format($totalago,0)."</th><th>".number_format($totalbik,0)."</th><th>".number_format(($totalbik+$totalago+$totalugl+$totalvp),0)."%</th></tr>";
		echo "</table>";
		
	}
	
		public function vehicleUtilisationIdRange($vid,$startDate,$endDate)
		{
		$year=date('Y');
		$data=array();
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table class='table table-striped table-hovered table-bordered'>";
		$sdBreak=explode("-",$startDate);
		$edBreak=explode("-",$endDate);
		$startPoint=intval($sdBreak[1]);
		$endPoint=intval($edBreak[1]);
		$i=$startPoint;
		echo"<tr><th>Date</th><th>Vehicle</th><th>VPOWER</th><th>ULG</th><th>AGO</th><th>BIK</th><th>%</th></tr>";
		for($i;$i<=$endPoint;$i++)
		{
			$m=$i<=9?("0".$i):$i;
			if($i==$startPoint){
				$startDate1=$startDate;
				$endDate1=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);	
			}else if($i==$endPoint)
			{
				$startDate1=$year."-".$m."-01";
				$endDate1=$endDate;
			}else{
				$startDate1=$year."-".$m."-01";
				$endDate1=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			}
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			echo"<tr class='success text-center'><th colspan='7'>".$month[$i-1]."</th>";
			//sql for order date;
			$sql="select o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where alo.truckId='$vid' and (os.name='Dispatched' or os.name='Delivered') and o.promiseDate between '$startDate1' and '$endDate1'  ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   
			   echo"<tr><td>".$row['promiseDate']."</td><td>".$row['regNo']."</td>";
			   if($row['name2']=="VPOWER"){
				   $vpower=$vpower+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="ULG"){
				   $ugl=$ugl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="AGO"){
				   $agl=$agl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="BIK"){
				   $bik=$bik+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td>".$percentage."%</td></tr>";  
			   }
			   $count=$count+1;
		   }
		    $count=$count>0?$count:1;
			echo "<tr><th>Total</th><th></th><th>".number_format($vpower,0)."</th><th>".number_format($ugl,0)."</th><th>".number_format($agl,0)."</th><th>".number_format($bik,0)."</th><th>".number_format(($totalPecent/$count),0)."%</th></tr>";
		}
			echo "</table>";
		
		}
	
	
	public function vehicleUtilisationRange($startDate,$endDate)
		{
		$year=date('Y');
		$data=array();
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table class='table table-striped table-hovered table-bordered'>";
		$sdBreak=explode("-",$startDate);
		$edBreak=explode("-",$endDate);
		$startPoint=intval($sdBreak[1]);
		$endPoint=intval($edBreak[1]);
		$i=$startPoint;
		echo"<tr><th>Date</th><th>Vehicle</th><th>VPOWER</th><th>ULG</th><th>AGO</th><th>BIK</th><th>%</th></tr>";
		for($i;$i<=$endPoint;$i++)
		{
			$m=$i<=9?("0".$i):$i;
			if($i==$startPoint){
				$startDate1=$startDate;
				$endDate1=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);	
			}else if($i==$endPoint)
			{
				$startDate1=$year."-".$m."-01";
				$endDate1=$endDate;
			}else{
				$startDate1=$year."-".$m."-01";
				$endDate1=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			}
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			echo"<tr class='success text-center'><th colspan='7'>".$month[$i-1]."</th>";
			//sql for order date;
			$sql="select o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where (os.name='Dispatched' or os.name='Delivered') and o.promiseDate between '$startDate1' and '$endDate1'  ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   
			   echo"<tr><td>".$row['promiseDate']."</td><td>".$row['regNo']."</td>";
			   if($row['name2']=="VPOWER"){
				   $vpower=$vpower+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="ULG"){
				   $ugl=$ugl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="AGO"){
				   $agl=$agl+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td>".$percentage."%</td></tr>";  
			   }else if($row['name2']=="BIK"){
				   $bik=$bik+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td>".$percentage."%</td></tr>";  
			   }
			   $count=$count+1;
		   }
		    $count=$count>0?$count:1;
			echo "<tr><th>Total</th><th></th><th>".number_format($vpower,0)."</th><th>".number_format($ugl,0)."</th><th>".number_format($agl,0)."</th><th>".number_format($bik,0)."</th><th>".number_format(($totalPecent/$count),0)."%</th></tr>";
		}
			echo "</table>";
		
		}
	
	
	public function vehicleGraphy()
	{
		$year=date('Y');
		$data=array();
		$data5;
		//$data4;
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		
		for($i=1;$i<=12;$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			//sql for order date;
			$sql="select o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where   (os.name='Dispatched' or os.name='Delivered') and  o.promiseDate between '$startDate' and '$endDate' ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
			   $count=$count+1;
		   }
		   $data4=number_format((($totalPecent>0&&$count>0?$totalPecent/$count:0)),0);
			if($i==1){
				$data5="0/".$data4."/";
			}else if($i==12){
				$data5.=$data4."/0";
			}else{
				$data5.=$data4."/";
			}
			
		}
		
		return $data5;
	}
	
	
	public function vehicleGraphyId($id)
	{
		$year=date('Y');
		$data=array();
		$data5;
		//$data4;
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		
		for($i=1;$i<=12;$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			//sql for order date;
			$sql="select o.orderDate,o.qtyOrder,g.name2,v.regNo,v.capacty from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id where alo.truckId='$id' and (os.name='Dispatched' or os.name='Delivered') and  o.orderDate between '$startDate' and '$endDate' ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
			   $count=$count+1;
		   }
		   $data4=number_format((($totalPecent>0&&$count>0?$totalPecent/$count:0)),0);
			if($i==1){
				$data5="0/".$data4."/";
			}else if($i==12){
				$data5.=$data4."/0";
			}else{
				$data5.=$data4."/";
			}
			
		}
		
		return $data5;
	}
	
	
	public function viewUtilisation() {
		
		$year=date('Y');
		$data=array();
		$month=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEPT','OCT','NOV','DEC');
		for($i=1;$i<=intval(date('m'));$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading,v.Capacty capacity  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id where c.creationDate between '$startDate' and '$endDate'  order by c.id DESC";
	   		$data3=array();
			   $data3["id"]="";
			   $data3["creationDate"]="FOR";
			   $data3["tripNo"]=$month[$i-1];
			   $data3["regNo"]=$startDate;
			   $data3["vpower"]=$endDate;
			   $data3["ago"]="";
			   $data3["bik"]="";
			   $data3["ugl"]="";
			   $data3["timeLoading"]="";
			   $data3["percent"]="";
			   $data3["fillColor"]="#51A8FF";
			   array_push($data,$data3);
		   foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $loadedCapacity=intval($row["ago"])+intval($row["bik"])+intval($row["ugl"])+intval($row["vpower"]);
			   $vehicleCapacity=intval($row["capacity"]);
			   $percent=intval(($loadedCapacity/$vehicleCapacity)*100);
			   $data2=array();
			   $data2["id"]=$row["id"];
			   $data2["regNo"]=$row["regNo"];
			   $data2["vpower"]=intval($row["vpower"]);
			   $data2["ago"]=intval($row["ago"]);
			   $data2["bik"]=intval($row["bik"]);
			   $data2["ugl"]=intval($row["ugl"]);
			   $data2["creationDate"]=$row["creationDate"];
			   $data2["timeLoading"]=$row["timeLoading"];
			   $data2["tripNo"]=$row["tripNo"];
			   $data2["percent"]=$percent."%";
			   $data2["fillColor"]="";
			   array_push($data,$data2);
		   }
		   	$data4=array();
			$data4["id"]="";
			$data4["regNo"]="";
			$data4["vpower"]="";
			$data4["ago"]="";
			$data4["bik"]="";
			$data4["ugl"]="";
			$data4["creationDate"]="";
			$data4["timeLoading"]="";
			$data4["tripNo"]="";
			$data4["percent"]="";
			$data4["fillColor"]="#FFF";
			array_push($data,$data4);
		}
	   return $data;
    }
	
	public function viewUtilisation2($startDate,$endDate) {
		
		//$year=date('Y');
		$data=array();
		$month=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEPT','OCT','NOV','DEC');
		//for($i=1;$i<=intval(date('m'));$i++)
		//{
			//$m=$i<=9?("0".$i):$i;
			//$startDate=$year."-".$m."-01";
			//$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading,v.Capacty capacity  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id where c.creationDate between '$startDate' and '$endDate'  order by c.id DESC";
	   		$data3=array();
			   $data3["id"]="";
			   $data3["creationDate"]="";
			   $data3["tripNo"]="";
			   $data3["regNo"]=$startDate;
			   $data3["vpower"]=$endDate;
			   $data3["ago"]="";
			   $data3["bik"]="";
			   $data3["ugl"]="";
			   $data3["timeLoading"]="";
			   $data3["percent"]="";
			   $data3["fillColor"]="#51A8FF";
			   array_push($data,$data3);
		   foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $loadedCapacity=intval($row["ago"])+intval($row["bik"])+intval($row["ugl"])+intval($row["vpower"]);
			   $vehicleCapacity=intval($row["capacity"]);
			   $percent=intval(($loadedCapacity/$vehicleCapacity)*100);
			   $data2=array();
			   $data2["id"]=$row["id"];
			   $data2["regNo"]=$row["regNo"];
			   $data2["vpower"]=intval($row["vpower"]);
			   $data2["ago"]=intval($row["ago"]);
			   $data2["bik"]=intval($row["bik"]);
			   $data2["ugl"]=intval($row["ugl"]);
			   $data2["creationDate"]=$row["creationDate"];
			   $data2["timeLoading"]=$row["timeLoading"];
			   $data2["tripNo"]=$row["tripNo"];
			   $data2["percent"]=$percent."%";
			   $data2["fillColor"]="";
			   array_push($data,$data2);
		   }
		   	$data4=array();
			$data4["id"]="";
			$data4["regNo"]="";
			$data4["vpower"]="";
			$data4["ago"]="";
			$data4["bik"]="";
			$data4["ugl"]="";
			$data4["creationDate"]="";
			$data4["timeLoading"]="";
			$data4["tripNo"]="";
			$data4["percent"]="";
			$data4["fillColor"]="#FFF";
			array_push($data,$data4);
		//}
	   return $data;
    }

	public function viewUtilisation3($id) {
		
		$year=date('Y');
		$data=array();
		$month=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEPT','OCT','NOV','DEC');
		for($i=1;$i<=intval(date('m'));$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading,v.Capacty capacity  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id where c.vehicleId=$id and c.creationDate between '$startDate' and '$endDate'  order by c.vehicleId  DESC";
	   		$data3=array();
			   $data3["id"]="";
			   $data3["creationDate"]="FOR";
			   $data3["tripNo"]=$month[$i-1];
			   $data3["regNo"]=$startDate;
			   $data3["vpower"]=$endDate;
			   $data3["ago"]="";
			   $data3["bik"]="";
			   $data3["ugl"]="";
			   $data3["timeLoading"]="";
			   $data3["percent"]="";
			   $data3["fillColor"]="#51A8FF";
			   array_push($data,$data3);
			   $x=0;
			   $percentTotal=0;
		   foreach( $this->con->getResultSet($sql) as $row)
		   {
			   
			   $loadedCapacity=intval($row["ago"])+intval($row["bik"])+intval($row["ugl"])+intval($row["vpower"]);
			   $vehicleCapacity=intval($row["capacity"]);
			   $percent=intval(($loadedCapacity/$vehicleCapacity)*100);
			   $data2=array();
			   $data2["id"]=$row["id"];
			   $data2["regNo"]=$row["regNo"];
			   $data2["vpower"]=intval($row["vpower"]);
			   $data2["ago"]=intval($row["ago"]);
			   $data2["bik"]=intval($row["bik"]);
			   $data2["ugl"]=intval($row["ugl"]);
			   $data2["creationDate"]=$row["creationDate"];
			   $data2["timeLoading"]=$row["timeLoading"];
			   $data2["tripNo"]=$row["tripNo"];
			   $data2["percent"]=$percent."%";
			   $data2["fillColor"]="";
			   array_push($data,$data2);
			   $percentTotal=$percentTotal+$percent;
			   $x=$x+1;
		   }
		   	$data4=array();
			$data4["id"]="";
			$data4["tripNo"]="";
			$data4["creationDate"]="";
			$data4["regNo"]="";
			$data4["vpower"]="";
			$data4["ago"]="";
			$data4["bik"]="";
			$data4["ugl"]="Total";
			$data4["timeLoading"]="Total";
			
			$data4["percent"]=intval(($percentTotal>0&&$x>0?$percentTotal/$x:0))."%";
			$data4["fillColor"]="#C4E1FF";
			array_push($data,$data4);
		}
	   return $data;
    }
	
	public function viewUtilisation5($id) {
		
		$year=date('Y');
		$data=array();
		$data4;
		$data5;
		$month=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEPT','OCT','NOV','DEC');
		for($i=1;$i<=12;$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading,v.Capacty capacity  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id where c.vehicleId=$id and c.creationDate between '$startDate' and '$endDate'  order by c.vehicleId  DESC";
	   		//$data3=array();
//			   //$data3["id"]="";
//			   //$data3["creationDate"]="FOR";
//			   $data3["tripNo"]=$month[$i-1];
//			   $data3["regNo"]=$startDate;
//			   $data3["vpower"]=$endDate;
//			   $data3["ago"]="";
//			   $data3["bik"]="";
//			   $data3["ugl"]="";
//			   $data3["timeLoading"]="";
//			   $data3["percent"]="";
//			   $data3["fillColor"]="#51A8FF";
//			   array_push($data,$data3);
			   $x=0;
			   $percentTotal=0;
		   foreach( $this->con->getResultSet($sql) as $row)
		   {
			   
			   $loadedCapacity=intval($row["ago"])+intval($row["bik"])+intval($row["ugl"])+intval($row["vpower"]);
			   $vehicleCapacity=intval($row["capacity"]);
			   $percent=intval(($loadedCapacity/$vehicleCapacity)*100);
			  // $data2=array();
			   //$data2["id"]=$row["id"];
//			   $data2["regNo"]=$row["regNo"];
//			   $data2["vpower"]=intval($row["vpower"]);
//			   $data2["ago"]=intval($row["ago"]);
//			   $data2["bik"]=intval($row["bik"]);
//			   $data2["ugl"]=intval($row["ugl"]);
//			   $data2["creationDate"]=$row["creationDate"];
//			   $data2["timeLoading"]=$row["timeLoading"];
//			   $data2["tripNo"]=$row["tripNo"];
			   //$data2["percent"]=$percent;
			   //$data2["fillColor"]="";
			   //array_push($data,$data2);
			   $percentTotal=$percentTotal+$percent;
			   $x=$x+1;
		   }
		   	
			//$data4["id"]="";
//			$data4["tripNo"]="";
//			$data4["creationDate"]="";
//			$data4["regNo"]="";
//			$data4["vpower"]="";
//			$data4["ago"]="";
//			$data4["bik"]="";
//			$data4["ugl"]="Total";
//			$data4["timeLoading"]="Total";
			
			$data4=intval(($percentTotal>0&&$x>0?$percentTotal/$x:0));
			if($i==1){
				$data5="0/".$data4."/";
			}else if($i==12){
				$data5.=$data4."/0";
			}else{
				$data5.=$data4."/";
			}
			//$data4["fillColor"]="#C4E1FF";
			//array_push($data,$data4);
		}
	   return $data5;
    }
	
	
	public function viewSearch() {
       $sql="select c.id,t.tripNo,v.regNo,c.vpower,c.ago,c.bik,c.ugl,c.creationDate,c.timeLoading  from tblcapacity c inner join tblvehicle v on c.vehicleId=v.id inner join tbltrip t on c.tripId=t.id where v.regNo like '%".parent::getvehicleId()."' order by c.id DESC";
	   $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["regNo"]=$row["regNo"];
		   $data2["vpower"]=$row["vpower"];
		   $data2["ago"]=$row["ago"];
		   $data2["bik"]=$row["bik"];
		   $data2["ugl"]=$row["ugl"];
		   $data2["creationDate"]=$row["creationDate"];
		   $data2["timeLoading"]=$row["timeLoading"];
		   $data2["tripNo"]=$row["tripNo"];
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function vehicleTimeUtilisation()
	{
		$year=date('Y');
		
		$totalOnTime=0;
		$totalOffTime=0;
		$totalOnTime2=0;
		$totalF1=0;
		$data=array();
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table class='table table-striped table-hovered table-bordered'>";
		echo"<tr><th rowspan='2'>Load Date</th><th rowspan='2'>Delivery Date</th><th rowspan='2'>Customer</th><th rowspan='2'>Vehicle</th><th colspan='4'>Products Quantities</th><th>24HRS(Delivery)</th><th>48HRS(Delivery)</th><th>Delay</th></tr>";
		echo "<tr><th>VPOWER</th><th>ULG</th><th>AGO</th><th>BIK</th><th>On Time <i class='fa fa-check-circle'></i></th><th>On Time<i class='fa fa-check-circle'></i></th><th></th></tr>";
		for($i=1;$i<=intval(date('m'));$i++)
		{
			$m=$i<=9?("0".$i):$i;
			$startDate=$year."-".$m."-01";
			$endDate=$year."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$rowspan=cal_days_in_month(CAL_GREGORIAN,$i,$year);
			$totalPecent=0;
			$count=0;
			$vpower=0;
			$bik=0;
			$agl=0;
			$ugl=0;
			$onTime=0;
			$offTime=0;
			$onTime2=0;
			$offTime2=0;
			echo"<tr class='success text-center'><th colspan='11'>".$month[$i-1]."</th>";
			//sql for order date;
			$sql="select o.mailingName,o.promiseDate,o.qtyOrder,g.name2,v.regNo,v.capacty,dil.Deliverydate  from tblorder o inner join tblallocation alo on o.id=alo.orderId inner join tblgood g on o.itemcodeId=g.id inner join tblvehicle v on alo.truckId=v.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tbldelivery dil on dil.orderId=alo.id  where ( os.name='Delivered' ) and o.promiseDate between '$startDate' and '$endDate' ";
			foreach( $this->con->getResultSet($sql) as $row)
		   {
			   $capacity=intval($row['capacty']);
			   $loadCapcity=intval($row['qtyOrder']);
			   //$orderDate2=(strtotime($row['promiseDate'])/(60*60));
			   $promiseDate=(strtotime($row['promiseDate'])/(60*60));
			   //$timeCat=$promiseDate-$orderDate2;
			   
			   $deliveryDate=(strtotime($row['Deliverydate'])/(60*60));
			   $timeDate=$deliveryDate-$promiseDate;
			  //$timeDelivery=$deliveryDate<=$promiseDate?true:false;
			   echo"<tr><td>".$row['promiseDate']."</td><td>".$row['Deliverydate']."</td><td>".$row['mailingName']."</td><td>".$row['regNo']."</td>";
			   if($row['name2']=="VPOWER"){
				  $vpower=$vpower+$loadCapcity;
				  // $percentage=number_format(($loadCapcity/$capacity)*100,0);
				 //  $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				   
				 echo "<td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td><td></td>";  
			   }else if($row['name2']=="ULG"){
				   $ugl=$ugl+$loadCapcity;
				   //$percentage=number_format(($loadCapcity/$capacity)*100,0);
				   //$totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td><td></td>";  
			   }else if($row['name2']=="AGO"){
				   $agl=$agl+$loadCapcity;
				   //$percentage=number_format(($loadCapcity/$capacity)*100,0);
				   //$totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td><td></td>";  
			   }else if($row['name2']=="BIK"){
				   $bik=$bik+$loadCapcity;
				   $percentage=number_format(($loadCapcity/$capacity)*100,0);
				   $totalPecent=$totalPecent+(($loadCapcity/$capacity)*100);
				 echo "<td></td><td></td><td></td><td>".number_format($row['qtyOrder'],0)."</td>";  
			   }
			 //  $cv=$timeCat==0?1:$timeCat;
			   if($timeDate<=24){
				
					$onTime=$onTime+1;
			  		echo"<td><i class='fa fa-check'></i></td><td></td><td></td></tr>";
				
			   }else if($timeDate<=48){
					$onTime2=$onTime2+1;
			  		echo"<td></td><td><i class='fa fa-check'></i></td><td></td></tr>";
			   }else{
				   $offTime=$offTime+1;
			  		echo"<td></td><td></td><td><i class='fa fa-check'></i></td></tr>";
			   }
			   
			   $count=$count+1;
		   }
		   $count=$count>0?$count:1;
		   $totalOnTime=$totalOnTime+$onTime;
		   $totalOnTime2=$totalOnTime2+$onTime2;
		   $totalOffTime=$totalOffTime+$offTime;
		   $totalF=0;
		   $totalF=$onTime+$onTime2+$offTime;
		   $totalF1=$totalF1+$totalF;
			echo "<tr><th>Total</th><th></th><th></th><th></th><th>".number_format($vpower,0)."</th><th>".number_format($ugl,0)."</th><th>".number_format($agl,0)."</th><th>".number_format($bik,0)."</th><th>".$onTime."/".$totalF."</th><th>".$onTime2."/".$totalF."</th><th>".$offTime."/".$totalF."</th></tr>";
		}
		echo "<tr style='background-color:blue;color:white;'><th>Over all Total</th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>".$totalOnTime."/".$totalF1."</th><th>".$totalOnTime2."/".$totalF1."</th><th>".$totalOffTime."/".$totalF1."</th></tr>";
		echo "</table>";
		
	}
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblcapacity");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
