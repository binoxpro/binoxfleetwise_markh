<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/notification.php';
class NotificationService extends Notification {
  
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblnotification");
		if(parent::getsection()!=NULL){
		$builder->addColumnAndData("section", parent::getsection());
		}
		if(parent::getto()!=NULL){
		$builder->addColumnAndData("to", parent::getto());
		}
		if(parent::getcc()!=NULL){
		$builder->addColumnAndData("cc",parent::getcc());
		}
		if(parent::getmsg()!=NULL){
		$builder->addColumnAndData("msg",parent::getmsg());
		}
		if(parent::getheaderNotif()!=NULL){
		$builder->addColumnAndData("headerNotif",parent::getheaderNotif());
		}
		if(parent::getsent()!=NULL){
		$builder->addColumnAndData("sent",parent::getsent());
		}
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tblnotification");
			if(parent::getsection()!=NULL){
			$builder->addColumnAndData("section", parent::getsection());
			}
			if(parent::getto()!=NULL){
			$builder->addColumnAndData("to", parent::getto());
			}
			if(parent::getcc()!=NULL){
			$builder->addColumnAndData("cc",parent::getcc());
			}
			if(parent::getmsg()!=NULL){
			$builder->addColumnAndData("msg",parent::getmsg());
			}
			if(parent::getheaderNotif()!=NULL){
			$builder->addColumnAndData("headerNotif",parent::getheaderNotif());
			}
			if(parent::getsent()!=NULL){
			$builder->addColumnAndData("sent",parent::getsent());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
       $sql="select * from tblnotification";
	   $data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["section"]=$row["section"];
		   $data2["to"]=$row["to"];
		   $data2["cc"]=$row["cc"];
		   $data2["msg"]=$row["msg"];
		   $data2["headerNotif"]=$row["headerNotif"];
		   $data2["sent"]=$row["sent"];
		   array_push($data,$data2);
	   }
	   return $data;
    }
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblnotification");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
}

	
