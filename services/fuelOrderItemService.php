<?php 
 require_once('../model/fuelOrderItem.php');
 class fuelOrderItemService extends fuelOrderItem{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('fuelorderitem');
	 	 //$builder->addColumnAndData('id',parent::getid());
	 	 $builder->addColumnAndData('fuelOrderId',parent::getfuelOrderId());
	 	 $builder->addColumnAndData('fuelProductId',parent::getfuelProductId());
	 	 $builder->addColumnAndData('litres',parent::getlitres());
	 	 $builder->addColumnAndData('rate',parent::getrate());
	 	 $builder->addColumnAndData('isActive',parent::getisActive());
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('fuelorderitem');

 	 	 if(!is_null(parent::getfuelOrderId()))
 	 	 {
			$builder->addColumnAndData('fuelOrderId',parent::getfuelOrderId()); 
		 }

 	 	 if(!is_null(parent::getfuelProductId()))
 	 	 {
			$builder->addColumnAndData('fuelProductId',parent::getfuelProductId()); 
		 }

 	 	 if(!is_null(parent::getlitres())){
$builder->addColumnAndData('litres',parent::getlitres()); 
}

 	 	 if(!is_null(parent::getrate())){
$builder->addColumnAndData('rate',parent::getrate()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  fuelorderitem";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select foi.*,fp.productName,(foi.litres*foi.rate) total from  fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where  fuelOrderId='".parent::getfuelOrderId()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select foi.*,fp.productName,(foi.litres*foi.rate) total from  fuelorderitem foi inner join fuelproduct fp on foi.fuelProductId=fp.id where  fuelOrderId='".parent::getfuelOrderId()."'  limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('fuelorderitem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  fuelorderitem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
			parent::setid($row['id']);
			parent::setfuelOrderId($row['fuelOrderId']);
			parent::setfuelProductId($row['fuelProductId']);
			parent::setlitres($row['litres']);
			parent::setrate($row['rate']);
			parent::setisActive($row['isActive']);
		}
	 }


	 public function findTotal()
	 {

		 $sql="select sum(litres*rate) total from  fuelorderitem  where fuelOrderId='".parent::getfuelOrderId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $total=$row['total'];
		 }
		 //parent::setid($row['id']);


		 return $total;
	 }

	 public function findTripFuelDetail($tripNo)
	 {
		 $x=0;
		 $totalLitres=0;
		 $rate=0;
		 $count=0;
		 $argRate=0;
		 $totalAmount=0;
		 $sql="select foi.* from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id  where fo.tripNo='$tripNo'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $totalLitres=$totalLitres+$row['litres'];
			 $rate=$rate+$row['rate'];
			 $totalAmount=$totalAmount+($row['litres']*$row['rate']);
			 $count=$count+1;
		 }
		if($totalLitres>0)
		{
			parent::setlitres($totalLitres);
		}else{
			parent::setlitres('-');
		}

		 if($count>0)
		 {
			 parent::setrate(round($rate / $count));
		 }else
		 {
			 parent::setrate('-');
		 }
		 if($totalAmount>0)
		 {
			 parent::setisActive($totalAmount);
		 }else{
			 parent::setisActive('-');
		 }



		 //return $x;
	 }

	 public function findTripFuelDetailSummary($tripNo,$startDate,$endDate)
	 {
		 $x=0;
		 $totalLitres=0;
		 $rate=0;
		 $count=0;
		 $argRate=0;
		 $totalAmount=0;
		 $sql="select foi.* from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id  where fo.tripNo='$tripNo'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $totalLitres=$totalLitres+$row['litres'];
			 $rate=$rate+$row['rate'];
			 $totalAmount=$totalAmount+($row['litres']*$row['rate']);
			 $count=$count+1;
		 }
		 if($totalLitres>0)
		 {
			 parent::setlitres($totalLitres);
		 }else{
			 parent::setlitres('-');
		 }

		 if($count>0)
		 {
			 parent::setrate(round($rate / $count));
		 }else
		 {
			 parent::setrate('-');
		 }
		 if($totalAmount>0)
		 {
			 parent::setisActive($totalAmount);
		 }else{
			 parent::setisActive('-');
		 }



		 //return $x;
	 }

	 public function findTripFuelDetailSummaryFuel($startDate,$endDate)
	 {

		 $sql="select sum(foi.litres)litres,sum(foi.litres*foi.rate)Amount  from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id inner join fuelproduct fp on foi.fuelProductId=fp.id where fp.productName!='Oil' AND fo.creationDate BETWEEN '$startDate' and '$endDate'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['litres']>0)
			 {
				 parent::setlitres($row['litres']);
			 }else{
				 parent::setlitres('-');
			 }

			 if($row['Amount']>0)
			 {
				 parent::setrate($row['Amount']);
			 }else
			 {
				 parent::setrate('-');
			 }
		 }





		 //return $x;
	 }

	 public function findTruckFuelDetailSummaryFuel($startDate,$endDate,$vehicleId)
	 {

		 $sql="select sum(foi.litres)litres,sum(foi.litres*foi.rate)Amount  from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id inner join fuelproduct fp on foi.fuelProductId=fp.id inner join tripnumbergenerator tng on fo.tripNo=tng.TripNumber  where (fp.productName!='Oil' AND tng.vehicleId='$vehicleId') AND fo.creationDate BETWEEN '$startDate' and '$endDate'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['litres']>0)
			 {
				 parent::setlitres($row['litres']);
			 }else{
				 parent::setlitres('-');
			 }

			 if($row['Amount']>0)
			 {
				 parent::setrate($row['Amount']);
			 }else
			 {
				 parent::setrate('-');
			 }
		 }





		 //return $x;
	 }


	 public function findTripFuelDetailSummaryOil($startDate,$endDate)
	 {

		 $sql="select sum(foi.litres)litres,sum(foi.litres*foi.rate)Amount  from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id inner join fuelproduct fp on foi.fuelProductId=fp.id where fp.productName ='Oil' AND fo.creationDate BETWEEN '$startDate' and '$endDate'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['litres']>0)
			 {
				 parent::setlitres($row['litres']);
			 }else{
				 parent::setlitres('-');
			 }

			 if($row['Amount']>0)
			 {
				 parent::setrate($row['Amount']);
			 }else
			 {
				 parent::setrate('-');
			 }
		 }





		 //return $x;
	 }

	 public function findTruckFuelDetailSummaryOil($startDate,$endDate,$vehicleId)
	 {

		 $sql="select sum(foi.litres)litres,sum(foi.litres*foi.rate)Amount  from  fuelorderitem foi inner join fuelorder fo on foi.fuelOrderId=fo.id inner join fuelproduct fp on foi.fuelProductId=fp.id inner join tripnumbergenerator tng on fo.tripNo=tng.TripNumber where (fp.productName ='Oil' AND tng.vehicleId='$vehicleId') AND fo.creationDate BETWEEN '$startDate' and '$endDate'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($row['litres']>0)
			 {
				 parent::setlitres($row['litres']);
			 }else{
				 parent::setlitres('-');
			 }

			 if($row['Amount']>0)
			 {
				 parent::setrate($row['Amount']);
			 }else
			 {
				 parent::setrate('-');
			 }
		 }





		 //return $x;
	 }



	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>