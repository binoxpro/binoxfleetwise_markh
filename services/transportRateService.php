<?php 
 require_once('../model/transportRate.php');
 class transportRateService extends transportRate{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('transportrate');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('rate',parent::getrate());
 		 	 $builder->addColumnAndData('lowerLimit',parent::getlowerLimit());
 		 	 $builder->addColumnAndData('upperLimit',parent::getupperLimit());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('transportrate');

 	 	 if(!is_null(parent::getrate())){
$builder->addColumnAndData('rate',parent::getrate()); 
}

 	 	 if(!is_null(parent::getlowerLimit())){
$builder->addColumnAndData('lowerLimit',parent::getlowerLimit()); 
}

 	 	 if(!is_null(parent::getupperLimit())){
$builder->addColumnAndData('upperLimit',parent::getupperLimit()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  transportrate";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  transportrate";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  transportrate limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('transportrate');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  transportrate  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setrate($row['rate']);
	 	parent::setlowerLimit($row['lowerLimit']);
	 	parent::setupperLimit($row['upperLimit']);
}	 	}



	 public function getTransportRateObject($x){
		 $rate=0;
		 $sql="select * from  transportrate";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 if($x>=intval($row['lowerLimit']) && $x<=intval($row['upperLimit']) )
			 {

				 $rate=$row['rate'];

			 }

		 }
		 return $rate;
	 }

	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>