<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountService
 *
 * @author plussoft
 */
include_once '../model/fuel.php';
class FuelService extends Fuel {
  
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblfuel");
		if(parent::getvehicleId()!="")
		{
		$builder->addColumnAndData("vehicleId", parent::getvehicleId());
		}
		if(parent::getdistance()!="")
		{
		$builder->addColumnAndData("distance", parent::getdistance());
		}
		if(parent::getqty()!="")
		{
		$builder->addColumnAndData("qty",parent::getqty());
		}
		if(parent::getRate()!="")
		{
		$builder->addColumnAndData("Rate",parent::getRate());
		}
		if(parent::getCreationDate()!="")
		{
		$builder->addColumnAndData("CreationDate",parent::getCreationDate());
		}
		
		if(parent::getDestination()!="")
		{
		$builder->addColumnAndData("Destination",parent::getDestination());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
   	public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tblfuel");
			if(parent::getvehicleId()!=""){
			$builder->addColumnAndData("vehicleId", parent::getvehicleId());
			}
			if(parent::getdistance()!=""){
			$builder->addColumnAndData("distance", parent::getdistance());
			}
			if(parent::getqty()!="")
			{
			$builder->addColumnAndData("qty",parent::getqty());
			}
			
			if(parent::getRate()!=""){
			$builder->addColumnAndData("Rate",parent::getRate());
			}
			
			if(parent::getCreationDate()!=""){
			$builder->addColumnAndData("CreationDate",parent::getCreationDate());
			}
			if(parent::getDestination()!="")
			{
			$builder->addColumnAndData("Destination",parent::getDestination());
			}
			
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function view() {
		$sqlInner="select f.*,v.regNo,v.id vid from tblfuel f inner join tblvehicle v on  f.vehicleId=v.id";
		$data=array();
		$totalQty=0.0;
		$totalAmount=0.0;
		foreach( $this->con->getResultSet($sqlInner) as $row2)
	   {
		   	$id=$row2['id'];
       		
	   			$data2=array();
			   $data2["id"]=$row2['id'];
			   $data2["vid"]=$row2['vid'];
			   $data2["vehicleId"]=$row2["regNo"];
			   $data2["destination"]=$row2["Destination"];
			   $data2["qty"]=$row2["qty"];
			   $data2["km"]="";
			   $data2["currency"]=number_format($row2["qty"]*$row2["Rate"],0);
			   $data2['date']=$row2['CreationDate'];
			   $data2['Rate']=$row2['Rate'];
			   
			   array_push($data,$data2);
			   $totalQty=$totalQty+$row2["qty"];
			  $totalAmount=$totalAmount+($row2["qty"]*$row2["Rate"]);
		  		
	   }
	  		 $data2=array();
			 $data2["id"]="";
			 $data2["vehicleId"]="Total";
			 $data2["distance"]="";
			 $data2["qty"]=$totalQty;
			 $data2["km"]="";
			 $data2["currency"]=number_format($totalAmount,0);
			 $data2['date']="";
			 $data2['Rate']="";
			 array_push($data,$data2);
	   		return $data;
    }
	
	
	public function view2() {
		$sqlInner="select f.*,v.regNo,v.id vid from tblfuel f inner join tblvehicle v on  f.vehicleId=v.id where f.vehicleId='".parent::getvehicleId()."' and f.CreationDate between '".parent::getCreationDate()."' AND '".parent::getRate()."'";
		$data=array();
		$totalQty=0.0;
		$totalAmount=0.0;
		foreach( $this->con->getResultSet($sqlInner) as $row2)
	   {
		   	$id=$row2['id'];
       		
	   			$data2=array();
			   $data2["id"]=$row2['id'];
			   $data2["vid"]=$row2['vid'];
			   $data2["vehicleId"]=$row2["regNo"];
			   $data2["destination"]=$row2["Destination"];
			   $data2["qty"]=$row2["qty"];
			   $data2["km"]="";
			   $data2["currency"]=number_format($row2["qty"]*$row2["Rate"],0);
			   $data2['date']=$row2['CreationDate'];
			   $data2['Rate']=$row2['Rate'];
			   
			   array_push($data,$data2);
			   $totalQty=$totalQty+$row2["qty"];
			  $totalAmount=$totalAmount+($row2["qty"]*$row2["Rate"]);
		  		
	   }
	  		 $data2=array();
			 $data2["id"]="";
			 $data2["vehicleId"]="Total";
			 $data2["distance"]="";
			 $data2["qty"]=$totalQty;
			 $data2["km"]="";
			 $data2["currency"]=number_format($totalAmount,0);
			 $data2['date']="";
			 $data2['Rate']="";
			 array_push($data,$data2);
	   		return $data;
    }
	public function view3() {
		$sqlInner="select f.*,v.regNo,v.id vid from tblfuel f inner join tblvehicle v on  f.vehicleId=v.id where f.vehicleId='".parent::getvehicleId()."'";
		$data=array();
		$totalQty=0.0;
		$totalAmount=0.0;
		foreach( $this->con->getResultSet($sqlInner) as $row2)
	   {
		   	$id=$row2['id'];
       		
	   			$data2=array();
			   $data2["id"]=$row2['id'];
			   $data2["vid"]=$row2['vid'];
			   $data2["vehicleId"]=$row2["regNo"];
			   $data2["destination"]=$row2["Destination"];
			   $data2["qty"]=$row2["qty"];
			   $data2["km"]="";
			   $data2["currency"]=number_format($row2["qty"]*$row2["Rate"],0);
			   $data2['date']=$row2['CreationDate'];
			   $data2['Rate']=$row2['Rate'];
			   
			   array_push($data,$data2);
			   $totalQty=$totalQty+$row2["qty"];
			  $totalAmount=$totalAmount+($row2["qty"]*$row2["Rate"]);
		  		
	   }
	  		 $data2=array();
			 $data2["id"]="";
			 $data2["vehicleId"]="Total";
			 $data2["distance"]="";
			 $data2["qty"]=$totalQty;
			 $data2["km"]="";
			 $data2["currency"]=number_format($totalAmount,0);
			 $data2['date']="";
			 $data2['Rate']="";
			 array_push($data,$data2);
	   		return $data;
    }
	public function view4() {
		$sqlInner="select f.*,v.regNo,v.id vid from tblfuel f inner join tblvehicle v on  f.vehicleId=v.id where f.CreationDate between '".parent::getCreationDate()."' AND '".parent::getRate()."'";
		$data=array();
		$totalQty=0.0;
		$totalAmount=0.0;
		foreach( $this->con->getResultSet($sqlInner) as $row2)
	   {
		   	$id=$row2['id'];
       		
	   			$data2=array();
			   $data2["id"]=$row2['id'];
			   $data2["vid"]=$row2['vid'];
			   $data2["vehicleId"]=$row2["regNo"];
			   $data2["destination"]=$row2["Destination"];
			   $data2["qty"]=$row2["qty"];
			   $data2["km"]="";
			   $data2["currency"]=number_format($row2["qty"]*$row2["Rate"],0);
			   $data2['date']=$row2['CreationDate'];
			   $data2['Rate']=$row2['Rate'];
			   
			   array_push($data,$data2);
			   $totalQty=$totalQty+$row2["qty"];
			  $totalAmount=$totalAmount+($row2["qty"]*$row2["Rate"]);
		  		
	   }
	  		 $data2=array();
			 $data2["id"]="";
			 $data2["vehicleId"]="Total";
			 $data2["distance"]="";
			 $data2["qty"]=$totalQty;
			 $data2["km"]="";
			 $data2["currency"]=number_format($totalAmount,0);
			 $data2['date']="";
			 $data2['Rate']="";
			 array_push($data,$data2);
	   		return $data;
    }
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblfuel");
		
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
}

	
