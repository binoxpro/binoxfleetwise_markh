<?php 
 require_once('../model/workorder.php');
 class workOrderService extends workOrder{
	 	 public function save()
		 {
			 $builder=new InsertBuilder();
			 $builder->setTable('workorder');
			 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
			 $builder->addColumnAndData('townFrom',parent::gettownFrom());
			 $builder->addColumnAndData('townTo',parent::gettownTo());
 		 	 $builder->addColumnAndData('DeliveryNo',parent::getDeliveryNo());
 		 	 $builder->addColumnAndData('PONo',parent::getPONo());
			 $builder->addColumnAndData('product',parent::getProduct());
 		 	 $builder->addColumnAndData('Quantity',parent::getQuantity());
 		 	 $builder->addColumnAndData('Bags',parent::getBags());
 		 	 $builder->addColumnAndData('Rate',parent::getRate());
			 $builder->addColumnAndData('distance',parent::getDistance());
 		 	 $builder->addColumnAndData('Amount',parent::getAmount());
 		 	 $builder->addColumnAndData('CustomerId',parent::getCustomerId());
 		 	 $builder->addColumnAndData('Claimed',parent::getClaimed());
 		 	 $builder->addColumnAndData('WhtApplied',parent::getWhtApplied());
 		 	 $builder->addColumnAndData('WhtValue',parent::getWhtValue());
 		 	 $builder->addColumnAndData('CreationDate',parent::getCreationDate());
 		 	 $builder->addColumnAndData('IsActive',parent::getIsActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 	return $this->con->execute_query2($builder->getValues());
 	  	}
 
	 	 public function update()
		 {
						 $builder=new UpdateBuilder();
						 $builder->setTable('workorder');
						 if(!is_null(parent::gettripNo())){
						$builder->addColumnAndData('tripNo',parent::gettripNo());
						}

							 if(!is_null(parent::gettownFrom())){
								 $builder->addColumnAndData('townFrom',parent::gettownFrom());
							 }


			 if(!is_null(parent::gettownTo())){
				 $builder->addColumnAndData('townTo',parent::gettownTo());
			 }

								 if(!is_null(parent::getDeliveryNo())){
						$builder->addColumnAndData('DeliveryNo',parent::getDeliveryNo());
						}

								 if(!is_null(parent::getPONo())){
						$builder->addColumnAndData('PONo',parent::getPONo());
						}
									 if(!is_null(parent::getProduct()))
									 {
										 $builder->addColumnAndData('product',parent::getProduct());
									 }

									if(!is_null(parent::getQuantity()))
									{
									$builder->addColumnAndData('Quantity',parent::getQuantity());
									}

								 if(!is_null(parent::getBags())){
						$builder->addColumnAndData('Bags',parent::getBags());
						}

								 if(!is_null(parent::getRate())){
						$builder->addColumnAndData('Rate',parent::getRate());
						}

					 if(!is_null(parent::getDistance()))
					 {
						 $builder->addColumnAndData('distance',parent::getDistance());
					 }

								 if(!is_null(parent::getAmount())){
						$builder->addColumnAndData('Amount',parent::getAmount());
						}

								 if(!is_null(parent::getCustomerId())){
						$builder->addColumnAndData('CustomerId',parent::getCustomerId());
						}

								 if(!is_null(parent::getClaimed())){
						$builder->addColumnAndData('Claimed',parent::getClaimed());
						}

								 if(!is_null(parent::getWhtApplied())){
						$builder->addColumnAndData('WhtApplied',parent::getWhtApplied());
						}

								 if(!is_null(parent::getWhtValue())){
						$builder->addColumnAndData('WhtValue',parent::getWhtValue());
						}

								 if(!is_null(parent::getCreationDate())){
						$builder->addColumnAndData('CreationDate',parent::getCreationDate());
						}

						 if(!is_null(parent::getDistance())) {
							 $builder->addColumnAndData('distance', parent::getDistance());
						 }

						 if(!is_null(parent::getIsActive()))
						 {
							$builder->addColumnAndData('IsActive',parent::getIsActive());
						 }
						$builder->setCriteria("where id='".parent::getid()."'");
						$this->con->setQuery(Director::buildSql($builder));
								 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  workorder";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select wo.*,tc.customerName from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode where wo.tripNo='".parent::gettripNo()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select wo.*,tc.customerName from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode where wo.tripNo='".parent::gettripNo()."' limit $offset,$rows  ";
		 foreach($this->con->getResultSet($sql) as $row)
		{
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }

	 public function viewUnClaimed()
	 {
		 $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		 $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		 $offset = ($page-1)*$rows;
		 $sql="select wo.*,tc.customerName from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode where wo.Claimed!='1'";
		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql="select wo.*,tc.customerName from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode where wo.Claimed!='1' limit $offset,$rows  ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;
	 }


	 public function viewUnClaimedDate($startDate,$endDate)
	 {
		 $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		 $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		 $offset = ($page-1)*$rows;
		 $sql="select wo.*,tc.customerName,tng.TripStartDate from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode inner join tripnumbergenerator tng on wo.tripNo=tng.TripNumber where wo.Claimed!='1' and tng.TripStartDate BETWEEN '$startDate' and '$endDate'";
		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql="select wo.*,tc.customerName,tng.TripStartDate from  workorder wo INNER JOIN tbl_customer tc on wo.customerId=tc.customerCode inner join tripnumbergenerator tng on wo.tripNo=tng.TripNumber where wo.Claimed!='1' and tng.TripStartDate BETWEEN '$startDate' and '$endDate' limit $offset,$rows  ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;
	 }

 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('workorder');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find()
	 {
	 	 $sql="select * from  workorder  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::settripNo($row['tripNo']);
	 	parent::setDeliveryNo($row['DeliveryNo']);
	 	parent::setPONo($row['PONo']);
	 	parent::setQuantity($row['Quantity']);
	 	parent::setBags($row['Bags']);
	 	parent::setRate($row['Rate']);
	 	parent::setAmount($row['Amount']);
	 	parent::setCustomerId($row['CustomerId']);
	 	parent::setClaimed($row['Claimed']);
	 	parent::setWhtApplied($row['WhtApplied']);
	 	parent::setWhtValue($row['WhtValue']);
	 	parent::setCreationDate($row['CreationDate']);
	 	parent::setIsActive($row['IsActive']);
		parent::setProduct($row['product']);
		}
	 }

 	 public function view_query($sql)
	 {
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>