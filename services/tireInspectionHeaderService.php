<?php 
 require_once('../model/tireInspectionHeader.php');
 class tireInspectionHeaderService extends tireInspectionHeader{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltireinspectionheader');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $builder->addColumnAndData('lastReading',parent::getlastReading());
 		 	 $builder->addColumnAndData('currentReading',parent::getcurrentReading());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
			 $builder->addColumnAndData('nextReading',parent::getNextReading());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	  $this->con->execute_query2($builder->getValues());
          return array('x'=>$this->con->getId());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltireinspectionheader');

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
}

 	 	 if(!is_null(parent::getlastReading())){
$builder->addColumnAndData('lastReading',parent::getlastReading()); 
}

 	 	 if(!is_null(parent::getcurrentReading())){
$builder->addColumnAndData('currentReading',parent::getcurrentReading()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

if(!is_null(parent::getNextReading())){
$builder->addColumnAndData('nextReading',parent::getNextReading()); 
}


 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select tih.*,v.regNo from  tbltireinspectionheader tih inner join tblvehicle v on v.id=tih.vehicleId where tih.IsActive=1";
	       $data=array();
		   foreach($this->con->getResultSet($sql) as $row)
		   {
			   $today=strtotime(date('Y-m-d'))/(60*60*24);
			   $nextReading=strtotime($row['nextReading'])/(60*60*24);
			   $row['days']=round(($nextReading-$today),0);
			   array_push($data,$row);
		   }
		   return $data;
 	 }

	 public function vehicleHistorySearch()
	 {
		 $sql="select tih.*,v.regNo from  tbltireinspectionheader tih inner join tblvehicle v on v.id=tih.vehicleId where tih.vehicleId='".parent::getvehicleId()."'";
		 $data=array();
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $today=strtotime(date('Y-m-d'))/(60*60*24);
			 $nextReading=strtotime($row['nextReading'])/(60*60*24);
			 $row['days']=round(($nextReading-$today),0);
			 array_push($data,$row);
		 }
		 return $data;
	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltireinspectionheader');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject()
		 {
	 	 $sql="select * from  tbltireinspectionheader where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
		 {

			 parent::setvehicleId($row["vehicleId"]);
			 parent::setlastReading($row["lastReading"]);
			 parent::setcurrentReading($row["currentReading"]);
			 parent::setcreationDate($row["creationDate"]);
			 parent::setisActive($row["isActive"]);

	 	} 
 }
 
 public function getLastInspectionObject(){
	 	 $sql="select * from  tbltireinspectionheader where vehicleId='".parent::getvehicleId()."' and isActive='1'";
	 	 foreach($this->con->getResultSet($sql) as $row)
		 {
        
             parent::setid($row['id']);
     	 	 parent::setvehicleId($row["vehicleId"]); 
     	 	 parent::setlastReading($row["lastReading"]); 
     	 	 parent::setcurrentReading($row["currentReading"]); 
     	 	 parent::setcreationDate($row["creationDate"]); 
     	 	 parent::setisActive($row["isActive"]); 
	 	} 
 }
 
 
 public function generateNextReading(){
	 	 $sql="select * from  tbltireinspectionheader";
	 	 foreach($this->con->getResultSet($sql) as $row){
        	
             $this->setid($row['id']);
     	 	 $this->setvehicleId($row["vehicleId"]); 
     	 	 $this->setlastReading($row["lastReading"]); 
     	 	 $this->setcurrentReading($row["currentReading"]); 
			 $this->setcreationDate($row["creationDate"]);
			 $date=date_create($row["creationDate"]);
			date_add($date,date_interval_create_from_date_string("25 days"));
		 	$this->setNextReading(date_format($date,"Y-m-d"));
			 //parent::setNextReading($row['nextReading']);
     	 	$this->setisActive($row["isActive"]); 
			 $this->update();
	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>