<?php 
 require_once('../model/storestock.php');
 class storestockService extends storestock{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('stock');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('itemId',parent::getitemId());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('unitPrice',parent::getunitPrice());
 		 	 $builder->addColumnAndData('stocktype',parent::getstocktype());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('recordedBy',parent::getrecordedBy());
 		 	 $builder->addColumnAndData('receivedBy',parent::getreceivedBy());
 		 	 $builder->addColumnAndData('supplierId',parent::getsupplierId());
 		 	 $builder->addColumnAndData('checkedBy',parent::getcheckedBy());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('stock');

 	 	 if(!is_null(parent::getitemId())){
$builder->addColumnAndData('itemId',parent::getitemId()); 
}

 	 	 if(!is_null(parent::getquantity())){
$builder->addColumnAndData('quantity',parent::getquantity()); 
}

 	 	 if(!is_null(parent::getunitPrice())){
$builder->addColumnAndData('unitPrice',parent::getunitPrice()); 
}

 	 	 if(!is_null(parent::getstocktype())){
$builder->addColumnAndData('stocktype',parent::getstocktype()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getrecordedBy())){
$builder->addColumnAndData('recordedBy',parent::getrecordedBy()); 
}

 	 	 if(!is_null(parent::getreceivedBy())){
$builder->addColumnAndData('receivedBy',parent::getreceivedBy()); 
}

 	 	 if(!is_null(parent::getsupplierId())){
$builder->addColumnAndData('supplierId',parent::getsupplierId()); 
}

 	 	 if(!is_null(parent::getcheckedBy())){
$builder->addColumnAndData('checkedBy',parent::getcheckedBy()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  stock";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select it.itemName,getitemstocklevel(s.itemId) QuantityInStore,it.levelAlert from  stock s inner join items it on s.itemId=it.id  GROUP BY it.itemName ";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select it.itemName,getitemstocklevel(s.itemId) QuantityInStore,it.levelAlert from  stock s inner join items it on s.itemId=it.id  GROUP BY it.itemName limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('stock');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  stock  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setitemId($row['itemId']);
	 	parent::setquantity($row['quantity']);
	 	parent::setunitPrice($row['unitPrice']);
	 	parent::setstocktype($row['stocktype']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setrecordedBy($row['recordedBy']);
	 	parent::setreceivedBy($row['receivedBy']);
	 	parent::setsupplierId($row['supplierId']);
	 	parent::setcheckedBy($row['checkedBy']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>