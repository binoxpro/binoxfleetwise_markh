<?php 
 require_once('../model/tripType.php');
 class tripTypeService extends tripType{
	 	 public function save()
		 {
			 $builder=new InsertBuilder();
			 $builder->setTable('tbltriptype');
			 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('Name',parent::getName());
 		 	 $builder->addColumnAndData('IsActive',parent::getIsActive());
			 $builder->addColumnAndData('prefix',parent::getPrefix());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 	return $this->con->execute_query2($builder->getValues());
		}
 
	 	 public function update()
		 {
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltriptype');

		 if(!is_null(parent::getName()))
		 {
			$builder->addColumnAndData('Name',parent::getName());
		}

 	 	 if(!is_null(parent::getIsActive()))
		 {
			$builder->addColumnAndData('IsActive',parent::getIsActive());
		}
		 if(!is_null(parent::getPrefix()))
		 {
			 $builder->addColumnAndData('prefix',parent::getPrefix());
		 }
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 		}
 

 	 public function view()
	 {
	 	 $sql="select * from  tbltriptype";
        return $this->con->getResultSet($sql);
 	 }

 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltriptype');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject()
		 {
			 $sql="select * from  tbltriptype where id='".parent::getid()."'";
			 foreach($this->con->getResultSet($sql) as $row)
			 {
				 parent::setName($row["Name"]);
				 parent::setPrefix($row['prefix']);
				 parent::setIsActive($row["IsActive"]);

			}
 		}
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>