<?php 
 require_once('../model/payRate.php');
 class payRateService extends payRate{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('payrate');
	 	 $builder->addColumnAndData('Id',parent::getId());
 		 	 $builder->addColumnAndData('Rate',parent::getRate());
 		 	 $builder->addColumnAndData('PositionId',parent::getPositionId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('payrate');

 	 	 if(!is_null(parent::getRate())){
$builder->addColumnAndData('Rate',parent::getRate()); 
}

 	 	 if(!is_null(parent::getPositionId())){
$builder->addColumnAndData('PositionId',parent::getPositionId()); 
}
$builder->setCriteria("where Id='".parent::getId()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  payrate";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  payrate";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select pr.*,pn.position from  payrate pr INNER JOIN tblposition pn on pr.positionId=pn.id limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('payrate');
	 	 $builder->setCriteria("where Id='".parent::getId()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  payrate  where Id='".parent::getId()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setId($row['Id']);
	 	parent::setRate($row['Rate']);
	 	parent::setPositionId($row['PositionId']);
}	 	}
	 public function findWithPosition(){
		 $sql="select * from  payrate  where positionId='".parent::getPositionId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setId($row['Id']);
			 parent::setRate($row['Rate']);
			 parent::setPositionId($row['PositionId']);
		 }	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>