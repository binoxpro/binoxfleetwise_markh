<?php 
 require_once('../model/paymentVoucherItem.php');
 class paymentVoucherItemService extends paymentVoucherItem{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('paymentvoucheritem');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('payVoucherId',parent::getpayVoucherId());
 		 	 $builder->addColumnAndData('particular',parent::getparticular());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
 		 	 $builder->addColumnAndData('individualCode',parent::getindividualCode());
 		 	 $builder->addColumnAndData('costCenter',parent::getcostCenter());
 		 	 $builder->addColumnAndData('tripNo',parent::gettripNo());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('paymentvoucheritem');

 	 	 if(!is_null(parent::getpayVoucherId())){
$builder->addColumnAndData('payVoucherId',parent::getpayVoucherId()); 
}

 	 	 if(!is_null(parent::getparticular())){
$builder->addColumnAndData('particular',parent::getparticular()); 
}

 	 	 if(!is_null(parent::getamount())){
$builder->addColumnAndData('amount',parent::getamount()); 
}

 	 	 if(!is_null(parent::getaccountCode())){
$builder->addColumnAndData('accountCode',parent::getaccountCode()); 
}

 	 	 if(!is_null(parent::getindividualCode())){
$builder->addColumnAndData('individualCode',parent::getindividualCode()); 
}

 	 	 if(!is_null(parent::getcostCenter())){
$builder->addColumnAndData('costCenter',parent::getcostCenter()); 
}

 	 	 if(!is_null(parent::gettripNo())){
$builder->addColumnAndData('tripNo',parent::gettripNo()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  paymentvoucheritem";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select pvi.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pvi.accountCode) AccountName,(select ia.AccountName from  individualaccounts ia  where ia.Id=pvi.individualCode) Individual from  paymentvoucheritem pvi where pvi.payVoucherId='".parent::getpayVoucherId()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select pvi.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=pvi.accountCode) AccountName,(select ia.AccountName from  individualaccounts ia  where ia.Id=pvi.individualCode) Individual,(select ia.AccountName from  individualaccounts ia  where ia.Id=pvi.costCenter) costCenterName from  paymentvoucheritem pvi where pvi.payVoucherId='".parent::getpayVoucherId()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('paymentvoucheritem');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  paymentvoucheritem  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setpayVoucherId($row['payVoucherId']);
	 	parent::setparticular($row['particular']);
	 	parent::setamount($row['amount']);
	 	parent::setaccountCode($row['accountCode']);
	 	parent::setindividualCode($row['individualCode']);
	 	parent::setcostCenter($row['costCenter']);
	 	parent::settripNo($row['tripNo']);
}	 	}


	 public function findTotalCost(){
		 $total=0;
		 $sql="select sum(amount) total from  paymentvoucheritem  where payVoucherId='".parent::getpayVoucherId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $total=$row['total'];
		 }
		 return $total;
	 }
 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>