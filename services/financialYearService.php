<?php 
 require_once('../model/financialYear.php');
 class financialYearService extends financialYear{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('financialyear');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('startDate',parent::getstartDate());
 		 	 $builder->addColumnAndData('endDate',parent::getendDate());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('financialyear');

 	 	 if(!is_null(parent::getstartDate())){
$builder->addColumnAndData('startDate',parent::getstartDate()); 
}

 	 	 if(!is_null(parent::getendDate())){
$builder->addColumnAndData('endDate',parent::getendDate()); 
}

 	 	 if(!is_null(parent::getisActive()))
		 {
				$builder->addColumnAndData('isActive',parent::getisActive());
		}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  financialyear";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  financialyear";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  financialyear limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('financialyear');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  financialyear  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setstartDate($row['startDate']);
	 	parent::setendDate($row['endDate']);
	 	parent::setisActive($row['isActive']);
}	 	}


	 public function updateAllFinancialYear()
	 {
		 $builder=new UpdateBuilder();
		 $builder->setTable('financialyear');
		 if(!is_null(parent::getisActive()))
		 {
			 $builder->addColumnAndData('isActive',parent::getisActive());
		 }

		 $this->con->setQuery(Director::buildSql($builder));
		 return $this->con->execute_query();
	 }
 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>