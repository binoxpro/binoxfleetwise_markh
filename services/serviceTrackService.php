<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcademicYearService
 *
 * @author plussoft
 * 
 */
include_once '../model/serviceTrack.php';

class ServiceTrackService extends ServiceTrack{
    
   private $school_no_init;
    
    function __construct(){
        parent::__construct();       
    }

#obslet method use save maintainance
    public function save() {
       
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			$builder->addColumnAndData("IntervalPeriod",parent::getInterval());
			$builder->addColumnAndData("Reminder",parent::getReminder());
			$builder->addColumnAndData("Service",parent::getService());
			$builder->addColumnAndData("TypeOfService",parent::getTypeOfService());
			$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_service where numberPlate='".parent::getNumberPlate()."' and km_reading='".parent::getKmReading()."'");
			if($this->con->sqlCount()<1){
				$this->con->execute_query2($builder->getValues());
				$this->setServiceID($this->con->getId());
				 return $this->updateStatus();
			}else{
				return array('msg'=>'Service schedue ready saved');
			}
            
       
       // parent::save();
    }
	
	public function saveStatus() {
       
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service_status");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			//$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_service_status where numberPlate='".parent::getNumberPlate()."' ");
			if($this->con->sqlCount()<1){
				//$this->updateStatus();
            	  return $this->con->execute_query2($builder->getValues());
				
			}else{
				return array('msg'=>'Service schedue ready saved');
			}
            
       
       // parent::save();
    }
	
    public function update() {
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			$builder->addColumnAndData("reg_date", parent::getRegDate());
			$builder->setCriteria("Where serviceID='".parent::getServiceID()."'");
            $this->con->setQuery(Director::buildSql($builder));
			$this->con->execute_query();
			return $this->updateStatus();
		
    }
	#Obsolet method
	public function updateStatus(){
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			//$builder->addColumnAndData("reg_date", $datex);
			$builder->setCriteria("Where numberPlate='".parent::getNumberPlate()."'");
            $this->con->setQuery(Director::buildSql($builder));
			$this->con->setSelect_query("select * from tbl_service_status where numberPlate='".parent::getNumberPlate()."' ");
			if($this->con->sqlCount()<1){
				 return $this->saveStatus();
			}else{
				 return $this->con->execute_query();
			}
		
    
	}
	public function updateStatusNew(){
		
         	$builder=new UpdateBuilder();
           	$builder->setTable("tbl_service_status");
           // $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
            $builder->addColumnAndData("PreviousDate", parent::getPreviousDate());
			$builder->addColumnAndData("NextDate", parent::getNextDate());
			//$builder->addColumnAndData("reg_date", $datex);
			$builder->setCriteria("Where numberPlate='".parent::getNumberPlate()."'");
            $this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    
	}
	public function saveMaintanance() {
       
           $datex=date('Y-m-d');
            $builder=new InsertBuilder();
            $builder->setTable("tbl_service");
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
			$builder->addColumnAndData("PreviousDate",parent::getPreviousDate());
			$builder->addColumnAndData("NextDate",parent::getNextDate());
			$builder->addColumnAndData("IntervalPeriod",parent::getInterval());
			$builder->addColumnAndData("Reminder",parent::getReminder());
			$builder->addColumnAndData("Service",parent::getService());
			$builder->addColumnAndData("TypeOfService",parent::getTypeOfService());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			$builder->addColumnAndData("reg_date", $datex);
            $this->con->setQuery(Director::buildSql($builder));
			//this part is not needed in the save but if the client change requirement we will add it 
           // $this->con->setSelect_query("select * from tbl_service where PreviousDate='".parent::getPreviousDate()."' and km_reading='".parent::getKmReading()."'");
			///if($this->con->sqlCount()<1){
				$this->con->execute_query2($builder->getValues());
				return $this->updateStatusNew();
			//}else{
				//return array('msg'=>'Service schedue ready saved');
			//}
            
       
       // parent::save();
    }
	public function updateMaintanance() {
       
           $datex=date('Y-m-d');
            $builder=new UpdateBuilder();
            $builder->setTable("tbl_service");
			
            $builder->addColumnAndData("numberPlate",parent::getNumberPlate());
			$builder->addColumnAndData("PreviousDate",parent::getPreviousDate());
			$builder->addColumnAndData("NextDate",parent::getNextDate());
			$builder->addColumnAndData("IntervalPeriod",parent::getInterval());
			$builder->addColumnAndData("Reminder",parent::getReminder());
			$builder->addColumnAndData("Service",parent::getService());
			$builder->addColumnAndData("TypeOfService",parent::getTypeOfService());
            $builder->addColumnAndData("km_reading", parent::getKmReading());
			$builder->addColumnAndData("next_reading", parent::getNextReading());
			$builder->addColumnAndData("reg_date", $datex);
            $builder->setCriteria("Where serviceID='".parent::getServiceID()."'");
            $this->con->setQuery(Director::buildSql($builder));
			 $this->con->execute_query();
				return $this->updateStatusNew();
			//}else{
				//return array('msg'=>'Service schedue ready saved');
			//}
            
       
       // parent::save();
    }
    public function view2($id) {
       $sql="select * from tbl_class ";
	   return $this->con->getResultSet($sql);
    }

	public function view() {
       $sql="select * from tbl_service where numberPlate='".parent::getNumberPlate()."'";
	   return $this->con->getResultSet($sql);
    }
	
	
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	//the method is litte in complete
	public function searchFilterForService(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getInterval()==NULL?'null':"'".parent::getInterval()."'";
		$para4=parent::getReminder()==NULL?'null':"'".parent::getReminder()."'";
		$para5=parent::getService()==NULL?'null':"'".parent::getService()."'";
		$para6=parent::getTypeOfService()==NULL?'null':"'".parent::getTypeOfService()."'";
		$para7=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		$para8=parent::getServiceID()==NULL?'null':"'".parent::getServiceID()."'";
		$sql="select serviceID,numberPlate,PreviousDate,NextDate,IntervalPeriod,Reminder,Service,TypeOfService from tbl_service where serviceID=COALESCE(".$para8.",serviceID) AND numberPlate=COALESCE(".$para7.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate) AND IntervalPeriod=COALESCE(".$para3.",IntervalPeriod) AND Reminder=COALESCE(".$para4.",Reminder) AND Service=COALESCE(".$para5.",Service) AND TypeOfService=COALESCE(".$para6.",TypeOfService) ";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	
	public function searchFilterForServiceStatus(){
		$para1=parent::getPreviousDate()==NULL?'null':"'".parent::getPreviousDate()."'";
		$para2=parent::getNextDate()==NULL?'null':"'".parent::getNextDate()."'";
		$para3=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		
		$sql="select * from tbl_service_status where numberPlate=COALESCE(".$para3.",numberPlate) AND PreviousDate=COALESCE(".$para1.",PreviousDate) AND NextDate=COALESCE(".$para2.",NextDate)";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	
	public function loadId()
	{
		return $this->con->getId();	
	}
    //put your code here
}
?>