<?php 
 require_once('../model/tireSize.php');
 class tireSizeService extends tireSize{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltiresize');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('name',parent::getname());
 		 	 $builder->addColumnAndData('description',parent::getdescription());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltiresize');

 	 	 if(!is_null(parent::getname())){
$builder->addColumnAndData('name',parent::getname()); 
}

 	 	 if(!is_null(parent::getdescription())){
$builder->addColumnAndData('description',parent::getdescription()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltiresize";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltiresize');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltiresize where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setname($row["name"]); 


 	 	 parent::setdescription($row["description"]); 


 	 	 parent::setisActive($row["isActive"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>