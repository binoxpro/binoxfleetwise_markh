<?php 
 require_once('../model/itemcategory.php');
 class itemcategoryService extends itemcategory{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('itemcategory');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('itemCategory',parent::getitemCategory());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('itemcategory');

 	 	 if(!is_null(parent::getitemCategory())){
$builder->addColumnAndData('itemCategory',parent::getitemCategory()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  itemcategory";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  itemcategory";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  itemcategory limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('itemcategory');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  itemcategory  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setitemCategory($row['itemCategory']);
	 	parent::setisActive($row['isActive']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>