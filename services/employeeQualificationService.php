<?php 
 require_once('../model/employeeQualification.php');
 class employeeQualificationService extends employeeQualification{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('employeequalification');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('empNo',parent::getempNo());
 		 	 $builder->addColumnAndData('instition',parent::getinstition());
 		 	 $builder->addColumnAndData('years',parent::getyears());
 		 	 $builder->addColumnAndData('ward',parent::getward());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('employeequalification');

 	 	 if(!is_null(parent::getempNo())){
$builder->addColumnAndData('empNo',parent::getempNo()); 
}

 	 	 if(!is_null(parent::getinstition())){
$builder->addColumnAndData('instition',parent::getinstition()); 
}

 	 	 if(!is_null(parent::getyears())){
$builder->addColumnAndData('years',parent::getyears()); 
}

 	 	 if(!is_null(parent::getward())){
$builder->addColumnAndData('ward',parent::getward()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  employeequalification";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view()
	 {
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  employeequalification where empNo='".parent::getempNo()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  employeequalification where empNo='".parent::getempNo()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
			}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('employeequalification');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  employeequalification  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setempNo($row['empNo']);
	 	parent::setinstition($row['instition']);
	 	parent::setyears($row['years']);
	 	parent::setward($row['ward']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>