<?php 
 require_once('../model/lopdelivery.php');
 class lopdeliveryService extends lopdelivery{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('purfdelivery');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('purfItemId',parent::getpurfItemId());
 		 	 $builder->addColumnAndData('quantity',parent::getquantity());
 		 	 $builder->addColumnAndData('passed1',parent::getpassed1());
 		 	 $builder->addColumnAndData('passed2',parent::getpassed2());
 		 	 $builder->addColumnAndData('deliveryDate',parent::getdeliveryDate());
 		 	 $builder->addColumnAndData('taxNo',parent::gettaxNo());
 		 	 $builder->addColumnAndData('deliveryNo',parent::getdeliveryNo());
 		 	 $builder->addColumnAndData('recordedBy',parent::getrecordedBy());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('purfdelivery');

 	 	 if(!is_null(parent::getpurfItemId())){
$builder->addColumnAndData('purfItemId',parent::getpurfItemId()); 
}

 	 	 if(!is_null(parent::getquantity())){
$builder->addColumnAndData('quantity',parent::getquantity()); 
}

 	 	 if(!is_null(parent::getpassed1())){
$builder->addColumnAndData('passed1',parent::getpassed1()); 
}

 	 	 if(!is_null(parent::getpassed2())){
$builder->addColumnAndData('passed2',parent::getpassed2()); 
}

 	 	 if(!is_null(parent::getdeliveryDate())){
$builder->addColumnAndData('deliveryDate',parent::getdeliveryDate()); 
}

 	 	 if(!is_null(parent::gettaxNo())){
$builder->addColumnAndData('taxNo',parent::gettaxNo()); 
}

 	 	 if(!is_null(parent::getdeliveryNo())){
$builder->addColumnAndData('deliveryNo',parent::getdeliveryNo()); 
}

 	 	 if(!is_null(parent::getrecordedBy())){
$builder->addColumnAndData('recordedBy',parent::getrecordedBy()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  purfdelivery";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  purfdelivery";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  purfdelivery limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('purfdelivery');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  purfdelivery  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setpurfItemId($row['purfItemId']);
	 	parent::setquantity($row['quantity']);
	 	parent::setpassed1($row['passed1']);
	 	parent::setpassed2($row['passed2']);
	 	parent::setdeliveryDate($row['deliveryDate']);
	 	parent::settaxNo($row['taxNo']);
	 	parent::setdeliveryNo($row['deliveryNo']);
	 	parent::setrecordedBy($row['recordedBy']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>