<?php 
 require_once('../model/tireWear.php');
 class tireWearService extends tireWear{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltirewear');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('tireWear',parent::gettireWear());
 		 	 $builder->addColumnAndData('comment',parent::getcomment());
 		 	 $builder->addColumnAndData('CreationDate',parent::getCreationDate());
 		 	 $builder->addColumnAndData('tireId',parent::gettireId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltirewear');

 	 	 if(!is_null(parent::gettireWear())){
$builder->addColumnAndData('tireWear',parent::gettireWear()); 
}

 	 	 if(!is_null(parent::getcomment())){
$builder->addColumnAndData('comment',parent::getcomment()); 
}

 	 	 if(!is_null(parent::getCreationDate())){
$builder->addColumnAndData('CreationDate',parent::getCreationDate()); 
}

 	 	 if(!is_null(parent::gettireId())){
$builder->addColumnAndData('tireId',parent::gettireId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tbltirewear";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltirewear');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltirewear where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::settireWear($row["tireWear"]); 


 	 	 parent::setcomment($row["comment"]); 


 	 	 parent::setCreationDate($row["CreationDate"]); 


 	 	 parent::settireId($row["tireId"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>