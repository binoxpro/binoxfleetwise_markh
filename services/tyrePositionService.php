<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 
 *
 * @author plussoft
 * 
 */
include_once '../model/tryePosition.php';

class TyrePositionService extends TyrePosition {
    
   
    
    function __construct() {
        parent::__construct();
     
               
    }


    public function save() {
       
            
            $builder=new InsertBuilder();
            $builder->setTable("tbl_tyre_position");
            $builder->addColumnAndData("positionDetails", parent::getPositionDetails());
            $this->con->setQuery(Director::buildSql($builder));
            $this->con->setSelect_query("select * from tbl_tyre_position where positionDetails='".parent::getPositionDetails()."'");
			if($this->con->sqlCount()<1){
            return $this->con->execute_query2($builder->getValues());
			}else{
				return array('msg'=>'The position already exist');
			}
            
       
       // parent::save();
    }



    public function update() {
      	 	$builder=new UpdateBuilder();
		 	$builder->setTable("tbl_tyre_position");
            $builder->addColumnAndData("positionDetails", parent::getPositionDetails());
			$builder->setCriteria("where Id='".parent::getId()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }


	public function view() {
       $sql="select * from tbl_tyre_position where isActive=true";
	   return $this->con->getResultSet($sql);
    }
	//the method is litte in complete
	public function searchFilter(){
		$para1=parent::getNumberPlate()==NULL?'null':"'".parent::getNumberPlate()."'";
		$para2=parent::getModel()==NULL?'null':"'".parent::getModel()."'";
		$para3=parent::getYom()==NULL?'null':"'".parent::getYom()."'";
		$sql="select numberPlate,model,yom,reg_date from tbl_track where numberPlate=COALESCE(".$para1.",numberPlate) AND model=COALESCE(".$para2.",model) AND yom=COALESCE(".$para3.",yom) ";
		//echo $sql;
		return $this->con->getResultSet($sql);
	}
	public function delete(){
			$builder=new UpdateBuilder();
		 	$builder->setTable("tbl_tyre_position");
            $builder->addColumnAndData("isActive", parent::getIsActive());
			$builder->setCriteria("where Id='".parent::getId()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
	}
    public function view_query($sql) {
        parent::view_query($sql);
    }
	
	public function returnPosition($regNo) {
		$id=-1;
       $sql="select id from tbl_tyre_position where regNo='$regNo'";
	   //$data=array();
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   //$data2=array();
		   $id=$row['id'];
		   
	   }
	   return $id;
    }
	
    //put your code here
}
?>