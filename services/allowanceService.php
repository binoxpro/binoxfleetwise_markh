<?php 
 require_once('../model/allowance.php');
 class allowanceService extends allowance{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblallowance');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('allowanceName',parent::getallowanceName());
 		 	 $builder->addColumnAndData('accountCode',parent::getaccountCode());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblallowance');

 	 	 if(!is_null(parent::getallowanceName())){
$builder->addColumnAndData('allowanceName',parent::getallowanceName()); 
}

 	 	 if(!is_null(parent::getaccountCode())){
$builder->addColumnAndData('accountCode',parent::getaccountCode()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tblallowance";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  tblallowance";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select a.*,(select coa.AccountName from chartofaccounts coa where coa.AccountCode=a.accountCode ) accountName from  tblallowance a limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblallowance');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tblallowance  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setallowanceName($row['allowanceName']);
	 	parent::setaccountCode($row['accountCode']);
	 	parent::setisActive($row['isActive']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>