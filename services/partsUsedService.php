<?php
include_once '../model/partsUsed.php';
class PartsUsedService extends PartsUsed
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblpartsused");
		
		$builder->addColumnAndData("partusedId", parent::getpartused());
		if(parent::getquantity()!=""){
		$builder->addColumnAndData("quantity", parent::getquantity());
		}
		
		if(parent::getnumberPlate()!=""){
			$builder->addColumnAndData("numberplate", parent::getnumberPlate());
		}
		
		$builder->addColumnAndData("cost", parent::getcost());
		if(parent::getjobcardId()!="" || !is_null(parent::getjobcardId()))
		{
		$builder->addColumnAndData("jobcardId", parent::getjobcardId());
		}
		
		if(parent::getDateofRepair()!="")
		{
		$builder->addColumnAndData("dateofrepair", parent::getDateofRepair());
		}
		
		if(parent::gettypeofRepair()!="")
		{
		$builder->addColumnAndData("typeofrepair", parent::gettypeofRepair());
		}
		if(!is_null(parent::getsupplierId()))
		{
			$builder->addColumnAndData("supplierId", parent::getsupplierId());
		}
		if(!is_null(parent::getsupplied()))
		{
			$builder->addColumnAndData("supplied",parent::getsupplied());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		 $this->con->execute_query2($builder->getValues());
		 return $this->con->getId();
		// get the part used id.
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblpartsused");
			$builder->addColumnAndData("partusedId", parent::getpartused());
    		if(!is_null(parent::getquantity()))
			{
    			$builder->addColumnAndData("quantity", parent::getquantity());
    		}
    		if(!is_null(parent::getnumberPlate()))
			{
    			$builder->addColumnAndData("numberplate", parent::getnumberPlate());
    		}
    			$builder->addColumnAndData("cost", parent::getcost());
    		if(!is_null(parent::getjobcardId()))
			{
    			$builder->addColumnAndData("jobcardId", parent::getjobcardId());
    		}
    		
    		if(!is_null(parent::getDateofRepair())){
    		$builder->addColumnAndData("dateofrepair", parent::getDateofRepair());
    		}
    		
    		if(!is_null(parent::gettypeofRepair()))
			{
    			$builder->addColumnAndData("typeofrepair", parent::gettypeofRepair());
    		}
			if(!is_null(parent::getsupplierId()))
			{
				$builder->addColumnAndData("supplierId", parent::getsupplierId());
			}
			if(!is_null(parent::getsupplied()))
			{
				$builder->addColumnAndData("supplied",parent::getsupplied());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }
	public function find(){
		$sql="select * from  tblpartsused  where id='".parent::getid()."'";
		foreach($this->con->getResultSet($sql) as $row)
		{
			parent::setid($row['id']);
			parent::setnumberPlate($row['numberplate']);
			parent::setpartused($row['partusedId']);
			parent::setquantity($row['quantity']);
			parent::setcost($row['cost']);
			parent::setjobcardId($row['jobcardId']);
			parent::setDateOfRepair($row['dateofrepair']);
			parent::setTypeOfRepair($row['typeofrepair']);
			parent::setsupplierId($row['supplierId']);
			parent::setsupplied($row['supplied']);
		}	 	}

	public function view()
	{
		//change the view to
       $sql="select (case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )ELSE (select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  ) END )regNo,p.partUsed,p.quantity,p.cost,p.jobcardId,p.id,p.dateofrepair,p.typeofrepair,p.numberplate from tblpartsused p ";
		$data=array();
		$total=0;
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2['regNo']=$row['regNo'];
		   $data2['partUsed']=$row['partUsed'];
		   $data2['quantity']=$row['quantity'];
		   
		   $data2['cost']=number_format(intval($row['cost']),0);
		   $data2['jobcardId']=$row['jobcardId'];
		   $data2['id']=$row['id'];
		   $data2['dateofrepair']=$row['dateofrepair'];
		   $data2['typeofrepair']=$row['typeofrepair'];
		   array_push($data,$data2);
		   $total=$total+$row['cost'];
		   
	   }
	   		$data2=array();
		   $data2['regNo']="TOTAL";
		   $data2['partUsed']="";
		   $data2['quantity']="";
		   $data2['cost']=number_format($total,0)."UGX";
		   $data2['jobcardId']="";
		   $data2['id']="";
		   $data2['dateofrepair']="";
		   $data2['typeofrepair']="";
		   array_push($data,$data2);
	       return $data;
	   
    }
	
	public function viewVehicle() {

		/*$sql="select
(
case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )
ELSE
(select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  )
END
)regNo,
p.partUsedId,
sp.itemName,
sp.partNo partNumber,
p.quantity,
p.cost,
p.jobcardId,
p.id,
p.dateofrepair,
p.typeofrepair,
p.numberplate,
p.supplied,
pi.id as prfItemid,
pi.itemDescription,
pi.quantity,
pi.uomId,
pi.quantity as quantity,
pi.status prfistatus,
(select case when sum(prfi.quantityIssued) is null then 0 else sum(prfi.quantityIssued) end  from prfitem prfi where prfi.itemId=p.id) issuedQty
from tblpartsused p inner join items sp on p.partUsedId=sp.id inner join prfitem pi on p.id=pi.itemId";*/


       $sql="select (case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )ELSE (select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  ) END )regNo,p.partUsed,p.quantity,p.cost,p.jobcardId,p.id,p.dateofrepair,p.typeofrepair,p.numberplate from tblpartsused p where p.numberplate='".parent::getnumberPlate()."'";
		$data=array();
		$total=0;
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2['regNo']=$row['regNo'];
		   $data2['partUsed']=$row['partUsed'];
		   $data2['quantity']=$row['quantity'];
		   
		   $data2['cost']=number_format(intval($row['cost']),0);
		   $data2['jobcardId']=$row['jobcardId'];
		   $data2['id']=$row['id'];
		   $data2['dateofrepair']=$row['dateofrepair'];
		   $data2['typeofrepair']=$row['typeofrepair'];
		   array_push($data,$data2);
		   $total=$total+$row['cost'];
		   
	   }
	   		$data2=array();
		   $data2['regNo']="TOTAL";
		   $data2['partUsed']="";
		   $data2['quantity']="";
		   $data2['cost']=number_format($total,0)."UGX";
		   $data2['jobcardId']="";
		   $data2['id']="";
		   $data2['dateofrepair']="";
		   $data2['typeofrepair']="";
		   array_push($data,$data2);
	   return $data;
	   
    }
    
    public function viewJson() {

		$sql="select 
(
case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )
ELSE 
(select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  ) 
END 
)regNo,
p.partUsedId,
sp.itemName,
sp.partNo partNumber,
p.quantity,
p.cost,
p.jobcardId,
p.id,
p.dateofrepair,
p.typeofrepair,
p.numberplate,
p.supplied,
pi.id as prfItemid,
pi.itemDescription,
pi.quantity,
pi.uomId,
pi.quantity as prfiquantity,
pi.status prfistatus,
uom.uom,
pi.prfid partRequest,
(select case when sum(prfi.quantityIssued) is null then 0 else sum(prfi.quantityIssued) end  from prfitem prfi where prfi.itemId=p.id) issuedQty 
from tblpartsused p inner join items sp on p.partUsedId=sp.id inner join prfitem pi on p.id=pi.itemId inner join unitofmeasure uom on pi.uomId=uom.id where p.jobcardId='".parent::getjobcardId()."'";

      // $sql="select (case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )ELSE (select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  ) END )regNo,p.partUsedId,sp.itemName,sp.partNo partNumber,p.quantity,p.cost,p.jobcardId,p.id,p.dateofrepair,p.typeofrepair,p.numberplate,p.supplied,(select sum(prfi.quantityIssued) from prfitem prfi where prfi.itemId=p.id) issuedQty from tblpartsused p inner join items sp on p.partUsedId=sp.id  where p.jobcardId='".parent::getjobcardId()."'";
		$data=array();
		$total=0;
	   foreach( $this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
           $data2['id']=$row['id'];
		   $data2['regNo']=$row['regNo'];
		   $data2['partUsedId']=$row['partUsedId'];
		   $data2['partName']=$row['itemName'];
		   $data2['partNumber']=$row['partNumber'];
		   $data2['quantity']=$row['quantity'];
		   $data2['cost']=number_format(intval($row['cost']),0);
		   $data2['jobcardId']=$row['jobcardId'];
		   $data2['id']=$row['id'];
		   $data2['dateofrepair']=$row['dateofrepair'];
		   $data2['typeofrepair']=$row['typeofrepair'];
		   $data2['supplierName']='';
		   $data2['supplierId']='';
		   $data2['supplied']=$row['supplied'];
		   $data2['total']='';
		   $data2['uomId']=$row['uomId'];
		   $data2['uom']=$row['uom'];
           $data2['issuedQty']=$row['issuedQty'];
           $data2['prfitemId']=$row['prfItemid'];
           $data2['itemDescription']=$row['itemDescription'];
           $data2['prfiquantity']=$row['prfiquantity'];
           $data2['prfiStatus']=$row['prfistatus'];
           $data2['partRequest']=$row['partRequest'];
		   array_push($data,$data2);
		   //$total=$total+$row['total'];
		   
	   }
	   		$data2=array();
		   $data2['id']="";
		   $data2['regNo']="TOTAL";
		   $data2['partUsed']="";
		   $data2['quantity']="TOTAL";
		   $data2['total']=number_format($total,0);
		   $data2['jobcardId']="";
		   $data2['id']="";
		   $data2['dateofrepair']="";
		   $data2['typeofrepair']="";
		   array_push($data,$data2);
		return $data;
	   
    }
    
    
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblpartsused");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
