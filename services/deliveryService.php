<?php
include_once '../model/delivery.php';
class DeliveryService extends Delivery
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tbldelivery");
		if(parent::getDeliveryDate()!=NULL){
		$builder->addColumnAndData("DeliveryDate", parent::getDeliveryDate());
		}
		if(parent::getDeliveryTime()!=NULL){
		$builder->addColumnAndData("DeliveryTime", parent::getDeliveryTime());
		}
		if(parent::getAllocationId()!=NULL){
		$builder->addColumnAndData("orderId",parent::getAllocationId());
		}
		
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }
    public function update() 
	{
      	 	$builder=new UpdateBuilder();
			$builder->setTable("tbldelivery");
			if(parent::getDeliveryDate()!=NULL){
			$builder->addColumnAndData("DeliveryDate", parent::getDeliveryDate());
			}
			if(parent::getDeliveryTime()!=NULL){
			$builder->addColumnAndData("DeliveryTime", parent::getDeliveryTime());
			}
			if(parent::getAllocationId()!=NULL){
			$builder->addColumnAndData("orderId",parent::getAllocationId());
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
	   $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
       $qty=0;
       $today=date('Y-m-d');
       $data=array();
       $sqlCount="SELECT count(*) noRo FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner join tblallocation alloc on o.id=alloc.orderId inner join tbldispatcher disp on alloc.id=disp.AllocationId where os.name='Dispatched' or os.name='Delivered' order by o.orderDate desc";
    	//$date=date('y-m-d');
         foreach($this->con->getResultSet($sqlCount) as $rowCount)
        {
	       $data['result']=$rowCount['noRo'];
        }
       $sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId ,(select v.regNo from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) truckNo ,(select al.id from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) eid,disp.id did ,disp.timeoutDepo timeout,disp.timeinDepo timein FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner join tblallocation alloc on o.id=alloc.orderId inner join tbldispatcher disp on alloc.id=disp.AllocationId where o.orderDate='$today' and (os.name='Dispatched' or os.name='Delivered') order  by o.orderDate desc";
	   $items=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["mailingName"]=$row["mailingName"];
		   $data2["orderDate"]=$row["orderDate"];
		   $data2["promiseDate"]=$row["promiseDate"];
		   $data2["orderNumber"]=$row["orderNumber"];
		   $data2["ordertypeId"]=$row["ordertypeId"];
		   $data2["qtyOrder"]=number_format($row["qtyOrder"],0);
		   $data2["statusorderId"]=$row["statusorderId"];
		   $data2["orderstatusId"]=$row["orderstatusId"];
		   $data2["itemcodeId"]=$row["itemcodeId"];
		   $data2["truck"]=$row["truckNo"];
		   $data2["eid"]=$row["eid"];
		   $data2['did']=$row['did'];
		    $data2['timeout']=$row['timeout'];
			$data2['timein']=$row['timein'];
			//$duration=str(
			$data2['duration']=((strtotime($row['timeout'])-strtotime($row['timein']))/60)."min";
		   $eid=$data2["eid"];
		   $did=$data2['did'];
		   $sqlInner="select dy.* from tbldelivery dy where dy.orderId=$did";
		   $deliveryDate="";
		   $deliveryTime="";
		   $deid="";
		   $qty=$qty+$row["qtyOrder"];
		   foreach($this->con->getResultSet($sqlInner) as $row2)
		   {
			   $deliveryDate=$row2["Deliverydate"];
			   $deliveryTime=$row2["Deliverytime"];
			   //$allocationId=$row2["AllocationId"];
			   $deid=$row2['id'];
			   
		   }
		   	$data2['deliveryDate']=$deliveryDate;
			$data2['deliveryTime']=$deliveryTime;
			//$data2['']=$dispatcher;
			//$data2['allocationId']=$allocationId; 
			$data2['deid']=$deid; 
		   
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($items,$data2);
	   }
	   $data3=array();
	   
	   $data2=array();
	  $data2["ordertypeId"]="Total";
	   $data2["qtyOrder"]=number_format($qty,0);
	  // $data3['footer']=$data2;
	   array_push($items,$data2);
	   //$data['results']="";
	   $data['rows']=$items;
	   return $data;
   
    
    }
	
	public function viewRange($startDate,$endDate) {
       $qty=0;
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
       $qty=0;
       $data=array();
       $sqlCount="SELECT count(*) noRo FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner join tblallocation alloc on o.id=alloc.orderId inner join tbldispatcher disp on alloc.id=disp.AllocationId where (os.name='Dispatched' or os.name='Delivered') and o.promiseDate between '$startDate' and '$endDate' order by o.orderDate desc";
    	//$date=date('y-m-d');
         foreach($this->con->getResultSet($sqlCount) as $rowCount)
        {
	       $data['result']=$rowCount['noRo'];
        }
       
       
       
    	//$date=date('y-m-d');
       $sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId ,(select v.regNo from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) truckNo ,(select al.id from tblallocation al inner join tblvehicle v on al.truckId=v.id where al.orderId=o.id) eid,disp.id did ,disp.timeoutDepo timeout,disp.timeinDepo timein FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner join tblallocation alloc on o.id=alloc.orderId inner join tbldispatcher disp on alloc.id=disp.AllocationId where (os.name='Dispatched' or os.name='Delivered') and o.promiseDate between '$startDate' and '$endDate' order by o.orderDate desc";
	   $items=array();
	   foreach($this->con->getResultSet($sql) as $row)
	   {
		   $data2=array();
		   $data2["id"]=$row["id"];
		   $data2["mailingName"]=$row["mailingName"];
		   $data2["orderDate"]=$row["orderDate"];
		   $data2["promiseDate"]=$row["promiseDate"];
		   $data2["orderNumber"]=$row["orderNumber"];
		   $data2["ordertypeId"]=$row["ordertypeId"];
		   $data2["qtyOrder"]=number_format($row["qtyOrder"],0);
		   $data2["statusorderId"]=$row["statusorderId"];
		   $data2["orderstatusId"]=$row["orderstatusId"];
		   $data2["itemcodeId"]=$row["itemcodeId"];
		   $data2["truck"]=$row["truckNo"];
		   $data2["eid"]=$row["eid"];
		   $data2['did']=$row['did'];
		    $data2['timeout']=$row['timeout'];
			$data2['timein']=$row['timein'];
			//$duration=str(
			$data2['duration']=((strtotime($row['timeout'])-strtotime($row['timein']))/60)."min";
		   $eid=$data2["eid"];
		   $did=$data2['did'];
		   $sqlInner="select dy.* from tbldelivery dy where dy.orderId=$did";
		   $deliveryDate="";
		   $deliveryTime="";
		   $deid="";
		   $qty=$qty+$row["qtyOrder"];
		   foreach($this->con->getResultSet($sqlInner) as $row2)
		   {
			   $deliveryDate=$row2["Deliverydate"];
			   $deliveryTime=$row2["Deliverytime"];
			   //$allocationId=$row2["AllocationId"];
			   $deid=$row2['id'];
			   
		   }
		   	$data2['deliveryDate']=$deliveryDate;
			$data2['deliveryTime']=$deliveryTime;
			//$data2['']=$dispatcher;
			//$data2['allocationId']=$allocationId; 
			$data2['deid']=$deid; 
		   
		   //$data2["capacty"]=$row["Capacty"];
		   array_push($items,$data2);
	   }
	   $data3=array();
	   
	   $data2=array();
	   $data2["ordertypeId"]="Total";
	   $data2["qtyOrder"]=number_format($qty,0);
	  // $data3['footer']=$data2;
	   array_push($items,$data2);
	   //$data['results']="";
	   $data['rows']=$items;
	   return $data;
   
    
    }
	
	
	
	public function dateDiff($starttime,$endtime)
	{
		$startPeriod=explode(":",$starttime);
		$endPeriod=explode(":",$endtime);
		$hr=0;
		$min=0;
		$hr=intval($endPeriod[0])-intval($startPeriod);
	}
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tbldelivery");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
