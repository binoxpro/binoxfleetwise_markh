<?php 
 require_once('../model/supplier.php');
 class supplierService extends supplier{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblsupplier');
	 	     $builder->addColumnAndData('supplierCode',parent::getsupplierCode());
 		 	 $builder->addColumnAndData('supplierName',parent::getsupplierName());
 		 	 $builder->addColumnAndData('address',parent::getaddress());
 		 	 $builder->addColumnAndData('paymentTerm',parent::getpaymentTerm());
 		 	 $builder->addColumnAndData('accountNumber',parent::getaccountNumber());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsupplier');

 	 	 if(!is_null(parent::getsupplierName())){
$builder->addColumnAndData('supplierName',parent::getsupplierName()); 
}

 	 	 if(!is_null(parent::getaddress())){
$builder->addColumnAndData('address',parent::getaddress()); 
}

 	 	 if(!is_null(parent::getpaymentTerm())){
$builder->addColumnAndData('paymentTerm',parent::getpaymentTerm()); 
}

 	 	 if(!is_null(parent::getaccountNumber())){
$builder->addColumnAndData('accountNumber',parent::getaccountNumber()); 
}
$builder->setCriteria("where supplierCode='".parent::getsupplierCode()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view()
	 {

		 $page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		 $rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		 $offset = ($page-1)*$rows;
		 $sql="select * from  tblsupplier";
		 $this->con->setSelect_query($sql);
		 $data2=array();
		 $data=array();
		 $data["total"]=$this->con->sqlCount();
		 $sql="select * from  tblsupplier limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		 }
		 $data["rows"]=$data2;
		 return $data;
	 	//$sql="select * from  tblsupplier";
		//return $this->con->getResultSet($sql);
 	 }

	 public function viewComboBox()
	 {
		 $sql="select * from  tblsupplier";
		 return $this->con->getResultSet($sql);
	 }

 	 public function delete()
	 {
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsupplier');
	 	 $builder->setCriteria("where supplierCode='".parent::getsupplierCode()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblsupplier where supplierCode='".parent::getsupplierCode()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setsupplierName($row["supplierName"]); 


 	 	 parent::setaddress($row["address"]); 


 	 	 parent::setpaymentTerm($row["paymentTerm"]); 


 	 	 parent::setaccountNumber($row["accountNumber"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>