<?php 
 require_once('../model/tire.php');
 class tireService extends tire{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbltire');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('serialNumber',parent::getserialNumber());
 		 	 $builder->addColumnAndData('tireBand',parent::gettireBand());
 		 	 $builder->addColumnAndData('tirePatternId',parent::gettirePatternId());
 		 	 $builder->addColumnAndData('tireSizeId',parent::gettireSizeId());
 		 	 $builder->addColumnAndData('retreadDepth',parent::getretreadDepth());
 		 	 $builder->addColumnAndData('maxmiumPressure',parent::getmaxmiumPressure());
 		 	 $builder->addColumnAndData('tireType',parent::gettireType());
 		 	 $builder->addColumnAndData('cost',parent::getcost());
 		 	 $builder->addColumnAndData('kms',parent::getkms());
 		 	 $builder->addColumnAndData('status',parent::getstatus());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
             $builder->addColumnAndData('parentId',parent::getParentId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
            $this->con->execute_query2($builder->getValues());
              return $this->con->getId();
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbltire');

 	 	 if(!is_null(parent::getserialNumber())){
$builder->addColumnAndData('serialNumber',parent::getserialNumber()); 
}

 	 	 if(!is_null(parent::gettireBand())){
$builder->addColumnAndData('tireBand',parent::gettireBand()); 
}

 	 	 if(!is_null(parent::gettirePatternId())){
$builder->addColumnAndData('tirePatternId',parent::gettirePatternId()); 
}

 	 	 if(!is_null(parent::gettireSizeId())){
$builder->addColumnAndData('tireSizeId',parent::gettireSizeId()); 
}

 	 	 if(!is_null(parent::getretreadDepth())){
$builder->addColumnAndData('retreadDepth',parent::getretreadDepth()); 
}

 	 	 if(!is_null(parent::getmaxmiumPressure())){
$builder->addColumnAndData('maxmiumPressure',parent::getmaxmiumPressure()); 
}

 	 	 if(!is_null(parent::gettireType())){
$builder->addColumnAndData('tireType',parent::gettireType()); 
}

 	 	 if(!is_null(parent::getcost())){
$builder->addColumnAndData('cost',parent::getcost()); 
}

 	 	 if(!is_null(parent::getkms())){
$builder->addColumnAndData('kms',parent::getkms()); 
}

 	 	 if(!is_null(parent::getstatus())){
$builder->addColumnAndData('status',parent::getstatus()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
if(!is_null(parent::getParentId())){
$builder->addColumnAndData('parentId',parent::getParentId());
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 
 public function viewCombox(){
    $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t";
	 return $this->con->getResultSet($sql);
    }
    public function viewCombox2($x){
    $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t where t.status='$x'";
	 return $this->con->getResultSet($sql);
    }


 	 public function view(){
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	   $offset = ($page-1)*$rows;
 
          $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t";
	 $this->con->setSelect_query($sql);
     $data2=array();
     $data=array();
     $data['total']=$this->con->sqlCount();
     $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t limit $offset,$rows";
     foreach($this->con->getResultSet($sql) as $row)
     {
        $td=$row['retreadDepth'];
        $td2=$this->tireTreadDepth($row['id']);
        if($td2!=0)
        {
           $row['retreadDepth']=$td2; 
        }
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
 	 }
     public function viewStatus($status){
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	   $offset = ($page-1)*$rows;
	 	 $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t where t.status='$status'";
	 
     $this->con->setSelect_query($sql);
     $data2=array();
     $data=array();
     $data['total']=$this->con->sqlCount();
     $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t where t.status='$status' limit $offset,$rows";
	  foreach($this->con->getResultSet($sql) as $row)
     {
        $td=$row['retreadDepth'];
        $td2=$this->tireTreadDepth($row['id']);
        if($td2!=0)
        {
           $row['retreadDepth']=$td2; 
        }
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
 	 }
     public function viewStatusx($status){
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	   $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	   $offset = ($page-1)*$rows;
	 	 $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t where t.tireType='$status'";
	 $this->con->setSelect_query($sql);
     $data2=array();
     $data=array();
     $data['total']=$this->con->sqlCount();
     
     $sql="select t.*,(select name from tbltirepattern tp where tp.id=t.tirePatternId ) pattern,(select name from tbltiresize ts where ts.id=t.tireSizeId) tireSize from  tbltire t where t.tireType='$status' limit $offset,$rows";
	
     foreach($this->con->getResultSet($sql) as $row)
     {
        $td=$row['retreadDepth'];
        $td2=$this->tireTreadDepth($row['id']);
        if($td2!=0)
        {
           $row['retreadDepth']=$td2; 
        }
        array_push($data2,$row);
     }
     $data['rows']=$data2;
     return $data;
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbltire');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tbltire where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

			 parent::setserialNumber($row["serialNumber"]); 
			 parent::settireBand($row["tireBand"]); 
			 parent::settirePatternId($row["tirePatternId"]); 
			 parent::settireSizeId($row["tireSizeId"]); 
			 parent::setretreadDepth($row["retreadDepth"]); 
			 parent::setmaxmiumPressure($row["maxmiumPressure"]); 
			 parent::settireType($row["tireType"]); 
			 parent::setcost($row["cost"]); 
			 parent::setkms($row["kms"]); 
			 parent::setstatus($row["status"]); 
			 parent::setParentId($row["parentId"]);	
			 parent::setisActive($row["isActive"]); 

	 	} 
 }
 
 public function tireExistance($serialNumber){
        $countNo=0;
	 	 $sql="select * from  tbltire where serialNumber='$serialNumber'";
	 	 $this->con->setSelect_query($sql);
         $countNo=$this->con->sqlCount();
         return $countNo>0?true:false;
 	 	 

	 	
 }
 
 public function tireTreadDepth($tireId){
        $td=0;
	 	 $sql="select tid.treadDepth from tbltireinspectiondetails tid inner join tbltirehold th on th.id=tid.tireHoldId where th.tireId='$tireId'";
	 	 foreach($this->con->getResultSet($sql) as $row)
         {
            $td=$row['treadDepth'];
         }
         return $td;
 	 	 

	 	
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>