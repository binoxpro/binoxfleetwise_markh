<?php 
 require_once('../model/exceptionLog.php');
 class exceptionLogService extends exceptionLog{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblexceptionlog');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('message',parent::getmessage());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblexceptionlog');

 	 	 if(!is_null(parent::getmessage())){
$builder->addColumnAndData('message',parent::getmessage()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblexceptionlog";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblexceptionlog');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblexceptionlog where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setmessage($row["message"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>