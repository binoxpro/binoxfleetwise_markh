<?php 
 require_once('../model/retreated.php');
 class retreatedService extends retreated{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblretreated');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('stockId',parent::getstockId());
 		 	 $builder->addColumnAndData('kmDone',parent::getkmDone());
 		 	 $builder->addColumnAndData('expectedKm',parent::getexpectedKm());
 		 	 $builder->addColumnAndData('cost',parent::getcost());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblretreated');

 	 	 if(!is_null(parent::getstockId())){
$builder->addColumnAndData('stockId',parent::getstockId()); 
}

 	 	 if(!is_null(parent::getkmDone())){
$builder->addColumnAndData('kmDone',parent::getkmDone()); 
}

 	 	 if(!is_null(parent::getexpectedKm())){
$builder->addColumnAndData('expectedKm',parent::getexpectedKm()); 
}

 	 	 if(!is_null(parent::getcost())){
$builder->addColumnAndData('cost',parent::getcost()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblretreated";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblretreated');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblretreated where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setstockId($row["stockId"]); 


 	 	 parent::setkmDone($row["kmDone"]); 


 	 	 parent::setexpectedKm($row["expectedKm"]); 


 	 	 parent::setcost($row["cost"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>