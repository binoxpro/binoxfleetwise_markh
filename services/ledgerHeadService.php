<?php 
 require_once('../model/ledgerHead.php');
 class ledgerHeadService extends ledgerHead{
	 	 public function save(){
			 $builder=new InsertBuilder();
			 $builder->setTable('ledgehead');
			 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('transactionDate',parent::gettransactionDate());
 		 	 $builder->addColumnAndData('monthId',parent::getmonthId());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('ipAddress',parent::getipAddress());
 		 	 $builder->addColumnAndData('userId',parent::getuserId());
 		 	 $builder->addColumnAndData('postingDate',parent::getpostingDate());
 		 	 $builder->addColumnAndData('postingTime',parent::getpostingTime());
 		 	 $this->con->setQuery(Director::buildSql($builder));
			 $this->con->execute_query2($builder->getValues());
			 parent::setid($this->con->getId());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('ledgehead');

 	 	 if(!is_null(parent::gettransactionDate())){
$builder->addColumnAndData('transactionDate',parent::gettransactionDate()); 
}

 	 	 if(!is_null(parent::getmonthId())){
$builder->addColumnAndData('monthId',parent::getmonthId()); 
}

 	 	 if(!is_null(parent::getamount())){
$builder->addColumnAndData('amount',parent::getamount()); 
}

 	 	 if(!is_null(parent::getipAddress())){
$builder->addColumnAndData('ipAddress',parent::getipAddress()); 
}

 	 	 if(!is_null(parent::getuserId())){
$builder->addColumnAndData('userId',parent::getuserId()); 
}

 	 	 if(!is_null(parent::getpostingDate())){
$builder->addColumnAndData('postingDate',parent::getpostingDate()); 
}

 	 	 if(!is_null(parent::getpostingTime())){
$builder->addColumnAndData('postingTime',parent::getpostingTime()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  ledgehead";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  ledgehead";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  ledgehead limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('ledgehead');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  ledgehead  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::settransactionDate($row['transactionDate']);
	 	parent::setmonthId($row['monthId']);
	 	parent::setamount($row['amount']);
	 	parent::setipAddress($row['ipAddress']);
	 	parent::setuserId($row['userId']);
	 	parent::setpostingDate($row['postingDate']);
	 	parent::setpostingTime($row['postingTime']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>