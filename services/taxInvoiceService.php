<?php 
 require_once('../model/taxInvoice.php');
 class taxInvoiceService extends taxInvoice{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('taxinvoice');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('dueDate',parent::getdueDate());
 		 	 $builder->addColumnAndData('parentId',parent::getparentId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('taxinvoice');

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getdueDate())){
$builder->addColumnAndData('dueDate',parent::getdueDate()); 
}

 	 	 if(!is_null(parent::getparentId())){
$builder->addColumnAndData('parentId',parent::getparentId()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  taxinvoice";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  taxinvoice";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  taxinvoice limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('taxinvoice');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 $sql="select * from  taxinvoice  where id='".parent::getid()."'";
	 foreach($this->con->getResultSet($sql) as $row)
	 {
		 parent::setid($row['id']);
		 parent::setcreationDate($row['creationDate']);
		 parent::setdueDate($row['dueDate']);
		 parent::setparentId($row['parentId']);
	 }	 	}
	 public function findWithParent(){
		 $sql="select * from  taxinvoice  where parentId='".parent::getparentId()."'";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
			 parent::setcreationDate($row['creationDate']);
			 parent::setdueDate($row['dueDate']);
			 parent::setparentId($row['parentId']);
		 }	 	}
	 public function invoiceNumbering()
	 {
		 $invoiceStart=0;
		 $sql="select * from taxinvoice ";
		 $this->con->setSelect_query($sql);
		 return intval($this->con->sqlCount())+1+$invoiceStart;


	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>