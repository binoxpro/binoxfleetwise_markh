<?php
session_start();
 require_once('../model/processhistory.php');
 require_once ('wpfService.php');
 class processhistoryService extends processhistory{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('processhistory');
	 	 //$builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('processId',parent::getprocessId());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('createdBy',parent::getcreatedBy());
 		 	 $builder->addColumnAndData('completedDate',parent::getcompletedDate());
 		 	 $builder->addColumnAndData('completedBy',parent::getcompletedBy());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	     $this->con->execute_query2($builder->getValues());
	 	     parent::setid($this->con->getId());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('processhistory');

 	 	 if(!is_null(parent::getprocessId())){
        $builder->addColumnAndData('processId',parent::getprocessId());
        }

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getcreatedBy())){
$builder->addColumnAndData('createdBy',parent::getcreatedBy()); 
}

 	 	 if(!is_null(parent::getcompletedDate())){
$builder->addColumnAndData('completedDate',parent::getcompletedDate()); 
}

 	 	 if(!is_null(parent::getcompletedBy())){
$builder->addColumnAndData('completedBy',parent::getcompletedBy()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  processhistory";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  processhistory";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  processhistory limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('processhistory');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  processhistory  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setprocessId($row['processId']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setcreatedBy($row['createdBy']);
	 	parent::setcompletedDate($row['completedDate']);
	 	parent::setcompletedBy($row['completedBy']);
	 	parent::setisActive($row['isActive']);
}	 	}


     public function findProcess($processId)
     {
         $sql="select * from  processhistory  where id='".$processId."'";
         foreach($this->con->getResultSet($sql) as $row)
         {
             parent::setid($row['id']);
             parent::setprocessId($row['processId']);
             parent::setcreationDate($row['creationDate']);
             parent::setcreatedBy($row['createdBy']);
             parent::setcompletedDate($row['completedDate']);
             parent::setcompletedBy($row['completedBy']);
             parent::setisActive($row['isActive']);
         }
 	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }

 	 public function startProcess($section)
     {
         $wfp=new wpfService();
         $wfp->setsection($section);
         $wfp->getStart();

         //

         parent::setprocessId($wfp->getprocessId());
         parent::setcreationDate(date('Y-m-d'));
         parent::setcreatedBy($_SESSION['username']);
         //parent::setcompletedDate($row['completedDate']);
         //parent::setcompletedBy($row['completedBy']);
         parent::setisActive(true);
         $this->save();


     }

     public function nextProcess($section,$processId,$passed,$id=0)
     {
         $wfp=new wpfService();
         $wfp->setsection($section);
         $wfp->setprocessId($processId);
         $wfp->getNext();
         //complete previous process.
         $this->findProcess($id);
         parent::setcompletedDate(date('Y-m-d'));
         parent::setcompletedBy($_SESSION['username']);
         parent::setisActive(false);
         $this->update();

         //clear for the next process move:
         parent::setprocessId(null);
         parent::setcreationDate(null);
         parent::setcreatedBy(null);
         parent::setcompletedDate(null);
         parent::setcompletedBy(null);
         parent::setisActive(null);
        //move next
         if($passed)
         {
             if(!is_null($wfp->getonSuccess()))
             {
                 parent::setprocessId($wfp->getonSuccess());
                 parent::setcreationDate(date('Y-m-d'));
                 parent::setcreatedBy($_SESSION['username']);
                 //parent::setcompletedDate(null);
                 //parent::setcompletedBy(null);
                 parent::setisActive(true);
                 $this->save();

             }

         }else if(is_null($passed))
         {
            if(!is_null($wfp->getonSuccess()))
            {
                parent::setprocessId($wfp->getonSuccess());
                parent::setcreationDate(date('Y-m-d'));
                parent::setcreatedBy($_SESSION['username']);
                //parent::setcompletedDate(null);
                //parent::setcompletedBy(null);
                parent::setisActive(true);
                $this->save();
            }

            if(!is_null($wfp->getonFailure()))
            {
                parent::setprocessId($wfp->getonFailure());
                parent::setcreationDate(date('Y-m-d'));
                parent::setcreatedBy($_SESSION['username']);
                //parent::setcompletedDate(null);
                //parent::setcompletedBy(null);
                parent::setisActive(true);
               $this->save();
            }

         }else
         {
             if(!is_null($wfp->getonFailure())) {
                 parent::setprocessId($wfp->getonFailure());
                 parent::setcreationDate(date('Y-m-d'));
                 parent::setcreatedBy($_SESSION['username']);
                 //parent::setcompletedDate(null);
                 //parent::setcompletedBy(null);
                 parent::setisActive(true);
                 $this->save();
             }

         }



     }


 }
?>