<?php 
 require_once('../model/localpurchaseorder.php');
 class localpurchaseorderService extends localpurchaseorder{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tbllop');
	 	 $builder->addColumnAndData('id',parent::getid());
         $builder->addColumnAndData('supplieId',parent::getsupplieId());
         $builder->addColumnAndData('creationDate',parent::getcreationDate());
         $builder->addColumnAndData('createdBy',parent::getcreatedBy());
         $builder->addColumnAndData('prfId',parent::getprfId());
         $builder->addColumnAndData('isActive',parent::getisActive());
         $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tbllop');

 	 	 if(!is_null(parent::getsupplieId())){
$builder->addColumnAndData('supplieId',parent::getsupplieId()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}

 	 	 if(!is_null(parent::getcreatedBy())){
$builder->addColumnAndData('createdBy',parent::getcreatedBy()); 
}

 	 	 if(!is_null(parent::getprfId())){
$builder->addColumnAndData('prfId',parent::getprfId()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  tbllop";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="sselect lp.*,lp.id lopNo,lp.prfId prfIdNo,sp.supplierName,sp.address from  tbllop lp inner join tblsupplier sp on lp.supplieId=sp.supplierCode where lp.prfId='".parent::getprfId()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select lp.*,lp.id lopNo,lp.prfId prfIdNo,sp.supplierName,sp.address from  tbllop lp inner join tblsupplier sp on lp.supplieId=sp.supplierCode where lp.prfId='".parent::getprfId()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
        {
			 array_push($data2,$row);
		}
		$data["rows"]=$data2;
		 return $data;
 	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tbllop');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  tbllop  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setsupplieId($row['supplieId']);
	 	parent::setcreationDate($row['creationDate']);
	 	parent::setcreatedBy($row['createdBy']);
	 	parent::setprfId($row['prfId']);
	 	parent::setisActive($row['isActive']);
}	 	}
     public function generateNumber(){
         $sql="select * from  tbllop ";
         $this->con->setSelect_query($sql);
         return $this->con->sqlCount()+1;

 	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>