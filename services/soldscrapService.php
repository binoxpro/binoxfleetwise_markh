<?php 
 require_once('../model/soldscrap.php');
 class soldscrapService extends soldscrap{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblsoldscrap');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('scrapId',parent::getscrapId());
 		 	 $builder->addColumnAndData('amount',parent::getamount());
 		 	 $builder->addColumnAndData('supplier',parent::getsupplier());
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsoldscrap');

 	 	 if(!is_null(parent::getscrapId())){
$builder->addColumnAndData('scrapId',parent::getscrapId()); 
}

 	 	 if(!is_null(parent::getamount())){
$builder->addColumnAndData('amount',parent::getamount()); 
}

 	 	 if(!is_null(parent::getsupplier())){
$builder->addColumnAndData('supplier',parent::getsupplier()); 
}

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblsoldscrap";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsoldscrap');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 
	 	 public function getObject(){
	 	 $sql="select * from  tblsoldscrap where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row){

 	 	 parent::setscrapId($row["scrapId"]); 


 	 	 parent::setamount($row["amount"]); 


 	 	 parent::setsupplier($row["supplier"]); 


 	 	 parent::setcreationDate($row["creationDate"]); 

	 	} 
 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>