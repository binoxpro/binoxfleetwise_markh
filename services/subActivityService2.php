<?php 

 require_once('model/subActivity2.php');
 class subActivityService extends subActivity{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('tblsubactivity');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('name',parent::getname());
 		 	 $builder->addColumnAndData('link',parent::getlink());
 		 	 $builder->addColumnAndData('activityId',parent::getactivityId());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsubactivity');

 	 	 if(parent::getname()!=" "){
$builder->addColumnAndData('name',parent::getname()); }

 	 	 if(parent::getlink()!=" "){
$builder->addColumnAndData('link',parent::getlink()); }

 	 	 if(parent::getactivityId()!=" "){
$builder->addColumnAndData('activityId',parent::getactivityId()); }

 	 	 if(parent::getisActive()!=""){
$builder->addColumnAndData('isActive',parent::getisActive()); }
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	$sql="select sa.*,a.name nameActivity from  tblsubactivity sa inner join tblactivity a on sa.activityId=a.id";
	 	return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsubactivity');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
	 
	 public  function viewSideMenu(){
	   $userId=$_SESSION['userId'];
		 $sql="select * from tblactivity where isActive='1'";
		 //$str='<li><a href="admin.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>';
		 $str='<li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a><ul class="nav child_menu"><li><a href="index.html">Dashboard</a></li><li><a href="index2.html">Dashboard2</a></li><li><a href="index3.html">Dashboard3</a></li></ul></li>';
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 //check role
             $id=$row['id'];
			 $sqlInner2="select * from  tblsubactivity sa inner join tblrolemanagement rm on rm.roleId=sa.id where sa.activityId='$id' and sa.isActive='1' and rm.userId='$userId' ";
			 $this->con->setSelect_query($sqlInner2);
			 $nox=$this->con->sqlCount();
             if($nox>0)
             {
			 $str.= '<li>';
                            $str.='<a><i class="fa fa-folder-o"></i>'.$row['name'].'<span class="fa fa-chevron-down"></span></a>';
                            
             $str.='<ul class="nav child_menu">';
			 
			 $sqlInner="select * from  tblsubactivity sa inner join tblrolemanagement rm on rm.roleId=sa.id where sa.activityId='$id' and sa.isActive='1' and rm.userId='$userId' ";
			 $strInner="";
			 foreach($this->con->getResultSet($sqlInner) as $rowInner)
			 {
				$strInner.='<li><a href="?'.$rowInner['link'].'">'.$rowInner['name'].'</a></li>';
			 }
			 $strInner.='</ul></li>';
             
		 	$str.=$strInner;
            }
		 }
		 return $str."</li>";
	 	//$sql="select sa.*,a.name nameActivity from  tblsubactivity sa inner join tblactivity a on sa.activityId=a.id";
	 	//return $this->con->getResultSet($sql);
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>