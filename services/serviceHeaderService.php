<?php 
 require_once('../model/serviceHeader.php');
 class serviceHeaderService extends serviceHeader{
	 	 public function save(){
			 //deacvtive service schedule
			 $object=new serviceHeaderService();
			 $object->setvehicleId(parent::getvehicleId());
			 $object->get_serviceDeactived();
			$object->setisActive(false);
			 $object->update();
			//saving the save 
			 $builder=new InsertBuilder();
			 $builder->setTable('tblserviceheader');
 		 	 $builder->addColumnAndData('creationDate',parent::getcreationDate());
 		 	 $builder->addColumnAndData('serviceIntervalId',parent::getserviceIntervalId());
 		 	 $builder->addColumnAndData('kmReading',parent::getkmReading());
 		 	 $builder->addColumnAndData('nextReading',parent::getnextReading());
 		 	 $builder->addColumnAndData('reminderReading',parent::getreminderReading());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $builder->addColumnAndData('vehicleId',parent::getvehicleId());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	  	 $this->con->execute_query2($builder->getValues());
			parent::setid($this->con->getId());
		  	
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblserviceheader');

 	 	 if(!is_null(parent::getcreationDate())){
$builder->addColumnAndData('creationDate',parent::getcreationDate()); }

 	 	 if(!is_null(parent::getserviceIntervalId())){
$builder->addColumnAndData('serviceIntervalId',parent::getserviceIntervalId()); }

 	 	 if(!is_null(parent::getkmReading())){
$builder->addColumnAndData('kmReading',parent::getkmReading()); }

 	 	 if(!is_null(parent::getnextReading())){
$builder->addColumnAndData('nextReading',parent::getnextReading()); }

 	 	 if(!is_null(parent::getreminderReading())){
$builder->addColumnAndData('reminderReading',parent::getreminderReading()); }

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); }

 	 	 if(!is_null(parent::getvehicleId())){
$builder->addColumnAndData('vehicleId',parent::getvehicleId()); 
			}
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
	 	 	return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading,(sh.nextReading-od.reading) remaining from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId where sh.isActive ='1'  order by sh.id asc";
	 	return $this->con->getResultSet($sql);
 	 }
	 
	 public function viewVehicleOn(){
	 	 //$sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Current' else'Past' end status,od.reading currentReading from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id inner join tblodometer od on od.vehicleId=sh.vehicleId  where sh.vehicleId='".parent::getvehicleId()."' order by sh.id asc ";
	        $sql="select sh.*,st.serviceName, v.regNo,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine,case when sh.isActive ='1' then 'Service Due' else'Serviced' end status,od.reading currentReading,(sh.nextReading-od.reading) remaining from  tblserviceheader sh inner join tblserviceinterval si on sh.serviceintervalId=si.id inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehicle v on sh.vehicleId=v.id  inner join tblodometer od on od.vehicleId=sh.vehicleId where sh.isActive ='1' and sh.vehicleId='".parent::getvehicleId()."'  order by sh.id asc";
         return $this->con->getResultSet($sql);
 	 }
	 
	  public function getLatest(){
	 	 $sql="select *  from  tblserviceheader where vehicleId='".parent::getvehicleId()."' and IsActive='1'";
		 $latestService="No Service Record in service";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 $id=$row['serviceIntervalId'];
			 $sqlOut="select si.*,m.makeName,st.serviceName,(select serviceName from tblservicetype where id=si.nextServiceType) nextinLine from  tblserviceinterval si inner join tblservicetype st on si.serviceTypeId=st.id inner join tblvehiclemake m on si.modal=m.makeName  where si.id='$id'";
			 foreach( $this->con->getResultSet($sqlOut) as $rowOut)
			 {
				 $latestService=$rowOut['nextinLine'];
			 }
		 }
		 return $latestService;
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblserviceheader');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
	 
	 public function get_serviceDeactived()
	 {
		  $sql="select *  from  tblserviceheader where vehicleId='".parent::getvehicleId()."' and IsActive='1'";
		  foreach($this->con->getResultSet($sql) as $row)
		 {
			 parent::setid($row['id']);
		 }
		 
	 }
	 public function getServiceTagNo()
	 {
		 $sql="select *  from  tblserviceheader where vehicleId='".parent::getvehicleId()."'";
		 $this->con->setSelect_query($sql);
		 return $this->con->sqlCount();
	 }
	 public function getServiceTagNoDateRange($startDate,$endDate)
	 {
		 $sql="select *  from  tblserviceheader where vehicleId='".parent::getvehicleId()."' and creationDate '$startDate' and '$endDate'";
		 $this->con->setSelect_query($sql);
		 return $this->con->sqlCount();
	 }
 }
?>