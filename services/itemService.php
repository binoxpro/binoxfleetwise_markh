<?php 
 require_once('../model/item.php');
 class itemService extends item{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('items');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('partNo',parent::getpartNo());
 		 	 $builder->addColumnAndData('serialNo',parent::getserialNo());
 		 	 $builder->addColumnAndData('itemName',parent::getitemName());
 		 	 $builder->addColumnAndData('itemCategoryId',parent::getitemCategoryId());
 		 	 $builder->addColumnAndData('uomId',parent::getuomId());
 		 	 $builder->addColumnAndData('levelAlert',parent::getlevelAlert());
 		 	 $builder->addColumnAndData('bin',parent::getbin());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('items');

 	 	 if(!is_null(parent::getpartNo())){
$builder->addColumnAndData('partNo',parent::getpartNo()); 
}

 	 	 if(!is_null(parent::getserialNo())){
$builder->addColumnAndData('serialNo',parent::getserialNo()); 
}

 	 	 if(!is_null(parent::getitemName())){
$builder->addColumnAndData('itemName',parent::getitemName()); 
}

 	 	 if(!is_null(parent::getitemCategoryId())){
$builder->addColumnAndData('itemCategoryId',parent::getitemCategoryId()); 
}

 	 	 if(!is_null(parent::getuomId())){
$builder->addColumnAndData('uomId',parent::getuomId()); 
}

 	 	 if(!is_null(parent::getlevelAlert())){
$builder->addColumnAndData('levelAlert',parent::getlevelAlert()); 
}

 	 	 if(!is_null(parent::getbin())){
$builder->addColumnAndData('bin',parent::getbin()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select id,lower(trim(itemName)) itemName from  items";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select it.*,ic.itemCategory,uom.uom from  items it inner join itemcategory ic on it.itemCategoryId=ic.id inner join unitofmeasure uom on it.uomId=uom.id";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select it.*,ic.itemCategory,uom.uom from  items it inner join itemcategory ic on it.itemCategoryId=ic.id inner join unitofmeasure uom on it.uomId=uom.id limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
			{
			 array_push($data2,$row);
		}$data["rows"]=$data2;return $data; }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('items');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  items  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
                parent::setid($row['id']);
                parent::setpartNo($row['partNo']);
                parent::setserialNo($row['serialNo']);
                parent::setitemName($row['itemName']);
                parent::setitemCategoryId($row['itemCategoryId']);
                parent::setuomId($row['uomId']);
                parent::setlevelAlert($row['levelAlert']);
                parent::setbin($row['bin']);
                parent::setisActive($row['isActive']);
        }
 	 }

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>