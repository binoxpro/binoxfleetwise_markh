<?php 
 require_once('../model/systemUser.php');
require_once('auditTrailService.php');
 class systemUserService extends systemUser{
	 	 public function save(){
	 	 	$builder=new InsertBuilder();
	 	 	$builder->setTable('tblsystemuser');
	 	     $builder->addColumnAndData('id',$this->getGUID());
 		 	 $builder->addColumnAndData('firstName',parent::getfirstName());
 		 	 $builder->addColumnAndData('lastName',parent::getlastName());
 		 	 $builder->addColumnAndData('dob',parent::getdob());
 		 	 $builder->addColumnAndData('contact',parent::getcontact());
 		 	 $builder->addColumnAndData('email',parent::getemail());
 		 	 $builder->addColumnAndData('isActive',parent::getisActive());
 		 	 $builder->addColumnAndData('username',parent::getusername());
 		 	 if(!is_null(parent::getpassword()))
            {
                $builder->addColumnAndData('password',parent::getpassword()); 
            }
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('tblsystemuser');

 	 	 if(!is_null(parent::getfirstName())){
$builder->addColumnAndData('firstName',parent::getfirstName()); 
}

 	 	 if(!is_null(parent::getlastName())){
$builder->addColumnAndData('lastName',parent::getlastName()); 
}

 	 	 if(!is_null(parent::getdob())){
$builder->addColumnAndData('dob',parent::getdob()); 
}

 	 	 if(!is_null(parent::getcontact())){
$builder->addColumnAndData('contact',parent::getcontact()); 
}

 	 	 if(!is_null(parent::getemail())){
$builder->addColumnAndData('email',parent::getemail()); 
}

 	 	 if(!is_null(parent::getisActive())){
$builder->addColumnAndData('isActive',parent::getisActive()); 
}

 	 	 if(!is_null(parent::getusername())){
$builder->addColumnAndData('username',parent::getusername()); 
}

 	 	 if(!is_null(parent::getpassword())){
$builder->addColumnAndData('password',parent::getpassword()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function view(){
	 	 $sql="select * from  tblsystemuser";
	 return $this->con->getResultSet($sql);
 	 }

 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('tblsystemuser');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }

	 public function login()
	 {
		 parent::setid(null);
		 $sql="select * from  tblsystemuser where username='".parent::getusername()."' and password='".parent::getpassword()."' and isActive=1";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
				parent::setid($row['id']);
			 	parent::setfirstName($row['firstName']);
			 	parent::setlastName($row['lastName']);

			 	$auditTrail=new auditTrailService();
			 	$auditTrail->setcreationdate(date('Y-m-d'));
			 	$auditTrail->settimeCreation(date('H:i:s'));
			 	//$auditTrail->setIpadderess($_SERVER['REMOTE_ADDR']);
			 	$url=isset($_REQUEST['returnUrl'])?$_REQUEST['returnurl']:'admin.php';
			 	$auditTrail->seturl($url);
			 	$auditTrail->setuserId($row['id']);
			 	$auditTrail->save();


		 }
		 //echo parent::getfirstName();
//		 echo parent::getusername();
//		 echo parent::getpassword();
//		 echo parent::getid();
//		 echo "Text";
	 }
	 public function getGUID()
	 {
		 if (function_exists('com_create_guid'))
		 {
			 return com_create_guid();
		 }else
		 {
			 mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			 $charid = strtoupper(md5(uniqid(rand(), true)));
			 $hyphen = chr(45);// "-"
			 $uuid = chr(123)// "{"
				 .substr($charid, 0, 8).$hyphen
				 .substr($charid, 8, 4).$hyphen
				 .substr($charid,12, 4).$hyphen
				 .substr($charid,16, 4).$hyphen
				 .substr($charid,20,12)
				 .chr(125);// "}"
			 return $uuid;
		 }
	 }
 }
?>