<?php 
 require_once('../model/employeeBank.php');
 class employeeBankService extends employeeBank{
	 	 public function save(){
	 	 $builder=new InsertBuilder();
	 	 $builder->setTable('employeebank');
	 	 $builder->addColumnAndData('id',parent::getid());
 		 	 $builder->addColumnAndData('empNo',parent::getempNo());
 		 	 $builder->addColumnAndData('bank',parent::getbank());
 		 	 $builder->addColumnAndData('branch',parent::getbranch());
 		 	 $builder->addColumnAndData('accountName',parent::getaccountName());
 		 	 $builder->addColumnAndData('accountNumber',parent::getaccountNumber());
 		 	 $builder->addColumnAndData('nssfNumber',parent::getnssfNumber());
 		 	 $builder->addColumnAndData('Isactive',parent::getIsactive());
 		 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query2($builder->getValues());
 	  }
 
	 	 public function update(){
	 	 $builder=new UpdateBuilder();
	 	 $builder->setTable('employeebank');

 	 	 if(!is_null(parent::getempNo())){
$builder->addColumnAndData('empNo',parent::getempNo()); 
}

 	 	 if(!is_null(parent::getbank())){
$builder->addColumnAndData('bank',parent::getbank()); 
}

 	 	 if(!is_null(parent::getbranch())){
$builder->addColumnAndData('branch',parent::getbranch()); 
}

 	 	 if(!is_null(parent::getaccountName())){
$builder->addColumnAndData('accountName',parent::getaccountName()); 
}

 	 	 if(!is_null(parent::getaccountNumber())){
$builder->addColumnAndData('accountNumber',parent::getaccountNumber()); 
}

 	 	 if(!is_null(parent::getnssfNumber())){
$builder->addColumnAndData('nssfNumber',parent::getnssfNumber()); 
}

 	 	 if(!is_null(parent::getIsactive())){
$builder->addColumnAndData('Isactive',parent::getIsactive()); 
}
$builder->setCriteria("where id='".parent::getid()."'");
$this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 }
 

 	 public function viewConbox(){
	 	 $sql="select * from  employeebank";
	 return $this->con->getResultSet($sql);
 	 }

	 public function view(){
		$page = isset($_POST["page"]) ? intval($_POST["page"]) : 1;
		$rows = isset($_POST["rows"]) ? intval($_POST["rows"]) : 10;
		$offset = ($page-1)*$rows;
		 $sql="select * from  employeebank where empNo='".parent::getempNo()."'";
		$this->con->setSelect_query($sql);
		$data2=array();
		$data=array();
		$data["total"]=$this->con->sqlCount();
		$sql="select * from  employeebank where empNo='".parent::getempNo()."' limit $offset,$rows ";
		 foreach($this->con->getResultSet($sql) as $row)
		 {
			 array_push($data2,$row);
		}
		 $data["rows"]=$data2;
		 return $data;
	 }
 	 public function delete(){
	 	 $builder=new DeleteBuilder;
	 	 $builder->setTable('employeebank');
	 	 $builder->setCriteria("where id='".parent::getid()."'");
	 	 $this->con->setQuery(Director::buildSql($builder));
	 	 return $this->con->execute_query();
 	 }
 

 	 public function find(){
	 	 $sql="select * from  employeebank  where id='".parent::getid()."'";
	 	 foreach($this->con->getResultSet($sql) as $row)
	 	{
	 	parent::setid($row['id']);
	 	parent::setempNo($row['empNo']);
	 	parent::setbank($row['bank']);
	 	parent::setbranch($row['branch']);
	 	parent::setaccountName($row['accountName']);
	 	parent::setaccountNumber($row['accountNumber']);
	 	parent::setnssfNumber($row['nssfNumber']);
	 	parent::setIsactive($row['Isactive']);
}	 	}

 	 public function view_query($sql){
	 	 return $this->con->getResultSet($sql);
 	 }
 }
?>