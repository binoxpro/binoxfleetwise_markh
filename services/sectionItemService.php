<?php
include_once '../model/sectionItem.php';
class SectionItemService extends SectionItem
{
	public function __construct() 
	{
        parent::__construct();
    }


    public function save() 
	{
		$builder=new InsertBuilder();
		$builder->setTable("tblsectionitem");
		$builder->addColumnAndData("itemDescription", parent::getitemDescription());
		$builder->addColumnAndData("comment", parent::getcomment());
		$builder->addColumnAndData("sectionId", parent::getsectionId());
		$builder->addColumnAndData("itemName", parent::getitemName());
		$this->con->setQuery(Director::buildSql($builder));
		return $this->con->execute_query2($builder->getValues());	  
    }

    public function update() 
	{
      	 	$builder=new UpdateBuilder();
            $builder->setTable("tblsectionitem");
			$builder->addColumnAndData("itemDescription", parent::getitemDescription());
			$builder->addColumnAndData("comment", parent::getcomment());
			$builder->addColumnAndData("sectionId", parent::getsectionId());
			$builder->addColumnAndData("itemName", parent::getitemName());
			$builder->setCriteria("where id='".parent::getid()."'");
			$this->con->setQuery(Director::buildSql($builder));
			return $this->con->execute_query();
		
    }

	public function view() {
       $sql="select * from tblsectionitem";
	   return $this->con->getResultSet($sql);
    }
	
	
	
	public function delete(){
		$builder=new DeleteBuilder();
		$builder->setTable("tblsectionitem");
		$builder->setCriteria("where id='".parent::getid()."'");
		$this->con->setQuery(Director::buildSql($builder));
		$this->con->execute_query();
	}
    public function view_query($sql) {
        return $this->con->getResultSet($sql);
    }
	
}

?>
