<?php
ob_start();
session_start();
 require_once('../services/partRequestFormItemService.php');
 require_once('../services/storestockService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $partRequestFormItem=new partRequestFormItemService();
 		 echo json_encode( $partRequestFormItem->view());
 		 //viewReview
 }else if($controller_templet->getAction()=='viewReview'){
         $partRequestFormItem=new partRequestFormItemService();
         if($_REQUEST['id'])
         {
             $partRequestFormItem->setprfId($_REQUEST['id']);
             echo json_encode($partRequestFormItem->view());
         }else
         {
             //echo json_encode($partRequestFormItem->view());
         }

         //viewReview
     }	 else if($controller_templet->getAction()=='add')
     {
	 	 $partRequestFormItem=new partRequestFormItemService();
 	 	 $partRequestFormItem->setitemId($_REQUEST['itemId']);
	 	 $partRequestFormItem->setitemDescription($_REQUEST['itemDescription']);
	 	 $partRequestFormItem->setuomId($_REQUEST['uomId']);
	 	 $partRequestFormItem->setquantity($_REQUEST['quantity']);
	 	 $partRequestFormItem->setquantityIssued($_REQUEST['quantityIssued']);
	 	 $partRequestFormItem->setdateIssued($_REQUEST['dateIssued']);
	 	 $partRequestFormItem->setissuedBy($_REQUEST['issuedBy']);
	 	 $partRequestFormItem->setreceviedBy($_REQUEST['receviedBy']);
	 	 $partRequestFormItem->setcomment($_REQUEST['comment']);
	 	 $partRequestFormItem->setstatus($_REQUEST['status']);
	 	 $partRequestFormItem->setprfId($_REQUEST['prfId']);
	 	 echo json_encode( $partRequestFormItem->save());
     }
     else if($controller_templet->getAction()=='edit')
     {
 	 	 $partRequestFormItem=new partRequestFormItemService();
 	 	 $partRequestFormItem->setid($_REQUEST['id']);
         if(isset($_REQUEST['itemId'])){
             $partRequestFormItem->setitemId($_REQUEST['itemId']);
         }
	 	 if(isset($_REQUEST['itemDescription'])) {
             $partRequestFormItem->setitemDescription($_REQUEST['itemDescription']);
         }
         if(isset($_REQUEST['uomId'])) {
             $partRequestFormItem->setuomId($_REQUEST['uomId']);
         }
         if(isset($_REQUEST['quantity'])) {
             $partRequestFormItem->setquantity($_REQUEST['quantity']);
         }
         if(isset($_REQUEST['quantityIssued'])) {
             $partRequestFormItem->setquantityIssued($_REQUEST['quantityIssued']);
         }
         if(isset($_REQUEST['dateIssued'])) {
             $partRequestFormItem->setdateIssued($_REQUEST['dateIssued']);
         }
         if(isset($_REQUEST['issuedBy'])) {
             $partRequestFormItem->setissuedBy($_REQUEST['issuedBy']);
         }
         if(isset($_REQUEST['receviedBy'])) {
             $partRequestFormItem->setreceviedBy($_REQUEST['receviedBy']);
         }
         if(isset($_REQUEST['comment']))
         {
             $partRequestFormItem->setcomment($_REQUEST['comment']);
         }
         if(isset($_REQUEST['status1']))
         {
             $partRequestFormItem->setstatus($_REQUEST['status1']);
         }
         if(isset($_REQUEST['prfId']))
         {
             $partRequestFormItem->setprfId($_REQUEST['prfId']);
         }
	 	 echo json_encode($partRequestFormItem->update());
    }else if($controller_templet->getAction()=='purchaseItems')
     {
         if(isset($_REQUEST['id']))
         {
             $partRequestFormItem=new partRequestFormItemService();

             $partRequestFormItem->setid($_REQUEST['id']);
             $partRequestFormItem->find();
             $partRequestFormItem->setstatus('PROCESS PURCHASE');
             //echo json_encode(array('msg'=>'Anything can be done'));
             echo json_encode($partRequestFormItem->update());
         }else{
             echo json_encode(array('msg'=>'Anything can be done'));
         }
     }


    else if($controller_templet->getAction()=='delete')
    {
 	 	 $partRequestFormItem=new partRequestFormItemService();
 	 	 $partRequestFormItem->setid($_REQUEST['id']);
        echo json_encode( $partRequestFormItem->delete());
    }else if($controller_templet->getAction()=='viewCombo')
    {
		 $partRequestFormItem=new partRequestFormItemService();
 		 echo json_encode( $partRequestFormItem->viewConbox());
    }else if($controller_templet->getAction()=='issue')
    {
        $newIssuedQty=0;
        $partRequestFormItem=new partRequestFormItemService();
        $partRequestFormItem->setid($_REQUEST['id']);
        $partRequestFormItem->find();
        $newIssuedQty=$partRequestFormItem->getquantityIssued()+$_REQUEST['quantityIssued'];
        //$partRequestFormItem->setquantityIssued($_REQUEST['quantityIssued']);
        $partRequestFormItem->setquantityIssued($newIssuedQty);
        $partRequestFormItem->setreceviedBy($_REQUEST['receviedBy']);
        $partRequestFormItem->setissuedBy($_SESSION['username']);
        $partRequestFormItem->setdateIssued($_REQUEST['issuedDate']);
        $status='';
        if($partRequestFormItem->getquantityIssued()==$newIssuedQty)
        {
            $status='ISSUED';
        }else{

        }
        //$status='ISSUED';
        $partRequestFormItem->setstatus($status);
        $partRequestFormItem->update();

        //record stock
        $stock=new storestockService();
        $stock->setitemId($partRequestFormItem->getitemId());
        $stock->setquantity($_REQUEST['quantityIssued']);
        $stock->setunitPrice(0);
        $stock->setstocktype('OUT');
        $stock->setreceivedBy($_SESSION['username']);
        $stock->setreceivedBy($_REQUEST['receviedBy']);
        $stock->setcreationDate(date('Y-m-d'));
        echo json_encode($stock->save());
        //$stock->setsupplierId(0);

    }else if($controller_templet->getAction()=='viewCombo3')
    {
        $partRequestFormItem=new partRequestFormItemService();
        if(isset($_REQUEST['prfNo']))
        {
            $partRequestFormItem->setprfId($_REQUEST['prfNo']);
            echo json_encode( $partRequestFormItem->viewConboxForPrf());
        }

    }
} ?>