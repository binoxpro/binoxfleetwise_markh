<?php

 ob_start();
// session_start();
require_once '../services/tyreTypeService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$tyreType=new TyreTypeService();
		echo json_encode($tyreType->view());
	}else if($controller_templet->getAction()=="add")
	{
		$tyreType=new TyreTypeService();
		if(isset($_REQUEST['tyretypeName'])){
		$tyreType->settyretypeName($_REQUEST['tyretypeName']);
        $tyreType->settyreIndex($_REQUEST['tyreIndex']);
        $tyreType->setrotationPosition($_REQUEST['rotationPosition']);
		echo json_encode($tyreType->save());
		}else{
			echo json_encode(array('msg'=>'Supply the tyre type Name'));
		}
	}else if($controller_templet->getAction()=="edit")
	{
		$tyreType=new TyreTypeService();
		if(isset($_REQUEST['tyretypeName'])){
		$tyreType->setId($_REQUEST['id']); 
		$tyreType->settyretypeName($_REQUEST['tyretypeName']);
        $tyreType->settyreIndex($_REQUEST['tyreIndex']);
        $tyreType->setrotationPosition($_REQUEST['rotationPosition']);
		echo json_encode($tyreType->update());
		}else{
			echo json_encode(array('msg'=>'Supply the tyre type Name'));
		}
	}else if($controller_templet->getAction()=="delete")
	{
	   	$tyreType=new TyreTypeService();
        $tyreType->setId($_REQUEST['id']); 
        echo json_encode($tyreType->delete());
        
	   }
}
?>