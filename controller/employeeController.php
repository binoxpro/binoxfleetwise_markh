<?php 
 require_once('../services/employeeService.php');
require_once('../services/individualAccountService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $employee=new employeeService();
 		 echo json_encode( $employee->view());
	}	 else if($controller_templet->getAction()=='add')
	 {
	 	 $employee=new employeeService();
		 if(!is_null($_REQUEST['empNo'])&&$_REQUEST['empNo']!='') {
			 $employee->setempNo($_REQUEST['empNo']);
			 $employee->setfirstName($_REQUEST['firstName']);
			 $employee->setlastName($_REQUEST['lastName']);
			 $employee->setsex($_REQUEST['sex']);
			 $employee->setmartialStatus($_REQUEST['martialStatus']);
			 $employee->setdob($_REQUEST['dob']);
			 $employee->settown($_REQUEST['town']);
			 $employee->setdistrict($_REQUEST['district']);
			 $employee->setnationID($_REQUEST['nationID']);
			 $employee->setmobile1($_REQUEST['mobile1']);
			 $employee->setmobile2($_REQUEST['mobile2']);
			 $employee->setnikname($_REQUEST['nikname']);
			 $employee->setcontact($_REQUEST['contact']);
			 //upload load the photo
			 $target_file = 'upload';
			 if ($_FILES['file']['name'] != "") {
				 if (!file_exists($target_file . "/" . basename($_FILES['file']['name']))) {
					 //exit("Please run 05featuredemo.php first." . EOL);

					 move_uploaded_file($_FILES["file"]["tmp_name"], $target_file . "/" . basename($_FILES['file']['name'])) or
					 die("Could not copy file!");
				 } else {
					 if (!unlink($target_file . "/" . basename($_FILES['file']['name']))) {
						 die("Cannot detele file");
					 } else {

						 move_uploaded_file($_FILES["file"]["tmp_name"], $target_file . "/" . basename($_FILES['file']['name'])) or
						 die("Could not copy file!");
					 }
				 }
				 $employee->setphoto($target_file . "/" . basename($_FILES['file']['name']));
			 } else {
				 //die("No file specified!");
			 }
			 $employee->save();
			 //account type
			 $individualAccount=new individualAccountService();
			 $individualAccount->setId($_REQUEST['empNo']);
			 $individualAccount->setAccountName($_REQUEST['firstName']." ".$_REQUEST['lastName']);
			 $individualAccount->setAccountCode(null);
			 $individualAccount->setIsActive(true);

			 echo json_encode( $individualAccount->save());

		 }else
		 {
			 echo json_encode(array('msg'=>'Supply Employee ID No'));
		 }
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $employee=new employeeService();
 	 	 $employee->setempNo($_REQUEST['empNo']);
	 	 $employee->setfirstName($_REQUEST['firstName']);
	 	 $employee->setlastName($_REQUEST['lastName']);
	 	 $employee->setsex($_REQUEST['sex']);
	 	 $employee->setmartialStatus($_REQUEST['martialStatus']);
	 	 $employee->setdob($_REQUEST['dob']);
	 	 $employee->settown($_REQUEST['town']);
	 	 $employee->setdistrict($_REQUEST['district']);
	 	 $employee->setnationID($_REQUEST['nationID']);
	 	 $employee->setmobile1($_REQUEST['mobile1']);
	 	 $employee->setmobile2($_REQUEST['mobile2']);
	 	 $employee->setnikname($_REQUEST['nikname']);
	 	 $employee->setcontact($_REQUEST['contact']);
	 	 echo json_encode( $employee->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $employee=new employeeService();
 	 	 $employee->setempNo($_REQUEST['empNo']);
echo json_encode( $employee->delete()); 
}else if($controller_templet->getAction()=='viewCombo')
 {
		 $employee=new employeeService();
 		 echo json_encode( $employee->viewConbox());
 }else if($controller_templet->getAction()=='viewComboType')
 {
     $employee=new employeeService();
     $str=$_REQUEST['str'];
     echo json_encode( $employee->viewConboxposition($str));
 }else if($controller_templet->getAction()=="getObject")
 {
		$employee=new employeeService();
		$employee->setempNo($_REQUEST['id']);
	 	echo json_encode($employee->findJson());
 }
} ?>