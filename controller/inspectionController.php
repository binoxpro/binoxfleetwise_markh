<?php
ob_start();
// session_start();
require_once '../services/checklistTypeService.php';
require_once '../services/checkedService.php';
require_once '../services/checkedItemService.php';
require_once '../services/jobCardService.php';
require_once '../lib/signturePhp/signature-to-image.php';

require_once 'controller.php';
if(isset($_REQUEST['action']))
{

    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$checkListType=new SectionItemService();
		echo json_encode($checkListType->view());
	}else if($controller_templet->getAction()=="loadCheck")
	{
		$checkListType=new ChecklistTypeService();
		$checkListType->setType($_REQUEST['id']);
		$i=1;
		$sql="select * from tblsection where checkTypeId='".$checkListType->getType()."' and isActive='ON'";
		foreach ($checkListType->view_query($sql) as $row)
		{
			$sectionId=$row['id'];
			$roman="";
			for($x=0;$x<$i;$x++)
			{
				$roman=$x+1;
			}
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th colspan='4' style='text-align:center;'> SECTION:".$roman."&nbsp".$row['name']."</th></tr></thead><tbody>";
			$sql2="select * from tblsectionitem where sectionId='".$sectionId."'";
			echo "<tr><td>Item</td><td>Item Description</td><td>Checked</td><td>Comment</td></tr>";
			foreach($checkListType->view_query($sql2) as $row2)
			{
				echo "<tr><td><input type='hidden' name='sectionItemId[]' value='".$row2['id']."'/>".$row2['itemName']."</td><td>".$row2['itemDescription']."</td><td><select name='checked[]' class='form-control'><option value='Passed'>Passed</option><option value='Failed'>Failed</option></select></td><td><textarea name='icommet[]' class='form-control'></textarea></textarea>"."</td></tr>";
			}
			$i=$i+1;
			echo "</tbody></table></div>";
		}
		//echo"";
		echo"<input type='submit' name='action' value='add'  class='btn btn-primary' style='margin-left:70%;'/>";
	}else if($controller_templet->getAction()=="add")
	{
		if(isset($_REQUEST['checked'])){
			try{
                $checked=new CheckedService();
                $checked->setdate($_REQUEST['date']);
                $checked->settime($_REQUEST['time']);
                $checked->setdriverName($_REQUEST['driverName']);
                $checked->setrepresentative($_REQUEST['representative']);
                $checked->setcomment($_REQUEST['comment']);
                $checked->setvehicleId($_REQUEST['regNo']);
                $checked->setCheckListTypeId($_REQUEST['type']);
				if(isset($_REQUEST['vehicleConditionOk']))
				{
					$checked->setvehicleCoditionOk(true);
				}else
                {
					$checked->setvehicleCoditionOk(false);
				}

				if(isset($_REQUEST['defectdonotcorrection']))
				{
					$checked->setdefectdonotcorrection(true);
				}else
                {
					$checked->setdefectdonotcorrection(false);
				}

				if(isset($_REQUEST['defectscorrected']))
				{
					$checked->setdefectscorrected(true);
				}else
                {
					$checked->setdefectscorrected(false);
				}

			    $checked->save();
			
			
			$noCheck=$_REQUEST['checked'];
			$noSectionItemId=$_REQUEST['sectionItemId'];
			$comment=$_REQUEST['icommet'];
			$checkItem=new CheckedItemService();
			$failed=false;
			for($i=0;$i<sizeof($noCheck);$i++)
			{
				if($noCheck[$i]=="Failed"){
					$checkItem->setcheckListId($checked->getid());
					$checkItem->setsectionItemId($noSectionItemId[$i]);
					$checkItem->setinspectorsComment($comment[$i]);
					$checkItem->save();
					$failed=true;
				}
			}
			
			if($failed)
			{
				if($checked->getvehicleCoditionOk()==0||$checked->getdefectdonotcorrection()==0)
				{
					$jobCard=new jobCardService();
					//echo $jobCard->jobNoGeneration();
					$jobCard->setjobNo($jobCard->jobNoGeneration());
					$jobCard->setvehicleId($checked->getvehicleId());
					$jobCard->setdate($checked->getdate());
					$jobCard->settime($checked->gettime());
					$jobCard->setChecklistId($checked->getid());
					$jobCard->save();
				}


			}
			
			$json = $_REQUEST['output'];
			$img = sigJsonToImage($json);
			
			imagepng($img, 'signature/'.$checked->getid().'Inspector.png');
			imagedestroy($img);
			
			$json2 = $_REQUEST['output-2'];
			$img2 = sigJsonToImage($json2);
			
			imagepng($img2,'signature/'.$checked->getid().'driver.png');
			
			imagedestroy($img2);
			
			
			$controller_templet->setUrlHeader('checkListDocuement2.php?id='.$checked->getid());
			$controller_templet->redirect();
			}catch(Exception $e){
				echo $e->getMessage();
			}
			
		}
		
		
	}else if($controller_templet->getAction()=="list_CheckList")
	{
			$inspection=new CheckedService();
		    $sql="";
		    if(isset($_REQUEST['regNo'])&&isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
            {
                $startDate=$_REQUEST['startDate'];
                $endDate=$_REQUEST['endDate'];
                $vehicleId=$_REQUEST['regNo'];
                $sql="select cd.*,v.regNo from tblchecked cd inner join tblvehicle v on cd.vehicleId=v.id where cd.vehicleId='".$vehicleId."' and cd.Date between '".$startDate."' and '".$endDate."'  order by cd.Date desc";
            }else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
            {
                $startDate=$_REQUEST['startDate'];
                $endDate=$_REQUEST['endDate'];
                //$vehicleId=$_REQUEST['regNo'];
                $sql="select cd.*,v.regNo from tblchecked cd inner join tblvehicle v on cd.vehicleId=v.id where cd.Date between '".$startDate."' and '".$endDate."'  order by cd.Date desc";
            }else if(isset($_REQUEST['regNo']))
            {
                //$startDate=$_REQUEST['startDate'];
                //$endDate=$_REQUEST['endDate'];
                $vehicleId=$_REQUEST['regNo'];
                $sql="select cd.*,v.regNo from tblchecked cd inner join tblvehicle v on cd.vehicleId=v.id where cd.vehicleId='".$vehicleId."'  order by cd.Date desc";
            }else
            {

                $sql="select cd.*,v.regNo from tblchecked cd inner join tblvehicle v on cd.vehicleId=v.id  order by cd.Date desc";
            }
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>CheckList No</th><th>Reg No</th><th>Defects</th><th>Creation Date</th><th>#</th></tr></thead><tbody>";
			foreach($inspection->view_query($sql) as $row2)
			{
				echo "<tr><td>".$row2['id']."</td><td>".$row2['regNo']."</td><td>"."</td><td>".$row2['Date']."/".$row2['Time']."</td><td><a href='controller/checkListDocuement2.php?id=".$row2['id']."' target='_blank' >View</a></td></tr>";
			}
			echo "</tbody></table></div>";
	}


}

?>