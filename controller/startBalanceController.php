<?php 
 require_once('../services/startBalanceService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherService.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/accountTypeService.php');
require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $startBalance=new startBalanceService();
 		 echo json_encode( $startBalance->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $startBalance=new startBalanceService();
 	 	 $startBalance->setaccountCode($_REQUEST['accountCode']);
	 	 $startBalance->setamount($_REQUEST['amount']);
	 	 $startBalance->setcreationDate($_REQUEST['creationDate']);
		 if($_REQUEST['individualNo']!=""&&$_REQUEST['individualNo']!=null) {
			 $startBalance->setindividual($_REQUEST['individualNo']);
		 }else{
			 $startBalance->setindividual(null);
		 }
	 	 $startBalance->save();
		 //do the entry
		 $dateArray=explode('-',$_REQUEST['creationDate']);

		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['creationDate']);
		 $ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
		 $ledgerHead->setamount($_REQUEST['amount']);
		 $ledgerHead->save();
		 //Define the Balance Type
		 $chartOfAccount=new chartOfaccountService();
		 $chartOfAccount->setAccountCode($_REQUEST['accountCode']);
		 $chartOfAccount->find();

		 $accountType=new accountTypeService();
		 $accountType->setPrefixCode($chartOfAccount->getAccountTypeCode());
		 $accountType->find();

		 $balanceAccount=new chartOfaccountService();
		 $balanceAccount->setAccountName('Multi-Journal Control A/c');
		 $balanceAccount->findAccountCode();

		 $balanceAccountToPost=new chartOfaccountService();
		 $balanceAccountToPost->setAccountCode($_REQUEST['accountCode']);
		 $balanceAccountToPost->find();


		if($accountType->getAccountBalanceType()=="Dr")
		{

			//Cr
			$generalLedger = new generalLedgerService();
			$generalLedger->setledgerHeadId($ledgerHead->getid());
			$generalLedger->setaccountCode($balanceAccount->getAccountCode());
			$generalLedger->setnarrative('Openning Balance for Account/'.$_REQUEST['accountCode'].'/'.$balanceAccountToPost->getAccountName());
			//$generalLedger->setreference();
			$generalLedger->setamount($_REQUEST['amount']);
			$generalLedger->settransactionType('Cr');
			$generalLedger->setisActive(true);
			$generalLedger->save();

			//Dr
			$generalLedger = new generalLedgerService();
			$generalLedger->setledgerHeadId($ledgerHead->getid());
			$generalLedger->setaccountCode($balanceAccountToPost->getAccountCode());
			$generalLedger->setnarrative('Openning Balance for Account/'.$_REQUEST['accountCode'].'/'.$balanceAccountToPost->getAccountName());
			$generalLedger->setamount($_REQUEST['amount']);
			$generalLedger->settransactionType('Dr');
			//$generalLedger->setreference($taxinvoice->getid());
			$generalLedger->setisActive(true);
			if($_REQUEST['individualNo']!=""&&$_REQUEST['individualNo']!=null)
			{
				$generalLedger->setindividualNo($_REQUEST['individualNo']);
			}
			$generalLedger->save();
		}else
		{
			//Dr
			$generalLedger = new generalLedgerService();
			$generalLedger->setledgerHeadId($ledgerHead->getid());
			$generalLedger->setaccountCode($balanceAccount->getAccountCode());
			$generalLedger->setnarrative('Openning Balance for Account/'.$_REQUEST['accountCode'].'/'.$balanceAccountToPost->getAccountName());
			//$generalLedger->setreference();
			$generalLedger->setamount($_REQUEST['amount']);
			$generalLedger->settransactionType('Dr');
			$generalLedger->setisActive(true);
			$generalLedger->save();

			//Cr
			$generalLedger = new generalLedgerService();
			$generalLedger->setledgerHeadId($ledgerHead->getid());
			$generalLedger->setaccountCode($balanceAccountToPost->getAccountCode());
			$generalLedger->setnarrative('Openning Balance for Account/'.$_REQUEST['accountCode'].'/'.$balanceAccountToPost->getAccountName());
			$generalLedger->setamount($_REQUEST['amount']);
			$generalLedger->settransactionType('Cr');
			//$generalLedger->setreference($taxinvoice->getid());
			$generalLedger->setisActive(true);
			if($_REQUEST['individualNo']!=""&&$_REQUEST['individualNo']!=null)
			{
				$generalLedger->setindividualNo($_REQUEST['individualNo']);
			}
			//$generalLedger->setindividualNo($_REQUEST['customerId']);
			$generalLedger->save();
		}

		 echo json_encode(array('success'=>true));


 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $startBalance=new startBalanceService();
 	 	 $startBalance->setid($_REQUEST['id']);
	 	 $startBalance->setaccountCode($_REQUEST['accountCode']);
	 	 $startBalance->setamount($_REQUEST['amount']);
	 	 $startBalance->setcreationDate($_REQUEST['creationDate']);
	 	 echo json_encode( $startBalance->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $startBalance=new startBalanceService();
 	 	 $startBalance->setid($_REQUEST['id']);
echo json_encode( $startBalance->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $startBalance=new startBalanceService();
 		 echo json_encode( $startBalance->viewConbox());
		}   
} ?>