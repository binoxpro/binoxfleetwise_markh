<?php 
 require_once('../services/truckMakeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $truckMake=new truckMakeService();
 echo json_encode( $truckMake->view());
 }else if($controller_templet->getAction()=='add'){
	 	 $truckMake=new truckMakeService();
 	 	 $truckMake->setmakeName($_REQUEST['modelName']);
	 	if(isset($_REQUEST['Prefered'])) {
			$truckMake->setPrefered($_REQUEST['Prefered']);
		}else{
			$truckMake->setPrefered(false);
		}
	 	 echo json_encode( $truckMake->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $truckMake=new truckMakeService();
 	 	 $truckMake->setid($_REQUEST['id']);
	 	 $truckMake->setmakeName($_REQUEST['modelName']);
         if(isset($_REQUEST['Prefered'])) {
             $truckMake->setPrefered($_REQUEST['Prefered']);
         }else{
             $truckMake->setPrefered(false);
         }
	 	 echo json_encode( $truckMake->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $truckMake=new truckMakeService();
 	 	 $truckMake->setid($_REQUEST['id']);
echo json_encode( $truckMake->delete()); } 
} ?>