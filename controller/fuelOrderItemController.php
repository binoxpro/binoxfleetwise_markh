<?php 
 require_once('../services/fuelOrderItemService.php');
require_once('../services/fuelProductService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $fuelOrderItem=new fuelOrderItemService();
		 $fuelOrderItem->setfuelOrderId($_REQUEST['fuelOrderId']);
 		 echo json_encode( $fuelOrderItem->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $fuelOrderItem=new fuelOrderItemService();
 	 	 $fuelOrderItem->setfuelOrderId($_REQUEST['fuelOrderId']);
		 $fuelProduct=new fuelProductService();
		 $fuelProduct->setid($_REQUEST['fuelProductId']);
		 $fuelProduct->find();
		 if(!is_null($fuelProduct->getid()) )
		 {
			 $fuelOrderItem->setfuelProductId($fuelProduct->getid());
			 $fuelOrderItem->setlitres($_REQUEST['litres']);
		 	 $fuelOrderItem->setrate($_REQUEST['rate']);
		 	 $fuelOrderItem->setisActive(false);
		 	 echo json_encode($fuelOrderItem->save());
		 }else
		 {
		 	
		 }

	 	 
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $fuelOrderItem=new fuelOrderItemService();
 	 	 $fuelOrderItem->setid($_REQUEST['id']);
	 	 $fuelOrderItem->setfuelOrderId($_REQUEST['fuelOrderId']);
		 $fuelProduct=new fuelProductService();
		 $fuelProduct->setid($_REQUEST['fuelProductId']);
		 $fuelProduct->find();
		 if(!is_null($fuelProduct->getid()));
		 {
			 $fuelOrderItem->setfuelProductId($_REQUEST['fuelProductId']);
		 }

		 $fuelOrderItem->setlitres($_REQUEST['litres']);
	 	 $fuelOrderItem->setrate($_REQUEST['rate']);
	 	if(isset($_REQUEST['isActive']))
		{
			$fuelOrderItem->setisActive($_REQUEST['isActive']);
		}

	 	 echo json_encode($fuelOrderItem->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $fuelOrderItem=new fuelOrderItemService();
 	 	 $fuelOrderItem->setid($_REQUEST['id']);
		echo json_encode( $fuelOrderItem->delete());
}else if($controller_templet->getAction()=='viewCombo')
 {
		 $fuelOrderItem=new fuelOrderItemService();
 		 echo json_encode($fuelOrderItem->viewConbox());
}
} ?>