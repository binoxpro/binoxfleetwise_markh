<?php 
 require_once('../services/tireInspectionHeaderService.php');
 require_once '../services/vehicleService.php';
 require_once('../services/tireHoldService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $tireInspectionHeader=new tireInspectionHeaderService();
     if(isset($_REQUEST['id']))
     {
         $tireInspectionHeader->setvehicleId($_REQUEST['id']);
		 echo json_encode($tireInspectionHeader->vehicleHistorySearch());
	 }else
     {
         echo json_encode($tireInspectionHeader->view());
     }
 }else if($controller_templet->getAction()=='add'){
	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setvehicleId($_REQUEST['vehicleId']);
	 	 $tireInspectionHeader->setlastReading($_REQUEST['lastReading']);
	 	 $tireInspectionHeader->setcurrentReading($_REQUEST['currentReading']);
	 	 $tireInspectionHeader->setcreationDate($_REQUEST['creationDate']);
		 $date=date_create($_REQUEST['creationDate']);
		date_add($date,date_interval_create_from_date_string("30 days"));
		 $tireInspectionHeader->setNextReading(date_format($date,"Y-m-d"));
	 	 $tireInspectionHeader->setisActive(true);
         if(intval($_REQUEST['llid'])>0)
         {
             $tireInspectionHeader2=new tireInspectionHeaderService();
             $tireInspectionHeader2->setid($_REQUEST['llid']);
             $tireInspectionHeader2->getObject();
             $tireInspectionHeader2->setisActive(false);
             $tireInspectionHeader2->update();
         }
	 	 echo json_encode($tireInspectionHeader->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setid($_REQUEST['id']);
	 	 $tireInspectionHeader->setvehicleId($_REQUEST['vehicleId']);
	 	 $tireInspectionHeader->setlastReading($_REQUEST['lastReading']);
	 	 $tireInspectionHeader->setcurrentReading($_REQUEST['currentReading']);
	 	 $tireInspectionHeader->setcreationDate($_REQUEST['creationDate']);
	 	 $tireInspectionHeader->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $tireInspectionHeader->update()); 
          }
 else if($controller_templet->getAction()=='delete'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setid($_REQUEST['id']);
echo json_encode( $tireInspectionHeader->delete()); 
}else if($controller_templet->getAction()=='getLastInspection'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setvehicleId($_REQUEST['id']);
         $tireInspectionHeader->getLastInspectionObject();
         $id=is_null($tireInspectionHeader->getid())?0:$tireInspectionHeader->getid();
         $reading=is_null($tireInspectionHeader->getcurrentReading())?0:$tireInspectionHeader->getcurrentReading();
        echo  $id."*".$reading; 
} else if($controller_templet->getAction()=='getInspectionDetails'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setid($_REQUEST['id']);
         $tireInspectionHeader->getObject();
         $vehicle=new VehicleService();
         echo "<h4 class='text-center'>MARKH INVESTMENT LIMITED</h4>";
         echo "<h4 class='text-center'>Tire Inspection Sheet</h4>";
         echo("<input id='inspectionId' name='inspectionId' type='hidden' value='".$_REQUEST['id']."'/>");
         echo "<table class='table'>";
         
         echo"<tr><th>Registration Number:</th><td>".$vehicle->returnVehicleNumber($tireInspectionHeader->getvehicleId())."</td><th>Odometer Reading</th><td>".$tireInspectionHeader->getcurrentReading()."</td><th>Date:</th><td>".$tireInspectionHeader->getcreationDate()."</td></tr>";
         echo "</table>";
         
         echo"<div class='table-responsive'><table class='table table-bordered'>";
         echo("<tr><td>Position</td><td>Brand</td><td>Pattern</td><td>Size</td><td>Serial</td><td>n/r</td><td>Tread Depth</td><td>Pressure</td><td>Sidewall Damaged</td><td>Tread Damage</td><td>Mech Damages</td><td>Mismatched Duals</td><td>Valve Condition</td><td>Rim Condition</td><td>Remarks</td></tr>");
         $sql="Select th.id,tt.tyretypeName position,t.tireBand brand,t.serialNumber serialNo,tp.name pattern,ts.name size,t.tireType nr from tbltirehold th inner join tbltire t on t.id=th.tireId inner join tbltyretype tt on tt.id=th.positionId inner join tbltiresize ts on ts.id=t.tireSizeId inner join tbltirepattern tp on tp.id=t.tirePatternId where th.vehicleId='".$tireInspectionHeader->getvehicleId()."' and th.isActive='1'";
         foreach($tireInspectionHeader->view_query($sql) as $row)
         {
            echo("<tr><td><input type='hidden' name='thid[]' value='".$row['id']."' />".$row['position']."</td><td>".$row['brand']."</td><td>".$row['pattern']."</td><td>".$row['size']."</td><td>".$row['serialNo']."</td><td>".$row['nr']."</td><td><input type='text' name='td[]' value='' style='width:20px;' /></td><td><input type='text' name='psi[]' value='' style='width:30px;'  /></td><td><select name='sd[]' style='width:50px;'><option value='0'>N</option><option value='1'>Y</option></select></td><td><select name='td2[]' style='width:50px;'><option value='0'>N</option><option value='1'>Y</option></select></td><td><select name='md[]' style='width:50px;'><option value='0'>N</option><option value='1'>Y</option></select></td><td><select name='mid[]' style='width:50px;'><option value='0'>N</option><option value='1'>Y</option></select></td><td><select name='vc[]' style='width:50px;'><option value='0'>Poor</option><option value='1'>Good</option></select></td><td><select name='rc[]' style='width:50px;'><option value='0'>Poor</option><option value='1'>Good</option></select></td><td><textarea name='remarks[]'></textarea></td></tr>");
         }
         echo "</table></div>";
} 
else if($controller_templet->getAction()=='getInspectionDetailsHardCopy'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setid($_REQUEST['id']);
         $tireInspectionHeader->getObject();
         $vehicle=new VehicleService();
         echo "<h4 class='text-center'>MARKH INVESTMENT LIMITED</h4>";
         echo "<h4 class='text-center'>Tire Inspection Sheet</h4>";
         echo("<input id='inspectionId' name='inspectionId' type='hidden' value='".$_REQUEST['id']."'/>");
         echo "<table class='table'>";
         
         echo"<tr><th>Registration Number:</th><td>".$vehicle->returnVehicleNumber($tireInspectionHeader->getvehicleId())."</td><th>Odometer Reading</th><td>".$tireInspectionHeader->getcurrentReading()."</td><th>Date:</th><td>".$tireInspectionHeader->getcreationDate()."</td></tr>";
         echo "</table>";
         
         echo"<table class='table table-bordered table-striped'>";
         echo("<tr><td>Position</td><td>Brand</td><td>Pattern</td><td>Size</td><td>Serial</td><td>n/r</td><td>Tread Depth</td><td>Pressure</td><td>Sidewall Damaged</td><td>Tread Damage</td><td>Mech Damages</td><td>Mismatched Duals</td><td>Valve Condition</td><td>Rim Condition</td><td>Remarks</td></tr>");
         $sql="Select th.id,tt.tyretypeName position,t.tireBand brand,t.serialNumber serialNo,tp.name pattern,ts.name size,t.tireType nr from tbltirehold th inner join tbltire t on t.id=th.tireId inner join tbltyretype tt on tt.id=th.positionId inner join tbltiresize ts on ts.id=t.tireSizeId inner join tbltirepattern tp on tp.id=t.tirePatternId where th.vehicleId='".$tireInspectionHeader->getvehicleId()."' and th.isActive='1'";
         foreach($tireInspectionHeader->view_query($sql) as $row)
         {
            echo("<tr><td><input type='hidden' name='thid[]' value='".$row['id']."' />".$row['position']."</td><td>".$row['brand']."</td><td>".$row['pattern']."</td><td>".$row['size']."</td><td>".$row['serialNo']."</td><td>".$row['nr']."</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
         }
         echo "</table>";
} else if($controller_templet->getAction()=='getInspectionDetailsHardCopy2'){
 	 	 $tireInspectionHeader=new tireInspectionHeaderService();
 	 	 $tireInspectionHeader->setid($_REQUEST['id']);
         $tireInspectionHeader->getObject();
         $vehicle=new VehicleService();
         echo "<h4 class='text-center'>MARKH INVESTMENT LIMITED</h4>";
         echo "<h4 class='text-center'>Tire Inspection Sheet</h4>";
         echo("<input id='inspectionId' name='inspectionId' type='hidden' value='".$_REQUEST['id']."'/>");
         echo "<table class='table'>";
         
         echo"<tr><th>Registration Number:</th><td>".$vehicle->returnVehicleNumber($tireInspectionHeader->getvehicleId())."</td><th>Odometer Reading</th><td>".$tireInspectionHeader->getcurrentReading()."</td><th>Date:</th><td>".$tireInspectionHeader->getcreationDate()."</td></tr>";
         echo "</table>";
         
         echo"<table class='table table-bordered table-striped'>";
         echo("<tr><td>Position</td><td>Brand</td><td>Pattern</td><td>Size</td><td>Serial</td><td>N/R</td><td>Tread Depth</td><td>Pressure</td><td>Sidewall Damaged</td><td>Tread Damage</td><td>Mech Damages</td><td>Mismatched Duals</td><td>Valve Condition</td><td>Rim Condition</td><td>Remarks</td></tr>");
         $sql="select tid.*,th.id,tt.tyretypeName position,t.tireBand brand,t.serialNumber serialNo,tp.name pattern,ts.name size,t.tireType nr from  tbltireinspectiondetails tid inner join tbltirehold th on th.id=tid.tireHoldId inner join tbltire t on t.id=th.tireId inner join tbltyretype tt on tt.id=th.positionId inner join tbltiresize ts on ts.id=t.tireSizeId inner join tbltirepattern tp on tp.id=t.tirePatternId where tid.inspectionHeaderId='".$tireInspectionHeader->getid()."'";
         
         foreach($tireInspectionHeader->view_query($sql) as $row)
         {
           
            $sd=$row['sidewall']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            $td2=$row['tread']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            $mech=$row['mech']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            $md=$row['mismatchedDual']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            $vc=$row['valveCondition']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            $rc=$row['rimCondition']==0?"<i class='fa fa-times'></i>":"<i class='fa fa-check'></i>";
            
            
            echo("<tr><td><input type='hidden' name='thid[]' value='".$row['id']."' />".$row['position']."</td><td>".$row['brand']."</td><td>".$row['pattern']."</td><td>".$row['size']."</td><td>".$row['serialNo']."</td><td>".$row['nr']."</td><td>".$row['treadDepth']."</td><td>".$row['pressure']."</td><td>".$sd."</td><td>".$td2."</td><td>".$mech."</td><td>".$md."</td><td>".$vc."</td><td>".$rc."</td><td>".$row['generalRemarks']."</td></tr>");
         }
         echo "</table>";
} else if($controller_templet->getAction()=='fix')
{
	$ti=new tireInspectionHeaderService();
	$ti->generateNextReading();
	
}

} ?>