<?php
session_start();
 require_once('../services/localpurchaseorderService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $localpurchaseorder=new localpurchaseorderService();
		 if(isset($_REQUEST['id']))
         {
             $localpurchaseorder->setprfId($_REQUEST['id']);
             echo json_encode( $localpurchaseorder->view());
         }else
         {
             $localpurchaseorder->setprfId(-1);
             echo json_encode( $localpurchaseorder->view());
         }

 }else if($controller_templet->getAction()=='add'){

	 	 $localpurchaseorder=new localpurchaseorderService();
         $localpurchaseorder->setid($_REQUEST['lopNo']);
 	 	 $localpurchaseorder->setsupplieId($_REQUEST['supplieId']);
	 	 $localpurchaseorder->setcreationDate($_REQUEST['creationDate']);
	 	 $localpurchaseorder->setcreatedBy(date('Y-m-d H:i:s'));
	 	 $localpurchaseorder->setprfId($_REQUEST['prfIdNo']);
	 	 $localpurchaseorder->setisActive(true);
	 	 echo json_encode( $localpurchaseorder->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $localpurchaseorder=new localpurchaseorderService();
 	 	 $localpurchaseorder->setid($_REQUEST['lopNo']);
	 	 $localpurchaseorder->setsupplieId($_REQUEST['supplieId']);
	 	 $localpurchaseorder->setcreationDate($_REQUEST['creationDate']);
	 	 $localpurchaseorder->setcreatedBy($_SESSION['username']);
	 	 $localpurchaseorder->setprfId($_REQUEST['prfIdNo']);
	 	 $localpurchaseorder->setisActive(true);
	 	 echo json_encode($localpurchaseorder->update());
 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $localpurchaseorder=new localpurchaseorderService();
 	 	 $localpurchaseorder->setid($_REQUEST['id']);
echo json_encode( $localpurchaseorder->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $localpurchaseorder=new localpurchaseorderService();
 		 echo json_encode( $localpurchaseorder->viewConbox());
		}else if($controller_templet->getAction()=='generateLopNumber')
 {
     $localpurchaseorder=new localpurchaseorderService();
     echo $localpurchaseorder->generateNumber();
 }
} ?>