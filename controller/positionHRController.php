<?php 
 require_once('../services/positionHRService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $positionHR=new positionHRService();
 		 echo json_encode( $positionHR->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $positionHR=new positionHRService();
 	 	 $positionHR->setposition($_REQUEST['position']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $positionHR->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $positionHR->setIsActive(false);
		 }

	 	 echo json_encode( $positionHR->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $positionHR=new positionHRService();
 	 	 $positionHR->setid($_REQUEST['id']);
	 	 $positionHR->setposition($_REQUEST['position']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $positionHR->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $positionHR->setIsActive(false);
		 }
	 	 echo json_encode( $positionHR->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $positionHR=new positionHRService();
 	 	 $positionHR->setid($_REQUEST['id']);
echo json_encode( $positionHR->delete()); 
}else if($controller_templet->getAction()=='viewCombo')
 {  	$positionHR=new positionHRService();
 		 echo json_encode( $positionHR->viewCombox());
 }
} ?>