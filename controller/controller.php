<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of Controller
 *
 * @author plussoft
 */
//session_start();
//require_once('services/auditTrailService2.php');
class Controller {
    
    private $urlHeader;
    private $viewHeader;
    private $action;
    
    function __construct($action) {

        $this->action = $action;

        //get the way to box out the user

        //$_SESSION['LAST_ACTIVITY'] = time();

    }
    
    public function getUrlHeader() {
        return $this->urlHeader;
    }

    public function getViewHeader() {
        return $this->viewHeader;
    }

    public function getAction() {
        return $this->action;
    }

    public function setUrlHeader($urlHeader) {
        $this->urlHeader = $urlHeader;
    }

    public function setViewHeader($viewHeader) {
        $this->viewHeader = $viewHeader;
    }

    public function setAction($action) {
        $this->action = $action;
    }

    public function redirect()
    {
        header("location:".$this->urlHeader);
    }
    
    public function render(){
        if(file_exists($this->viewHeader))
        {
            include_once $this->viewHeader;
        }else
        {
            //include_once '';
            echo "File Not 404";
        }
    }
    
    public function  filter_inputs($input){
        return isset($input)?$input:NULL;
    }
	public function includeJavascriptFile($script){
		echo"<script type='application/javascript' src='".$script."'></script>";
	}
    //put your code here
    public function returnResult($successCount,$failureCount,$resultArray,$errorMsg)
    {
        if(array_key_exists('success',$resultArray))
        {
            $success=$successCount+1;
        }else
        {
            $failure=$failureCount+1;
            $errorMsg.=" ".$resultArray['msg'];
        }
    }
}
?>
