<?php 
 require_once('../services/employeeBankService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $employeeBank=new employeeBankService();
		 $employeeBank->setempNo($_REQUEST['empNo']);
 		 echo json_encode( $employeeBank->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $employeeBank=new employeeBankService();
 	 	 $employeeBank->setempNo($_REQUEST['empNo']);
	 	 $employeeBank->setbank($_REQUEST['bank']);
	 	 $employeeBank->setbranch($_REQUEST['branch']);
	 	 $employeeBank->setaccountName($_REQUEST['accountName']);
	 	 $employeeBank->setaccountNumber($_REQUEST['accountNumber']);
	 	 $employeeBank->setnssfNumber($_REQUEST['nssfNumber']);
	 	 $employeeBank->setIsactive($_REQUEST['Isactive']);
	 	 echo json_encode( $employeeBank->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $employeeBank=new employeeBankService();
 	 	 $employeeBank->setid($_REQUEST['id']);
	 	 $employeeBank->setempNo($_REQUEST['empNo']);
	 	 $employeeBank->setbank($_REQUEST['bank']);
	 	 $employeeBank->setbranch($_REQUEST['branch']);
	 	 $employeeBank->setaccountName($_REQUEST['accountName']);
	 	 $employeeBank->setaccountNumber($_REQUEST['accountNumber']);
	 	 $employeeBank->setnssfNumber($_REQUEST['nssfNumber']);
	 	 $employeeBank->setIsactive($_REQUEST['Isactive']);
	 	 echo json_encode( $employeeBank->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $employeeBank=new employeeBankService();
 	 	 $employeeBank->setid($_REQUEST['id']);
echo json_encode( $employeeBank->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $employeeBank=new employeeBankService();
 		 echo json_encode( $employeeBank->viewConbox());
		}   
} ?>