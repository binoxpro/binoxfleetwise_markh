<?php 
 require_once('../services/puService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $pu=new puService();
 		 echo json_encode( $pu->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $pu=new puService();
 	 	 $pu->setnumberplate($_REQUEST['numberplate']);
	 	 $pu->setpartusedId($_REQUEST['partusedId']);
	 	 $pu->setquantity($_REQUEST['quantity']);
	 	 $pu->setcost($_REQUEST['cost']);
	 	 $pu->setjobcardId($_REQUEST['jobcardId']);
	 	 $pu->setdateofrepair($_REQUEST['dateofrepair']);
	 	 $pu->settypeofrepair($_REQUEST['typeofrepair']);
	 	 $pu->setsupplierId($_REQUEST['supplierId']);
	 	 $pu->setsupplied($_REQUEST['supplied']);
	 	 echo json_encode( $pu->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $pu=new puService();
 	 	 $pu->setid($_REQUEST['id']);
	 	 $pu->setnumberplate($_REQUEST['numberplate']);
	 	 $pu->setpartusedId($_REQUEST['partusedId']);
	 	 $pu->setquantity($_REQUEST['quantity']);
	 	 $pu->setcost($_REQUEST['cost']);
	 	 $pu->setjobcardId($_REQUEST['jobcardId']);
	 	 $pu->setdateofrepair($_REQUEST['dateofrepair']);
	 	 $pu->settypeofrepair($_REQUEST['typeofrepair']);
	 	 $pu->setsupplierId($_REQUEST['supplierId']);
	 	 $pu->setsupplied($_REQUEST['supplied']);
	 	 echo json_encode( $pu->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $pu=new puService();
 	 	 $pu->setid($_REQUEST['id']);
echo json_encode( $pu->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $pu=new puService();
 		 echo json_encode( $pu->viewConbox());
		}   
} ?>