<?php 
 require_once('../services/financialMonthService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $financialMonth=new financialMonthService();
 		 echo json_encode( $financialMonth->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $financialMonth=new financialMonthService();
 	 	 $financialMonth->setyearId($_REQUEST['yearId']);
	 	 $financialMonth->setstartDate($_REQUEST['startDate']);
	 	 $financialMonth->setendDate($_REQUEST['endDate']);
	 	 $financialMonth->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $financialMonth->save());
 }
 else if($controller_templet->getAction()=='edit'){
		$financialMonth=new financialMonthService();
	 	$financialMonth->setisActive(false);
	 	$financialMonth->updateAllFinancialMonth();

 	 	 $financialMonth=new financialMonthService();
 	 	 $financialMonth->setid($_REQUEST['id']);
	 	 $financialMonth->setyearId($_REQUEST['yearId']);
	 	 $financialMonth->setstartDate($_REQUEST['startDate']);
	 	 $financialMonth->setendDate($_REQUEST['endDate']);
	 	 $financialMonth->setisActive(true);
	 	 echo json_encode( $financialMonth->update());
 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $financialMonth=new financialMonthService();
 	 	 $financialMonth->setid($_REQUEST['id']);
echo json_encode( $financialMonth->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $financialMonth=new financialMonthService();
 		 echo json_encode( $financialMonth->viewConbox());
		}   
} ?>