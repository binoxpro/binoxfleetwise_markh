<?php 
 require_once('../services/tripTypeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action']))
 {
 	$controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
			 $tripType=new tripTypeService();
			echo json_encode( $tripType->view());
	 }else if($controller_templet->getAction()=='add')
	 {
			 $tripType=new tripTypeService();
			 $tripType->setName($_REQUEST['Name']);
			$tripType->setPrefix($_REQUEST['Prefix']);
			 $tripType->setIsActive($_REQUEST['IsActive']);
			 echo json_encode( $tripType->save());
	 }
	 else if($controller_templet->getAction()=='edit')
	 {
			 $tripType=new tripTypeService();
			 $tripType->setid($_REQUEST['id']);
			 $tripType->setName($_REQUEST['Name']);
			$tripType->setPrefix($_REQUEST['Prefix']);
			 $tripType->setIsActive($_REQUEST['IsActive']);
			 echo json_encode( $tripType->update());
	 }
	 else if($controller_templet->getAction()=='delete')
	 {
			 $tripType=new tripTypeService();
			 $tripType->setid($_REQUEST['id']);
			echo json_encode( $tripType->delete());
	 }
} ?>