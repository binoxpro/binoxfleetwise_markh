<?php
ob_start();
//require_once '../services/accountService.php';
require_once 'controller.php';
require_once('services/auditTrailService2.php');
require_once('utility.php');
if(isset($_REQUEST['view'])){
    $controller_templet=new Controller($_REQUEST['view']);
	$auditTrail=new auditTrailService();
	$auditTrail->setcreationdate(date('Y-m-d'));
	$auditTrail->settimeCreation(date('H:i:s'));
	//$auditTrail->setIpadderess($_SERVER['REMOTE_ADDR']);

	$url='admin.php?view='.$_REQUEST['view'];
	$auditTrail->seturl($url);
	$auditTrail->setuserId($_SESSION['userId']);
	$auditTrail->save();
    if($controller_templet->getAction()=="System_Administration_Vehicle_Management")
	{
		$controller_templet->setViewHeader('view/vehicleNew/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/vehicleNew/add.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="System_Administration_Vehicle_Types")
	{
		$controller_templet->setViewHeader('view/vehicleType/view.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="System_Administration_CheckList_Management")
	{
		$controller_templet->setViewHeader('view/checkListType/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="System_Administration_CheckListType_Management")
	{
		$controller_templet->setViewHeader('view/section/viewlist.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Create_JobCard")
	{
		$controller_templet->setViewHeader('view/jobCard/add.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Edit_JobCard")
	{
		$controller_templet->setViewHeader('view/jobCard/edit.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Complete_JobCard")
	{
		$controller_templet->setViewHeader('view/jobCard/complete.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Follow_Up_JobCard")
	{
		$controller_templet->setViewHeader('view/jobCard/followup.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="System_Administration_Trailer_Management")
	{
		$controller_templet->setViewHeader('view/trailer/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="vehicle_Load")
	{
		$controller_templet->setViewHeader('view/capacity/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Utilisation_Report")
	{
		$controller_templet->setViewHeader('view/capacity/report.php');
		$controller_templet->render();
		//Utilisation_Graphy
	}else if($controller_templet->getAction()=="Utilisation_Graphy")
	{
		$controller_templet->setViewHeader('view/capacity/viewGraphy.php');
		$controller_templet->render();
		//Utilisation_Graphy
	}
	else if($controller_templet->getAction()=="Utilisation_vGraphy")
	{
		$controller_templet->setViewHeader('view/capacity/vGraphy.php');
		$controller_templet->render();
		//Utilisation_Graphy
	}
	else if($controller_templet->getAction()=="Manage_Order")
	{
		$controller_templet->setViewHeader('view/order/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Allocation")
	{
		$controller_templet->setViewHeader('view/allocation/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Dispatcher")
	{
		$controller_templet->setViewHeader('view/truckDispatcher/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Delivery")
	{
		$controller_templet->setViewHeader('view/delivery/view.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="IOV_Inspection")
	{
		$controller_templet->setViewHeader('view/inpsection/view.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="add_jobcard_items")
	{
		$controller_templet->setViewHeader('view/jobcarditem/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Access_Control")
	{
		$controller_templet->setViewHeader('view/user/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Fuel")
	{
		$controller_templet->setViewHeader('view/fuel/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Tyre_Type")
	{
		$controller_templet->setViewHeader('view/tyreType/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Odometer")
	{
		$controller_templet->setViewHeader('view/odometer/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Tyre")
	{
		$controller_templet->setViewHeader('view/tyre/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Other_Cost")
	{
		$controller_templet->setViewHeader('view/otherCost/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Other_Cost_Management")
	{
		$controller_templet->setViewHeader('view/othercostManagement/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="complete_jobcard_items")
	{
        $controller_templet->setViewHeader('view/jobcarditem/view.php');
        $controller_templet->render();
	}
	else if($controller_templet->getAction()=="complete_jobcard_items")
	{
		$controller_templet->setViewHeader('view/jobCard/completeView.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Notification")
	{
		$controller_templet->setViewHeader('view/notification/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Vehicle_History_Report")
	{
		$controller_templet->setViewHeader('view/vehicle/history.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Parts_used_Report")
	{
		$controller_templet->setViewHeader('view/jobCard/graphyBar.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Vehicle_Kilometer_Graph")
	{
		$controller_templet->setViewHeader('view/odometer/graphyBar.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Total_Order_Delivered")
	{
		$controller_templet->setViewHeader('view/order/graphyBar.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Products_Delivered")
	{
		$controller_templet->setViewHeader('view/order/graphyBarQty.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Running_Cost_Report")
	{
		$controller_templet->setViewHeader('view/othercostManagement/history.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Time_Utilisation_Report")
	{
		$controller_templet->setViewHeader('view/capacity/reportTime.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Service_Management")
	{
		$controller_templet->setViewHeader('view/service/view.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="upload")
	{
		$controller_templet->setViewHeader('view/order/fileUpload.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="uploadDelivery")
	{
		$controller_templet->setViewHeader('view/order/fileUploadD.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Driver_Registration")
	{
		$controller_templet->setViewHeader('view/driver/add.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="licence_management")
	{
		$controller_templet->setViewHeader('view/driver/licence.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Defence_Driving_Course")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="SOPAF")
	{
		$controller_templet->setViewHeader('view/driver/wizard2.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Fitness_and_Medical_Examination")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Fire_Fighting_Training")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="First_Aid")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Journey_Management_Training")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Road_Transport_Safety")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Driver_Assement")
    {
        $controller_templet->setViewHeader('view/driver/wizard2.php');
        $controller_templet->render();
        //Defence_Driving_Course
    }else if($controller_templet->getAction()=="Induction")
    {
        $controller_templet->setViewHeader('view/driver/wizard2.php');
        $controller_templet->render();
        //Defence_Driving_Course
    }else if($controller_templet->getAction()=="Emergency_Response")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		//Defence_Driving_Course
	}else if($controller_templet->getAction()=="Dangerous_Goods_Handling")
	{
		$controller_templet->setViewHeader('view/driver/wizard.php');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="Nothing")
	{
		$controller_templet->setViewHeader('view/driver/end.php');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="Vehicle_Standards_Management")
	{
		$controller_templet->setViewHeader('view/vehicle/standards.php');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="Driver_Records")
	{
		$controller_templet->setViewHeader('view/driver/training.php');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="upload_Odometer_File")
	{
		$controller_templet->setViewHeader('view/odometer/fileUpload.php');
		$controller_templet->render();
		//upload_tyre_data
	}else if($controller_templet->getAction()=="checkList_download")
	{
		$controller_templet->setViewHeader('view/checkListType/download.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Inspection_List")
	{
		$controller_templet->setViewHeader('view/inpsection/list.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_of_License")
	{
		$controller_templet->setViewHeader('view/license/view.php');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Driver_Training")
	{
		$controller_templet->setViewHeader('view/training/view.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="upload_tyre_data")
	{
		$controller_templet->setViewHeader('view/tyre/fileUpload.php');
		$controller_templet->render();
		//upload_tyre_data
	}
	else if($controller_templet->getAction()=="upload_service_data")
	{
		$controller_templet->setViewHeader('view/service/fileUpload.php');
		$controller_templet->render();
		//upload_tyre_data
	}else if($controller_templet->getAction()=="Spare_Parts_Management")
	{
		$controller_templet->setViewHeader('view/spare/view.php');
		$controller_templet->render();
		//upload_tyre_data
	}else if($controller_templet->getAction()=="upload_spare_data")
	{
		$controller_templet->setViewHeader('view/spare/fileUpload.php');
		$controller_templet->render();
		
	}
	else if($controller_templet->getAction()=="licence_Management")
	{
		$controller_templet->setViewHeader('view/license/viewLicence.php');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="licence_Management2")
	{
		$controller_templet->setViewHeader('view/license/fileUpload.php');
		$controller_templet->render();
		//upload_Fuel_Datasheet
		
	}else if($controller_templet->getAction()=="upload_Fuel_Datasheet")
	{
		$controller_templet->setViewHeader('view/fuel/fileUpload.php');
		$controller_templet->render();
		//upload_Fuel_Datasheet
		//upload_Data
	}else if($controller_templet->getAction()=="upload_Fuel_Datasheet")
	{
		$controller_templet->setViewHeader('view/fuel/fileUpload.php');
		$controller_templet->render();
		//upload_Fuel_Datasheet
		//upload_Data
	}else if($controller_templet->getAction()=="upload_other_cost")
	{
		$controller_templet->setViewHeader('view/othercostManagement/fileUpload.php');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Manage_Model")
	{
		$controller_templet->setViewHeader('view/model/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/model/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Manage_Component")
	{
		$controller_templet->setViewHeader('view/component/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/component/add.html');
		$controller_templet->render();
		//Manage_service_type
	}else if($controller_templet->getAction()=="Manage_service_type")
	{
		$controller_templet->setViewHeader('view/serviceType/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/serviceType/add.html');
		$controller_templet->render();
		//Manage_Action_Notes
	}else if($controller_templet->getAction()=="Manage_Action_Notes")
	{
		$controller_templet->setViewHeader('view/actionNote/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/actionNote/add.html');
		$controller_templet->render();
		
	}else if($controller_templet->getAction()=="Manage_Service_Interval")
	{
		$controller_templet->setViewHeader('view/serviceInterval/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/serviceInterval/add.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Service_Management_New")
	{
		$controller_templet->setViewHeader('view/serviceHeader/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/serviceHeader/add.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="Manage_Driver_Training_new")
	{
		$controller_templet->setViewHeader('view/trainingTracker/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/trainingTracker/add.html');
		$controller_templet->render();
		//Driver_Registration_New
	}
	else if($controller_templet->getAction()=="Driver_Registration_New")
	{
		$controller_templet->setViewHeader('view/driverNew/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/driverNew/add.html');
		$controller_templet->render();
		//Driver_Registration_New
	}else if($controller_templet->getAction()=="manage_stock")
	{
		$controller_templet->setViewHeader('view/stock/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/stock/add.html');
		$controller_templet->render();
		//Driver_Registration_New
	}else if($controller_templet->getAction()=="scrapped_stock")
	{
		$controller_templet->setViewHeader('view/scrapped/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/scrapped/add.html');
		$controller_templet->render();
		//Driver_Registration_New
	}else if($controller_templet->getAction()=="used_stock")
	{
		$controller_templet->setViewHeader('view/stock/viewUsed.html');
		$controller_templet->render();
		//$controller_templet->setViewHeader('view/stock/add.html');
		//$controller_templet->render();
		//Driver_Registration_New
	}else if($controller_templet->getAction()=="Manage_Tire_size")
	{
		$controller_templet->setViewHeader('view/tireSize/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tireSize/add.html');
		$controller_templet->render();
		//Driver_Registration_New
	}else if($controller_templet->getAction()=="Manage_Tire_Pattern")
	{
        $controller_templet->setViewHeader('view/tirePattern/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tirePattern/add.html');
		$controller_templet->render();
    }else if($controller_templet->getAction()=="Purchase_Tire")
	{
        $controller_templet->setViewHeader('view/tire/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tire/add.html');
		$controller_templet->render();
    }else if($controller_templet->getAction()=="Mount_Tire")
	{
        $controller_templet->setViewHeader('view/tireHold/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tireHold/add.html');
		$controller_templet->render();
    }else if($controller_templet->getAction()=="retread_return_list")
	{
        $controller_templet->setViewHeader('view/tireRetread/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tireRetread/add.html');
		$controller_templet->render();
        $controller_templet->setViewHeader('view/tireRetread/addInvoice.php');
		$controller_templet->render();
    }
    else if($controller_templet->getAction()=="scrapped_tire")
	{
        $controller_templet->setViewHeader('view/tireScrapped/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tireScrapped/add.html');
		$controller_templet->render();
        $controller_templet->setViewHeader('view/tireScrapped/addInvoice.php');
		$controller_templet->render();
    } else if($controller_templet->getAction()=="tire_inspection")
	{
        //$controller_templet->setViewHeader('view/tireInspectionHeader/view.html');
		//$controller_templet->render();
		$controller_templet->setViewHeader('view/tireInspectionHeader/add.html');
		$controller_templet->render();
        
    } else if($controller_templet->getAction()=="inspect_vehicle_tire")
	{
	   $controller_templet->setViewHeader('view/tireInspectionDetails/add.html');
	   $controller_templet->render();
	   
    }else if($controller_templet->getAction()=="tire_inspection_history")
	{
	   $controller_templet->setViewHeader('view/tireInspectionHeader/view.html');
	   $controller_templet->render();
	   
    }else if($controller_templet->getAction()=="tire_monitoring")
	{
	   $controller_templet->setViewHeader('view/tireHold/viewMonitor.html');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="tire_Rotate")
	{
	   $controller_templet->setViewHeader('view/tireRotation/addRotation.html');
	   $controller_templet->render();
       //tire_history
    }else if($controller_templet->getAction()=="tire_history")
	{
	   $controller_templet->setViewHeader('view/reports/tireReport.php');
	   $controller_templet->render();
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="Manage_Order_Dispatch")
	{
	   $controller_templet->setViewHeader('view/newOrder/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/newOrder/add.html');
	   $controller_templet->render();
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="manage_trip_type")
	{
	   $controller_templet->setViewHeader('view/tripType/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/tripType/add.html');
	   $controller_templet->render();
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="trip_Number_manger")
	{
	   $controller_templet->setViewHeader('view/tripNumberManager/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/tripNumberManager/add.html');
	   $controller_templet->render();
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="manage_customer_list")
	{
	   $controller_templet->setViewHeader('view/customerNew/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/customerNew/add.html');
	   $controller_templet->render();
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="Manage_Delivery_New")
	{
	   $controller_templet->setViewHeader('view/newOrder/viewDelivery.html');
	   $controller_templet->render();
       
       //Manage_Order_Dispatch
    }else if($controller_templet->getAction()=="Manage_Order_New")
	{
	   $controller_templet->setViewHeader('view/newOrder/viewOrder.html');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="tire_summary")
	{
	   $controller_templet->setViewHeader('view/report/tireSummary.html');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="mounted_tire_report")
	{
	   $controller_templet->setViewHeader('view/report/mountedReport.php');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="tire_brand_summary")
	{
	   $controller_templet->setViewHeader('view/report/tireBrandSummary.html');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="tire_history_by_Vehicle")
	{
	   $controller_templet->setViewHeader('view/report/tireHistoryByTruck.html');
	   $controller_templet->render();
	   //tire_history_by_tire
	   
    }else if($controller_templet->getAction()=="tire_history_by_tire")
	{
	   $controller_templet->setViewHeader('view/report/tireHistoryByTire.html');
	   $controller_templet->render();
	   //tire_history_by_tire
	   
    }else if($controller_templet->getAction()=="tire_inventory_listing")
	{
	   $controller_templet->setViewHeader('view/report/tireInventory.html');
	   $controller_templet->render();
	   //tire_history_by_tire
	   
    }else if($controller_templet->getAction()=="manage_module")
	{
	   $controller_templet->setViewHeader('view/activity/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/activity/add.html');
	   $controller_templet->render();
	   
    }else if($controller_templet->getAction()=="manage_feature")
	{
	   $controller_templet->setViewHeader('view/subActivity/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/subActivity/add.html');
	   $controller_templet->render();
    }else if($controller_templet->getAction()=="manage_access")
	{
	   $controller_templet->setViewHeader('view/roleManagement/view.html');
	   $controller_templet->render();
       $controller_templet->setViewHeader('view/roleManagement/add.html');
	   $controller_templet->render();
    }
    else if($controller_templet->getAction()=="manage_account")
	{
		$controller_templet->setViewHeader('view/systemUser/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/systemUser/add.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="manage_cargo_type")
	{
		$controller_templet->setViewHeader('view/cargoType/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/cargoType/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_department")
	{
		$controller_templet->setViewHeader('view/department/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/department/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_client")
	{
		$controller_templet->setViewHeader('view/client/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/client/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_workorder")
	{
		$controller_templet->setViewHeader('view/workorder/view.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="prepare_workorder")
	{
		$controller_templet->setViewHeader('view/workorder/add.html');
		$controller_templet->render();
		//view=manage_make
	}else if($controller_templet->getAction()=="manage_make")
	{
		$controller_templet->setViewHeader('view/truckMake/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/truckMake/view.html');
		$controller_templet->render();
		//manage_vehicle_model

	}else if($controller_templet->getAction()=="manage_vehicle_model")
	{
		$controller_templet->setViewHeader('view/truckModel/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/truckModel/view.html');
		$controller_templet->render();
		//view=vehicle_configuration

	}else if($controller_templet->getAction()=="vehicle_configuration")
	{
		$controller_templet->setViewHeader('view/vehicleConfiguration/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/vehicleConfiguration/view.html');
		$controller_templet->render();
		//view=vehicle_configuration

	}else if($controller_templet->getAction()=="setup_position")
	{
		$controller_templet->setViewHeader('view/positionHR/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/positionHR/view.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="setup_account_category")
	{
		$controller_templet->setViewHeader('view/accountType/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/accountType/view.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="chart_of_accounts")
	{
		$controller_templet->setViewHeader('view/chartOfaccount/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/chartOfaccount/view.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="individual_account")
	{
		$controller_templet->setViewHeader('view/individualAccount/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/individualAccount/view.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="setup_allowance")
	{
		$controller_templet->setViewHeader('view/allowance/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/allowance/view.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="Register_Employee")
	{
		$controller_templet->setViewHeader('view/employee/add.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="record_employee")
	{
		$controller_templet->setViewHeader('view/employee/view.html');
		$controller_templet->render();
		//driver_Vehicle_assignment
	}else if($controller_templet->getAction()=="driver_Vehicle_assignment")
	{
		$controller_templet->setViewHeader('view/driverVehicle/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/driverVehicle/add.html');
		$controller_templet->render();
		//driver_Vehicle_assignment
		//trailer_management
	}else if($controller_templet->getAction()=="trailer_management")
	{
		$controller_templet->setViewHeader('view/trailerAssigment/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/trailerAssigment/add.html');
		$controller_templet->render();
		//driver_Vehicle_assignment
		//trailer_management
	}else if($controller_templet->getAction()=="setup_fuel_and_lubes")
	{
		$controller_templet->setViewHeader('view/fuelProduct/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/fuelProduct/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="expense_Template")
	{
		$controller_templet->setViewHeader('view/expenseTemplate/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/expenseTemplate/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="trip_generation")
	{
		$controller_templet->setViewHeader('view/trip/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/trip/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/tripOtherExpense/viewTripExpense.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/workorder/workOrderTrip.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/workorder/workOrderTripAdd.html');
		$controller_templet->render();
		//manage_supplier
	}else if($controller_templet->getAction()=="manage_supplier")
	{
		$controller_templet->setViewHeader('view/supplier/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/supplier/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_fuel_and_lubes")
	{
		$controller_templet->setViewHeader('view/fuelOrder/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/fuelOrder/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/fuelOrder/fuelOrderTrip.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_financial_year")
	{
		$controller_templet->setViewHeader('view/financialYear/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/financialYear/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_financial_month")
	{
		$controller_templet->setViewHeader('view/financialMonth/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/financialMonth/add.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="manage_payment_voucher")
	{
		//$controller_templet->setViewHeader('view/paymentVoucher/view.html');
		//$controller_templet->render();
		$controller_templet->setViewHeader('view/paymentVoucher/voucher.html');
		$controller_templet->render();
		//payment_voucher_list
	}else if($controller_templet->getAction()=="payment_voucher_list")
	{
		$controller_templet->setViewHeader('view/paymentVoucher/view.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="transport_rate")
	{
		$controller_templet->setViewHeader('view/transportRate/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/transportRate/add.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="manage_pay_rate")
	{
		$controller_templet->setViewHeader('view/payRate/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/payRate/add.html');
		$controller_templet->render();
		//manage_debitor
	}else if($controller_templet->getAction()=="manage_debitor")
	{
		$controller_templet->setViewHeader('view/debitor/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/debitor/add.html');
		$controller_templet->render();
		//manage_debitor
	}else if($controller_templet->getAction()=="create_invoice")
	{
		$controller_templet->setViewHeader('view/invoice/add.html');
		$controller_templet->render();

		//manage_debitor
		//setup_currency
	}else if($controller_templet->getAction()=="setup_currency")
	{
		$controller_templet->setViewHeader('view/currency/add.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/currency/view.html');
		$controller_templet->render();

		//manage_debitor
		//setup_currency
	}else if($controller_templet->getAction()=="manage_invoice")
	{
		$controller_templet->setViewHeader('view/invoice/view.html');
		$controller_templet->render();
		//manage_debitor
		//setup_currency
	}else if($controller_templet->getAction()=="setup_vehicle_system")
	{
		$controller_templet->setViewHeader('view/vehicleSystem/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/vehicleSystem/add.html');
		$controller_templet->render();
		//manage_debitor
		//setup_currency
		//setup_spare_part
	}else if($controller_templet->getAction()=="setup_spare_part")
	{
		$controller_templet->setViewHeader('view/sparePart/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/sparePart/add.html');
		$controller_templet->render();
		//manage_debitor
		//setup_currency
		//setup_spare_part
	}else if($controller_templet->getAction()=="manage_supplier_statement")
	{
		$controller_templet->setViewHeader('view/supplier/statement.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="jobcard_report")
	{
		$controller_templet->setViewHeader('view/report/jobcardReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="jobcard_summary")
	{
		$controller_templet->setViewHeader('view/report/jobcardSummaryReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="spare_against_model")
	{
		$controller_templet->setViewHeader('view/report/spareAgainstModelReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="pm_report")
	{
		$controller_templet->setViewHeader('view/report/pmReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="service_occurance_report")
	{
		$controller_templet->setViewHeader('view/report/serviceOccuranceReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="profitability_report")
	{
		$controller_templet->setViewHeader('view/report/tripProfitabilityReport.html');
		$controller_templet->render();
	}
	else if($controller_templet->getAction()=="running_cost_report")
	{
		$controller_templet->setViewHeader('view/report/tripProfitabilitySummaryReport.html');
		$controller_templet->render();
	}else if($controller_templet->getAction()=="licence_Manager")
	{
		$controller_templet->setViewHeader('view/licenceManager/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/licenceManager/add.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="truck_running_cost_report")
	{
		$controller_templet->setViewHeader('view/report/truckRunningCostReport.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="setup_start_balance")
	{
		$controller_templet->setViewHeader('view/startBalance/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/startBalance/add.html');
		$controller_templet->render();

	}else if($controller_templet->getAction()=="distance")
	{
		$controller_templet->setViewHeader('view/distance/view.html');
		$controller_templet->render();
		$controller_templet->setViewHeader('view/distance/add.html');
		$controller_templet->render();
		//view=import_fleet_data

	}else if($controller_templet->getAction()=="import_fleet_data")
    {
        $controller_templet->setViewHeader('view/vehicleNew/uploadFleet.html');
        $controller_templet->render();

        //view=import_fleet_data

    }else if($controller_templet->getAction()=="import_odometer")
    {
        $controller_templet->setViewHeader('view/odometer/uploadOdometer.html');
        $controller_templet->render();

        //view=import_fleet_data

    }else if($controller_templet->getAction()=="import_service")
    {
        $controller_templet->setViewHeader('view/serviceHeader/uploadService.html');
        $controller_templet->render();
        //view=import_fleet_data

    }else if($controller_templet->getAction()=="import_trailer_assignment")
    {
        $controller_templet->setViewHeader('view/vehicleNew/uploadAssignment.html');
        $controller_templet->render();
        //view=import_fleet_data
        //import_workorder
    }else if($controller_templet->getAction()=="import_workorder")
    {
        $controller_templet->setViewHeader('view/workorder/uploadWorkOrder.html');
        $controller_templet->render();
        //view=import_fleet_data
        //import_workorder
    }
    else if($controller_templet->getAction()=="import_tire")
    {
        $controller_templet->setViewHeader('view/tire/uploadTire.html');
        $controller_templet->render();
        //view=import_fleet_data
        //import_workorder
        //view=store
    }
    else if($controller_templet->getAction()=="store")
    {
        $controller_templet->setViewHeader('view/store/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/store/add.html');
        $controller_templet->render();

    }else if($controller_templet->getAction()=="item_category")
    {
        $controller_templet->setViewHeader('view/itemcategory/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/itemcategory/add.html');
        $controller_templet->render();
        //item_category
        //
    }else if($controller_templet->getAction()=="uom")
    {
        $controller_templet->setViewHeader('view/uom/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/uom/add.html');
        $controller_templet->render();
        //item_category
        //uom
        //items
    }else if($controller_templet->getAction()=="items")
    {
        $controller_templet->setViewHeader('view/item/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/item/add.html');
        $controller_templet->render();
        //wpf
    }else if($controller_templet->getAction()=="wpf")
    {
        $controller_templet->setViewHeader('view/wpf/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/wpf/add.html');
        $controller_templet->render();
    }
    else if($controller_templet->getAction()=="store_process_status")
    {
        $controller_templet->setViewHeader('view/storeproces/view.html');
        $controller_templet->render();

    }
    else if($controller_templet->getAction()=="Receive_Part_Request_Form")
    {
        $controller_templet->setViewHeader('view/partRequestForm/viewPreview.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/partRequestForm/partRequestForm.html');
        $controller_templet->render();
    }else if($controller_templet->getAction()=="issue_parts")
    {
        $controller_templet->setViewHeader('view/partRequestForm/viewIusse.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/partRequestForm/partRequestFormIssue.html');
        $controller_templet->render();

    }else if($controller_templet->getAction()=="create_lop")
    {
        $controller_templet->setViewHeader('view/partRequestForm/viewlop.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/partRequestForm/partRequestFormlop.html');
        $controller_templet->render();
    }else if($controller_templet->getAction()=="opening_stock")
    {
        $controller_templet->setViewHeader('view/storestock/view.html');
        $controller_templet->render();
        $controller_templet->setViewHeader('view/storestock/add.html');
        $controller_templet->render();
    }














}else{
	require_once('view/index.php');
}

?>