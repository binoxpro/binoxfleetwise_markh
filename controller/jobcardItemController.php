<?php

 ob_start();
// session_start();
require_once '../services/jobcardItemService.php';
require_once '../services/jobCardService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$jobcardItem=new JobcardItemService();
        if(isset($_REQUEST['jobCardId'])){
            $jobcardItem->setjobcardId($_REQUEST['jobCardId']);
		    echo json_encode($jobcardItem->view());
        }
	}else if($controller_templet->getAction()=="add")
	{
		$jobcardItem=new JobcardItemService();
		$jobcardItem->setdetails($_REQUEST['details']);
		//$jobcardItem->settimeTaken($_REQUEST['timeTaken']);
		$jobcardItem->setinitials($_REQUEST['initials']);
		$jobcardItem->setjobcardId($_REQUEST['jobCardId']);
		$jobcardItem->setstatus('Pending');
		echo json_encode($jobcardItem->save());
		//$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$jobcardItem->getjobcardId());
		//$controller_templet->redirect();
	}else if($controller_templet->getAction()=="edit")
	{
		$jobcardItem=new JobcardItemService();
        if(isset($_REQUEST['details'])) {
            $jobcardItem->setdetails($_REQUEST['details']);
        }
		//$jobcardItem->settimeTaken($_REQUEST['timeTaken']);
        if(isset($_REQUEST['initials'])) {
            $jobcardItem->setinitials($_REQUEST['initials']);
        }
        if(isset($_REQUEST['id'])) {
            $jobcardItem->setid($_REQUEST['id']);
        }
        if(isset($_REQUEST['jobCardId'])) {
            $jobcardItem->setjobcardId($_REQUEST['jobCardId']);
        }
		if(isset($_REQUEST['repairReview']))
		{
            $jobcardItem->setrepairReview($_REQUEST['repairReview']);
        }

        if(isset($_REQUEST['status']))
        {
            $jobcardItem->setstatus($_REQUEST['status']);
        }

		echo json_encode($jobcardItem->update());
		//$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$jobcardItem->getjobcardId());
		//$controller_templet->redirect();
	}else if($controller_templet->getAction()=="delete")
	{
		$jobcardItem=new JobcardItemService();
		
		$jobcardItem->setid($_REQUEST['id']);
		//$jobcardItem->setjobcardId($_REQUEST['jobcardId']);
		echo json_encode($jobcardItem->delete());
		//$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$jobcardItem->getjobcardId());
		//$controller_templet->redirect();
	}else if($controller_templet->getAction()=="loadItem")
	{
		$jobcardItem=new JobcardItemService();
		$jobcardItem->setjobcardId($_REQUEST['id']);
		$sql="select * from tbljobcarditem where jobcardId='".$jobcardItem->getjobcardId()."'";
		
			
			echo "<form action='controller/jobcardItemController.php' name='jobCardItem'><div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<tr><td>Details</td><td>Time Taken</td><td>Initials</td><td></td></tr>";
			foreach($jobcardItem->view_query($sql) as $row2)
			{
				echo "<tr><td>".$row2['details']."</td><td>".$row2['timeTaken']."</td><td>".$row2['initials']."</td><td><a href='#' class='btn btn-link' onclick='editItem(this.id)' id='".$row2['id']."'>Edit</a><a href='controller/jobcardItemController.php?action=Delete&id=".$row2['id']."&jobcardId=".$row2['jobcardId']."' class='btn btn-link' >Delete</a></td></tr>";	
			}
			
			echo"<tr><td><textarea name='details' id='details' style='width:100%; height:70px;' ></textarea></td><td><input type='text' name='timetaken' id='timetaken'  style='width:100%;' /></td><td><input type='text' name='initials' id='initials' value='' style='width:100%;'/><input type='hidden' name='jobcardId' id='jobcardId' value='".$jobcardItem->getjobcardId()."'/><input type='hidden' name='id' id='id' value=''/></td><td><input type ='submit' name='action' id='action' value='add' class='btn btn-primary' style='width:100%;' /></td></tr>";
			echo"</tbody></table></div></form>";
			
	}else if($controller_templet->getAction()=="loadItem2")
	{
		$jobcardItem=new JobcardItemService();
		$jobcardItem->setjobcardId($_REQUEST['id']);
		$sql="select * from tbljobcarditem where jobcardId='".$jobcardItem->getjobcardId()."'";
		
			
			echo "<form action='controller/jobcardItemController.php' name='jobCardItem'><div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<tr><td>Details</td><td>Time Taken</td><td>Initials</td><td></td></tr>";
			foreach($jobcardItem->view_query($sql) as $row2)
			{
				echo "<tr><td>".$row2['details']."</td><td>".$row2['timeTaken']."</td><td>".$row2['initials']."</td><td><a href='#' class='btn btn-link' onclick='editItem(this.id)' id='".$row2['id']."'>Edit</a><a href='controller/jobcardItemController.php?action=Delete&id=".$row2['id']."&jobcardId=".$row2['jobcardId']."' class='btn btn-link' >Delete</a></td></tr>";	
			}
			
			//echo"<tr><td><textarea name='details' id='details' style='width:100%; height:70px;' ></textarea></td><td><input type='text' name='timetaken' id='timetaken'  style='width:100%;' /></td><td><input type='text' name='initials' id='initials' value='' style='width:100%;'/><input type='hidden' name='jobcardId' id='jobcardId' value='".$jobcardItem->getjobcardId()."'/><input type='hidden' name='id' id='id' value=''/></td><td><input type ='submit' name='action' id='action' value='add' class='btn btn-primary' style='width:100%;' /></td></tr>";
			echo"</tbody></table></div></form>";
			
	}else if($controller_templet->getAction()=="loadItemHeader")
	{
		$jobcard=new JobcardService();
		$jobcard->setid($_REQUEST['id']);
		$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id INNER JOIN tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where j.id='".$jobcard->getid()."'";
		
			$checkListId=-1;
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'><tbody>";

			foreach($jobcard->view_query($sql) as $row2)
			{
				$checkListId=$row2['checklistId'];
				echo"<tr><td>Serial No:</td><td colspan='5'><span class='text-danger' style=''>".$row2['jobNo']."</span></td></tr>";
				echo"<tr><td>VEHICLE REG. NO.</td><td>".$row2['regNo']."</td><td>MAKE</td><td>".$row2['model']."</td><td>Route</td><td></td></tr>";
				echo"<tr><td>MILEAGE</td><td>".$row2['odometer']."</td><td>TIME:</td><td>".$row2['time']."</td><td>DATE</td><td>".$row2['creationDate']."</td></tr>";
				//echo"<tr><td>".$row2['creationDate']."</td><td>".$row2['regNo']."</td><td>".$row2['model']."</td><td>&nbsp;</td><td>&nbsp;</td><td>".$row2['odometer']."</td></tr>";

			}
			echo"</tbody></table></div><div></div>";
			if($checkListId>0){
			
			$sql2="select ci.*,si.* from tblcheckedItem ci inner join tblsectionitem si on ci.sectionItemId =si.id where ci.checklistId='$checkListId'";
			//$sql2="select * from tblsectionitem where sectionId='".$sectionId."'";
			echo "<h3>Inspection Details</h3>";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'><thead><tr><th>Item</th><th>Item Description</th><th>Comment</th></tr></thead><tbody>";
			foreach($jobcard->view_query($sql2) as $row2)
			{
				echo "<tr><td>".$row2['itemName']."</td><td>".$row2['itemDescription']."</td><td>".$row2['comment']."</td></tr>";	
			}
			echo"</tbody></table></div>";
			}
			//echo"<tr><td><textarea name='details' style='width:100%; height:70px;' ></textarea></td><td><input type='text' name='timetaken' class='timespinner' /></td><td><input type='text' name='initials' value=''/><input type='hidden' name='jobcardId' value='".$jobcardItem->getjobcardId()."'/></td><td><input type ='submit' name='action' value='add' class='btn btn-primary' style='width:100%;' /></td></tr>";
			
			
	}else if($controller_templet->getAction()=="getItem")
	{
		$jobcardItem=new JobcardItemService();
		$jobcardItem->setid($_REQUEST['id']);
		$sql="select * from tbljobcardItem where id='".$jobcardItem->getid()."'";
		$data=array();
		foreach($jobcardItem->view_query($sql) as $row)
		{
			$data2=array();
			$data['details']=$row['details'];
			$data['timeTaken']=$row['timeTaken'];
			$data['initials']=$row['initials'];
			//array_push($data,$data2);
		}
		echo json_encode($data);
		
	}
}
?>