<?php 
 require_once('../services/cargoTypeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $cargoType=new cargoTypeService();
 		echo json_encode( $cargoType->view());
 }
 else if($controller_templet->getAction()=='add')
 {
	 	 $cargoType=new cargoTypeService();
 	 	 $cargoType->setname($_REQUEST['name']);
	 	 $cargoType->setisActive(isset($_REQUEST['isActive'])?1:0);
	 	 echo json_encode( $cargoType->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $cargoType=new cargoTypeService();
 	 	 $cargoType->setid($_REQUEST['id']);
	 	 $cargoType->setname($_REQUEST['name']);
	 	 $cargoType->setisActive(isset($_REQUEST['isActive'])?1:0);
	 	 echo json_encode( $cargoType->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $cargoType=new cargoTypeService();
 	 	 $cargoType->setid($_REQUEST['id']);
		echo json_encode( $cargoType->delete());
 }
} ?>