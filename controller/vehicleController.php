<?php

 ob_start();
// session_start();
require_once '../services/vehicleService.php';
require_once '../services/odometerService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$vehicle=new VehicleService();
		echo json_encode($vehicle->view());
	}
	else if($controller_templet->getAction()=="add")
	{
		$vehicle=new VehicleService();
		$vehicle->setregNo($_REQUEST['regNo']);
		$vehicle->setconfiguration($_REQUEST['configuration']);
		$vehicle->setaxel($_REQUEST['axel']);
		$vehicle->setmodel($_REQUEST['model']);
		$vehicle->setcountryMake($_REQUEST['countryMake']);
		$vehicle->setcardNumber($_REQUEST['cardNumber']);
		$vehicle->setvehicletypeId($_REQUEST['vehicleTypeId']);
		$vehicle->setcapacity($_REQUEST['capacty']);
		$id=$vehicle->save();
		
		$odometer=new OdometerService();
		$odometer->setvehicleId($id);
		$odometer->setreading(0);
		echo json_encode($odometer->firstSave());
		
	}else if($controller_templet->getAction()=="edit")
	{
		$vehicle=new VehicleService();
		$vehicle->setid($_REQUEST['id']);
		$vehicle->setregNo($_REQUEST['regNo']);
		$vehicle->setconfiguration($_REQUEST['configuration']);
		$vehicle->setaxel($_REQUEST['axel']);
		$vehicle->setmodel($_REQUEST['model']);
		$vehicle->setcountryMake($_REQUEST['countryMake']);
		$vehicle->setcardNumber($_REQUEST['cardNumber']);
		$vehicle->setvehicletypeId($_REQUEST['vehicleTypeId']);
		$vehicle->setcapacity($_REQUEST['capacty']);
		echo json_encode($vehicle->update());
		
	}else if($controller_templet->getAction()=="getDetail")
	{
		$vehicle=new VehicleService();
		$vehicle->setid($_REQUEST['id']);
		$sql="select * from tblvehicle where id='".$vehicle->getid()."'";
		foreach($vehicle->view_query($sql) as $row)
		{
			echo "<strong>".$row['regNo']."</strong>";
		}
	}else if($controller_templet->getAction()=="getVehicle")
	{
		$vehicle=new VehicleService();
		$vehicle->setid($_REQUEST['id']);
		$sql="select * from tblvehicle where id='".$vehicle->getid()."'";
		foreach($vehicle->view_query($sql) as $row)
		{
			echo $row['regNo'];
		}
	}else if($controller_templet->getAction()=="getDriver")
	{
		$vehicle=new VehicleService();
		$vehicle->setid($_REQUEST['id']);
		$sql="select d.* from tbldrivervehicle dv inner join tbldriver d on dv.driverId=d.id where dv.vehicleId='".$vehicle->getid()."' and dv.isActive='1' ";
		foreach($vehicle->view_query($sql) as $row)
		{
			echo $row['firstName']." ".$row["lastName"];
		}
	}else if($controller_templet->getAction()=="assignVehicle")
	{
		$vehicle=new VehicleService();
		echo json_encode($vehicle->saveDriver($_REQUEST['driverId3'],$_REQUEST['regNo3'],true));
	}
}
?>
