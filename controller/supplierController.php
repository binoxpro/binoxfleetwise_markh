<?php 
 require_once('../services/supplierService.php');
 require_once('../services/individualAccountService.php');
 require_once('../services/generalLedgerService.php');
 require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
     if($controller_templet->getAction()=='view'){
    	 	 $supplier=new supplierService();
            echo json_encode( $supplier->view()); 
     }else if($controller_templet->getAction()=='add')
     {
        //SUP-
            $categoryCode='SUP-';
            $individual=new individualAccountService();
            $individual->setId('Sup-'.$individual->Count());
            $individual->setAccountName($_REQUEST['supplierName']);
            $individual->setAccountCode(null);
            $individual->setIsActive(true);
            $individual->save();

    	 	 $supplier=new supplierService();
             $supplier->setsupplierCode($individual->getId());
     	 	 $supplier->setsupplierName($_REQUEST['supplierName']);
    	 	 $supplier->setaddress($_REQUEST['address']);
    	 	 $supplier->setpaymentTerm($_REQUEST['paymentTerm']);
    	 	 $supplier->setaccountNumber($_REQUEST['accountNumber']);
    	 	 echo json_encode( $supplier->save());
     }
     else if($controller_templet->getAction()=='edit')
     {
     	 	 $supplier=new supplierService();
     	 	 $supplier->setsupplierCode($_REQUEST['supplierCode']);
    	 	 $supplier->setsupplierName($_REQUEST['supplierName']);
    	 	 $supplier->setaddress($_REQUEST['address']);
    	 	 $supplier->setpaymentTerm($_REQUEST['paymentTerm']);
    	 	 $supplier->setaccountNumber($_REQUEST['accountNumber']);
    	 	 echo json_encode( $supplier->update()); 
     }
     else if($controller_templet->getAction()=='delete')
     {
     	 	 $supplier=new supplierService();
     	 	 $supplier->setsupplierCode($_REQUEST['supplierCode']);
            echo json_encode( $supplier->delete()); 
     } else if($controller_templet->getAction()=='viewComboBox')
     {
         $supplier=new supplierService();
         echo json_encode($supplier->viewComboBox());

     }else if($controller_templet->getAction()=='loadStatement')
     {
         //
         $generaledger=new generalLedgerService();
         $generaledger->setaccountCode('1801');
         if(isset($_REQUEST['supplierCode']))
         {
             $generaledger->setindividualNo($_REQUEST['supplierCode']);
             echo json_encode($generaledger->getStatement('2016-01-01', date('Y-m-d')));
         }
     }
} ?>