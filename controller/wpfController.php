<?php 
 require_once('../services/wpfService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $wpf=new wpfService();
 		 echo json_encode( $wpf->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $wpf=new wpfService();
         $wpf->setprocessId($_REQUEST['processId']);
 	 	 $wpf->setprocessDescription($_REQUEST['processDescription']);
	 	 $wpf->setonSuccess($_REQUEST['onSuccess']);
	 	 $wpf->setonFailure($_REQUEST['onFailure']);
	 	 $wpf->setsection($_REQUEST['section']);
        $wpf->setorderIndex($_REQUEST['orderIndex']);
	 	 echo json_encode( $wpf->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $wpf=new wpfService();
 	 	 $wpf->setprocessId($_REQUEST['processId']);
	 	 $wpf->setprocessDescription($_REQUEST['processDescription']);
	 	 $wpf->setonSuccess($_REQUEST['onSuccess']);
	 	 $wpf->setonFailure($_REQUEST['onFailure']);
	 	 $wpf->setsection($_REQUEST['section']);
         $wpf->setorderIndex($_REQUEST['orderIndex']);
	 	 echo json_encode( $wpf->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $wpf=new wpfService();
 	 	 $wpf->setprocessId($_REQUEST['processId']);
        echo json_encode( $wpf->delete());
}else if($controller_templet->getAction()=='viewCombo'){  
		 $wpf=new wpfService();
 		 echo json_encode( $wpf->viewConbox());
		}   
} ?>