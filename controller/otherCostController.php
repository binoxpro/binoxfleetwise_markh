<?php

 ob_start();
// session_start();
require_once '../services/tyreService.php';
require_once '../services/otherCostService.php';
require_once '../services/orderSevice.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$otherCost=new OtherCostService();
		echo json_encode($otherCost->view());
	}else if($controller_templet->getAction()=="add")
	{
		$otherCost=new OtherCostService();
		$otherCost->setcostName($_REQUEST['costName']);
		echo json_encode($otherCost->save());
	
	}else if($controller_templet->getAction()=="edit")
	{
		$otherCost=new OtherCostService();
		$otherCost->setid($_REQUEST['id']);
		$otherCost->setcostName($_REQUEST['costName']);
		echo json_encode($otherCost->update());
	}else if($controller_templet->getAction()=="runningCost")
	{ini_set('max_execution_time', 3000);
	if(isset($_REQUEST['vehicleId'])){
	   
       $tab='<ul class="nav nav-tabs">';
       $tabContent='<div class="tab-content">';
       
       
		$idv=$_REQUEST['vehicleId'];
		$order=new OrderService();
		$totalVal="";
		$totalVal2="";
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		echo "<table border='1' cellpadding='0' cellspacing='0' style='width:100%;' class='table table-striped table-hovered table-bordered'><thead><tr class='info'><td rowspan='2'>Vehicle</td><td rowspan='2'>Kilometer(Km)</td><td colspan='2'>Labour</td><td colspan='2'>Part</td><td colspan='4'>Fuel/Oil</td><td>Tyre</td><td>Others</td><td>Total</td></tr></thead><tbody>";
		echo"<tr><td></td><td></td><td>Cost(UGS)</td><td>Cost/Hour</td><td>Cost</td><td>Cost/Km</td><td>Litres</td><td>Cost(UGS)</td><td>Consumption/Km</td><td>Currency/Km</td><td>Cost/Km</td><td>Cost</td><td>UGS</td></tr>";
		for($i=1;$i<=intval(date('m'));$i++){
		
				$partsTotal=0;//we have it
				$kmCoveredTotal=0;//we have it
				$labourTotal=0;//we have it
				$labourHourTotal=0;// we have it
				$fuelTotal=0;// we have it
				$distanceTotal=0;
				$qtyTotal=0;
				$consumptionRatioTotal=0;
				$currencyRatioTotal=0;
				$generalTotal2=0;
				$otherMgtCostTotal=0;
				$tryeTotal=0;//missing cost	
                $partTotal=0;
                $partKmTotal=0;
			
		echo "<tr><td colspan='13'>".$month[$i-1]."</td></tr>";
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		
		$sql1="select * from tblvehicle where id='$idv'";
			foreach($order->view_query($sql1) as $row1)
			{
				$parts=0;//we have it
				$kmCovered=0;//we have it
				$labour=0;//we have it
				$labourHour=0;// we have it
				$fuel=0;// we have it
				$distance=0;
				$qty=0;
				$consumptionRatio=0;
				$currencyRatio=0;
				$generalTotal=0;
				$trye=0;//missing cost
				$otherMgtCost=0;
                $partKm=0.0;
				$vehicleId=$row1['id'];
				$regNo=$row1['regNo'];
				$sqlPart="select sum(pud.cost) totalCost from tblpartsused pud where pud.numberplate='$vehicleId' and pud.dateofrepair between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$parts=$rowPart['totalCost']==NULL?0:$rowPart['totalCost'];
				}
				
                
				$tyreService=new TyreService();
				$trye=$tyreService->getTyreCostInRanage($startDate,$endDate,$vehicleId);
			
				
				$sqlLabour="select sum(j.labourCost) totalLabourCost, sum((j.labourCost/j.labourHour)) totalLabourHours from tbljobcard j where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlLabour) as $rowLabour)
				{
					$labour=$rowLabour['totalLabourCost']==NULL?0:$rowLabour['totalLabourCost'];
					$labourHour=$rowLabour['totalLabourHours']==NULL?0:$rowLabour['totalLabourHours'];
					
				}
				
				$sqlFuel="select sum(Rate) rate,sum(qty) totalQty,SUM(qty*Rate) totalCost from tblfuel where vehicleId='$vehicleId' and CreationDate between '$startDate' and '$endDate'  ";
				$rate=0.0;
                foreach($order->view_query($sqlFuel) as $rowFuel)
				{
					//$distance=$rowFuel['totalDistance']==NULL?0:$rowFuel['totalDistance'];
					$qty=$rowFuel['totalQty']==NULL?0:$rowFuel['totalQty'];
					$rate=$rowFuel['rate']==NULL?0:$rowFuel['rate'];
					$fuel=$rowFuel['totalCost']==NULL?0:$rowFuel['totalCost'];
				}
				
				$sqlOtherCost="select sum(amount) totalCost from tblothercostmanagement where vehicleId='$vehicleId' and creationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlOtherCost) as $rowOtherCost)
				{
					
					$otherMgtCost=$rowOtherCost['totalCost']==NULL?0:$rowOtherCost['totalCost'];
				}
				
				
				$otherCost=new OtherCostService();
				$kmCovered=$otherCost->getDistanceMoved($vehicleId,$startDate,$endDate);
				$trye=($kmCovered*$trye);
                
                if($kmCovered>0&&$qty>0)
                {
                    $consumptionRatio=round($kmCovered/$qty,2);
                    $currencyRatio=round($fuel/$kmCovered,2);
                }else
                {
                    $consumptionRatio=0;
                    $currencyRatio=0;
                }
					
				$generalTotal=$labour+$parts+$fuel+$otherMgtCost+$trye;
                if($parts>0 && $kmCovered>0)
                {
                    $partKm=(intval($parts)/intval($kmCovered));
                }else
                {
                    $partKm=0.00;
                }
				//number_format(),2)
			echo"<tr><td>".$regNo."</td><td>".$kmCovered."</td><td>".$labour."</td><td>".number_format($labourHour,2)."</td><td>".number_format($parts,2)."</td><td>".number_format($partKm,2)."</td><td>".$qty."</td><td>".$fuel."</td><td>".$consumptionRatio."</td><td>".$currencyRatio."</td><td>".number_format(intval($trye),0)."</td><td>".$otherMgtCost."</td><td>".number_format(intval($generalTotal),0)."</td></tr>";
				
				$labourTotal=$labourTotal+$labour;
				$labourHourTotal=$labourHourTotal+$labourHour;
				$partsTotal=$partsTotal+$parts;
                $partKmTotal=$partKmTotal+$partKm;
				$qtyTotal=$qtyTotal+$qty;
				$fuelTotal=$fuelTotal+$fuel;
				$otherMgtCostTotal=$otherMgtCostTotal+$otherMgtCost;
				$consumptionRatioTotal=$consumptionRatioTotal+$consumptionRatio;
				$currencyRatioTotal=$currencyRatioTotal+$currencyRatio;
				$tryeTotal=$tryeTotal+$trye;
				$generalTotal2=$generalTotal2+$generalTotal;
			}
			echo"<tr><td colspan='2'>Total</td><td>".$labourTotal."</td><td>".number_format($labourHourTotal,2)."</td><td>".$partsTotal."</td><td>Null</td><td>".$qtyTotal."</td><td>".$fuelTotal."</td><td>".number_format($consumptionRatioTotal,2)."</td><td>".number_format($currencyRatioTotal,2)."</td><td>".number_format(intval($tryeTotal),0)."</td><td>".$otherMgtCostTotal."</td><td>".number_format(intval($generalTotal2),0)."</td></tr>";
			echo "<tr><td colspan='13'>&nbsp;</td></tr>";
		
		
		}
		echo "</tbody></table>";
	}
	else{
	   $tab='<ul class="nav nav-tabs">';
       $tabContent='<div class="tab-content">';
       //<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
//  <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
//  <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
//</ul>
//
//<div class="tab-content">
//  <div id="home" class="tab-pane fade in active">
//    <h3>HOME</h3>
//    <p>Some content.</p>
//  </div>
//  <div id="menu1" class="tab-pane fade">
//    <h3>Menu 1</h3>
//    <p>Some content in menu 1.</p>
//  </div>
//  <div id="menu2" class="tab-pane fade">
//    <h3>Menu 2</h3>
//    <p>Some content in menu 2.</p>
//  </div>
//</div>";
       
		$order=new OrderService();
		$totalVal="";
		$totalVal2="";
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		//for($i=1;$i<=3;$i++)
		for($i=1;$i<=intval(date('m'));$i++)
        {
             $active2=$i==1?"class='active'":'';
            $tab.='<li '.$active2.'><a data-toggle="tab" href="#'.$month[$i-1].'">'.$month[$i-1].'</a></li>';
            $active=$i==1?"in active":'';
           
            $tabContent.= "<div id='".$month[$i-1]."' class='tab-pane fade ".$active."'><table border='1' cellpadding='0' cellspacing='0' style='width:100%;' class='table table-striped table-hovered table-bordered'><thead><tr class='info'><td rowspan='2'>Vehicle</td><td rowspan='2'>Kilometer(Km)</td><td colspan='2'>Labour</td><td colspan='2'>Part</td><td colspan='4'>Fuel/Oil</td><td>Tyre</td><td>Others</td><td>Total</td></tr></thead><tbody>";
		      $tabContent.="<tr><td></td><td></td><td>Cost(UGS)</td><td>Cost/Hour</td><td>Cost</td><td>Cost/Km</td><td>Litres</td><td>Cost(UGS)</td><td>Consumption/Km</td><td>Currency/Km</td><td>Cost/Km</td><td>Cost</td><td>UGS</td></tr>";
		
				$partsTotal=0;//we have it
				$kmCoveredTotal=0;//we have it
				$labourTotal=0;//we have it
				$labourHourTotal=0;// we have it
				$fuelTotal=0;// we have it
				$distanceTotal=0;
				$qtyTotal=0;
				$consumptionRatioTotal=0;
				$currencyRatioTotal=0;
				$generalTotal2=0;
				$otherMgtCostTotal=0;
				$tryeTotal=0;//missing cost	
                $partTotal=0;
                $partKmTotal=0;
                
			
		$tabContent.= "<tr><td colspan='13'>".$month[$i-1]."</td></tr>";
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		
		$sql1="select * from tblvehicle";
			foreach($order->view_query($sql1) as $row1)
			{
				$parts=0;//we have it
				$kmCovered=0;//we have it
				$labour=0;//we have it
				$labourHour=0;// we have it
				$fuel=0;// we have it
				$distance=0;
				$qty=0;
				$consumptionRatio=0;
				$currencyRatio=0;
				$generalTotal=0;
				$trye=0;//missing cost
				$otherMgtCost=0;
                $partKm=0.0;
				$vehicleId=$row1['id'];
				$regNo=$row1['regNo'];
				$sqlPart="select sum(pud.cost) totalCost from tblpartsused pud where pud.numberplate='$vehicleId' and pud.dateofrepair between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$parts=$rowPart['totalCost']==NULL?0:$rowPart['totalCost'];
				}
				$sqlLabour="select sum(j.labourCost) totalLabourCost, sum((j.labourCost/j.labourHour)) totalLabourHours from tbljobcard j where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlLabour) as $rowLabour)
				{
					$labour=$rowLabour['totalLabourCost']==NULL?0:$rowLabour['totalLabourCost'];
					$labourHour=$rowLabour['totalLabourHours']==NULL?0:$rowLabour['totalLabourHours'];
					
				}
				$tyreService=new TyreService();
				$trye=$tyreService->getTyreCostInRanage($startDate,$endDate,$vehicleId);
				
				$sqlFuel="select sum(Rate) rate,sum(qty) totalQty,SUM(qty*Rate) totalCost from tblfuel where vehicleId='$vehicleId' and CreationDate between '$startDate' and '$endDate'  ";
				$rate=0.0;
                foreach($order->view_query($sqlFuel) as $rowFuel)
				{
					//$distance=$rowFuel['totalDistance']==NULL?0:$rowFuel['totalDistance'];
					$qty=$rowFuel['totalQty']==NULL?0:$rowFuel['totalQty'];
					$rate=$rowFuel['rate']==NULL?0:$rowFuel['rate'];
					$fuel=$rowFuel['totalCost']==NULL?0:$rowFuel['totalCost'];
				}
				
				$sqlOtherCost="select sum(amount) totalCost from tblothercostmanagement where vehicleId='$vehicleId' and creationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlOtherCost) as $rowOtherCost)
				{
					
					$otherMgtCost=$rowOtherCost['totalCost']==NULL?0:$rowOtherCost['totalCost'];
				}
				
				
				$otherCost=new OtherCostService();
				$kmCovered=$otherCost->getDistanceMoved($vehicleId,$startDate,$endDate);
				$trye=$trye*$kmCovered;
				$generalTotal=$labour+$parts+$fuel+$otherMgtCost+$trye;
                 if($kmCovered>0&&$qty>0)
                {
                    $consumptionRatio=round($kmCovered/$qty,2);
                    $currencyRatio=round($fuel/$kmCovered,2);
                }else
                {
                    $consumptionRatio=0;
                    $currencyRatio=0;
                }
                if($parts>0 && $kmCovered>0)
                {
                    $partKm=(intval($parts)/intval($kmCovered));
                }else
                {
                    $partKm=0.00;
                }
				//number_format(),2)
			$tabContent.="<tr><td>".$regNo."</td><td>".$kmCovered."</td><td>".$labour."</td><td>".number_format($labourHour,2)."</td><td>".number_format($parts,2)."</td><td>".number_format($partKm,2)."</td><td>".$qty."</td><td>".$fuel."</td><td>".$consumptionRatio."</td><td>".$currencyRatio."</td><td>".number_format(intval($trye),0)."</td><td>".$otherMgtCost."</td><td>".number_format(intval($generalTotal),0)."</td></tr>";
				
				
				$labourTotal=$labourTotal+$labour;
				$labourHourTotal=$labourHourTotal+$labourHour;
				$partsTotal=$partsTotal+$parts;
                $partKmTotal=$partKmTotal+$partKm;
				$qtyTotal=$qtyTotal+$qty;
				$fuelTotal=$fuelTotal+$fuel;
				$otherMgtCostTotal=$otherMgtCostTotal+$otherMgtCost;
				$consumptionRatioTotal=$consumptionRatioTotal+$consumptionRatio;
				$currencyRatioTotal=$currencyRatioTotal+$currencyRatio;
				$tryeTotal=$tryeTotal+$trye;
				$generalTotal2=$generalTotal2+$generalTotal;
			}
			$tabContent.="<tr><td colspan='2'>Total</td><td>".number_format($labourTotal,0)."</td><td>".number_format($labourHourTotal,2)."</td><td>".number_format($partsTotal,0)."</td><td>".number_format($partKmTotal,2)."</td><td>".number_format($qtyTotal,0)."</td><td>".$fuelTotal."</td><td>".number_format($consumptionRatioTotal,2)."</td><td>".number_format($currencyRatioTotal,2)."</td><td>".number_format(intval($tryeTotal),0)."</td><td>".$otherMgtCostTotal."</td><td>".number_format(intval($generalTotal2),0)."</td></tr>";
			$tabContent.= "<tr><td colspan='13'>&nbsp;</td></tr>";
		
		$tabContent.= "</tbody></table></div>";
		}
        $tab.="</ul>".$tabContent."";
		echo $tab;
		}
	}else if($controller_templet->getAction()=="runningCostExportExcel")
	{
	if(isset($_REQUEST['vehicleId'])){
		$data=array();
		$idv=$_REQUEST['vehicleId'];
		$order=new OrderService();
		$totalVal="";
		$totalVal2="";
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		//echo "<table border='1' cellpadding='0' cellspacing='0' style='width:100%;' class='table table-striped table-hovered table-bordered'><thead><tr class='info'><td rowspan='2'>Vehicle</td><td rowspan='2'>Kilometer(Km)</td><td colspan='2'>Labour</td><td colspan='2'>Part</td><td colspan='4'>Fuel/Oil</td><td>Tyre</td><td>Others</td><td>Total</td></tr></thead><tbody>";
		//echo"<tr><td></td><td></td><td>Cost(UGS)</td><td>Cost/Hour</td><td>Cost</td><td>Cost/Km</td><td>Litres</td><td>Cost(UGS)</td><td>Consumption/Km</td><td>Currency/Km</td><td>Cost/Km</td><td>Cost</td><td>UGS</td></tr>";
		for($i=1;$i<=intval(date('m'));$i++){
		
				$partsTotal=0;//we have it
				$kmCoveredTotal=0;//we have it
				$labourTotal=0;//we have it
				$labourHourTotal=0;// we have it
				$fuelTotal=0;// we have it
				$distanceTotal=0;
				$qtyTotal=0;
				$consumptionRatioTotal=0;
				$currencyRatioTotal=0;
				$generalTotal2=0;
				$otherMgtCostTotal=0;
				$tryeTotal=0;//missing cost	
			
		//echo "<tr><td colspan='13'>".$month[$i-1]."</td></tr>";
		$data2=array();
				$data2['Registration Number']=$month[$i-1];
				$data2['KM']="";
				$data2['LabourCost']="";
				$data2['LabourHours']="";
				$data2['PartsCost']="";
				$data2['PartsCostPerKm']="";
				$data2['Qty']="";
				$data2['Cost']="";
				$data2['consumptionRatio']="";
				$data2['currencyRatio']="";
				$data2['otherCost']="";
				$data2['total']="";
				array_push($data,$data2);
				
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		
		$sql1="select * from tblvehicle where id='$idv'";
			foreach($order->view_query($sql1) as $row1)
			{
				$parts=0;//we have it
				$kmCovered=0;//we have it
				$labour=0;//we have it
				$labourHour=0;// we have it
				$fuel=0;// we have it
				$distance=0;
				$qty=0;
				$consumptionRatio=0;
				$currencyRatio=0;
				$generalTotal=0;
				$trye=0;//missing cost
				$otherMgtCost=0;
				$vehicleId=$row1['id'];
				$regNo=$row1['regNo'];
				$sqlPart="select sum(pud.cost) totalCost from tblpartsused pud inner join tbljobcard j on pud.jobcardId=j.id where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$parts=$rowPart['totalCost']==NULL?0:$rowPart['totalCost'];
				}
				$sqlLabour="select sum(j.labourCost) totalLabourCost, sum((j.labourCost/j.labourHour)) totalLabourHours from tbljobcard j where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlLabour) as $rowLabour)
				{
					$labour=$rowLabour['totalLabourCost']==NULL?0:$rowLabour['totalLabourCost'];
					$labourHour=$rowLabour['totalLabourHours']==NULL?0:$rowLabour['totalLabourHours'];
					
				}
				
				$sqlFuel="select sum(distance) totalDistance, sum(qty) totalQty, sum((distance/qty)) consumptionRatio,sum(((distance/qty)*Rate)) currencyRatio,SUM(qty*Rate) totalCost from tblfuel where vehicleId='$vehicleId' and CreationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlFuel) as $rowFuel)
				{
					$distance=$rowFuel['totalDistance']==NULL?0:$rowFuel['totalDistance'];
					$qty=$rowFuel['totalQty']==NULL?0:$rowFuel['totalQty'];
					$consumptionRatio=$rowFuel['consumptionRatio']==NULL?0:$rowFuel['consumptionRatio'];
					$currencyRatio=$rowFuel['currencyRatio']==NULL?0:$rowFuel['currencyRatio'];
					$fuel=$rowFuel['totalCost']==NULL?0:$rowFuel['totalCost'];
				}
				
				$sqlOtherCost="select sum(amount) totalCost from tblothercostmanagement where vehicleId='$vehicleId' and creationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlOtherCost) as $rowOtherCost)
				{
					
					$otherMgtCost=$rowOtherCost['totalCost']==NULL?0:$rowOtherCost['totalCost'];
				}
				
				$generalTotal=$labour+$parts+$fuel+$otherMgtCost;
				$otherCost=new OtherCostService();
				$kmCovered=$otherCost->getDistanceMoved($vehicleId,$startDate,$endDate);
				//echo"<tr><td>".$regNo."</td><td>".$kmCovered."</td><td>".$labour."</td><td>".$labourHour."</td><td>".$parts."</td><td>Null</td><td>".$qty."</td><td>".$fuel."</td><td>".$consumptionRatio."</td><td>".$currencyRatio."</td><td>NULL</td><td>".$otherMgtCost."</td><td>".$generalTotal."</td></tr>";
				$data2=array();
				$data2['Registration Number']=$regNo;
				$data2['KM']=$kmCovered;
				$data2['LabourCost']=$labour;
				$data2['LabourHours']=$labourHour;
				$data2['PartsCost']=$parts;
				$data2['PartsCostPerKm']='NULL';
				$data2['Qty']=$qty;
				$data2['Cost']=$fuel;
				$data2['consumptionRatio']=$consumptionRatio;
				$data2['currencyRatio']=$currencyRatio;
				$data2['otherCost']=$otherMgtCost;
				$data2['total']=$generalTotal;
				array_push($data,$data2);
				
				
				$labourTotal=$labourTotal+$labour;
				$labourHourTotal=$labourHourTotal+$labourHour;
				$partsTotal=$partsTotal+$parts;
				$qtyTotal=$qtyTotal+$qty;
				$fuelTotal=$fuelTotal+$fuel;
				$otherMgtCostTotal=$otherMgtCostTotal+$otherMgtCost;
				$consumptionRatioTotal=$consumptionRatioTotal+$consumptionRatio;
				$currencyRatioTotal=$currencyRatioTotal+$currencyRatio;
				
				$generalTotal2=$generalTotal2+$generalTotal;
			}
			
			
			
			$data2=array();
				$data2['Registration Number']="Total";
				$data2['KM']=$kmCoveredTotal;
				$data2['LabourCost']=$labourTotal;
				$data2['LabourHours']=$labourHourTotal;
				$data2['PartsCost']=$partsTotal;
				$data2['PartsCostPerKm']='NULL';
				$data2['Qty']=$qtyTotal;
				$data2['Cost']=$fuelTotal;
				$data2['consumptionRatio']=$consumptionRatioTotal;
				$data2['currencyRatio']=$currencyRatioTotal;
				$data2['otherCost']=$otherMgtCostTotal;
				$data2['total']=$generalTotal2;
				array_push($data,$data2);

			//echo"<tr><td colspan='2'>Total</td><td>".$labourTotal."</td><td>".$labourHourTotal."</td><td>".$partsTotal."</td><td>Null</td><td>".$qtyTotal."</td><td>".$fuelTotal."</td><td>".$consumptionRatioTotal."</td><td>".$currencyRatioTotal."</td><td>NULL</td><td>".$otherMgtCostTotal."</td><td>".$generalTotal2."</td></tr>";
			//echo "<tr><td colspan='13'>&nbsp;</td></tr>";
		}
		//echo "</tbody></table>";
		echo json_encode($data);
		
	}
	else{
		$data=array();
		$order=new OrderService();
		$totalVal="";
		$totalVal2="";
		$month=array('JANUARY','FEB','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOCBER','NOV','DEC');
		//echo "<table border='1' cellpadding='0' cellspacing='0' style='width:100%;' class='table table-striped table-hovered table-bordered'><thead><tr class='info'><td rowspan='2'>Vehicle</td><td rowspan='2'>Kilometer(Km)</td><td colspan='2'>Labour</td><td colspan='2'>Part</td><td colspan='4'>Fuel/Oil</td><td>Tyre</td><td>Others</td><td>Total</td></tr></thead><tbody>";
		//echo"<tr><td></td><td></td><td>Cost(UGS)</td><td>Cost/Hour</td><td>Cost</td><td>Cost/Km</td><td>Litres</td><td>Cost(UGS)</td><td>Consumption/Km</td><td>Currency/Km</td><td>Cost/Km</td><td>Cost</td><td>UGS</td></tr>";
		for($i=1;$i<=intval(date('m'));$i++){
		
				$partsTotal=0;//we have it
				$kmCoveredTotal=0;//we have it
				$labourTotal=0;//we have it
				$labourHourTotal=0;// we have it
				$fuelTotal=0;// we have it
				$distanceTotal=0;
				$qtyTotal=0;
				$consumptionRatioTotal=0;
				$currencyRatioTotal=0;
				$generalTotal2=0;
				$otherMgtCostTotal=0;
				$tryeTotal=0;//missing cost	
			
		//echo "<tr><td colspan='13'>".$month[$i-1]."</td></tr>";
		$data2=array();
				$data2['Registration Number']=$month[$i-1];
				$data2['KM']="";
				$data2['LabourCost']="";
				$data2['LabourHours']="";
				$data2['PartsCost']="";
				$data2['PartsCostPerKm']="";
				$data2['Qty']="";
				$data2['Cost']="";
				$data2['consumptionRatio']="";
				$data2['currencyRatio']="";
				$data2['otherCost']="";
				$data2['total']="";
				array_push($data,$data2);
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		
		$sql1="select * from tblvehicle";
			foreach($order->view_query($sql1) as $row1)
			{
				$parts=0;//we have it
				$kmCovered=0;//we have it
				$labour=0;//we have it
				$labourHour=0;// we have it
				$fuel=0;// we have it
				$distance=0;
				$qty=0;
				$consumptionRatio=0;
				$currencyRatio=0;
				$generalTotal=0;
				$trye=0;//missing cost
				$otherMgtCost=0;
				$vehicleId=$row1['id'];
				$regNo=$row1['regNo'];
				$sqlPart="select sum(pud.cost) totalCost from tblpartsused pud inner join tbljobcard j on pud.jobcardId=j.id where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$parts=$rowPart['totalCost']==NULL?0:$rowPart['totalCost'];
				}
				$sqlLabour="select sum(j.labourCost) totalLabourCost, sum((j.labourCost/j.labourHour)) totalLabourHours from tbljobcard j where j.vehicleId='$vehicleId' and j.creationDate between '$startDate' and '$endDate'";
				foreach($order->view_query($sqlLabour) as $rowLabour)
				{
					$labour=$rowLabour['totalLabourCost']==NULL?0:$rowLabour['totalLabourCost'];
					$labourHour=$rowLabour['totalLabourHours']==NULL?0:$rowLabour['totalLabourHours'];
					
				}
				
				$sqlFuel="select sum(distance) totalDistance, sum(qty) totalQty, sum((distance/qty)) consumptionRatio,sum(((distance/qty)*Rate)) currencyRatio,SUM(qty*Rate) totalCost from tblfuel where vehicleId='$vehicleId' and CreationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlFuel) as $rowFuel)
				{
					$distance=$rowFuel['totalDistance']==NULL?0:$rowFuel['totalDistance'];
					$qty=$rowFuel['totalQty']==NULL?0:$rowFuel['totalQty'];
					$consumptionRatio=$rowFuel['consumptionRatio']==NULL?0:$rowFuel['consumptionRatio'];
					$currencyRatio=$rowFuel['currencyRatio']==NULL?0:$rowFuel['currencyRatio'];
					$fuel=$rowFuel['totalCost']==NULL?0:$rowFuel['totalCost'];
				}
				
				$sqlOtherCost="select sum(amount) totalCost from tblothercostmanagement where vehicleId='$vehicleId' and creationDate between '$startDate' and '$endDate'  ";
				foreach($order->view_query($sqlOtherCost) as $rowOtherCost)
				{
					
					$otherMgtCost=$rowOtherCost['totalCost']==NULL?0:$rowOtherCost['totalCost'];
				}
				
				$generalTotal=$labour+$parts+$fuel+$otherMgtCost;
				$otherCost=new OtherCostService();
				$kmCovered=$otherCost->getDistanceMoved($vehicleId,$startDate,$endDate);
				//echo"<tr><td>".$regNo."</td><td>".$kmCovered."</td><td>".$labour."</td><td>".$labourHour."</td><td>".$parts."</td><td>Null</td><td>".$qty."</td><td>".$fuel."</td><td>".$consumptionRatio."</td><td>".$currencyRatio."</td><td>NULL</td><td>".$otherMgtCost."</td><td>".$generalTotal."</td></tr>";
				
				$data2=array();
				$data2['Registration Number']=$regNo;
				$data2['KM']=$kmCovered;
				$data2['LabourCost']=$labour;
				$data2['LabourHours']=$labourHour;
				$data2['PartsCost']=$parts;
				$data2['PartsCostPerKm']='NULL';
				$data2['Qty']=$qty;
				$data2['Cost']=$fuel;
				$data2['consumptionRatio']=$consumptionRatio;
				$data2['currencyRatio']=$currencyRatio;
				$data2['otherCost']=$otherMgtCost;
				$data2['total']=$generalTotal;
				array_push($data,$data2);
				
				
				$labourTotal=$labourTotal+$labour;
				$labourHourTotal=$labourHourTotal+$labourHour;
				$partsTotal=$partsTotal+$parts;
				$qtyTotal=$qtyTotal+$qty;
				$fuelTotal=$fuelTotal+$fuel;
				$otherMgtCostTotal=$otherMgtCostTotal+$otherMgtCost;
				$consumptionRatioTotal=$consumptionRatioTotal+$consumptionRatio;
				$currencyRatioTotal=$currencyRatioTotal+$currencyRatio;
				
				$generalTotal2=$generalTotal2+$generalTotal;
			}
			//echo"<tr><td colspan='2'>Total</td><td>".$labourTotal."</td><td>".$labourHourTotal."</td><td>".$partsTotal."</td><td>Null</td><td>".$qtyTotal."</td><td>".$fuelTotal."</td><td>".$consumptionRatioTotal."</td><td>".$currencyRatioTotal."</td><td>NULL</td><td>".$otherMgtCostTotal."</td><td>".$generalTotal2."</td></tr>";
			//echo "<tr><td colspan='13'>&nbsp;</td></tr>";
			$data2=array();
				$data2['Registration Number']="Total";
				$data2['KM']=$kmCoveredTotal;
				$data2['LabourCost']=$labourTotal;
				$data2['LabourHours']=$labourHourTotal;
				$data2['PartsCost']=$partsTotal;
				$data2['PartsCostPerKm']='NULL';
				$data2['Qty']=$qtyTotal;
				$data2['Cost']=$fuelTotal;
				$data2['consumptionRatio']=$consumptionRatioTotal;
				$data2['currencyRatio']=$currencyRatioTotal;
				$data2['otherCost']=$otherMgtCostTotal;
				$data2['total']=$generalTotal2;
				array_push($data,$data2);

		
		
		}
		echo json_encode($data);
		}
	}
}
?>