<?php
ob_start();

require_once 'controller.php';

if(isset($_REQUEST['view'])){
    $controller_templet=new Controller($_REQUEST['view']);
    if($controller_templet->getAction()=="System_Administration_Vehicle_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/vehicleNew.js');
	}
	else if($controller_templet->getAction()=="System_Administration_Vehicle_Types")
	{
		echo"<script type='application/javascript' src='scripts/vehicleType.js'></script>";
	}else if($controller_templet->getAction()=="System_Administration_CheckList_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/checkListType.js');
	}
	else if($controller_templet->getAction()=="System_Administration_CheckListType_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/section.js');
	}
	else if($controller_templet->getAction()=="Create_JobCard")
	{
		$controller_templet->includeJavascriptFile('scripts/jobCard.js');
	}else if($controller_templet->getAction()=="Follow_Up_JobCard")
	{
		$controller_templet->includeJavascriptFile('scripts/editjobCard.js');
	}else if($controller_templet->getAction()=="Complete_JobCard")
	{
		$controller_templet->includeJavascriptFile('scripts/completeJobCard.js');
	}else if($controller_templet->getAction()=="Edit_JobCard")
	{
		$controller_templet->includeJavascriptFile('scripts/editjobCard.js');
	}
	else if($controller_templet->getAction()=="System_Administration_Trailer_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/trailer.js');
	}
	else if($controller_templet->getAction()=="vehicle_Load")
	{
		$controller_templet->includeJavascriptFile('scripts/capacity.js');
	}
	else if($controller_templet->getAction()=="Utilisation_Report")
	{
		$controller_templet->includeJavascriptFile('scripts/utilisation.js');
	}
	else if($controller_templet->getAction()=="Utilisation_Graphy")
	{
		$controller_templet->includeJavascriptFile('scripts/utilisationGraphy.js');
	}else if($controller_templet->getAction()=="Utilisation_vGraphy")
	{
		$controller_templet->includeJavascriptFile('scripts/vGraphy.js');
		//Utilisation_Graphy
	}
	else if($controller_templet->getAction()=="Manage_Order")
	{
		$controller_templet->includeJavascriptFile('scripts/order.js');
	}
	else if($controller_templet->getAction()=="Manage_Allocation")
	{
		$controller_templet->includeJavascriptFile('scripts/allocation.js');
	}
	else if($controller_templet->getAction()=="Manage_Dispatcher")
	{
		$controller_templet->includeJavascriptFile('scripts/dispatcher.js');
		
	}
	else if($controller_templet->getAction()=="Manage_Delivery")
	{
		$controller_templet->includeJavascriptFile('scripts/delivery.js');
	}else if($controller_templet->getAction()=="IOV_Inspection")
	{
		$controller_templet->includeJavascriptFile('scripts/inpsection.js');
	}else if($controller_templet->getAction()=="add_jobcard_items")
	{
		$controller_templet->includeJavascriptFile('scripts/jobcardItem.js');
	}
	else if($controller_templet->getAction()=="Manage_Access_Control")
	{
		$controller_templet->includeJavascriptFile('scripts/user.js');
	}
	else if($controller_templet->getAction()=="Manage_Fuel")
	{
		$controller_templet->includeJavascriptFile('scripts/fuel.js');
	}
	else if($controller_templet->getAction()=="Manage_Tyre_Type")
	{
		$controller_templet->includeJavascriptFile('scripts/tyreType.js');
	}
	else if($controller_templet->getAction()=="Manage_Odometer")
	{
		$controller_templet->includeJavascriptFile('scripts/odometer.js');
	}
	else if($controller_templet->getAction()=="Manage_Tyre")
	{
		$controller_templet->includeJavascriptFile('scripts/tyre.js');
	}
	else if($controller_templet->getAction()=="Manage_Other_Cost")
	{
		$controller_templet->includeJavascriptFile('scripts/otherCost.js');
	}
	else if($controller_templet->getAction()=="Manage_Other_Cost_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/othercostManagement.js');
	}
	else if($controller_templet->getAction()=="complete_jobcard_items")
	{
        $controller_templet->includeJavascriptFile('scripts/jobcardItem.js');
	}
	else if($controller_templet->getAction()=="Manage_Notification")
	{
		$controller_templet->includeJavascriptFile('scripts/notification.js');
	}else if($controller_templet->getAction()=="Vehicle_History_Report")
	{
		$controller_templet->includeJavascriptFile('scripts/history.js');
		
	}else if($controller_templet->getAction()=="Parts_used_Report")
	{
		$controller_templet->includeJavascriptFile('scripts/partsUsedReport.js');
	}
	else if($controller_templet->getAction()=="Vehicle_Kilometer_Graph")
	{
		$controller_templet->includeJavascriptFile('scripts/odometerGraph.js');
	}else if($controller_templet->getAction()=="Total_Order_Delivered")
	{
		$controller_templet->includeJavascriptFile('scripts/orderGraph.js');
	}else if($controller_templet->getAction()=="Products_Delivered")
	{
		$controller_templet->includeJavascriptFile('scripts/orderGraphQty.js');
		
	}else if($controller_templet->getAction()=="Running_Cost_Report")
	{
		$controller_templet->includeJavascriptFile('scripts/runningCost.js');
	}
	else if($controller_templet->getAction()=="Time_Utilisation_Report")
	{
		$controller_templet->includeJavascriptFile('scripts/utilisationTime.js');
	}
	else if($controller_templet->getAction()=="Service_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/service.js');
	}else if($controller_templet->getAction()=="licence_management")
	{
		$controller_templet->includeJavascriptFile('scripts/licence.js');
	}else if($controller_templet->getAction()=="Defence_Driving_Course")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="SOPAF")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Fitness_and_Medical_Examination")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Fire_Fighting_Training")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="First_Aid")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Journey_Management_Training")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Driver_Assement")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Road_Transport_Safety")
    {
        $controller_templet->includeJavascriptFile('scripts/wizardNext.js');
    }else if($controller_templet->getAction()=="Induction")
    {
        $controller_templet->includeJavascriptFile('scripts/wizardNext.js');
    }else if($controller_templet->getAction()=="Emergency_Response")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}else if($controller_templet->getAction()=="Dangerous_Goods_Handling")
	{
		$controller_templet->includeJavascriptFile('scripts/wizardNext.js');
	}
	else if($controller_templet->getAction()=="Vehicle_Standards_Management")
	{
		$controller_templet->includeJavascriptFile('scripts/standards.js');
	}
	else if($controller_templet->getAction()=="Driver_Records")
	{
		$controller_templet->includeJavascriptFile('scripts/driverRecord.js');
	}
	else if($controller_templet->getAction()=="checkList_download")
	{
		$controller_templet->includeJavascriptFile('scripts/downloadList.js');
	}
	else if($controller_templet->getAction()=="Inspection_List")
	{
		$controller_templet->includeJavascriptFile('scripts/listInsecption.js');
	}
	else if($controller_templet->getAction()=="Manage_of_License")
	{
		$controller_templet->includeJavascriptFile('scripts/licenceView.js');
	}
	else if($controller_templet->getAction()=="Manage_Driver_Training")
	{
		$controller_templet->includeJavascriptFile('scripts/trainingView.js');
	}
	else if($controller_templet->getAction()=="Manage_Model")
	{
		$controller_templet->includeJavascriptFile('scripts/model.js');
	}else if($controller_templet->getAction()=="Manage_Component")
	{
		$controller_templet->includeJavascriptFile('scripts/component.js');
	}else if($controller_templet->getAction()=="Manage_service_type")
	{
		$controller_templet->includeJavascriptFile('scripts/serviceType.js');
	}else if($controller_templet->getAction()=="Manage_Action_Notes")
	{
		$controller_templet->includeJavascriptFile('scripts/actionNote.js');
	}else if($controller_templet->getAction()=="Manage_Service_Interval")
	{
		$controller_templet->includeJavascriptFile('scripts/serviceInterval.js');
	}else if($controller_templet->getAction()=="Service_Track")
	{
		$controller_templet->includeJavascriptFile('');
	}else if($controller_templet->getAction()=="Service_Management_New")
	{
		$controller_templet->includeJavascriptFile('scripts/serviceHeader.js');
	}else if($controller_templet->getAction()=="Manage_Driver_Training_new")
	{
		$controller_templet->includeJavascriptFile('scripts/trainingTracker.js');
	}else if($controller_templet->getAction()=="Driver_Registration_New")
	{
		$controller_templet->includeJavascriptFile('scripts/driverNew.js');
	}else if($controller_templet->getAction()=="Driver_Registration")
	{
		$controller_templet->includeJavascriptFile('scripts/driverScreen.js');
	}else if($controller_templet->getAction()=="manage_stock")
	{
		$controller_templet->includeJavascriptFile('scripts/stock.js');
	}else if($controller_templet->getAction()=="scrapped_stock")
	{
		$controller_templet->includeJavascriptFile('scripts/scrapped.js');
	}else if($controller_templet->getAction()=="Manage_Tire_size")
	{
		$controller_templet->includeJavascriptFile('scripts/tireSize.js');
	}else if($controller_templet->getAction()=="Manage_Tire_Pattern")
	{
		$controller_templet->includeJavascriptFile('scripts/tirePattern.js');
	}else if($controller_templet->getAction()=="Purchase_Tire")
	{
	   $controller_templet->includeJavascriptFile('scripts/tire.js');
    }else if($controller_templet->getAction()=="Mount_Tire")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireHold.js');
    }else if($controller_templet->getAction()=="retread_return_list")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireRetread.js');
    }else if($controller_templet->getAction()=="tire_inspection")
	{
       $controller_templet->includeJavascriptFile('scripts/tireInspectionHeader.js'); 
    }else if($controller_templet->getAction()=="inspect_vehicle_tire")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireInspectionDetails.js'); 
    }else if($controller_templet->getAction()=="tire_inspection_history")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireInspectionHeader.js'); 
    }else if($controller_templet->getAction()=="tire_Rotate")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireRotation.js'); 
    }else if($controller_templet->getAction()=="tire_monitoring")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireHold.js'); 
    }else if($controller_templet->getAction()=="manage_customer_list")
	{
	   $controller_templet->includeJavascriptFile('scripts/customerNew.js'); 
    }
    else if($controller_templet->getAction()=="trip_Number_manger")
	{
	   $controller_templet->includeJavascriptFile('scripts/tripNumberManager.js'); 
    }else if($controller_templet->getAction()=="manage_trip_type")
	{
	   $controller_templet->includeJavascriptFile('scripts/tripType.js'); 
    }else if($controller_templet->getAction()=="Manage_Order_Dispatch")
	{
	   $controller_templet->includeJavascriptFile('scripts/newOrder.js'); 
    }else if($controller_templet->getAction()=="Manage_Delivery_New")
	{
	   $controller_templet->includeJavascriptFile('scripts/deliveryOrder.js'); 
    }else if($controller_templet->getAction()=="Manage_Order_New")
	{
	   $controller_templet->includeJavascriptFile('scripts/manageOrder.js'); 
    }
    else if($controller_templet->getAction()=="tire_summary")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireSummary.js'); 
    }else if($controller_templet->getAction()=="mounted_tire_report")
	{
	   $controller_templet->includeJavascriptFile('scripts/mountedTireReport.js'); 
	}else if($controller_templet->getAction()=="tire_brand_summary")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireBrandSummary.js'); 
	}else if($controller_templet->getAction()=="tire_history_by_Vehicle")
	{
	  $controller_templet->includeJavascriptFile('scripts/tireHistoryByTruck.js'); 
	}else if($controller_templet->getAction()=="tire_history_by_tire")
	{
		$controller_templet->includeJavascriptFile('scripts/tireHistoryByTire.js'); 
	}else if($controller_templet->getAction()=="tire_inventory_listing")
	{
	   $controller_templet->includeJavascriptFile('scripts/tireInventoryReport.js'); 
	   //tire_history_by_tire
	   
    }else if($controller_templet->getAction()=="manage_module")
	{
	    $controller_templet->includeJavascriptFile('scripts/activity.js'); 
    }else if($controller_templet->getAction()=="manage_feature")
	{
	   $controller_templet->includeJavascriptFile('scripts/subActivity.js');
    }else if($controller_templet->getAction()=="manage_access")
	{
	   $controller_templet->includeJavascriptFile('scripts/roleManagement.js');
    }else if($controller_templet->getAction()=="manage_account")
	{
		$controller_templet->includeJavascriptFile('scripts/systemUser.js');
	}else if($controller_templet->getAction()=="manage_cargo_type")
	{
		$controller_templet->includeJavascriptFile('scripts/cargoType.js');
	}else if($controller_templet->getAction()=="manage_department")
	{
		$controller_templet->includeJavascriptFile('scripts/department.js');
	}else if($controller_templet->getAction()=="manage_client")
	{
		$controller_templet->includeJavascriptFile('scripts/client.js');
	}else if($controller_templet->getAction()=="manage_workorder")
	{
		$controller_templet->includeJavascriptFile('scripts/workorder.js');
	}else if($controller_templet->getAction()=="manage_make")
	{
		$controller_templet->includeJavascriptFile('scripts/truckMake.js');
	}else if($controller_templet->getAction()=="manage_vehicle_model")
	{
		$controller_templet->includeJavascriptFile('scripts/truckModel.js');
	}else if($controller_templet->getAction()=="vehicle_configuration")
	{
		$controller_templet->includeJavascriptFile('scripts/vehicleConfiguration.js');
	}else if($controller_templet->getAction()=="setup_position")
	{
		$controller_templet->includeJavascriptFile('scripts/positionHR.js');
	}else if($controller_templet->getAction()=="setup_account_category")
	{
		$controller_templet->includeJavascriptFile('scripts/accountType.js');
	}else if($controller_templet->getAction()=="chart_of_accounts")
	{
		$controller_templet->includeJavascriptFile('scripts/chartOfaccount.js');
	}else if($controller_templet->getAction()=="individual_account")
	{
		$controller_templet->includeJavascriptFile('scripts/individualAccount.js');
	}else if($controller_templet->getAction()=="setup_allowance")
	{
		$controller_templet->includeJavascriptFile('scripts/allowance.js');
	}else if($controller_templet->getAction()=="manage_pay_rate")
	{

		$controller_templet->includeJavascriptFile('scripts/payRate.js');
	}else if($controller_templet->getAction()=="Register_Employee")
	{

		$controller_templet->includeJavascriptFile('scripts/employee.js');
	}else if($controller_templet->getAction()=="record_employee")
	{
		$controller_templet->includeJavascriptFile('scripts/employeeRecord.js');
	}else if($controller_templet->getAction()=="driver_Vehicle_assignment")
	{
		$controller_templet->includeJavascriptFile('scripts/driverVehicle.js');

	}else if($controller_templet->getAction()=="trailer_management")
	{
		$controller_templet->includeJavascriptFile('scripts/trailerAssigment.js');
	}else if($controller_templet->getAction()=="setup_fuel_and_lubes")
	{
		$controller_templet->includeJavascriptFile('scripts/fuelProduct.js');
	}else if($controller_templet->getAction()=="expense_Template")
	{
		$controller_templet->includeJavascriptFile('scripts/expenseTemplate.js');
	}else if($controller_templet->getAction()=="trip_generation")
	{
		$controller_templet->includeJavascriptFile('scripts/trip.js');
	}else if($controller_templet->getAction()=="manage_supplier")
	{
		$controller_templet->includeJavascriptFile('scripts/supplier.js');
	}else if($controller_templet->getAction()=="manage_fuel_and_lubes")
	{
		$controller_templet->includeJavascriptFile('scripts/fuelOrder.js');
	}else if($controller_templet->getAction()=="manage_financial_year")
	{
		$controller_templet->includeJavascriptFile('scripts/financialYear.js');
	}else if($controller_templet->getAction()=="manage_financial_month")
	{
		$controller_templet->includeJavascriptFile('scripts/financialMonth.js');
	}else if($controller_templet->getAction()=="manage_payment_voucher")
	{
		$controller_templet->includeJavascriptFile('scripts/paymentVoucher.js');
	}else if($controller_templet->getAction()=="payment_voucher_list")
	{
		$controller_templet->includeJavascriptFile('scripts/paymentVoucherList.js');
	}else if($controller_templet->getAction()=="transport_rate")
	{
		$controller_templet->includeJavascriptFile('scripts/transportRate.js');
	}else if($controller_templet->getAction()=="manage_debitor")
	{
		$controller_templet->includeJavascriptFile('scripts/debitor.js');
	}else if($controller_templet->getAction()=="create_invoice")
	{
		$controller_templet->includeJavascriptFile('scripts/invoice.js');
	}else if($controller_templet->getAction()=="setup_currency")
	{
		$controller_templet->includeJavascriptFile('scripts/currency.js');
	}else if($controller_templet->getAction()=="manage_invoice")
	{
		$controller_templet->includeJavascriptFile('scripts/manageInvoice.js');
	}else if($controller_templet->getAction()=="setup_vehicle_system")
	{
		$controller_templet->includeJavascriptFile('scripts/vehicleSystem.js');
	}else if($controller_templet->getAction()=="setup_spare_part")
	{
		$controller_templet->includeJavascriptFile('scripts/sparePart.js');

	}else if($controller_templet->getAction()=="manage_supplier_statement")
	{
		$controller_templet->includeJavascriptFile('scripts/supplierStatement.js');
	}else if($controller_templet->getAction()=="jobcard_report")
	{
		$controller_templet->includeJavascriptFile('scripts/jobcardReport.js');
	}else if($controller_templet->getAction()=="jobcard_summary")
	{
		$controller_templet->includeJavascriptFile('scripts/jobcardSummaryReport.js');
	}else if($controller_templet->getAction()=="spare_against_model")
	{
		$controller_templet->includeJavascriptFile('scripts/spareAgainstModelReport.js');
	}else if($controller_templet->getAction()=="pm_report")
	{
		$controller_templet->includeJavascriptFile('scripts/pmReport.js');
	}else if($controller_templet->getAction()=="service_occurance_report")
	{
		$controller_templet->includeJavascriptFile('scripts/serviceOccuranceReport.js');
	}else if($controller_templet->getAction()=="profitability_report")
	{
		$controller_templet->includeJavascriptFile('scripts/tripProfitabilityReport.js');
	}else if($controller_templet->getAction()=="running_cost_report")
	{
		$controller_templet->includeJavascriptFile('scripts/tripProfitabilitySummaryReport.js');
	}else if($controller_templet->getAction()=="licence_Manager")
	{
		$controller_templet->includeJavascriptFile('scripts/licenceManager.js');
	}else if($controller_templet->getAction()=="truck_running_cost_report")
	{
		$controller_templet->includeJavascriptFile('scripts/truckRunningCostReport.js');
	}else if($controller_templet->getAction()=="setup_start_balance")
	{
		$controller_templet->includeJavascriptFile('scripts/startBalance.js');
	}else if($controller_templet->getAction()=="distance")
	{
		$controller_templet->includeJavascriptFile('scripts/distance.js');
	}
    else if($controller_templet->getAction()=="import_fleet_data")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadFleet.js');
    }
    else if($controller_templet->getAction()=="import_fleet_data")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadFleet.js');
    }else if($controller_templet->getAction()=="import_odometer")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadOdometer.js');
    }else if($controller_templet->getAction()=="import_service")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadService.js');
    }else if($controller_templet->getAction()=="import_trailer_assignment")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadAssignment.js');
    }else if($controller_templet->getAction()=="import_workorder")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadWorkOrder.js');
    }else if($controller_templet->getAction()=="import_tire")
    {
        $controller_templet->includeJavascriptFile('scripts/uploadTire.js');
    }else if($controller_templet->getAction()=="store")
    {
        $controller_templet->includeJavascriptFile('scripts/store.js');
    }else if($controller_templet->getAction()=="item_category")
    {
        $controller_templet->includeJavascriptFile('scripts/itemcategory.js');
    }else if($controller_templet->getAction()=="uom")
    {
        $controller_templet->includeJavascriptFile('scripts/uom.js');
    }else if($controller_templet->getAction()=="items")
    {
        $controller_templet->includeJavascriptFile('scripts/item.js');
    }else if($controller_templet->getAction()=="wpf")
    {
        $controller_templet->includeJavascriptFile('scripts/wpf.js');
    }else if($controller_templet->getAction()=="store_process_status")
    {
        $controller_templet->includeJavascriptFile('scripts/storeproces.js');
    }else if($controller_templet->getAction()=="Receive_Part_Request_Form")
    {
        $controller_templet->includeJavascriptFile('scripts/partRequestFormPs.js');
    }else if($controller_templet->getAction()=="issue_parts")
    {
        $controller_templet->includeJavascriptFile('scripts/partRequestFormIssue.js');
    }else if($controller_templet->getAction()=="create_lop")
    {
        $controller_templet->includeJavascriptFile('scripts/partRequestFormlop.js');
    }
    else if($controller_templet->getAction()=="opening_stock")
    {
        $controller_templet->includeJavascriptFile('scripts/storestock.js');
    }








}else{
	$controller_templet=new Controller(NULL);
	//$controller_templet->includeJavascriptFile('scripts/dasbboard.js');
}

?>