<?php 
 require_once('../services/individualAccountService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $individualAccount=new individualAccountService();
 		 echo json_encode( $individualAccount->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $individualAccount=new individualAccountService();
		 if(isset($_REQUEST['Id']))
		 {
			 $individualAccount->setId($_REQUEST['Id']);
		 }else
		 {
			 $individualAccount->setId('BAI'.$individualAccount->count());
		 }
 	 	 $individualAccount->setAccountName($_REQUEST['AccountName']);
	 	 $individualAccount->setAccountCode($_REQUEST['AccountCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $individualAccount->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $individualAccount->setIsActive(false);
		 }

	 	 echo json_encode( $individualAccount->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $individualAccount=new individualAccountService();
 	 	 $individualAccount->setId($_REQUEST['Id']);
	 	 $individualAccount->setAccountName($_REQUEST['AccountName']);
	 	 $individualAccount->setAccountCode($_REQUEST['AccountCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $individualAccount->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $individualAccount->setIsActive(false);
		 }
	 	 echo json_encode( $individualAccount->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $individualAccount=new individualAccountService();
 	 	 $individualAccount->setId($_REQUEST['Id']);
		echo json_encode( $individualAccount->delete());
}else if($controller_templet->getAction()=='viewCombo'){
	 $individualAccount=new individualAccountService();
 		 echo json_encode( $individualAccount->viewConbox());
}   
} ?>