<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/partsUsedService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
		$spareService=new PartsUsedService();
		$spareService->setnumberPlate($_REQUEST['regNo']);
		$spareService->setDateOfRepair($_REQUEST['dateofrepair']);
		$spareService->setTypeOfRepair($_REQUEST['typeofrepair']);
		$spareService->setpartused($_REQUEST['partUsed']);
		$spareService->setcost($_REQUEST['cost']);
		//$spareService->setcomment($_REQUEST['comment']);
		//$spareService->set($_REQUEST['Comment']);
		echo json_encode($spareService->save());
		
	}else  if($controller_templet->getAction()=="view"){
		
		$spareService=new PartsUsedService();
		if(isset($_REQUEST['vehicleId'])){
			$spareService->setnumberPlate($_REQUEST['vehicleId']);
			echo json_encode($spareService->viewVehicle());
			
		}else{
		echo json_encode($spareService->view());
		}
		
		
	}else if($controller_templet->getAction()=="update"){
		$spareService=new PartsUsedService();
		$spareService->setnumberPlate($_REQUEST['regNo']);
		$spareService->setDateOfRepair($_REQUEST['dateofrepair']);
		$spareService->setTypeOfRepair($_REQUEST['typeofrepair']);
		$spareService->setpartused($_REQUEST['partUsed']);
		$spareService->setcost($_REQUEST['cost']);
		//$spareService->setcomment($_REQUEST['comment']);
		$spareService->setid($_REQUEST['id']);
		echo json_encode($spareService->update());
	}else if ($controller_templet->getAction()=="addServiceMaintainance")
	{
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['numberPlate']);
		$serviceTrack->setPreviousDate($_REQUEST['previousDate']);
		$serviceTrack->setNextDate($_REQUEST['nextDate']);
		$serviceTrack->setInterval($_REQUEST['interval']);
		$serviceTrack->setReminder($_REQUEST['reminder']);
		$serviceTrack->setService($_REQUEST['service']);
		$serviceTrack->setTypeOfservice($_REQUEST['typeofservice']);
		echo json_encode($serviceTrack->saveMaintanance());
		
	}else if($controller_templet->getAction()=="manage")
	{
		$serviceTrack=new ServiceTrackService();
		# the filter current does not return data 
		/*$serviceTrack->setServiceID(NULL);
		$serviceTrack->setNumberPlate(NULL);
		$serviceTrack->setPreviousDate(NULL);
		$serviceTrack->setNextDate(NULL);
		$serviceTrack->setInterval(NULL);
		$serviceTrack->setReminder(NULL);
		$serviceTrack->setTypeOfservice(NULL);*/
		$sql="select * from tbl_service";
		echo json_encode($serviceTrack->view_query($sql));
	}else if ($controller_templet->getAction()=="getPreviousReading")
	{
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['id']);
		$sql="select nextDate from tbl_service_status where numberPlate='".$serviceTrack->getNumberPlate()."'";
		foreach($serviceTrack->view_query($sql) as $row){
			$serviceTrack->setPreviousDate($row['nextDate']);
		}
		echo $serviceTrack->getPreviousDate();
		
	}
	else if ($controller_templet->getAction()=="getNextReading")
	{
		$nextReading=$_REQUEST['nextReading'];
		$newTime=strtotime($nextReading);
		
	}else if($controller_templet->getAction()=="delete")
	{
		$partsUsedService=new PartsUsedService();
		$partsUsedService->setid($_REQUEST['id']);
		echo json_encode($partsUsedService->delete());
		
	}
    
}
?>