<?php 
 	require_once('../services/financialYearService.php');
	require_once('../services/financialMonthService.php');
	require_once('controller.php');
	require_once('utility.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $financialYear=new financialYearService();

 		 echo json_encode( $financialYear->view());
	}else if($controller_templet->getAction()=='add'){

		 $financialYear2=new financialYearService();
		 $financialYear2->setisActive(false);
		 $financialYear2->updateAllFinancialYear();


	 	 $financialYear=new financialYearService();
		 $financialYear->setid($_REQUEST['id']);
 	 	 $financialYear->setstartDate($_REQUEST['startDate']);
	 	 $financialYear->setendDate($_REQUEST['endDate']);
		 if(isset($_REQUEST['isActive']))
		 {
			 $financialYear->setisActive(true);
		 }else
		 {
			 $financialYear->setisActive(false);
		 }

	 	 $financialYear->save();
		 //Generate finanical Months;
		 $startDate=$_REQUEST['startDate'];
		 $endDate=$_REQUEST['endDate'];
		 //set up the range of months to roll
		 $monthLimit=utility::getMonthsBetweenDates($startDate,$endDate);
	     $dateArray=utility::explodeDate($startDate);
		 $startMonth=$dateArray[1];
		 $month=intval($startMonth);
		 $monthString=null;
		 $startYear=intval($dateArray[0]);
		 //roll throught the month of the finanical month
		 for($i=0;$i<=$monthLimit;$i++)
		 {

			if($month>12)
			{
				$month=$month-12;
				$startYear=$startYear+1;
				$monthString=utility::getMonthString($month);
				//set the start and End

			}else
			{
				$monthString=utility::getMonthString($month);
			}
			 $start_date=$startYear."-".$monthString."-01";
			 $end_date=$startYear."-".$monthString."-".cal_days_in_month(CAL_GREGORIAN,$month,$startYear);
			 $month_code=$monthString."/".$startYear;

			 $finanicalMonth=new financialMonthService();
			 $finanicalMonth->setid($month_code);
			 $finanicalMonth->setyearId($financialYear->getid());
			 $finanicalMonth->setstartDate($start_date);
			 $finanicalMonth->setendDate($end_date);
			 $finanicalMonth->setisActive(false);
			 $finanicalMonth->save();
			 $month=$month+1;

		 }
		 echo json_encode(array('success'=>true));


 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $financialYear=new financialYearService();
 	 	 $financialYear->setid($_REQUEST['id']);
	 	 $financialYear->setstartDate($_REQUEST['startDate']);
	 	 $financialYear->setendDate($_REQUEST['endDate']);
	 if(isset($_REQUEST['isActive']))
	 {
		 $financialYear->setisActive(true);
	 }else
	 {
		 $financialYear->setisActive(false);
	 }
	 echo json_encode( $financialYear->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
	 	$finanicalMonth=new financialMonthService();
	 	$finanicalMonth->setyearId($_REQUEST['id']);
	 	$finanicalMonth->deleteForYear();

	 	$financialYear=new financialYearService();
	 	$financialYear->setid($_REQUEST['id']);
		echo json_encode( $financialYear->delete());
}else if($controller_templet->getAction()=='viewCombo')
{
		 $financialYear=new financialYearService();
 		 echo json_encode( $financialYear->viewConbox());
}else if($controller_templet->getAction()=='getActiveYear')
 {
	 $finanicalMonth=new financialMonthService();
	 //$finanicalMonth->findOpenMonth();
	 echo json_encode($finanicalMonth->findOpenMonthArray());

 }
}
?>