<?php 
 require_once('../services/tripService.php');
	require_once('../services/tripTypeService.php');
require_once('../services/employeeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $Trip=new TripService();
 		 echo json_encode( $Trip->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $Trip=new TripService();
		 $Trip->setTripNumber($_REQUEST['TripNumber']);
 	 	 $Trip->setTripTypeId($_REQUEST['TripTypeId']);
	 	 $Trip->setVehicleId($_REQUEST['VehicleId']);

		 $employee=new employeeService();
		 $arrayName=explode(' ',$_REQUEST['DriverId']);
		 $employee->setfirstName($arrayName[0]);
		 $employee->findWithName();
	 	 $Trip->setDriverId($employee->getempNo());
		 if(isset($_REQUEST['Status']))
		 {
			 $Trip->setStatus($_REQUEST['Status']);

		 }else
		 {
			 $Trip->setStatus(true);
		 }

	 	 $Trip->setTripStartDate($_REQUEST['TripStartDate']);
	 	 //$trip->setTripEndDate(null);
		 if(isset($_REQUEST['WorkOrderAttached']))
		 {
			 $Trip->setWorkOrderAttached($_REQUEST['WorkOrderAttached']);
		 }else
		 {
			 $Trip->setWorkOrderAttached(false);
		 }
	 	 if(isset($_REQUEST['Approved']))
		 {
			 $Trip->setApproved($_REQUEST['Approved']);
		 }else{
			 $Trip->setApproved(false);
		 }
	 	 $Trip->setApproved(false);
	 	 echo json_encode( $Trip->save());
 }
 else if($controller_templet->getAction()=='edit'){


	 $Trip=new TripService();
	 $Trip->setTripNumber($_REQUEST['TripNumber']);
	 $Trip->setTripTypeId($_REQUEST['TripTypeId']);
	 $Trip->setVehicleId($_REQUEST['VehicleId']);

	 $employee=new employeeService();
	 $arrayName=explode(' ',$_REQUEST['DriverId']);
	 $employee->setfirstName($arrayName[0]);
	 $employee->findWithName();
	 $Trip->setDriverId($employee->getempNo());
	 if(isset($_REQUEST['Status']))
	 {
		 $Trip->setStatus($_REQUEST['Status']);

	 }else
	 {
		 $Trip->setStatus(true);
	 }

	 $Trip->setTripStartDate($_REQUEST['TripStartDate']);
	 //$trip->setTripEndDate(null);
	 if(isset($_REQUEST['WorkOrderAttached']))
	 {
		 $Trip->setWorkOrderAttached($_REQUEST['WorkOrderAttached']);
	 }else
	 {
		 $Trip->setWorkOrderAttached(false);
	 }
	 if(isset($_REQUEST['Approved']))
	 {
		 $Trip->setApproved($_REQUEST['Approved']);
	 }else{
		 $Trip->setApproved(false);
	 }
	 if(isset($_REQUEST['TripEndDate']))
	 {
		 $Trip->setTripEndDate($_REQUEST['TripEndDate']);
	 }
	 $Trip->setApproved(false);
	 echo json_encode( $Trip->update());


 	 	/* $trip=new TripService();
 	 	 $trip->setTripNumber($_REQUEST['TripNumber']);
	 	 $trip->setTripTypeId($_REQUEST['TripTypeId']);
	 	 $trip->setVehicleId($_REQUEST['VehicleId']);
	 	 $trip->setDriverId($_REQUEST['DriverId']);
	 	 $trip->setStatus($_REQUEST['Status']);
	 	 $trip->setTripStartDate($_REQUEST['TripStartDate']);
	 	 $trip->setTripEndDate($_REQUEST['TripEndDate']);
	 	 $trip->setWorkOrderAttached($_REQUEST['WorkOrderAttached']);
	 	 $trip->setApproved($_REQUEST['Approved']);
	 	 echo json_encode( $trip->update());*/
 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $Trip=new TripService();
 	 	 $Trip->setTripNumber($_REQUEST['TripNumber']);
		echo json_encode( $Trip->delete());
}else if($controller_templet->getAction()=='viewCombo')
 {
		 $Trip=new TripService();
 		 echo json_encode( $Trip->viewConbox());
 }  else if($controller_templet->getAction()=='getTripNumber')
 {
	 	$trip=new TripService();
	 	$trip->setTripTypeId($_REQUEST['tripTypeId']);
	 	$tripType=new tripTypeService();
	 	$tripType->setid($_REQUEST['tripTypeId']);
	 	$tripType->getObject();
	 	echo $trip->tripNumberGenerator($tripType->getPrefix());

 }else if($controller_templet->getAction()=='close') {

	 $Trip = new TripService();
	 $Trip->setTripNumber($_REQUEST['TripNumber']);
	 $Trip->find();
	 $Trip->setTripEndDate($_REQUEST['closeDate']);
	 $Trip->setStatus(false);
	 echo json_encode($Trip->update());
 }
 } ?>