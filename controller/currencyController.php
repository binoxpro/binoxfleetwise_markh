<?php 
 require_once('../services/currencyService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $currency=new currencyService();
 echo json_encode( $currency->view()); 
 }else if($controller_templet->getAction()=='add'){
	 	 $currency=new currencyService();
 	 	 $currency->setsymbol($_REQUEST['symbol']);
	 	 $currency->setdefault($_REQUEST['default1']);
	 	 $currency->setrate($_REQUEST['rate']);
	 	 echo json_encode( $currency->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $currency=new currencyService();
 	 	 $currency->setid($_REQUEST['id']);
	 	 $currency->setsymbol($_REQUEST['symbol']);
	 	 $currency->setdefault($_REQUEST['default1']);
	 	 $currency->setrate($_REQUEST['rate']);
	 	 echo json_encode( $currency->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $currency=new currencyService();
 	 	 $currency->setid($_REQUEST['id']);
        echo json_encode( $currency->delete()); } 
} ?>