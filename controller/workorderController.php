<?php 
 require_once('../services/workorderService.php');
require_once('../services/tripService.php');
require_once('../services/distanceService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
		 $workOrder=new workOrderService();
		 if(isset($_REQUEST['tripNo']))
		 {
			 $workOrder->settripNo($_REQUEST['tripNo']);
			 echo json_encode($workOrder->view());
		 }
	}else if($controller_templet->getAction()=='unClaimed')
	 {
		 $workOrder=new workOrderService();
		 if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
		 {
			 echo json_encode($workOrder->viewUnClaimedDate($_REQUEST['startDate'],$_REQUEST['endDate']));
		 }else
		 {
			 echo json_encode($workOrder->viewUnClaimed());
		 }


	 }else if($controller_templet->getAction()=='add')
	 {
		 $distance=new distanceService();
		 $distance->setid($_REQUEST['distanceId']);
		 $distance->find();


	 	 $workOrder=new workOrderService();
 	 	 $workOrder->settripNo($_REQUEST['tripNo4']);
		 $workOrder->settownFrom($distance->getdistrictFrom());
		 $workOrder->settownTo($distance->getdistrictTo());
	 	 $workOrder->setDeliveryNo($_REQUEST['DeliveryNo']);
	 	 $workOrder->setPONo($_REQUEST['PONo']);
		 $workOrder->setProduct($_REQUEST['product']);
	 	 $workOrder->setQuantity($_REQUEST['Quantity']);
	 	 $workOrder->setBags($_REQUEST['Bags']);
	 	 $workOrder->setRate($_REQUEST['Rate']);
		 $workOrder->setDistance($_REQUEST['distance']);
	 	 $workOrder->setAmount($_REQUEST['Amount']);
	 	 $workOrder->setCustomerId($_REQUEST['CustomerId']);
	 	 $workOrder->setClaimed(false);
		 if(isset($_REQUEST['WhtApplied']))
		 {
			 $workOrder->setWhtApplied(true);
			 $workOrder->setWhtValue(0.06*$_REQUEST['Amount']);
		 }else
         {
			 $workOrder->setWhtApplied(false);
			 $workOrder->setWhtValue(0);
		 }
		 $workOrder->setDistance($_REQUEST['distance']);
	 	 //$workOrder->setWhtValue($_REQUEST['WhtValue']);
	 	 $workOrder->setCreationDate($_REQUEST['CreationDate']);
	 	 $workOrder->setIsActive(true);
	 	 echo json_encode($workOrder->save());

		 $tripService=new TripService();
		 $tripService->setTripNumber($_REQUEST['tripNo4']);
		 $tripService->find();
		 //$tripService->setWorkOrderAttached(true);
		 if($tripService->getWorkOrderAttached()==true)
		 {
			 echo json_encode(array('success'=>true));

		 }else
		 {
			 $tripService->setWorkOrderAttached(true);
			 echo json_encode($tripService->update());
		 }
 }
 else if($controller_templet->getAction()=='edit'){
		 $distance=new distanceService();
		 $distance->setid($_REQUEST['distanceId']);
		 $distance->find();

 	 	 $workOrder=new workOrderService();
 	 	 $workOrder->setid($_REQUEST['id']);
	 	 $workOrder->settripNo($_REQUEST['tripNo4']);
		 $workOrder->settownFrom($distance->getdistrictFrom());
		 $workOrder->settownTo($distance->getdistrictTo());
	 	 $workOrder->setDeliveryNo($_REQUEST['DeliveryNo']);
	 	 $workOrder->setPONo($_REQUEST['PONo']);
	 	 $workOrder->setProduct($_REQUEST['product']);
	 	 $workOrder->setQuantity($_REQUEST['Quantity']);
	 	 $workOrder->setBags($_REQUEST['Bags']);
	 	 $workOrder->setRate($_REQUEST['Rate']);
	 	 $workOrder->setAmount($_REQUEST['Amount']);
	 	 $workOrder->setCustomerId($_REQUEST['CustomerId']);



	 $workOrder->setClaimed(false);
	 if(isset($_REQUEST['WhtApplied']))
	 {
		 $workOrder->setWhtApplied(true);
		 $workOrder->setWhtValue(0.06*$_REQUEST['Amount']);
	 }else{
		 $workOrder->setWhtApplied(false);
		 $workOrder->setWhtValue(0);
	 }

	 //$workOrder->setWhtValue($_REQUEST['WhtValue']);
	 $workOrder->setCreationDate($_REQUEST['CreationDate']);
	 $workOrder->setIsActive(true);
	 //$workOrder->save();
	 $workOrder->update();
	 $tripService=new TripService();
	 $tripService->setTripNumber($_REQUEST['tripNo4']);
	 $tripService->find();
	 if($tripService->getWorkOrderAttached()==true)
	 {
		 echo json_encode(array('success'=>true));

	 }else
	 {
		 $tripService->setWorkOrderAttached(true);
		 echo json_encode($tripService->update());
	 }


	 	 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $workOrder=new workOrderService();
 	 	 $workOrder->setid($_REQUEST['id']);
echo json_encode( $workOrder->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $workOrder=new workOrderService();
 		 echo json_encode( $workOrder->viewConbox());
		}   
} ?>