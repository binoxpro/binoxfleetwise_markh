<?php 
 require_once('../services/accountTypeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $accountType=new accountTypeService();
 		 echo json_encode( $accountType->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $accountType=new accountTypeService();
		 $accountType->setPrefixCode($_REQUEST['PrefixCode']);
 	 	 $accountType->setAccountTypeName($_REQUEST['AccountTypeName']);
	 	 $accountType->setAccountBalanceType($_REQUEST['AccountBalanceType']);
	 	 $accountType->setParentPrefixCode($_REQUEST['ParentPrefixCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $accountType->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $accountType->setIsActive(false);
		 }
	 	 echo json_encode($accountType->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $accountType=new accountTypeService();
 	 	 $accountType->setPrefixCode($_REQUEST['PrefixCode']);
	 	 $accountType->setAccountTypeName($_REQUEST['AccountTypeName']);
	 	 $accountType->setAccountBalanceType($_REQUEST['AccountBalanceType']);

	 	 $accountType->setParentPrefixCode($_REQUEST['ParentPrefixCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $accountType->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $accountType->setIsActive(false);
		 }
	 	 echo json_encode( $accountType->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $accountType=new accountTypeService();
 	 	 $accountType->setPrefixCode($_REQUEST['PrefixCode']);
echo json_encode( $accountType->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){
	 $accountType=new accountTypeService();
 		 echo json_encode( $accountType->viewConbox());
}   
} ?>