<?php

 ob_start();
// session_start();
require_once '../services/orderSevice.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    	if($controller_templet->getAction()=="view")
	{
		$order=new OrderService();
		echo json_encode($order->view());
	}else if($controller_templet->getAction()=="viewCustomer2")
	{
		$orderv=new OrderService();
		echo json_encode($orderv->customerView());
	}
	else if($controller_templet->getAction()=="addCustomer")
	{
		$order=new OrderService();
		echo json_encode($order->saveCustomer($_REQUEST['customerName']));
	}
	else if($controller_templet->getAction()=="add")
	{
		$order=new OrderService();
		$order->setmailingName($_REQUEST['mailingName']);
		$order->setorderDate($_REQUEST['orderDate']);
		$order->setpromiseDate($_REQUEST['promiseDate']);
		$order->setorderNumber($_REQUEST['orderNumber']);
		$order->setqtyOrder($_REQUEST['qtyOrder']);
		$order->setstatusOrder($_REQUEST['statusorderId']);
		$order->setorderStatus($_REQUEST['orderstatusId']);
		$order->setitemCode($_REQUEST['itemcodeId']);
		$order->setorderType($_REQUEST['ordertypeId']);
		echo json_encode($order->save());
		
		
	}else if($controller_templet->getAction()=="update")
	{
		$order=new OrderService();
		$order->setmailingName($_REQUEST['mailingName']);
		$order->setorderDate($_REQUEST['orderDate']);
		$order->setpromiseDate($_REQUEST['promiseDate']);
		$order->setorderNumber($_REQUEST['orderNumber']);
		$order->setqtyOrder($_REQUEST['qtyOrder']);
		$order->setstatusOrder($_REQUEST['statusorderId']);
		$order->setorderStatus($_REQUEST['orderstatusId']);
		$order->setitemCode($_REQUEST['itemcodeId']);
		$order->setorderType($_REQUEST['ordertypeId']);
		$order->setid($_REQUEST['id']);
		echo json_encode($order->update());	
	}else if($controller_templet->getAction()=="viewUtilisation")
	{
		$capacityService=new capacityService();
		if(isset($_REQUEST['subAction'])){
			$subAccount=$_REQUEST['subAction'];
			if($subAccount=="rangeDate"){
			echo json_encode($capacityService->viewUtilisation2($_REQUEST['startDate'],$_REQUEST['endDate']));
			}else if($subAccount=="vehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation3(intval($_REQUEST['vehicleId'])));
			}else if($subAccount=="rangeVehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation4($_REQUEST['vehicleId'],$_REQUEST['startDate'],$_REQUEST['endDate']));
			}
		}else{
			echo json_encode($capacityService->viewUtilisation());
		}
	}else if($controller_templet->getAction()=="graphData")
	{
		$capacity=new capacityService();
		echo $capacity->viewUtilisation5($_REQUEST['vehicleId']);
		
	}
	else if($controller_templet->getAction()=="view_ordertypeid")
	{
		$order =new OrderService();
		$sql="select * from tblordertype where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_orderStatus")
	{
		$order =new OrderService();
		$sql="select * from tblorderstatus where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_itemCome")
	{
		$order =new OrderService();
		$sql="select * from tblgood where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_statusOrder")
	{
		$order =new OrderService();
		$sql="select * from tblstatusorder where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="delete")
	{
		$order=new OrderService();
		$order->setid($_REQUEST['id']);
		echo json_encode($order->delete());
	}
	else if($controller_templet->getAction()=="excelExport")
	{
		$data=array();
		$order=new OrderService();
		$order->setorderDate($_REQUEST['date']);
		$sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where o.orderDate='".$order->getorderDate()."'";
		foreach($order->view_query($sql) as $row)
		{
			$data2=array();
		   	$data2["#"]=$row["id"];
		   	$data2["MailingName"]=$row["mailingName"];
		   	$data2["OrderDate"]=$row["orderDate"];
		   	$data2["PromiseDate"]=$row["promiseDate"];
		   	$data2["OrderNumber"]=$row["orderNumber"];
		   	$data2["Ordertype"]=$row["ordertypeId"];
		   	$data2["QtyOrder"]=$row["qtyOrder"];
		   	$data2["Statusorder"]=$row["statusorderId"];
		   	$data2["Qrderstatus"]=$row["orderstatusId"];
		   	$data2["Itemcode"]=$row["itemcodeId"];
		   array_push($data,$data2);	
		}
		echo json_encode($data);
	}else if($controller_templet->getAction()=="graphVal")
	{
		$order=new OrderService();
		$totalVal="";
		for($i=1;$i<13;$i++){
		
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		$sqlPart="SELECT count(o.id) noorder  FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where deli.Deliverydate between '$startDate' and '$endDate' ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['noorder']);
				}
				if($i==1){
					$totalVal=$total;
				}else{
				 $totalVal.="/".$total;
				}
		}
				//echo $totalVal;
				
		//$order=new OrderService();
		$totalVal2="";
		for($i=1;$i<13;$i++){
		
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		$sqlPart="SELECT sum(o.qtyOrder) qtyCarried  FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where deli.Deliverydate between '$startDate' and '$endDate' ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['qtyCarried']);
				}
				if($i==1){
					$totalVal2=$total;
				}else{
				 $totalVal2.="/".$total;
				}
		}
				echo $totalVal."-".$totalVal2;
				//$totalPart=$totalPart+$total;
				
	}
	else if($controller_templet->getAction()=="qtyVal")
	{
		$order=new OrderService();
		$totalVal="";
		$totalVal2="";
		$productArray=array("AGO","ULG","BIK","VPOWER");
		for($c=0;$c<sizeof($productArray);$c++){
			$product=$productArray[$c];
		for($i=1;$i<13;$i++){
		
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$i,date('Y'));
		$sqlPart="SELECT sum(o.qtyOrder) qtyCarried  FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where g.name2='$product' and deli.Deliverydate between '$startDate' and '$endDate'  ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['qtyCarried']);
				}
				if($i==1){
					$totalVal=$total;
				}else{
				 $totalVal.="/".$total;
				}
		}
				if($c==0){
					$totalVal2=$totalVal;
				}else{
					$totalVal2.="-".$totalVal;	
				}
				 //$totalVal2.=;
		}
				//$totalPart=$totalPart+$total;
			echo $totalVal2;	
	}
	else if($controller_templet->getAction()=="dasbboard")
	{
		$order=new OrderService();
		$m=date('m');
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,$m,date('Y'));
		$sqlPart="SELECT sum(o.qtyOrder) quantity,count(o.id) noOrder   FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner  join tbldelivery deli on o.id=deli.orderId where deli.Deliverydate between '$startDate' and '$endDate'  ";
		//$sqlPart="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id inner tbldelivery deli on o.id=deli.orderId where (deli.Deliverydate between ('$startDate' and '$endDate') ) ";
				$total=0;
				echo "<table class='table table-striped table-hovered table-bordered'>";
				foreach($order->view_query($sqlPart) as $rowPart)
				{
					echo"<tr><td>Total Order </td><td>".$rowPart['noOrder']."</td></tr>";
					echo"<tr><td>Total Quantity</td><td>".$rowPart['quantity']."</td></tr>";
					echo"<tr><td>Day's Worked</td><td>".$order->noworkedDays()."</td></tr>";
					echo"<tr><td>Day's Lost</td><td>".$order->noDaysLost()."</td></tr>";
					echo"<tr><td>Day's Remaining</td><td>".(intval(cal_days_in_month(CAL_GREGORIAN,$m,date('Y')))-intval(date('d')))."</td></tr>";
				}
				echo"</table>";
	}else if($controller_templet->getAction()=="orderNotification")
	{
		$order=new OrderService();
		$sql="SELECT count(o.id) noOrder   FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where os.name='Ready'";
		$noorder=0;
		foreach($order->view_query($sql) as $row)
		{
			$noorder=$row['noOrder'];
		}
		echo "They are ".$noorder." new orders ready";
	}
}
?>