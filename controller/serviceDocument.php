<!--<p style="font-family:Calibri;">-->
<?php
require_once '../services/subServiceService.php';
require_once '../services/serviceHeaderService.php';
$serviceId=0;
$vehicle="";
$Driver="";
$ServiceDate="";
$serviceKm="";
$nextKm="";
$odometer=0;
$serviceType="";
if(isset($_REQUEST['serviceId']))
{
	$serviceId=$_REQUEST['serviceId'];
}
if(isset($_REQUEST['vehicleId']))
{
	$vehicle=$_REQUEST['vehicleId'];
	
}
if(isset($_REQUEST['driver']))
{
	$Driver=$_REQUEST['driver'];
	
}
	
	if(isset($_REQUEST['serviceDate']))
{
	$ServiceDate=$_REQUEST['serviceDate'];
	
}	
if(isset($_REQUEST['serviceKm']))
{
	$serviceKm=$_REQUEST['serviceKm'];
	
}
if(isset($_REQUEST['nextReading']))
{
	$nextKm=$_REQUEST['nextReading'];
	
}
if(isset($_REQUEST['odometer']))
{
	$odometer=$_REQUEST['odometer'];
	
}
if(isset($_REQUEST['serviceType']))
{
	$serviceType=$_REQUEST['serviceType'];
	
}
$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 12pt;">MARKH INVESTMENT LIMITED</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 10pt;">Preventive Maintenance</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<tbody>
<tr>
<td width="15%">Vehicle No</td>
<td width="10%">'.$vehicle.'</td>
<td width="45%">Driver</td>
<td width="20%">'.$Driver.'</td>
</tr>
<tr>
<td width="15%">Trailer No</td>
<td width="10%">'.$trailer.'</td>
<td width="45%">Service Type</td>
<td width="20%">'.$serviceType.'</td>
</tr>
<tr>
<td width="15%">Service Date</td>
<td width="10%">'.$ServiceDate.'</td>
<td width="45%"></td>
<td width="20%"></td>

</tr>
<tr>
<td width="15%">Km (Reading)</td>
<td width="10%">'.$serviceKm.'</td>
<td width="45%">Next Service</td>
<td width="20%">'.$nextKm.'</td>
</tr>

</tbody>
</table>

<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td width="15%">(PM)Component</td>
<td width="10%">Task</td>
<td width="45%">Comment(By Mechanic)</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
';

//<tr>
//<td align="center">MF1234567</td>
//<td align="center">10</td>
//<td>Large pack Hoover bags</td>
//<td class="cost">&pound;2.56</td>
//</tr>
$html2="";
$subService=new subServiceService();
$subService->setserviceHeaderId($serviceId);
//$sql="select * from tblsection where checkTypeId='".$checkListType->getType()."' and isActive='ON'";
		
			foreach($subService->viewHeader() as $row2)
			{
				
				$html2.= "<tr><td class='totals'>".$row2["componentName"]."</td><td class='totals'>".$row2['actionNote']."</td><td class='totals'>".$row2['comment']."</td></tr>";	
			}
			//echo"</tbody></table>";
		


$html.=$html2.'<!-- END ITEMS HERE -->
</tbody>
</table>
<div style="text-align: left; font-style: italic;">Prepaid by:........................ Signture:.......................</div>
<div style="text-align: left; font-style: italic;">Checked by:........................Signture:........................</div>
<div style="text-align: left; font-style: bold;"><h4>Mechanics Attending</h4></div>
<div style="text-align: left; font-style: italic;">Mechanic:........................ Signture:................</div>
<div style="text-align: left; font-style: italic;">Mechanic:........................ Signture:................</div>
<div style="text-align: left; font-style: italic;">Mechanic:........................ Signture:................</div>
<div style="text-align: left; font-style: italic;">Mechanic:........................ Signture:................</div>
<div style="text-align: left; font-style: italic;">Mechanic:........................ Signture:................</div>
<div style="text-align: center; font-style: italic;"></div>


</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");

$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("serviceinspectionjobcard");
$mpdf->SetAuthor("c3ds");
$mpdf->SetWatermarkText("BINOX FLEET SOFTWARE");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');



$mpdf->WriteHTML($html);


$mpdf->Output(); 
exit;

//exit;

?>