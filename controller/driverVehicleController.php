<?php 
 require_once('../services/driverVehicleService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $driverVehicle=new driverVehicleService();
 		 echo json_encode( $driverVehicle->view());
 }else if($controller_templet->getAction()=='add'){

		 $driverVehicle2=new driverVehicleService();
		 $driverVehicle2->setdriverId($_REQUEST['driverId']);
		 $driverVehicle2->updateAllDriverRecord();

	 	 $driverVehicle=new driverVehicleService();
 	 	 $driverVehicle->setdriverId($_REQUEST['driverId']);
	 	 $driverVehicle->setvehicleId($_REQUEST['vehicleId']);
	 	 $driverVehicle->setassignmentDate(date('Y-m-d'));
	 	 $driverVehicle->setisActive(true);
	 	 echo json_encode( $driverVehicle->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $driverVehicle=new driverVehicleService();
 	 	 $driverVehicle->setid($_REQUEST['id']);
	 	 $driverVehicle->setdriverId($_REQUEST['driverId']);
	 	 $driverVehicle->setvehicleId($_REQUEST['vehicleId']);
	 	 //$driverVehicle->setassignmentDate($_REQUEST['assignmentDate']);
	 	 //$driverVehicle->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $driverVehicle->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $driverVehicle=new driverVehicleService();
 	 	 $driverVehicle->setid($_REQUEST['id']);
		echo json_encode( $driverVehicle->delete());
}
 else if($controller_templet->getAction()=='viewCombo')
 {
		 $driverVehicle=new driverVehicleService();
 		 echo json_encode( $driverVehicle->viewConbox());
 }else if($controller_templet->getAction()=='getLastAssigment')
 {
		$driverVehicle=new driverVehicleService();
	 	$driverVehicle->setdriverId($_REQUEST['driverId']);
	 	echo json_encode($driverVehicle->findActive());

 }else if($controller_templet->getAction()=='getLastDriver')
 {
	 $driverVehicle=new driverVehicleService();
	 $driverVehicle->setvehicleId($_REQUEST['vehicleId']);
	 echo json_encode($driverVehicle->findActiveDrive());

 }
} ?>