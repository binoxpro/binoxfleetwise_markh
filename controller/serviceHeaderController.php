<?php 
 require_once('../services/serviceHeaderService.php');
 require_once('../services/subServiceService.php');
 require_once('../services/componentScheduleService.php');
 require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
			$serviceHeader=new serviceHeaderService();
			if(isset($_REQUEST['vehicleId'])){
				$serviceHeader->setvehicleId($_REQUEST['vehicleId']);
				echo json_encode( $serviceHeader->viewVehicleOn());
			}else{
				echo json_encode( $serviceHeader->view());
			}
	 }else if($controller_templet->getAction()=='add')
	 {
			 $serviceHeader=new serviceHeaderService();
			 $serviceHeader->setcreationDate($_REQUEST['creationDate']);
			 $serviceHeader->setserviceIntervalId($_REQUEST['serviceIntervalId']);
			 $serviceHeader->setkmReading($_REQUEST['kmReading']);
			 $serviceHeader->setnextReading($_REQUEST['nextReading']);
			 $serviceHeader->setreminderReading($_REQUEST['reminderReading']);
			 $serviceHeader->setisActive(true);
			 $serviceHeader->setvehicleId($_REQUEST['vehicleId']);
             $serviceHeader->save();
			 //sub service
			 $subService=new subServiceService();
			 $subService->setserviceHeaderId($serviceHeader->getid());
			 //component Object
			 $componentSchedule=new componentScheduleService();
			 $componentSchedule->setserviceIntervalId($_REQUEST['serviceIntervalId']);
			 foreach($componentSchedule->view() as $row)
			 {
				 $subService->setcomponentScheduleId($row['id']);
				 $subService->setdone("Maintence Due");
				 $subService->setcomment(" ");
				 $subService->save();
			 }
			 echo json_encode(array("success"=>true));
	 }
	 else if($controller_templet->getAction()=='edit')
	 {
			 $serviceHeader=new serviceHeaderService();
			 $serviceHeader->setid($_REQUEST['id']);
			 $serviceHeader->setcreationDate($_REQUEST['creationDate']);
			 $serviceHeader->setserviceIntervalId($_REQUEST['serviceIntervalId']);
			 $serviceHeader->setkmReading($_REQUEST['kmReading']);
			 $serviceHeader->setnextReading($_REQUEST['nextReading']);
			 $serviceHeader->setreminderReading($_REQUEST['reminderReading']);
			 $serviceHeader->setisActive($_REQUEST['isActive']);
			 $serviceHeader->setvehicleId($_REQUEST['vehicleId']);
			 echo json_encode( $serviceHeader->update());
			 //delete componenet

			 //insert new component

	}
	 else if($controller_templet->getAction()=='delete')
	 {
			 $serviceHeader=new serviceHeaderService();
			 $serviceHeader->setid($_REQUEST['id']);
			echo json_encode( $serviceHeader->delete());
	} else if($controller_templet->getAction()=='getLatest')
	 {
			 $serviceHeader=new serviceHeaderService();
			 $serviceHeader->setvehicleId($_REQUEST['id']);
			echo $serviceHeader->getLatest();
	}
} ?>