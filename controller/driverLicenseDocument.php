<!--<p style="font-family:Calibri;">-->
<?php
require_once '../services/checkedService.php';
require_once '../services/vehicleService.php';
	
$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 12pt;">FRED SEBYALA TRANSPORTERS LIMITED</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 10pt;">'.'Driving Licence Report'.'</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
FST &copy;2015 Page {PAGENO} of {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td width="15%">Driver ID</td>
<td width="10%">Name</td>
<td width="15%">License No</td>
<td width="10%">Class</td>
<td width="20%">Issue Date</td>
<td width="20%">Expiry Date</td>
<td width="10%">Status</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
';

//<tr>
//<td align="center">MF1234567</td>
//<td align="center">10</td>
//<td>Large pack Hoover bags</td>
//<td class="cost">&pound;2.56</td>
//</tr>
$checked=new CheckedService();
$html2="";
$sql="select li.driverId, CONCAT(concat(dr.firstName,' '),dr.lastName) Name,li.licenceNo,li.licenseClass,li.issueDate,li.expiryDate,Case when li.isActive=1 then 'Current' else 'Not In Use' end Status   from tbllicence li inner join tbldriver dr on li.driverId=dr.id ";
		foreach ($checked->view_query($sql) as $row2)
		{
			
					$html2.= "<tr><td class='totals'>".$row2["driverId"]."</td><td class='totals'>".$row2['Name']."</td><td class='totals'>".$row2['licenceNo']."</td><td class='totals'>".$row2['licenseClass']."</td><td class='totals'>".$row2['issueDate']."</td><td class='totals'>".$row2['expiryDate']."</td><td class='totals'>".$row2['Status']."</td></tr>";
					//$nox=0;
				
			
		}


$html.=$html2.'<!-- END ITEMS HERE -->
</tbody>
</table>



</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");

$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("fred sebyala transporter limited");
$mpdf->SetAuthor("code360 data solution");
$mpdf->SetWatermarkText("Fred Sebyala Transporter");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.2;
$mpdf->SetDisplayMode('fullpage');



$mpdf->WriteHTML($html);


$mpdf->Output(); exit;

exit;

?>