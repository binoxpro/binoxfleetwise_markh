<?php 
require_once('../services/vehicleNewService.php');
require_once('../services/individualAccountService.php');
require_once('../services/vehicleTypeService.php');
require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $vehicleNew=new vehicleNewService();
 		 echo json_encode( $vehicleNew->view());
 }else if($controller_templet->getAction()=='add')
 {
	 	 $vehicleNew=new vehicleNewService();
 	 	 $vehicleNew->setregNo($_REQUEST['regNo']);
	 	 $vehicleNew->setyom($_REQUEST['yom']);
	 	 $vehicleNew->setmodelId($_REQUEST['modelId']);
	 	 $vehicleNew->setdop($_REQUEST['dop']);
	 	 $vehicleNew->setchaseNumber($_REQUEST['chaseNumber']);
	 	 $vehicleNew->setengineCapacityCC($_REQUEST['enginecapacitycc']);
	 	 $vehicleNew->setvehicleTypeId($_REQUEST['vehicleTypeId']);
	 	 $vehicleNew->settonnage($_REQUEST['tonnage']);
	 	 $vehicleNew->setcolor($_REQUEST['color']);
	 	 $vehicleNew->setstatus($_REQUEST['status']);
	 	 $vehicleNew->setfuel($_REQUEST['fuel']);
	 	 $vehicleNew->setpurpose($_REQUEST['purpose']);
         $vehicleNew->setIsTrailer($_REQUEST['isTrailer']);

	 	 $vehicleNew->save();
         //account type for individual
         $individualAccount=new individualAccountService();
         $individualAccount->setId($vehicleNew->getid());
         $individualAccount->setAccountName($_REQUEST['regNo']);
         $individualAccount->setAccountCode(null);
         $individualAccount->setIsActive(true);
	     echo json_encode($individualAccount->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
	 $vehicleNew = new vehicleNewService();
	 $vehicleNew->setid($_REQUEST['id']);
    $vehicleNew->setregNo($_REQUEST['regNo']);
//	 $vehicleNew->setyom($_REQUEST['yom']);
//	 $vehicleNew->setmodelId($_REQUEST['modelId']);
//	 $vehicleNew->setdop($_REQUEST['dop']);
//	 $vehicleNew->setchaseNumber($_REQUEST['chaseNumber']);
//	 $vehicleNew->setengineCapactyCC($_REQUEST['engineCapactyCC']);
      $vehicleNew->setvehicleTypeId($_REQUEST['vehicleTypeId']);
//	 $vehicleNew->settonnage($_REQUEST['tonnage']);
//     $vehicleNew->setcolor($_REQUEST['color']);
//     $vehicleNew->setstatus($_REQUEST['status']);
//     $vehicleNew->setfuel($_REQUEST['fuel']);
//     $vehicleNew->setpurpose($_REQUEST['purpose']);
	 $vehicleNew->update();

	 $individualAccount = new individualAccountService();
	 $individualAccount->setId($vehicleNew->getid());
	 $individualAccount->setAccountName($_REQUEST['regNo']);
	 $individualAccount->setAccountCode(null);
	 $individualAccount->setIsActive(true);
	 echo json_encode($individualAccount->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $vehicleNew=new vehicleNewService();
 	 	 $vehicleNew->setid($_REQUEST['id']);
         echo json_encode( $vehicleNew->delete());
 }
 else if($controller_templet->getAction()=='viewCombo')
 {
	 $vehicleNew=new vehicleNewService();
	 if(isset($_REQUEST['vehicleType']))
	 {
		 $vehicleType=new VehicleTypeService();
		 $vehicleType->setvehicleType($_REQUEST['vehicleType']);
		 $vehicleType->setTypeId();
		 $vehicleNew->setvehicleTypeId($vehicleType->getid());
	 }else
	 {
		 $vehicleNew->setvehicleTypeId(null);
	 }
	 echo json_encode($vehicleNew->viewCombo());
 }else if($controller_templet->getAction()=='viewCombo2')
 {
     $vehicleNew=new vehicleNewService();
     echo json_encode($vehicleNew->viewCombo2());
 }
} ?>