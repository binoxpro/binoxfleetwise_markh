<?php

 ob_start();
// session_start();
require_once '../services/othercostManagementService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$othercostManagement=new OthercostManagementService();
		if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['vehicleId'])){
			$sd=$_REQUEST['startDate'];
			$ed=$_REQUEST['endDate'];
			$othercostManagement->setvehicleId($_REQUEST['vehicleId']);
			echo json_encode($othercostManagement->viewDateVehicle($sd,$ed));
		}else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])){
			$sd=$_REQUEST['startDate'];
			$ed=$_REQUEST['endDate'];

			echo json_encode($othercostManagement->viewDate($sd,$ed));
		}else if(isset($_REQUEST['vehicleId'])){
			$othercostManagement->setvehicleId($_REQUEST['vehicleId']);
			echo json_encode($othercostManagement->viewVehicle());
		}else
		{
			echo json_encode($othercostManagement->view());
		}

	}
	else if($controller_templet->getAction()=="add")
	{
		$othercostManagement=new OthercostManagementService();
		$othercostManagement->setvehicleId($_REQUEST['vehicleId']);
		$othercostManagement->setDescription($_REQUEST['description']);
		$othercostManagement->setamount($_REQUEST['amount']);
		$othercostManagement->setCreationDate($_REQUEST['creationDate']);
		$othercostManagement->setDestination($_REQUEST['destination']);
		echo json_encode($othercostManagement->save());
		
	}else if($controller_templet->getAction()=="edit")
	{
		$othercostManagement=new OthercostManagementService();
		$othercostManagement->setid($_REQUEST['id']);
		$othercostManagement->setvehicleId($_REQUEST['vehicleId']);
		$othercostManagement->setothercostId($_REQUEST['othercostId']);
		$othercostManagement->setamount($_REQUEST['amount']);
		echo json_encode($othercostManagement->update());
		
	}else if($controller_templet->getAction()=="getDetail")
	{
		$othercostManagement=new othercostManagementService();
		$othercostManagement->setid($_REQUEST['id']);
		$sql="select * from tblothercostManagement where id='".$othercostManagement->getid()."'";
		foreach($othercostManagement->view_query($sql) as $row)
		{
			echo "<strong>".$row['vehicleId']."</strong>";
		}
	}
}
?>
