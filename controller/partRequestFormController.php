<?php
ob_start();
require_once('../services/partRequestFormService.php');
require_once'../services/partRequestFormService.php';
require_once'../services/partRequestFormItemService.php';
require_once'../services/processhistoryService.php';
require_once'../services/storeprocesService.php';
require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $partRequestForm=new partRequestFormService();
 		 echo json_encode( $partRequestForm->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $partRequestForm=new partRequestFormService();
 	 	 $partRequestForm->setsection($_REQUEST['section']);
	 	 $partRequestForm->setvehicleId($_REQUEST['vehicleId']);
	 	 $partRequestForm->setrequestedBy($_REQUEST['requestedBy']);
	 	 $partRequestForm->setrequestDate($_REQUEST['requestDate']);
	 	 $partRequestForm->setstatus($_REQUEST['status']);
	 	 $partRequestForm->setjobcardId($_REQUEST['jobcardId']);
	 	 echo json_encode( $partRequestForm->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $partRequestForm=new partRequestFormService();
     if(isset($_REQUEST['prfId']))
     {$partRequestForm->setid($_REQUEST['prfId']);}
     if(isset($_REQUEST['section']))
     {$partRequestForm->setsection($_REQUEST['section']);}
     if(isset($_REQUEST['vehicleId']))
     {$partRequestForm->setvehicleId($_REQUEST['vehicleId']);}
     if(isset($_REQUEST['requestedBy']))
     {$partRequestForm->setrequestedBy($_REQUEST['requestedBy']);}
     if(isset($_REQUEST['requestDate']))
     {$partRequestForm->setrequestDate($_REQUEST['requestDate']);}
     if(isset($_REQUEST['status']))
     {$partRequestForm->setstatus($_REQUEST['status']);}
     if(isset($_REQUEST['jobcardId']))
     {$partRequestForm->setjobcardId($_REQUEST['jobcardId']);}

        $partRequestForm->update();

//Loading parts voucher

     $prfI=new partRequestFormItemService();
     $prfI->setprfId($_REQUEST['prfId']);
    //Issue,Issue/Purchase,Place Purchase,Rejected,Issued,Not Issued,PROCESS PURCHASE
     $Issue=0;
     $IssuePurchase=0;
     $PlacePurchase=0;
     $Rejected=0;
     $lussed=0;
     $NotIssued=0;
     $processPurchase=0;


     //$Issue=$prfI->getNumberStatusItem('Issue');
     $IssuePurchase=$prfI->getNumberStatusItem('Issue/Purchase');
     $PlacePurchase=$prfI->getNumberStatusItem('PROCESS PURCHASE');
     $Rejected=$prfI->getNumberStatusItem('Rejected');
     $lussed=$prfI->getNumberStatusItem('lssued');
     $NotIssued=$prfI->getNumberStatusItem('NOT ISSUED');

	 	 //work
     //echo $Issue."-IP:".$IssuePurchase.'-PP:'.$PlacePurchase.'-REJ:'.$Rejected=0;
     //process
     $processHistory=new processhistoryService();
     if($NotIssued<=0)
     {


         if ($Issue > 0) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], true, $_REQUEST['id']);
             //echo 'Get Id:'.$processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
         }
         if ($IssuePurchase > 0) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], true, $_REQUEST['id']);
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], false, $_REQUEST['id']);
             //$processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
         }

         if ($PlacePurchase > 0) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], false, $_REQUEST['id']);
             // get Id
             $processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
         }

         if ($Rejected > 0) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], false, $_REQUEST['id']);
             $processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
         }

         //
         if ($lussed > 0 and $PlacePurchase <= 0) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], true, $_REQUEST['id']);
             //echo 'Get Id:'.$processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
         }


         //echo $processHistory->getid();
         echo json_encode(array('success' => true));

     }else{
         echo json_encode(array('msg' => 'You have some items not processed'));
     }


 }else if($controller_templet->getAction()=='closeLocalpurchase')
 {
     $successCount=0;
     $errorCount=0;
     $errorMsg='Error Occurred while transacting please call support (:-:';
     $result=array();

     $partRequestForm=new partRequestFormService();

     if(isset($_REQUEST['prfId']))
     {$partRequestForm->setid($_REQUEST['prfId']);}
     $partRequestForm->find();
     if(isset($_REQUEST['section']))
     {$partRequestForm->setsection($_REQUEST['section']);}
     if(isset($_REQUEST['vehicleId']))
     {$partRequestForm->setvehicleId($_REQUEST['vehicleId']);}
     if(isset($_REQUEST['requestedBy']))
     {$partRequestForm->setrequestedBy($_REQUEST['requestedBy']);}
     if(isset($_REQUEST['requestDate']))
     {$partRequestForm->setrequestDate($_REQUEST['requestDate']);}
     if(isset($_REQUEST['status']))
     {$partRequestForm->setstatus($_REQUEST['status']);}
     if(isset($_REQUEST['jobcardId']))
     {
         $partRequestForm->setjobcardId($_REQUEST['jobcardId']);
     }

     $result=$partRequestForm->update();
    $controller_templet->returnResult($successCount,$errorCount,$result,$errorMsg);


//Loading parts voucher

     $prfI=new partRequestFormItemService();
     $prfI->setprfId($_REQUEST['prfId']);
     //Issue,Issue/Purchase,Place Purchase,Rejected,Issued,Not Issued,PROCESS PURCHASE, Bal Delivery
     $Issue=0;
     $IssuePurchase=0;
     $PlacePurchase=0;
     $Rejected=0;
     $lussed=0;
     $NotIssued=0;
     $processPurchase=0;
     $balDelivery=0;


     $Issue=$prfI->getNumberStatusItem('Issue');
     $IssuePurchase=$prfI->getNumberStatusItem('Issue/Purchase');
     $PlacePurchase=$prfI->getNumberStatusItem('PROCESS PURCHASE');
     $Rejected=$prfI->getNumberStatusItem('Rejected');
     $lussed=$prfI->getNumberStatusItem('lssued');
     $NotIssued=$prfI->getNumberStatusItem('NOT ISSUED');
     $balDelivery=$prfI->getNumberStatusItem('Bal Delivery');
     //work
     //echo $Issue."-IP:".$IssuePurchase.'-PP:'.$PlacePurchase.'-REJ:'.$Rejected=0;
     //process
     $processHistory=new processhistoryService();
     if($PlacePurchase<=0)
     {


         if ($Issue > 0 && $balDelivery == 0 ) {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], true, $_REQUEST['id']);
             //echo 'Get Id:'.$processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $result=array();
             $result=$storeProcess->save();
             $controller_templet->returnResult($successCount,$errorCount,$result,$errorMsg);
         }
         if ($balDelivery > 0)
         {
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], true, $_REQUEST['id']);
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $storeProcess->save();
             $processHistory->nextProcess('Store Managment', $_REQUEST['processId'], false, $_REQUEST['id']);
             //$processHistory->getid();
             //storeProcess
             $storeProcess = new storeprocesService();
             $storeProcess->setprfId($partRequestForm->getid());
             $storeProcess->setprocessHistoryId($processHistory->getid());
             $result=array();
             $result=$storeProcess->save();
             $controller_templet->returnResult($successCount,$errorCount,$result,$errorMsg);
         }
         //echo $processHistory->getid();
         //array('success' => true);
         if($errorCount>0)
         {
             echo json_encode(array('msg'=>$errorMsg));
         }else{
            echo json_encode(array('success'=>true));
         }

     }else
     {
         echo json_encode(array('msg' => 'You have some items not processed'));
     }


 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $partRequestForm=new partRequestFormService();
 	 	 $partRequestForm->setid($_REQUEST['id']);
         echo json_encode($partRequestForm->delete());
}else if($controller_templet->getAction()=='viewCombo')
{
		 $partRequestForm=new partRequestFormService();
 		 echo json_encode( $partRequestForm->viewConbox());
}
} ?>