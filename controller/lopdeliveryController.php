<?php
ob_start();
session_start();
 require_once('../services/lopdeliveryService.php');
 require_once('../services/storestockService.php');
 require_once ('../services/partRequestFormItemService.php');
 require_once('../services/localpurchaseorderService.php');
 require_once ('../services/localpurchaseorderitemService.php');

 require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $lopdelivery=new lopdeliveryService();
 		 echo json_encode( $lopdelivery->view());
        }
        else if($controller_templet->getAction()=='add'){
	     //if(intval($_REQUEST['quantityDeliveried']))
	 	 $lopdelivery=new lopdeliveryService();
 	 	 $lopdelivery->setpurfItemId($_REQUEST['purfitemId']);
	 	 $lopdelivery->setquantity($_REQUEST['quantityDeliveried']);
	 	 $lopdelivery->setpassed1($_REQUEST['passed1']=='YES'?1:0);
	 	 $lopdelivery->setpassed2($_REQUEST['passed2']);
	 	 $lopdelivery->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $lopdelivery->settaxNo($_REQUEST['taxNo']);
	 	 $lopdelivery->setdeliveryNo($_REQUEST['deliveryNo']);
	 	 $lopdelivery->setrecordedBy($_SESSION['username']);
	 	 $lopdelivery->save();

	 	 $prfi=new partRequestFormItemService();
	 	 $prfi->setid($_REQUEST['purfitemId']);
	 	 $prfi->find();
	 	 if($prfi->getquantity()<=floatval($_REQUEST['quantityDeliveried'])) {
             $prfi->setstatus('Issue');
         }
         if($prfi->getquantity()>floatval($_REQUEST['quantityDeliveried']))
         {
             $prfi->setstatus('Bal Delivery');
         }
         //update status.
         $prfi->update();


	 	 //lopitem
        $lopitem=new localpurchaseorderitemService();
        $lopitem->setid($_REQUEST['id']);
        $lopitem->find();

        $lop=new localpurchaseorderService();
        $lop->setid($lopitem->getlopId());
        $lop->find();


	 	 //ADD TO THE STORE TRANSACTION
         $storestock=new storestockService();
         $storestock->setitemId($prfi->getitemId());
         $storestock->setquantity($_REQUEST['quantityDeliveried']);
         $storestock->setunitPrice($_REQUEST['rate']);
         $storestock->setstocktype('IN');
         $storestock->setcreationDate($_REQUEST['deliveryDate']);
         $storestock->setrecordedBy($_SESSION['username']);
         $storestock->setreceivedBy($_SESSION['username']);
         $storestock->setsupplierId($lop->getsupplieId());
         $storestock->setcheckedBy($_SESSION['username']);
         echo json_encode($storestock->save());


 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $lopdelivery=new lopdeliveryService();
 	 	 $lopdelivery->setid($_REQUEST['id']);
	 	 $lopdelivery->setpurfItemId($_REQUEST['purfItemId']);
	 	 $lopdelivery->setquantity($_REQUEST['quantity']);
	 	 $lopdelivery->setpassed1($_REQUEST['passed1']);
	 	 $lopdelivery->setpassed2($_REQUEST['passed2']);
	 	 $lopdelivery->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $lopdelivery->settaxNo($_REQUEST['taxNo']);
	 	 $lopdelivery->setdeliveryNo($_REQUEST['deliveryNo']);
	 	 $lopdelivery->setrecordedBy($_REQUEST['recordedBy']);
	 	 echo json_encode( $lopdelivery->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $lopdelivery=new lopdeliveryService();
 	 	 $lopdelivery->setid($_REQUEST['id']);
         echo json_encode( $lopdelivery->delete());
}else if($controller_templet->getAction()=='viewCombo')
{
		 $lopdelivery=new lopdeliveryService();
 		 echo json_encode( $lopdelivery->viewConbox());
}
} ?>