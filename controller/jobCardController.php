<?php

 ob_start();
session_start();
require_once '../services/jobCardService.php';
require_once'utility.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$jobCard=new JobCardService();
		echo json_encode($jobCard->view());
	}
	else if($controller_templet->getAction()=="add")
	{
		$jobCard=new JobCardService();
		//$jobCard->setid($_REQUEST['id']);
		$jobCard->setjobNo($_REQUEST['jobNo']);
		$jobCard->setdate($_REQUEST['creationDate']);
		$jobCard->setvehicleId($_REQUEST['vehicleId']);
		$jobCard->settime($_REQUEST['time']);
		//$jobCard->setrectified($_REQUEST['rectified']);
		//$jobCard->setdateCompleted($_REQUEST['dateCompleted']);
		$jobCard->setodometer($_REQUEST['odometer']);
		//$jobCard->setComment($_REQUEST['comment']);
		//$jobCard->setgarrage($_REQUEST['garage']);
		//$jobCard->setfuelLevel($_REQUEST['fuelLevel']);
		$jobCard->setDriverName($_REQUEST['driverName']);
		$jobCard->setcreatedBy($_SESSION['username']);
		$jobCard->save();
		//change is in timing
		$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$jobCard->getid());
		$controller_templet->redirect();
		
	}else if($controller_templet->getAction()=="getAutoJobCardNumber")
	{
		//pliz refactor the code using a function defined in
		$jobCard=new jobCardService();
		$sql="select * from tbljobcard";
		$jobCard->con->setSelect_query($sql);
		$no=$jobCard->con->sqlCount();
		$prefix="MARKH";
		echo $prefix.utility::returnThreeFiguareString($no+1);
	}else if($controller_templet->getAction()=="editJobCard")
	{
		$sql="";
		$jobCard=new jobCardService();
		if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate'])&&isset($_REQUEST['jobId']))
		{
			$startDate=$_REQUEST['startDate'];
			$endDate=$_REQUEST['endDate'];
			$jobId=$_REQUEST['jobId'];
			$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where  (j.approvedBy is not null or approvedDate is not null) and  j.rectified is NULL and j.jobNo='$jobId'  and j.creationDate BETWEEN '$startDate' AND '$endDate'";

		}
		else if(isset($_REQUEST['startDate'])&&isset($_REQUEST['endDate']))
		{
			$startDate=$_REQUEST['startDate'];
			$endDate=$_REQUEST['endDate'];
			$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where (j.approvedBy is  null or approvedDate is null) and j.rectified is NULL and j.creationDate BETWEEN '$startDate' AND '$endDate'";

		}
		else if(isset($_REQUEST['jobId']))
		{

			$jobId=$_REQUEST['jobId'];
			$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where (j.approvedBy is  null or approvedDate is null) and j.rectified is NULL and j.jobNo='$jobId' ";

		}

		else{
			$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where (j.approvedBy is  null or approvedDate is null) and j.rectified is NULL";

		}
		//$sql="select j.*,v.regNo,concat(vm.makeName,'-',tmo.modelName) model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tblvehiclemake vm on v.makeId=vm.id inner join tbltruckmodel tmo on vm.id=tmo.makeId";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Make</th><th>Creation Date</th><th>Work Summary</th><th>Workdone Summary</th><th>Work Pending</th><th>Assigned Mechanics</th><th>Parts Requested Status</th><th>#</th><th></th></tr></thead><tbody>";
			foreach($jobCard->view_query($sql) as $row2)
			{
			echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['model']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$jobCard->getSummaryWork($row2['id'])."</td><td>".$jobCard->getSummaryWorkDo($row2['id'])."</td><td>".$jobCard->getSummaryWorkPending($row2['id'])."</td><td>".$jobCard->getSummaryofmechanic($row2['id'])."</td><td>".$jobCard->getSummaryofpartRequested($row2['id'])."</td><td><a href='admin.php?view=add_jobcard_items&id=".$row2['id']."' >Process</a></td><td><a href='controller/jobcardDocument.php?id=".$row2['id']."' target='blank' >view</a></td></tr>";
			}
			echo "</tbody></table></div>";
	}else if($controller_templet->getAction()=="followUpJobCard")
	{
		$jobCard=new jobCardService();
		$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where j.rectified is NULL";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Make</th><th>Creation Date</th><th>Work Summary</th><th>Workdone Summary</th><th>Part Issued</th><th>Part to Purchase</th><th>Assigned Mechanics</th><th>#</th></tr></thead><tbody>";
			foreach($jobCard->view_query($sql) as $row2)
			{
				echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['model']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td></td><td></td><td></td><td><a href='admin.php?view=add_jobcard_items&id=".$row2['id']."' >Check Out</a></td></tr>";
			}
			echo "</tbody></table></div>";
	}else if($controller_templet->getAction()=="detailView")
	{
		if(isset($_REQUEST['id']) && (!isset($_REQUEST['vehicleId']) && (!isset($_REQUEST['startDate']) && !isset($_REQUEST['endDate'])))){
			$jobCard=new jobCardService();
			$id=$_REQUEST['id'];
			echo "<h3>Repair and Service Details</h3>";
			$sqlCard="Select * from tbljobcarditem where jobcardId='$id'";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr class='info'><th>Repair/Service Details</th><th>Time Taken</th><th>Initial</th></tr></thead><tbody>";

			foreach($jobCard->view_query($sqlCard) as $row)
			{
				echo "<tr><td>".$row['details']."</td><td>".$row['timeTaken']."</td><td>".$row['initials']."</td></tr>";
			}
			echo "</tbody></table>";
			echo "<h3>Parts Used</h3>";
			$sql="select pu.*,v.regNo from tblpartsused pu inner join tbljobcard j on pu.jobcardId=j.id inner join tblvehicle v on j.vehicleId=v.id where pu.jobcardId='$id'";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr class='info'><th>Part Description</th><th>Quantity</th><th>Cost</th></tr></thead><tbody>";
			$total=0;
			foreach($jobCard->view_query($sql) as $row2)
			{
				$total=$total+intval($row2['cost']);
				echo "<tr><td>".$row2['partused']."</td><td>".$row2['quantity']."</td><td>".number_format($row2['cost'],0)."</td></tr>";	
			}
			echo"</tbody><tfoot><tr><td colspan='2'>Total</td><td>".number_format($total,0)."</td></tr>";
			echo "</tfoot></table></div>";
		}
	}else if($controller_templet->getAction()=="CompleteJobCard")
	{
			$jobCard=new jobCardService();
			$sql="select j.*,(SELECT supplierName from tblsupplier s where s.supplierCode=j.garrage) garage,v.regNo,vm.makeName model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id  where (j.approvedBy is not  null or approvedDate is not null) and j.rectified is NULL";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Make</th><th>Creation Date</th><th>Work Summary</th><th>Workdone Summary</th><th>Assigned Mechanics</th><th>Part Issued.</th><th>Part to puchase.</th><th>#</th><th>#</th><th>#</th></tr></thead><tbody>";
			foreach($jobCard->view_query($sql) as $row2)
			{
                echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['model']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$jobCard->getSummaryWork($row2['id'])."</td><td>".$jobCard->getSummaryWorkDo($row2['id'])."</td><td>".$jobCard->getSummaryWorkPending($row2['id'])."</td><td>".$jobCard->getSummaryofmechanic($row2['id'])."</td><td></td><td></td><td><a href='admin.php?view=complete_jobcard_items&id=".$row2['id']."' >Complete</a></td><td><a href='controller/jobcardDocument.php?id=".$row2['id']."' target='blank' >view</a></td></tr>";

                //echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['model']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td></td><td></td><td></td><td><a href='admin.php?view=complete_jobcard_items&id=".$row2['id']."' >Complete</a></td></tr>";
			}
			echo "</tbody></table></div>";
	}else if($controller_templet->getAction()=="LoadCheckList")
	{
			$jobcardItem=new JobcardItemService();
			$jobcardItem->setjobcardId($_REQUEST['id']);
			$sql="select * from tbljobcarditem where jobcardId='".$jobcardItem->getjobcardId()."'";
		
			
			echo "<form action='controller/jobcardItemController.php' name='jobCardItem'><div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<tr><thead><th>Details</th><th>Time Taken</th><th>Initials</th><th></th></tr></thead><tbody>";
			foreach($jobcardItem->view_query($sql) as $row2)
			{
				echo "<tr><td>".$row2['details']."</td><td>".$row2['timeTaken']."</td><td>".$row2['initials']."</td><td><a href='#' class='btn btn-link' onclick='editItem(this.id)' id='".$row2['id']."'>Edit</a><a href='controller/jobcardItemController.php?action=Delete&id=".$row2['id']."&jobcardId=".$row2['jobcardId']."' class='btn btn-link' >Delete</a></td></tr>";	
			}
			
			
			echo"</tbody></table></div></form>";
			
	}else if($controller_templet->getAction()=="complete")
	{
		
		$jobCard=new JobCardService();
		$jobCard->setid($_REQUEST['jobId']);
		//$jobCard->setjobNo($_REQUEST['jobNo']);
		//$jobCard->setdate($_REQUEST['creationDate']);
		//$jobCard->setvehicleId($_REQUEST['vehicleId']);
		//$jobCard->settime($_REQUEST['time']);
		$jobCard->setrectified($_REQUEST['approve']);
		$jobCard->setdateCompleted($_REQUEST['creationDate']);
		$jobCard->setlabourHour($_REQUEST['labourHour']);
		$jobCard->setlabourCost($_REQUEST['labourCost']);
		$jobCard->update();
		$controller_templet->setUrlHeader('../admin.php?view=Complete_JobCard');
		$controller_templet->redirect();
	}
	else if($controller_templet->getAction()=="generalJobCardReport")
	{
		if( (isset($_REQUEST['vehicleId']) && (isset($_REQUEST['startDate']) && isset($_REQUEST['endDate'])))){
			$vehicleId=$_REQUEST['vehicleId'];
			$startDate=$_REQUEST['startDate'];
			$endDate=$_REQUEST['endDate'];
			$jobCard=new jobCardService();
		$sql="select j.*,v.regNo,v.model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id where j.rectified is not NULL and j.vehicleId='$vehicleId' and (creationDate between '$startDate' and '$endDate' )";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered table-condensed'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Creation Date</th><th>Description</th><th>Parts Cost</th><th>Labour Cost</th><th>Labour Hour</th><th>#</th></tr></thead><tbody>";
			$totalPart=0;
			$totalLabour=0;
			foreach($jobCard->view_query($sql) as $row2)
			{
				$id=$row2['id'];
				$sqlDesc="select * from tbljobcarditem where jobcardId='$id'";
				$description="";
				foreach($jobCard->view_query($sqlDesc) as $rowDesc)
				{
					$description.=$rowDesc['details'].",";
				}
				
				$sqlPart="select * from tblpartsused where jobcardId='$id'";
				$total=0;
				foreach($jobCard->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['cost']);
				}
				$totalPart=$totalPart+$total;
				$totalLabour=$totalLabour+$row2['labourCost'];
				$description2=$description==""?$row2['comment']:$description;
				echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$description2."</td><td>".number_format($total,2)."</td><td>".number_format($row2['labourCost'],2)."</td><td>".$row2['labourHour']."</td><td><a href='#' onclick='view(".$id.")' ><i class='fa fa-file-pdf-o'></i></a></td></tr>";	
			}
			echo"<tr><td colspan='4'>Total</td><td>".number_format($totalPart,2)."</td><td>".number_format($totalLabour,2)."</td><td></td><td></td></tr>";
			echo "</tbody></table></div>";
			
			}else if( (isset($_REQUEST['vehicleId']) && (!isset($_REQUEST['startDate']) && !isset($_REQUEST['endDate'])))){
			$vehicleId=$_REQUEST['vehicleId'];
			//$startDate=$_REQUEST['startDate'];
			//$endDate=$_REQUEST['endDate'];
			$jobCard=new jobCardService();
		$sql="select j.*,v.regNo,v.model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id where j.rectified is not NULL and j.vehicleId='$vehicleId' ";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Creation Date</th><th>Description</th><th>Parts Cost</th><th>Labour Cost</th><th>Labour Hour</th><th>#</th></tr></thead><tbody>";
			$totalPart=0;
			$totalLabour=0;
			foreach($jobCard->view_query($sql) as $row2)
			{
				$id=$row2['id'];
				$sqlDesc="select * from tbljobcarditem where jobcardId='$id'";
				$description="";
				foreach($jobCard->view_query($sqlDesc) as $rowDesc)
				{
					$description.=$rowDesc['details'].",";
				}
				
				$sqlPart="select * from tblpartsused where jobcardId='$id'";
				$total=0;
				foreach($jobCard->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['cost']);
				}
				$totalPart=$totalPart+$total;
				$totalLabour=$totalLabour+$row2['labourCost'];
				$description2=$description==""?$row2['comment']:$description;
				echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$description2."</td><td>".number_format($total,2)."</td><td>".number_format($row2['labourCost'],2)."</td><td>".$row2['labourHour']."</td><td><a href='#' onclick='view(".$id.")' ><i class='fa fa-file-pdf-o'></i></a></td></tr>";	
			}
			echo"<tr><td colspan='4'>Total</td><td>".number_format($totalPart,2)."</td><td>".number_format($totalLabour,2)."</td><td></td><td></td></tr>";
			echo "</tbody></table></div>";
			
			} else if( (!isset($_REQUEST['vehicleId']) && (isset($_REQUEST['startDate']) && isset($_REQUEST['endDate'])))){
			//$vehicleId=$_REQUEST['vehicleId'];
			$startDate=$_REQUEST['startDate'];
			$endDate=$_REQUEST['endDate'];
			$jobCard=new jobCardService();
		$sql="select j.*,v.regNo,v.model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id where j.rectified is not NULL and (creationDate between '$startDate' and '$endDate' )";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Creation Date</th><th>Description</th><th>Parts Cost</th><th>Labour Cost</th><th>Labour Hour</th><th>#</th></tr></thead><tbody>";
			$totalPart=0;
			$totalLabour=0;
			foreach($jobCard->view_query($sql) as $row2)
			{
				$id=$row2['id'];
				$sqlDesc="select * from tbljobcarditem where jobcardId='$id'";
				$description="";
				foreach($jobCard->view_query($sqlDesc) as $rowDesc)
				{
					$description.=$rowDesc['details'].",";
				}
				
				$sqlPart="select * from tblpartsused where jobcardId='$id'";
				$total=0;
				foreach($jobCard->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['cost']);
				}
				$totalPart=$totalPart+$total;
				$totalLabour=$totalLabour+$row2['labourCost'];
				$description2=$description==""?$row2['comment']:$description;
				echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$description2."</td><td>".number_format($total,2)."</td><td>".number_format($row2['labourCost'],2)."</td><td>".$row2['labourHour']."</td><td><a href='#' onclick='view(".$id.")' ><i class='fa fa-file-pdf-o'></i></a></td></tr>";	
			}
			echo"<tr><td colspan='4'>Total</td><td>".number_format($totalPart,2)."</td><td>".number_format($totalLabour,2)."</td><td></td><td></td></tr>";
			echo "</tbody></table></div>";
			
			}else{
		$jobCard=new jobCardService();
		$sql="select j.*,v.regNo,v.model from tbljobcard j inner join tblvehicle v on j.vehicleId=v.id where j.rectified is not NULL";
			echo "<div class='table-responsive'><table class='table table-striped table-bordered'>";
			echo "<thead><tr><th>JobCard No</th><th>Reg No</th><th>Creation Date</th><th>Description</th><th>Parts Cost</th><th>Labour Cost</th><th>Labour Hour</th><th>#</th></tr></thead><tbody>";
			$totalPart=0;
			$totalLabour=0;
			foreach($jobCard->view_query($sql) as $row2)
			{
				$id=$row2['id'];
				$sqlDesc="select * from tbljobcarditem where jobcardId='$id'";
				$description="";
				foreach($jobCard->view_query($sqlDesc) as $rowDesc)
				{
					$description.=$rowDesc['details'].",";
				}
				
				$sqlPart="select * from tblpartsused where jobcardId='$id'";
				$total=0;
				foreach($jobCard->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['cost']);
				}
				$totalPart=$totalPart+$total;
				$totalLabour=$totalLabour+$row2['labourCost'];
				$description2=$description==""?$row2['comment']:$description;
				echo "<tr><td>".$row2['jobNo']."</td><td>".$row2['regNo']."</td><td>".$row2['creationDate']."/".$row2['time']."</td><td>".$description2."</td><td>".number_format($total,2)."</td><td>".number_format($row2['labourCost'],2)."</td><td>".$row2['labourHour']."</td><td><a href='#' onclick='view(".$id.")' ><i class='fa fa-file-pdf-o'></i></a></td></tr>";	
			}
			echo"<tr><td colspan='4'>Total</td><td>".number_format($totalPart,2)."</td><td>".number_format($totalLabour,2)."</td><td></td><td></td></tr>";
			echo "</tbody></table></div>";
			/*echo'<!-- Modal -->
            <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Details</h3>
            </div>
            <div class="modal-body">
            <p>One fine body…</p>
            </div>
            <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
			</div>';*/
			}
	}
	else if($controller_templet->getAction()=="graphVal")
	{
		$jobCard=new jobCardService();
		$totalVal="";
		for($i=1;$i<=12;$i++){
		$m=$i<10?"0".($i):($i);
		$startDate=date('Y')."-".$m."-"."01";
		$endDate=date('Y')."-".$m."-".cal_days_in_month(CAL_GREGORIAN,intval($i),date('Y'));
		$sqlPart="select pud.* from tblpartsused pud inner join tbljobcard jc on pud.jobcardId=jc.id where jc.creationDate between '$startDate' and '$endDate'";
				$total=0;
				foreach($jobCard->view_query($sqlPart) as $rowPart)
				{
					$total=$total+floatval($rowPart['cost']);
				}
				if($i==1){
					$totalVal=$total;
				}else
					{
				 $totalVal.="/".$total;
				}
		}
				echo $totalVal;
				//$totalPart=$totalPart+$total;
				
	}
	else if($controller_templet->getAction()=="softcopy_jobcard")
	{
		if(isset($_REQUEST['jobcardId']))
		{
			$htmlContent='';
			$jobcard=new jobCardService();
			$id=$_REQUEST['jobcardId'];
			$prfId=isset($_REQUEST['prfId'])?$_REQUEST['prfId']:-1;
			//jobcard header
			$sql="SELECT jc.id, jc.jobNo,jc.creationDate,jc.dateCompleted,jc.odometer,jc.checklistId,jc.createdBy,jc.approvedBy,jc.approvedDate,jc.driverName,jc.time,jc.comment,v.regNo,tm.modelName model,vm.makeName make FROM tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id inner join tbltruckmodel tm on v.modelId=tm.id inner join tblvehiclemake vm on tm.makeId=vm.id where jc.id='".$id."'";
			$htmlContent.='<table class="table table-bordered table-responsive table-striped"><thead>';
			foreach($jobcard->view_query($sql) as $row)
			{
				$htmlContent.="<tr><th>Serial No.</th><th colspan='5'>".$row['jobNo']."</th></tr>";
                $htmlContent.="<tr><th>Vehicle No.</th><th>".$row['regNo']."</th><th>Make</th><th>".$row['model']."/".$row['make']."</th><th>Mileage</th><th>".$row['odometer']."</th></tr>";
                $htmlContent.="<tr><th>Driver.</th><th>".$row['driverName']."</th><th>Time:</th><th>".$row['time']."</th><th>Date</th><th>".$row['creationDate']."</th></tr>";
			}
			$htmlContent.="</thead></table><h4>Work Description</h4>";


			//the carried workout
            $htmlContent.='<table class="table table-bordered table-responsive table-striped"><thead><tr><th>Work Description </th><th>Mechanic</th><th>Status</th></tr></thead><tbody>';
			$sqlWorkDescripition="SELECT jbi.* FROM tbljobcarditem jbi where jbi.jobcardId='".$id."' order by jbi.status ";

            foreach($jobcard->view_query($sqlWorkDescripition) as $rowDescription)
            {
                $htmlContent.="<tr><td>".$rowDescription['details']."</td><td>".$rowDescription['initials']."</td><td>".$rowDescription['status']."</td></tr>";
            }
            $htmlContent.="</tbody></table><h4>Part Request Form.</h4>";
            // the spare parts for the request.
			if(isset($_REQUEST['prfId'])) {


                $sqlSparePart = "select * from partvoucher where prfId='" . $prfId . "'";
                $htmlContent .= '<table class="table table-bordered table-responsive table-striped"><thead><tr><th>Item Description </th><th>Qty</th><th>Qty In Store</th><th>Issued</th><th>Status</th></tr></thead><tbody>';
                foreach ($jobcard->view_query($sqlSparePart) as $rowPart) {
                    $htmlContent .= "<tr><td>" . $rowPart['itemName'] . "</td><td>" . $rowPart['quantity'] . "</td><td>" . $rowPart['quantityInStore'] . "</td><td>" . $rowPart['quantityIssued'] . "</td><td>" . $rowPart['status'] . "</td></tr>";
                }
            }
            $htmlContent.="</tbody></table>";






            echo $htmlContent;

		}
	}
}
?>
