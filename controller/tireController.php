<?php 
 require_once('../services/tireService.php');
 require_once('../services/tireTreadDepthService.php');
 require_once('../services/tireHoldService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $tire=new tireService();
         if(isset($_REQUEST['status']))
         {
            echo json_encode( $tire->viewStatus($_REQUEST['status']));
         }else if(isset($_REQUEST['statusx']))
         {
            echo json_encode( $tire->viewStatusx($_REQUEST['statusx']));
         }else if(isset($_REQUEST['combox']))
         {
            echo json_encode( $tire->viewCombox());
         }else if(isset($_REQUEST['combox2']))
         {
            echo json_encode($tire->viewCombox2($_REQUEST['combox2']));
         }else
         {
            echo json_encode($tire->view());
         } 
 }else if($controller_templet->getAction()=='add'){
	 	 $tire=new tireService();
 	 	 $tire->setserialNumber($_REQUEST['serialNumber']);
	 	 $tire->settireBand($_REQUEST['tireBand']);
	 	 $tire->settirePatternId($_REQUEST['tirePatternId']);
	 	 $tire->settireSizeId($_REQUEST['tireSizeId']);
	 	 $tire->setretreadDepth($_REQUEST['retreadDepth']);
	 	 $tire->setmaxmiumPressure($_REQUEST['maxmiumPressure']);
	 	 $tire->settireType($_REQUEST['tireType']);
	 	 $tire->setcost($_REQUEST['cost']);
	 	 $tire->setkms($_REQUEST['kms']);
	 	 $tire->setstatus($_REQUEST['status']);
	 	 $tire->setisActive($_REQUEST['isActive']);
         if(isset($_REQUEST['id']))
         {
           
            $tire2=new tireService();
            $tire2->setid($_REQUEST['id']);
            $tire2->getObject();
			
            $tire2->setstatus("Returned from Retread");
            $tire2->update();
			if($tire2->getParentId()>0&&!is_null($tire2->getParentId()))
			{
				 $tire->setParentId($tire2->getParentId());
			}else
			{
				 $tire->setParentId($_REQUEST['id']);
			}
			
         }else{
            $tire->setParentId(0);
         }
 	      $id=$tire->save();
          
          $tireTreadDepth=new tireTreadDepthService();
          $tireTreadDepth->settireId($id);
          $tireTreadDepth->settreadDepth($_REQUEST['retreadDepth']);
          $tireTreadDepth->setCreationDate($_REQUEST['creationDate']);
          echo json_encode($tireTreadDepth->save());
          
          
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $tire=new tireService();
 	 	 $tire->setid($_REQUEST['id']);
	 	 $tire->setserialNumber($_REQUEST['serialNumber']);
	 	 $tire->settireBand($_REQUEST['tireBand']);
	 	 $tire->settirePatternId($_REQUEST['tirePatternId']);
	 	 $tire->settireSizeId($_REQUEST['tireSizeId']);
	 	 $tire->setretreadDepth($_REQUEST['retreadDepth']);
	 	 $tire->setmaxmiumPressure($_REQUEST['maxmiumPressure']);
	 	 $tire->settireType($_REQUEST['tireType']);
	 	 $tire->setcost($_REQUEST['cost']);
	 	 $tire->setkms($_REQUEST['kms']);
	 	 $tire->setstatus($_REQUEST['status']);
	 	 $tire->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $tire->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $tire=new tireService();
 	 	 $tire->setid($_REQUEST['id']);
echo json_encode( $tire->delete()); } 
else if($controller_templet->getAction()=='getStock'){
 	 	 $tire=new tireService();
 	 	 $tire->setid($_REQUEST['id']);
            $tire->getObject();
            $tireTreadDepht=new tireTreadDepthService();
            $tireTreadDepht->settireId($tire->getid());
            $tireTreadDepht->getObjectActual();
            echo $tire->getcost()."*".$tireTreadDepht->gettreadDepth()."*".$tire->getmaxmiumPressure()."*".$tire->getkms();
        
  } else if($controller_templet->getAction()=='repaired'){
 	 	 $tire=new tireService();
 	 	 $tire->setid($_REQUEST['id']);
         $tire->getObject();
         $tire->setstatus('Instock');
         $tireHold=new tireHoldService();
         $tireHold->settireId($_REQUEST['id']);
         $tire->setkms($tireHold->getObjectTireDistance());
        echo json_encode( $tire->update()); 
        } 
} ?>