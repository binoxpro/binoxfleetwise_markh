<?php

 ob_start();
// session_start();
require_once '../services/trailerService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$trailer=new TrailerService();
		echo json_encode($trailer->view());
	}
	else if($controller_templet->getAction()=="add")
	{
		$trailer=new TrailerService();
		$trailer->setTractorHeadId($_REQUEST['tractorheadId']);
		$trailer->setTrailerId($_REQUEST['trailerId']);
		$trailer->setIsActive('ON');
		echo json_encode($trailer->save());
		
	}
}
?>
