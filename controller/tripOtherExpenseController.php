<?php 
 require_once('../services/tripOtherExpenseService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
		 $TripOtherExpense=new TripOtherExpenseService();
		 $TripOtherExpense->settripNo($_REQUEST['tripNo']);
 		 echo json_encode( $TripOtherExpense->view());
 	}else if($controller_templet->getAction()=='add'){
	 	 $TripOtherExpense=new TripOtherExpenseService();
 	 	 $TripOtherExpense->settripNo($_REQUEST['tripNo']);
	 	 $TripOtherExpense->setexpenseId($_REQUEST['expenseId']);
	 	 $TripOtherExpense->setAmount($_REQUEST['Amount']);
	 	 echo json_encode( $TripOtherExpense->save());
	 }
	 else if($controller_templet->getAction()=='edit')
	 {
 	 	 $TripOtherExpense=new TripOtherExpenseService();
 	 	 $TripOtherExpense->setid($_REQUEST['id']);
	 	 $TripOtherExpense->settripNo($_REQUEST['tripNo']);
	 	 $TripOtherExpense->setexpenseId($_REQUEST['expenseId']);
	 	 $TripOtherExpense->setAmount($_REQUEST['Amount']);
	 	 echo json_encode( $TripOtherExpense->update());
	 }
	 else if($controller_templet->getAction()=='delete')
	 {
 	 	 $TripOtherExpense=new TripOtherExpenseService();
 	 	 $TripOtherExpense->setid($_REQUEST['id']);
		echo json_encode( $TripOtherExpense->delete());
	}else if($controller_templet->getAction()=='viewCombo')
	 {
		 $TripOtherExpense=new TripOtherExpenseService();
 		 echo json_encode( $TripOtherExpense->viewConbox());
 	}else if($controller_templet->getAction()=='viewPayment')
	 {
		 $tripOtherExpense=new TripOtherExpenseService();
		 $tripOtherExpense->settripNo($_REQUEST['tripNo']);
		 echo json_encode($tripOtherExpense->viewPayment());
	 }
} ?>