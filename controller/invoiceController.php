<?php 
 require_once('../services/invoiceService.php');
require_once('utility.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $invoice=new invoiceService();
 		 echo json_encode( $invoice->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $invoice=new invoiceService();
		 $invoice->setid($_REQUEST['id']);
 	 	 $invoice->setcreationDate($_REQUEST['creationDate']);
	 	 $invoice->setconfirmed(0);
	 	 $invoice->setcurrencyId($_REQUEST['currencyId']);
	 	 $invoice->setcustomerId($_REQUEST['customerId']);
	 	 echo json_encode( $invoice->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $invoice=new invoiceService();
 	 	 $invoice->setid($_REQUEST['id']);
	 	 $invoice->setcreationDate($_REQUEST['creationDate']);
	 	 $invoice->setconfirmed($_REQUEST['confirmed']);
	 	 $invoice->setcurrencyId($_REQUEST['currencyId']);
	 	 $invoice->setcustomerId($_REQUEST['customerId']);
	 	 echo json_encode( $invoice->update());
 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $invoice=new invoiceService();
 	 	 $invoice->setid($_REQUEST['id']);
		echo json_encode( $invoice->delete());
}else if($controller_templet->getAction()=='viewCombo'){  
		 $invoice=new invoiceService();
 		 echo json_encode( $invoice->viewConbox());
} else if($controller_templet->getAction()=='getInvoiceNo')
 {
	 $invoice=new invoiceService();
	 echo utility::returnThreeFiguareString($invoice->invoiceNumbering());


 }else if($controller_templet->getAction()=='getInvoiceObject')
 {
	 $invoice=new invoiceService();
	 $invoice->setid($_REQUEST['id']);
	 echo json_encode($invoice->findObject());
 }
 } ?>