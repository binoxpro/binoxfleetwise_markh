<?php 
 require_once('../services/chartOfaccountService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
		 $chartOfaccount=new chartOfaccountService();
 		 echo json_encode( $chartOfaccount->view());
	}else if($controller_templet->getAction()=='add')
	 {
		 $chartOfaccount=new chartOfaccountService();
		 $chartOfaccount->setAccountCode($_REQUEST['AccountCode']);


 	 	 $chartOfaccount->setAccountName($_REQUEST['AccountName']);
	 	 $chartOfaccount->setAccountTypeCode($_REQUEST['AccountTypeCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $chartOfaccount->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $chartOfaccount->setIsActive(false);
		 }

	 	 $chartOfaccount->setParentAccountCode($_REQUEST['ParentAccountCode']);
	 	 echo json_encode( $chartOfaccount->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $chartOfaccount=new chartOfaccountService();
 	 	 $chartOfaccount->setAccountCode($_REQUEST['AccountCode']);
	 	 $chartOfaccount->setAccountName($_REQUEST['AccountName']);
	 	 $chartOfaccount->setAccountTypeCode($_REQUEST['AccountTypeCode']);
		 if(isset($_REQUEST['IsActive']))
		 {
			 $chartOfaccount->setIsActive($_REQUEST['IsActive']);
		 }else
		 {
			 $chartOfaccount->setIsActive(false);
		 }
	 	 $chartOfaccount->setParentAccountCode($_REQUEST['ParentAccountCode']);
	 	 echo json_encode( $chartOfaccount->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $chartOfaccount=new chartOfaccountService();
 	 	 $chartOfaccount->setAccountCode($_REQUEST['AccountCode']);
echo json_encode( $chartOfaccount->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){
	 $chartOfaccount=new chartOfaccountService();
	 echo json_encode( $chartOfaccount->viewConbox());
}else if($controller_templet->getAction()=='viewComboType'){
	 $chartOfaccount=new chartOfaccountService();
	 $chartOfaccount->setAccountTypeCode($_REQUEST['accountType']);
	 echo json_encode( $chartOfaccount->viewConboxoftype());
 }
 } ?>