<?php
//session_start();
require_once('../services/paymentVoucherService.php');
require_once('controller.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/tripService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/fuelOrderService.php');
 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
		 $paymentVoucher=new paymentVoucherService();
 		 echo json_encode( $paymentVoucher->view());
	}
	 	else if($controller_templet->getAction()=='add')
 	{
		$post=false;
	 	 $paymentVoucher=new paymentVoucherService();
		$paymentVoucher->setid($_REQUEST['id']);
 	 	 $paymentVoucher->setsourceAccount($_REQUEST['sourceAccount']);
	 	 $paymentVoucher->setpreparedBy($_SESSION['username']);
	 	 //$paymentVoucher->setAuthorisedBy($_REQUEST['AuthorisedBy']);
		if(isset($_REQUEST['Posted']))
		{
			$paymentVoucher->setPosted($_REQUEST['Posted']);
			$paymentVoucher->setapproved(true);
			$post=true;
		}else
		{
			$paymentVoucher->setPosted(false);
			$paymentVoucher->setapproved(false);
		}

	 	 $paymentVoucher->settransactionDate($_REQUEST['transactionDate']);
	 	 $paymentVoucher->setcreationDate(date('Y-m-d'));
	 	 $paymentVoucher->setsourceRef($_REQUEST['sourceRef']);

	 	 $paymentVoucher->save();
		//check if you can post the data to account;
		if($post)
		{
			$paymentVoucherItem=new paymentVoucherItemService();
			$paymentVoucherItem->setpayVoucherId($_REQUEST['id']);
			$totalAmount=$paymentVoucherItem->findTotalCost();
			if($totalAmount>0)
			{


				$ledgerHead = new ledgerHeadService();
				$ledgerHead->settransactionDate($_REQUEST['transactionDate']);
				$ledgerHead->setmonthId($_REQUEST['monthCode']);
				$ledgerHead->setamount($totalAmount);
				$ledgerHead->save();

				//Cr
				$generalLedger = new generalLedgerService();
				$generalLedger->setledgerHeadId($ledgerHead->getid());
				$generalLedger->setaccountCode($_REQUEST['sourceAccount']);
				$generalLedger->setnarrative('Payment voucher posting');
				$generalLedger->setreference($_REQUEST['sourceRef']);
				$generalLedger->setamount($totalAmount);
				$generalLedger->settransactionType('Cr');
				$generalLedger->setisActive(true);
				$generalLedger->save();


				//Dr
				$sql = "select * from  paymentvoucheritem  where payVoucherId='" .$paymentVoucherItem->getpayVoucherId() . "'";
				foreach ($paymentVoucherItem->view_query($sql) as $row) {
					$generalLedger->setaccountCode($row['accountCode']);
					$generalLedger->setnarrative($row['particular']);
					$generalLedger->setreference('PV-' . $row['payVoucherId']);
					$generalLedger->setamount($row['amount']);
					$generalLedger->settransactionType('Dr');
					if (!is_null($row['costCenter'])&&$row['costCenter']!='')
					{
						$generalLedger->setcostCentreId($row['costCenter']);
					}
					//$generalLedger->setcostCentreId();
					if (!is_null($row['tripNo'])&&$row['tripNo']!='')
					{
						$generalLedger->settripNo($row['tripNo']);
					}
					if (!is_null($row['individualCode'])&&$row['individualCode']!='')
					{
						$generalLedger->setindividualNo($row['individualCode']);
					}
					$generalLedger->setisActive(true);
					$generalLedger->save();

				}
			}


		}
		echo json_encode(array('success'=>true));
 	}
 		else if($controller_templet->getAction()=='edit')
 	{
 	 	 $paymentVoucher=new paymentVoucherService();
 	 	 $paymentVoucher->setid($_REQUEST['id']);
	 	 $paymentVoucher->setsourceAccount($_REQUEST['sourceAccount']);
	 	 $paymentVoucher->setpreparedBy($_REQUEST['preparedBy']);
	 	 $paymentVoucher->setAuthorisedBy($_REQUEST['AuthorisedBy']);
	 	 $paymentVoucher->setPosted($_REQUEST['Posted']);
	 	 $paymentVoucher->settransactionDate($_REQUEST['transactionDate']);
	 	 $paymentVoucher->setcreationDate($_REQUEST['creationDate']);
	 	 $paymentVoucher->setsourceRef($_REQUEST['sourceRef']);
	 	 $paymentVoucher->setapproved($_REQUEST['approved']);
	 	 echo json_encode( $paymentVoucher->update());
 	}
 		else if($controller_templet->getAction()=='delete')
	{
	 	$paymentVoucher=new paymentVoucherService();
	 	$paymentVoucher->setid($_REQUEST['id']);
		echo json_encode( $paymentVoucher->delete());
	}
		else if($controller_templet->getAction()=='viewCombo')
	{
		 $paymentVoucher=new paymentVoucherService();
 		 echo json_encode( $paymentVoucher->viewConbox());
	}
 		else if($controller_templet->getAction()=='getVoucherNo')
 	{
		$paymentVoucher=new paymentVoucherService();
		if(isset($_REQUEST['id']))
		{
			$paymentVoucher->setid($_REQUEST['id']);
			echo json_encode($paymentVoucher->findJson());
		}else
		{
			echo $paymentVoucher->getVoucherNo();
		}


 	}else if($controller_templet->getAction()=='tripExpensePayment')
	{
			//payment Voucher
		$paymentVoucher=new paymentVoucherService();
		$paymentVoucher->setid($_REQUEST['idV']);
		$paymentVoucher->setsourceAccount($_REQUEST['sourceAccount']);
		$paymentVoucher->setpreparedBy($_SESSION['username']);
		//$paymentVoucher->setAuthorisedBy($_REQUEST['AuthorisedBy']);
		if(isset($_REQUEST['Posted']))
		{
			$paymentVoucher->setPosted($_REQUEST['Posted']);
			$paymentVoucher->setapproved(true);
			$post=true;
		}else
		{
			$paymentVoucher->setPosted(true);
			$paymentVoucher->setapproved(true);
		}

		$paymentVoucher->settransactionDate($_REQUEST['transactionDate']);
		$paymentVoucher->setcreationDate(date('Y-m-d'));
		$paymentVoucher->setsourceRef($_REQUEST['sourceRef']);

		$paymentVoucher->save();
			//payment voucher Item
		$paymentVoucherItem=new paymentVoucherItemService();
		$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
		$paymentVoucherItem->setparticular($_REQUEST['particular']);
		$paymentVoucherItem->setamount($_REQUEST['amount']);
		$paymentVoucherItem->setaccountCode($_REQUEST['accountCode']);
		$paymentVoucherItem->setindividualCode($_REQUEST['paidTo']);

		$trip=new TripService();
		$trip->setTripNumber($_REQUEST['tripNo3']);
		$trip->find();

		$paymentVoucherItem->setcostCenter($trip->getVehicleId());
		$paymentVoucherItem->settripNo($_REQUEST['tripNo3']);
		$paymentVoucherItem->save();

		$paymentVoucherItem=new paymentVoucherItemService();
		$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
		$totalAmount=$paymentVoucherItem->findTotalCost();
		if($totalAmount>0)
		{
			$ledgerHead = new ledgerHeadService();
			$ledgerHead->settransactionDate($_REQUEST['transactionDate']);
			$ledgerHead->setmonthId($_REQUEST['monthCode']);
			$ledgerHead->setamount($totalAmount);
			$ledgerHead->save();

			//Cr
			$generalLedger = new generalLedgerService();
			$generalLedger->setledgerHeadId($ledgerHead->getid());
			$generalLedger->setaccountCode($_REQUEST['sourceAccount']);
			$generalLedger->setnarrative('Payment voucher posting');
			$generalLedger->setreference($_REQUEST['sourceRef']);
			$generalLedger->setamount($totalAmount);
			$generalLedger->settransactionType('Cr');
			$generalLedger->setisActive(true);
			$generalLedger->save();


			//Dr
			$sql = "select * from  paymentvoucheritem  where payVoucherId='" .$paymentVoucherItem->getpayVoucherId() . "'";
			foreach ($paymentVoucherItem->view_query($sql) as $row) {
				$generalLedger->setaccountCode($row['accountCode']);
				$generalLedger->setnarrative($row['particular']);
				$generalLedger->setreference('PV-' . $row['payVoucherId']);
				$generalLedger->setamount($row['amount']);
				$generalLedger->settransactionType('Dr');
				if (!is_null($row['costCenter'])&&$row['costCenter']!='')
				{
					$generalLedger->setcostCentreId($row['costCenter']);
				}
				//$generalLedger->setcostCentreId();
				if (!is_null($row['tripNo'])&&$row['tripNo']!='')
				{
					$generalLedger->settripNo($row['tripNo']);
				}
				if (!is_null($row['individualCode'])&&$row['individualCode']!='')
				{
					$generalLedger->setindividualNo($row['individualCode']);
				}
				$generalLedger->setisActive(true);
				$generalLedger->save();

			}
		}

		echo json_encode(array('success'=>true));


	}else if($controller_templet->getAction()=='fuelPayment')
		{


			//payment Voucher
			$paymentVoucher=new paymentVoucherService();
			$paymentVoucher->setid($_REQUEST['idV']);

			$paymentVoucher->setsourceAccount($_REQUEST['sourceAccount']);
			$paymentVoucher->setpreparedBy($_SESSION['username']);
			//$paymentVoucher->setAuthorisedBy($_REQUEST['AuthorisedBy']);
			if(isset($_REQUEST['Posted']))
			{
				$paymentVoucher->setPosted($_REQUEST['Posted']);
				$paymentVoucher->setapproved(true);
				$post=true;
			}else
			{
				$paymentVoucher->setPosted(true);
				$paymentVoucher->setapproved(true);
			}

			$paymentVoucher->settransactionDate($_REQUEST['transactionDate']);
			$paymentVoucher->setcreationDate(date('Y-m-d'));
			$paymentVoucher->setsourceRef($_REQUEST['sourceRef']);

			$paymentVoucher->save();
			//payment voucher Item
			$paymentVoucherItem=new paymentVoucherItemService();
			$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
			$paymentVoucherItem->setparticular($_REQUEST['particular']);
			$paymentVoucherItem->setamount($_REQUEST['amount']);
			$chartOfAccount=new chartOfaccountService();
			$chartOfAccount->setAccountName($_REQUEST['accountCode']);
			$chartOfAccount->findAccountCode();

			$paymentVoucherItem->setaccountCode($chartOfAccount->getAccountCode());
			$paymentVoucherItem->setindividualCode($_REQUEST['individualNo']);

			//$trip=new TripService();
			//$trip->setTripNumber($_REQUEST['tripNo3']);
			//$trip->find();

			//$paymentVoucherItem->setcostCenter(null);
			//$paymentVoucherItem->settripNo($_REQUEST['tripNo3']);
			$paymentVoucherItem->save();

			//$paymentVoucherItem=new paymentVoucherItemService();
			//$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
			$totalAmount=intval($_REQUEST['amount']);
			if($totalAmount>0)
			{


				$ledgerHead = new ledgerHeadService();
				$ledgerHead->settransactionDate($_REQUEST['transactionDate']);
				$ledgerHead->setmonthId($_REQUEST['monthCode']);
				$ledgerHead->setamount($totalAmount);
				$ledgerHead->save();

				//Cr
				$generalLedger = new generalLedgerService();
				$generalLedger->setledgerHeadId($ledgerHead->getid());
				$generalLedger->setaccountCode($_REQUEST['sourceAccount']);
				$generalLedger->setnarrative('Payment voucher posting');
				$generalLedger->setreference($_REQUEST['sourceRef']);
				$generalLedger->setamount($totalAmount);
				$generalLedger->settransactionType('Cr');
				$generalLedger->setisActive(true);
				$generalLedger->save();


				//Dr
				$sql = "select * from  paymentvoucheritem  where payVoucherId='" .$paymentVoucherItem->getpayVoucherId() . "'";
				foreach ($paymentVoucherItem->view_query($sql) as $row) {
					$generalLedger->setaccountCode($row['accountCode']);
					$generalLedger->setnarrative($row['particular']);
					$generalLedger->setreference('PV-' . $row['payVoucherId']);
					$generalLedger->setamount($row['amount']);
					$generalLedger->settransactionType('Dr');
					if (!is_null($row['costCenter'])&&$row['costCenter']!='')
					{
						$generalLedger->setcostCentreId($row['costCenter']);
					}
					//$generalLedger->setcostCentreId();
					if (!is_null($row['tripNo'])&&$row['tripNo']!='')
					{
						$generalLedger->settripNo($row['tripNo']);
					}
					if (!is_null($row['individualCode'])&&$row['individualCode']!='')
					{
						$generalLedger->setindividualNo($row['individualCode']);
					}
					$generalLedger->setisActive(true);
					$generalLedger->save();

				}
			}
			//fuelOrder update
			$fuelOrder=new fuelOrderService();
			$fuelOrder->setid($_REQUEST['idFuelOrder']);
			$fuelOrder->find();
			$fuelOrder->setpaid(true);
			$fuelOrder->update();

			echo json_encode(array('success'=>true));


		}else if($controller_templet->getAction()=="paySupplier")
		{
			//payment Voucher
			$paymentVoucher=new paymentVoucherService();
			$paymentVoucher->setid($_REQUEST['idV']);

			$paymentVoucher->setsourceAccount($_REQUEST['sourceAccount']);
			$paymentVoucher->setpreparedBy($_SESSION['username']);
			//$paymentVoucher->setAuthorisedBy($_REQUEST['AuthorisedBy']);
			if(isset($_REQUEST['Posted']))
			{
				$paymentVoucher->setPosted($_REQUEST['Posted']);
				$paymentVoucher->setapproved(true);
				$post=true;
			}else
			{
				$paymentVoucher->setPosted(true);
				$paymentVoucher->setapproved(true);
			}

			$paymentVoucher->settransactionDate($_REQUEST['transactionDate']);
			$paymentVoucher->setcreationDate(date('Y-m-d'));
			$paymentVoucher->setsourceRef($_REQUEST['sourceRef']);

			$paymentVoucher->save();
			//payment voucher Item
			$paymentVoucherItem=new paymentVoucherItemService();
			$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
			$paymentVoucherItem->setparticular($_REQUEST['particular']);
			$paymentVoucherItem->setamount($_REQUEST['amount']);
			$chartOfAccount=new chartOfaccountService();
			$chartOfAccount->setAccountName($_REQUEST['accountCode']);
			$chartOfAccount->findAccountCode();

			$paymentVoucherItem->setaccountCode($chartOfAccount->getAccountCode());
			$paymentVoucherItem->setindividualCode($_REQUEST['individualNo']);

			//$trip=new TripService();
			//$trip->setTripNumber($_REQUEST['tripNo3']);
			//$trip->find();

			//$paymentVoucherItem->setcostCenter(null);
			//$paymentVoucherItem->settripNo($_REQUEST['tripNo3']);
			$paymentVoucherItem->save();

			//$paymentVoucherItem=new paymentVoucherItemService();
			//$paymentVoucherItem->setpayVoucherId($_REQUEST['idV']);
			$totalAmount=intval($_REQUEST['amount']);
			if($totalAmount>0)
			{


				$ledgerHead = new ledgerHeadService();
				$ledgerHead->settransactionDate($_REQUEST['transactionDate']);
				$ledgerHead->setmonthId($_REQUEST['monthCode']);
				$ledgerHead->setamount($totalAmount);
				$ledgerHead->save();

				//Cr
				$generalLedger = new generalLedgerService();
				$generalLedger->setledgerHeadId($ledgerHead->getid());
				$generalLedger->setaccountCode($_REQUEST['sourceAccount']);
				$generalLedger->setnarrative('Payment voucher posting');
				$generalLedger->setreference($_REQUEST['sourceRef']);
				$generalLedger->setindividualNo($_REQUEST['individualNo']);
				$generalLedger->setamount($totalAmount);
				$generalLedger->settransactionType('Cr');
				$generalLedger->setisActive(true);
				$generalLedger->save();


				//Dr
				$sql = "select * from  paymentvoucheritem  where payVoucherId='" .$paymentVoucherItem->getpayVoucherId() . "'";
				foreach ($paymentVoucherItem->view_query($sql) as $row) {
					$generalLedger->setaccountCode($row['accountCode']);
					$generalLedger->setnarrative($row['particular']);
					$generalLedger->setreference('PV-' . $row['payVoucherId']);
					$generalLedger->setamount($row['amount']);
					$generalLedger->settransactionType('Dr');
					if (!is_null($row['costCenter'])&&$row['costCenter']!='')
					{
						$generalLedger->setcostCentreId($row['costCenter']);
					}
					//$generalLedger->setcostCentreId();
					if (!is_null($row['tripNo'])&&$row['tripNo']!='')
					{
						$generalLedger->settripNo($row['tripNo']);
					}
					if (!is_null($row['individualCode'])&&$row['individualCode']!='')
					{
						//$generalLedger->setindividualNo($row['individualCode']);
					}
					$generalLedger->setisActive(true);
					$generalLedger->save();

				}
			}


			echo json_encode(array('success'=>true));

		}
 } ?>