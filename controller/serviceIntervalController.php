<?php 
 require_once('../services/serviceIntervalService.php');
 require_once('../services/vehicleService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $serviceInterval=new serviceIntervalService();
 		echo json_encode( $serviceInterval->view()); 
 }else if($controller_templet->getAction()=='viewForModel'){
	 	 $serviceInterval=new serviceIntervalService();
		 $vehicle=new VehicleService();
		 $vehicle->setid($_REQUEST['id']);
		 $vehicle->setModelNow();
		 $serviceInterval->setmodal($vehicle->getmodel());
 		echo json_encode($serviceInterval->viewModel()); 
 }else if($controller_templet->getAction()=='add'){
	 	 $serviceInterval=new serviceIntervalService();
 	 	 $serviceInterval->setintervalValue($_REQUEST['intervalValue']);
	 	 $serviceInterval->setserviceTypeId($_REQUEST['serviceTypeId']);
	 	 $serviceInterval->setmodal($_REQUEST['modal']);
	 	 $serviceInterval->setisActive(true);
	 	 $serviceInterval->setnextServiceType($_REQUEST['nextServiceType']);
	 	 echo json_encode( $serviceInterval->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $serviceInterval=new serviceIntervalService();
 	 	 $serviceInterval->setid($_REQUEST['id']);
	 	 $serviceInterval->setintervalValue($_REQUEST['intervalValue']);
	 	 $serviceInterval->setserviceTypeId($_REQUEST['serviceTypeId']);
	 	 $serviceInterval->setmodal($_REQUEST['modal']);
	 	 $serviceInterval->setisActive(true);
	 	 $serviceInterval->setnextServiceType($_REQUEST['nextServiceType']);
	 	 echo json_encode( $serviceInterval->update()); 
}
 else if($controller_templet->getAction()=='delete'){
 	 	 $serviceInterval=new serviceIntervalService();
 	 	 $serviceInterval->setid($_REQUEST['id']);
		echo json_encode( $serviceInterval->delete());
		 } 
		else if($controller_templet->getAction()=='getInterval')
		{
 	 	 $serviceInterval=new serviceIntervalService();
 	 	 $serviceInterval->setid($_REQUEST['id']);
		 $serviceInterval->setObject();
		echo  $serviceInterval->getintervalValue(); 
		} 

} ?>