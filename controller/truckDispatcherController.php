<?php

 ob_start();
// session_start();
require_once '../services/truckDispatcherService.php';
require_once '../services/orderSevice.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$truckDispatcher=new TruckDispatcherService();
		echo json_encode($truckDispatcher->view());
	}
	else if($controller_templet->getAction()=="add")
	{
		$truckDispatcher=new TruckDispatcherService();
		$truckDispatcher->settimeinDepo($_REQUEST['depotouttime']);
		$truckDispatcher->settimeoutDepo($_REQUEST['depotintime']);
		//$truckDispatcher->setdispatcher($_REQUEST['Dispatchercomment']);
		$truckDispatcher->setAllocationId($_REQUEST['eid']);
		//$allocation->setorderId($_REQUEST['id']);
		if(intval($_REQUEST['did'])!=0 && $_REQUEST['did']==NULL)
		{
			if($_REQUEST['depotouttime']!=NULL && $_REQUEST['depotouttime']!="" )
			{
				$order=new OrderService();
				$order->setid($_REQUEST['id']);
				$order->dispatcherOrder();
			}
			$truckDispatcher->setid($_REQUEST['did']);
			echo json_encode($allocation->update());
		}else{
			if($_REQUEST['depotouttime']!=NULL && $_REQUEST['depotouttime']!="" )
			{
				$order=new OrderService();
				$order->setid($_REQUEST['id']);
				$order->dispatcherOrder();
			}
		echo json_encode($allocation->save());
			
		}
		
		
	}else if($controller_templet->getAction()=="update")
	{
		$truckDispatcher=new TruckDispatcherService();
		$truckDispatcher->settimeinDepo($_REQUEST['depotintime']);
		$truckDispatcher->settimeoutDepo($_REQUEST['depotouttime']);
		//$truckDispatcher->setdispatcher($_REQUEST['Dispatchercomment']);
		$truckDispatcher->setAllocationId($_REQUEST['eid']);
		//$allocation->setorderId($_REQUEST['id']);
		if(intval($_REQUEST['did'])!=0 && $_REQUEST['did']!=NULL)
		{
			if($_REQUEST['depotouttime']!=NULL && $_REQUEST['depotouttime']!="" )
			{
				$order=new OrderService();
				$order->setid($_REQUEST['id']);
				$order->dispatcherOrder();
			}
			$truckDispatcher->setid($_REQUEST['did']);
			echo json_encode($truckDispatcher->update());
		}else{
			if($_REQUEST['depotouttime']!=NULL && $_REQUEST['depotouttime']!="" )
			{
				$order=new OrderService();
				$order->setid($_REQUEST['id']);
				$order->dispatcherOrder();
			}
			echo json_encode($truckDispatcher->save());
		}	
	}else if($controller_templet->getAction()=="viewUtilisation")
	{
		$capacityService=new capacityService();
		if(isset($_REQUEST['subAction'])){
			$subAccount=$_REQUEST['subAction'];
			if($subAccount=="rangeDate"){
			echo json_encode($capacityService->viewUtilisation2($_REQUEST['startDate'],$_REQUEST['endDate']));
			}else if($subAccount=="vehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation3(intval($_REQUEST['vehicleId'])));
			}else if($subAccount=="rangeVehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation4($_REQUEST['vehicleId'],$_REQUEST['startDate'],$_REQUEST['endDate']));
			}
		}else{
			echo json_encode($capacityService->viewUtilisation());
		}
	}else if($controller_templet->getAction()=="graphData")
	{
		$capacity=new capacityService();
		echo $capacity->viewUtilisation5($_REQUEST['vehicleId']);
		
	}
	else if($controller_templet->getAction()=="view_ordertypeid")
	{
		$order =new OrderService();
		$sql="select * from tblordertype where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_orderStatus")
	{
		$order =new OrderService();
		$sql="select * from tblorderstatus where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_itemCome")
	{
		$order =new OrderService();
		$sql="select * from tblgood where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="view_statusOrder")
	{
		$order =new OrderService();
		$sql="select * from tblstatusorder where isActive='1'";
		echo json_encode($order->view_query($sql));
		
	}
	else if($controller_templet->getAction()=="delete")
	{
		$order=new OrderService();
		$order->setid($_REQUEST['id']);
		echo json_encode($order->delete());
	}
	else if($controller_templet->getAction()=="excelExport")
	{
		$data=array();
		$order=new OrderService();
		$order->setorderDate($_REQUEST['date']);
		$sql="SELECT o.id,o.mailingName,o.orderDate,o.promiseDate,o.orderNumber,o.qtyOrder,ot.name ordertypeId,so.name statusorderId,os.name orderstatusId, g.name itemcodeId FROM tblorder o inner join tblordertype ot on o.ordertypeId=ot.id inner join tblstatusorder so on o.statusorderId=so.id inner join tblorderstatus os on o.orderstatusId=os.id inner join tblgood g  on o.itemcodeId=g.id where o.orderDate='".$order->getorderDate()."'";
		foreach($order->view_query($sql) as $row)
		{
			$data2=array();
		   	$data2["#"]=$row["id"];
		   	$data2["MailingName"]=$row["mailingName"];
		   	$data2["OrderDate"]=$row["orderDate"];
		   	$data2["PromiseDate"]=$row["promiseDate"];
		   	$data2["OrderNumber"]=$row["orderNumber"];
		   	$data2["Ordertype"]=$row["ordertypeId"];
		   	$data2["QtyOrder"]=$row["qtyOrder"];
		   	$data2["Statusorder"]=$row["statusorderId"];
		   	$data2["Qrderstatus"]=$row["orderstatusId"];
		   	$data2["Itemcode"]=$row["itemcodeId"];
		   array_push($data,$data2);	
		}
		echo json_encode($data);
	}
}
?>