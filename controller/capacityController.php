<?php

 ob_start();
// session_start();
require_once '../services/capacityService.php';
require_once '../services/tripService.php';
require_once '../services/vehicleService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$capacity=new capacityService();
		if(isset($_REQUEST['id'])){
			$capacityService=new capacityService();
		$capacityService->setvehicleId($_REQUEST['id']);
		echo json_encode($capacityService->viewSearch());
		}else{
		echo json_encode($capacity->view());
		}
	}
	else if($controller_templet->getAction()=="add")
	{
		
		$capacityService=new capacityService();
		$capacityService->setvehicleId($_REQUEST['regNo']);
		$trip=new tripService();
		$trip->settripNo($_REQUEST['tripNo']);
		$trip->setTrip();
		$capacityService->settripId($trip->getid());
		$capacityService->setvpower($_REQUEST['vpower']);
		$capacityService->setago($_REQUEST['ago']);
		$capacityService->setbik($_REQUEST['bik']);
		$capacityService->setugl($_REQUEST['ugl']);
		$capacityService->setcreationDate($_REQUEST['creationDate']);
		$capacityService->settimeLoading($_REQUEST['timeLoading']);
		$capacityService->save();
		
		echo json_encode(array('success'=>true));
		
	}else if($controller_templet->getAction()=="edit")
	{
		$capacityService=new capacityService();
		$capacityService->setvehicleId($_REQUEST['regNo']);
		$trip=new tripService();
		$trip->settripNo($_REQUEST['tripNo']);
		$trip->setTrip();
		$capacityService->settripId($trip->getid());
		$capacityService->setvpower($_REQUEST['vpower']);
		$capacityService->setago($_REQUEST['ago']);
		$capacityService->setbik($_REQUEST['bik']);
		$capacityService->setugl($_REQUEST['ugl']);
		$capacityService->setcreationDate($_REQUEST['creationDate']);
		$capacityService->settimeLoading($_REQUEST['timeLoading']);
		$capacityService->setid($_REQUEST['id']);
		echo json_encode($capacityService->update());
	}else if($controller_templet->getAction()=="viewUtilisation")
	{
		$capacityService=new capacityService();
		if(isset($_REQUEST['subAction'])){
			$subAccount=$_REQUEST['subAction'];
			if($subAccount=="rangeDate"){
			echo json_encode($capacityService->viewUtilisation2($_REQUEST['startDate'],$_REQUEST['endDate']));
			}else if($subAccount=="vehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation3(intval($_REQUEST['vehicleId'])));
			}else if($subAccount=="rangeVehicleSearch")
			{
				echo json_encode($capacityService->viewUtilisation4($_REQUEST['vehicleId'],$_REQUEST['startDate'],$_REQUEST['endDate']));
			}
		}else{
			echo json_encode($capacityService->viewUtilisation());
		}
	}else if($controller_templet->getAction()=="graphData")
	{
		$capacity=new capacityService();
		echo $capacity->vehicleGraphy();
		
	}else if($controller_templet->getAction()=="graphDataId")
	{
		$capacity=new capacityService();
		echo $capacity->vehicleGraphyId($_REQUEST['id']);
		
	}else if($controller_templet->getAction()=="loadUtilisation")
	{
		$capacity=new capacityService();
		$capacity->vehicleUtilisation();
	}else if($controller_templet->getAction()=="loadUtilisationId")
	{
		$capacity=new capacityService();
		$capacity->vehicleUtilisationId($_REQUEST['id']);
	}else if($controller_templet->getAction()=="loadUtilisationIdRange")
	{
		$capacity=new capacityService();
		$capacity->vehicleUtilisationIdRange($_REQUEST['id'],$_REQUEST['startDate'],$_REQUEST['endDate']);
	}else if($controller_templet->getAction()=="loadUtilisationRange")
	{
		$capacity=new capacityService();
		$capacity->vehicleUtilisationRange($_REQUEST['startDate'],$_REQUEST['endDate']);
	}else if($controller_templet->getAction()=="timeUtilisation")
	{
		$capacity=new capacityService();
		$capacity->vehicleTimeUtilisation();
		
		
	}

}
?>