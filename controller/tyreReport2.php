<!--<p style="font-family:Calibri;">-->
<?php
require_once '../services/tyreService.php';
//require_once '../services/vehicleService.php';
$tyre=new TyreService();

	
		
$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 12pt;">FRED SEBYALA TRANSPORTERS LIMITED</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 10pt;">TYRE MANAGEMENT REPORT</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td>Truck</td>
<td>Position</td>
<td>Brand</td>
<td>Serial</td>
<td>Serial</td>
<td>Fixed As</td>
<td>Fitting Date</td>
<td>Fitting Odometer</td>
<td>Expected Km</td>
<td>Current Mileage</td>
<td>Removal Mileage</td>
<td>Remaining Mileage</td>
<td>Comment</td>

</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
';

//<tr>
//<td align="center">MF1234567</td>
//<td align="center">10</td>
//<td>Large pack Hoover bags</td>
//<td class="cost">&pound;2.56</td>
//</tr>
$html2="";
$sql="select ty.Id,ty.stockId,(select sk.treadDepth from tblstock sk where sk.id=ty.stockId) treadDepth,v.regNo, tt.tyretypeName position, ty.brand,ty.serialNumber, ty.cost , ty.fixedAs,ty.fixingDate, ty.Odometer,ty.expectedKm, 	od.reading currentReading,(ty.removalMileage-od.reading) remaining ,round((ty.cost/ty.expectedKm),2) costperkm,ty.removalMileage,ty.comment,ty.numberPlate,ty.tyrePosition tyreDetails from tbl_tyre ty inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId  where ty.serialNumber is null order by ty.numberPlate,tt.id asc ";

		
				//$nox=$checked->loadStatus($sqlChecked);
				foreach($tyre->view_query($sql) as $row2)
				{
					
				
					$html2.= "<tr><td class='totals'>".$row2["regNo"]."</td><td class='totals'>".$row2['position']."</td><td class='totals'>".$row2['brand']."</td><td class='totals'>".$row2['serialNumber']."</td><td class='totals'>".$row2['cost']."</td><td class='totals'>".$row2['fixedAs']."</td><td class='totals'>".$row2['fixingDate']."</td><td class='totals'>".$row2['Odometer']."</td><td class='totals'>".$row2['expectedKm']."</td><td class='totals'>".$row2['currentReading']."</td><td class='totals'>".$row2['removalMileage']."</td><td class='totals'>".$row2['remaining']."</td><td class='totals'>".$row2['comment']."</td></tr>";
					
				
			}
			//echo"</tbody></table>";
		


$html.=$html2.'<!-- END ITEMS HERE -->
</tbody>
</table>

<div style="text-align: center; font-style: italic;">Fred sebyala transporter</div>


</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");

$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("Tyre Report");
$mpdf->SetAuthor("code360 data solution");
$mpdf->SetWatermarkText("FST");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');



$mpdf->WriteHTML($html);


$mpdf->Output(); exit;

exit;

?>