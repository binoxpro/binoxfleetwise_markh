<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/trainingComplianceService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="Next>>")
	{
		$trainingCompliance=new TrainingComplianceService();
		$trainingCompliance->settrainingId($_REQUEST['wid']);
		if(isset($_REQUEST['issueDate'])){
		$trainingCompliance->setIssueDate($_REQUEST['issueDate']);
		}
		if(isset($_REQUEST['expiryDate']))
		{
		$trainingCompliance->setexpireDate($_REQUEST['expiryDate']);
		}
		if(isset($_REQUEST['status']))
		{
			$trainingCompliance->setStatus($_REQUEST['status']);
		}else
		{
			
			$trainingCompliance->setStatus('Done');
		}
		$trainingCompliance->setdriverId($_REQUEST['idDriver']);
		$trainingCompliance->setisActive(true);
		$msgarray=$trainingCompliance->save();
		if(array_key_exists('success',$msgarray))
		{
			$view=$trainingCompliance->getWid();
			header('location:../admin.php?view='.$trainingCompliance->getWidCourse($view).'&did='.$trainingCompliance->getdriverId().'&wid='.$trainingCompliance->getWid());
		}else
		{
			//header('location:../admin.php?view=licence_Management&did='.$licence->getid()."&msg=".array_keys($msgarray,'msg'));
			echo json_encode($msgarray);
		}
		
	}else  if($controller_templet->getAction()=="view")
	{
		$training=new TrainingComplianceService();
		echo json_encode($training->view());
		
	}else if($controller_templet->getAction()=="viewTable")
	{
		$training=new TrainingComplianceService();
		echo "<table class='table table-striped table-bordered table-condensed'><tr><td>Driver's Name</td><td>DOB</td><td>Telephone</td><td>ID No</td><td>Licence No</td><td>Iusse Date</td><td>Expiry Date</td><td>Status</td><td>Next DDC</td><td>Status</td><td>Driver Assement</td><td>Fitness And Medical Examination</td><td>Status</td><td>Fire Fighting Training</td><td>Status</td><td>First Aid Training</td><td>Status</td><td>Road Transport Safety</td><td>Status</td><td>Induction</td><td>Edit</td><td>Delete</td></tr>";
		foreach($training->view() as $row){
			//work on the licence Status
			$today=date('y-m-d');
			$licenceStatus=strtotime($row["expiryDate"])<strtotime($today)?'Invalid':'Valid';
			$ddc=$row['statusDDC']=="Invalid"?"<span style='color:red'>".$row['statusDDC']."</span>":"<span style='color:green'>".$row['statusDDC']."</span>";
			$sopaf=$row['statusDriver_Assement']=="Invalid"?"<span style='color:red'>".$row['statusDriver_Assement']."</span>":"<span style='color:green'>".$row['statusDriver_Assement']."</span>";
			$sme=$row['statusFitness_and_Medical_Examination']=="Invalid"?"<span style='color:red'>".$row['statusFitness_and_Medical_Examination']."</span>":"<span style='color:green'>".$row['statusFitness_and_Medical_Examination']."</span>";
			//$sfft=$row["statusFire_Fighting_Training"]=="Invalid"?"<span style='color:red'>".$row["statusFire_Fighting_Training"]."</span>":"<span style='color:green'>".$row["statusFire_Fighting_Training"]."</span>";
			$sfa=$row["statusFirst_Aid"]=="Invalid"?"<span style='color:red'>".$row["statusFirst_Aid"]."</span>":"<span style='color:green'>".$row["statusFirst_Aid"]."</span>";
			$sjmt=$row['statusRoad_Transport_Safety']=="Invalid"?"<span style='color:red'>".$row['statusRoad_Transport_Safety']."</span>":"<span style='color:green'>".$row['statusRoad_Transport_Safety']."</span>";
			$si=$row["statusInduction"]=="Invalid"?"<span style='color:red'>".$row["statusInduction"]."</span>":"<span style='color:green'>".$row["statusInduction"]."</span>";
			//$ser=$row["statusEmergency_Response"]=="Invalid"?"<span style='color:red'>".$row["statusEmergency_Response"]."</span>":"<span style='color:green'>".$row["statusEmergency_Response"]."</span>";
			//$sdgh=$row["statusDangerous_Goods_Handling"]=="Invalid"?"<span style='color:red'>".$row["statusDangerous_Goods_Handling"]."</span>":"<span style='color:green'>".$row["statusDangerous_Goods_Handling"]."</span>";
			
			echo "<tr><td>".$row['firstName']."&nbsp;".$row['lastName']."</td><td>".$row['dob']."</td><td>".$row["contact"]."</td><td>".$row["DriverId"]."</td><td>".$row["licenceNo"]."</td><td>".$row["issueDate"]."</td><td>".$row["expiryDate"]."</td><td>".$licenceStatus."</td><td>".$row["DDC"]."</td><td>".$ddc."</td><td>".$sopaf."</td><td>".$row["Fitness_and_Medical_Examination"]."</td><td>".$sme."</td><td></td><td></td><td>".$row["First_Aid"]."</td><td>".$sfa."</td><td>".$row["Road_Transport_Safety"]."</td><td>".$sjmt."</td><td>".$si."</td><td><a href='?view=Driver_Registration&did=".$row["DriverId"]."'>Edit</a></a></td><td>Not supportted</td></tr>";
		}
		echo "</table>";
		
	}else  if($controller_templet->getAction()=="viewJson"){
		$training=new TrainingComplianceService();
		echo json_encode($training->viewJson());
		
	}else  if($controller_templet->getAction()=="viewTraining"){
		$training=new TrainingComplianceService();
		$sql2="select * from tbltrainingcourse where isActive='1'";
		echo json_encode($training->view_query($sql2));
	}else if($controller_templet->getAction()=="add")
	{
		$trainingCompliance=new TrainingComplianceService();
		$trainingCompliance->settrainingId($_REQUEST['trainingId']);
		$trainingCompliance->setIssueDate($_REQUEST['issueDate']);
		$trainingCompliance->setexpireDate($_REQUEST['expiryDate']);
		$trainingCompliance->setdriverId($_REQUEST['driverId']);
		$trainingCompliance->setisActive(true);
		echo json_encode($trainingCompliance->save());
		
	}else if($controller_templet->getAction()=="update")
	{
		$trainingCompliance=new TrainingComplianceService();
		$trainingCompliance->setid($_REQUEST['id']);
		$trainingCompliance->settrainingId($_REQUEST['trainingId']);
		$trainingCompliance->setIssueDate($_REQUEST['issueDate']);
		$trainingCompliance->setexpireDate($_REQUEST['expiryDate']);
		$trainingCompliance->setdriverId($_REQUEST['driverId']);
		$trainingCompliance->setisActive(true);
		echo json_encode($trainingCompliance->update());
		
	}
    
}
?>