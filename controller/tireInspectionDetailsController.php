<?php 
 require_once('../services/tireInspectionDetailsService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view')
 {
	 	 $tireInspectionDetails=new tireInspectionDetailsService();

		 echo json_encode($tireInspectionDetails->view());

  }else if($controller_templet->getAction()=='add'){
	 	 $tireInspectionDetails=new tireInspectionDetailsService();
         $thid=$_REQUEST['thid'];
         $td=$_REQUEST['td'];
         $psi=$_REQUEST['psi'];
         $sd=$_REQUEST['sd'];
         $td2=$_REQUEST['td2'];
         $md=$_REQUEST['md'];
         $mid=$_REQUEST['mid'];
         $vc=$_REQUEST['vc'];
         $rc=$_REQUEST['rc'];
         $remarks=$_REQUEST['remarks'];
         for($i=0;$i<sizeof($thid);$i++)
         {
     	 	 $tireInspectionDetails->settireHoldId($thid[$i]);
    	 	 $tireInspectionDetails->settreadDepth($td[$i]);
    	 	 $tireInspectionDetails->setpressure($psi[$i]);
    	 	 $tireInspectionDetails->setsidewall($sd[$i]);
    	 	 $tireInspectionDetails->settread($td2[$i]);
    	 	 $tireInspectionDetails->setmech($md[$i]);
    	 	 $tireInspectionDetails->setmismatchedDual($mid[$i]);
    	 	 $tireInspectionDetails->setvalveCondition($vc[$i]);
    	 	 $tireInspectionDetails->setrimCondition($rc[$i]);
    	 	 $tireInspectionDetails->setinspectionHeaderId($_REQUEST['inspectionId']);
    	 	 $tireInspectionDetails->setgeneralRemarks($remarks[$i]);
            $tireInspectionDetails->save();
         }
         header('location:../admin.php');
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $tireInspectionDetails=new tireInspectionDetailsService();
 	 	 $tireInspectionDetails->setid($_REQUEST['id']);
	 	 $tireInspectionDetails->settireHoldId($_REQUEST['tireHoldId']);
	 	 $tireInspectionDetails->settreadDepth($_REQUEST['treadDepth']);
	 	 $tireInspectionDetails->setpressure($_REQUEST['pressure']);
	 	 $tireInspectionDetails->setsidewall($_REQUEST['sidewall']);
	 	 $tireInspectionDetails->settread($_REQUEST['tread']);
	 	 $tireInspectionDetails->setmech($_REQUEST['mech']);
	 	 $tireInspectionDetails->setmismatchedDual($_REQUEST['mismatchedDual']);
	 	 $tireInspectionDetails->setvalveCondition($_REQUEST['valveCondition']);
	 	 $tireInspectionDetails->setrimCondition($_REQUEST['rimCondition']);
	 	 $tireInspectionDetails->setinspectionHeaderId($_REQUEST['inspectionHeaderId']);
	 	 $tireInspectionDetails->setgeneralRemarks($_REQUEST['generalRemarks']);
	 	 echo json_encode( $tireInspectionDetails->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $tireInspectionDetails=new tireInspectionDetailsService();
 	 	 $tireInspectionDetails->setid($_REQUEST['id']);
echo json_encode( $tireInspectionDetails->delete()); } 
} ?>