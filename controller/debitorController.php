<?php 
 require_once('../services/debitorService.php');
require_once('../services/individualAccountService.php');
require_once('utility.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $debitor=new debitorService();
 		 echo json_encode( $debitor->view());
 }	 else if($controller_templet->getAction()=='add'){
		 $individual=new individualAccountService();
		 $code='Debitor-'.utility::returnThreeFiguareString($individual->Count());
	 	 $debitor=new debitorService();
		 $debitor->setid($code);
 	 	 $debitor->setcontactPerson(ucfirst($_REQUEST['contactPerson']));
	 	 $debitor->setcompanyName(ucfirst($_REQUEST['companyName']));
	 	 $debitor->setbuilding($_REQUEST['building']);
	 	 $debitor->settown($_REQUEST['town']);
	 	 $debitor->setdistrict($_REQUEST['district']);
	 	 $debitor->setcountry($_REQUEST['country']);
	 	 $debitor->settel($_REQUEST['tel']);
	 	 $debitor->setemail($_REQUEST['email']);
	 	 $debitor->setcreditPeriod($_REQUEST['creditPeriod']);
		 if(isset($_REQUEST['isActive']))
		 {
			 $debitor->setisActive(true);
		 }else{
			 $debitor->setisActive(false);
		 }
	 	 $debitor->setisActive($_REQUEST['isActive']);
	 	$debitor->save();

		 $individual->setId($debitor->getid());
		 $individual->setAccountName($debitor->getcompanyName());
		 $individual->setIsActive(true);
		 echo json_encode($individual->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $debitor=new debitorService();
 	 	 $debitor->setid($_REQUEST['id']);
	 	 $debitor->setcontactPerson($_REQUEST['contactPerson']);
	 	 $debitor->setcompanyName($_REQUEST['companyName']);
	 	 $debitor->setbuilding($_REQUEST['building']);
	 	 $debitor->settown($_REQUEST['town']);
	 	 $debitor->setdistrict($_REQUEST['district']);
	 	 $debitor->setcountry($_REQUEST['country']);
	 	 $debitor->settel($_REQUEST['tel']);
	 	 $debitor->setemail($_REQUEST['email']);
	 	 $debitor->setcreditPeriod($_REQUEST['creditPeriod']);
		 if(isset($_REQUEST['isActive']))
		 {
			 $debitor->setisActive(true);
		 }else{
			 $debitor->setisActive(false);
		 }
	 	 $debitor->update();
	 	 $individual=new individualAccountService();
		 $individual->setId($debitor->getid());
		 $individual->setAccountName($debitor->getcompanyName());
		 $individual->setIsActive(true);
		 echo json_encode($individual->update());


 }
 else if($controller_templet->getAction()=='delete'){
 	 	 $debitor=new debitorService();
 	 	 $debitor->setid($_REQUEST['id']);
		echo json_encode( $debitor->delete());
}else if($controller_templet->getAction()=='viewCombo'){  
		 $debitor=new debitorService();
 		 echo json_encode( $debitor->viewConbox());
		}  else if($controller_templet->getAction()=='getDebitorDetail'){
	 		$debitor=new debitorService();
	 		$debitor->setid($_REQUEST['id']);
	 		$debitor->find();
	 		echo $debitor->formatAddress();



 }
 } ?>