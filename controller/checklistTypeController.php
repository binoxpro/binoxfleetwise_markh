<?php
 ob_start();
// session_start();
require_once '../services/checklistTypeService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$checkListType=new ChecklistTypeService();
		echo json_encode($checkListType->view());
	}else if($controller_templet->getAction()=="add")
	{
		$checkListType=new ChecklistTypeService();
		$checkListType->setType($_REQUEST['type']);
		echo json_encode($checkListType->save());
	}else if($controller_templet->getAction()=="edit")
	{
		$checkListType=new ChecklistTypeService();
		$checkListType->setid($_REQUEST['id']);
		$checkListType->setType($_REQUEST['type']);	
		echo json_encode($checkListType->update());
		
	}else if($controller_templet->getAction()=="loadCheck")
	{
		$checkListType=new ChecklistTypeService();
		$checkListType->setType($_REQUEST['id']);
		$sql="select * from tblsection where checkTypeId='".$checkListType->getType()."' and isActive='ON'";
		foreach ($checkListType->view_query($sql) as $row)
		{
			$sectionId=$row['id'];
			echo "<table class='table table-striped table-bordered'>";
			echo "<thead><tr><th colspan='3'>".$row['name']."</th></tr></thead><tbody>";
			$sql2="select * from tblsectionitem where sectionId='".$sectionId."'";
			echo "<tr><td>Item</td><td>Item Description</td><td>Comment</td></tr>";
			foreach($checkListType->view_query($sql2) as $row2)
			{
				echo "<tr><td>".$row2['itemName']."</td><td>".$row2['itemDescription']."</td><td>".$row2['comment']."</td></tr>";	
			}
			echo"</tbody></table>";
		}
	}else if($controller_templet->getAction()=="getType")
	{
		$checkListType=new ChecklistTypeService();
		$checkListType->setType($_REQUEST['id']);
		$sql="select * from tblchecklisttype where id='".$checkListType->getType()."'";
		foreach ($checkListType->view_query($sql) as $row)
		{
			echo $row['type'];
		}
	}
}
?>