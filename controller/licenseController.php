<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/licenseService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
		$license=new LicenseService();
		$license->setNumberPlate($_REQUEST['numberPlate1']);
		$license->setthirdPartyDOI($_REQUEST['tpDoi']);
		$license->setthirdPartyDOEg($_REQUEST['tpDoe']);
		if($_REQUEST['cDoi']!="") {
			$license->setcaliberationDOI($_REQUEST['cDoi']);
			$license->setcaliberationDOE($_REQUEST['cDoe']);
		}
		//$license->sethsse($_REQUEST['hsse']);
		if($_REQUEST['fDoi']!="") {
			$license->setFDoi($_REQUEST['fDoi']);
			$license->setFDoe($_REQUEST['fDoe']);
		}
		if($_REQUEST['pDoi']!="") {
			$license->setPDoi($_REQUEST['pDoi']);
			$license->setPDoe($_REQUEST['pDoe']);
		}
        $license->setIsActive(true);
		echo json_encode($license->save());
		
	}else  if($controller_templet->getAction()=="view"){
		if(isset($_REQUEST['numberPlate'])){
			$license=new LicenseService();
			$sql="SELECT lic.*,v.regNo,lic.numberPlate numberPlatel FROM tbl_license lic inner join tblvehicle v on lic.numberPlate=v.id where lic.numberPlate='".$_REQUEST['numberPlate']."' and isActive='1'";
			echo json_encode($license->view_query($sql));
		}else{
			$license=new LicenseService();
			echo json_encode($license->view());
		}
		
	}else if($controller_templet->getAction()=="update"){
		$license=new LicenseService();
		$license->setNumberPlate($_REQUEST['numberPlate1']);
		$license->setthirdPartyDOI($_REQUEST['tpDoi']);
		$license->setthirdPartyDOEg($_REQUEST['tpDoe']);
		$license->setcaliberationDOI($_REQUEST['cDoi']);
		$license->setcaliberationDOE($_REQUEST['cDoe']);
		//$license->sethsse($_REQUEST['hsse']);
		$license->setFDoi($_REQUEST['fDoi']);
		$license->setFDoe($_REQUEST['fDoe']);
		$license->setPDoi($_REQUEST['pDoi']);
		$license->setPDoe($_REQUEST['pDoe']);
		$license->setid($_REQUEST['id']);
		echo json_encode($license->update());
	}else if($controller_templet->getAction()=="renewal"){
		$license=new LicenseService();
        $license->setid($_REQUEST['id']);
        $license->setIsActive(false);
        $license->updateIsActive();
		$license->setNumberPlate($_REQUEST['numberPlate1']);
		$license->setthirdPartyDOI($_REQUEST['tpDoi']);
		$license->setthirdPartyDOEg($_REQUEST['tpDoe']);
		$license->setcaliberationDOI($_REQUEST['cDoi']);
		$license->setcaliberationDOE($_REQUEST['cDoe']);
		//$license->sethsse($_REQUEST['hsse']);
		$license->setFDoi($_REQUEST['fDoi']);
		$license->setFDoe($_REQUEST['fDoe']);
		$license->setPDoi($_REQUEST['pDoi']);
		$license->setPDoe($_REQUEST['pDoe']);
        $license->setIsActive(true);
		//$license->setid($_REQUEST['id']);
		echo json_encode($license->save());
	}else if($controller_templet->getAction()=="delete"){
		$license=new LicenseService();
		$license->setid($_REQUEST['id']);
		echo json_encode($license->delete());
	}
    
}
?>