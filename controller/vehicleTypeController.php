<?php

 ob_start();
// session_start();
require_once '../services/vehicleTypeService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="view")
	{
		$vehicleType=new VehicleTypeService();
		echo json_encode($vehicleType->view());
	}else if($controller_templet->getAction()=="add")
	{
		$vehicleType=new VehicleTypeService();
		$vehicleType->setvehicleType($_REQUEST['vehicleType']);
		echo json_encode($vehicleType->save());
	}
}
?>