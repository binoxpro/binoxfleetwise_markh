<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/serviceTrackService.php';
require_once '../services/jobCardService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="addServiceMaintainance"){
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['numberPlate']);
		$serviceTrack->setKmReading($_REQUEST['km_reading']);
		$serviceTrack->setNextReading($_REQUEST['next_reading']);
		$serviceTrack->setRegDate($_REQUEST['reg_date']);
		$serviceTrack->setInterval($_REQUEST['IntervalPeriod']);
		$serviceTrack->setReminder($_REQUEST['Reminder']);
		$serviceTrack->setService($_REQUEST['Service']);
		$serviceTrack->setTypeOfservice($_REQUEST['TypeOfService']);
		
		$serviceTrack->save();
		//$serviceId=$serviceTrack->loadId();
		//generate jobcard 
		if($serviceTrack->getServiceID()!=NULL || $serviceTrack->getServiceID()!=""){
		$jobcardService=new jobCardService();
		$jobcardService->setjobNo($jobcardService->jobNoGeneration());
		$jobcardService->setdate($_REQUEST['reg_date']);
		$jobcardService->setvehicleId($_REQUEST['numberPlate']);
		$jobcardService->setodometer($_REQUEST['km_reading']);
		$jobcardService->settime(date('h:i'));
		$jobcardService->setComment("Servicing of the vehicle is due");
		$jobcardService->save();
		
		 echo json_encode($jobcardService->saveServiceJobCard($serviceTrack->getServiceID(),$jobcardService->getid()));
		 
		}else{
		echo json_encode(array('msg'=>'Information is aready saved please change the km Reading'));	
		}
		
	}else  if($controller_templet->getAction()=="view"){
		$serviceTrack=new ServiceTrackService();
		if(isset($_REQUEST['id'])){
		$serviceTrack->setNumberPlate($_REQUEST['id']);
		echo json_encode($serviceTrack->view());
		}
		
	}else if($controller_templet->getAction()=="update"){
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['numberPlate']);
		$serviceTrack->setKmReading($_REQUEST['km_reading']);
		$serviceTrack->setNextReading($_REQUEST['next_reading']);
		$serviceTrack->setRegDate($_REQUEST['reg_date']);
		$serviceTrack->setServiceID($_REQUEST['id']);
		echo json_encode($serviceTrack->update());
	}else if ($controller_templet->getAction()=="addServiceMaintainancex")
	{
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['numberPlate']);
		$serviceTrack->setPreviousDate($_REQUEST['PreviousDate']);
		$serviceTrack->setNextDate($_REQUEST['NextDate']);
		$serviceTrack->setInterval($_REQUEST['IntervalPeriod']);
		$serviceTrack->setReminder($_REQUEST['Reminder']);
		$serviceTrack->setService($_REQUEST['Service']);
		$serviceTrack->setTypeOfservice($_REQUEST['TypeOfService']);
		echo json_encode($serviceTrack->saveMaintanance());
		
	}else if($controller_templet->getAction()=="manage")
	{
		if(isset($_REQUEST['numberPlate'])){
		$serviceTrack=new ServiceTrackService();
		$truck=$_REQUEST['numberPlate'];
		$sql="select *,v.regNo,(SELECT reading from tblodometer where vehicleId=tbl_service.numberPlate) currentReading from tbl_service inner join tblvehicle v on tbl_service.numberPlate= v.id where numberPlate='$truck'";
		echo json_encode($serviceTrack->view_query($sql));	
		}else{
		$serviceTrack=new ServiceTrackService();
		
		$sql="select tbl_service.*,v.regNo,(SELECT reading from tblodometer where vehicleId=tbl_service.numberPlate) currentReading from tbl_service inner join tblvehicle v on tbl_service.numberPlate= v.id";
		echo json_encode($serviceTrack->view_query($sql));
		}
	}else if($controller_templet->getAction()=="excelReport")
	{
		if(isset($_REQUEST['numberPlate'])){
		$serviceTrack=new ServiceTrackService();
		$sql="select v.regNo vehicle,km_reading PerviousServiceKm,PreviousDate NxtServiceDueDate,IntervalPeriod ServiceInterval,next_reading ReminderPeriodInKm,service Minor_Major,TypeOfService,(SELECT reading from tblodometer where vehicleId=tbl_service.numberPlate) currentReading from tbl_service inner join tblvehicle v on tbl_service.numberPlate= v.id where numberPlate='".$_REQUEST['numberPlate']."'";
		echo json_encode($serviceTrack->view_query($sql));	
		}else{
		$serviceTrack=new ServiceTrackService();
		
		$sql="select v.regNo vehicle,km_reading PerviousServiceKm,PreviousDate NxtServiceDueDate,IntervalPeriod ServiceInterval,next_reading ReminderPeriodInKm,service Minor_Major,TypeOfService,(SELECT reading from tblodometer where vehicleId=tbl_service.numberPlate) currentReading from tbl_service inner join tblvehicle v on tbl_service.numberPlate= v.id";
		echo json_encode($serviceTrack->view_query($sql));
		}
	}else if ($controller_templet->getAction()=="getPreviousReading")
	{
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['id']);
		$sql="select nextDate from tbl_service_status where numberPlate='".$serviceTrack->getNumberPlate()."'";
		$serviceTrack->setPreviousDate(NULL);
		foreach($serviceTrack->view_query($sql) as $row){
			$serviceTrack->setPreviousDate($row['nextDate']);
		}
		if($serviceTrack->getPreviousDate()==NULL)
		{
			$serviceTrack->setPreviousDate(0);
		}
		echo $serviceTrack->getPreviousDate();
		
	}
	else if ($controller_templet->getAction()=="getNextReading")
	{
		$nextReading=$_REQUEST['nextReading'];
		$newTime=strtotime($nextReading);
		
	}else if ($controller_templet->getAction()=="updateServiceMaintance")
	{
		$serviceTrack=new ServiceTrackService();
		$serviceTrack->setNumberPlate($_REQUEST['numberPlate']);
		$serviceTrack->setPreviousDate($_REQUEST['PreviousDate']);
		$serviceTrack->setNextDate($_REQUEST['NextDate']);
		$serviceTrack->setInterval($_REQUEST['IntervalPeriod']);
		$serviceTrack->setReminder($_REQUEST['Reminder']);
		$serviceTrack->setService($_REQUEST['Service']);
		$serviceTrack->setTypeOfservice($_REQUEST['TypeOfService']);
		$serviceTrack->setServiceID($_REQUEST['id']);
		echo json_encode($serviceTrack->updateMaintanance());
		
	}else if($controller_templet->getAction()=="serviceDashboard")
	{
		
		
		$send=false;
		$msg='<html><head><link href="http://code360ds.com/fst100/css/bootstrap.min.css" rel="stylesheet"><link href="http://code360ds.com/fst100/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet"><link href="http://code360ds.com/fst100/css/sb-admin-2.css" rel="stylesheet"><link href="http://code360ds.com/fst100/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"><link rel="stylesheet" type="text/css" href="http://code360ds.com/fst100/lib/external/jquery/themes/bootstrap/easyui.css" /><link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/icon.css" /><script src="http://code360ds.com/fst100/js/jquery.js"></script><script src="http://code360ds.com/fst100/js/bootstrap.min.js"></script><script src="http://code360ds.com/fst100/js/plugins/metisMenu/metisMenu.min.js"></script><script src="http://code360ds.com/fst100/js/sb-admin-2.js"></script>
   </head><body>';
		
		$serviceTrack=new ServiceTrackService();
		$sql="select tbl_service.*,v.regNo,(SELECT reading from tblodometer where vehicleId=tbl_service.numberPlate) currentReading from tbl_service inner join tblvehicle v on tbl_service.numberPlate= v.id";
		echo "<h3 >Service Report</h3><p>Please vehicles' service is due</p><table class='table table-striped table-hovered table-bordered'><tr><td>Vehicle</td><td>Service Reading</td><td>Status</td></tr>";
		$msg.="<h3 style='color:#096;'>Service Alerts</h3><p>Dear sir/Madam</p><p>Please  vehicles' service is due</p><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><td>Vehicle</td><td>Position</td><td>Status</td></tr>";
		foreach($serviceTrack->view_query($sql) as $row)
		{
			$nextReading=$row['next_reading'];
			$regNo=$row['regNo'];
			$currentReading=$row['currentReading'];
			if(($nextReading-$currentReading)<=1000){
				echo"<tr><td>".$row['regNo']."</td><td>".$row['next_reading']."</td><td>Service Due</td></tr>";
				$msg.="<tr><td>".$row['regNo']."</td><td>".$row['next_reading']."</td><td>Service Due</td></tr>";
				$send=true;
			}
		}
		echo"</table>";
		
	}
    
}
?>