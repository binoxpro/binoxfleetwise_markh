<?php 
 require_once('../services/localpurchaseorderitemService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $localpurchaseorderitem=new localpurchaseorderitemService();
		 if(isset($_REQUEST['lopId']))
         {
             $localpurchaseorderitem->setlopId($_REQUEST['lopId']);
         }
 		 echo json_encode( $localpurchaseorderitem->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $localpurchaseorderitem=new localpurchaseorderitemService();
 	 	 $localpurchaseorderitem->setprfItemId($_REQUEST['prfItemId']);
	 	 $localpurchaseorderitem->setqty($_REQUEST['qty']);
	 	 $localpurchaseorderitem->setrate($_REQUEST['rate']);
	 	 $localpurchaseorderitem->setcurrencyCode('UGX');
	 	 $localpurchaseorderitem->setisActive(true);
         $localpurchaseorderitem->setlopId($_REQUEST['lopId']);
	 	 $localpurchaseorderitem->setcreationDate(date('Y-m-d H:i:s'));
	 	 echo json_encode( $localpurchaseorderitem->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $localpurchaseorderitem=new localpurchaseorderitemService();
 	 	 $localpurchaseorderitem->setid($_REQUEST['id']);
	 	 $localpurchaseorderitem->setprfItemId($_REQUEST['prfItemId']);
	 	 $localpurchaseorderitem->setqty($_REQUEST['qty']);
	 	 $localpurchaseorderitem->setrate($_REQUEST['rate']);
	 	 $localpurchaseorderitem->setcurrencyCode('UGX');
	 	 $localpurchaseorderitem->setisActive(true);
         $localpurchaseorderitem->setlopId($_REQUEST['lopId']);
	 	 $localpurchaseorderitem->setcreationDate(date('Y-m-d H:i:s'));
	 	 echo json_encode( $localpurchaseorderitem->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $localpurchaseorderitem=new localpurchaseorderitemService();
 	 	 $localpurchaseorderitem->setid($_REQUEST['id']);
echo json_encode( $localpurchaseorderitem->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $localpurchaseorderitem=new localpurchaseorderitemService();
 		 echo json_encode( $localpurchaseorderitem->viewConbox());
		}   
} ?>