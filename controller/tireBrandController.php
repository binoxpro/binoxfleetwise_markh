<?php 
 require_once('../services/tireBrandService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $tireBrand=new tireBrandService();
 echo json_encode( $tireBrand->view()); 
 
 }else if($controller_templet->getAction()=='add'){
    set_time_limit(0);
     $tireBrand=new tireBrandService();
     $sql="select *,od.reading currentReading,od.reading-th.fixingOdometer kmsDone,(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') treadDepthx,(case when (select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1') is null then format(((t.retreadDepth-0)/t.retreadDepth)*0,2) else format(((t.retreadDepth-(select treadDepth from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id and tih.isActive='1'))/t.retreadDepth)*100,2) end ) treadWear,(select pressure from tbltireinspectiondetails tid inner join tbltireinspectionheader tih on tih.id=tid.inspectionHeaderId where tid.tireHoldId=th.id  and tih.isActive='1' ) pressurex from  tbltirehold th inner join tbltyretype tt on tt.id=th.positionId inner join tblvehicle v on v.id =th.vehicleId inner join tbltire t on t.id=th.tireId inner join tblodometer od on th.vehicleId=od.vehicleId WHERE th.isActive='1' order by th.vehicleId,tt.id";
      foreach($tireBrand->view_query($sql) as $row)
        {
	 	 $tireBrand=new tireBrandService();
 	 	 $tireBrand->setbrand($row['tireBand']);
	 	 $tireBrand->setisActive(true);
         $tireBrand->save();
	 	 }
          //echo json_encode( );
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $tireBrand=new tireBrandService();
 	 	 $tireBrand->setid($_REQUEST['id']);
	 	 $tireBrand->setbrand($_REQUEST['brand']);
	 	 $tireBrand->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $tireBrand->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $tireBrand=new tireBrandService();
 	 	 $tireBrand->setid($_REQUEST['id']);
echo json_encode( $tireBrand->delete()); 
} 
} ?>