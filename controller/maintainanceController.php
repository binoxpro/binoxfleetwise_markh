<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/maintainanceService.php';
require_once '../services/weeklyReadingService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
      
	   $maintain=new MaintainanceService();
	   $maintain->setWorkID($_REQUEST['workdone']);
	   $maintain->setNumberPlate($_REQUEST['numberPlate3']);
	   $maintain->setReg_date($_REQUEST['reg_date3']);
	   echo json_encode($maintain->save());
    }else if($controller_templet->getAction()=="view"){
		  $maintain=new MaintainanceService();
		  if(isset($_REQUEST['id']) ){
		  $maintain->setNumberPlate($_REQUEST['id']);
		 echo json_encode($maintain->view_report());
		  }
		
	}else if($controller_templet->getAction()=="view_parts"){
		$maintain=new MaintainanceService();
		$maintain->setMID($_REQUEST['id']);
		echo json_encode($maintain->view2());
	}else if($controller_templet->getAction()=="viewDriver"){
		$maintain=new MaintainanceService();
		
		echo json_encode($maintain->view3());
	}else if($controller_templet->getAction()=="addPart"){
		  
		$maintain=new MaintainanceService();
		$maintain->setMID($_REQUEST['id']);
		$maintain->setPartUsed($_REQUEST['partUsed']);
		echo json_encode($maintain->savePart());
	}else if($controller_templet->getAction()=="updatePart"){
		  $maintain=new MaintainanceService();
		$maintain->setMID($_REQUEST['id']);
		$maintain->setPartUsed($_REQUEST['partUsed']);
		$maintain->setPartID($_REQUEST['partID']);
		echo json_encode($maintain->updatePart());
		
	}else if($controller_templet->getAction()=="getMID"){
       $maintainanceService=new MaintainanceService();
	   echo $maintainanceService->getMIDNo();
	}else if($controller_templet->getAction()=="addDriver"){
       $maintainanceService=new MaintainanceService();
	   echo json_encode($maintainanceService->saveDriver($_REQUEST['names'],$_REQUEST['drivingClass'],$_REQUEST['expiry']));
	}else if($controller_templet->getAction()=="addWeekly"){
		$weekly=new weeklyReadingService();
		$weekly->setNumberPlate($_REQUEST['numberPlate']);
		$weekly->setKmReading($_REQUEST['mileage']);
		$weekly->setRegDate($_REQUEST['reg_Date']);
		echo json_encode($weekly->save());
		
	}else if($controller_templet->getAction()=="update"){
		$maintain=new MaintainanceService();
	   $maintain->setWorkID($_REQUEST['workDone']);
	   $maintain->setNumberPlate($_REQUEST['numberPlate']);
	   $maintain->setReg_date($_REQUEST['reg_date']);
	   $maintain->setMID($_REQUEST['id']);
	   echo json_encode($maintain->update());
	}
    
}
?>