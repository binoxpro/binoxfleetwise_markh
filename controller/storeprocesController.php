<?php 
 require_once('../services/storeprocesService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
	     $str='';
		 $storeproces=new storeprocesService();
		 $str.='<table class="table table-striped table-bordered"><thead><tr><td>Status</td><td>Description</td><td>Items In Quene</td></tr></thead><tbody>';
       $sql="select * from StoreProcessStatus";

        foreach ($storeproces->view_query($sql) as $row)
        {
            $str.='<tr><td>'.$row['Status'].'</td><td>'.$row['Description'].'</td><td>'.$row['WorkItems'].'</td></tr>';
        }
        $str.="</tbody></table>";
        echo $str;
 }else if($controller_templet->getAction()=='viewProcess')
 {
     $storeproces=new storeprocesService();
     $vehicleId=isset($_REQUEST['vehicleId'])?$_REQUEST['vehicleId']:NULL;
     $jobcardNo=isset($_REQUEST['jobcardNo'])?$_REQUEST['jobcardNo']:NULL;
     $startDate=isset($_REQUEST['startDate'])?$_REQUEST['startDate']:NULL;
     $endDate=isset($_REQUEST['endDate'])?$_REQUEST['endDate']:NULL;
     if(!is_null($vehicleId)&&!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],$vehicleId,$jobcardNo,$startDate,$endDate));
     }else if(!is_null($vehicleId)&&!is_null($jobcardNo))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],$vehicleId,$jobcardNo));
     }else if(!is_null($vehicleId)&&(!is_null($startDate)&&!is_null($endDate))){

         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],$vehicleId,NULL,$startDate,$endDate));
     }else if(!is_null($jobcardNo)&&(!is_null($startDate)&&!is_null($endDate)))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],NULL,$jobcardNo,$startDate,$endDate));

     }else if(!is_null($vehicleId))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],$vehicleId));

     }else if(!is_null($jobcardNo))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],NULL,$jobcardNo));
     }else if((!is_null($startDate)&&!is_null($endDate)))
     {
         echo json_encode($storeproces->viewProcessSearch($_REQUEST['processId'],NULL,NULL,$startDate,$endDate));
     }else{
         echo json_encode($storeproces->viewProcess($_REQUEST['processId']));
     }


 }
	     else if($controller_templet->getAction()=='add'){
	 	 $storeproces=new storeprocesService();
 	 	 $storeproces->setprfId($_REQUEST['prfId']);
	 	 $storeproces->setprocessHistoryId($_REQUEST['processHistoryId']);
	 	 echo json_encode( $storeproces->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $storeproces=new storeprocesService();
 	 	 $storeproces->setid($_REQUEST['id']);
	 	 $storeproces->setprfId($_REQUEST['prfId']);
	 	 $storeproces->setprocessHistoryId($_REQUEST['processHistoryId']);
	 	 echo json_encode( $storeproces->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $storeproces=new storeprocesService();
 	 	 $storeproces->setid($_REQUEST['id']);
echo json_encode( $storeproces->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $storeproces=new storeprocesService();
 		 echo json_encode( $storeproces->viewConbox());
		}   
} ?>