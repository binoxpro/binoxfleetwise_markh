<?php 
 require_once('../services/tireHoldService.php');
 require_once('../services/tireService.php');
 require_once('../services/tireIusseService.php');
 require_once('../services/tireMaintenanceService.php');
 require_once('../services/tireRotationService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view')
 {
	 	 $tireHold=new tireHoldService();
        echo json_encode( $tireHold->view());
  }else if($controller_templet->getAction()=='view2')
  {
     $tireHold=new tireHoldService();
     if(isset($_REQUEST['id']))
     {
        echo json_encode($tireHold->viewMonitiorVehicle($_REQUEST['id']));
     }else
     {
        echo json_encode( $tireHold->viewMonitior());
     }
    
    }else if($controller_templet->getAction()=='view3')
  {
     $tireHold=new tireHoldService();
     if(isset($_REQUEST['id']))
     {
        echo json_encode($tireHold->viewMonitiorRotationVehicle($_REQUEST['id']));
     }else
     {
        echo json_encode( $tireHold->viewMonitiorRotation());
     }
    
    }else if($controller_templet->getAction()=='add')
  {
	 	 $tireHold=new tireHoldService();
 	 	 $tireHold->setvehicleId($_REQUEST['vehicleId']);
	 	 $tireHold->setpositionId($_REQUEST['positionId']);
	 	 $tireHold->settireId($_REQUEST['tireId']);
	 	 $tireHold->setfixingDate($_REQUEST['fixingDate']);
	 	 $tireHold->setfixingOdometer($_REQUEST['fixingOdometer']);
	 	 $tireHold->setexpectedKms($_REQUEST['expectedKms']);
	 	 $tireHold->setremovalKms($_REQUEST['removalKms']);
	 	 $tireHold->setactualKms($_REQUEST['actualKms']);
	 	 $tireHold->setcreationDate(date('y-m-d'));
	 	 $tireHold->setcomment("Fitted new tire");
	 	 $tireHold->setisActive(true);
         $tireHold->save();
         //if the tire history extists update the tire status
         //create an iusse record
         $tireIusse=new tireIusse();
         $tireIusse->settireId($_REQUEST['tireId']);
         $tireIusse->setcreationDate($_REQUEST['fixingDate']);
         $tireIusse->setisActive(true);
         $tireIusse->save();
         
         if(intval($_REQUEST['oldTireId'])>0)
         {
            $tireOld=new tireHoldService();
            $tireOld->setid($_REQUEST['oldTireId']);
            $tireOld->getObject();
            $tireOld->setactualKms($_REQUEST['fixingOdometer']);
            $tireOld->setremovalKms($_REQUEST['fixingOdometer']);
            $tireOld->setcomment($_REQUEST['comment']);
            $tireOld->setisActive(false);
            $tireOld->update();
            
            $tireMaintance=new tireMaintenanceService();
           $tireMaintance->settireId($tireOld->gettireId());
           $tireMaintance->setconditionM("Removal of tire");
           $tireMaintance->setcomment($_REQUEST['comment']);
           $tireMaintance->setcreationDate(date('y-m-d'));
           $tireMaintance->save();
           
           $tire2=new tireService();
           $tire2->setid($_REQUEST['tireId']);
           $tire2->getObject();
           $tire2->setstatus('Fitted');
           $tire2->update();
           //THE TIRE ITS SELF
           $tire=new tireService();
           $tire->setid($tireOld->gettireId());
           $tire->getObject();
           $tire->setstatus($_REQUEST['isActive']);
           echo json_encode($tire->update());
           //The tire action
           
            
            
         }else
         {
            $tire=new tireService();
           $tire->setid($_REQUEST['tireId']);
           $tire->getObject();
           $tire->setstatus('Fitted');
           echo json_encode($tire->update());
            //array('success'=>true));
         }
         
         
         //if the take action is  prepare
         
         //record the tread depth change
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $tireHold=new tireHoldService();
 	 	 $tireHold->setid($_REQUEST['id']);
         $tireHold->getObject();
	 	 $tireHold->setvehicleId($_REQUEST['vehicleId']);
	 	 $tireHold->setpositionId($_REQUEST['positionId']);
	 	 $tireHold->settireId($_REQUEST['tireId']);
	 	 $tireHold->setfixingDate($_REQUEST['fixingDate']);
	 	 $tireHold->setfixingOdometer($_REQUEST['fixingOdometer']);
	 	 $tireHold->setexpectedKms($_REQUEST['expectedKms']);
	 	 $tireHold->setremovalKms($_REQUEST['removalKms']);
	 	 $tireHold->setactualKms($_REQUEST['actualKms']);
	 	 $tireHold->setcreationDate($_REQUEST['creationDate']);
	 	 $tireHold->setcomment("Fixed new tire");
	 	 $tireHold->setisActive(true);
	 	 echo json_encode( $tireHold->update()); 
 }

 else if($controller_templet->getAction()=='editExistion')
 {
     $tireHold=new tireHoldService();
     $tireHold->setid($_REQUEST['thid']);
     $tireHold->getObject();
     $range=floatval($_REQUEST['removalKms'])-$tireHold->getremovalKms();
     $expectedKms=$tireHold->getexpectedKms()+$range;

     //$tireHold->setvehicleId($_REQUEST['vehicleId']);
     //$tireHold->setpositionId($_REQUEST['positionId']);
     //$tireHold->settireId($_REQUEST['tireId']);
     //$tireHold->setfixingDate($_REQUEST['fixingDate']);
     //$tireHold->setfixingOdometer($_REQUEST['fixingOdometer']);
     $tireHold->setexpectedKms($expectedKms);
     $tireHold->setremovalKms($_REQUEST['removalKms']);
     $tireHold->setactualKms($_REQUEST['removalKms']);
     //$tireHold->setcreationDate($_REQUEST['creationDate']);
     //$tireHold->setcomment("Fixed new tire");
     //$tireHold->setisActive(true);
     echo json_encode( $tireHold->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $tireHold=new tireHoldService();
 	 	 $tireHold->setid($_REQUEST['id']);
			echo json_encode( $tireHold->delete()); 
}else if($controller_templet->getAction()=='getTireHold')
{
    $tireHold=new tireHoldService();
    $tireHold->setvehicleId($_REQUEST['vid']);
    $tireHold->setpositionId($_REQUEST['pid']);
    $tireHold->setisActive(true);
    $tireHold->getObjectExistanceTireOn();
    if(!is_null($tireHold->getid())||$tireHold->getid()!="")
    {
    $tire=new tireService();
    $tire->setid($tireHold->gettireId());
    $tire->getObject();
    $cr=$tireHold->setCurrentReadingOn($_REQUEST['vid']);
    echo $cr-$tireHold->getfixingOdometer()."*".$tire->gettireBand()."*".$tireHold->getid();
    }else
    {
       echo "Not in System*Not in System*-1"; 
    }
}else if($controller_templet->getAction()=='edit2')
{
    try{
        //comment other line
    //$thid1=$_REQUEST['thid'];
    $thid1=$_REQUEST['thid'];
    $toPosition=$_REQUEST['toPosition'];
    $fromVehicle=$_REQUEST['vehicleId2'];
    $kmsDoneTotal=0;
    $rotationDate=$_REQUEST['rotationDate'];
    $currentReading=$_REQUEST['currentReading'];
    $distanceTire1=0;
    $distanceTire2=0;
    $tyre1=-1;
    $tire2=-1;
    //set one for the first tire in position
    //current position
    $tireHold=new tireHoldService();
    $tireHold->setid($thid1);
    $tireHold->getObject();
    $tyre1=$tireHold->gettireId();
    
    //set 2 for the to fix in position
    //Rotation to 
    $tireHoldTwo=new tireHoldService();
    $tireHoldTwo->setvehicleId($fromVehicle);
    $tireHoldTwo->setpositionId($toPosition);
    $tireHoldTwo->getObjectExistanceTireOn();
    $tire2=$tireHoldTwo->gettireId();
    //Distance
    $distanceTire1=$tireHold->getObjectTireDistance2($currentReading);
    $tireHold->setremovalKms($currentReading);
    $tireHold->setactualKms($currentReading);
    $tireHold->setisActive(false);
    $tireHold->update();
    //save the new tire
    $position=$tireHold->getpositionId();
    $tireHold->setpositionId($toPosition);
    $tireHold->setfixingOdometer($currentReading);
    $tire=new tireService();
    $tire->setid($tyre1);
    $tire->getObject();
    
    
    $expectedKms=$tire->getkms()-$distanceTire1;
    $tireHold->setexpectedKms($expectedKms);
    $tireHold->settireId($tyre1);
    $tireHold->setremovalKms($expectedKms+$currentReading);
    $tireHold->setactualKms($expectedKms+$currentReading);
    $tireHold->setcreationDate($_REQUEST['rotationDate']);
    $tireHold->setisActive(true);
    $tireHold->setcomment('Tire Rotated');
    $tireHold->save();
    
    //Distance
    $distanceTire2=$tireHoldTwo->getObjectTireDistance2($currentReading);
    $tireHoldTwo->setremovalKms($currentReading);
    $tireHoldTwo->setactualKms($currentReading);
    $tireHoldTwo->setisActive(false);
    $tireHoldTwo->update();
    //fix new
    $tireHoldTwo->settireId($tire2);
    $tireHoldTwo->setpositionId($position);
    $tireHoldTwo->setfixingOdometer($currentReading);
    $tire1=new tireService();
    $tire1->setid($tire2);
    $tire1->getObject();
    $expectedKms=$tire1->getkms()-$distanceTire2;
    $tireHoldTwo->setexpectedKms($expectedKms);
    $tireHoldTwo->setremovalKms($expectedKms+$currentReading);
    $tireHoldTwo->setactualKms($expectedKms+$currentReading);
    $tireHoldTwo->setcreationDate($_REQUEST['rotationDate']);
    $tireHoldTwo->setisActive(true);
    $tireHoldTwo->setcomment('Tire Rotated');
    $tireHoldTwo->save();
    //save the tire rotation.
    $tireRotation=new tireRotationService();
    $tireRotation->settireId($tireHold->gettireId());
    $tireRotation->setfromPosition($position);
    $tireRotation->settoPosition($toPosition);
    $tireRotation->settireHoldId($tireHold->getid());
    $tireRotation->setkmsDone($distanceTire1);
    $tireRotation->setinspectionDetailsId(null);
    $tireRotation->setCreationDate($_REQUEST['rotationDate']);
    $tireRotation->save();
    
    //second rotation 
    $tireRotation=new tireRotationService();
    $tireRotation->settireId($tireHoldTwo->gettireId());
    $tireRotation->setfromPosition($toPosition);
    $tireRotation->settoPosition($position);
    $tireRotation->settireHoldId($tireHoldTwo->getid());
    $tireRotation->setkmsDone($distanceTire2);
    $tireRotation->setinspectionDetailsId(null);
    $tireRotation->setCreationDate($_REQUEST['rotationDate']);
    $tireRotation->save();
    //fix old
        echo json_encode(array('success'=>true));
    }catch(exception $exc)
    {
        echo json_encode(array('msg'=>$exc->getmessage()));
    }
    
}
} ?>