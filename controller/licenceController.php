<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/licenceService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="Next>>"){
		$licence=new LicenceService();

		$licence->setlicenceNo($_REQUEST['licenceNo']);
		$licence->setIssueDate($_REQUEST['issueDate']);
		$licence->setexpiryDate($_REQUEST['expiryDate']);
		$licence->setdriverId($_REQUEST['idDriver']);
		$licence->setisActive(true);
		$licence->setlicenceClass($_REQUEST['licenceClass']);
		if(isset($_REQUEST['id']))
		{
            //$licence->setid($licence->autoIncrement());
            //echo $licence->getid();
			$msgarray = $licence->save();
			//echo "SaVE";
		}
		else{
			$licence->setid($_REQUEST['id']);
			$msgarray = $licence->update();
		}
		if(array_key_exists('success',$msgarray)){
			header('location:../admin.php?view=Defence_Driving_Course&did='.$licence->getdriverId()."&wid=2");
		}else{
			//header('location:../admin.php?view=licence_Management&did='.$licence->getid()."&msg=".array_keys($msgarray,'msg'));
			echo json_encode($msgarray);
		}
		
	}else  if($controller_templet->getAction()=="view"){
		if(isset($_REQUEST['id'])){
			$licence=new LicenceService();
			$licence->setdriverId($_REQUEST['id']);
			$sql="select li.*, CONCAT(concat(dr.firstName,' '),dr.lastName) driverName,Case when li.isActive=1 then 'Current' else 'Not In Use' end IsActive   from tbllicence li inner join tbldriver dr on li.driverId=dr.id where li.driverId='".$licence->getdriverId()."'";
			echo json_encode($licence->view_query($sql));
			}else{
			$licence= new LicenceService();
			echo json_encode($licence->view());
			}
		
	}else if($controller_templet->getAction()=="loadDriver")
	{
		$licence=new LicenceService();
		$licence->setdriverId($_REQUEST['id']);
		$sql="select li.*, CONCAT(concat(dr.firstName,' '),dr.lastName) driverName,Case when li.isActive=1 then 'Current' else 'Not In Use' end IsActive   from tbllicence li inner join tbldriver dr on li.driverId=dr.id where li.driverId='".$licence->getdriverId()."'";
		echo json_encode($licence->view_query($sql));
	}else if($controller_templet->getAction()=="add")
	{
		$licence=new LicenceService();
		$licence->setlicenceNo($_REQUEST['licenceNo']);
		$licence->setIssueDate($_REQUEST['issueDate']);
		$licence->setexpiryDate($_REQUEST['expiryDate']);
		$licence->setdriverId($_REQUEST['driverId']);
		//$licence->setisActive(true);
		$licence->setlicenceClass($_REQUEST['licenceClass']);
		echo json_encode($licence->save());
	}
	else if($controller_templet->getAction()=="update")
	{
		$licence=new LicenceService();
		$licence->setid($_REQUEST['id']);
		$licence->setlicenceNo($_REQUEST['licenceNo']);
		$licence->setIssueDate($_REQUEST['issueDate']);
		$licence->setexpiryDate($_REQUEST['expiryDate']);
		$licence->setdriverId($_REQUEST['driverId']);
		$licence->setlicenceClass($_REQUEST['licenceClass']);
		echo json_encode($licence->update());
	}
	else if($controller_templet->getAction()=="exportExcel")
	{
		if(isset($_REQUEST['id'])){
			$licence=new LicenceService();
			$licence->setdriverId($_REQUEST['id']);
			$sql="select li.driverId, CONCAT(concat(dr.firstName,' '),dr.lastName) Name,li.licenceNo,li.issueDate,li.expiryDate,Case when li.isActive=1 then 'Current' else 'Not In Use' end Status  from tbllicence li inner join tbldriver dr on li.driverId=dr.id where li.driverId='".$licence->getdriverId()."'";
			echo json_encode($licence->view_query($sql));
			}else
            {
			$licence= new LicenceService();
			$sql="select li.driverId, CONCAT(concat(dr.firstName,' '),dr.lastName) Name,li.licenceNo,li.issueDate,li.expiryDate,Case when li.isActive=1 then 'Current' else 'Not In Use' end Status   from tbllicence li inner join tbldriver dr on li.driverId=dr.id ";
			echo json_encode($licence->view_query($sql));
			}
		
	}else if($controller_templet->getAction()=="loadLicence")
	{
		$licence=new LicenceService();
		$licence->setdriverId($_REQUEST['id']);
		echo json_encode($licence->getObject());
	}
    
}
?>