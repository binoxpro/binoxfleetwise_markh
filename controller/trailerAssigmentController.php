<?php 
 require_once('../services/trailerAssigmentService.php');
require_once('../services/employeeService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $trailerAssigment=new trailerAssigmentService();
 		 echo json_encode( $trailerAssigment->view());
 }else if($controller_templet->getAction()=='add'){

		$trailerAssigment2=new trailerAssigmentService();
		$trailerAssigment2->settractorId($_REQUEST['tractorId']);
		$trailerAssigment2->updateAllVehicle();

	 	 $trailerAssigment=new trailerAssigmentService();
 	 	 $trailerAssigment->settractorId($_REQUEST['tractorId']);
	 	 $trailerAssigment->settrailerId($_REQUEST['trailerId']);
	 	 $trailerAssigment->setassignmentDate($_REQUEST['assignmentDate']);
	 	 $trailerAssigment->setaddBy($_REQUEST['addBy']);
		 $employee=new employeeService();
		 $arrayName=explode(' ',$_REQUEST['driverId']);
		 $employee->setfirstName($arrayName[0]);
		 $employee->findWithName();
	 	 $trailerAssigment->setdriverId($employee->getempNo());
		 $trailerAssigment->setIsActive(true);
	 	 echo json_encode( $trailerAssigment->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $trailerAssigment=new trailerAssigmentService();
 	 	 $trailerAssigment->setid($_REQUEST['id']);
	 	 $trailerAssigment->settractorId($_REQUEST['tractorId']);
	 	 $trailerAssigment->settrailerId($_REQUEST['trailerId']);
	 	 $trailerAssigment->setassignmentDate($_REQUEST['assignmentDate']);
	 	 $trailerAssigment->setaddBy($_REQUEST['addBy']);
	 	 $trailerAssigment->setdriverId($_REQUEST['driverId']);
	 	 echo json_encode( $trailerAssigment->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $trailerAssigment=new trailerAssigmentService();
 	 	 $trailerAssigment->setid($_REQUEST['id']);
		echo json_encode( $trailerAssigment->delete());
}else if($controller_templet->getAction()=='viewCombo')
 {
		 $trailerAssigment=new trailerAssigmentService();
 		 echo json_encode( $trailerAssigment->viewConbox());
	}
} ?>