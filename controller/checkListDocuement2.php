<!--<p style="font-family:Calibri;">-->
<?php
require_once '../services/checkedService.php';
require_once '../services/vehicleService.php';
require_once'../services/vehicleNewService.php';
require_once'../services/driverVehicleService.php';
	$checked=new CheckedService();
	$checked->setid($_REQUEST['id']);
	$checked->loadObject();

	$vehicle=new vehicleNewService();
	$vehicle->setid($checked->getvehicleId());
	$vehicle->find();

	$driverVehicle=new driverVehicleService();
	$driverVehicle->setvehicleId($checked->getvehicleId());

$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr >
<td width="50%" style="color:#0e0b5c; " align="center"><span style="font-weight: bold; font-size: 12pt;">MARKH INVESTMENTS LIMITED COMPANY</span><br /><i>to be the main provider to the service-transport industry in Uganda and the region at large.</i><br /><p>P.O. BOX 6469, Kampala, Ntinda Complex 1st Floor, 1st Floor,Suite F1-05 +256777581120, +256701781120, +256780648455</p><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
<table width="100%"><tr >
<td width="50%" style="color:#0e0b5c; " align="center"><span style="font-weight: bold; font-size: 10pt;">'.$checklistName. ' INSPECTION CHECKLIST</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Page {PAGENO} of {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<table border="1" cellspacing="0" width="100%"><tr >
<td width="50%" style="color:#0e0b5c; " align="center">
Truck Reg No:<strong>' .$vehicle->getregNo().' /Trailer:'.'</strong></td><td> &nbsp;&nbsp;&nbsp;Date of Inspection:'.$checked->getdate().'</div></td></tr>
<tr><td><div style="text-align: left">Driver&acute;s Name:'.$checked->getdriverName().' </div></td><td>Signature:............................................</td></tr></table>

<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td width="15%">NO</td>
<td width="10%">ITEM</td>
<td width="10%">Status</td>
<td width="10%">Check</td>
<td width="20%">Inspectors&acute;s Comments</td>
</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
';

//<tr>
//<td align="center">MF1234567</td>
//<td align="center">10</td>
//<td>Large pack Hoover bags</td>
//<td class="cost">&pound;2.56</td>
//</tr>
$html2="";
$sql="select * from tblsection where checkTypeId='".$checked->getcheckListTypeId()."' and isActive='ON'";
		foreach ($checked->view_query($sql) as $row)
		{
			$sectionId=$row['id'];
			//echo "<table class='table table-striped table-bordered'>";
			//$html2.= "<tr ><td colspan='4' class='totals'>".$row['name']."</td></tr>";
			$sql2="select * from tblsectionitem where sectionId='".$sectionId."'";
			//$html2.= "<tr><td>Item</td><td>Item Description</td><td>Comment</td></tr>";
			$nox=0;
			foreach($checked->view_query($sql2) as $row2)
			{
				$comment="N/A";
				$sectionItemId=$row2['id'];
				$sqlChecked="select * from tblcheckedItem where sectionItemId='$sectionItemId' and checklistId='".$checked->getid()."' ";
				//$nox=$checked->loadStatus($sqlChecked);
				foreach($checked->view_query($sqlChecked) as $rowChecked)
				{
					$nox=1;
					$comment=$rowChecked['inspectorsComment'];

				}
				if($nox==0){
					$html2.= "<tr><td class='totals'>".$row2["id"]."</td><td class='totals'>".$row2['itemName']."</td><td class='totals'>Passed</td><td class='totals'></td><td class='totals'>".$comment."</td></tr>";
					$nox=0;
				}else{
				$html2.= "<tr><td class='totals'>".$row2["id"]."</td><td class='totals'>".$row2['itemName']."</td><td class='totals'>Failed</td><td class='totals'></td><td class='totals'>".$comment."</td></tr>";
					$nox=0;
				}
				$nox=0;
			}
			//echo"</tbody></table>";
		}

$vo=$checked->getvehicleCoditionOk()==1?'<input type="checkbox" id="ckecked" checked="checked">':'<input type="checkbox" >';
$dc=$checked->getdefectscorrected()==1?'<input type="checkbox" checked>':'<input type="checkbox" >';
$dfc=$checked->getdefectdonotcorrection()==1?'<input type="checkbox" checked>':'<input type="checkbox" >';
$html.=$html2.'<!-- END ITEMS HERE -->
</tbody>
</table>

<table>
<tr><td>'.$vo.'Vehicle Condition ok</td><td>'.$dc.'Defects corrected</td></tr>
<tr><td>'.$dfc.'Defects do not need to be corrected for safe driving</td><td>Inspection Remarks:'.$checked->getcomment().'<br/>........................................................
<br/>Siginature:..............................</td></tr>
</table>
</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");

$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("markh investment");
$mpdf->SetAuthor("code360 data solution");
$mpdf->SetWatermarkText("Orginal Copy");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');



$mpdf->WriteHTML($html);


$mpdf->Output(); exit;

exit;

?>