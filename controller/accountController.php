<?php
ob_start();
require_once '../services/accountService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
    $accountService=new AccountService();
    $accountService->setAccount_code($controller_templet->filter_inputs($_REQUEST['account_code']));
    $accountService->setAccount_name($controller_templet->filter_inputs($_REQUEST['account_name']));
    $accountService->setAccount_type($controller_templet->filter_inputs($_REQUEST['account_type']));
    $accountService->setAnalysis_required($controller_templet->filter_inputs($_REQUEST['analysis_required']));
    $accountService->setAccount_balance($controller_templet->filter_inputs($_REQUEST['account_balance']));
    //$accountService->setAccount_status($controller_templet->filter_inputs($_REQUEST['account_status']));
    echo json_encode($accountService->save());
    
	 
	 
    } if($controller_templet->getAction()=="view"){
		$accountService=new AccountService();
		
		echo json_encode($accountService->view());
	}else if($controller_templet->getAction()=="view_cash_book_payment"){
       $accountService=new AccountService();
	   $accountService->setAccount_type('ACA');
	   echo json_encode($accountService->view_cash_accounts());
	}else if($controller_templet->getAction()=="update"){
        $accountService=new AccountService();
    $accountService->setAccount_code($controller_templet->filter_inputs($_REQUEST['id']));
    $accountService->setAccount_name($controller_templet->filter_inputs($_REQUEST['account_name']));
    $accountService->setAccount_type($controller_templet->filter_inputs($_REQUEST['account_type']));
    $accountService->setAnalysis_required($controller_templet->filter_inputs($_REQUEST['analysis_required']));
    $accountService->setAccount_balance($controller_templet->filter_inputs($_REQUEST['account_balance']));
	echo json_encode($accountService->update());
	   
    }else if($controller_templet->getAction()=="account_status"){
       $accountService=new AccountService();
	   $accountService->setAccount_code($controller_templet->filter_inputs($_REQUEST['account_code']));
	   $accountService->view_account_analysis_status();
	   echo $accountService->getAccount_status();
	   
	   
    }else if($controller_templet->getAction()=="view_expense_account"){
		$accountService=new AccountService();
		$accountService->setAccount_type('EXP');
		echo json_encode($accountService->viewofType());
	}
    
}
?>