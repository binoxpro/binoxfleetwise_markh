<?php 
 require_once('../services/distanceService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $distance=new distanceService();
 		 echo json_encode( $distance->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $distance=new distanceService();
 	 	 $distance->setdistrictFrom($_REQUEST['districtFrom']);
	 	 $distance->setdistrictTo($_REQUEST['districtTo']);
	 	 $distance->setdistance($_REQUEST['distance']);
	 	 echo json_encode( $distance->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $distance=new distanceService();
 	 	 $distance->setid($_REQUEST['id']);
	 	 $distance->setdistrictFrom($_REQUEST['districtFrom']);
	 	 $distance->setdistrictTo($_REQUEST['districtTo']);
	 	 $distance->setdistance($_REQUEST['distance']);
	 	 echo json_encode( $distance->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $distance=new distanceService();
 	 	 $distance->setid($_REQUEST['id']);
echo json_encode( $distance->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $distance=new distanceService();
 		 echo json_encode( $distance->viewConbox());
		}
 else if($controller_templet->getAction()=='getDistance'){
	 $distance=new distanceService();
	 $distance->setid($_REQUEST['id']);
	 $distance->find();
		echo $distance->getdistance();
 }
 } ?>