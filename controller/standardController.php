<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/standardService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
		$standard=new StandardsService();
		$standard->setabs($_REQUEST['abs']);
		$standard->setbmc($_REQUEST['bmc']);
		$standard->setfbsm($_REQUEST['fbsm']);
		$standard->setobc($_REQUEST['obc']);
		$standard->setsst($_REQUEST['sst']);
		$standard->setsstt($_REQUEST['sstt']);
		$standard->setrur($_REQUEST['rur']);
		$standard->setsur($_REQUEST['sur']);
		$standard->setvehicleId($_REQUEST['vehicleId']);
		$standard->setisActive(true);
		//$standard->setparentId($_REQUEST['parentId']);
		echo json_encode($standard->save());
	}else  if($controller_templet->getAction()=="view"){
		$standard=new StandardsService();
		echo json_encode($standard->view());
	}else if($controller_templet->getAction()=="edit"){
		$standard=new StandardsService();
		$standard->setid($_REQUEST['id']);
		$standard->setabs($_REQUEST['abs']);
		$standard->setbmc($_REQUEST['bmc']);
		$standard->setfbsm($_REQUEST['fbsm']);
		$standard->setobc($_REQUEST['obc']);
		$standard->setsst($_REQUEST['sst']);
		$standard->setsstt($_REQUEST['sstt']);
		$standard->setrur($_REQUEST['rur']);
		$standard->setsur($_REQUEST['sur']);
		$standard->setvehicleId($_REQUEST['vehicleId']);
		$standard->setisActive(true);
		//$standard->setparentId($_REQUEST['parentId']);
		echo json_encode($standard->update());
	}
    
}
?>