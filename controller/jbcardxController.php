<?php 
 require_once('../services/jbcardxService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $jbcardx=new jbcardxService();
 		 echo json_encode( $jbcardx->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $jbcardx=new jbcardxService();
 	 	 $jbcardx->setjobNo($_REQUEST['jobNo']);
	 	 $jbcardx->setcreationDate($_REQUEST['creationDate']);
	 	 $jbcardx->setvehicleId($_REQUEST['vehicleId']);
	 	 $jbcardx->setrectified($_REQUEST['rectified']);
	 	 $jbcardx->setdateCompleted($_REQUEST['dateCompleted']);
	 	 $jbcardx->setodometer($_REQUEST['odometer']);
	 	 $jbcardx->settime($_REQUEST['time']);
	 	 $jbcardx->setchecklistId($_REQUEST['checklistId']);
	 	 $jbcardx->setcomment($_REQUEST['comment']);
	 	 $jbcardx->setlabourCost($_REQUEST['labourCost']);
	 	 $jbcardx->setlabourHour($_REQUEST['labourHour']);
	 	 $jbcardx->setgarrage($_REQUEST['garrage']);
	 	 $jbcardx->setfuelLevel($_REQUEST['fuelLevel']);
	 	 $jbcardx->setdriverName($_REQUEST['driverName']);
	 	 echo json_encode( $jbcardx->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $jbcardx=new jbcardxService();
 	 	 $jbcardx->setid($_REQUEST['id']);
	 	 $jbcardx->setjobNo($_REQUEST['jobNo']);
	 	 $jbcardx->setcreationDate($_REQUEST['creationDate']);
	 	 $jbcardx->setvehicleId($_REQUEST['vehicleId']);
	 	 $jbcardx->setrectified($_REQUEST['rectified']);
	 	 $jbcardx->setdateCompleted($_REQUEST['dateCompleted']);
	 	 $jbcardx->setodometer($_REQUEST['odometer']);
	 	 $jbcardx->settime($_REQUEST['time']);
	 	 $jbcardx->setchecklistId($_REQUEST['checklistId']);
	 	 $jbcardx->setcomment($_REQUEST['comment']);
	 	 $jbcardx->setlabourCost($_REQUEST['labourCost']);
	 	 $jbcardx->setlabourHour($_REQUEST['labourHour']);
	 	 $jbcardx->setgarrage($_REQUEST['garrage']);
	 	 $jbcardx->setfuelLevel($_REQUEST['fuelLevel']);
	 	 $jbcardx->setdriverName($_REQUEST['driverName']);
	 	 echo json_encode( $jbcardx->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $jbcardx=new jbcardxService();
 	 	 $jbcardx->setid($_REQUEST['id']);
echo json_encode( $jbcardx->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $jbcardx=new jbcardxService();
 		 echo json_encode( $jbcardx->viewConbox());
		}   
} ?>