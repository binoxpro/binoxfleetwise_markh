<?php 
 require_once('../services/truckModelService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $truckModel=new truckModelService();
 		echo json_encode( $truckModel->view());
 }else if($controller_templet->getAction()=='add')
 {
	 	 $truckModel=new truckModelService();
 	 	 $truckModel->setmodelName($_REQUEST['modelName']);
	 	 $truckModel->setmakeId($_REQUEST['makeId']);
	 	 $truckModel->setyearManufacture($_REQUEST['yearManufacture']);
	 	 $truckModel->setcountry($_REQUEST['country']);
	 	if(isset($_REQUEST['isActive']))
		{
			$truckModel->setisActive($_REQUEST['isActive']);
		}else{
			$truckModel->setisActive(false);
		}

	 	 echo json_encode( $truckModel->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $truckModel=new truckModelService();
 	 	 $truckModel->setid($_REQUEST['id']);
	 	 $truckModel->setmodelName($_REQUEST['modelName']);
	 	 $truckModel->setmakeId($_REQUEST['makeId']);
	 	 $truckModel->setyearManufacture($_REQUEST['yearManufacture']);
	 	 $truckModel->setcountry($_REQUEST['country']);
		 if(isset($_REQUEST['isActive']))
		 {
			 $truckModel->setisActive($_REQUEST['isActive']);
		 }else{
			 $truckModel->setisActive(false);
		 }
	 	 echo json_encode( $truckModel->update());
 }
 else if($controller_templet->getAction()=='delete'){
	 	$truckModel=new truckModelService();
	 	$truckModel->setid($_REQUEST['id']);
		echo json_encode( $truckModel->delete());
 }
 else if($controller_templet->getAction()=='viewCombo')
 {
	 $truckModel=new truckModelService();
	 echo json_encode($truckModel->viewCombo(null));

 }

} ?>