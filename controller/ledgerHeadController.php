<?php 
 require_once('../services/ledgerHeadService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $ledgerHead=new ledgerHeadService();
 		 echo json_encode( $ledgerHead->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $ledgerHead=new ledgerHeadService();
 	 	 $ledgerHead->settransactionDate($_REQUEST['transactionDate']);
	 	 $ledgerHead->setmonthId($_REQUEST['monthId']);
	 	 $ledgerHead->setamount($_REQUEST['amount']);
	 	 $ledgerHead->setipAddress($_REQUEST['ipAddress']);
	 	 $ledgerHead->setuserId($_REQUEST['userId']);
	 	 $ledgerHead->setpostingDate($_REQUEST['postingDate']);
	 	 $ledgerHead->setpostingTime($_REQUEST['postingTime']);
	 	 echo json_encode( $ledgerHead->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $ledgerHead=new ledgerHeadService();
 	 	 $ledgerHead->setid($_REQUEST['id']);
	 	 $ledgerHead->settransactionDate($_REQUEST['transactionDate']);
	 	 $ledgerHead->setmonthId($_REQUEST['monthId']);
	 	 $ledgerHead->setamount($_REQUEST['amount']);
	 	 $ledgerHead->setipAddress($_REQUEST['ipAddress']);
	 	 $ledgerHead->setuserId($_REQUEST['userId']);
	 	 $ledgerHead->setpostingDate($_REQUEST['postingDate']);
	 	 $ledgerHead->setpostingTime($_REQUEST['postingTime']);
	 	 echo json_encode( $ledgerHead->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $ledgerHead=new ledgerHeadService();
 	 	 $ledgerHead->setid($_REQUEST['id']);
echo json_encode( $ledgerHead->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $ledgerHead=new ledgerHeadService();
 		 echo json_encode( $ledgerHead->viewConbox());
		}   
} ?>