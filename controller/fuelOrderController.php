<?php 
 require_once('../services/fuelOrderService.php');
  require_once('controller.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/financialMonthService.php');
require_once('../services/fuelOrderItemService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/fuelProductService.php');
require_once('../services/tripService.php');

 if(isset($_REQUEST['action']))
 {
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view')
	 {
		 $fuelOrder=new fuelOrderService();
		 if(isset($_REQUEST['TripNo']))
		 {
			 //echo $_REQUEST['TripNo'];
			 $fuelOrder->settripNo($_REQUEST['TripNo']);
			 echo json_encode($fuelOrder->viewTrip());
		 }else
		 {
			 echo json_encode($fuelOrder->view());
		 }
	}else if($controller_templet->getAction()=='add')
	{
	 	 $fuelOrder=new fuelOrderService();
         $fuelOrder->setid($_REQUEST['id']);
 	 	 $fuelOrder->setorderNo($_REQUEST['orderNo']);
	 	 $fuelOrder->settripNo($_REQUEST['tripNo']);
	 	 $fuelOrder->setinvoiceNo($_REQUEST['invoiceNo']);
	 	 $fuelOrder->setcreationDate($_REQUEST['creationDate']);
		 if(isset($_REQUEST['paid']))
		 {
			 $fuelOrder->setpaid($_REQUEST['paid']);
		 }else
         {
			 $fuelOrder->setpaid(0);
		 }



	 	 if(isset($_REQUEST['checked']))
		 {
			 if(!is_null($_REQUEST['checked']))
			 {
				 $fuelOrder->setchecked($_REQUEST['checked']);
			 }else
             {
				 $fuelOrder->setchecked(0);
			 }

		 }else
		 {
			 $fuelOrder->setchecked(0);
		 }

		if(isset($_REQUEST['posted']))
		{
			$fuelOrder->setposted($_REQUEST['posted']);
		}else
		{
			$fuelOrder->setposted(0);
		}

	 	 $fuelOrder->setsupplierId($_REQUEST['supplierId']);
	 	 echo json_encode( $fuelOrder->save());
		 //
 }
 else if($controller_templet->getAction()=='edit')
 {
	 $fuelOrder=new fuelOrderService();
	 $fuelOrder->setid($_REQUEST['id']);
	 $fuelOrder->setorderNo($_REQUEST['orderNo']);
	 $fuelOrder->settripNo($_REQUEST['tripNo']);
	 $fuelOrder->setinvoiceNo($_REQUEST['invoiceNo']);
	 $fuelOrder->setcreationDate($_REQUEST['creationDate']);
	 if(isset($_REQUEST['paid']))
	 {
		 $fuelOrder->setpaid($_REQUEST['paid']);
	 }else{
		 $fuelOrder->setpaid(0);
	 }



	 if(isset($_REQUEST['checked']))
	 {
		 if(!is_null($_REQUEST['checked']))
		 {
			 $fuelOrder->setchecked($_REQUEST['checked']);
		 }else{
			 $fuelOrder->setchecked(0);
		 }

	 }else
	 {
		 $fuelOrder->setchecked(0);
	 }

	 if(isset($_REQUEST['posted']))
	 {
		 $fuelOrder->setposted($_REQUEST['posted']);
	 }else
	 {
		 $fuelOrder->setposted(0);
	 }

	 $fuelOrder->setsupplierId($_REQUEST['supplierId']);
	 echo json_encode( $fuelOrder->update());
 }else if($controller_templet->getAction()=='invoiced')
 {
	 $fuelOrder=new fuelOrderService();
	 $fuelOrder->setid($_REQUEST['id']);
	 $fuelOrder->setinvoiceNo($_REQUEST['invoiceNo']);
	 echo json_encode( $fuelOrder->update());
 } else if($controller_templet->getAction()=='issued')
 {

	 $fuelOrder=new fuelOrderService();
	 $fuelOrder->setid($_REQUEST['id']);
	 $fuelOrder->find();
	if($fuelOrder->getchecked()=='0')
	{


	 $fuelOrder->setorderNo($_REQUEST['orderNo']);
	 $fuelOrder->settripNo($_REQUEST['tripNo']);
	 $fuelOrder->setinvoiceNo($_REQUEST['invoiceNo']);
	 $fuelOrder->setcreationDate($_REQUEST['creationDate']);
	 /*if(isset($_REQUEST['paid']))
	 {
		 $fuelOrder->setpaid($_REQUEST['paid']);
	 }else{
		 $fuelOrder->setpaid(0);
	 }*/



	 if(isset($_REQUEST['checked']))
	 {
		 $fuelOrder->setchecked(true);
		 //$fuelOrder->setsupplierId($_REQUEST['supplierId']);
		 $fuelOrder->setposted(true);
		 $fuelOrder->update();
		 //
		 //the accounting entry
		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['creationDate']);
		 //generate the monthcode;
		 $monthCode=null;
		 $dateArray=explode("-",$ledgerHead->gettransactionDate());
		 if(sizeof($dateArray)>0)
		 {
			 $monthCode =$dateArray[1]."/".$dateArray[0];
		 }
		 else
		 {

		 }

		 $fuelOrderItem=new fuelOrderItemService();
		 $fuelOrderItem->setfuelOrderId($_REQUEST['id']);
		 $totalAmount=$fuelOrderItem->findTotal();

		 $ledgerHead->setmonthId($monthCode);
		 $ledgerHead->setamount($totalAmount);
		 $ledgerHead->save();

		 //Cr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 //get the account code base on the crebitor account payable;
		 $chartOfAccount=new chartOfaccountService();
		 $chartOfAccount->setAccountName("Payables/Creditors");
		 $chartOfAccount->findAccountCode();
		 $generalLedger->setaccountCode($chartOfAccount->getAccountCode());
		 $generalLedger->setnarrative('Supplied fuel ');
		 $generalLedger->setreference('FO-'.$_REQUEST['id']);
		 $generalLedger->setamount($totalAmount);
		 $generalLedger->settransactionType('Cr');
		 $generalLedger->setindividualNo($_REQUEST['supplierId']);
		 $generalLedger->setisActive(true);
		 $generalLedger->save();


		 //Dr
		 $sql = "select * from  fuelorderitem  where fuelOrderId='" .$fuelOrder->getid() . "'";
		 foreach ($fuelOrder->view_query($sql) as $row) {
			 //get narrative
			 $amount=0;
			 $fuelProduct=new fuelProductService();
			 $fuelProduct->setid($row['fuelProductId']);
			 $fuelProduct->find();
			 $amount=$row['litres']*$row['rate'];

			 $trip=new TripService();
			 $trip->setTripNumber($fuelOrder->gettripNo());
			 $trip->find();
			 //get accountCode
			 //get total
			 //get costCenter
			 //get tripNumber
			 //


			 $generalLedger->setaccountCode($fuelProduct->getaccountCode());
			 $generalLedger->setnarrative('Supplied fuel');
			 $generalLedger->setreference('FO-' . $row['fuelOrderId']);
			 $generalLedger->setamount($amount);
			 $generalLedger->settransactionType('Dr');
			 $generalLedger->setcostCentreId($trip->getVehicleId());
			 $generalLedger->settripNo($fuelOrder->gettripNo());
			 $generalLedger->setisActive(true);
			 $generalLedger->save();

		 }



	 }else
	 {
		 //$fuelOrder->setchecked(0);
	 }





	}
	 echo json_encode(array('success'=>true));


 }

 else if($controller_templet->getAction()=='delete'){
 	 	 $fuelOrder=new fuelOrderService();
 	 	 $fuelOrder->setid($_REQUEST['id']);
		echo json_encode( $fuelOrder->delete());
}else if($controller_templet->getAction()=='viewCombo'){  
		 $fuelOrder=new fuelOrderService();
 		 echo json_encode( $fuelOrder->viewConbox());
}else if($controller_templet->getAction()=='getId')
 {
			$fuelOrder=new fuelOrderService();
	 		echo $fuelOrder->getObjectId();
 }
 } ?>