<?php 
 require_once('../services/partSupplyService.php');
require_once('../services/paymentVoucherService.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/invoiceService.php');
require_once('../services/taxInvoiceService.php');
require_once('../services/debitorService.php');
require_once('../services/partsUsedService.php');
require_once('../services/jobCardService.php');
require_once('utility.php');
require_once('controller.php');


if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $partSupply=new partSupplyService();
 		 echo json_encode( $partSupply->view());
 }	 else if($controller_templet->getAction()=='add'){

		$partsUsed=new PartsUsedService();
		$partsUsed->setid($_REQUEST['id']);
		$partsUsed->find();
		 $partsUsed->setsupplied(true);
		$partsUsed->update();

		 $jobCard=new jobCardService();
		 $jobCard->setid($partsUsed->getjobcardId());
		 $jobCard->find();

		 $creditorAccount=new chartOfaccountService();
		 $creditorAccount->setAccountName('Payables/Creditors');
		 $creditorAccount->findAccountCode();

		 $expenseAccount=new chartOfaccountService();
		 $expenseAccount->setAccountName('Spare Parts');
		 $expenseAccount->findAccountCode();

		 $dateArray=explode('-',$_REQUEST['deliveryDate']);
		 $total=0;
		 $total=(floatval($_REQUEST['qty'])*floatval($_REQUEST['unitprice']));
		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['deliveryDate']);
		 $ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
		 $ledgerHead->setamount((floatval($_REQUEST['qty'])*floatval($_REQUEST['unitprice'])));
		 $ledgerHead->save();

		 //Cr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($creditorAccount->getAccountCode());
		 $generalLedger->setnarrative('Purchase of '.$_REQUEST['partName'].' for invoice '.$_REQUEST['invoiceNo']);
		 //$generalLedger->setreference();
		 $generalLedger->setamount($total);
		 $generalLedger->settransactionType('Cr');
		 $generalLedger->setisActive(true);
		 $generalLedger->setindividualNo($_REQUEST['supplierId']);
		 //$generalLedger->setcostCentreId();
		 $generalLedger->save();

		 //Dr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($expenseAccount->getAccountCode());
		 $generalLedger->setnarrative('Purchase of '.$_REQUEST['partName'].' for invoice '.$_REQUEST['invoiceNo']);
		 $generalLedger->setamount($total);
		 $generalLedger->settransactionType('Dr');
		 $generalLedger->setreference($jobCard->getjobNo());
		 $generalLedger->setisActive(true);
		 $generalLedger->setcostCentreId($jobCard->getvehicleId());
		 $generalLedger->save();



	 	 $partSupply=new partSupplyService();
 	 	 $partSupply->setinvoiceNo($_REQUEST['invoiceNo']);
	 	 $partSupply->setdeliveryNoteNo($_REQUEST['deliveryNoteNo']);
	 	 $partSupply->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $partSupply->setpartUsedId($_REQUEST['id']);
	 	 $partSupply->setqty($_REQUEST['qty']);
	 	 $partSupply->setunitprice($_REQUEST['unitprice']);
	 	 echo json_encode( $partSupply->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $partSupply=new partSupplyService();
 	 	 $partSupply->setid($_REQUEST['id']);
	 	 $partSupply->setinvoiceNo($_REQUEST['invoiceNo']);
	 	 $partSupply->setdeliveryNoteNo($_REQUEST['deliveryNoteNo']);
	 	 $partSupply->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $partSupply->setpartUsedId($_REQUEST['partUsedId']);
	 	 $partSupply->setqty($_REQUEST['qty']);
	 	 $partSupply->setunitprice($_REQUEST['unitprice']);
	 	 echo json_encode( $partSupply->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $partSupply=new partSupplyService();
 	 	 $partSupply->setid($_REQUEST['id']);
		echo json_encode( $partSupply->delete());
}else if($controller_templet->getAction()=='viewCombo')
 {
		 $partSupply=new partSupplyService();
 		 echo json_encode( $partSupply->viewConbox());
		}   
} ?>