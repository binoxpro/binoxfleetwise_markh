<?php 
 require_once('../services/stockService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $stock=new stockService();
		 if(isset($_REQUEST['status'])){
		echo json_encode( $stock->viewStock2($_REQUEST['status'])); 	 
		 }else{
 		echo json_encode( $stock->view()); 
		 }
 }else  if($controller_templet->getAction()=='viewUsed'){
	 	 $stock=new stockService();
 echo json_encode( $stock->viewUsed()); 
 }else if($controller_templet->getAction()=='viewInStock'){
	 	 $stock=new stockService();
 echo json_encode( $stock->viewInStock()); }
 else if($controller_templet->getAction()=='add'){
	 	 $stock=new stockService();
 	 	 $stock->setbrand($_REQUEST['brand']);
	 	 $stock->setserialNumber($_REQUEST['serialNumber']);
	 	 $stock->setcost($_REQUEST['cost']);
	 	 $stock->setstatus('instock');
	 	 $stock->setretreadstatus(false);
	 	 $stock->setcreationDate($_REQUEST['creationDate']);
		 $stock->settreadDepth($_REQUEST['treadDepth']);
	 	 echo json_encode( $stock->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $stock=new stockService();
 	 	 $stock->setid($_REQUEST['id']);
	 	 $stock->setbrand($_REQUEST['brand']);
	 	 $stock->setserialNumber($_REQUEST['serialNumber']);
	 	 $stock->setcost($_REQUEST['cost']);
	 	 //$stock->setstatus($_REQUEST['status']);
	 	 //$stock->setretreadstatus($_REQUEST['retreadstatus']);
	 	 $stock->setcreationDate($_REQUEST['creationDate']);
	 	 echo json_encode( $stock->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $stock=new stockService();
 	 	 $stock->setid($_REQUEST['id']);
		echo json_encode( $stock->delete()); 
	}else if($controller_templet->getAction()=='stockTyre'){
		$stock=new stockService();
		$stock->transferStock();
	}else if($controller_templet->getAction()=='treadDepth'){
		$stock=new stockService();
		$stock->setid($_REQUEST['id']);
		$stock->getObject();
		$stock->settreadDepth($_REQUEST['treadDepth']);
		echo json_encode($stock->update());
	
	}else if($controller_templet->getAction()=='getStock'){
		$stock=new stockService();
		$stock->setid($_REQUEST['id']);
		$stock->getObject();
		echo $stock->getcost()."-".$stock->gettreadDepth()."-".$stock->getbrand();
	}
} ?>