<?php 
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherService.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/invoiceService.php');
require_once('../services/taxInvoiceService.php');
require_once('../services/debitorService.php');
require_once('utility.php');
require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $generalLedger=new generalLedgerService();
 		 echo json_encode( $generalLedger->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $generalLedger=new generalLedgerService();
 	 	 $generalLedger->setledgerHeadId($_REQUEST['ledgerHeadId']);
	 	 $generalLedger->setaccountCode($_REQUEST['accountCode']);
	 	 $generalLedger->setnarrative($_REQUEST['narrative']);
	 	 $generalLedger->setreference($_REQUEST['reference']);
	 	 $generalLedger->setamount($_REQUEST['amount']);
	 	 $generalLedger->settransactionType($_REQUEST['transactionType']);
	 	 $generalLedger->setcostCentreId($_REQUEST['costCentreId']);
	 	 $generalLedger->setindividualNo($_REQUEST['individualNo']);
	 	 $generalLedger->settripNo($_REQUEST['tripNo']);
	 	 $generalLedger->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $generalLedger->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $generalLedger=new generalLedgerService();
 	 	 $generalLedger->setid($_REQUEST['id']);
	 	 $generalLedger->setledgerHeadId($_REQUEST['ledgerHeadId']);
	 	 $generalLedger->setaccountCode($_REQUEST['accountCode']);
	 	 $generalLedger->setnarrative($_REQUEST['narrative']);
	 	 $generalLedger->setreference($_REQUEST['reference']);
	 	 $generalLedger->setamount($_REQUEST['amount']);
	 	 $generalLedger->settransactionType($_REQUEST['transactionType']);
	 	 $generalLedger->setcostCentreId($_REQUEST['costCentreId']);
	 	 $generalLedger->setindividualNo($_REQUEST['individualNo']);
	 	 $generalLedger->settripNo($_REQUEST['tripNo']);
	 	 $generalLedger->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $generalLedger->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $generalLedger=new generalLedgerService();
 	 	 $generalLedger->setid($_REQUEST['id']);
echo json_encode( $generalLedger->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $generalLedger=new generalLedgerService();
 		 echo json_encode( $generalLedger->viewConbox());
		}else if($controller_templet->getAction()=='confirmSale'){
				 $invoice=new invoiceService();
				 $invoice->setid($_REQUEST['id']);
				 $invoice->find();
				 $invoice->setconfirmed(true);
				 $invoice->update();
				 //generate supplier object
				 $debitor=new debitorService();
				 $debitor->setid($_REQUEST['customerId']);
				 $debitor->find();
				 //generate a tax invoice
				 $taxinvoice=new taxInvoiceService();
				 $taxinvoice->setcreationDate($_REQUEST['creationDate']);
				 $taxinvoice->setparentId($_REQUEST['id']);
				 $numberOfDays=60;
				 $date=date_create($_REQUEST['creationDate']);
				 date_add($date,date_interval_create_from_date_string("60 days"));
				 $taxinvoice->setdueDate(date_format($date,"Y-m-d"));
				 $taxinvoice->setid(utility::returnThreeFiguareString($taxinvoice->invoiceNumbering()));
				 $taxinvoice->save();
				if(isset($_REQUEST['vatApplied']))
				{
					$incomeAccount=new chartOfaccountService();
					$incomeAccount->setAccountName('Sales');
					$incomeAccount->findAccountCode();

					$debtorAccount=new chartOfaccountService();
					$debtorAccount->setAccountName('Debtors/Receivables');
					$debtorAccount->findAccountCode();

					$vatAccount=new chartOfaccountService();
					$vatAccount->setAccountName("VAT A/C");
					$vatAccount->findAccountCode();

					$dateArray=explode('-',$_REQUEST['creationDate']);

					$ledgerHead = new ledgerHeadService();
					$ledgerHead->settransactionDate($_REQUEST['creationDate']);
					$ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
					$ledgerHead->setamount($_REQUEST['total']);
					$ledgerHead->save();

					//Cr
					$generalLedger = new generalLedgerService();
					$generalLedger->setledgerHeadId($ledgerHead->getid());
					$generalLedger->setaccountCode($incomeAccount->getAccountCode());
					$generalLedger->setnarrative('Posted for invoice No:'.$_REQUEST['id']);
					//$generalLedger->setreference();
					$generalLedger->setamount($_REQUEST['subTotal']);
					$generalLedger->settransactionType('Cr');
					$generalLedger->setisActive(true);
					$generalLedger->save();

					//Cr
					$generalLedger = new generalLedgerService();
					$generalLedger->setledgerHeadId($ledgerHead->getid());
					$generalLedger->setaccountCode($vatAccount->getAccountCode());
					$generalLedger->setnarrative('Posted for invoice No:'.$_REQUEST['id']);
					//$generalLedger->setreference();
					$generalLedger->setamount($_REQUEST['vat']);
					$generalLedger->settransactionType('Cr');
					$generalLedger->setisActive(true);
					$generalLedger->save();

					//Dr
					$generalLedger = new generalLedgerService();
					$generalLedger->setledgerHeadId($ledgerHead->getid());
					$generalLedger->setaccountCode($debtorAccount->getAccountCode());
					$generalLedger->setnarrative('Posted for issuing invoice No:'.$_REQUEST['id']);
					//$generalLedger->setreference();
					$generalLedger->setreference($taxinvoice->getid());
					$generalLedger->setamount($_REQUEST['total']);
					$generalLedger->settransactionType('Dr');
					$generalLedger->setisActive(true);
					$generalLedger->setindividualNo($_REQUEST['customerId']);
					$generalLedger->save();
				}else
				{
					$incomeAccount=new chartOfaccountService();
					$incomeAccount->setAccountName('Sales');
					$incomeAccount->findAccountCode();

					$debtorAccount=new chartOfaccountService();
					$debtorAccount->setAccountName('Debtors/Receivables');
					$debtorAccount->findAccountCode();

					$dateArray=explode('-',$_REQUEST['creationDate']);

					$ledgerHead = new ledgerHeadService();
					$ledgerHead->settransactionDate($_REQUEST['creationDate']);
					$ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
					$ledgerHead->setamount($_REQUEST['total']);
					$ledgerHead->save();

					//Cr
					$generalLedger = new generalLedgerService();
					$generalLedger->setledgerHeadId($ledgerHead->getid());
					$generalLedger->setaccountCode($incomeAccount->getAccountCode());
					$generalLedger->setnarrative('Posted for invoice No:'.$_REQUEST['id']);
					//$generalLedger->setreference();
					$generalLedger->setamount($_REQUEST['subTotal']);
					$generalLedger->settransactionType('Cr');
					$generalLedger->setisActive(true);
					$generalLedger->save();

					//Dr
					$generalLedger = new generalLedgerService();
					$generalLedger->setledgerHeadId($ledgerHead->getid());
					$generalLedger->setaccountCode($debtorAccount->getAccountCode());
					$generalLedger->setnarrative('Posted for issuing invoice No:'.$_REQUEST['id']);
					$generalLedger->setamount($_REQUEST['total']);
					$generalLedger->settransactionType('Dr');
					$generalLedger->setreference($taxinvoice->getid());
					$generalLedger->setisActive(true);
					$generalLedger->setindividualNo($_REQUEST['customerId']);
					$generalLedger->save();
				}

			echo json_encode(array('success'=>true));
 		}else if($controller_templet->getAction()=='getBalance'){
	 		$generalLedger=new generalLedgerService();
	 		$generalLedger->setreference($_REQUEST['invoiceNo']);
	 		$debtorAccount=new chartOfaccountService();
	 		$debtorAccount->setAccountName('Debtors/Receivables');
	 		$debtorAccount->findAccountCode();
	 		$generalLedger->setaccountCode($debtorAccount->getAccountCode());
			echo number_format($generalLedger->getInvoiceBalance(),2);

 		}else if($controller_templet->getAction()=='receiptSale'){

	 if(isset($_REQUEST['whtApplied']))
	 {
		 $cashAccount=$_REQUEST['cashAccount'];

		 $debtorAccount=new chartOfaccountService();
		 $debtorAccount->setAccountName('Debtors/Receivables');
		 $debtorAccount->findAccountCode();

		 $whtAccount=new chartOfaccountService();
		 $whtAccount->setAccountName("Withholding Tax");
		 $whtAccount->findAccountCode();
		 $whtAmount=0;
		 $whtAmount=0.06*intval($_REQUEST['amount']);
		 $bankedAmount=0;
		 $bankedAmount=$_REQUEST['amount']-$whtAmount;
		 $dateArray=explode('-',$_REQUEST['transactionDate']);

		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['transactionDate']);
		 $ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
		 $ledgerHead->setamount($_REQUEST['amount']);
		 $ledgerHead->save();

		 //Cr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($debtorAccount->getAccountCode());
		 $generalLedger->setnarrative('Received payment from '.$_REQUEST['customerNamex'].' for invoice'.$_REQUEST['idx']);
		 $generalLedger->setreference($_REQUEST['idx']);
		 $generalLedger->setamount($_REQUEST['amount']);
		 $generalLedger->settransactionType('Cr');
		 $generalLedger->setisActive(true);
		 $generalLedger->setindividualNo($_REQUEST['customerIdx']);
		 $generalLedger->save();

		 //Dr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($cashAccount);
		 $generalLedger->setnarrative('Received payment from '.$_REQUEST['customerNamex'].' for invoice'.$_REQUEST['idx']);
		 //$generalLedger->setreference();
		 $generalLedger->setamount($bankedAmount);
		 $generalLedger->settransactionType('Dr');
		 $generalLedger->setisActive(true);
		 $generalLedger->save();

		 //Dr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($whtAccount->getAccountCode());
		 $generalLedger->setnarrative('Received payment from '.$_REQUEST['customerNamex'].' for invoice'.$_REQUEST['idx']);
		 //$generalLedger->setreference();
		 $generalLedger->setamount($bankedAmount);
		 $generalLedger->settransactionType('Dr');
		 $generalLedger->setisActive(true);
		 $generalLedger->save();


	 }else
	 {
		 $cashAccount=$_REQUEST['cashAccount'];

		 $debtorAccount=new chartOfaccountService();
		 $debtorAccount->setAccountName('Debtors/Receivables');
		 $debtorAccount->findAccountCode();

		 $dateArray=explode('-',$_REQUEST['transactionDate']);

		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['transactionDate']);
		 $ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
		 $ledgerHead->setamount($_REQUEST['amount']);
		 $ledgerHead->save();

		 //Cr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($debtorAccount->getAccountCode());
		 $generalLedger->setnarrative('Received payment from '.$_REQUEST['customerNamex'].' for invoice'.$_REQUEST['idx']);
		 $generalLedger->setreference($_REQUEST['idx']);
		 $generalLedger->setamount($_REQUEST['amount']);
		 $generalLedger->settransactionType('Cr');
		 $generalLedger->setisActive(true);
		 $generalLedger->setindividualNo($_REQUEST['customerIdx']);
		 $generalLedger->save();

		 //Dr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($cashAccount);
		 $generalLedger->setnarrative('Received payment from '.$_REQUEST['customerNamex'].' for invoice'.$_REQUEST['idx']);
		 //$generalLedger->setreference();
		 $generalLedger->setamount($_REQUEST['amount']);
		 $generalLedger->settransactionType('Dr');
		 $generalLedger->setisActive(true);
		 $generalLedger->save();

	 }

	 		echo json_encode(array('success'=>true));
 		}else if($controller_templet->getAction()=='supplier')
 		{

 		}

 } ?>