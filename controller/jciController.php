<?php 
 require_once('../services/jciService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $jci=new jciService();
 		 echo json_encode( $jci->view());
 }	 else if($controller_templet->getAction()=='add'){
	 	 $jci=new jciService();
 	 	 $jci->setdetails($_REQUEST['details']);
	 	 $jci->settimeTaken($_REQUEST['timeTaken']);
	 	 $jci->setinitials($_REQUEST['initials']);
	 	 $jci->setjobcardId($_REQUEST['jobcardId']);
	 	 $jci->setrepairReview($_REQUEST['repairReview']);
	 	 echo json_encode( $jci->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $jci=new jciService();
 	 	 $jci->setid($_REQUEST['id']);
	 	 $jci->setdetails($_REQUEST['details']);
	 	 $jci->settimeTaken($_REQUEST['timeTaken']);
	 	 $jci->setinitials($_REQUEST['initials']);
	 	 $jci->setjobcardId($_REQUEST['jobcardId']);
	 	 $jci->setrepairReview($_REQUEST['repairReview']);
	 	 echo json_encode( $jci->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $jci=new jciService();
 	 	 $jci->setid($_REQUEST['id']);
echo json_encode( $jci->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $jci=new jciService();
 		 echo json_encode( $jci->viewConbox());
		}   
} ?>