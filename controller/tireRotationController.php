<?php 
 require_once('../services/tireRotationService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $tireRotation=new tireRotationService();
 echo json_encode( $tireRotation->view()); }else if($controller_templet->getAction()=='add'){
	 	 $tireRotation=new tireRotationService();
 	 	 $tireRotation->settireId($_REQUEST['tireId']);
	 	 $tireRotation->setfromPosition($_REQUEST['fromPosition']);
	 	 $tireRotation->settoPosition($_REQUEST['toPosition']);
	 	 $tireRotation->settireHoldId($_REQUEST['tireHoldId']);
	 	 $tireRotation->setkmsDone($_REQUEST['kmsDone']);
	 	 $tireRotation->setinspectionDetailsId($_REQUEST['inspectionDetailsId']);
	 	 $tireRotation->setCreationDate($_REQUEST['CreationDate']);
	 	 echo json_encode( $tireRotation->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $tireRotation=new tireRotationService();
 	 	 $tireRotation->setid($_REQUEST['id']);
	 	 $tireRotation->settireId($_REQUEST['tireId']);
	 	 $tireRotation->setfromPosition($_REQUEST['fromPosition']);
	 	 $tireRotation->settoPosition($_REQUEST['toPosition']);
	 	 $tireRotation->settireHoldId($_REQUEST['tireHoldId']);
	 	 $tireRotation->setkmsDone($_REQUEST['kmsDone']);
	 	 $tireRotation->setinspectionDetailsId($_REQUEST['inspectionDetailsId']);
	 	 $tireRotation->setCreationDate($_REQUEST['CreationDate']);
	 	 echo json_encode( $tireRotation->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $tireRotation=new tireRotationService();
 	 	 $tireRotation->setid($_REQUEST['id']);
echo json_encode( $tireRotation->delete()); } 
} ?>