<?php 
 require_once('../services/subServiceService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $subService=new subServiceService();
		 if(isset($_REQUEST['serviceHeaderId'])){
			 $subService->setserviceHeaderId($_REQUEST['serviceHeaderId']);
			 echo json_encode( $subService->viewHeader()); 
		 }else{
 			echo json_encode( $subService->view()); 
		 }
 
 }else if($controller_templet->getAction()=='add')
 {
	 	 $subService=new subServiceService();

 	 	 $subService->setcomponentScheduleId($_REQUEST['componentScheduleId']);
	 	 $subService->setserviceHeaderId($_REQUEST['serviceHeaderId']);
	 	 $subService->setcomment($_REQUEST['comment']);
	 	 $subService->setdone($_REQUEST['done']);
	 	 echo json_encode( $subService->save());
 }
 else if($controller_templet->getAction()=='edit')
 {
 	 	 $subService=new subServiceService();
 	 	 $subService->setid($_REQUEST['idx']);
 	 	 $subService->setserviceHeaderId($_REQUEST['id']);
	 	 //$subService->setcomponentScheduleId($_REQUEST['componentScheduleId']);
	 	    //$subService->setserviceHeaderId($_REQUEST['serviceHeaderId']);
	 	 $subService->setcomment($_REQUEST['comment']);
		 $done=$_REQUEST['done']=="Yes"?"Action Performed":"Not Performed";
	 	 $subService->setdone($done);
	 	 echo json_encode($subService->update());
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $subService=new subServiceService();
 	 	 $subService->setid($_REQUEST['id']);
		echo json_encode( $subService->delete()); 
 }
} 

?>