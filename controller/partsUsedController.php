<?php
 ob_start();
// session_start();
require_once '../services/partsUsedService.php';
require_once'../services/jobCardService.php';
require_once'../services/sparePartService.php';
require_once'../services/vehicleSystemService.php';
require_once'../services/partRequestFormService.php';
require_once'../services/partRequestFormItemService.php';
require_once'../services/processhistoryService.php';
require_once'../services/storeprocesService.php';
require_once'controller.php';
require_once'../services/processhistoryService.php';

if(isset($_REQUEST['action'])) {
    $controller_templet = new Controller($_REQUEST['action']);
    if ($controller_templet->getAction() == "view") {
        $partsUsed = new PartsUsedService();
        if (isset($_REQUEST['jobCardId'])) {
            $partsUsed->setjobcardId($_REQUEST['jobCardId']);
            echo json_encode($partsUsed->viewJson());
        } else {
            echo json_encode($partsUsed->view());
        }
    } else if ($controller_templet->getAction() == "add") {
        //----------------------------------------------------------------------------------
        //find the jobcard
        //----------------------------------------------------------------------------------
        $jobCard = new jobCardService();
        $jobCard->setid($_REQUEST['jobCardId']);
        $jobCard->find();
        //JY20171102
        /*$sparePart=new sparePartService();
        $sparePart->setid($_REQUEST['partUsedId']);
        $sparePart->find();*/
        //JY20171102
        /*$vehicleSystem=new vehicleSystemService();
        $vehicleSystem->setid($sparePart->getsystemId());
        $vehicleSystem->find();*/
        //---------------------------------------------------------------------------------
        //saving the sparepart.(To the used part table)
        //=================================================================================
        $partsUsed = new PartsUsedService();
        $partsUsed->setjobcardId($_REQUEST['jobCardId']);
        //$partsUsed->setjobcardId($_REQUEST['id']);
        $partsUsed->setquantity($_REQUEST['quantity']);
        //$partsUsed->setcost($_REQUEST['cost']);
        $partsUsed->setpartused($_REQUEST['partUsedId']);

        $partsUsed->setDateOfRepair($jobCard->getcreationDate());
        //$partsUsed->gettypeofRepair($vehicleSystem->getsystemName());
        $partsUsed->setnumberPlate($jobCard->getvehicleId());
        //$partsUsed->setsupplierId($_REQUEST['supplierId']);
        $partsUsed->setsupplied(false);
        $lastId=null;
        $lastId=$partsUsed->save();

        $partsUsed->setid($lastId);
        // get the partsUsed id
        //=================================================================================
        //CREATE Part request.
        //=================================================================================
        $partRequest = new partRequestFormService();
        $partRequest->setjobcardId($_REQUEST['jobCardId']);
        $partRequest->findWithJobcard();
        if (is_null($partRequest->getid())) {
            //save
            $partRequest->setsection('WORKSHOP');
            $partRequest->setvehicleId($jobCard->getvehicleId());
            $partRequest->setrequestedBy($jobCard->getdriverName());
            $partRequest->setrequestDate(date('Y-m-d'));
            $partRequest->setstatus("Review");
            $partRequest->setjobcardId($_REQUEST['jobCardId']);
            //
            $partRequest->save();
            $partRequest->findWithJobcard();

            //save the item
            $partRequestItem = new partRequestFormItemService();
            $partRequestItem->setitemId($partsUsed->getid());
            $partRequestItem->setuomId($_REQUEST['uomId']);
            $partRequestItem->setquantity($_REQUEST['quantity']);
            $partRequestItem->setstatus('NOT ISSUED');
            $partRequestItem->setprfId($partRequest->getid());
            $partRequestItem->save();


        } else {
            //save the item
            $partRequestItem = new partRequestFormItemService();
            $partRequestItem->setitemId($partsUsed->getid());
            $partRequestItem->setuomId($_REQUEST['uomId']);
            $partRequestItem->setquantity($_REQUEST['quantity']);
            $partRequestItem->setstatus('NOT ISSUED');
            $partRequestItem->setprfId($partRequest->getid());
            $partRequestItem->save();

        }
        echo json_encode(array('success' => true,'partRequest'=>$partRequest->getid()));


        //$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$partsUsed->getjobcardId());
        //$controller_templet->redirect();
    } else if ($controller_templet->getAction() == "edit") {
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // the message variable
        $success_count=0;
        $failureCount=0;
        $resultArray=array();
        $errorMsg=null;
        //==============================================================
        $jobCard = new jobCardService();
        $jobCard->setid($_REQUEST['jobCardId']);
        $jobCard->find();
        //================================================
        //$sparePart = new sparePartService();
        //$sparePart->setid($_REQUEST['partUsedId']);
        //$sparePart->find();

        //$vehicleSystem = new vehicleSystemService();
        //$vehicleSystem->setid($sparePart->getsystemId());
        //$vehicleSystem->find();
        //===============================================
        //Edit the partsused.
        //===============================================
        $partsUsed = new PartsUsedService();
        $partsUsed->setid($_REQUEST['id']);
        $partsUsed->setjobcardId($_REQUEST['jobCardId']);
        //$partsUsed->setjobcardId($_REQUEST['id']);
        $partsUsed->setquantity($_REQUEST['quantity']);
        //$partsUsed->setcost($_REQUEST['cost']);
        $partsUsed->setpartused($_REQUEST['partUsedId']);

        $partsUsed->setDateOfRepair($jobCard->getcreationDate());
        //$partsUsed->gettypeofRepair($vehicleSystem->getsystemName());
        $partsUsed->setnumberPlate($jobCard->getvehicleId());
        //$partsUsed->setsupplierId($_REQUEST['supplierId']);
        //$partsUsed->setsupplied(true);
        //echo json_encode($partsUsed->save());
        $resultArray=$partsUsed->update();
        $controller_templet->returnResult($success_count,$failureCount,$resultArray,$errorMsg);
        //===================================================
        //Edit the the prf-item
        //===================================================
        //++++++++++++++++++++++++++++++++++++++++++++++++++
        //Paart Request is already know there is no need to redo it.
//        $partRequest = new partRequestFormService();
//        $partRequest->setjobcardId($_REQUEST['jobCardId']);
//        $partRequest->findWithJobcard();
        //++++___________________++++=========================
        //Update the item
        $partRequestItem = new partRequestFormItemService();
        $partRequestItem->setid($_REQUEST['prfitemId']);
        //$partRequestItem->setitemId($_REQUEST['partUsedId']);
        $partRequestItem->setuomId($_REQUEST['uomId']);
        $partRequestItem->setquantity($_REQUEST['quantity']);
        $partRequestItem->setstatus('NOT ISSUED');
        //$partRequestItem->setprfId($partRequest->getid());
        $resultArray=array();
        $resultArray=$partRequestItem->update();
        $controller_templet->returnResult($success_count,$failureCount,$resultArray,$errorMsg);
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //THE FINAL
        if($failureCount==0)
        {
            echo json_encode(array('success' => true));
        }else{
            echo json_encode(array('msg' => $errorMsg));
        }



        //echo json_encode(array('success' => true));

        //$controller_templet->setUrlHeader('../admin.php?view=add_jobcard_items&id='.$partsUsed->getjobcardId());
        //$controller_templet->redirect();
    } else if ($controller_templet->getAction()=="sendtostore"){

        $result=array();
        $success=0;
        $failure=0;
        $errorMsg='';

        //process
        $processHistory=new processhistoryService();
        $processHistory->startProcess('Store Managment');

        //storeProcess
        $storeProcess=new storeprocesService();
        $storeProcess->setprfId($_REQUEST['prfId']);
        $storeProcess->setprocessHistoryId($processHistory->getid());
        $result=$storeProcess->save();
        if(array_key_exists('success',$result))
        {
            $success=$success+1;
        }else
        {
            $failure=$failure+1;
            $errorMsg.=" ".$result['msg'];
        }
        $result=array();
        //Part request form
        $prf= new partRequestFormService();
        $prf->setid($_REQUEST['prfId']);
        $prf->find();

        $jobCard =new jobCardService();
        $jobCard->setid($prf->getjobcardId());
        $jobCard->setapprovedDate(date('y-m-d'));
        $jobCard->setapprovedBy($_SESSION['username']);
        $result=$jobCard->update();
        if(array_key_exists('success',$result))
        {
            $success=$success+1;
        }else
        {
            $failure=$failure+1;
            $errorMsg.=" ".$result['msg'];
        }
        $result=array();

        if($failure==0)
        {
            echo json_encode(array('success'=>'true'));

        }else
        {
            echo json_encode(array('msg'=>$errorMsg));
        }



        //if it exist
	    }else if($controller_templet->getAction()=="delete")
	{
		$partsUsed=new PartsUsedService();
		$partsUsed->setid($_REQUEST['id']);
        echo json_encode($partsUsed->delete());
        
    }else if($controller_templet->getAction()=="loadParts")
	{
		$partsUsed=new PartsUsedService();
		$partsUsed->setjobcardId($_REQUEST['id']);
		$total=0;
		$sql="select * from tblpartsused where jobcardId='".$partsUsed->getjobcardId()."'";
		
			echo "<h3>Parts Used</h3>";
			echo "<form action='controller/partsUsedController.php' name='jobCardItem'><div class='table-responsive'><table class='table table-striped table-bordered'>";
			
			echo "<thead><tr><th>Part Used</th><th>Quantity</th><th>Cost</th><th colspan='2'>#</th></tr><thead<tbody>";
			foreach($partsUsed->view_query($sql) as $row2)
			{
				$total=$total+$row2['cost'];
				echo "<tr><td>".$row2['partused']."</td><td>".$row2['quantity']."</td><td>".number_format($row2['cost'],0)."</td><td><a href='#' onclick='editItem(this.id)' id='".$row2['id']."'>Edit</a></td><td><a href='controller/partUsedController.php?action=Delete&id=".$row2['id']."&jobcardId=".$row2['jobcardId']."'  >Delete</a></td></tr>";	
			}
			echo "<tr class='info'><th colspan='2'>Total</th><th colspan='3'>".number_format($total,0)."</th></tr>";
			echo"<tr><td><textarea name='partUsed' id='partUsed' style='width:100%; height:70px;' ></textarea></td><td><input type='text' name='quantity' id='quatity'  style='width:100%;' /></td><td><input type='text' name='cost' id='cost' value='' style='width:100%;'/><input type='hidden' name='jobcardId' id='jobcardId2' value='".$partsUsed->getjobcardId()."'/><input type='hidden' name='id' id='id2' value=''/></td><td colspan='2'><input type ='submit' name='action' id='action' value='add' class='btn btn-primary' style='width:100%;' /></td></tr>";
			echo"</tbody></table></div></form>";
		
	}else if($controller_templet->getAction()=="loadParts2")
	{
		$partsUsed=new PartsUsedService();
		$partsUsed->setjobcardId($_REQUEST['id']);
		$total=0;
		$sql="select (case when p.jobcardId is NULL THEN (select v.regNo from tblvehicle v where v.id=p.numberplate )ELSE (select v.regNo from tbljobcard jc inner join tblvehicle v on jc.vehicleId=v.id where jc.id=p.jobcardId  ) END )regNo,p.partUsedId,sp.partName,sp.partNumber,p.quantity,p.cost,p.jobcardId,p.id,p.dateofrepair,p.typeofrepair,p.numberplate,spl.supplierName,p.supplierId,p.supplied,(p.quantity*p.cost) total from tblpartsused p inner join sparepart sp on p.partUsedId=sp.id inner join tblsupplier spl on p.supplierId=spl.supplierCode where jobcardId='".$partsUsed->getjobcardId()."'";
		
			echo "<h3>Parts Used</h3>";
			echo "<form action='controller/partsUsedController.php' name='jobCardItem'><div class='table-responsive'><table class='table table-striped table-bordered'>";
			
			echo "<thead><tr><th>MATERIAL/SPAREPARTS</th><th>SUPPLIER</th><th>PART NO</th><th>QTY</th><th>COST</th><th>AMOUNT</th><th colspan='2'>#</th></tr><thead<tbody>";
			foreach($partsUsed->view_query($sql) as $row2)
			{
				$total=$total+$row2['cost'];
				echo "<tr><td>".$row2['partName']."</td><td>".$row2['supplierName']."</td><td>".$row2['partNumber']."</td><td>".$row2['quantity']."</td><td>".number_format($row2['cost'],0)."</td><td>".($row2['quantity']*$row2['cost'])."</td><td><a href='#' onclick='editItem(this.id)' id='".$row2['id']."'>Edit</a></td><td><a href='controller/partUsedController.php?action=Delete&id=".$row2['id']."&jobcardId=".$row2['jobcardId']."'  >Delete</a></td></tr>";
			}
			echo "<tr class='info'><th colspan='2'>Total</th><th colspan='3'>".number_format($total,0)."</th></tr>";
			//echo"<tr><td><textarea name='partUsed' id='partUsed' style='width:100%; height:70px;' ></textarea></td><td><input type='text' name='quantity' id='quatity'  style='width:100%;' /></td><td><input type='text' name='cost' id='cost' value='' style='width:100%;'/><input type='hidden' name='jobcardId' id='jobcardId2' value='".$partsUsed->getjobcardId()."'/><input type='hidden' name='id' id='id2' value=''/></td><td colspan='2'><input type ='submit' name='action' id='action' value='add' class='btn btn-primary' style='width:100%;' /></td></tr>";
			echo"</tbody></table></div></form>";
		
	}else if ($controller_templet->getAction()=="getprfid")
    {
        $partRequest = new partRequestFormService();
        $partRequest->setjobcardId($_REQUEST['jobCardId']);
        $partRequest->findWithJobcard();
        echo $partRequest->getid();
    }
}
?>