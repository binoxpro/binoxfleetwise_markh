<!--<p style="font-family:Calibri;">-->
<?php
require_once '../services/trainingComplianceService.php';
require_once '../services/vehicleService.php';
	
$html = '
<html>
<head>
<style>
body {font-family: Tahoma;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #001C37;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: left;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
.heading td{
	background-color:#FFB56A;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 12pt;">FRED SEBYALA TRANSPORTERS LIMITED</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
<table width="100%"><tr >
<td width="50%" style="color:#0000BB; " align="center"><span style="font-weight: bold; font-size: 10pt;">'.'Driving Licence Report'.'</span><br /><br /><br /><br /><span style="font-family:dejavusanscondensed;"></span> </td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
FST &copy;2015 Page {PAGENO} of {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td width="15%">Driver Name</td>
<td width="10%">Course</td>
<td width="25%">Issue Date</td>
<td width="20%">Expiry Date</td>
<td width="20%">Status</td>

</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
';

//<tr>
//<td align="center">MF1234567</td>
//<td align="center">10</td>
//<td>Large pack Hoover bags</td>
//<td class="cost">&pound;2.56</td>
//</tr>
$training=new TrainingComplianceService() ;
$html2="";

		foreach ($training->viewJson() as $row2)
		{
			
			if($row2['name']==""){
				$html2.= "<tr class='heading'><td class='heading'>".$row2["name"]."</td><td class='heading'>".$row2['course']."</td><td class='heading'>".$row2['issueDate']."</td><td class='heading'>".$row2['expiryDate']."</td><td class='heading'>".$row2['status']."</td></tr>";
			}else{
					$html2.= "<tr><td class='totals'>".$row2["name"]."</td><td class='totals'>".$row2['course']."</td><td class='totals'>".$row2['issueDate']."</td><td class='totals'>".$row2['expiryDate']."</td><td class='totals'>".$row2['status']."</td></tr>";
					//$nox=0;
			}
			
		}


$html.=$html2.'<!-- END ITEMS HERE -->
</tbody>
</table>



</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../lib/mpdf60/');
include("../lib/mpdf60/mpdf.php");

$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("fred sebyala transporter limited");
$mpdf->SetAuthor("code360 data solution");
$mpdf->SetWatermarkText("FST");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.2;
$mpdf->SetDisplayMode('fullpage');



$mpdf->WriteHTML($html);


$mpdf->Output(); exit;

exit;

?>