 <?php
 require_once('../services/tireHoldService.php');
 require_once('../services/tireService.php');
 require_once('../services/tireIusseService.php');
 require_once('../services/tireMaintenanceService.php');
 require_once('../services/tireRotationService.php');
 require_once '../services/tyreService.php';
 require_once('../services/tireTreadDepthService.php');
 set_time_limit(0);
 $tyre=new TyreService();
 $odometer=0;
 foreach($tyre->view() as $row)
 {
    $tireTest=new tireService();
    $serialNumber=$row['serialNumber'];
    if($tireTest->tireExistance($serialNumber)){
        
    }else{
    $vehilceId=$row['numberPlate'];
    $positionId=$row['positionId'];
    $brand=$row['brand'];
    $serialNumber=$row['serialNumber'];
    $cost=$row['cost'];
    $fixedAs=$row['fixedAs'];
    $odometer=$row['Odometer'];
    $expectedKm=$row['expectedKm'];
    $currentReading=$row['currentReading'];
    $remaining=$row['remaining'];
    $fixedDate=$row['fixingDate'];
    $removalMileage=$row['removalMileage'];
    $comment=$row['comment'];
    $treadDepth=!is_null($fixedAs)?$fixedAs=='NEW'?20:15:15;
    $tireId=-1;
     //tire purchase
     $tire=new tireService();
     $tire->setserialNumber($serialNumber);
     $tire->settireBand($brand);
     $tire->settirePatternId(1);
     $tire->settireSizeId(1);
     $tire->setretreadDepth($treadDepth);
     $tire->setmaxmiumPressure(120);
     $tire->settireType($fixedAs);
     $tire->setcost($cost);
     $tire->setkms($expectedKm);
     $tire->setstatus('Instock');
     $tire->setisActive(true);
     $tireId=$tire->save();
     //Tread Depth
     $treadDepthObj=new tireTreadDepthService();
     $treadDepthObj->settireId($tireId);
     $treadDepthObj->settreadDepth($treadDepth);
     $treadDepthObj->setCreationDate($fixedDate);
     $treadDepthObj->save();
     //tire hold
     $tireHold=new tireHoldService();
     $tireHold->setvehicleId($vehilceId);
     $tireHold->setpositionId($positionId);
     $tireHold->settireId($tireId);
     $tireHold->setfixingDate($fixedDate);
     $tireHold->setcreationDate($fixedDate);
     $tireHold->setfixingOdometer($odometer);
     $tireHold->setexpectedKms($expectedKm);
     $tireHold->setremovalKms($removalMileage);
     $tireHold->setactualKms($removalMileage);
     $tireHold->setcomment($comment);
     $tireHold->setisActive(true);
     $tireHold->save();
     //tire tirelusse
     $tireIusse=new tireIusseService();
     $tireIusse->settireId($tireId);
     $tireIusse->setcreationDate($fixedDate);
     $tireIusse->setisActive(true);
     $tireIusse->save();
     //tire update 
     $tire2=new tireService();
     $tire2->setid($tireId);
     $tire2->getObject();
     $tire2->setstatus('Fitted');
     $tire2->update();
    }
    
 }
 
 ?>