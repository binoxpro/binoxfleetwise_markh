<?php 
 require_once('../services/newOrderService.php');
 require_once('../services/tripTypeService.php');
 require_once('../services/tripNumberManagerService.php');
 require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
 if($controller_templet->getAction()=='view'){
	 	 $newOrder=new newOrderService();
         if(isset($_REQUEST['show']))
         {
            if($_REQUEST['show']=="delivery")
            {
                echo json_encode( $newOrder->view2()); 
            }else if($_REQUEST['show']=="all")
            {
               echo json_encode( $newOrder->view3());  
            }
         }else
         {
            echo json_encode( $newOrder->view()); 
         }
 }else if($controller_templet->getAction()=='add'){
	 	 $newOrder=new newOrderService();
 	 	 $newOrder->setcustomerName($_REQUEST['customerName']);
	 	 $newOrder->setorderDate($_REQUEST['orderDate']);
	 	 $newOrder->setloadDate($_REQUEST['loadDate']);
	 	 $newOrder->setorderNo($_REQUEST['orderNo']);
	 	 $newOrder->setulg($_REQUEST['ulg']);
	 	 $newOrder->setago($_REQUEST['ago']);
	 	 $newOrder->setbik($_REQUEST['bik']);
	 	 $newOrder->setvpower($_REQUEST['vpower']);
	 	 $newOrder->setdepotTimeIn($_REQUEST['depotTimeIn']);
	 	 $newOrder->setdepotTimeOut($_REQUEST['depotTimeOut']);
	 	 $newOrder->setvehicleId($_REQUEST['vehicleId']);
	 	 $newOrder->setdeliveryDate(null);
	 	 $newOrder->setdeliveryTime(null);
         $tripType=new tripTypeService();
         $tripType->setid($_REQUEST['tripType']);
         $tripType->getObject();
         
         $tripNumberManager=new tripNumberManagerService();
         $tripNumberManager->settripTypeId($_REQUEST['tripType']);
         $tripId=$tripNumberManager->getLastTripNo($tripType->getName());
         
	 	 $newOrder->settripNo($tripId);
	 	 //$newOrder->setRevenue($_REQUEST['Revenue']);
	 	 //$newOrder->setExpense($_REQUEST['Expense']);
	 	 //$newOrder->settotalProduct($_REQUEST['totalProduct']);
	 	 $newOrder->setsystDate(date('Y-m-d'));
	 	 $newOrder->setisActive(true);
	 	 $orderId=$newOrder->save();
         
         //update number;
         $tripNumberManager2=new tripNumberManagerService();
         $tripNumberManager2->setid($tripId);
         $tripNumberManager2->getObject();
         $tripNumberManager2->setOrderNo($orderId);
         echo json_encode($tripNumberManager2->update());
         
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $newOrder=new newOrderService();
 	 	 $newOrder->setid($_REQUEST['id']);
	 	 $newOrder->setcustomerName($_REQUEST['customerName']);
	 	 $newOrder->setorderDate($_REQUEST['orderDate']);
	 	 $newOrder->setloadDate($_REQUEST['loadDate']);
	 	 $newOrder->setorderNo($_REQUEST['orderNo']);
	 	 $newOrder->setulg($_REQUEST['ulg']);
	 	 $newOrder->setago($_REQUEST['ago']);
	 	 $newOrder->setbik($_REQUEST['bik']);
	 	 $newOrder->setvpower($_REQUEST['vpower']);
	 	 $newOrder->setdepotTimeIn($_REQUEST['depotTimeIn']);
	 	 $newOrder->setdepotTimeOut($_REQUEST['depotTimeOut']);
	 	 $newOrder->setvehicleId($_REQUEST['vehicleId']);
	 	 $newOrder->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $newOrder->setdeliveryTime($_REQUEST['deliveryTime']);
	 	 $newOrder->settripNo($_REQUEST['tripNo']);
	 	 //$newOrder->setRevenue($_REQUEST['Revenue']);
	 	 //$newOrder->setExpense($_REQUEST['Expense']);
	 	 //$newOrder->settotalProduct($_REQUEST['totalProduct']);
	 	 //$newOrder->setsystDate($_REQUEST['systDate']);
	 	 //$newOrder->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $newOrder->update()); 
 }else if($controller_templet->getAction()=='edit2'){
 	 	 $newOrder=new newOrderService();
 	 	 $newOrder->setid($_REQUEST['id']);
	 	 //$newOrder->setcustomerName($_REQUEST['customerName']);
	 	 //$newOrder->setorderDate($_REQUEST['orderDate']);
	 	 //$newOrder->setloadDate($_REQUEST['loadDate']);
	 	 //$newOrder->setorderNo($_REQUEST['orderNo']);
	 	 //$newOrder->setulg($_REQUEST['ulg']);
	 	 //$newOrder->setago($_REQUEST['ago']);
	 	 //$newOrder->setbik($_REQUEST['bik']);
	 	 //$newOrder->setvpower($_REQUEST['vpower']);
	 	 //$newOrder->setdepotTimeIn($_REQUEST['depotTimeIn']);
	 	 //$newOrder->setdepotTimeOut($_REQUEST['depotTimeOut']);
	 	 //$newOrder->setvehicleId($_REQUEST['vehicleId']);
	 	 $newOrder->setdeliveryDate($_REQUEST['deliveryDate']);
	 	 $newOrder->setdeliveryTime($_REQUEST['deliveryTime']);
	 	 //$newOrder->settripNo($_REQUEST['tripNo']);
	 	 $newOrder->setRevenue($_REQUEST['Revenue']);
	 	 $newOrder->setExpense($_REQUEST['Expense']);
	 	 //$newOrder->settotalProduct($_REQUEST['totalProduct']);
	 	 //$newOrder->setsystDate($_REQUEST['systDate']);
	 	 //$newOrder->setisActive($_REQUEST['isActive']);
	 	 echo json_encode( $newOrder->update()); 
 }
 else if($controller_templet->getAction()=='delete')
 {
 	 	 $newOrder=new newOrderService();
 	 	 $newOrder->setid($_REQUEST['id']);
echo json_encode( $newOrder->delete()); 
} 
} ?>