<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once '../services/tyreService.php';
require_once '../services/stockService.php';
require_once 'controller.php';

if(isset($_REQUEST['action'])){
    $controller_templet=new Controller($_REQUEST['action']);
    if($controller_templet->getAction()=="add"){
		$tyreService= new TyreService();
		$tyreService->setNumberPlate($_REQUEST['numberPlate']);
		$tyreService->settyrePosition($_REQUEST['tyreDetails']);
		$tyreService->setbrand($_REQUEST['brand']);
		$tyreService->setfixingDate($_REQUEST['fixingDate']);
		//$stock=new stockService();
		//$stock->setid($_REQUEST['serialNumber']);
		//$stock->getObject();
		$stockStatus=$_REQUEST['fixedAs']=="NEW"?'inUse':'inUse/Retread';
		$retreadStatus=$_REQUEST['fixedAs']=="NEW"?false:true;
		//$stock->setstatus($stockStatus);
		//$stock->setretreadstatus($retreadStatus);
		//$stock->getserialNumber();
		$tyreService->setserialNumber($_REQUEST['serialNumber']);
		$tyreService->setodometer($_REQUEST['Odometer']);
		$tyreService->setexpectedKm($_REQUEST['expectedKm']);
		$tyreService->setcurrentMileage($_REQUEST['currentReading']);
		$tyreService->setremovalMileage($_REQUEST['removalMileage']);
		$tyreService->setcomment($_REQUEST['comment']);
		$tyreService->setCost($_REQUEST['cost']);
		$tyreService->setFixedAs($_REQUEST['fixedAs']);
		//$tyreService->setStockId($_REQUEST['serialNumber']);
		//$stock->update();
		$stock=new stockService();
		$stock->setbrand($_REQUEST['brand']);
		$stock->setcost($_REQUEST['cost']);
		$stock->setretreadstatus($retreadStatus);
		$stock->setstatus($stockStatus);
		$stock->setserialNumber($_REQUEST['serialNumber']);
		$stock->settreadDepth($_REQUEST['treadDepth']);
		$stock->setcreationDate($_REQUEST['fixingDate']);
		$idValue=$stock->saveNew();
		$tyreService->setStockId($idValue);
		//$stock->set
		echo json_encode($tyreService->save());
	}else  if($controller_templet->getAction()=="updateExtension"){
		$tyreService=new TyreService();
		$oldMileage=$_REQUEST['rm'];
		$extenison=$_REQUEST['em'];
		$newMileage=$oldMileage+$extenison;
		$tyreService->setremovalMileage($newMileage);
		$tyreService->setId($_REQUEST['id']);
		echo json_encode($tyreService->updateExtension());
		
	}else  if($controller_templet->getAction()=="view"){
		if(isset($_REQUEST['numberPlate'])){
		$tyreService=new TyreService();
		$tyreService->setNumberPlate($_REQUEST['numberPlate']);
		$sql="select ty.Id,ty.stockId,(select sk.treadDepth from tblstock sk where sk.id=ty.stockId) treadDepth,v.regNo, tt.tyretypeName position, ty.brand,ty.serialNumber, ty.cost , ty.fixedAs,ty.fixingDate, ty.Odometer,ty.expectedKm, od.reading currentReading,case when (ty.removalMileage-od.reading)>0 then (ty.removalMileage-od.reading)  else 0 end remaining ,round((ty.cost/ty.expectedKm),2) costperkm,ty.removalMileage,ty.comment,ty.numberPlate,ty.tyrePosition tyreDetails,case when (ty.removalMileage-od.reading)>0 then (od.reading-ty.Odometer)  else ty.expectedKm end kmsDone,FORMAT(round(((ty.cost/ty.expectedKm)*(case when (ty.removalMileage-od.reading)>0 then (od.reading-ty.Odometer)  else ty.expectedKm end)),2),0) currentCost from tbl_tyre ty inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId   where ty.numberPlate='".$tyreService->getNumberPlate()."' and ty.isActive =1 order by ty.numberPlate asc ";
		echo json_encode($tyreService->view_query($sql));
		}else{
		$tyreService=new TyreService();
		echo json_encode($tyreService->view());
		}
		
	}else  if($controller_templet->getAction()=="getMileage"){
		if(isset($_REQUEST['id'])){
		$tyreService=new TyreService();
		$tyreService->setNumberPlate($_REQUEST['id']);
		$tyreService->setCurrentReadingOn();
		echo $tyreService->getcurrentMileage();
		}
		
	}else  if($controller_templet->getAction()=="exportExcel"){
		if(isset($_REQUEST['numberPlate'])){
		$tyreService=new TyreService();
		$tyreService->setNumberPlate($_REQUEST['numberPlate']);
		$sql="select ty.Id,v.regNo, tt.tyretypeName position, ty.brand,ty.serialNumber, ty.cost , ty.fixedAs,ty.fixingDate, ty.Odometer,ty.expectedKm, 	od.reading currentReading, ty.removalMileage,ty.comment  from tbl_tyre ty inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId  where ty.numberPlate='".$tyreService->getNumberPlate()."' order by ty.numberPlate asc ";
		echo json_encode($tyreService->view_query($sql));
		}else{
		$tyreService=new TyreService();
		echo json_encode($tyreService->view());
		}
	}else  if($controller_templet->getAction()=="tryeDashBoard"){
		$send=false;
		$msg='<html><head><link href="http://code360ds.com/fst100/css/bootstrap.min.css" rel="stylesheet"><link href="http://code360ds.com/fst100/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet"><link href="http://code360ds.com/fst100/css/sb-admin-2.css" rel="stylesheet"><link href="http://code360ds.com/fst100/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"><link rel="stylesheet" type="text/css" href="http://code360ds.com/fst100/lib/external/jquery/themes/bootstrap/easyui.css" /><link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/icon.css" /><script src="http://code360ds.com/fst100/js/jquery.js"></script><script src="http://code360ds.com/fst100/js/bootstrap.min.js"></script><script src="http://code360ds.com/fst100/js/plugins/metisMenu/metisMenu.min.js"></script><script src="http://code360ds.com/fst100/js/sb-admin-2.js"></script>
   </head><body>';
		$tyreService=new TyreService();
		//$tyreService->setNumberPlate($_REQUEST['numberPlate']);
		$sql="select v.regNo, tt.tyretypeName position, ty.brand,ty.serialNumber, ty.cost , ty.fixedAs,ty.fixingDate, ty.Odometer,ty.expectedKm, 	od.reading currentReading, ty.removalMileage,ty.comment  from tbl_tyre ty inner join tblvehicle v on ty.numberPlate=v.id inner join tbltyretype tt on ty.tyrePosition=tt.id inner join tblodometer od on ty.numberPlate=od.vehicleId  where isActive='1' order by ty.numberPlate asc ";
		echo "<h3 >Tire notification Report</h3><p>Please the replace the tires</p><table class='table table-striped table-hovered table-bordered'><tr><td>Vehicle</td><td>Position</td><td>Status</td></tr>";
		$msg.="<h3 style='color:#096;'>Tire notification Report</h3><p>Dear sir/Madam</p><p>Please  Replace the Tires</p><table border='1' cellpadding='0' cellspacing='0' style='width:100%; font-size:15px;'><tr><td>Vehicle</td><td>Position</td><td>Status</td></tr>";
		foreach($tyreService->view_query($sql) as $row){
		$range=intval($row['removalMileage'])-intval($row['currentReading']);
			if($range<=1000)
			{
				echo"<tr><td>".$row['regNo']."</td><td>".$row['position']."</td><td>Replace the Tire</td></tr>";
				$msg.="<tr><td>".$row['regNo']."</td><td>".$row['position']."</td><td>Replace the Tire</td></tr>";
				$send=true;
			}
		}
		echo"</table>";
		$msg.="</table></body></html>";
		
		
		//echo"</table>";
	}else if($controller_templet->getAction()=="addEmail"){
		$tyreService=new TyreService();
		echo json_encode($tyreService->saveNotification($_REQUEST['name'],$_REQUEST['email'],true));
	}else if($controller_templet->getAction()=="delete"){
		$tyreService=new TyreService();
		$tyreService->setId($_REQUEST['id']);
		echo json_encode($tyreService->delete());
	}else if($controller_templet->getAction()=="update"){
		$tyreService=new TyreService();
		$tyreService->setId($_REQUEST['id']);
		$tyreService->setNumberPlate($_REQUEST['numberPlate']);
		$tyreService->settyrePosition($_REQUEST['tyreDetails']);
		$tyreService->setbrand($_REQUEST['brand']);
		$tyreService->setfixingDate($_REQUEST['fixingDate']);
		$tyreService->setserialNumber($_REQUEST['serialNumber']);
		$tyreService->setodometer($_REQUEST['Odometer']);
		$tyreService->setexpectedKm($_REQUEST['expectedKm']);
		$tyreService->setcurrentMileage($_REQUEST['currentReading']);
		$tyreService->setremovalMileage($_REQUEST['removalMileage']);
		$tyreService->setcomment($_REQUEST['comment']);
		$tyreService->setCost($_REQUEST['cost']);
		$tyreService->setFixedAs($_REQUEST['fixedAs']);
		echo json_encode($tyreService->update());
	}
    
}
?>