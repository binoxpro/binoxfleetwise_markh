<?php 
 require_once('../services/labourPaymentService.php');
require_once('../services/paymentVoucherService.php');
require_once('../services/ledgerHeadService.php');
require_once('../services/generalLedgerService.php');
require_once('../services/paymentVoucherItemService.php');
require_once('../services/chartOfaccountService.php');
require_once('../services/invoiceService.php');
require_once('../services/taxInvoiceService.php');
require_once('../services/debitorService.php');
require_once('../services/partsUsedService.php');
require_once('../services/jobCardService.php');
require_once('../services/jciService.php');
  require_once('controller.php');

 if(isset($_REQUEST['action'])){
 $controller_templet=new Controller($_REQUEST['action']);
	 if($controller_templet->getAction()=='view'){
		 $labourPayment=new labourPaymentService();
 		 echo json_encode( $labourPayment->view());
 }	 else if($controller_templet->getAction()=='add'){
		 $jobcardItem=new jciService();
		 $jobcardItem->setid($_REQUEST['labourId']);
		 $jobcardItem->find();

		 $jobCard=new jobCardService();
		 $jobCard->setid($jobcardItem->getjobcardId());
		 $jobCard->find();


		 $expenseAccount=new chartOfaccountService();
		 $expenseAccount->setAccountName('Garage Labour');
		 $expenseAccount->findAccountCode();

		 $dateArray=explode('-',$_REQUEST['paymentDate']);
		 $total=0;
		 //$total=(floatval($_REQUEST['qty'])*floatval($_REQUEST['unitprice']));
		 $ledgerHead = new ledgerHeadService();
		 $ledgerHead->settransactionDate($_REQUEST['paymentDate']);
		 $ledgerHead->setmonthId($dateArray[1]."/".$dateArray[0]);
		 $ledgerHead->setamount(floatval($_REQUEST['amount']));
		 $ledgerHead->save();

		 //Cr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($_REQUEST['sourceAccount']);
		 $generalLedger->setnarrative('Payment for Labour- '.$_REQUEST['details']);
		 //$generalLedger->setreference();
		 $generalLedger->setamount($_REQUEST['amount']);
		 $generalLedger->settransactionType('Cr');
		 $generalLedger->setisActive(true);
		 //$generalLedger->setindividualNo($_REQUEST['supplierId']);
		 //$generalLedger->setcostCentreId();
		 $generalLedger->save();

		 //Dr
		 $generalLedger = new generalLedgerService();
		 $generalLedger->setledgerHeadId($ledgerHead->getid());
		 $generalLedger->setaccountCode($expenseAccount->getAccountCode());
		 $generalLedger->setnarrative('Payment for Labour- '.$_REQUEST['details']);
		 $generalLedger->setamount($_REQUEST['amount']);
		 $generalLedger->settransactionType('Dr');
		 $generalLedger->setreference($jobCard->getjobNo());
		 $generalLedger->setisActive(true);
		 $generalLedger->setcostCentreId($jobCard->getvehicleId());
		 $generalLedger->save();





		 $labourPayment=new labourPaymentService();
 	 	 $labourPayment->setlabourId($_REQUEST['labourId']);
	 	 $labourPayment->setamount($_REQUEST['amount']);
	 	 $labourPayment->setpaymentDate($_REQUEST['paymentDate']);
	 	 echo json_encode( $labourPayment->save());
 }
 else if($controller_templet->getAction()=='edit'){
 	 	 $labourPayment=new labourPaymentService();
 	 	 $labourPayment->setid($_REQUEST['id']);
	 	 $labourPayment->setlabourId($_REQUEST['labourId']);
	 	 $labourPayment->setamount($_REQUEST['amount']);
	 	 $labourPayment->setpaymentDate($_REQUEST['paymentDate']);
	 	 echo json_encode( $labourPayment->update()); }
 else if($controller_templet->getAction()=='delete'){
 	 	 $labourPayment=new labourPaymentService();
 	 	 $labourPayment->setid($_REQUEST['id']);
echo json_encode( $labourPayment->delete()); 
}else if($controller_templet->getAction()=='viewCombo'){  
		 $labourPayment=new labourPaymentService();
 		 echo json_encode( $labourPayment->viewConbox());
		}   
} ?>