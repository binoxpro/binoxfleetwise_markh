<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Print Preview</title>
	<link rel="shortcut icon" href="image/setting2.png" />
    <!-- Bootstrap Core CSS -->
    <link href="cssOld/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="cssOld/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="cssOld/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 " id="tt">
            
            </div>	
        </div>
    </div>

    <!-- jQuery -->
    <script src="jsOld/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="jsOld/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="jsOld/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="jsOld/sb-admin-2.js"></script>
    <script type="text/javascript" src="lib/js/scriptEngine.js"></script>
<script type="text/javascript">
$(function(){
    var id=qs('id');
    getLastInspection(id);
    
    
})

function getLastInspection(x){
	$.post('controller/tireInspectionHeaderController.php?action=getInspectionDetailsHardCopy',{id:x},function(res){
		$('#tt').html(res);
		window.print();
	});
}

</script>
</body>

</html>
