<?php
require_once('../config/db/universalconnection.php');
require_once('htmlControls.php');
$tableName=$_REQUEST['tablename'];
$className=$_REQUEST['className'];
$db=new UniversalConnection();
$conn=$db->getConn();
$fileName='../model/'.$className.".php";

$fn=fopen($fileName,'w') or die("can't open file");

$stringData="<?php \n require_once('../services/dao.php');\nclass ".$className." extends DAO{\n";

fwrite($fn,$stringData);
$query=$conn->prepare("Describe ".$tableName);
$query->execute();
$table_names=$query->fetchAll(PDO::FETCH_COLUMN);
print_r($table_names);
for($i=0;$i<sizeof($table_names);$i++){
	$stringData2="private $".$table_names[$i].";\n";
	fwrite($fn,$stringData2);
}
$stringData3=" function __construct(){\n parent::__construct();\n}\n";
fwrite($fn,$stringData3);
for($i=0;$i<sizeof($table_names);$i++){
	$stringData2="public function get".$table_names[$i]."(){\n return $"."this->".$table_names[$i].";\n}\n";
	fwrite($fn,$stringData2);
}

for($i=0;$i<sizeof($table_names);$i++){
	$stringData2="public function set".$table_names[$i]."($".$table_names[$i]."){\n  $"."this->".$table_names[$i]."=$".$table_names[$i].";\n}\n";
	fwrite($fn,$stringData2);
}

$stringData4="public function save()\n{\n} \n";
fwrite($fn,$stringData4);
$stringData4="public function update()\n{\n} \n";
fwrite($fn,$stringData4);
$stringData4="public function view()\n{\n} \n";
fwrite($fn,$stringData4);
$stringData4="public function delete()\n{\n} \n";
fwrite($fn,$stringData4);
$stringData4="public function view_query($"."sql)\n{\n} \n";
fwrite($fn,$stringData4);

$stringData4="} \n ?>";
fwrite($fn,$stringData4);

fclose($fn);

$fileName2='../services/'.$className."Service.php";

$fn=fopen($fileName2,'w') or die("can't open file");

$stringData="<?php \n require_once('../model/".$className.".php');\n class ".$className."Service extends ".$className."{\n";

fwrite($fn,$stringData);

//save method
$stringData3="\t \t public function save(){\n";
$stringData3=$stringData3."\t \t $"."builder=new InsertBuilder();\n";
$stringData3=$stringData3."\t \t $"."builder->setTable('".$tableName."');\n";

fwrite($fn,$stringData3);
for($i=0;$i<sizeof($table_names);$i++){
	$stringData2="\t \t $"."builder->addColumnAndData('".$table_names[$i]."',parent::get".$table_names[$i]."());\n \t";
	fwrite($fn,$stringData2);
}

$stringData6="\t \t $"."this->con->setQuery(Director::buildSql($"."builder));\n";
$stringData6=$stringData6."\t \t return $"."this->con->execute_query2($"."builder->getValues());\n \t  }\n \n";
fwrite($fn,$stringData6);

//update method
$stringData3="\t \t public function update(){\n";
$stringData3=$stringData3."\t \t $"."builder=new UpdateBuilder();\n";
$stringData3=$stringData3."\t \t $"."builder->setTable('".$tableName."');\n";

fwrite($fn,$stringData3);
for($i=1;$i<sizeof($table_names);$i++){
	$stringData2="\n \t \t if(!is_null(parent::get".$table_names[$i]."())){\n$"."builder->addColumnAndData('".$table_names[$i]."',parent::get".$table_names[$i]."()); \n}\n";
	fwrite($fn,$stringData2);
}
$stringData7="$"."builder->setCriteria(\"where ".$table_names[0]."='\".parent::get".$table_names[0]."().\"'\");\n";
$stringData7=$stringData7."$"."this->con->setQuery(Director::buildSql($"."builder));\n";
$stringData7=$stringData7."\t \t return $"."this->con->execute_query();\n }\n \n";
fwrite($fn,$stringData7);

//view method
$stringData3="\n \t public function view(){\n";
$stringData3=$stringData3."\t \t $"."sql=\"select * from  ".$tableName."\";\n";


fwrite($fn,$stringData3);
$stringData7="\t return $"."this->con->getResultSet($"."sql);\n \t }\n";
fwrite($fn,$stringData7);


//delete method
$stringData3="\n \t public function delete(){\n";
$stringData3=$stringData3."\t \t $"."builder=new DeleteBuilder;\n";
$stringData3=$stringData3."\t \t $"."builder->setTable('".$tableName."');\n";

fwrite($fn,$stringData3);

$stringData7="\t \t $"."builder->setCriteria(\"where ".$table_names[0]."='\".parent::get".$table_names[0]."().\"'\");\n";
$stringData7=$stringData7."\t \t $"."this->con->setQuery(Director::buildSql($"."builder));\n";
$stringData7=$stringData7."\t \t return $"."this->con->execute_query();\n \t }\n \n";
fwrite($fn,$stringData7);

//view method
//view method
$stringData3="\n \t public function view_query($"."sql){\n";
fwrite($fn,$stringData3);
$stringData7="\t \t return $"."this->con->getResultSet($"."sql);\n \t }\n }\n?>";
fwrite($fn,$stringData7);



fclose($fn);
//controller writing
$fileName2='../controller/'.$className."Controller.php";

$fn=fopen($fileName2,'w') or die("can't open file");
$stringData="<?php \n require_once('../services/".$className."Service.php');\n  require_once('controller.php');\n";

fwrite($fn,$stringData);
$string="\n if(isset($"."_REQUEST['"."action"."'])){\n $"."controller_templet=new Controller($"."_REQUEST['action']);\n if($"."controller_templet->getAction()=='view'){\n";
$string.="\t \t $".$className."=new ".$className."Service();\n echo json_encode( $".$className."->view()); }";
fwrite($fn,$string);
//fwrite($fn,$stringData);
$string2="else if($"."controller_templet->getAction()=='add'){\n";
$string2.="\t \t $".$className."=new ".$className."Service();\n ";
fwrite($fn,$string2);
//set varible
for($i=1;$i<sizeof($table_names);$i++){
	
	$string3="\t \t $".$className."->set".$table_names[$i]."($"."_REQUEST['".$table_names[$i]."']);\n";
	fwrite($fn,$string3);
}
$string4="\t \t echo json_encode( $".$className."->save());\n }\n else if($"."controller_templet->getAction()=='edit'){\n \t \t $".$className."=new ".$className."Service();\n ";
fwrite($fn,$string4);
for($i=0;$i<sizeof($table_names);$i++){
	
	$string3="\t \t $".$className."->set".$table_names[$i]."($"."_REQUEST['".$table_names[$i]."']);\n";
	fwrite($fn,$string3);
}
$string4="\t \t echo json_encode( $".$className."->update()); }\n else if($"."controller_templet->getAction()=='delete'){\n \t \t $".$className."=new ".$className."Service();\n ";
fwrite($fn,$string4);
$string3="\t \t $".$className."->set".$table_names[0]."($"."_REQUEST['".$table_names[0]."']);\n";
	fwrite($fn,$string3);
	$string5="echo json_encode( $".$className."->delete()); } \n} ?>";
	fwrite($fn,$string5);
	fclose($fn);
	//create the view folder
	mkdir("../view/".$className);
	//create the add item
	$fileName2='../view/'.$className.'/add.html';

$fn=fopen($fileName2,'w') or die("can't open file");
	
$string6=HtmlControlGenerator::openDialog($className)."\n";
fwrite($fn,$string6);
for($i=1;$i<sizeof($table_names);$i++){
	
	$string7=HtmlControlGenerator::normalInputBox($table_names[$i])."\n";
	fwrite($fn,$string7);
}
$string8=HtmlControlGenerator::closeDialog($className);
fwrite($fn,$string8);
fclose($fn);
$fileName2='../view/'.$className.'/view.html';
$fn=fopen($fileName2,'w') or die("can't open file");
$string9='<div class="col-lg-12"><table id="dg'.$className.'" title="Manage '.$className.'" class="easyui-datagrid" style="height:700px;" url="controller/'.$className.'Controller.php?action=view" pagination="true" toolbar="#toolbar" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-table"><thead>
        	<tr>';
fwrite($fn,$string9);
for($i=0;$i<sizeof($table_names);$i++){
	
	$string7="<th field='".$table_names[$i]."' width='90'>".$table_names[$i]."</th>";
	fwrite($fn,$string7);
}
$string10="</tr></thead></table><div id='toolbar'>";
$string10.='<a href="#" class="btn btn-primary" onclick="new'.$className.'()"><i class="fa fa-plus-circle"></i>Add</a><a href="#" class="btn btn-link" onclick="edit'.$className.'()"><i class="fa fa-pencil"></i>Edit</a>
        <a href="#" class="btn btn-link" onclick="delete'.$className.'()"><i class="fa fa-times-circle"></i>Delete</a></div></div>';
		fwrite($fn,$string10);
		fclose($fn);
		
		$fileName2='../scripts/'.$className.'.js';
$fn=fopen($fileName2,'w') or die("can't open file");
//write the common javascript function
$string30="\n\n function new".$className."(){\n$"."('#dlg".$className."').dialog('open').dialog('setTitle','Enter ".$className." ');\n$"."('#frm".$className."').form('clear');\n url='controller/".$className."Controller.php?action=add'; \n}";
fwrite($fn,$string30);
$string31="\n\n function edit".$className."(){\n var row=$"."('#dg".$className."').datagrid('getSelected');\n if(row)\n{\n $"."('#dlg".$className."').dialog('open').dialog('setTitle','Edit ".$className."');\n $"."('#frm".$className."').form('load',row); \n url='controller/".$className."Controller.php?action=edit&".$table_names[0]."='+row.".$table_names[0].";\n}\n else{\n$".".messager.show({title:'Warning!',msg:'Please select a item to to edit'});} \n}";
fwrite($fn,$string31);
$string32="\n\n function delete".$className."(){\n var row=$"."('#dg".$className."').datagrid('getSelected');\n if(row)\n{\n$."."post('controller/".$className."Controller.php?action=delete&".$table_names[0]."='+row.".$table_names[0].",{},function(data){ $"."('#dg".$className."').datagrid('reload');});\n}\n else{\n$".".messager.show({title:'Warning!',msg:'Please select a item to to edit'});} \n}";
fwrite($fn,$string32);
$string33="\n\n function save".$className."(){\n saveForm('#frm".$className."',url,'#dlg".$className."','#dg".$className."');\n}";
fwrite($fn,$string33);
fclose($fn);
?>