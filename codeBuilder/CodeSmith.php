<?php


class CodeGenerator
{
    
    private $tableName;
    private $className;
    private $conn;
    //the table columns
    private $table_names; 
    
    public function __construct($tableName,$className,$conn)
    {
        $this->tableName=$tableName;
        $this->className=$className;
        $this->conn=$conn;
        $query=$conn->prepare("Describe ".$this->tableName);
        $query->execute();
        $this->table_names=$query->fetchAll(PDO::FETCH_COLUMN);
        $this->GenerateModel();
        $this->GenerateService();
        $this->GenerateView();
        $this->GenerateScript();
        $this->GenerateController();
    }
    
    private function GenerateModel()
    {
        
        //the file path for the model
                    $fileName='../model/'.$this->className.".php";

                    $fn=fopen($fileName,'w') or die("can't open file");
                    
                    $stringData="<?php \n require_once('../services/dao.php');\nclass ".$this->className." extends DAO{\n";
                    
                    fwrite($fn,$stringData);
                    
                    print_r($this->table_names);
                    for($i=0;$i<sizeof($this->table_names);$i++){
                    	$stringData2="private $".$this->table_names[$i].";\n";
                    	fwrite($fn,$stringData2);
                    }
                    $stringData3="\t\tfunction __construct()\n\t\t{\n \t\tparent::__construct();\n\t\t}\n";
                    fwrite($fn,$stringData3);
                    for($i=0;$i<sizeof($this->table_names);$i++){
                    	$stringData2="\n\t\tpublic function get".$this->table_names[$i]."()\n\t\t{\n \t\treturn $"."this->".$this->table_names[$i].";\n}\n";
                    	fwrite($fn,$stringData2);
                    }
                    
                    for($i=0;$i<sizeof($this->table_names);$i++){
                    	$stringData2="\n\t\tpublic function set".$this->table_names[$i]."($".$this->table_names[$i].")\n\t\t{\n\t\t  $"."this->".$this->table_names[$i]."=$".$this->table_names[$i].";\n\t\t}\n";
                    	fwrite($fn,$stringData2);
                    }
                    
                    $stringData4="\n\t\tpublic function save()\n\t\t{\n} \n";
                    fwrite($fn,$stringData4);
                    $stringData4="\n\t\tpublic function update()\n\t\t{\n} \n";
                    fwrite($fn,$stringData4);
                    $stringData4="public function view()\n\t\t{\n} \n";
                    fwrite($fn,$stringData4);
                    $stringData4="public function delete()\n\t\t{\n} \n";
                    fwrite($fn,$stringData4);
                    $stringData4="public function view_query($"."sql)\n\t\t{\n} \n";
                    fwrite($fn,$stringData4);
                    
                    $stringData4="} \n ?>";
                    fwrite($fn,$stringData4);
                    
                    fclose($fn);
    }
    
    public function GenerateService()
    {
                $fileName2='../services/'.$this->className."Service.php";

                $fn=fopen($fileName2,'w') or die("can't open file");
                
                $stringData="<?php \n require_once('../model/".$this->className.".php');\n class ".$this->className."Service extends ".$this->className."{\n";
                
                fwrite($fn,$stringData);
                
                //save method
                $stringData3="\t \t public function save(){\n";
                $stringData3=$stringData3."\t \t $"."builder=new InsertBuilder();\n";
                $stringData3=$stringData3."\t \t $"."builder->setTable('".$this->tableName."');\n";
                
                fwrite($fn,$stringData3);
                for($i=0;$i<sizeof($this->table_names);$i++){
                	$stringData2="\t \t $"."builder->addColumnAndData('".$this->table_names[$i]."',parent::get".$this->table_names[$i]."());\n \t";
                	fwrite($fn,$stringData2);
                }
                
                $stringData6="\t \t $"."this->con->setQuery(Director::buildSql($"."builder));\n";
                $stringData6=$stringData6."\t \t return $"."this->con->execute_query2($"."builder->getValues());\n \t  }\n \n";
                fwrite($fn,$stringData6);
                
                //update method
                $stringData3="\t \t public function update(){\n";
                $stringData3=$stringData3."\t \t $"."builder=new UpdateBuilder();\n";
                $stringData3=$stringData3."\t \t $"."builder->setTable('".$this->tableName."');\n";
                
                fwrite($fn,$stringData3);
                for($i=1;$i<sizeof($this->table_names);$i++){
                	$stringData2="\n \t \t if(!is_null(parent::get".$this->table_names[$i]."())){\n$"."builder->addColumnAndData('".$this->table_names[$i]."',parent::get".$this->table_names[$i]."()); \n}\n";
                	fwrite($fn,$stringData2);
                }
                $stringData7="$"."builder->setCriteria(\"where ".$this->table_names[0]."='\".parent::get".$this->table_names[0]."().\"'\");\n";
                $stringData7=$stringData7."$"."this->con->setQuery(Director::buildSql($"."builder));\n";
                $stringData7=$stringData7."\t \t return $"."this->con->execute_query();\n }\n \n";
                fwrite($fn,$stringData7);
                
                //view method
                $stringData3="\n \t public function viewConbox(){\n";
                $stringData3=$stringData3."\t \t $"."sql=\"select * from  ".$this->tableName."\";\n";
                fwrite($fn,$stringData3);
                $stringData7="\t return $"."this->con->getResultSet($"."sql);\n \t }\n";
                fwrite($fn,$stringData7);

                //create a new view method
                $stringData3="\n\t public function view(){\n";
                $stringData3.="\t\t$"."page = isset($"."_POST[\"page\"]) ? intval($"."_POST[\"page\"]) : 1;\n";
                $stringData3.="\t\t$"."rows = isset($"."_POST[\"rows\"]) ? intval($"."_POST[\"rows\"]) : 10;\n";
                $stringData3.="\t\t$"."offset = ($"."page-1)*$"."rows;\n";
                $stringData3.="\t\t $"."sql=\"select * from  ".$this->tableName."\";\n";
                $stringData3.="\t\t$"."this->con->setSelect_query($"."sql);\n";
                $stringData3.="\t\t$"."data2=array();\n\t\t$"."data=array();\n\t\t$"."data[\"total\"]=$"."this->con->sqlCount();\n";
                $stringData3.="\t\t$"."sql=\"select * from  ".$this->tableName." limit $"."offset,$"."rows \";\n";
                $stringData3.="\t\t foreach($"."this->con->getResultSet($"."sql) as $"."row)\n\t\t\t{\n";
                $stringData3.="\t\t\t array_push($"."data2,$"."row);\n\t\t}";
                $stringData3.="$"."data[\"rows\"]=$"."data2;return $"."data; }";

                fwrite($fn,$stringData3);


                //$stringData7="\t return $"."this->con->getResultSet($"."sql);\n \t }\n";
                //fwrite($fn,$stringData7);

                //delete method
                $stringData3="\n \t public function delete(){\n";
                $stringData3=$stringData3."\t \t $"."builder=new DeleteBuilder;\n";
                $stringData3=$stringData3."\t \t $"."builder->setTable('".$this->tableName."');\n";
                
                fwrite($fn,$stringData3);
                
                $stringData7="\t \t $"."builder->setCriteria(\"where ".$this->table_names[0]."='\".parent::get".$this->table_names[0]."().\"'\");\n";
                $stringData7=$stringData7."\t \t $"."this->con->setQuery(Director::buildSql($"."builder));\n";
                $stringData7=$stringData7."\t \t return $"."this->con->execute_query();\n \t }\n \n";
                fwrite($fn,$stringData7);

                //the object find
                $stringData3="\n \t public function find(){\n";
                $stringData3=$stringData3."\t \t $"."sql=\"select * from  ".$this->tableName."  where ".$this->table_names[0]."='\".parent::get".$this->table_names[0]."().\"'\";\n";
                $stringData3=$stringData3."\t \t foreach($"."this->con->getResultSet($"."sql) as $"."row)\n\t \t{\n";
                //$stringData3=$stringData3."\t \t $"."builder->setTable('".$this->tableName."');\n";
                fwrite($fn,$stringData3);
                for($i=0;$i<sizeof($this->table_names);$i++){
                    $stringData2="\t \t"."parent::set".$this->table_names[$i]."($"."row['".$this->table_names[$i]."']);\n";
                    fwrite($fn,$stringData2);
                }
                $stringData3="}\t \t"."}\n";
                fwrite($fn,$stringData3);

                //view method
                //view method
                $stringData3="\n \t public function view_query($"."sql){\n";
                fwrite($fn,$stringData3);
                $stringData7="\t \t return $"."this->con->getResultSet($"."sql);\n \t }\n }\n?>";
                fwrite($fn,$stringData7);
                
                
                
                fclose($fn); 
    }
    public function GenerateController()
    {
                $fileName2='../controller/'.$this->className."Controller.php";
                
                $fn=fopen($fileName2,'w') or die("can't open file");
                $stringData="<?php \n require_once('../services/".$this->className."Service.php');\n  require_once('controller.php');\n";
                
                fwrite($fn,$stringData);
                $string="\n if(isset($"."_REQUEST['"."action"."'])){\n $"."controller_templet=new Controller($"."_REQUEST['action']);\n\t if($"."controller_templet->getAction()=='view'){\n";
                $string.="\t\t $".$this->className."=new ".$this->className."Service();\n \t\t echo json_encode( $".$this->className."->view());\n }";
                fwrite($fn,$string);
                //fwrite($fn,$stringData);
                $string2="\t else if($"."controller_templet->getAction()=='add'){\n";
                $string2.="\t \t $".$this->className."=new ".$this->className."Service();\n ";
                fwrite($fn,$string2);
                //set varible
                for($i=1;$i<sizeof($this->table_names);$i++){
                	
                	$string3="\t \t $".$this->className."->set".$this->table_names[$i]."($"."_REQUEST['".$this->table_names[$i]."']);\n";
                	fwrite($fn,$string3);
                }
                $string4="\t \t echo json_encode( $".$this->className."->save());\n }\n else if($"."controller_templet->getAction()=='edit'){\n \t \t $".$this->className."=new ".$this->className."Service();\n ";
                fwrite($fn,$string4);
                for($i=0;$i<sizeof($this->table_names);$i++){
                	
                	$string3="\t \t $".$this->className."->set".$this->table_names[$i]."($"."_REQUEST['".$this->table_names[$i]."']);\n";
                	fwrite($fn,$string3);
                }
                $string4="\t \t echo json_encode( $".$this->className."->update()); }\n else if($"."controller_templet->getAction()=='delete'){\n \t \t $".$this->className."=new ".$this->className."Service();\n ";
                fwrite($fn,$string4);
                $string3="\t \t $".$this->className."->set".$this->table_names[0]."($"."_REQUEST['".$this->table_names[0]."']);\n";
            	fwrite($fn,$string3);
            	$string5="echo json_encode( $".$this->className."->delete()); \n}else if($"."controller_templet->getAction()=='viewCombo'){  \n\t\t $".$this->className."=new ".$this->className."Service();\n \t\t echo json_encode( $".$this->className."->viewConbox());\n\t\t}   \n} ?>";
            	fwrite($fn,$string5);
            	fclose($fn);
    }
    public function GenerateView()
    {
                mkdir("../view/".$this->className);
            	//create the add item
            	$fileName2='../view/'.$this->className.'/add.html';

                $fn=fopen($fileName2,'w') or die("can't open file");
                	
                $string6=HtmlControlGenerator::openDialog($this->className)."\n";
                fwrite($fn,$string6);
                for($i=1;$i<sizeof($this->table_names);$i++){
                	
                	$string7=HtmlControlGenerator::normalInputBox($this->table_names[$i])."\n";
                	fwrite($fn,$string7);
                }
                $string8=HtmlControlGenerator::closeDialog($this->className);
                fwrite($fn,$string8);
                fclose($fn);
                $fileName2='../view/'.$this->className.'/view.html';
                $fn=fopen($fileName2,'w') or die("can't open file");
                $string9="<div class='col-lg-12'>"."<table id='dg".$this->className."' title='Manage ".$this->className."' class='easyui-datagrid' style='height:auto; width:100%; ' url='controller/".$this->className."Controller.php?action=view' pagination='true' toolbar='#toolbar' rownumbers='true' fitColumns='true' singleSelect='true' iconCls='fa fa-table'><thead>
                        	<tr>";
                fwrite($fn,$string9);
                for($i=0;$i<sizeof($this->table_names);$i++){
                	
                	$string7="\t\t<th field='".$this->table_names[$i]."' width='90'>".$this->table_names[$i]."</th>\n";
                	fwrite($fn,$string7);
                }
                $string10="</tr></thead></table><div id='toolbar'>";
                $string10.='<a href="#" class="btn btn-primary btn-sm" onclick="new'.$this->className.'()"><i class="fa fa-plus-circle"></i>Add</a><a href="#" class="btn btn-link" onclick="edit'.$this->className.'()"><i class="fa fa-pencil"></i>Edit</a>
                        <a href="#" class="btn btn-link" onclick="delete'.$this->className.'()"><i class="fa fa-times-circle"></i>Delete</a></div></div>';
                		fwrite($fn,$string10);
                		fclose($fn);
		
    }
    public function GenerateScript()
    {
        	$fileName2='../scripts/'.$this->className.'.js';
            $fn=fopen($fileName2,'w') or die("can't open file");
            //write the common javascript function
            $string30="\n\n function new".$this->className."(){\n$"."('#dlg".$this->className."').dialog('open').dialog('setTitle','Enter ".$this->className." ');\n$"."('#frm".$this->className."').form('clear');\n url='controller/".$this->className."Controller.php?action=add'; \n}";
            fwrite($fn,$string30);
            $string31="\n\n function edit".$this->className."(){\n var row=$"."('#dg".$this->className."').datagrid('getSelected');\n if(row)\n{\n $"."('#dlg".$this->className."').dialog('open').dialog('setTitle','Edit ".$this->className."');\n $"."('#frm".$this->className."').form('load',row); \n url='controller/".$this->className."Controller.php?action=edit&".$this->table_names[0]."='+row.".$this->table_names[0].";\n}\n else{\n$".".messager.show({title:'Warning!',msg:'Please select a item to to edit'});} \n}";
            fwrite($fn,$string31);
            $string32="\n\n function delete".$this->className."(){\n var row=$"."('#dg".$this->className."').datagrid('getSelected');\n if(row)\n{\n$."."post('controller/".$this->className."Controller.php?action=delete&".$this->table_names[0]."='+row.".$this->table_names[0].",{},function(data){ $"."('#dg".$this->className."').datagrid('reload');});\n}\n else{\n$".".messager.show({title:'Warning!',msg:'Please select a item to to edit'});} \n}";
            fwrite($fn,$string32);
            $string33="\n\n function save".$this->className."(){\n saveForm('#frm".$this->className."',url,'#dlg".$this->className."','#dg".$this->className."');\n}";
            fwrite($fn,$string33);
            fclose($fn);
    }
    
    
    
    
    
    
}




?>