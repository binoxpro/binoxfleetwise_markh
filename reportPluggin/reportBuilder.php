<?php
require_once('caption.php');
class ReportBuilder
{
    public $headerSection;
    public $bodySection;
    public $footerSection;
    public $tableBuilder;
    
    public function __construct(TableBuilder $tb)
    {
        $this->tableBuilder=$tb;
        $this->addHeaderSection();
        $this->addBodySection();
        $this->addfooterSection(); 
    } 
    
    public function addHeaderSection()
    {
        $this->headerSection=$this->tableBuilder->tableTemplate->getCaption();
    }
    
    public function addBodySection()
    {
        $this->bodySection=$this->tableBuilder->tableTemplate->getHeaderInfo().$this->tableBuilder->tableTemplate->getData();
    }
    
    public function addfooterSection()
    {
        $this->footerSection=$this->tableBuilder->tableTemplate->getFooter().$this->tableBuilder->setTableClose();
    }
    
    public function getHeaderSection()
    {
        return $this->headerSection;
    }
    
    public function getBodySection()
    {
        return $this->bodySection;
    }
    
    public function getFooterSection()
    {
        return $this->footerSection;
    }
    
    public function getReport()
    {
        return $this->getHeaderSection().$this->getBodySection().$this->getFooterSection();
    }
    
    
}
?>