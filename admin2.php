<?php 
require_once('header.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FMMS</title>
    
	<link rel="shortcut icon" href="image/setting2.png" />
    <!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/bootstrap/easyui.css" />
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/icon.css" /> 
   	<link rel="stylesheet" media="screen" href="lib/handsontable/dist/jquery.handsontable.full.css">
	<link rel="stylesheet" href="lib/signtures/assets/jquery.signaturepad.css">
    <link rel="stylesheet" href="lib/datepicker/css/datepicker.css" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
	/* *, *:before, *:after{
-webkit-box-sizing:content-box;
-moz-box-sizing:content-box;
box-sizing:content-box;
}*/
.datagrid-editable-input,.textbox-text{
	color:#0061C1;
}
td input{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
td select{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
easyui-datebox{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
	</style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top " role="navigation" style="background:#532900; margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin.php"><h4 style='color:#FFF;'>FST Maintenance Management System</h4></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <!--<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>-->
                    <!-- /.dropdown-messages -->
                <!--</li>-->
                <!-- /.dropdown -->
                <!--<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>-->
                    <!-- /.dropdown-tasks -->
                <!--</li>-->
                <!-- /.dropdown -->
                <!--<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>-->
                    <!-- /.dropdown-alerts -->
                <!--</li>-->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i><?php echo $_SESSION['username']; ?>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="controller/usercontroller.php?action=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                             /input-group 
                        </li>-->
                        <li>
                            <a href="admin.php" style="color:#532900;"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-folder-open-o"></i>Operations Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <!--<li>
                                    <a href="?view=vehicle_Load">Load Vehicle</a>
                                </li>-->
                                
                                <li>
                                    <a href="?view=Manage_Order">Order</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Delivery">Delivery Order</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Other_Cost_Management">OtherCost  To Manage</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Fuel">Fuel Management</a>
                                </li>
                                 <li>
                                    <a href="?view=Manage_Odometer">Weekly Odometer Reading</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-file-pdf-o"></i>Utilisation Report<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                    	<li>
                                            <a href="?view=Utilisation_Report">Capacity Utilisation</a>
                                        </li>
                                        <li>
                                            <a href="?view=Time_Utilisation_Report">Time Utilisation</a>
                                        </li>
                                        <li>
                                            <a href="?view=Running_Cost_Report">Running Cost Report</a>
                                        </li>
                                        <li>
                                            <a href="?view=Utilisation_Graphy">Utilisation Graph</a>
                                        </li>
                                        <li>
                                            <a href="?view=Total_Order_Delivered">Total Order_Delivered</a>
                                        </li>
                                        <li>
                                            <a href="?view=Products_Delivered">Products_Delivered</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="openDia()">Vehicle Km Graph</a>
                                        </li>
                                    </ul> 
                                </li>
                             </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-folder-open-o"></i>Inspection Of Vehicle<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            	<li>
                                    <a href="?view=checkList_download">Download Check List</a>
                                </li>
                                <li>
                                    <a href="?view=IOV_Inspection">Inspection Vehicle</a>
                                </li>
                                <li>
                                    <a href="?view=Inspection_List">Inspection List</a>
                                </li>
                                <li>
                                    <a href="?view=Create_JobCard">Create Job Card</a>
                                </li>
                               <!-- <li>
                                    <a href="?view=Edit_JobCard">Edit Job Card</a>
                                </li>-->
                                <li>
                                    <a href="?view=Follow_Up_JobCard">Follow Up Job Card</a>
                                </li>
                                 <li>
                                    <a href="?view=Complete_JobCard">Close Job Card</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-file-pdf-o"></i>Reports<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="?view=Vehicle_History_Report">Vehicle History</a>
                                        </li>
                                        <li>
                                            <a href="?view=Parts_used_Report">Parts Used/Month</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                               </li>
                                <li>
                                    <a href="#"><i class="fa fa-cogs"></i>Settings<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="?view=xxx">Check list Type</a>
                                        </li>
                                        <li>
                                            <a href="?view=Setting_Check_List_Section">Check List Section</a>
                                        </li>
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>

                            </ul>
                          <!--  /.nav-second-level-->
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-file-pdf-o"></i>Tyre Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <!--<li>
                                    <a href="?view=Manage_Tyre">Tyre Management</a>
                                </li>-->
                                <!--<li>
                                    <a href="#"><i class="fa fa-cogs"></i>Settings<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                         <li>
                                    <a href="?view=Manage_Tyre_Type">Tyre Position</a>
                                		</li>
                                    </ul>
                                   
                                </li>-->
                                 <li>
                                    <a href="#"><i class="fa fa-truck"></i>Inventory Mgt<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                         <li>
                                            <a href="?view=Purchase_Tire">Purchase Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=Mount_Tire">DisMount/Mount Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=retread_return_list">Retread Return list</a>
                                		</li>
                                        <li>
                                            <a href="?view=scrapped_tire">Scrapped Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=scrapped_tire">Tire Disposal</a>
                                		</li>
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cog"></i>Mounted Tire Mgt<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="?view=tire_inspection">Tire Inspection</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_inspection_history">Tire Inspection History</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_monitoring">Tire Monitoring</a>
                                		</li>
                                         <li>
                                            <a href="?view=tire_Rotate">Rotate Tire/Transfer Tire</a>
                                		</li>
                                        
                                        <li>
                                            <a href="?view=warning_Tire">Warning Tires</a>
                                		</li>
                                        
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cogs"></i>Tire Reports<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="?view=tire_history">Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=tread_wear">Tread Wear</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_Scrap">Scrapped</a>
                                		</li>
                            
                                        <li>
                                            <a href="?view=tire_cost">CPM Report</a>
                                		</li>
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cogs"></i>General Settings<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                         <li>
                                    <a href="?view=Manage_Tyre_Type">Tire Position</a>
                                		</li>
                                        <li>
                                    <a href="?view=Manage_Tire_size">Tire Size</a>
                                		</li>
                                        <li>
                                    <a href="?view=Manage_Tire_Pattern">Tire Pattern</a>
                                		</li>


                                    </ul>
                                   
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-file-pdf-o"></i>Service Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <!-- <li>
                                    <a href="?view=Service_Management">Service Management</a>
                                </li>-->
                                <li>
                                    <a href="?view=Service_Management_New">Service Management(New)</a>
                                </li>
                                <li>
                                    <a href="?view=Spare_Parts_Management">Spare Parts</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cogs"></i>Settings<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                    <li>
                                    <a href="?view=System_Administration_CheckList_Management">CheckList Management</a>
                                    </li>
                                    <li>
                                        <a href="?view=System_Administration_CheckListType_Management">CheckListType Management</a>
                                    </li>
                                     <li>
                                        <a href="?view=Manage_Sevrice_Type">Service Setup</a>
                                    </li>
                                    
                                     <li>
                                        <a href="?view=Manage_Model">Setup Model</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Component">Setup Component</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_service_type">Setup Service Type</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Action_Notes">Setup Action Note</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Service_Interval">Setup Service Interval</a>
                                    </li>
                                        
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        <a href="#" style="color:#532900;"><i class="fa fa-cogs"></i>Vehicle Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="?view=System_Administration_Vehicle_Management">Vehicle</a>
                                </li>
                                <li>
                                    <a href="?view=Vehicle_Standards_Management">Vehicle Standard</a>
                                </li>
                                <li>
                                    <a href="?view=licence_Management">Licence Management</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cogs"></i>Settings<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                <li>
                                    <a href="?view=System_Administration_Vehicle_Types">Vechile Type Management</a>
                                </li>
                                <li>
                                    <a href="?view=System_Administration_Trailer_Management">Trailer Management</a>
                                </li>
                                </ul>
                                </li>
							</ul>
                        
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-cogs"></i>System Administration<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <li>
                                    <a href="?view=Manage_Other_Cost">OtherCost Management</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Access_Control">User Management</a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-user"></i>Driver Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="?view=Driver_Registration">Register Driver(Old Version)</a>
                                </li>
                                <!-- <li>
                                    <a href="?view=Driver_Registration_New">Register Driver(New)</a>
                                </li>-->
                                <li>
                                    <a href="?view=Driver_Records">Drivers Records(New)</a>
                                </li>
                              <!--  <li>
                                    <a href="?view=Management_Of_Driver">Manage Driver</a>
                                </li> -->
                                <li>
                                    <a href="?view=Manage_of_License">Manage License</a>
                                </li>
                               <!-- <li>
                                    <a href="?view=Manage_Driver_Training">Manage Driver's Training</a>
                                </li>-->
                                <li>
                                    <a href="?view=Manage_Driver_Training_new">Manage Driver's Training(New)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#" style="color:#532900;"><i class="fa fa-truck"></i>Stock Management<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="?view=manage_stock">New Stock</a>
                                </li>
                                
                                <li>
                                    <a href="?view=used_stock">Used Stock</a>
                                </li>
                              
                                <li>
                                    <a href="?view=scrapped_stock">Scrapped Stock</a>
                                </li>
                               
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header"><?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										$header2="";
										$header3="";
										for($i=0;$i<sizeof($header);$i++){
											if($i<2){
											$header2.= strtoupper($header[$i])."&nbsp;";
											}else{
											$header3.=$header[$i]."&nbsp;";	
											}
										}
										echo "<span>".$header2."</span>:<span>".$header3."</span>";
									}else{
										echo "DashBoard";
									}
			
							?>
                            </h4>
                            <ol class="breadcrumb">
            	<li><i class="icon-home"></i><a href="admin.php"><i class='fa fa-home'></i>Home</a></li>
                <li><span class="divider"></span><a href="admin.php<?php echo isset($_REQUEST['view'])?"?view=".$_REQUEST['view']:"";?>"><?php echo isset($_REQUEST['view'])?$_REQUEST['view']:"DashBoard";?></a></li>
            </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                        <i class="fa fa-cogs"></i>
                            <?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										for($i=0;$i<sizeof($header);$i++){
											echo strtoupper( $header[$i])."&nbsp;";
										}
									}else{
										echo "DashBoard";
									}
			
							?>
                        </div>
                        <div class="panel-body" >
                           <!-- <div class="row">-->
                               <!-- <div class="col-lg-12">-->
                                <!-- /.col-lg-6 (nested) -->
                                <div style="margin-top:10px;">
                                <?php require_once('controller/dispatcher.php'); ?>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                               <!-- </div>-->
                         	<!--</div>-->
                            <!-- /.row (nested) -->
                        </div>
                         <?php require_once('view/odometer/kmgraphgenerator.php'); ?>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
   <!--<script src="js/jquery.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
   <!--<script src="js/bootstrap.min.js"></script>-->

    <!-- Metis Menu Plugin JavaScript -->
   <!--<script src="js/plugins/metisMenu/metisMenu.min.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <!--<script src="js/sb-admin-2.js"></script>-->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <!--<script src="js/plugins/dataTables/jquery.dataTables.js"></script>-->
    <!--<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
    <!-- Custom data table-->
    <!--<script src="dataTb.js"></script>-->
    <!--<script src="lib/jquery.min.js"></script>-->
  <script src="lib/handsontable/dist/jquery.handsontable.full.js"></script>
		
    
    <!--<script type="text/javascript" src="lib/external/jquery/jquery.min.js"></script>-->
    <script type="text/javascript" src="lib/external/jquery/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="lib/external/jquerylib/jquery.edatagrid.js"></script>
	<script type="text/javascript" src="lib/external/jquerylib/datagrid-detailview.js"></script>
    <script src="lib/signtures/assets/jquery.min.js"></script>
	<script src="lib/signtures/jquery.signaturepad.min.js"></script>
	<script src="lib/signtures/assets/json2.min.js"></script>
    
	<script type="text/javascript" src="lib/js/scriptEngine.js"></script>
	<script type="text/javascript" src="lib/external/chart/Chart.js"></script>
    <?php require_once('controller/scriptController.php'); ?>
    <script type="text/javascript">
	function loadingBar(message){
	$.messager.progress({title:'loading',msg:message});
	}

	function closeBar()
	{
	$.messager.progress('close');
	}
	
	function saveForm(formName,url,dialogName,datagridName){
		//url=url;
		$.messager.progress();
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						$(dialogName).dialog('close');
						$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


}

function saveFormUrl(formName,url,urlx){
		//url=url;
		$.messager.progress({title:'Submitting',msg:'Please wait connecting to server in progress...'});
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Error',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						//$(dialogName).dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
						 window.location=urlx+'&id='+result.x;
		}
		
	}
}); 

}
function openDia(){
	$('#dlgGraphGenerator').dialog('open').dialog('setTitle','Km Graph Generator');
	$('#frmGraphGenerator').form('clear');
	//newTrip();
	//url='controller/capacityController.php?action=add';
}
function generateGraph()
{
	var vehicleId=$('#geneId').combobox('getValue');
	location.href='admin.php?view=Vehicle_Kilometer_Graph&id='+vehicleId;
	
}

function notifyMe() {
  if (!("Notification" in window)) {
   // alert("This browser does not support desktop notification");
  }
  else if (Notification.permission === "granted") {
	  $.post('controller/orderController.php?action=orderNotification',{x:'0'},function(data){
		  
	  
        var options = {
                body: data,
                icon: "image2.png",
                dir : "ltr"
             };
          var notification = new Notification("System Alerts",options);
	  });
  }
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }
   
      if (permission === "granted") {
        var options = {
              body: "This is the body of the notification",
              icon: "image2.jpg",
              dir : "ltr"
          };
        var notification = new Notification("System Alerts",options);
      }
    });
  }
}
//var myVar=setInterval(function(){notifyMe()},150000);
//var myVar=setTimeout(function(){notifyMe()}, 1000);
$(function (){
	$('.tdate').datepicker({format:'yyyy-mm-dd'});
	
});

</script>
</body>
</html>