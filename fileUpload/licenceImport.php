<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
//require_once '../services/tyreService.php';
require_once '../services/licenseService.php';
require_once '../services/vehicleService.php';
try{
$target_file='upload7';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Error in uploading file" . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));
$icount=1;
//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		$license=new LicenseService();
		$vehicle=new VehicleService();
		$i=0;
		if($icount>2){
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					//if($cell->getValue()!="Truck" && $cell->getValue()!="Position" && $cell->getValue()!="Serial"&& $cell->getValue()!="New/Retreat"&& $cell->getValue()!="Date of Fitting"&& $cell->getValue()!="truck Odometer fitting"&& $cell->getValue()!="expected Km"&& $cell->getValue()!="Comment"&& $cell->getValue()!="Expected Mileage For Renewal"&& $cell->getValue()!="Brand" && $cell->getValue()!="Cost"  )
					//{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'A',0,2)==1){
								$license->setNumberPlate($vehicle->returnVehicle($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getValue() , EOL;
							}
						
							if(strspn($cell->getCoordinate(),'B',0,2)==1){
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								$license->setthirdPartyDOI($datex);
							}
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1){
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								$license->setthirdPartyDOEg($datex);
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1){
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								$license->setcaliberationDOI($datex);
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1){
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								$license->setcaliberationDOE($datex);
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if((strspn($cell->getCoordinate(),'F',0,2)==1 && intval($cell->getCalculatedValue())>0))
							{
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $datex , EOL;
								$license->setFDoi($datex);
							}
							
							if(strspn($cell->getCoordinate(),'G',0,2)==1){
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								$license->setFDoe($datex);
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							
					//}
				}
			}
		}
		
		if($i>0){
			$license->save();
			//$tyre->update();
		//echo "save cell",EOL;
		}
	}
	$icount=$icount+1;
	}
}

header("location:../admin.php?view=licence_Management");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>