<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once('../services/serviceHeaderService.php');
require_once('../services/subServiceService.php');
require_once('../services/componentScheduleService.php');
require_once('../services/serviceIntervalService.php');
require_once('../services/truckModelService.php');
require_once('../services/vehicleNewService.php');
require_once('../services/truckMakeService.php');
try
{
$target_file='service';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		//$service=new ServiceTrackService();
		$serviceHeader=new serviceHeaderService();
		$vehicle=new vehicleNewService();
		$modelName='';
        $interval=0;
		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if(trim($cell->getValue())!="Tactor  No." && trim($cell->getValue())!="Model" && trim($cell->getValue())!="Chasis no"&& trim($cell->getValue())!="Service Date"&& trim($cell->getValue())!="KM @ Service"&& trim($cell->getValue())!="KM @ Service"&& trim($cell->getValue())!="Service Interval"&& trim($cell->getValue())!="Next service KM"&& trim($cell->getValue())!="current km" && trim($cell->getValue())!="status"&& trim($cell->getValue())!="Expected date of service"&& trim($cell->getValue())!="Service Type"  )
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'B',0,2)==1)
							{
								//$service->setNumberPlate($vehicle->returnVehicle($cell->getValue()));
                                $vehicle->findCriteria($cell->getValue());
                                $serviceHeader->setvehicleId($vehicle->getid());
							}
						
							if(strspn($cell->getCoordinate(),'C',0,2)==1)
							{
								$modelName=$cell->getValue();

							}
							
							//if(strspn($cell->getCoordinate(),'E',0,2)==1)
							//{
								//$serviceHeader->setcreationDate($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							//}

                            if((strspn($cell->getCoordinate(),'E',0,2)==1 && intval($cell->getCalculatedValue())>0))
                            {
                                $datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
                                //echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $datex , EOL;
                                $serviceHeader->setcreationDate($datex);
                            }

							if(strspn($cell->getCoordinate(),'F',0,2)==1)
							{
								$serviceHeader->setkmReading($cell->getValue());

							}

                            if(strspn($cell->getCoordinate(),'G',0,2)==1)
                            {
                                $interval=intval($cell->getValue());
                                //echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
                            }

							

							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0)
		{
		    //first get the service interval.
            $truckModel=new truckModelService();
            $truckModel->setid($vehicle->getmodelId());
            $truckModel->find();
            //get the make.
            $make=new truckMakeService();
            $make->setid($truckModel->getmakeId());
            $make->find();
            //Service Interval
            $serviceInterval=new serviceIntervalService();
            $serviceInterval->findCriteria($make->getmakeName(),$interval,substr($truckModel->getmodelName(),0,3));
            //service Interrval
            $serviceHeader->setserviceIntervalId($serviceInterval->getid());
            $serviceHeader->setnextReading($serviceHeader->getkmReading()+$interval);
            $serviceHeader->setreminderReading($serviceHeader->getnextReading()-3000);
            $serviceHeader->setisActive(true);
            //service Header save.
            $serviceHeader->save();
            //sub service
            $subService=new subServiceService();
            $subService->setserviceHeaderId($serviceHeader->getid());
            //component Object
            $componentSchedule=new componentScheduleService();
            $componentSchedule->setserviceIntervalId($serviceInterval->getid());
            foreach($componentSchedule->view() as $row)
            {
                $subService->setcomponentScheduleId($row['id']);
                $subService->setdone("Maintainence Due");
                $subService->setcomment(" ");
                $subService->save();
            }





			//$service->save();
			//$tyre->update();
		//echo "save cell",EOL;
		}
	}
}
echo "save";
//header("location:../admin.php?view=Service_Management");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>