<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 3000);
/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
//require_once '../services/orderSevice.php';
//require_once '../services/allocationService.php';
//require_once '../services/truckDispatcherService.php';
//require_once '../services/vehicleService.php';
require_once '../services/vehicleNewService.php';
require_once '../services/modelService.php';
require_once'../services/truckMakeService.php';
require_once'../services/truckModelService.php';
require_once '../services/individualAccountService.php';
require_once '../controller/utility.php';
require_once'../services/driverVehicleService.php';
require_once'../services/driverNewService.php';
require_once'../services/driverService.php';
require_once'../services/trailerAssigmentService.php';

try{
$target_file='vehicle';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else
		    {
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    	die( "No file to upload!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));
$countrun=0;
//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		


        //Setting the necessary Objects
        $driverVehicle=new driverVehicleService();
        $tractorHead=new vehicleNewService();
        $trailer=new vehicleNewService();
        $driver=new driverNewService();
        $trailerAssigment=new trailerAssigmentService();


        //
        $firstName="";
        $lastName="";

		$i=0;
		foreach ($cellIterator as $cell) 
		{
		  
			if (!is_null($cell)) 
			{
				if(($cell->getValue()!=""||$cell->getValue()!=NULL)||!is_null($cell->getValue()))
				{
					if($cell->getValue()!="TractorHead" && $cell->getValue()!="Trailer" && $cell->getValue()!="firstName" && $cell->getValue()!="lastName" )
					{
							
							$i=$i+1;
							$countrun=$countrun+1;
							//tractorhead
							if(strspn($cell->getCoordinate(),'A',0,2)==1){
								//$vehicle->setregNo($cell->getValue());
								$tractorHead->findCriteria(trim($cell->getValue()));
								echo "Th:".$tractorHead->getid().":Trailer:";
							}
                            //trailerHead
                            if(strspn($cell->getCoordinate(),'B',0,2)==1){
                                //$vehicle->setregNo($cell->getValue());
                                //$makeVar=$cell->getValue();
                                $trailer->findCriteria(trim($cell->getValue()));
                                echo $trailer->getid().'Status:';
                            }
                            //firstName
                            if(strspn($cell->getCoordinate(),'D',0,2)==1)
                            {
                                //$vehicle->setregNo($cell->getValue());
                                $firstName=trim($cell->getValue());
                                echo '--Driver'.$firstName;
                            }
                            //lastName
                            if(strspn($cell->getCoordinate(),'C',0,2)==1){
                                $lastName=trim($cell->getValue());
                                echo '--Driver'.$lastName."<br/>";
                            }


							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		//we save from there
		if($i>0)
		{
            echo "In Now";
		    $tractorAttached=false;
		    $isTrailer=false;
		    $hasTractorHead=false;
		    $hasTrailer=false;
            //get the Tractorhead ID.
            $hasTractorHead=!is_null($tractorHead->getid())?true:false;
            $hasTrailer=!is_null($trailer->getid())?true:false;
            $driver->findCriteria($firstName, $lastName);
            if($hasTractorHead)
            {
                //get the Driver Name DriverID.

                //save driver vehicle
                if(!is_null($driver->getid()))
                {
                    $driverVehicle->setdriverId($driver->getid());
                    $driverVehicle->updateAllDriverRecord();

                    $driverVehicle = new driverVehicleService();
                    $driverVehicle->setdriverId($driver->getid());
                    $driverVehicle->setvehicleId($tractorHead->getid());
                    $driverVehicle->setassignmentDate(date('Y-m-d'));
                    $driverVehicle->setisActive(true);
                    echo json_encode($driverVehicle->save());
                }

            }
            //save vehicle trailer
            if($hasTrailer)
            {

                if(!is_null($driver->getid())) {
                    $trailerAssigment->settractorId($tractorHead->getid());
                    $trailerAssigment->updateAllVehicle();
                    $trailerAssigment = new trailerAssigmentService();
                    $trailerAssigment->settractorId($tractorHead->getid());
                    $trailerAssigment->settrailerId($trailer->getid());
                    $trailerAssigment->setdriverId($driver->getid());
                    $trailerAssigment->setaddBy('System');
                    $trailerAssigment->setassignmentDate(date('Y-m-d'));
                    $trailerAssigment->setIsActive(true);
                    echo json_encode($trailerAssigment->save());
                }
            }
            //update vehicle has trailer
            $trailer->setIsTrailer($hasTrailer);
            $trailer->setHasTrailer(false);
            $trailer->update();
            //update vehicle is  Trailer
            $tractorHead->setHasTrailer($hasTrailer);
            $tractorHead->setIsTrailer(false);
            $tractorHead->update();

            $firstName=NULL;
            $lastName=NULL;
        }

	}
}





echo "Data Imported|".$countrun;
//header("location:../admin.php?view=Manage_Order");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>