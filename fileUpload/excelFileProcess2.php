<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once '../services/odometerService.php';
try{
$target_file='odometer';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));
//month array
$month=array('jan','feb','march','April','may','june','july','aug','sept','oct','Nov','Dec');

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		$odometer=new OdometerService();
		//array


		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if($cell->getValue()!="Truck" && $cell->getValue()!="month" && $cell->getValue()!="Mileage" )
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'A',0,2)==1)
							{
								$odometer->setvehicleId($odometer->SetForExcelvechileId($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getValue() , EOL;
							}


                            if(strspn($cell->getCoordinate(),'B',0,2)==1)
                            {
                                $odometer->setCreationDate($odometer->setEndDate($cell->getValue()));
                            }

							
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1)
							{
							    $val=$odometer->getOdometerReading(floatval($cell->getValue()),$odometer->getvehicleId());
								$odometer->setreading($val);
								echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() ,'/',$val,'/',$cell->getValue(), EOL;
							}
							
							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0)
		{
			$odometer->save();
			$odometer->firstSave();
			//$odometer->update();
		    //echo "save cell",EOL;
		}
	}
}
echo "Done";


//header("location:../admin.php?view=Manage_Odometer");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>