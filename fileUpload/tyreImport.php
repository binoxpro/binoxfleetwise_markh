<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//ini_set('maxmiu')

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once '../services/tyreService.php';
require_once '../services/tyreTypeService.php';
require_once '../services/stockService.php';
require_once '../services/vehicleService.php';
try
{
$target_file='upload3';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           	die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));
$treadDepth=0.0;
//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		$tyre=new TyreService();
		$tyreType=new TyreTypeService();
		$vehicle=new VehicleService();
		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if($cell->getValue()!="Truck" && $cell->getValue()!="Position" && $cell->getValue()!="Serial"&& $cell->getValue()!="New/Retreat"&& $cell->getValue()!="Date of Fitting"&& $cell->getValue()!="truck Odometer fitting"&& $cell->getValue()!="expected Km"&& $cell->getValue()!="Comment"&& $cell->getValue()!="Expected Mileage For Renewal"&& $cell->getValue()!="Brand" && $cell->getValue()!="Cost" && $cell->getValue()!="Tread DEPTH"  )
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'A',0,2)==1){
								$tyre->setNumberPlate($vehicle->returnVehicle($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getValue() , EOL;
							}
						
							if(strspn($cell->getCoordinate(),'B',0,2)==1){
								$tyre->settyrePosition($tyreType->returnTyreType($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1){
								$tyre->setbrand($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1){
								$tyre->setserialNumber($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1){
								$tyre->setFixedAs($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							if((strspn($cell->getCoordinate(),'F',0,2)==1 && intval($cell->getCalculatedValue())>0))
							{
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $datex , EOL;
								$tyre->setfixingDate($datex);
							}
							
							if(strspn($cell->getCoordinate(),'G',0,2)==1){
								$tyre->setodometer($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							if(strspn($cell->getCoordinate(),'H',0,2)==1){
								$tyre->setexpectedKm($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							
							
							if(strspn($cell->getCoordinate(),'I',0,2)==1){
								$tyre->setremovalMileage($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							if(strspn($cell->getCoordinate(),'J',0,2)==1){
								$tyre->setcomment($cell->getValue());
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
							}
							if(strspn($cell->getCoordinate(),'K',0,2)==1){
								$tyre->setCost(intval($cell->getValue()));
								
							}
							if(strspn($cell->getCoordinate(),'L',0,2)==1){
								$treadDepth=floatval($cell->getValue());
								
							}
							
					}
				}
			}
		}
		
		if($i>0)
		{
			$stock=new stockService();
			$stock->setserialNumber($tyre->getserialNumber());
			if($stock->existanceOf())
			{
				$stock->getObjectExtensance();
				$tyre->setStockId($stock->getid());
				echo json_encode($tyre->save());
				
			}else{
				$stock->setbrand($tyre->getbrand());
				$stock->setcost($tyre->getCost());
				$stock->setcreationDate($tyre->getfixingDate());
				$stock->setstatus("inuse");
				if($tyre->getFixedAs()=='NEW')
			 {
				 $stock->setretreadstatus(false);
				$stock->setstatus('inUse');
				
			 }else if($tyre->getFixedAs()=='RETREAD'){
				$stock->setretreadstatus(true);
				$stock->setstatus('inUse/Retread');
				 
			 }
			 	$stock->settreadDepth($treadDepth);
			 	$id=$stock->saveNew();
			 	$tyre->setStockId($id);
				echo json_encode($tyre->save());
			 
			}
			
			
		}
	}
}

//header("location:../admin.php?view=Manage_Tyre");
}catch(Exception $c)
{
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>