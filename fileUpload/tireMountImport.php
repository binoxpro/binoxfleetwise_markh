<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
//require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
//require_once '../services/partsUsedService.php';
//require_once '../services/vehicleService.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once('../services/tireService.php');
require_once('../services/tireTreadDepthService.php');
require_once('../services/tireHoldService.php');
require_once '../services/vehicleNewService.php';

require_once('../services/tireIusseService.php');
require_once('../services/tireMaintenanceService.php');
require_once('../services/tireRotationService.php');
require_once '../services/tyreTypeService.php';
require_once'../controller/utility.php';
try{
$target_file='upload4';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		//set saving options
        $tire=new tireService();
        $vehicle=new vehicleNewService();
        $tirePositionObj=new TyreTypeService();
        $datex='';
        $tireposition='';
        $odometer='';

		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if($cell->getValue()!="No. Plate" && $cell->getValue()!="Date" && $cell->getValue()!="Pos."&& $cell->getValue()!="Brand"&& $cell->getValue()!="Status"&& $cell->getValue()!="Serial No."&& $cell->getValue()!="Odometer" && $cell->getValue()!="Expected")
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'B',0,2)==1)
							{
                                $datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//$service->setnumberPlate($vehicle->returnVehicle($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getValue() , EOL;
                                //$datex=utility::convertDateFormat($datex,'/','-');
							}
						
							if(strspn($cell->getCoordinate(),'A',0,2)==1)
							{
							    if(strlen($cell->getValue())==7)
                                {
                                    $vehicle->findCriteria(substr($cell->getValue(),0,3)." ".substr($cell->getValue(),3,4));
                                }else{
                                    $vehicle->findCriteria($cell->getValue());
                                }

							}
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1)
							{
                                $tireposition=$cell->getValue();
                                //$workOrder->settownFrom($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1)
							{
							    //$customerDestination=$cell->getValue();
							    $tire->settireBand($cell->getValue());
                            }
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1)
							{
							    //$customerDistance=$cell->getValue();
							    $tire->settireType($cell->getValue());
                            }
							
							if(strspn($cell->getCoordinate(),'F',0,2)==1)
							{
							    $tire->setserialNumber($cell->getValue());
                            }
							if(strspn($cell->getCoordinate(),'G',0,2)==1)
							{
								//$tire->set($cell->getValue());
                                $odometer=$cell->getValue();
							}
                            if(strspn($cell->getCoordinate(),'H',0,2)==1)
                            {
                                $tire->setkms($cell->getValue());
                                //$service->setTypeOfservice($cell->getValue());
                                //echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
                            }
							
							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0){
            //save tire
            $tireId='';
            $cost=array(1500000,2500000,700000,900000);
            $tire->settirePatternId(2);
            $tire->setretreadDepth(rand(8,14));
            $tire->setmaxmiumPressure(120);
            $tire->settireSizeId(1);
            //$tire->settireType()
            $tire->setcost($cost[rand(0,3)]);
            $tire->setstatus('Instock');
            $tire->setisActive(1);
            $tire->setParentId(0);
            $tireId=$tire->save();

            //ret

            $tireTreadDepth=new tireTreadDepthService();
            $tireTreadDepth->settireId($tireId);
            $tireTreadDepth->settreadDepth($tire->getretreadDepth());
            $tireTreadDepth->setCreationDate(date('Y-m-d'));
            $tireTreadDepth->save();


            //mount the tire;
            $tireHold=new tireHoldService();
            $tireHold->setvehicleId($vehicle->getid());
            $tireHold->setpositionId($tirePositionObj->returnPosition($tireposition));
            $tireHold->settireId($tireId);
            $tireHold->setfixingDate($datex);
            $tireHold->setfixingOdometer($odometer);
            $tireHold->setexpectedKms($tire->getkms());
            $tireHold->setremovalKms(($odometer+$tire->getkms()));
            $tireHold->setactualKms(($odometer+$tire->getkms()));
            $tireHold->setcreationDate(date('y-m-d'));
            $tireHold->setcomment("Fitted new tire");
            $tireHold->setisActive(true);
            $tireHold->save();
            //
            $tireIusse=new tireIusse();
            $tireIusse->settireId($tireId);
            $tireIusse->setcreationDate($datex);
            $tireIusse->setisActive(true);
            $tireIusse->save();
            //
            $tire=new tireService();
            $tire->setid($tireId);
            $tire->getObject();
            $tire->setstatus('Fitted');
            $tire->update();

            $datex='';
            $tireposition='';
            $odometer='';
		}
	}
}
echo "Imported Succesfull";
//header("location:../admin.php?view=Spare_Parts_Management");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>