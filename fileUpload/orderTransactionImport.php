<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
//require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
//require_once '../services/partsUsedService.php';
//require_once '../services/vehicleService.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once('../services/workorderService.php');
require_once('../services/tripService.php');
require_once('../services/distanceService.php');
require_once '../services/vehicleNewService.php';
require_once'../services/customerNewService.php';
require_once('../services/tripService.php');
require_once('../services/tripTypeService.php');
require_once('../services/employeeService.php');
require_once'../services/driverVehicleService.php';
require_once'../services/driverNewService.php';
require_once'../services/driverService.php';
require_once'../services/driverVehicleService.php';
require_once'../services/driverNewService.php';
require_once'../services/driverService.php';
require_once '../controller/utility.php';
try{
$target_file='upload4';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    die("No file specified!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		//set saving options
		$workOrder=new workOrderService();
		$distance=new distanceService();
		$vehicle=new vehicleNewService();
		$customer=new customerNewService();
        $trip=new TripService();
        $tripObj=new TripService();
        $customerName='';
        $customerDistance=0;
        $customerDestination='';


		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if($cell->getValue()!="Deliv.Date" && $cell->getValue()!="Plate no." && $cell->getValue()!="Ship-to Name"&& $cell->getValue()!="Transp.Zone"&& $cell->getValue()!="Distance (km)"&& $cell->getValue()!="Qty."&& $cell->getValue()!="Net Price" && $cell->getValue()!="Net Value")
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'A',0,2)==1)
							{
                                $datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//$service->setnumberPlate($vehicle->returnVehicle($cell->getValue()));
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getValue() , EOL;
                                $workOrder->setCreationDate(utility::convertDateFormat($datex,'.','-'));
							}
						
							if(strspn($cell->getCoordinate(),'B',0,2)==1)
							{
							    if(strlen($cell->getValue())==7)
                                {
                                    $vehicle->findCriteria(substr($cell->getValue(),0,3)." ".substr($cell->getValue(),3,4));
                                }else{
                                    $vehicle->findCriteria($cell->getValue());
                                }

							}
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1)
							{
                                $customerName=$cell->getValue();
                                $workOrder->settownFrom($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1)
							{
							    $customerDestination=$cell->getValue();
							    $workOrder->settownTo($cell->getValue());
                            }
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1)
							{
							    $customerDistance=$cell->getValue();
							    $workOrder->setDistance($cell->getValue());
                            }
							
							if(strspn($cell->getCoordinate(),'F',0,2)==1)
							{
							    $workOrder->setQuantity($cell->getValue());
                            }
							if(strspn($cell->getCoordinate(),'G',0,2)==1)
							{
								$workOrder->setRate($cell->getValue());
							}
                            if(strspn($cell->getCoordinate(),'H',0,2)==1)
                            {
                                //$service->setTypeOfservice($cell->getValue());
                                //echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $odometer->getreading() , EOL;
                            }
							
							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0){
			//save the customer
            $customer->setcustomerName($customerName);
            $customer->setdestination($customerDestination);
            $customer->setdistance($customerDistance);
           echo "Client Save:".json_encode($customer->save());
            //distance
            $distance->setdistrictTo($customerDestination);
            $distance->setdistance($customerDistance);
            echo "Distance Save:".json_encode($distance->save());
            //Set to Local trip
            $trip->setTripTypeId(1);
            $tripType=new tripTypeService();
            $tripType->setid(1);
            $tripType->getObject();
            $trpNumber=$trip->tripNumberGenerator($tripType->getPrefix());
            //trip information
            $tripObj->setTripNumber($trpNumber);
            $tripObj->setTripTypeId(1);
            $tripObj->setVehicleId($vehicle->getid());
            //get driver id
            $driverVehicle=new driverVehicleService();
            $driverVehicle->setvehicleId($vehicle->getid());
            $driverVehicle->findDriver();
            //
            $tripObj->setDriverId($driverVehicle->getdriverId());
            $tripObj->setStatus(true);
            $tripObj->setTripStartDate($workOrder->getCreationDate());
            //$trip->setTripEndDate(null);
            $tripObj->setWorkOrderAttached(true);
            $tripObj->setApproved(true);
            $tripObj->setApproved(false);
            echo "Trip Save:".json_encode($tripObj->save());
            //workorder
            $workOrder->setCustomerId($customer->getcustomercode());
            $workOrder->setProduct('Cement');
            $amount=0;
            $amount=$workOrder->getQuantity()*$workOrder->getRate()*$workOrder->getDistance();
            $workOrder->setAmount($amount);
            $workOrder->settripNo($trpNumber);
            $workOrder->setClaimed(true);
            $workOrder->setWhtApplied(true);
            $workOrder->setWhtValue((0.06*$amount));
            echo "Workorder Save:".json_encode($workOrder->save());
            //save the trip
$customerName="";
$customerDistance="";
$customerDestination="";
		}
	}
}
echo "Imported Succesfull";
//header("location:../admin.php?view=Spare_Parts_Management");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>