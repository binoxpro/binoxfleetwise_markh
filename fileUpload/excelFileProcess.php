<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once '../services/orderSevice.php';
require_once '../services/allocationService.php';
require_once '../services/truckDispatcherService.php';
require_once '../services/vehicleService.php';
try{
$target_file='upload';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
            move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    	die( "No file to upload!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		$order=new OrderService();
		$allocation = new AllocationService();
		$dispatcherTruck=new TruckDispatcherService();
		$vehicle=new VehicleService();
		$i=0;
		foreach ($cellIterator as $cell) 
		{
			if (!is_null($cell)) 
			{
				if($cell->getValue()!=""||$cell->getValue()!=NULL)
				{
					if($cell->getValue()!="Customer" && $cell->getValue()!="Load Date(M/D/Y)" && $cell->getValue()!="OrderNo" && $cell->getValue()!="ItemCode" && $cell->getValue()!="InTime" && $cell->getValue()!="OutTime" && $cell->getValue()!="TruckNo" )
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'A',0,2)==1){
								$order->setmailingName($cell->getValue());
							}
							
							
							if((strspn($cell->getCoordinate(),'B',0,2)==1 && intval($cell->getCalculatedValue())>0))
							{
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $datex , EOL;
								$order->setpromiseDate($datex);
							}
							
							//if((strspn($cell->getCoordinate(),'B',0,2)==1 && intval($cell->getCalculatedValue())>0))
//							{
//								$dateY= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
//								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $dateY , EOL;
//								$order->setorderDate($dateY);
//							}
							
							if(strspn($cell->getCoordinate(),'C',0,2)==1){
								$order->setorderNumber($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1){
								$order->setqtyOrder($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1){
								$order->setitemCode($order->returnId($cell->getValue(),"tblgood"));
								
							}
							if(strspn($cell->getCoordinate(),'F',0,2)==1){
								//time Intime
								$timeIn= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "hh:mm");
								$dispatcherTruck->settimeinDepo($timeIn);
								
							}
							
							if(strspn($cell->getCoordinate(),'G',0,2)==1){
								//time Outtime
								$timeOut= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "hh:mm");
								$dispatcherTruck->settimeoutDepo($timeOut);
								
							}
							
							if(strspn($cell->getCoordinate(),'H',0,2)==1){
								// truck number
								$allocation->settruckId($vehicle->returnVehicle($cell->getValue()));
							}
							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0){
			
			$order->setorderStatus($order->returnId("Ready","tblorderstatus"));
			$order->setorderType($order->returnId('SA',"tblordertype"));
			$order->setstatusOrder($order->returnId("525","tblstatusorder"));			
			$order->save();
			//allocate truck
			$allocation->setorderId($order->getid());
			$allocation->save();
			//update order status
			$order->updateOrderStatus();
			//dispatcher truck
			$dispatcherTruck->setAllocationId($allocation->getid());
			$dispatcherTruck->save();
			//update order status
			$order->dispatcherOrder();
			//echo "save cell",EOL;
		}
	}
}

header("location:../admin.php?view=Manage_Order");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>