<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 3000);
/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
require_once '../services/orderSevice.php';
require_once '../services/allocationService.php';
require_once '../services/truckDispatcherService.php';
require_once '../services/vehicleService.php';
try{
$target_file='upload';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    	die( "No file to upload!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));

//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		
		//set saving options
		$order=new OrderService();
		$allocation = new AllocationService();
		$dispatcherTruck=new TruckDispatcherService();
		$vehicle=new VehicleService();
        $vpower=0;
        $ugl=0;
        $ago=0;
        $bik=0;
        
		$i=0;
		foreach ($cellIterator as $cell) 
		{
		  
			if (!is_null($cell)) 
			{
				if(($cell->getValue()!=""||$cell->getValue()!=NULL)||!is_null($cell->getValue()))
				{
					if($cell->getValue()!="Customer" && $cell->getValue()!="Date" && $cell->getValue()!="Distance" && $cell->getValue()!="Vpower" && $cell->getValue()!="UGL" && $cell->getValue()!="AGO"&& $cell->getValue()!="BIK"&& $cell->getValue()!="RATE"&& $cell->getValue()!="TransBill" && $cell->getValue()!="VehicleNo"&& $cell->getValue()!="TransBill"&& $cell->getValue()!="DocumentNo" )
					{
							
							$i=$i+1;
							if(strspn($cell->getCoordinate(),'C',0,2)==1){
								$order->setmailingName($cell->getValue());
							}
							
							
							if((strspn($cell->getCoordinate(),'A',0,2)==1 && intval($cell->getCalculatedValue())>0))
							{
								$datex= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $datex , EOL;
								$order->setpromiseDate($datex);
							}
							
							//if((strspn($cell->getCoordinate(),'B',0,2)==1 && intval($cell->getCalculatedValue())>0))
//							{
//								$dateY= PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
//								//echo 'Cell - ' , $cell->getCoordinate() , ' - ' , $dateY , EOL;
//								$order->setorderDate($dateY);
//							}
							
							if(strspn($cell->getCoordinate(),'K',0,2)==1){
								$order->setorderNumber($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'D',0,2)==1)
                            {
								//$order->setqtyOrder($cell->getValue());
							}
							
							if(strspn($cell->getCoordinate(),'E',0,2)==1)
                            {
								//$order->setitemCode($order->returnId($cell->getValue(),"tblgood"));
                                $vpower=$cell->getValue();
								
							}
                            
                            if(strspn($cell->getCoordinate(),'F',0,2)==1)
                            {
                                $ugl=$cell->getValue();
								//$order->setqtyOrder($cell->getValue());
							}
                            if(strspn($cell->getCoordinate(),'G',0,2)==1)
                            {
								//$order->setitemCode($order->returnId($cell->getValue(),"tblgood"));
                                $ago=$cell->getValue();
								
							}
                            if(strspn($cell->getCoordinate(),'H',0,2)==1)
                            {
								//$order->setitemCode($order->returnId($cell->getValue(),"tblgood"));
                                $bik=$cell->getValue();
								
							}
                            
                            
							
							if(strspn($cell->getCoordinate(),'B',0,2)==1){
								// truck number
								$allocation->settruckId($vehicle->returnVehicle($cell->getValue()));
							}
							
							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		
		if($i>0){
			if($vpower>0){
			$order->setorderStatus($order->returnId("Ready","tblorderstatus"));
            $order->setqtyOrder($vpower);
            $order->setitemCode($order->returnId('SUPVP',"tblgood"));
			$order->setorderType($order->returnId('SA',"tblordertype"));
			$order->setstatusOrder($order->returnId("525","tblstatusorder"));			
			$order->save();
			//allocate truck
			$allocation->setorderId($order->getid());
			$allocation->save();
			//update order status
			$order->updateOrderStatus();
			//dispatcher truck
			$dispatcherTruck->setAllocationId($allocation->getid());
			$dispatcherTruck->save();
			//update order status
			$order->dispatcherOrder();
            }
            if($ugl>0){
			$order->setorderStatus($order->returnId("Ready","tblorderstatus"));
            $order->setqtyOrder($ugl);
            $order->setitemCode($order->returnId('FSUL',"tblgood"));
			$order->setorderType($order->returnId('SA',"tblordertype"));
			$order->setstatusOrder($order->returnId("525","tblstatusorder"));			
			$order->save();
			//allocate truck
			$allocation->setorderId($order->getid());
			$allocation->save();
			//update order status
			$order->updateOrderStatus();
			//dispatcher truck
			$dispatcherTruck->setAllocationId($allocation->getid());
			$dispatcherTruck->save();
			//update order status
			$order->dispatcherOrder();
            }
            if($ago>0){
			$order->setorderStatus($order->returnId("Ready","tblorderstatus"));
            $order->setqtyOrder($ago);
            $order->setitemCode($order->returnId('FSADIE 100',"tblgood"));
			$order->setorderType($order->returnId('SA',"tblordertype"));
			$order->setstatusOrder($order->returnId("525","tblstatusorder"));			
			$order->save();
			//allocate truck
			$allocation->setorderId($order->getid());
			$allocation->save();
			//update order status
			$order->updateOrderStatus();
			//dispatcher truck
			$dispatcherTruck->setAllocationId($allocation->getid());
			$dispatcherTruck->save();
			//update order status
			$order->dispatcherOrder();
            }
            if($bik>0){
			$order->setorderStatus($order->returnId("Ready","tblorderstatus"));
            $order->setqtyOrder($bik);
            $order->setitemCode($order->returnId('KER',"tblgood"));
			$order->setorderType($order->returnId('SA',"tblordertype"));
			$order->setstatusOrder($order->returnId("525","tblstatusorder"));			
			$order->save();
			//allocate truck
			$allocation->setorderId($order->getid());
			$allocation->save();
			//update order status
			$order->updateOrderStatus();
			//dispatcher truck
			$dispatcherTruck->setAllocationId($allocation->getid());
			$dispatcherTruck->save();
			//update order status
			$order->dispatcherOrder();
            }
            $vpower=0;
            $ugl=0;
            $ago=0;
            $bik=0;
			//echo "save cell",EOL;
		}
	}
}

header("location:../admin.php?view=Manage_Order");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>