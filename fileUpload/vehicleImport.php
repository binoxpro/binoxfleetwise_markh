<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');
ini_set('max_execution_time', 3000);
/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . '\Classes\PHPExcel\IOFactory.php';
require_once '../lib/excelplugin/Classes/PHPExcel/IOFactory.php';
//require_once '../services/orderSevice.php';
//require_once '../services/allocationService.php';
//require_once '../services/truckDispatcherService.php';
//require_once '../services/vehicleService.php';
require_once '../services/vehicleNewService.php';
require_once '../services/modelService.php';
require_once'../services/truckMakeService.php';
require_once'../services/truckModelService.php';
require_once '../services/individualAccountService.php';
require_once '../controller/utility.php';
try{
$target_file='vehicle';
if( $_FILES['file']['name'] != "" )
{
	if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	//exit("Please run 05featuredemo.php first." . EOL);
	
   move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
	}else{
		if(!unlink($target_file."/".basename($_FILES['file']['name']))){
			die( "Cannot detele file");
		}else{
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file."/".basename($_FILES['file']['name']))or 
           die( "Could not copy file!");
		}
	}
}
else
{
    	die( "No file to upload!");
}

if (!file_exists($target_file."/".basename($_FILES['file']['name']))) {
	exit("Please run 05featuredemo.php first." . EOL);
}

//echo date('H:i:s') , " Load from Excel2007 file" , EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($target_file."/".basename($_FILES['file']['name']));
$countrun=0;
//echo date('H:i:s') , " Iterate worksheets" , EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	//echo 'Worksheet - ' , $worksheet->getTitle() , EOL;
	foreach ($worksheet->getRowIterator() as $row) 
	{
		//echo 'Row number - ' , $row->getRowIndex() , EOL;
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
		


        //Setting the necessary Objects
        $vehicle=new vehicleNewService();
        $model=new truckModelService();
        $make=new truckMakeService();
        //Donot Save the Vehicle if $
        $modelId=-1;
        $makeVar='';
        $modelVar='';
        $countryofman='';


		$i=0;
		foreach ($cellIterator as $cell) 
		{
		  
			if (!is_null($cell)) 
			{
				if(($cell->getValue()!=""||$cell->getValue()!=NULL)||!is_null($cell->getValue()))
				{
					if($cell->getValue()!="TRUCK" && $cell->getValue()!="MAKE" && $cell->getValue()!="MODEL" && $cell->getValue()!="CHASSIS NO" && $cell->getValue()!="YOM" && $cell->getValue()!="DOP"&& $cell->getValue()!="COUNTRY OF ORIGIN"&& $cell->getValue()!="ENGINE CAPACITY CC"&& $cell->getValue()!="CROSS WEIGHT- KG" && $cell->getValue()!="COLOR"&& $cell->getValue()!="STATUS"&& $cell->getValue()!="FUEL"&&$cell->getValue()!="PURPOSE" )
					{
							
							$i=$i+1;
							$countrun=$countrun+1;
							//Truck
							if(strspn($cell->getCoordinate(),'C',0,2)==1){
								$vehicle->setregNo($cell->getValue());
							}
                            //Make
                            if(strspn($cell->getCoordinate(),'D',0,2)==1){
                                //$vehicle->setregNo($cell->getValue());
                                $makeVar=$cell->getValue();
                            }
                            //MODEL
                            if(strspn($cell->getCoordinate(),'E',0,2)==1){
                                //$vehicle->setregNo($cell->getValue());
                                $modelVar=$cell->getValue();
                            }
                            //Chassis No
                            if(strspn($cell->getCoordinate(),'F',0,2)==1){
                                $vehicle->setchaseNumber($cell->getValue());
                            }
                            //YOM
                            if(strspn($cell->getCoordinate(),'G',0,2)==1){
                                $vehicle->setyom($cell->getValue());
                            }
                            //DOP
                        if (strspn($cell->getCoordinate(), 'H', 0, 2) == 1)
                        {
                            if ((strspn($cell->getCoordinate(), 'H', 0, 2) == 1 && intval($cell->getCalculatedValue()) > 0)) {

                                $datex = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), "YYYY-MM-DD");
                                echo 'Cell - ', $cell->getCoordinate(), ' - ', utility::convertDateFormat($datex,'/','-'), EOL;
                                $vehicle->setdop(utility::convertDateFormat($datex,'/','-'));
                            }
                        }
                            //Country of origin
                            if(strspn($cell->getCoordinate(),'I',0,2)==1)
                            {
                                //$vehicle->setdop($cell->getValue());
                                $countryofman=$cell->getValue();
                            }
                            //Engine Capacity CC
                            if(strspn($cell->getCoordinate(),'J',0,2)==1)
                            {
                                $vehicle->setengineCapacityCC($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }
                            //Gross Weight- Height.
                            if(strspn($cell->getCoordinate(),'K',0,2)==1)
                            {
                                $vehicle->settonnage($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }
                            //Color.
                            if(strspn($cell->getCoordinate(),'L',0,2)==1)
                            {
                                $vehicle->setcolor($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }
                            //Color.
                            if(strspn($cell->getCoordinate(),'M',0,2)==1)
                            {
                                $vehicle->setstatus($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }
                            //Status.
                            if(strspn($cell->getCoordinate(),'N',0,2)==1)
                            {
                                $vehicle->setfuel($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }
                            //Purpose.
                            if(strspn($cell->getCoordinate(),'O',0,2)==1)
                            {
                                $vehicle->setpurpose($cell->getValue());
                                //$countryofman=$cell->getValue();
                            }


							//echo "Save cell test 2",EOL;
					}
				}
			}
		}
		//we save from there
		if($i>0){
            //first get the Model and make.
            $make->findMakeId($makeVar);
            //if the make doesnot exit first save else get id.
            if($make->getid()>0)
            {

            }else{
                $make->setid(null);
                $make->setmakeName($makeVar);
                $make->setPrefered(true);
                $make->save();
                $make->findMakeId($makeVar);
            }
            //get Model existance.
            $model->setmodelName($modelVar);
            $model->setmakeId($make->getid());
            $model->findModel();

            if($model->getid()>0)
            {

            }else
            {
                $model->setid(null);
                $model->setcountry($countryofman);
                $model->setisActive(true);
                $model->save();
                $model->findModel();
            }
            //set the vehicle model id
            $vehicle->setmodelId($model->getid());
            //save vehicle;
            $vehicle->save();

            $individualAccount=new individualAccountService();
            $individualAccount->setId($vehicle->getid());
            $individualAccount->setAccountName($vehicle->getregNo());
            $individualAccount->setAccountCode(null);
            $individualAccount->setIsActive(true);
            $individualAccount->save();

            //Donot Save the Vehicle if $
            $modelId=-1;
            $makeVar='';
            $modelVar='';
            $countryofman='';
			//echo "save cell",EOL;
		}
	}
}





echo "Data Imported|".$countrun;
//header("location:../admin.php?view=Manage_Order");
}catch(Exception $c){
	echo $c->getMessage();
}
// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

//echo "<br/>	Seco";
?>