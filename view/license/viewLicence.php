
<table id="dgLicenseManagement" title="License Management" class="easyui-datagrid" style="height:450px;" url="controller/licenseController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarLicenseManageement">
<thead>
<tr>
	<th  field="regNo" width="90" rowspan="2">Number Plate</th>
    <th colspan="2">Third Party</th>
    <th colspan="2">Comprehensive Insurance</th>
    <th colspan="2">Fire Extinguishers</th>

</tr>
<tr>
    <th field="tpDoi" width="90" >(DOI)</th>
    <th field="tpDoe" width="90" data-options="styler:cellStylertp">(DOE)</th>
    <th field="cDoi" width="90">(DOI)</th>
    <th field="cDoe" width="120" data-options="styler:cellStylerc">(DOE)</th>
    <th field="fDoi" width="90">(DOI)</th>
    <th field="fDoe" width="90" data-options="styler:cellStylerf">(DOE)</th>

</tr>
</thead>

</table>
<div id="toolBarLicenseManageement">
<a href="#" class="btn btn-primary" onclick="newLicenseManagement()" ><i class="fa fa-plus-circle"></i>Add</a>
<a href="#" class="btn btn-link"  onclick="editLicenseMaintance()" ><i class="fa fa-edit"></i>Edit</a>
<a href="#" class="btn btn-link"  onclick="renewLicenseMaintance()" ><i class="fa fa-check"></i>Renewal</a>
<a href="#" class="btn btn-link" onclick="loadGrid()" ><i class="fa fa-reload"></i>Reload All</a>
<a href="#" class="btn btn-link" onclick="deleteLicense()" ><i class="fa fa-reload"></i>Delete</a>
<a href="admin.php?view=upload_licence_Data" class="btn btn-link">Upload Data</a>
<span style="">Select Vehicle:<select id="numberPlateSearchLicense" name="numberPlateSearchLicense" class="easyui-combobox form-control" style="width:250px;height:30px;" data-options="url:'controller/vehicleNewController.php?action=viewCombo&vehicleType=Tractor Head',textField:'vehicle',valueField:'id',onSelect:function(){
searchLicenseDetails($('#numberPlateSearchLicense').combobox('getValue'));
}"></select></span>
</div>

<?php
include_once('addLicence.php');
?>
<script type="text/javascript">
function cellStylertp(value,row,index){
			if(row.tpStatus=='ex'){
             return 'color:red;text-decoration:blink';
        	}
		}
		function cellStylerc(value,row,index){
			if(row.cStatus=='ex'){
            return 'color:red;text-decoration:blink';
			}
		}
		function cellStylerp(value,row,index){
			 if(row.pStatus=='ex')
			{
				 return 'color:red;text-decoration:blink';
			} 
		}
		function cellStylerf(value,row,index){
			 if(row.fStatus=='ex')
			{
				 return 'color:red;text-decoration:blink';
			} 
		}
	



function loadGrid()
{
	$('#dgLicenseManagement').datagrid('load',{});
}
function searchLicenseDetails(searchVal)
{
	$('#dgLicenseManagement').datagrid('load',{numberPlate:searchVal});
}
function newLicenseManagement(){
		
		
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Service Management');
			
			$('#frmLicenseMaintainance').form('clear');
			
			url='controller/licenseController.php?action=add';
				
	}
	
	
	function editLicenseMaintance(){
		var row=$('#dgLicenseManagement').datagrid('getSelected');
		if(row){
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Edit Vechile');
			$('#frmLicenseMaintainance').form('load',row);
			url='controller/licenseController.php?action=update&id='+row.Id;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a License edit'});
		}
		
	}
    function renewLicenseMaintance(){
		var row=$('#dgLicenseManagement').datagrid('getSelected');
		if(row){
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Edit Vechile');
			$('#frmLicenseMaintainance').form('load',row);
			url='controller/licenseController.php?action=renewal&id='+row.Id;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a License edit'});
		}
		
	}
	
	function deleteLicense(){
		var row=$('#dgLicenseManagement').datagrid('getSelected');
		if(row){
			$.messager.confirm('Confirm', 'Are sure you would like to delete is licence Information', function(r){
				if (r){
		
			//$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Edit Vechile');
			//$('#frmLicenseMaintainance').form('load',row);
			url='controller/licenseController.php?action=delete&id='+row.Id;
			$.post(url,{},function(data){
				$.messager.show({title:'Message',msg:'Licence Information has deleted successful'});
				$('#dgLicenseManagement').datagrid('reload');
			})
				}});
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a Licence to edit'});
		}
		
	}
	
	function saveTrack(){
		
		
		
		$.messager.progress();
	 $('#frmTrack').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Truck successfully added'});
						$('#dlgTrack').dialog('close'); // close the dialog
						 $('#dgLicenseManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	
	
</script>