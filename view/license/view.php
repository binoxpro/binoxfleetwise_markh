<table id="dgLicenseManagement" title="License Management" class="easyui-datagrid" style="height:450px;" url="controller/licenceController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarLicenseManageement">
<thead>
<tr>
    <th field="driverName" width="90">Driver Name</th>
    <th field="licenceNo" width="90">License No</th>
    <th field="issueDate" width="90">Issue Date</th>
    <th field="expiryDate" width="120">Expiry Date</th>
    <th field="licenseClass" width="90">Class</th>
    <th field="IsActive" width="90">Status</th>
</tr>
</thead>
</table>
<div id="toolBarLicenseManageement">
<a href="#" class="btn btn-primary"  onclick="newLicenseManagement()" ><i class="fa fa-plus-circle"></i>Add</a>
<a href="#" class="btn btn-link"  onclick="editLicenseMaintance()" ><i class="fa fa-edit"></i>Edit</a>

<a href="#" class="btn btn-link" onclick="loadGrid()"exportToExcel() ><i class="fa fa-refresh"></i>Refresh</a>
<a href="#" class="btn btn-link" onclick="exportToExcel()" ><i class="fa fa-file-excel-o"></i>Export Excel</a>
<a href="controller/driverLicenseDocument.php" class="btn btn-link" target="_blank"  ><i class="fa fa-file-pdf-o"></i>Export PDF Format</a>
<span style="margin-left:10%;">Driver:<select id="numberPlateSearchLicense" name="numberPlateSearchLicense" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/driverController.php?action=view',textField:'firstName',valueField:'id',onSelect:function(){
searchLicenseDetails($('#numberPlateSearchLicense').combobox('getValue'));
}"></select></span>
</div>

<?php
include_once('add.php');
?>
<script type="text/javascript">
	
</script>