<div id="dlgLicenseManagement" class="easyui-dialog" style="height:auto; width:800px; " closed='true' buttons='#button_barLicenseManagement'>

<form id="frmLicenseMaintainance" name="frmLicenseMaintainance" method="post">
<div class="col-lg-6">
	<div class="form-group">
	<label>Registration Number:</label><select id="numberPlate1" name="numberPlate1" class="easyui-combobox form-control" style="width:100%;height:30px;" data-options="url:'controller/vehicleNewController.php?action=viewCombo',textField:'vehicle',valueField:'id'"></select></div>
	<div class="form-group">
	<label>Third Party(DOI):</label><input type="text"  name="tpDoi" value="" id="tpDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;"  /></div>
	<div class="form-group">
	<label>Third Party(DOE)</label><input type="text"  name="tpDoe" value="" id="tpDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
	<div class="form-group">
	<label>Comprehensive Insurance(DOI):</label><input type="text"  name="cDoi" value="" id="cDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;"  /></div>
	<div class="form-group">
	<label>Comprehensive Insurance(DOE)</label><input type="text"  name="cDoe" value="" id="cDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
</div>
<div class="col-lg-6">
	<div class="form-group">
	<label>Fire exhauster (DOI)</label><input type="text"  name="fDoi" value="" id="fDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
	<div class="form-group">
	<label>Fire exhauster (DOE)</label><input type="text"  name="fDoe" value="" id="fDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
	<div class="form-group">
	<label>Pressure Test(DOI)</label><input type="text"  name="pDoi" value="" id="pDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
	<div class="form-group">
	<label>Pressure Test(DOE)</label><input type="text"  name="pDoe" value="" id="pDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;height:30px;" /></div>
	</div>
</form>

</div>
 <div id="button_barLicenseManagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveLicenseManagement()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgLicenseManagement').dialog('close')">Close</a>
</div>
<script type="text/javascript">

function saveLicenseManagement()
{
		$.messager.progress();
	 $('#frmLicenseMaintainance').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Information add'});
						$('#dlgLicenseManagement').dialog('close'); // close the dialog
						 $('#dgLicenseManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}
</script>
