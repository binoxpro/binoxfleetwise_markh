<div id="dlgLicenseManagement" class="easyui-dialog" style="height:auto; width:500px; " closed='true' buttons='#button_barLicenseManagement'>

<form id="frmLicenseMaintainance" name="frmLicenseMaintainance" method="post">
<div class="form-group">
    <label id="driverDetails" style="font-size:18px;"></label>
    <label>Driver ID:</label>
    </div>
    <div class="form-group">
    <select id="driverId" name="driverId" class="easyui-combobox" data-options="url:'controller/driverController.php?action=view',textField:'firstName',valueField:'id'" style="width:485px; height:30px;"></select>
    </div>
    <div class="form-group">
    <label>Licence No</label>
    <input class="form-control" name="licenceNo" id="licenceNo" value="" type="text"/>
    </div>
    <div class="form-group">
    <label>Licence Class</label>
    <select class="form-control" name="licenceClass"><option value="B">B</option><option value="CM">CM</option><option value="CH">CH</option><option value="H">H</option><option value="DL">DL</option><option value="DM">DM</option></select>
    </div>
    <div class="form-group">
    <label>Issue Date</label>
    <input class="form-control tdate" name="issueDate" id="issueDate" value="" type="text" />
    </div>
    <div class="form-group">
    <label>Expiry Date</label>
    <input class="form-control tdate" name="expiryDate" id="expiryDate" value="" type="text"/>
    </div>
</form>

</div>
 <div id="button_barLicenseManagement">
<a href="#" class="btn btn-success"  onclick="saveLicenseManagement()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger"  onclick="javascript:$('#dlgLicenseManagement').dialog('close')"><i class="fa fa-times"></i>Close</a>
</div>

