<div class="col-lg-12" style="margin-top:10px; margin-bottom:10px;">
    <table id="dgAllocation" title="Manage Allocation" style="height:auto;" url="controller/allocationController.php?action=view" pagination="true" toolbar="#toolbarAllocation" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-chart">
    	<thead>
        	<tr>
            <th field="id" width="150" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="mailingName" width="150">Name</th>
            <th field="orderDate" width="100">Order Date</th>
            <th field="promiseDate" width="100">Promise Date</th>
            <th field="ordertypeId" width="50">Order Type Id</th>
            <th field="orderNumber" width="100">Order No</th>
            <th field="qtyOrder" width="100">Qty</th>
            <th field="orderstatusId" width="100">Order Status</th>
            <th field="truck" width="100" data-options="editor:{type:'combogrid',options:{panelWidth:150,idField:'id',textField:'regNo',url:'controller/vehicleController.php?action=view',columns:[[{field:'regNo',title:'Truck No',width:90},{field:'capacty',title:'Capacity',width:100}]]}}" formatter="truckFormatter" >Truck No</th>
            <th field="eid" width="150" data-options="editor:{type:'text'}" hidden="true">#</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarAllocation">
    	<a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgAllocation').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>Add</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="saveAllocation()"><i class="fa fa-save"></i>Save</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgAllocation').edatagrid('deleteRow');refreshGird()"><i class="fa fa-times-circle"></i>Delete</a>
        <a href="#" class="btn btn-link" onclick="exportToExcel()">Excel File</a>
    </div>
    </div>