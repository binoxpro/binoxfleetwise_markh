    <div class="col-lg-12">
    <table id="dgOdometer" title="Manage Odometer" class="easyui-datagrid" style="height:500px;" url="controller/odometerController.php?action=view" pagination="true" toolbar="#toolbarOdometer" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
            <th field="vehicleId" width="120">Vehicle</th>
            <th field="reading" width="150">Reading</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarOdometer" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newOdometer()"><i class="fa fa-plus-circle"></i>Add Odometer</a>
       <!--<a href="#" class="btn btn-primary btn-sm" onclick="editOdometer()"><i class="fa fa-pencil"></i>Edit Odometer</a>-->
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Odometer</a>
        <a href="admin.php?view=upload_Odometer_File" class="btn btn-primary btn-sm"><i class="fa fa-upload"></i>Upload File</a>
    </div>
    </div>
    <?php include_once('add.php'); ?>
    
