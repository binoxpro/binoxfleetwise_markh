<div id="dlgLicenseManagement" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barLicenseManagement'>
<div class="easyui-panel" title="License Management" style="height:400px;">
<form id="frmLicenseMaintainance" name="frmLicenseMaintainance" method="post">
<table cellpadding="0" cellspacing="5" style="width:80%;">
<tr><td>
<label>Number Plate:</label></td><td><select id="numberPlatel" name="numberPlate" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/trackController.php?action=viewTrack',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){

}"></select></td></tr>
<tr><td>
<label>Third Party(DOI):</label></td><td><input type="text"  name="tpDoi" value="" id="tpDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;"  /></td></tr>
<tr><td>
<label>Third Party(DOE)</label></td><td><input type="text"  name="tpDoe" value="" id="tpDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Caliberation(DOI):</label></td><td><input type="text"  name="cDoi" value="" id="cDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;"  /></td></tr>
<tr><td>
<label>Caliberation(DOE)</label></td><td><input type="text"  name="cDoe" value="" id="cDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Fire exhauster (DOI)</label></td><td><input type="text"  name="fDoi" value="" id="fDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Fire exhauster (DOE)</label></td><td><input type="text"  name="fDoe" value="" id="fDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Pressure Test(DOI)</label></td><td><input type="text"  name="pDoi" value="" id="pDoi" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Pressure Test(DOE)</label></td><td><input type="text"  name="pDoe" value="" id="pDoe" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barLicenseManagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveLicenseManagement()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgLicenseManagement').dialog('close')">Close</a>
</div>
<script type="text/javascript">

function saveLicenseManagement()
{
	
		
		
		
		$.messager.progress();
	 $('#frmLicenseMaintainance').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Service information add'});
						$('#dlgLicenseManagement').dialog('close'); // close the dialog
						 $('#dgLicenseManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}
</script>
