<div id="dlgLicenseManagement" class="easyui-dialog" style="height:auto; width:500px; " closed='true' modal='true' buttons='#button_barLicenseManagement'>

<form id="frmLicenseMaintainance" name="frmLicenseMaintainance" method="post">
<div id="col-lg-12">
	<div class="form-group">
    <label>Course</label>
    <select id="trainingId" name="trainingId" class="easyui-combobox" data-options="url:'controller/trainingController.php?action=viewTraining',textField:'course',valueField:'id'" style="width:485px; height:30px;"></select>
    </div>
	<div class="form-group">
    <label>Driver</label>
    <select id="driverId" name="driverId" class="easyui-combobox" data-options="url:'controller/driverController.php?action=view',textField:'firstName',valueField:'id'" style="width:485px; height:30px;"></select>
    </div>
    
    <div class="form-group">
    <label>Issue Date</label>
    <input class="form-control tdate" name="issueDate" id="issueDate" value="" type="text"/>
    </div>
    <div class="form-group">
    <label>Expiry Date</label>
    <input class="form-control tdate" name="expiryDate" id="expiryDate" value="" type="text"/>
    </div>
</div>
</form>

</div>
 <div id="button_barLicenseManagement">
<a href="#" class="btn btn-success"  onclick="saveLicenseManagement()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger"  onclick="javascript:$('#dlgLicenseManagement').dialog('close')"><i class="fa fa-times"></i>Close</a>
</div>

