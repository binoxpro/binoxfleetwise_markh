
<table id="dgLicenseManagement" title="License Management" class="easyui-datagrid" style="height:450px;" url="controller/licenseController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarLicenseManageement">
<thead>
<tr>
	<th field="numberPlate" width="90" rowspan="2">Number Plate</th>
    <th colspan="2">Third Party</th>
    <th colspan="2">Caliberation</th>
    <th colspan="2">Fire Exhausit</th>
    <th colspan="2">Pressure Test</th>
</tr>
<tr>
    <th field="tpDoi" width="90">(DOI)</th>
    <th field="tpDoe" width="90">(DOE)</th>
    <th field="cDoi" width="90">(DOI)</th>
    <th field="cDoe" width="120">(DOE)</th>
    <th field="fDoi" width="90">(DOI)</th>
    <th field="fDoe" width="90">(DOE)</th>
    <th field="pDoi" width="90">(DOI)</th>
    <th field="pDoe" width="90">(DOE)</th>
</tr>
</thead>

</table>
<div id="toolBarLicenseManageement">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="newLicenseManagement()" >Add</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editLicenseMaintance()" >Edit</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="loadGrid()" >Reload All</a>
<span style="margin-left:20%;">Select Track:<select id="numberPlateSearchLicense" name="numberPlateSearchLicense" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/trackController.php?action=viewTrack',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){
searchLicenseDetails($('#numberPlateSearchLicense').combobox('getValue'));
}"></select></span>
</div>

<?php
include_once('add.php');
?>
<script type="text/javascript">
function loadGrid()
{
	$('#dgLicenseManagement').datagrid('load',{});
}
function searchLicenseDetails(searchVal)
{
	$('#dgLicenseManagement').datagrid('load',{numberPlate:searchVal});
}
function newLicenseManagement(){
		
		
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Service Management');
			
			$('#frmLicenseMaintainance').form('clear');
			
			url='controller/licenseController.php?action=add';
				
	}
	
	
	function editLicenseMaintance(){
		var row=$('#dgLicenseManagement').datagrid('getSelected');
		if(row){
			$('#dlgLicenseManagement').dialog('open').dialog('setTitle','Edit Vechile');
			$('#frmLicenseMaintainance').form('load',row);
			url='controller/licenseController.php?action=update&id='+row.Id;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a License edit'});
		}
		
	}
	
	function saveTrack(){
		
		
		
		$.messager.progress();
	 $('#frmTrack').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Truck successfully added'});
						$('#dlgTrack').dialog('close'); // close the dialog
						 $('#dgLicenseManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	$(function(){
	$('#dgLicenseManagement').datagrid({
	rowStyler:function(index,row){
		var nextR=parseInt(row.nextReading);
		var current=parseInt(row.km_reading);
		//alert(nextR+"/"+current);
		if((nextR-current)<=2000){
			return 'background-color:#F00;';
		}else{
			//return 'background-color:#F00;';
		}
	}
	
	});
	});
	
	
</script>