<div id="dlgTodayExpense" class="easyui-dialog" style="height:400px; width:700px; " closed='true' buttons='#button_barTodayExpense'>
<table id="dgTodayExpense" title="Today's Expense" class="easyui-datagrid" style="height:auto;" url="controller/transactionController.php?action=view_today_expense" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarTransaction2" showFooter="true">
<thead>
<tr>
	<th field="ACODE" width="90">Account Code</th>
    <th field="ANAME" width="90">Account Name</th>
    <th field="ANALCD" width="90">On:</th>
    <th field="REMARKS" width="90">DETAILS</th>
    <th field="type2" width="90" data-options="formatter:function(val,row){
				
			if(val=='C'){
				return row.AMOUNT;
			}else if(parseInt(val)>=0){
				return row.type2;
			}else {
				return '';
			}
			
			}">Credit</th>
    <th field="TTYPE" width="90" data-options="formatter:function(val,row){
				
			if(val=='D'){
				return row.AMOUNT;
			}else if(parseInt(val)>=0){
				return row.TTYPE;
			}else {
				return '';
			}
			
			}">Debit</th>
    
    
    
	
</tr>
</thead>

</table>
<div id="toolbarTransaction2">
Start Date:<input type="text" name="datex" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startDate"/> End Date:<input type="text" name="datex2" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="endDate" />
<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="false" onclick="viewRange()" >Search</a><br />
<!--
Expense Item:
<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="viewItem()" >Search By Expense Item</a>
<br />
Amount:
<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="viewAmount()" >Search Value</a>
-->
</div>
</div>
 <div id="button_barTodayExpense">
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgTodayExpense').dialog('close')">Close</a>
</div>
<script type="text/javascript">
function viewRange(){
	$('#dgTodayExpense').datagrid('load',{start:$('#startDate').datebox('getValue'),end:$('#endDate').datebox('getValue'),action2:'range'});
}
</script>

