
<table id="dgTransaction" title="Daliy Expense Tracking" class="easyui-datagrid" style="height:300px;" url="controller/accountController.php?action=view_expense_account" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarTransaction">
<thead>
<tr>
	
    <th field="ANAME" width="90">Account Name</th>
    <th field="ACODE" width="90">Account Code</th>
    
	
</tr>
</thead>

</table>
<div id="toolbarTransaction">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="newTransaction()" >Expend</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="newBank()" >Go to bank / Purchase Asset</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-sum" plain="true" onclick="viewTodays()" >View Today's Expense</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-sum" plain="true" onclick="openGraphy()" >Month Income Vs Expense</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="" >More</a>
</div>

<?php
include_once('add.php');
include_once('viewToday.php');
include_once('graphy.php');
?>
<script type="text/javascript">
$(function(){	
		$('#dgTransaction').datagrid({
			rowStyler:function(index,row){
				if(parseFloat(row.account_balance)<=0.00){
					return 'background-color:#FFD5D5;';
				}else{
					
					return 'background-color:#CAE4FF;';
				}
			},
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					rowStyler:function(index,row){
				if(row.account_type=='ACA'){
					return 'background:#CAE4FF;';
				}else{
					
				}
			},
					url:'controller/transactionController.php?action=view_expense_trans&expense_account='+row.ACODE,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'',
					showFooter:true,
					height:'auto',
					columns:[[{field:'HDNO',title:'Trans No:',width:50},{field:'ANALCD',title:'On:',width:70},{field:'REMARKS',title:'Detail',width:120},{field:'DATEX',title:'Date',width:90},{field:'type2',title:'Credit',width:90,formatter:function(val,row){
				
			if(val=='C'){
				return row.AMOUNT;
			}else if(parseInt(val)>=0){
				return row.type2;
			}else {
				return '';
			}
			
			}},{field:'TTYPE',title:'Debit',width:90,formatter:function(val,row){
				
			if(val=='D'){
				return row.AMOUNT;
			}else if(parseInt(val)>=0){
				return row.TTYPE;
			}else {
				return '';
			}
			
			}}]],
					onResize:function(){
						$('#dgTransaction').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgTransaction').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgTransaction').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						$('#dgTransaction').datagrid('expandRow',0);	
						}
		});
	});
	function newTransaction(){
		
		var row=$('#dgTransaction').datagrid('getSelected');
		if(row){
			$.post('controller/accountController.php?action=getCashAtHand',{account:'Cash at hand'},function(data){
				var newstr=data.split('/');
				if(newstr[1]<=20000){
					
					$.messager.confirm('Warning','Cash at hand is Low!!! Would you like to Go to Bank',function(r){
						if(r){
							newBank();
						}
						
						});
					
				}else{
			$('#dlgExpense').dialog('open').dialog('setTitle','Expense On'+row.ANAME);
			
			$('#frmExpense').form('load',row);
			$('#amount').numberbox('setValue','');
			$('#cashCode').val(newstr[0]);
			$('#cashBalance').val(newstr[1]);
			//var id=get_log_id();
			//alert(id);
		//	get_log_id();
			//prepare_datagrid();
			//$('#header_account').val(row.account_code);
			url='controller/transactionController.php?action=saveExpense'
				}
			});
				
				
			
		}else{
			$.messager.show({title:'Help',msg:'Please Select the Account to spread On:'});
		}
	}
	function saveExpense(){
		
		
		$.messager.progress();
	 $('#frmExpense').form('submit',{ url: url, 
	 onSubmit: function(){
		
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Expense Saved'});
						$('#dlgExpense').dialog('close'); // close the dialog
						 $('#dgTransaction').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

	}
	function newBank(){
		
		
			$('#dlgBank').dialog('open').dialog('setTitle','Go to Bank / Purchase from Bank');
			
			$('#frmBank').form('clear');
			
			url='controller/transactionController.php?action=saveBank';
				
	}
	function saveBank(){
		
		
		
		$.messager.progress();
	 $('#frmBank').form('submit',{ url: url, 
	 onSubmit: function(){
		
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Fund With Drawn'});
						$('#dlgBank').dialog('close'); // close the dialog
						 $('#dgTransaction').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	function viewTodays(){
		
		$('#dlgTodayExpense').dialog('open').dialog('setTitle','Today"s Expense');	
	
	}
	function openGraphy(){
		$('#dlgGraphy').dialog('open').dialog('setTitle','Income Vs Expense');
		//getExpense();
		drawGrahpy();
	}
	
</script>