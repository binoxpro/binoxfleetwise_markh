<div id="dlgExpense" class="easyui-dialog" style="height:400px; width:500px; " closed='true' buttons='#button_barExpense'>
<div class="easyui-panel" title="XX">
<form id="frmExpense" name="frmExpense" method="post">
<div><label style="margin-right:5px;">Expense Account:&nbsp;&nbsp;</label><input type="hidden" name="ACODE" id="ACODE" value="" /><input type="text" name="ANAME" id="ANAME" value="" readonly /></div>
<div><label style="margin-right:109px;">On:</label>&nbsp;&nbsp;<select name="on" style="width:100px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'analysisCode',textField:'clientName',url:'controller/analysisCodeController.php?action=view_coa',columns:[[{field:'analysisCode',title:'Code',width:120},{field:'clientName',title:'Actual',width:130}]]"></select></div>
<div><label style="margin-right:88px;">Amount:</label><input type="text" name="amount" class="easyui-numberbox" id="amount" required onblur="getBal(this.value)"  /></div>
<div><label style="margin-right:53px;">Cash Balance:</label><input type="text" name="cashBalance" id="cashBalance" value="" readonly/> </div>
<div><label>Detail</label><br /><textarea name="details" cols="15" rows="5" style="margin-left:139px;"></textarea></div>
<div><label style="margin-right:106px;">Date </label><input type="text" name="trans_date" value="" id="trans_date" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" /></div>
<div><label style="margin-right:68px;">Cash From:</label><input type="hidden" name="cashCode" id="cashCode" value="" /><input type="text" name="cashamount" id="cashamount" value="Cash At Hand" /></div>

</form>
</div>
</div>
 <div id="button_barExpense">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveExpense()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgExpense').dialog('close')">Close</a>
</div>
<?php require_once('addBank.php');?>
<script type="text/javascript">
function getBal(x){
	var bal=0;
	bal=(parseInt($('#cashBalance').val())-parseInt(x));
	if(bal>=0){
		
	$('#cashBalance').val(bal);
	}else{
		getActualBal();
		$('#amount').numberbox('setValue','');
	}
}
function getActualBal(){
	$.post('controller/accountController.php?action=getCashAtHand',{account:'Cash at hand'},function(data){
				var newstr=data.split('/');
				$('#cashBalance').val(newstr[1]);
	});
}
</script>