<div id="dlgBank" class="easyui-dialog" style="height:400px; width:500px; " closed='true' buttons='#button_barBank'>
<div class="easyui-panel" title="Go to Bank">
<form id="frmBank" name="frmBank" method="post">
<div><label style="margin-right:5px;">Bank&nbsp;&nbsp;</label><select name="creditAccount" style="width:200px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'ACODE',textField:'ANAME',url:'controller/accountController.php?action=view_cash_book_payment',columns:[[{field:'ACODE',title:'CODE',width:50},{field:'ANAME',title:'ACCOUNT',width:120},{field:'CBAL',title:'Current Balance',width:120}]]"></select></div>
<div><label style="margin-right:2px;">To:</label><select name="debitAccount" style="width:200px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'ACODE',textField:'ANAME',url:'controller/accountController.php?action=view',columns:[[{field:'ACODE',title:'Code',width:120},{field:'ANAME',title:'CASH ACCOUNT',width:120},{field:'CBAL',title:'Current Balance',width:120}]]"></select></div>
<div><label style="margin-right:5px;">Amount:</label><input type="text" name="amount" class="easyui-numberbox" id="amount" required /></div>
<div><label>Detail</label><br /><textarea name="details" cols="15" rows="5" style="margin-left:30px;"></textarea></div>
<div><label style="margin-right:5px;">Date </label><input type="text" name="trans_date" value="" id="trans_date" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" /></div>


</form>
</div>
</div>
 <div id="button_barBank">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveBank()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgBank').dialog('close')">Close</a>
</div>
