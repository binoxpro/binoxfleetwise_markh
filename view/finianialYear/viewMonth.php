<div id="dlgMonth" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barMonth'>
<table id="dgMonth" title="Finanical Month" class="easyui-datagrid" style="height:400px;" url="controller/finanicalController.php?action=view_month" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarMonth" pageSize="10" iconCls="icon-save">
<thead>
<tr>
	<th field="monthId" width="90">Month Code</th>
    <th field="startDate" width="90">Start Date</th>
    <th field="endDate" width="90" >End Date</th>
    <th field="status" width="90" >Status</th>
	
</tr>
</thead>

</table>
<div id="toolbarMonth">
<a href="#" class="btn btn-primary" onclick="openMonth()" ><i class="icon-eye-open"></i>Open Month</a>
<a href="#" class="btn btn-link" onclick="closeMonth()" ><i class="icon-wrench"></i>Close Month</a>
</div>
</div>
<div id="button_barMonth">
<a href="#" class="btn btn-danger"  onclick="javascript:$('#dlgMonth').dialog('close')"><i class="icon-remove"></i>Close</a>
</div>
<script type="text/javascript">
function openMonth(){
	
		var row=$('#dgMonth').datagrid('getSelected');
		if(row){
			$.messager.confirm('Warning','Would You like to Open',function(r){
				if(r){
			$.post('controller/finanicalController.php',{monthId:row.monthId,action:'openMonth'},function(data){
				
				//var data = eval('('+data+')');
				 if(data.success){
				$.messager.show({title:'Message',msg:'Month Open'});
				$('#dgMonth').datagrid('reload'); 
					 
				 }else{
					$.messager.show({title:'Message',msg:data.msg}); 
				
				 }
			},'json');
			
				}else{
					
				}
			});
		}else{
			$.messager.show({title:'Message',msg:'Select  a month to open'});
		}
	
}
$(function(){
	$('#dgMonth').datagrid({
		rowStyler:function(index,row){
    	if(row.status=='ON'){
        return 'background-color:#F4007A;';
        }
		}
    
		});
		
	
});

</script>
