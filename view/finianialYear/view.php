<div class="col-lg-12">
<table id="dgFinanicalYear" title="Manage Finanical Year" class="easyui-datagrid" style="height:630px;" url="controller/finanicalController.php?action=view" pagination="true" toolbar="#toolbar" rownumbers="true" fitColumns="true" singleSelect="true" iconCls='fa fa-calendar-o'>
<thead>
<tr>
	<th field="year_code" width="90">YEAR</th>
    <th field="startDate" width="90">Start Date</th>
    <th field="endDate" width="90">End Date</th> 
    <th field="status" width="90">Active</th>        
</tr>
</thead>

</table>
<div id="toolbar">
<a href="#" class="btn btn-primary" onclick="newFinanicalYear()" ><i class="fa fa-plus-circle"></i>Add Year</a>
<a href="#" class="btn btn-link" onclick="activtyFinanicalYear()" ><i class="fa fa-openid"></i>Open Finanical Year</a>
<a href="#" class="btn btn-link" onclick="activityMonth()" ><i class="fa fa-calendar-o"></i>Open Finanical Month</a>
</div>
</div>
<script type="text/javascript">
function newFinanicalYear(){
			$('#dlgFinanicalYear').dialog('open').dialog('setTitle','School Plus Version 1.0');
			$('#frmFinanicalYear').form('clear');
			url='controller/finanicalController.php?action=add';
	}
		function saveFinanicalYear(){
		
		$.messager.progress();
	 $('#frmFinanicalYear').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Academic Year Saved'});
						$('#dlgFinanicalYear').dialog('close'); // close the dialog
						 $('#dgFinanicalYear').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
$(function(){	
		$('#dgFinanicalYear').datagrid({
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					url:'controller/finanicalController.php?action=view_month&year_code='+row.year_code,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'',
					height:'auto',
					columns:[[{field:'monthId',title:'Month',width:50},{field:'startDate',title:'Start Date',width:70},{field:'endDate',title:'endDate',width:90},{field:'yearCode',title:'yearCode',width:90},{field:'status',title:'Status',width:90}]],
					onResize:function(){
						$('#dgFinanicalYear').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgFinanicalYear').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgFinanicalYear').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						$('#dgFinanicalYear').datagrid('expandRow',0);	
						}
		});
	});
	function activtyFinanicalYear(){
		var row=$('#dgFinanicalYear').datagrid('getSelected');
		if(row){
			$.messager.confirm('Warning','Would You like to activity Year',function(r){
				if(r){
			$.post('controller/finanicalController.php',{yearCode:row.year_code,action:'activityYear'},function(data){
				
				//var data = eval('('+data+')');
				 if(data.success){
				$.messager.show({title:'Message',msg:'Year Activity'});
				$('#dgFinanicalYear').datagrid('reload'); 
					 
				 }else{
					$.messager.show({title:'Message',msg:data.msg}); 
				
				 }
			},'json');
			
				}else{
					
				}
			});
		}else{
			$.messager.show({title:'Message',msg:'Select finanical year to activity'});
		}
	}
	function activityMonth(){
		var row=$('#dgFinanicalYear').datagrid('getSelected');
		if(row){
			if(row.status=="ON"){
				$('#dlgMonth').dialog('open').dialog('setTitle','Open And Close Month');
				$('#dgMonth').datagrid('load',{year_code:row.year_code});
			}else{
				$.messager.show({title:'Message',msg:'Year is not Activity '});
			}
		}else{
			
			
		}
	}
		</script>
<?php include_once('add.php');
include_once('viewMonth.php');
//require('view/academicTerm/view.php');
?>