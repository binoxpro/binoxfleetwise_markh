    <table id="dgTyre" title="Manage Tyre" class="easyui-datagrid" style="height:500px;" url="controller/tyreController.php?action=view" pagination="true" toolbar="#toolbarTyre" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<!--<th field="id" width="50">id</th>-->
            <th field="vehicleId" width="120">Vehicle</th>
            <th field="tyretypeId" width="150">Tyre Type</th>
            <th field="fixon" width="90">Fix On</th>
            <th field="fixDate" width="80">Fix Date</th>
            <th field="removalon" width="100">Removal On</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarTyre" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newTyre()"><i class="fa fa-plus-circle"></i>Add Tyre</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="editTyre()"><i class="fa fa-pencil"></i>Edit Tyre</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Tyre</a>
    </div>
    <?php include_once('add.php'); ?>
    
