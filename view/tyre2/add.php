<div id="dlgTyre" class="easyui-dialog" closed='true' style="width:500px;" buttons='#Tyrebutton_bar' modal="true">
<form id="frmTyre" name="frmTyre" method="post">
<div id="col-lg-6" style="padding:5px;">
    <div class="form-group" style="padding-right:50%;" >
    <label>Vehicle:</label>
    <select name="vehicleId" id="vehicleId" class="easyui-combobox" style="height:auto; width:150px;" data-options=       			   "url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>Tyre Type:</label>
    <select name="tyretypeId" id="tyretypeId" class="easyui-combobox" style="height:auto; width:150px;" data-options=       			   "url:'controller/tyreTypeController.php?action=view',valueField:'id',textField:'tyretypeName',panelWidth:'120',panelHeight:'auto'" ></select>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>Fix On:</label>
    <input class="form-control" name="fixon" id="fixon" value="" type="text"/>
    </div>
    
    <div class="form-group" style="padding-right:50%;">
     <label>Fix Date:</label>
    <input name="fixDate" id="fixDate" value="" type="text" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:50%;"/>
    </div>
    <div class="form-group" style="padding-right:50%;">
     <label>Cost:</label>
    <input name="CostTyre" id="CostTyre" value="" type="text" />
    </div>
    <div class="form-group" style="padding-right:50%;">
     <label>Tyre Name(Supplier Name):</label>
    <input name="tyreName" id="tyreName" value="" type="text" />
    </div>
 
    <div class="form-group" style="padding-right:50%;">
    <label>Expected Mileage </label>
    <input class="form-control" name="removalon" id="removalon" value="" type="text"/>
    </div>
  
</div>
</form>
</div>
<div id="Tyrebutton_bar">
<a href="#" class="btn btn-primary" onclick="saveTyre()">Save</a>
<a href="#" class="btn btn-primary">Close</a>
</div>

