<div id="dlgAnalysisCode" class="easyui-dialog" style="height:auto; width:500px; padding:5px; " closed='true' buttons='#button_bar' modal="true">
<form id="frmAnalysisCode" name="frmAnalysisCode" method="post">
<table style="width:100%;">

<tr><td><label>Category Code:</label></td><td><select id="categoryCode" name="categoryCode" class="easyui-combobox" data-options="url:'controller/analysisCategoryController.php?action=view',textField:'categoryDetail',valueField:'categoryCode',onSelect:function(){

}"></select></td></tr>
<tr><td><label>Analysis Code:</label></td><td><input type="text" name="analysisCode" value="" id="analysisCode" /></td></tr>
<tr><td><label>Analysis Name:</label></td><td><input type="text"  name="analysisName" value="" id="analysisName" /></td></tr>
</table>
</form>
</div>
 <div id="button_bar">
<a href="#" class="btn btn-success" onclick=" saveAnalysisCode()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgAnalysisCode').dialog('close')"><i class="fa fa-times"></i>Cancel</a>
</div>
