<div class="col-lg-12">
<table id="dgAnalysisCode" title="AnalysisCode" class="easyui-datagrid" style="height:700px;" url="controller/analysisCodeController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarAnalysisCode" pageSize="10" iconCls="fa fa-power-off">
<thead>
<tr>
	<th field="analysisCode" width="90">Code</th>
    <th field="analysisName" width="90">Description</th>
    <th field="currentBalance" width="90">Balance</th>
    <th field="categoryCode" width="90">Belong to</th>
</tr>
</thead>

</table>
<div id="toolbarAnalysisCode">
<a href="#" class="btn btn-primary" onclick="newAnalysisCode()" ><i class="fa fa-plus-circle"></i>Add AnalysisCode</a>
<a href="#" class="btn btn-link" onclick="editAnalysisCode()" ><i class="fa fa-edit"></i>Edit AnalysisCode</a>
</div>
</div>
<?php
include_once('addCode.php');
?>