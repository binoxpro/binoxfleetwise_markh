<div class="col-lg-12">
<table id="dgAnalysisCategory" title="AnalysisCategory" class="easyui-datagrid" style="height:700px;" url="controller/analysisCategoryController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarAnalysisCategory" pageSize="10" iconCls="fa fa-code">
<thead>
<tr>
	<th field="categoryCode" width="90">Category Code</th>
    <th field="categoryDetail" width="90">Description</th>
</tr>
</thead>

</table>
<div id="toolbarAnalysisCategory">
<a href="#" class="btn btn-primary" onclick="newAnalysisCategory()" ><i class="fa fa-plus-circle"></i>Add AnalysisCategory</a>
<a href="#" class="btn btn-link" onclick="editAnalysisCategory()" ><i class="fa fa-edit"></i>Edit AnalysisCategory</a>
<a href="#" class="btn btn-link disabled" onclick="" ><i class="fa fa-file-o"></i>View Legder</a>
</div>
</div>
<?php
include_once('add.php');
?>