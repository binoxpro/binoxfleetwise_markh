<div id="dlgAccountability" class="easyui-dialog" style="height:auto; width:550px; padding:5px; " closed='true' buttons='#button_barac'>
<form id="frmAccountability" name="frmAccountability" method="post">
<table width="100%"><tr><td><label>Item Name:</label></td><td><input type="hidden" name="internalBudgetItemId" value="" id="internalBudgetItemId" /><input type="text" name="itemNameBudget" value="" id="itemNameBudget" readonly="readonly" /></td></tr>
<tr><td>Previous Amount Paid:</td><td><input type="text"  name="actualPaid" value="" id="actualPaid" readonly="readonly"/></td></tr>
<tr><td><label>Amount:</label></td><td><input type="text"  name="AmountAccount" value="" id="AmountAccount" onblur="getBalanceSpent(this.value)" /><input type="hidden"  name="actualBalance" value="" id="actualBalance" /></td></tr>
<tr><td><label>Balance:</label></td><td><input type="text"  name="balanceAccount" value="" id="balanceAccount" readonly="readonly" /></td></tr>
<tr><td><label>Date</label></td><td><input type="text" name="dateAccount" value="" id="dateAccount" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" /></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barac">
<a href="#" class="btn btn-success" onclick="saveAccountability()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgAccountability').dialog('close')"><i class="icon-remove"></i>Cancel</a>
</div>
<script type="text/javascript">
function getBalanceSpent(x)
{
	
	$('#balanceAccount').val(parseInt($('#actualBalance').val())-parseInt(x));
}
function saveAccountability(){
		//url='controller/jobController.php?action=add';
		$.messager.progress();
	 $('#frmAccountability').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Accountability posted'});
						$('#dlgAccountability2').dialog('close'); // close the dialog
						$('#dgAccountability').datagrid('load'); // reload the user
		}
		
	}
}); 
		
}

</script>