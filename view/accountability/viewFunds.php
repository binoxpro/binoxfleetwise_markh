<div id="dlgAccountabilityFund" class="easyui-dialog" style="height:500px; width:700px;" closed='true' buttons='#button_barAccountability'>
<table id="dgAccountability" title="Accountability" class="easyui-datagrid" style="height:auto;" url="controller/accountabilityController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarAccountability" pageSize="10">
<thead>
<tr>
	<th field="itemName" width="90">Item Name</th>
    <th field="Amount" width="90">Amount</th>
    <th field="ActualDeposit" width="90">Actual Deposit</th>
    <th field="BalanceDeposit" width="90">Balance</th>
</tr>
</thead>
</table>
<div id="toolbarAccountability">
<a href="#" class="btn btn-primary"  onclick="newAccountability()"><i class="icon-plus-sign"></i>Pay Supplier</a>
<span>Released Funds:<input type="text" name="releaseFundAmount" value="" id="releaseFundAmount" />

</span>
</div>

</div>
<div id="button_barAccountability">
<a href="#" class="btn btn-primary" onclick="javascript:$('#dlgAccountabilityFund').dialog('close')">Close</a>
</div>

<?php
include_once('view/accountability/add.php');
?>
<script type="text/javascript">
function newAccountability()
{
	var row=$('#dgAccountability').datagrid('getSelected');
	if(row)
	{
		if(row.BalanceDeposit>0){
	$('#dlgAccountability').dialog('open').dialog('setTitle','Account for payments made');	
	$('#itemNameBudget').val(row.itemName);
	$('#internalBudgetItemId').val(row.Id);
	$('#actualBalance').val(row.BalanceDeposit);
	$('#actualPaid').val(row.ActualDeposit);
	url="controller/accountabilityController.php?action=add&Id="+row.jobID;
		}else{
		$.messager.show({title: 'Warning',msg: 'You can not pay a zero balance'});	
		}
	}else
	{
		$.messager.show({title: 'Warning',msg: 'Please Select an item to pay off'});
	}
}
</script>
