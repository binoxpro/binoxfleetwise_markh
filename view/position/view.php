<div class="col-lg-12">
<table id="dgPosition" title="Positions" class="easyui-datagrid" style="height:700px;" url="controller/positionController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarPosition" pageSize="10">
<thead>
<tr>
	<th field="positionID" width="90">Product ID</th>
    <th field="positionName" width="90">Product Name</th>
    <th field="allowance" width="90">Allowance</th>
    <th field="salary" width="90">Salary</th>
    <th field="other" width="90">Other</th>
</tr>
</thead>

</table>
<div id="toolbarPosition">
<a href="#" class="btn btn-primary" onclick="newPosition()" ><i class="icon-plus-sign"></i>New Position</a>
<a href="#" class="btn btn-link" onclick="editPosition()" ><i class="icon-edit"></i>Edit Position</a>
<a href="#" class="btn btn-link" onclick="deletePoition()" ><i class="icon-minus-sign"></i>Delete Positon</a>
</div>
</div>
<?php
include_once('add.php');
?>
<script type="text/javascript">
function newPosition(){
	$('#dlgPosition').dialog('open').dialog('setTitle','Add A Position:');
	$('#frmPosition').form('clear');
	url='controller/positionController.php?action=add';
}
function editPosition(){
	var row=$('#dgPosition').datagrid('getSelected');
	if(row){
	$('#dlgPosition').dialog('open').dialog('setTitle','Edit A Department:');
	$('#frmPosition').form('load',row);
	url='controller/positionController.php?action=update&id='+row.positionID;
		
	}else{
		$.messager.show({title:'Info',msg:'select a position  to edit'});
	}
}
function savePosition(){
		
		$.messager.progress();
	 $('#frmPosition').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Position Saved'});
						$('#dlgPosition').dialog('close'); // close the dialog
						 $('#dgPosition').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function deletePosition(){
	$.messager.alert('Warning','This operation was not secure so it was turned off for  more information call help line:0781587081');
}

</script>