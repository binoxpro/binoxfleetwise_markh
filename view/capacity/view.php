    <table id="dgCapacity" title="Manage Capacity Utilisation" class="easyui-datagrid" style="height:700px;" url="controller/capacityController.php?action=view" pagination="true" toolbar="#toolbarCapacity" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-graph">
    	<thead>
        	<tr>
        	<th field="id" width="50">Record No</th>
            <th field="creationDate" width="100">Date</th>
            <th field="tripNo" width="150">Trip</th>
            <th field="regNo" width="120">Registration Number</th>
            <th field="vpower" width="90">Vpower</th>
            <th field="ago" width="80">Ago</th>
            <th field="bik" width="100">Bik</th>
            <th field="ugl" width="100">Ugl</th>
            <th field="timeLoading" width="100">Time</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarCapacity">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newCapacity()"><i class="fa fa-plus-circle"></i>Enter Loaded Capacity</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="editCapacity()"><i class="fa fa-pencil"></i>Edit Loaded Capacity </a>
        <div>
        <span>
        <table width="60%"><tr><td>Registration Number:</td><td><input type="text" name="searchQuick" id="quickSearch"stype="width:70%;"   onkeyup="searchVehicle(this.value)"/></td></tr></table>
        </span>
        </div>
    </div>
    <?php include_once('add.php'); ?>
    
