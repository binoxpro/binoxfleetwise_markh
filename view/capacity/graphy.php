<div id="dlgUtilisationGraph" class="easyui-dialog" closed='true' buttons='#capacitybutton_bar' modal="true" style="height:600px; width:870px; padding:5px;" >
<h1 class="label label-info text-info text-center" style="width:400px; height:40px; font-size:18px; padding-top:5px; margin-left:30%;" id="graphTitle"></h1>
<div><h5>KEY:</h5><table><tr><td><div style="width:10px; height:10px; background:#999;"></div></td><td>Utilisation in Percentage</td></tr><tr><td><div style="width:10px; height:10px; background:#036;"></div></td><td>Expense</td></tr></table></div>
<canvas id="canvas" height="250" width="500"></canvas>
</div>
</div>
<div id="capacitybutton_bar">
</div>
