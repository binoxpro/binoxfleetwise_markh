    <table id="dgCapacityUtilisation" title="Manage Capacity Utilisation" class="easyui-datagrid" style="height:700px;" url="controller/capacityController.php?action=viewUtilisation" pagination="true" toolbar="#toolbarCapacityUtilisation" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-graph" data-options="rowStyler:function(index,row){
if(row.fillColor!=''){
	return'font-weight:bold;background-color:'+row.fillColor;
}
}">
    	<thead>
        	<tr>
        	<th field="id" width="50">Record No</th>
            <th field="creationDate" width="100">Date</th>
            <th field="tripNo" width="150">Trip</th>
            <th field="regNo" width="120">Registration Number</th>
            <th field="vpower" width="90">Vpower</th>
            <th field="ago" width="80">Ago</th>
            <th field="bik" width="100">Bik</th>
            <th field="ugl" width="100">Ugl</th>
            <th field="percent" width="100">Utilisation%</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarCapacityUtilisation">
    	
        <span>
        Registration Number:<select name="regNo" id="regNo" class="easyui-combobox form-control" style="height:25px; width:20%;" data-options="url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
        </select>
        &nbsp;&nbsp;Starting<input type="text" name="startDate" value="" id="startDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />&nbsp;&nbsp;Ending<input type="text" name="endDate" value="" id="endDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />
        <a href="#" class="btn btn-primary btn-sm" onclick="viewSearchC()"><i class="fa fa-search"></i></a>
        <a href="#" class="btn btn-primary btn-sm" onclick="viewGraph()"><i class="fa fa-graph"></i>Graph View</a>
        
        </span>
        
    </div>
    <?php include_once('graphy.php'); ?>
    
