<div class="col-lg-12">
	<h3>Driver Details Report</h3>
    <div class="row">
    <div class="col-lg-4" ><div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <a href="#" class="btn btn-primary btm-sm" onclick="exportToExcel()">Export <i class="fa fa-file-excel-o"></i></a></div>
    </div>
    <div class="row">
    <div id="driverDetail" style="overflow:auto;"></div>
    </div>
 </div>
