    
    <div class="col-lg-12" style="margin-top:10px;">
    <table id="dgTyreType" title="Manage Tyre Type" class="easyui-datagrid" style="height:500px;" url="controller/tyreTypeController.php?action=view" pagination="true" toolbar="#toolbarTyreType" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="tyretypeName" width="120">Tyre Type Name</th>
            <th field="tyreIndex" width="120">Tyre Index</th>
            <th field="rotationIndex" width="120">Rotation Position</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarTyreType">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newTyreType()"><i class="fa fa-plus-circle"></i>Add Tyre Type</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="editTyreType()"><i class="fa fa-pencil"></i>Edit Tyre Type</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="deleteTyreType()"><i class="fa fa-times-circle"></i>Delete Tyre Type</a>
    </div>
    </div>
    <?php include_once('add.php'); ?>
    
