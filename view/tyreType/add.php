<div id="dlgTyreType" class="easyui-dialog" closed='true' style="width:500px;" toolbar='#Tyrebutton_bar' modal="true" >
<form id="frmTyreType" name="frmTyreType" method="post">
<div id="col-lg-6" style="padding:5px;">

    <div class="form-group" style="padding-right:50%;" >
    <label>Tyre Type Name</label>
    <input class="form-control" name="tyretypeName" id="tyretypeName" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;" >
    <label>Index</label>
    <input class="form-control" name="tyreIndex" id="tyreIndex" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;" >
    <label>Rotation </label>
     <select id="rotationPosition" name="rotationPosition" class="easyui-combobox form-control" style="width:100%;height:30px;" data-options="url:'controller/tyreTypeController.php?action=view',textField:'tyretypeName',valueField:'id'"></select>
    </div>
    
</div>
</form>
</div>
<div id="Tyrebutton_bar">
<a href="#" class="btn btn-primary" onclick="saveTyreType()">Save</a>
<a href="#" class="btn btn-primary">Close</a>
</div>

