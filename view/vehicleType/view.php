    <table id="dgVehicleType" title="Manage Vehicle Type" class="easyui-datagrid" style="height:700px;" url="controller/vehicleTypeController.php?action=view" pagination="true" toolbar="#toolbarVehicleType" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="vehicleType" width="120">Configuration Type</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarVehicleType">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newVehicleType()"><i class="fa fa-plus-circle"></i>Add Vehicle</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i>Edit Vehicle</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Vehicle</a>
    </div>
    <?php include_once('add.php'); ?>
    
