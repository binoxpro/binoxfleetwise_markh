<div id="dlgInvoiceAddInvoice" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barInvoiceAddInvoice'>
<div class="easyui-panel" title="Create Out going Budget" style="height:400px;">
<form id="frmInvoiceAddInvoice" name="frmInvoiceAddInvoice" method="post">
<table cellspacing="4" style="width:90%;">
<tr><td><label>Job ID:</label></td><td><input type="text" name="jobNo" value="" id="jobNoInvoice" class="easyui-validatebox" required/></td></tr>
<tr><td><label>Client</label></td><td><input type="text" name="client" value="" id="clientInvoice" class="easyui-validatebox" required/></td></tr>
<tr><td><label>Details:</label></td><td><textArea name="details" id="detailsInvoice"></textArea></td></tr>
<tr><td><label>Date:</label></td><td><input type="text" name="regDate" value="" id="regDateInvoice" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser"/>Income:<input type="text" name="income" id="incomeInvoice" value="" class="easyui-validatebox" readonly required /></td></tr>
<tr><td><label>Invoice No:</label></td><td><input type="text" name="budgetNo" value="" id="budgetNoInvoice" /></td></tr>
<tr><td><label>Product Line:</label></td><td><select name="product" id="productInvoice" class="easyui-combobox" style="width:100px; height:auto;" data-options="url:'controller/positionController.php?action=showDepartments',valueField:'departNo2',textField:'departName',panelWidth:'120',panelHeight:'auto'" ></select> </td></tr>
</table>

</form>
<div>
<h4>Create Invoice:</h4>
<?php require_once('addInvoiceTax.php') ?>
</div>
</div>
</div>
 <div id="button_barInvoiceAddInvoice">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveInvoiceJob()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgInvoiceAddInvoice').dialog('close')">Close</a>
</div>