<table id="dgBudgetInvoice" class="easyui-datagrid" title="Out going Budget" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbBudgetInvoice',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'subitemNo',width:50" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:120, editor:{type:'text'}">Item</th>
				<th data-options="field:'qty',width:100,
						editor:{
							type:'numberbox'
						}">Quantity</th>
				<th data-options="field:'unitPrice',width:140,align:'right',editor:'numberbox'">Rate</th>
				<th data-options="field:'nodays',width:100,align:'right',editor:'numberbox'">No Days</th>
				<th data-options="field:'total',width:100,editor:{type:'numberbox'}">Total</th>
                <th data-options="field:'headNo',width:100,editor:{type:'text'}" hidden="false">headerNo</th>
			</tr>
		</thead>
	</table>

	<div id="tbBudgetInvoice" style="height:auto">
    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="addHeader()">Add Header</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="javascript:$('#dgBudgetInvoice').edatagrid('addRow')">Add Item</a>
		
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="javascript:$('#dgBudgetInvoice').edatagrid('saveRow');reloadDataGrid();reloadSummary2();setTotal()">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="deleteItem();reloadSummary2();setTotal()">Remove</a><br />
        ID:<input type="text" name="headerNo" id="headerNo" value="" readonly="readonly"  />&nbsp;&nbsp;&nbsp;item:<input type="text" name="itemName" id="itemName" value="" readonly="readonly"  />
	</div>
	<?php require_once('addBudgetHeader.php'); ?>
	<script type="text/javascript">
	function addHeader(){
		$('#dlgBudgetHeaderInvoice').dialog('open').dialog('setTitle','Add Budget Header');
		$('#frmBudgetHeader').form('clear');
		$('#budgetNo2').val($('#budgetNo').val());
		
	}
	function saveHeader(){
		var budgetNo=$('#budgetNo2').val();
		var itemName=$('#itemName2').val();
		if(itemName!=null || itemName!=''){
		$.post('controller/budgetController.php?action=addHeader&budgetNo='+budgetNo+"&itemName="+itemName,function(data){
			
			if(data==null){
				$.messager.alert('warning','Header ');
			}else{
				$('#headerNo').val(data);
				$('#itemName').val(itemName);
				$('#dlgBudgetHeaderInvoice').dialog('close');
			}
			
		});
		}else{
			$.messager.alert('Warning','supply the Header Item Name');
		}
	}
	function prepare_datagrid(){
			var id=$('#budgetNo').val();
			var budgetNo=$('#budgetNo').val();
			
			
		
			$('#dgBudgetInvoice').edatagrid({
				method:'get',
				url: 'controller/budgetController.php?action=viewItem&budgetNo='+budgetNo,
				saveUrl:  'controller/budgetController.php?action=addItem&budgetNo='+budgetNo,
				updateUrl:  'controller/budgetController.php?action=updateItem&budgetNo='+budgetNo,
				onAdd:function(index,row){
					var head=$('#headerNo').val();
					if(head!=null && head!=''){
					set_values(index);
					}else{
						$.messager.alert('Warning','Supply the header catergory');
						$('#dgBudgetInvoice').edatagrid('reload');
					}
				},
				onEdit:function(index,row){
					calcu(index)
				}
			});
			
		}
		function get_log_id(){
		/*
		$.post('controller/transactionController.php',{action:'addLog'},function(response){
			//alert(response);
			$('#log_id').val(response);
			var id=response;
			$('#dgBudgetInvoice').edatagrid({
				url:'controller/transactionController.php?action=view_temp&log='+id,
				saveUrl: 'controller/transactionController.php?action=add_temp&log='+id,
				updateUrl: 'controller/transactionController.php?action=update_temp&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy_temp&log='+id
			});
			
		});
		*/
		
	}
	function reloadDataGrid(){
		$('#dgBudgetInvoice').edatagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
	function calcu(rowIndex){
		
		var editors=$('#dgBudgetInvoice').edatagrid('getEditors',rowIndex);
		var datex=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	
	}
	function set_values(rowIndex){
		var editors=$('#dgBudgetInvoice').edatagrid('getEditors',rowIndex);
		var headno=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		items.target.blur(function(){
			var head_val=$('#headerNo').val();
			//alert(head_val);
		$(headno.target).val(head_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItem(){
		var row=$('#dgBudgetInvoice').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItem&id='+row.subItemNo,function(result){
			if(result.success){
				//$('#dgBudgetInvoice').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgBudgetInvoice').datagrid('reload',{budegtNo:id});	
}
function setTotal(){
	var id=$('#budgetNo').val();
	$.post('controller/budgetController.php?action=viewTotal&budgetNo='+id,function (data){
		if(data!=null){
		$('#income').val(data);
		}
		
	});
}
    </script>