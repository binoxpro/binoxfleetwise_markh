<table id="dgInvoiceTaxInvoice" class="easyui-datagrid" title="Proforma Invoice" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbInvoiceTaxInvoice',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'itemNo',width:50" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:120, editor:{type:'text'}">Item</th>
				<th data-options="field:'qty',width:100,
						editor:{
							type:'numberbox'
						}">Quantity</th>
				<th data-options="field:'unitPrice',width:140,align:'right',editor:'numberbox'">Rate</th>
				<th data-options="field:'no_of_days',width:100,align:'right',editor:'numberbox'">No Days</th>
				<th data-options="field:'total',width:100,editor:{type:'numberbox'}">Total</th>
               
			</tr>
		</thead>
	</table>

	<div id="tbInvoiceTaxInvoice" style="height:auto">
    
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="javascript:$('#dgInvoiceTaxInvoice').edatagrid('addRow')">Add</a>
		
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="javascript:$('#dgInvoiceTaxInvoice').edatagrid('saveRow');reloadDataGrid2();setTotalInvoice()">Save/Update</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="deleteItemInvoice();setTotal();reloadDataGrid2()">Remove Item</a><br />
        
	</div>
	<?php //require_once('addBudgetHeader.php'); ?>
	<script type="text/javascript">
	
	
	function prepare_datagridInvoice(){
			var id=$('#budgetNoInvoice').val();
			var budgetNo=$('#budgetNoInvoice').val();
			
			
		
			$('#dgInvoiceTaxInvoice').edatagrid({
				method:'get',
				url: 'controller/budgetController.php?action=viewInvoiceItem&budgetNo='+budgetNo,
				saveUrl:  'controller/budgetController.php?action=addInvoiceItem&budgetNo='+budgetNo,
				updateUrl:  'controller/budgetController.php?action=updateInvoiceItem&budgetNo='+budgetNo,
				onAdd:function(index,row){
					set_values2(index);
				},
				onEdit:function(index,row){
					calcu2(index);
				}
			});
			
		}
		
	function reloadDataGrid2(){
		$('#dgInvoiceTaxInvoice').edatagrid('reload');
	}
	function calcu2(rowIndex){
		
		var editors=$('#dgInvoiceTaxInvoice').edatagrid('getEditors',rowIndex);
		
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	
	}
	function set_values2(rowIndex){
		var editors=$('#dgInvoiceTaxInvoice').edatagrid('getEditors',rowIndex);
		
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItemInvoice(){
		var row=$('#dgInvoiceTaxInvoice').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItemInvoice&id='+row.itemNo,function(result){
			if(result.success){
				$('#dgInvoiceTaxInvoice').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgInvoiceTaxInvoice').datagrid('reload',{budegtNo:id});	
}
function setTotalInvoice(){
	var id=$('#budgetNoInvoice').val();
	$.post('controller/budgetController.php?action=viewTotalInvoice&budgetNo='+id,function (data){
		if(data!=null){
		$('#incomeInvoice').val(data);
		}
		
	});
}
    </script>