<table id="dgBudgetInternalInvoice" class="easyui-datagrid" title="Internal Budget" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbBudgetInternal',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'Id',width:50,editor:{type:'text'}" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:120, editor:{type:'text'}">Item</th>
				<th data-options="field:'qty',width:100,
						editor:{
							type:'numberbox'
						}">Quantity</th>
				<th data-options="field:'unitPrice',width:140,align:'right',editor:'numberbox'">Rate</th>
				<th data-options="field:'nodays',width:100,align:'right',editor:'numberbox'">No Days</th>
				<th data-options="field:'total',width:100,editor:{type:'numberbox'}">Total</th>
               
                <th data-options="field:'supplier',width:90,
						editor:{
							type:'combobox',
					options:{panelWidth:100,valueField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=viewCategory&id=sup'
                            }
						}">Supplier</th>
                        <th data-options="field:'contact',width:100">Contact</th>
			</tr>
		</thead>
	</table>

	<div id="tbBudgetInternal" style="height:auto">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="javascript:$('#dgBudgetInternalInvoice').edatagrid('addRow')">Add Item</a>
		
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="javascript:$('#dgBudgetInternalInvoice').edatagrid('saveRow');reloadDataGridInternal();setTotalInternal()">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteItem();reloadSummary2();setTotalInternal()">Remove</a>
         <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addSupplier()">New Supplier</a>
	</div>
	<?php 
	require_once('addCode.php'); 
	?>
	<script type="text/javascript">
	function addSupplier()
	{
		$('#dlgSupplier').dialog('open').dialog('setTitle','Add Supplier');
		$('#frmSuppier').form('clear');
		url='controller/analysisCodeController.php?action=addSupplier';
		
	}
	function prepare_Internaldatagrid(){
			//var id=$('#budgetNo').val();
			var budgetNo=$('#jobNoInternal').val();
			
			
		
			$('#dgBudgetInternalInvoice').edatagrid({
				method:'get',
				url: 'controller/budgetController.php?action=viewItemInternal&budgetNo='+budgetNo,
				saveUrl:  'controller/budgetController.php?action=addItemInternal&budgetNo='+budgetNo,
				updateUrl:  'controller/budgetController.php?action=updateItemInternal&budgetNo='+budgetNo,
				onAdd:function(index,row){
					set_valuesx(index);
					
				},
				onEdit:function(index,row){
					calcux(index)
				}
			});
			
		}
	
	function reloadDataGridInternal(){
		$('#dgBudgetInternalInvoice').edatagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
	function calcux(rowIndex){
		var editors=$('#dgBudgetInternalInvoice').edatagrid('getEditors',rowIndex);
		var headno=editors[6];
		var items=editors[1];
		var qty=editors[2];
		var noday=editors[4]
		var rates=editors[3];
		var total=editors[5];
		items.target.blur(function(){
			var head_val=$('#headerNo').val();
			
		$(headno.target).val(head_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function set_valuesx(rowIndex){
		var editors=$('#dgBudgetInternalInvoice').edatagrid('getEditors',rowIndex);
		var headno=editors[6];
		var items=editors[1];
		var qty=editors[2];
		var noday=editors[4]
		var rates=editors[3];
		var total=editors[5];
		items.target.blur(function(){
			var head_val=$('#headerNo').val();
			
		$(headno.target).val(head_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItemInternal(){
		var row=$('#dgBudgetInternalInvoice').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItem&id='+row.subItemNo,function(result){
			if(result.success){
				
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgBudgetInternalInvoice').datagrid('reload',{budegtNo:id});	
}
function setTotalInternal(){
	var id=$('#jobNoInternal').val();
	$.post('controller/budgetController.php?action=viewTotalInternal&budgetNo='+id,function (data){
		if(data!=null){
		$('#incomeInternal').val(data);
		}
		
	});
}
    </script>