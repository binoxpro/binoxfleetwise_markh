
<table id="dgJobAccount" title="Jobs" class="easyui-datagrid" style="height:300px;" url="controller/jobController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarJobAccounts" pageSize="10">
<thead>
<tr>
	<th field="jobNo" width="90">Project No</th>
    <th field="client" width="90">Client Name</th>
    <th field="income" width="90">Invoice Amount</th>
     <th field="expense" width="90">Expense Amount</th>
    <th field="issueType" width="90">Issues As</th>
    <th field="operationStatus" width="90">Editable</th>
    
</tr>
</thead>

</table>
<div id="toolbarJobAccounts">
<a href="#" class="easyui-linkbutton btn-large" iconCls="icon-add" plain="false" onclick="newBudgetJob()" >New Job / Budget</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="newInvoiceJob()" >New Job/ Proforma </a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="viewBudget();viewBudget()" >View Details</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="internalBudget()" >Attach Internal Budget</a>

<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="editBudgetJob()" >Edit Job</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="editJob()" >View Sales</a>
</div>

<?php
include_once('add.php');
include_once('addInvoice.php');
include_once('viewDetails.php');
include_once('viewTaxInvoice.php');
include_once('addLpo.php');
include_once('addInternal.php');
include_once('addRecepit.php');

?>
<script type="text/javascript">
var output;
function setDetails(bno){
	
		$.post('controller/budgetController.php?action=viewDetails&budgetNo='+bno,{actionv:'viewDetails',budgetNov:bno},function (response){
			$('#viewDetails').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsInternal(bno){
	
		$.post('controller/budgetController.php?action=viewDetails&budgetNo='+bno,{actionv:'viewDetails',budgetNov:bno},function (response){
			$('#viewDetailsInternal').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetails2(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno},function (response){
			$('#viewDetails2').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsPorformaInvoice(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno},function (response){
			$('#viewDetails').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsPorformaInvoiceInternal(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno},function (response){
			$('#viewDetailsInternal').html(response);
			//internal
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setBudgetNo(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno},function (response){
			$('#viewDetails2').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getBudgetNox(bno){
	
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			//alert(rep\
			//output=response;
			
			$('#budgetNoLpo').val(response);
			setDetails(response);
			
		});
		
		//return output;
	
}
function getBudgetNoxx(bno){
	
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			//alert(rep\
			//output=response;
			
			//$('#budgetNoLpo').val(response);
			setDetailsInternal(response);
			//viewDetailsInternal
			
		});
		
		//return output;
	
}
function getBudgetNoForEdit(bno){
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			$('#budgetNo').val(response);
			prepare_datagrid();
			
		});
}


function internalBudget()
{
	var row=$('#dgJobAccount').datagrid('getSelected');
	if(row)
	{
		$('#dlgInternalBudgetInvoice').dialog('open').dialog('setTitle','Create Internal Budget');
		
		$("#jobNoInternal").val(row.jobNo);
		prepare_Internaldatagrid();
		if(row.issueType=='budget'){
			getBudgetNoxx(row.jobNo);	
		}else{
			setDetailsPorformaInvoiceInternal(row.jobNo);
		}
		
	}else
	{
		$.messager.show({title:'Info',msg:'Select a job to define its internal budget'});
	}

}
function saveBudgetInternalBugdet(){
		url='controller/jobController.php?action=addInternal';
		$.messager.progress();
	 $('#frmBudgetInternal').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Internal Prepared'});
						//$('#dlgLpoInvocie').dialog('close'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


}
function newBudgetJob(){
	$('#dlgBudgetJobInvoice').dialog('open').dialog('setTitle','Create a budget job:');
	$('#frmBudgetJobInvoice').form('clear');
	getJobNo();
	$('#headerNo').val('');
	url='controller/jobController.php?action=add';
}

function newInvoiceJob(){
	$('#dlgInvoiceAdd').dialog('open').dialog('setTitle','Create A Invoice Job');
	$('#frmInvoiceAdd').form('clear');
	getInvoiceNo()
	//$('#headerNo').val('');
	url='controller/jobController.php?action=addInvoiceJob';
}
function editBudgetJob(){
	var row=$('#dgJobAccount').datagrid('getSelected');
	if(row){
		if(row.operationStatus=='opened'){
			if(row.issueType=='budget'){
				$('#dlgBudgetJobInvoice').dialog('open').dialog('setTitle','Edit Out going budget  for::'+row.client);
				$('#frmBudgetJobInvoice').form('load',row);
				getBudgetNoForEdit(row.jobNo);
				
				url='controller/jobController.php?action=updateBudgetJob';	
			}else{
				$('#dlgInvoiceAdd').dialog('open').dialog('setTitle','Edit A Invoice Job');
				$('#frmInvoiceAdd').form('load',row);
				$('#jobNoInvoice').val(row.jobNo)
				getInvoiceNoForEdit(row.jobNo);
	
				url='controller/jobController.php?action=updateInvoiceJob';
				
			}
	
		}else{
		$.messager.show({title:'Info',msg:'A Job is not Editable To make it editable'});	
		}
	}else{
		$.messager.show({title:'Info',msg:'Please Select A Job to edit'});
	}
}

function viewBudget(){
	var row=$('#dgJobAccount').datagrid('getSelected');
	if(row){
		if(row.operationStatus=='opened' ){
		$('#dlgViewDetailsInvoice').dialog('open').dialog('setTitle','View Budget for:: '+row.client);
			
			if(row.issueType=='budget'){
				getBudgetNox(row.jobNo);	
				var budgetNo=$('#budgetNoLpo').val();
				$('#clientLpo').val(row.client);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				$('#lop_button').linkbutton('enable');
			}else{
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('enable');
			}
	
		}else if(row.operationStatus=='confirmed' || row.operationStatus=='paid' ){
			$('#dlgViewTaxInvoiceInvoice').dialog('open').dialog('setTitle','Tax Invoice:: '+row.client);
			setDetails2(row.jobNo);
			$('#jobNoLpo23').val(row.jobNo);
			$('#productLpo23').val(row.product);
			$('#clientLpo23').val(row.client);
			$('#lop_button2').linkbutton('disable');
	
		}else{
			
		$('#dlgViewDetailsInvoice').dialog('open').dialog('setTitle','View Budget for:: '+row.client);
			
			if(row.issueType=='budget'){
				getBudgetNox(row.jobNo);	
				var budgetNo=$('#budgetNoLpo').val();
				
				$('#clientLpo').val(row.client);
				
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				
				$('#lop_button').linkbutton('disable');
			}else{
				
				
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('disable');
			}
			//$.messager.show({title:'Info',msg:'select a Job  to edit'});
		}
	}else{
		$.messager.show({title:'Info',msg:'select a Job  to edit'});
	}
}

function recevieLpo(){
	
		
	$('#dlgLpoInvocie').dialog('open').dialog('setTitle','View Budget for:: ');
	$('#frmLpoInvoice').form('clear');
	//prepare_datagrid();
	url='controller/budgetController.php?action=receviceLpo';
	$('#clientLpo2').val($('#clientLpo').val());
	$('#budgetNoLpo2').val($('#budgetNoLpo').val());
	$('#jobNoLpo2').val($('#jobNoLpo').val());
	$('#productLpo2').val($('#productLpo').val());
	
		
}
function receviePayment(){
	
		
	$('#dlgRecepit').dialog('open').dialog('setTitle','Payment of an invoice');
	$('#frmRecepit').form('clear');
	
	url='controller/transactionController.php?action=payment';
	
	$('#jobid4').val($('#jobNoLpo23').val());
	$('#productRecepit').val($('#productLpo23').val());
	getAccountBal($('#clientLpo23').val());
	getOpenYearAndMonthRecepit()
	
		
}
function getAccountBal(clientName)
{
	$.post('controller/transactionController.php?action=getAAcodeBal&clientName='+clientName,{actionx:'getAAcodeBal',clientNamex:clientName},function(response){
		//alert(response);
		str=response.split('$');
		$('#balancebefore').numberbox('setValue',str[0]);
		$('#credit').val(str[1]);
		$('#analcode').val(str[2]);
	});
}
function getOpenYearAndMonthRecepit(){
		$.post('controller/finanicalController.php',{action:'getYear'},function(data){
			//alert(data);
			var newstr=data.split('*');
			$('#year_codeRecepit').val(newstr[0]);
			$('#month_codeRecepit').val(newstr[1]);
		});
	}
	
function saveLpo(){
		
		$.messager.progress();
	 $('#frmLpoInvoice').form('submit',{ url: url, 
	 onSubmit: function(){
		
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Lpo'});
						$('#dlgLpoInvocie').dialog('close'); // close the dialog
						 $('#dgJobAccount').datagrid('reload');// reload the user
		}
		
	}
}); 
		
}

function saveBudgetJob(){
		
		$.messager.progress();
	 $('#frmBudgetJobInvoice').form('submit',{ url: url, 
	 onSubmit: function(){
		
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Job Saved'});
						$('#dlgBudgetJobInvoice').dialog('close'); // close the dialog
						$('#dgJobAccount').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function saveInvoiceJob(){
		//url='controller/jobController.php?action=add';
		$.messager.progress();
	 $('#frmInvoiceAdd').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Job Saved'});
						$('#dlgInvoiceAdd').dialog('close'); // close the dialog
						$('#dgJobAccount').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function deleteJob(){
	$.messager.alert('Warning','This operation was not secure so it was turned off for  more information call help line:0781587081');
}
function getJobNo(){
	
		$.post('controller/jobController.php',{action:'setupJob'},function (response){
			var str;
			str=response.split('*');
			$('#budgetNo').val(str[0]);
			$('#jobNo').val(str[1]);
			prepare_datagrid();
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getJobNoInternal(){
	
		$.post('controller/jobController.php',{action:'setupJob'},function (response){
			var str;
			str=response.split('*');
			//$('#budgetNo').val(str[0]);
			//$('#jobNo').val(str[1]);
			prepare_datagrid();
			//prepare_datagrid2();
			//loadSummary();
		});
	
}

function getInvoiceNo(){
	
		$.post('controller/budgetController.php',{action:'setProforma'},function (response){
			var str;
			str=response.split('*');
			$('#budgetNoInvoice').val(str[0]);
			$('#jobNoInvoice').val(str[1]);
			prepare_datagridInvoice();
			//prepare_datagrid();
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getInvoiceNoForEdit(id){
	
		$.post('controller/budgetController.php',{action:'loadState',id:id},function (response){
			var str;
			
			//$('#budgetNoInvoice').val(str[0]);
			$('#budgetNoInvoice').val(response);
			prepare_datagridInvoice();
			
		});
	
}


function editJob()
{
	var row=$('#dgJobAccount').datagrid('getSelected');
	if(row){
		if(row.issueType=='budget'){
			
		$('#dlgViewDetailsInvoice').dialog('open').dialog('setTitle','View Budget for:: '+row.client);
			//$('#viewDetails').html("");
			if(row.issueType=='budget'){
				
				
				
	
				
				getBudgetNox(row.jobNo);	//getBudgetNo(row.jobNo);
				var budgetNo=$('#budgetNoLpo').val();
				
				$('#clientLpo').val(row.client);
				
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				
				$('#lop_button').linkbutton('enable');
			}else{
				
				
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('enable');
			}
	
		
		}
		
	}else{
		
	}
	
}

</script>