<div id="dlgRecepit" class="easyui-dialog" style="height:350px; width:700px; padding:5px;" closed='true' buttons='#button_barw'>

<form id="frmRecepit" name="frmRecepit" method="post">
<table width="100%">
<tr><td><input type="hidden" name="year_code" value="" id="year_codeRecepit" style="width:100%;" readonly /></td><td><input type="hidden" name="month_code" value="" id="month_codeRecepit" readonly /></td></tr>
<tr><td>JobID:</td><td><input type="text" name="jobid4" value="" id="jobid4" readonly /></td></tr>

<tr><td>Balance Before:</td><td><input type="text" name="balancebefore" value=""  id='balancebefore' class="easyui-numberbox"/><input type="hidden" name="analcode" value="" id="analcode" readonly /><input type="hidden" name="credit" value="" id="credit" readonly /><input type="hidden" name="product" value="" id="productRecepit" readonly /></td></tr>
<tr><td>Amount:</td><td><input type="text" name="amount" value="" class="easyui-numberbox" data-option="min:1,max:1000000000" required="true" onblur="showBal()" id="amount"/></td></tr>
<tr><td>Balance:</td><td><input type="text" name="bal" value="" onfocus="showBal()" id="bal"  readonly="readonly" /></td></tr>
<tr><td>Date:</td><td><input type="text" name="datex" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" /></td></tr>
<tr><td>Method of Payment:</td><td><select name="payment_method" id="payment_method" class="easyui-combobox" ><option value="cash">Cash</option><option value="cheque">Cheque</option><option value="ETF">Electron Transfer</option></select></td></tr>
<tr><td>Reference No:</td><td><input type="text" name="refNo" value="" id="refNo" /></td></tr>
<tr><td>Receving Account</td><td><select name="debitAccount" style="width:250px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view_cash_book_payment',columns:[[{field:'account_code',title:'Account Code',width:50},{field:'account_name',title:'ACCOUNT',width:120},{field:'account_balance',title:'Current Balance',width:120}]]"></select></td></tr>

</table>
</form>
</div>
 <div id="button_barw">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecepit()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgRecepit').dialog('close')">Cancel</a>
</div>
<script type="text/javascript">
function showBal(){
	var balb=$('#balancebefore').numberbox('getValue');
	var x=$('#amount').numberbox('getValue')
	var total=parseInt(balb)-parseInt(x);
	$('#bal').val(total);
	
}
function saveRecepit(){
		
		
		$.messager.progress();
	 $('#frmRecepit').form('submit',{ url: url, 
	 onSubmit: function(){
		 
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress();
			 var result = eval('('+result+')');
				 if (result.msg){
					 $.messager.progress('close');
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Payment Successfully Recorded'});
						$('#dlgRecepit').dialog('close'); // close the dialog
						//$('#dgInvoice').datagrid('reload');// reload the user
						//$('#payoff').linkbutton('disable');
						$.messager.progress('close');
		}
		
		
	}
}); 
		

	}
</script>