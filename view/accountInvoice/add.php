<div id="dlgBudgetJobInvoice" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barBudgetJobInvoice'>
<div class="easyui-panel" title="Create Out going Budget" style="height:400px;">
<form id="frmBudgetJobInvoice" name="frmBudgetJobInvoice" method="post">
<table cellspacing="3" style="width:90%">
<tr><td><label>Job ID:</label></td><td><input type="text" name="jobNo" value="" id="jobNo"  /></td></tr>
<tr><td><label>Client</label></td><td><input type="text" name="client" value="" id="client" class="easyui-validatebox" required/></td></tr>
<tr><td><label>Details:</label></td><td><textArea name="details" id="details"></textArea></td></tr>
<tr><td><label>Date:</label></td><td><input type="text" name="regDate" value="" id="regDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%;" /><input type="hidden" name="income" id="income" value=""  /></td></tr>
<tr><td><label>BudgetNo:</label></td><td><input type="text" name="budgetNo" value="" id="budgetNo" /></td></tr>
<tr><td><label>Product Line:</label></td><td><select name="product" id="product" class="easyui-combobox" style="width:100%; height:auto;" data-options="url:'controller/positionController.php?action=showDepartments',valueField:'departNo2',textField:'departName',panelWidth:'120',panelHeight:'auto'" ></select> </td></tr>
</table>
</form>
<div>
<h3>Create out going Budget</h3>
<?php require_once('addBudget.php'); ?>
</div>
</div>
</div>
 <div id="button_barBudgetJobInvoice">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick=" saveBudgetJob()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgBudgetJobInvoice').dialog('close')">Close</a>
</div>