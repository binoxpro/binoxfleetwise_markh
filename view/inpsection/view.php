<div class="col-lg-12" style="margin-bottom:10px;">
<h3>Capture Inspection Details</h3>
<form name="inspection" id="inspectionFrm" method="post" action="controller/inspectionController.php" >
    <div class="col-lg-6">
        <div class="form-group">
        Vehicle Registration Number:
        <select name="regNo" id="regNo" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/vehicleNewController.php?action=viewCombo',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto',onSelect:function(row){getDriver(row.id)}" ></select>
        </div>
        <div class="form-group">
            Trailer No:
            <input type="text" name="trailer2" id="trailer2" value="" class="form-control" readonly/>
        </div>
        <div class="form-group">
            Driver Name:
            <input type="text" name="driverName" id="driverName" value="" class="form-control" readonly/>
        </div>
        <div class="form-group">
            Representative
            <input type="text" name="representative" id="representative" value="" class="form-control"/>
        </div>

    </div>
    <div class="col-lg-6">
        <div class="form-group">
            Date:
            <input name="date" id="date" value="" type="text" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%; height:30px;"/>
        </div>
        <div class="form-group">
            Time
            <input type="text" value="" id="time" name="time" class="easyui-timespinner" style="width:100%; height:30px;" />
        </div>
        <div class="form-group">
            Comment:
            <textarea name="comment" id="comment" class="form-control"></textarea>
        </div>
        <div class="form-group">
            Check List:
            <select name="type" id="checkTypeCombobox" class="easyui-combobox" style="height:30px; width:100%;" data-options="url:'controller/checklistTypeController.php?action=view',valueField:'id',textField:'type',panelHeight:'auto'" ></select>
         </div>
        <div class="form-group">
            <a href='#' class="btn btn-primary" onclick="loadUserInterface()"><i class="fa fa-plus-circle"></i>Load</a>
        </div>
    </div>
    <div class="col-lg-12">
        <div id='inspection'></div>
    </div>
    <div class="col-lg-12">
    <div class="col-lg-6">
        <div class="form-group">
            <label>Vehicle Condition ok</label>
            <input type="checkbox" name="vehicleConditionOk" id="vehicleConditionOk" value="1"/>
        </div>
        <div class="form-group">
            <label>Defects donot need to be corrected for safe driving</label>
            <input type="checkbox" name="defectdonotcorrection" id="defectdonotcorrection" value="1"/>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label>Defects Corrected</label>
            <input type="checkbox" name="defectscorrected" id="defectscorrected" value="1"/>
        </div>
    </div>
    </div>
    <div class="col-lg-6">
        <div class="sigPad">
              <label for="name">Inspected By</label>
              <input type="text" name="name" id="name" class="name">
              <p class="typeItDesc">Review your signature</p>
              <p class="drawItDesc">Draw your signature</p>
              <ul class="sigNav">
                <li class="typeIt"><a href="#type-it" class="current">Type It</a></li>
                <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
                <li class="clearButton"><a href="#clear">Clear</a></li>
              </ul>
              <div class="sig sigWrapper">
                <div class="typed"></div>
                <canvas class="pad" width="198" height="55"></canvas>
                <input type="hidden" name="output" class="output">
              </div>
        </div>
</div>
    <div class="col-lg-6">
<div class="sigPad">
      <label for="name">Driver</label>
      <input type="text" name="name" id="name" class="name">
      <p class="typeItDesc">Review your signature</p>
      <p class="drawItDesc">Draw your signature</p>
      <ul class="sigNav">
        <li class="typeIt"><a href="#type-it" class="current">Type It</a></li>
        <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
        <li class="clearButton"><a href="#clear">Clear</a></li>
      </ul>
      <div class="sig sigWrapper">
        <div class="typed"></div>
        <canvas class="pad" width="198" height="55"></canvas>
        <input type="hidden" name="output-2" class="output">
      </div>
  </div>
    </div>
</form>
</div>