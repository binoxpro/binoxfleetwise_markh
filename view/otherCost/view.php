    <table id="dgOtherCost" title="Manage Other Cost" class="easyui-datagrid" style="height:500px;" url="controller/otherCostController.php?action=view" pagination="true" toolbar="#toolbarOtherCost" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="costName" width="120">Cost Name</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarOtherCost">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newOtherCost()"><i class="fa fa-plus-circle"></i>Add Other Cost</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i>Edit Other Cost</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Other Cost</a>
    </div>
    <?php include_once('add.php'); ?>
    
