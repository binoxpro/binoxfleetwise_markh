<div class="col-lg-12">
<a href="#" onclick="drawGraphy()" class="btn btn-primary" >Show Graphy</a><br/>
<h1 class="label label-info text-info text-center" style="width:400px; height:40px; font-size:18px; padding-top:5px; margin-left:30%;">Income Vs Expense Graphy</h1>
<div><h5>KEY:</h5><table><tr><td><div style="width:10px; height:10px; background:#999;"></div></td><td>Income</td></tr><tr><td><div style="width:10px; height:10px; background:#036;"></div></td><td>Expense</td></tr></table></div>
<canvas id="canvas" height="250" width="500"></canvas>
</div>
<script type="text/javascript">
var str=new Array();
var strExpense=new Array();
function getArrayValue(){
	var str1;
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	str1=xmlhttp.responseText;
	var strnew=str1.split('/');
	var income=strnew[0].split(',');
	 for(var i=1;i<13;i++){
		 str[i-1]=income[i];
	 }
	 var expense=strnew[1].split(',');
	 for(var c=1;c<13;c++){
		 strExpense[c-1]=expense[c];
	 }
	 //$.messager.show({title:'info',msg:str});
	}
}


xmlhttp.open("GET",'controller/transactionController.php?action=graphyY',true);
xmlhttp.send();	
	
}

function getArrayValueExpense(){
	var str2;
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }

xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	str2=xmlhttp.responseText;
	var strnew2=str2.split(',');
	 for(var x=1;x<13;x++){
		 strExpense[x-1]=strnew2[x];
	 }
	 //$.messager.show({title:'info',msg:strExpense});
	}
}


xmlhttp.open("GET",'controller/transactionController.php?action=graphyX',true);
xmlhttp.send();	
	
}
function valIncome() {
	getArrayValue();
	return str;	
}
function valExpense() {
	return strExpense;		
}

	var lineChartData = {
		labels : ["Jan","Feb","Mar","Apr","May","Jun","jul","Aug","Sept","Oct","Nov","Dec"],
		datasets : [
			{
				label:"Income",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data:valIncome()
			},
			{
				label:"Expense",
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data:valExpense()
			}
			
		]

	}
	
	function drawGraphy(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData,{
			responsive: true });
	}
	
	/*window.onload=function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData,{
			responsive: true });
	}*/
	
	$(function(){
		drawGraphy();
	});
	window.onload=function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData,{
			responsive: true });
	}
//window.myLine = new Chart(ctx).Line(lineChartData,{
			//responsive: true });
</script>