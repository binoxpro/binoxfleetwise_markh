<script type="text/javascript">
function viewSearchC(){
	
	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#clientAnalysis').combobox('getValue');
	if(p1==null || p1==''){
		alert('not');
	$('#dgIndividualReport').datagrid('load',{startDate:d1,endDate:e1});
	}else if((p1!="" || p1!=null)&&((d1==null || d1=='')&&(e1==null || e1==''))){
		alert('in' +p1);
		$('#dgIndividualReport').datagrid('load',{subAccount:p1});	
	}else{
		alert('Ok');
		$('#dgIndividualReport').datagrid('load',{startDate:d1,endDate:e1,subAccount:p1});
	}
}
function viewSearchI(){
	
}

function viewSearchCompany(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	var row=$('#dgClientIncomeReport').datagrid('getSelected');
	if(row){
	$('#dgClientIncomeReport').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}
function exportToExcel(){
	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#clientAnalysis').combobox('getValue');
	if(p1==null || p1==''){
		$.post('controller/transactionController.php?action=individualReport',{subAccount:p1},function(res){
		//alert(res);
		//alert($('#clientAnalysis').combobox('getValue'));
		JSONToCSVConvertor(res,"FINANICAL STATEMENT OF "+$('#clientAnalysis').combobox('getValue'),true);
	});
	}else if((p1!="" || p1!=null)&&((d1==null || d1=='')&&(e1==null || e1==''))){
		$.post('controller/transactionController.php?action=individualReport',{subAccount:p1},function(res){
		JSONToCSVConvertor(res,"FINANICAL STATEMENT ",true);
	});
	}else{
		//alert('Ok');
		$.post('controller/transactionController.php?action=individualReport',{startDate:d1,endDate:e1,subAccount:p1},function(res){
		JSONToCSVConvertor(res,"FINANICAL STATEMENT FROM:"+d1+"TO:"+e1,true);
		});
	}
}
</script>
<div class="col-lg-12">
<table id="dgIndividualReport" title="Client Income Report" class="easyui-datagrid" style="height:700px;" url="controller/transactionController.php?action=individualReport" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarClientReport" showFooter="true" iconCls="fa fa-file-pdf-o fa-fw" >
<thead>
<tr>
	<th field="TransNo" width="150">Trans No</th>
    <th field="Account" width="90">Account</th>
    <th field="Details" width="150">Details</th>
    <th field="Ref" width="50">Ref</th>
    <th field="Dr" width="90">DR</th>
    <th field="Cr" width="150">CR</th>
    <th field="Bal" width="">Balance</th>
</tr>
</thead>
</table>
<div id="toolbarClientReport">
Category:<select name="category" id="catgory" class="easyui-combobox" style="height:auto;width:10%;" data-options="url:'controller/analysisCategoryController.php?action=view',valueField:'categoryCode',textField:'categoryDetail',panelWidth:'120',panelHeight:'auto',onSelect:function(record){
	var categoryCode=$(this).combobox('getValue');
    alert(categoryCode);
    $('#clientAnalysis').combobox('reload','controller/analysisCodeController.php?action=viewCategory&id='+categoryCode);
}" ></select> 
Client:<select name="clientAnalysis" id="clientAnalysis" class="easyui-combobox" style="width:10%; height:auto;" data-options="url:'controller/analysisCodeController.php?action=viewCategory',valueField:'analysisCode',textField:'analysisName',panelWidth:'120',panelHeight:'auto'" ></select>&nbsp;&nbsp; 
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatec" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatec" />
<a href="#" class="btn btn-primary btn-sm" onclick="viewSearchC()" ><i class="fa fa-search fa-fw"></i>Search</a>&nbsp;&nbsp;
<a href="#" class="btn btn-link" onclick="exportToExcel()" ><i class="fa fa-file-excel-o"></i>Excel Report</a>&nbsp;&nbsp;
</div>
</div>
<?php

?>