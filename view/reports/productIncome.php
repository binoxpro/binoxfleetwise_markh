<script type="text/javascript">
function viewSearch(){
	
	var d1=$('#startdatep').datebox('getValue');
	var e1=$('#enddatep').datebox('getValue');
	var p1=$('#product').combobox('getValue');
	if(p1==null || p1==''){
		//alert('not');
	$('#dgProductReport').datagrid('load',{startDate:d1,endDate:e1});
	}else{
		//alert('Ok');
		$('#dgProductReport').datagrid('load',{startDate:d1,endDate:e1,productLine:p1});
	}
}

function viewSearchCompany(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	var row=$('#dgProductReport').datagrid('getSelected');
	if(row){
	$('#dgProductReport').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}
function exportToExcel(){
	var d1=$('#startdatep').datebox('getValue');
	var e1=$('#enddatep').datebox('getValue');
	var p1=$('#product').combobox('getValue');
	if(p1==null || p1==''){
		$.post('controller/transactionController.php?action=productReport',{startDate:d1,endDate:e1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,"Report of Products Income",true);
	});
	}else{
		$.post('controller/transactionController.php?action=productReport',{startDate:d1,endDate:e1,productLine:p1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,"Report of Products Income",true);
	});
	}
	/*
	var date1=$('#datev').val();
	var date2=$('#datex').val();
	$.post('controller/transactionController.php?action=productReport',{date1:date1,date2:date2,report:'rangeReport'},function(res){
		alert(res);
		JSONToCSVConvertor(res,"Report",true);
	});
	*/
	
}

</script>
<div class="col-lg-12">
<table id="dgProductReport" title="Product Reports" class="easyui-datagrid" style="height:700px;" url="controller/transactionController.php?action=productReport" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarProductReport" showFooter="true" iconCls="icon-add">
<thead>
<tr>
    <th field="detail" width="150">Product:</th>
    <th field="Amount" width="90">Amount(UGX)</th>
</tr>
</thead>

</table>
<div id="toolbarProductReport">
Product:<select name="product" id="product" class="easyui-combobox" style="height:auto;" data-options="url:'controller/positionController.php?action=showDepartments',valueField:'departNo2',textField:'departName',panelWidth:'120',panelHeight:'auto'" ></select> 
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatep" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatep" />
<a href="#" class="btn btn-primary" onclick="viewSearch()" ><i class="icon-search"></i>Search</a>&nbsp;&nbsp;
<a href="#" class="btn btn-link" onclick="exportToExcel()" ><i class="icon-share"></i>Excel Report</a>&nbsp;&nbsp;
</div>
</div>
<?php

?>