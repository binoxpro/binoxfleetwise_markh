<script type="text/javascript">
function viewSearchC(){
	
	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#client').combobox('getValue');
	if(p1==null || p1==''){
		//alert('not');
	$('#dgClientIncomeReport').datagrid('load',{startDate:d1,endDate:e1});
	}else{
		//alert('Ok');
		$('#dgClientIncomeReport').datagrid('load',{startDate:d1,endDate:e1,client:p1});
	}
}

function viewSearchCompany(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	var row=$('#dgClientIncomeReport').datagrid('getSelected');
	if(row){
	$('#dgClientIncomeReport').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}
function exportToExcel(){
	var d1=$('#startdatec').datebox('getValue');
	var e1=$('#enddatec').datebox('getValue');
	var p1=$('#client').combobox('getValue');
	if(p1==null || p1==''){
		$.post('controller/budgetController.php?action=clientIncomeReport',{startDate:d1,endDate:e1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,"Report of Products Income",true);
	});
	}else{
		$.post('controller/budgetController.php?action=clientIncomeReport',{startDate:d1,endDate:e1,client:p1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,"Invoice for "+p1,true);
	});
	}
}
</script>
<div class="col-lg-12">
<table id="dgClientIncomeReport" title="Client Income Report" class="easyui-datagrid" style="height:700px;" url="controller/budgetController.php?action=clientIncomeReport" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarClientReport" showFooter="true" iconCls="icon-add" data-options="rowStyler:function(index,row){
if(row.operationStatus=='confirmed'){
	return'background-color:#FFFF4F;';
}
}">
<thead>
<tr>
	 <th field="regDate" width="150">Date</th>
    <th field="jobNo" width="150">JobNo</th>
    <th field="invoiceNo" width="50">InvoiceNo</th>
    <th field="client" width="90">Client</th>
    <th field="income" width="150">Income</th>
    <th field="expense" width="90">Budget</th>
    <th field="accural" width="">Accurals</th>
</tr>
</thead>

</table>
<div id="toolbarClientReport">
Client:<select name="client" id="client" class="easyui-combobox" style="width:100%; height:auto;" data-options="url:'controller/analysisCodeController.php?action=viewCategory&id=Dedit',valueField:'analysisName',textField:'analysisName',panelWidth:'120',panelHeight:'auto'" ></select> 
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatec" style="width:100%;" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatec" style="width:100%;" />
<a href="#" class="btn btn-primary" onclick="viewSearchC()" ><i class="icon-search"></i>Search</a>&nbsp;&nbsp;
<a href="#" class="btn btn-link" onclick="exportToExcel()" >Excel Report</a>&nbsp;&nbsp;
</div>
</div>
<?php

?>