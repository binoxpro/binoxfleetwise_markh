<script type="text/javascript">
function viewSearch(){
	
	var d1=$('#startdatee').datebox('getValue');
	var e1=$('#enddatee').datebox('getValue');
	var p1=$('#expense').combobox('getValue');
	if(p1==null || p1==''){
		//alert('not');
	$('#dgExpenseReport').datagrid('load',{startDate:d1,endDate:e1});
	}else{
		//alert('Ok');
		$('#dgExpenseReport').datagrid('load',{startDate:d1,endDate:e1,account_Code:p1});
	}
}

function viewSearchCompany(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	var row=$('#dgExpenseReport').datagrid('getSelected');
	if(row){
	$('#dgExpenseReport').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}
function exportToExcel(){
	var d1=$('#startdatee').datebox('getValue');
	var e1=$('#enddatee').datebox('getValue');
	var p1=$('#expense').combobox('getValue');
	if(p1==null || p1==''){
		$.post('controller/transactionController.php?action=expenseReport',{startDate:d1,endDate:e1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,"General Area of Expense for Royal Way Media as on"+d1+" To "+e1,true);
	});
	}else{
		$.post('controller/transactionController.php?action=expenseReport',{startDate:d1,endDate:e1,client:p1},function(res){
		//alert(res);
		JSONToCSVConvertor(res,p1+"Expense statement for Royal Way Media as on "+d1+" To "+e1,true);
	});
	}
}
</script>
<div class="col-lg-12">
<table id="dgExpenseReport" title="Expense Reports" class="easyui-datagrid" style="height:700px;" url="controller/transactionController.php?action=expenseReport" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarExpenseReport" showFooter="true">
<thead>
<tr>
	
    <th field="detail" width="150">Product:</th>
    <th field="Amount" width="90">Amount(UGX)</th>
</tr>
</thead>

</table>
<div id="toolbarExpenseReport">
Expense:<select name="expense" id="expense" class="easyui-combobox" style="height:auto;" data-options="url:'controller/accountController.php?action=view_expense_account',valueField:'account_code',textField:'account_name',panelWidth:'120',panelHeight:'auto'" ></select> 
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatee" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatee" />
<a href="#" class="btn btn-primary" onclick="viewSearch()" ><i class="icon-search"></i>Search</a>&nbsp;&nbsp;
<a href="#" class="btn btn-link" onclick="exportToExcel()" ><i class="icon-share"></i>Excel Report</a>&nbsp;&nbsp;
</div>
</div>
<?php

?>