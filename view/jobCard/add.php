<div class="col-lg-12" style="margin-top:10px;">
<h3>Job Card Details</h3>
<form name="jobcard" id="jobcardFrm" action="controller/jobCardController.php">
<div class="col-lg-4">
    <div class="form-group">

        <label>Serial NO.</label>
        <input class="form-control" name="jobNo" id="jobNo" value="" class="form-control" type="text" readonly="readonly" />
    </div>
    <div class="form-group"><label>Date</label>
        <input name="creationDate" id="creationDate" value="" type="text" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%; height: 35px;"/>
    </div>

</div>

<div class="col-lg-4">
    <!--<div class="form-group">
        <label>GARAGE Name</label>
        <select name="garage" id="garage" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/supplierController.php?action=viewComboBox',valueField:'supplierCode',textField:'supplierName',panelHeight:'auto'" ></select>
    </div>-->
    <div class="form-group">
        <label>Time In</label>
        <input type="text" value="" id="time" name="time" class="easyui-timespinner"  style="height:30px; width:100%;"/>
    </div>
    <div class="form-group">
        Vehicle Registration Number:
        <select name="vehicleId" id="vehicleId" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/vehicleNewController.php?action=viewCombo',valueField:'id',textField:'regNo',panelHeight:'auto',onSelect:function(rec){
			$.post('controller/driverVehicleController.php?action=getLastDriver',{vehicleId:rec.id},function(res){
				var data=$.parseJSON(res);
				if(data===null)
				{
					alert('Empty')
					$('#driverName').val('');
				$('#trailer').val('');
				}else{
				$('#driverName').val(data.fullName);

				}
			});
			}"  ></select>
    </div>
    <!--Customised for markh investment ref document--->
   <!-- <div class="form-group">
        <label>FUEL level</label>
        <select name="fuelLevel" id="fuelLevel" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'view/jobCard/fuelLevel.json',valueField:'text',textField:'text',panelHeight:'auto'" ></select>
    </div>-->
</div>
<div class="col-lg-4">

    <div class="form-group">
        <label>Odometer Reading</label>
        <input type="text" name="odometer" id="odometer" class="form-control" value="" />
    </div>

    <div class="form-group">
        <label>Driver's Name</label>
        <input type="text" name="driverName" id="driverName" class="form-control" value="" />
    </div>
</div>
<div class="col-lg-12">
<input type="submit" name="action" value="add"  class="btn btn-primary"/>
</div>
<!--<a href="#" class="btn btn-primary" onclick="saveJobCard()">Add</a>-->
</form>
</div>

