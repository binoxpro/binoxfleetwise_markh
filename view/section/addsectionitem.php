<table id="dgSectionItem" title="Section Item Set Up" class="easyui-datagrid" style="height:500px;" url="controller/sectionItemController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarSectionitemAd" iconCls="icon-save">
<thead>
<tr>
	<th field="id" width="90">ID</th>
    <th field="itemName" width="90">Item Name</th>
    <th field="itemDescription" width="90">Description</th>
    <th field="checklistId" width="90">checklist ID</th>
    <th field="comment" width="90">Comment</th>
    <th field="sectionId" width="90">Section Id</th>
</tr>
</thead>

</table>
<div id="toolbarSectionitemAd">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addSectionitem()" >Add Section Item</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editSection()" >Edit Section Item</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteSection()" >Delete Section Item</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="" >More</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="" >More</a>
</div>

<?php
include_once('add.php');
?>