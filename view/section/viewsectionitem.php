<div id="dlgSectionitem" class="easyui-dialog" style="height:auto; width:500px; padding: 5px; " closed='true' toolbar='#button_bar24' modal='true'>
<form id="frmSectionitem" name="frmSectionitem" method="post">

<div class="form-group"><label>Item</label><input type="text" name="itemName" value="" id="itemName" class="form-control"/></div>
<div class="form-group"><label>Description</label><textarea name="itemDescription" id="itemDescription" class="form-control"></textarea></div>
<div class="form-group"><label>Comment</label><textarea name="comment" id="comment" class="form-control"></textarea></div>
<div class="form-group"><label>Section:</label><select type="text" id="sectionCombobox" name="section"  class="easyui-combobox" style="width:100%;height: 30px;" data-options="url:'controller/sectionController.php?action=view',valueField:'id',textField:'name',panelWidth:'200',panelHeight:'auto'" ></select></div>
</form>
</div>
<div id="button_bar24">
<a href="#" class="btn btn-primary" onclick="saveSectionItem()"><i class="fa fa-check"></i>Save</a>
</div>
