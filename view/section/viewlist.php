    <table id="dgSection" title="Manage Section" class="easyui-datagrid" style="height:500px;" url="controller/checklistTypeController.php?action=view" pagination="true" toolbar="#toolbarSection" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="type" width="120">Checklist Type</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarSection">
    	<a href="#" class="btn btn-primary btn-sm" onclick="addSection()"><i class="fa fa-plus-circle"></i>Add Section</a>
        <!--<a href="#" class="btn btn-primary btn-sm"onclick="editSection()"><i class="fa fa-pencil"></i>Edit Section</a>-->
        
    </div>
    <?php include_once('view.php'); ?>
    
