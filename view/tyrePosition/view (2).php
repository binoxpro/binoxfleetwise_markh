
<table id="dgTyrePosition" title="Tyre Position" class="easyui-datagrid" style="height:500px;" url="controller/tyrePositionController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarTyrePosition">
<thead>
<tr>
	<th field="Id" width="90">No</th>
	<th field="positionDetails" width="90">Position Details</th>
    <th field="isActive" width="90">isActive</th>
</tr>
</thead>

</table>
<div id="toolBarTyrePosition">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="newTyrePosition()" >Add</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editTyrePosition()" >Edit</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteServiceMaintance()" >Delete</a>
</div>

<?php
include_once('add.php');
?>
<script type="text/javascript">

function newTyrePosition(){
		
		
			$('#dlgTyrePosition').dialog('open').dialog('setTitle','Add Tyre Position');
			
			$('#frmTyrePosition').form('clear');
			
			url='controller/tyrePositionController.php?action=add';
				
	}
	
	
	function editTyrePosition(){
		var row=$('#dgTyrePosition').datagrid('getSelected');
		if(row){
			$('#dlgTyrePosition').dialog('open').dialog('setTitle','Edit Vechile');
			$('#frmTyrePosition').form('load',row);
			url='controller/tyrePositionController.php?action=update';
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select  a tyre position to edit'});
		}
		
	}
	
	/*
	$(function(){
	$('#dgTyrePosition').datagrid({
	rowStyler:function(index,row){
		var nextR=parseInt(row.nextReading);
		var current=parseInt(row.km_reading);
		//alert(nextR+"/"+current);
		if((nextR-current)<=2000){
			return 'background-color:#F00;';
		}else{
			//return 'background-color:#F00;';
		}
	}
	
	});
	});
	*/
	
</script>