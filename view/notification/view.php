    <table id="dgNotification" title="Manage Notification" class="easyui-datagrid" style="height:700px;" url="controller/notificationController.php?action=view" pagination="true" toolbar="#toolbarNotification" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="section" width="50">Section</th>
            <th field="to" width="120">To</th>
            <th field="cc" width="150">CC</th>
            <th field="msg" width="90">Message</th>
            <th field="" width="80">Consumption/Km</th>
            <th field="" width="100">Consumption/Currency</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarNotification" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newNotification()"><i class="fa fa-plus-circle"></i>Add Notification</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="editNotification()"><i class="fa fa-pencil"></i>Edit Notification</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Notification</a>
    </div>
    <?php include_once('add.php'); ?>
    
