<div id="dlgNotification" class="easyui-dialog" closed='true' style="width:500px;" buttons='#Notificationbutton_bar' modal="true" >
<form id="frmNotification" name="frmNotification" method="post">
<div id="col-lg-6" style="padding:5px;">
    <div class="form-group" style="padding-right:50%;">
    <label>Section:</label>
    <input class="form-control" name="section" id="section" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>To:</label>
    <input class="form-control" name="to" id="to" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>CC:</label>
    <input class="form-control" name="cc" id="cc" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>Message:</label>
    <textarea class="form-control" name="msg" id="msg" value="" type="text"/>
    </div>
     <div class="form-group" style="padding-right:50%;">
    <label>Header:</label>
    <input class="form-control" name="headerNotif" id="headerNotif" value="" type="text"/>
    </div>
    <div class="form-group" style="padding-right:50%;">
    <label>Sent:</label>
    <input class="form-control" name="sent" id="sent" value="" type="text"/>
    </div>
</div>
</form>
</div>
<div id="Notificationbutton_bar">
<a href="#" class="btn btn-primary" onclick="savesNotification()">Save</a>
<a href="#" class="btn btn-primary">Close</a>
</div>

