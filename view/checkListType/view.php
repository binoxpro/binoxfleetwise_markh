    <table id="dgCheckListType" title="Manage Vehicle Type" class="easyui-datagrid" style="height:700px;" url="controller/checklistTypeController.php?action=view" pagination="true" toolbar="#toolbarVehicleType" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="type" width="120">Checklist Type</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarVehicleType">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newCheckListType()"><i class="fa fa-plus-circle"></i>Add CheckList Type</a>
        <a href="#" class="btn btn-primary btn-sm"onclick="editCheckListType()"><i class="fa fa-pencil"></i>Edit CheckList Type</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete CheckList Type</a>
    </div>
    <?php include_once('add.php'); ?>
    
