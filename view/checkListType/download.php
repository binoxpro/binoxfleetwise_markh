<div class="col-lg-12">
<form class="" name="inspection" id="inspectionFrm" action="controller/inspectionController.php">
	<div class="col-lg-12">
		<input type="hidden" name="header1" id="header1" value=""/><a href='#' class="btn btn-primary" onclick="downloadCheck()"><i class="fa fa-download"></i>Load</a>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<label>Vehicle Registration Number:</label>
			<select name="regNo3" id="regNo3" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/vehicleNewController.php?action=viewCombo',valueField:'id',textField:'regNo',panelHeight:'auto',onSelect:function(rec){
			$.post('controller/driverVehicleController.php?action=getLastDriver',{vehicleId:rec.id},function(res){
				var data=$.parseJSON(res);
				if(data===null)
				{
					alert('Empty')
					$('#driverName').val('');
				$('#trailer').val('');
				}else{
				$('#driverName').val(data.fullName);
				$('#trailer').val(data.trailerNo);
				}
			});
			}" ></select>
		</div>
		<div class="form-group">

				<label>Trailer.</label>
				<input type="text" name="trailer" id="trailer" value=""  class="form-control"/>
		</div>
		<div class="form-group">

			<label>Driver Name</label>
			<input type="text" name="driverName" id="driverName" value=""  class="form-control"/>
		</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
			<label>Date of Inspection:</label>
				<input name="date" id="date" value="" type="text" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="height:30px; width:100%;"/>
			</div>
			<div class="form-group">
			<label>Check List:</label>
				<select name="type" id="checkTypeCombobox" class="easyui-combobox" style="height:30px; width:100%;" data-options="url:'controller/checklistTypeController.php?action=view',valueField:'id',textField:'type',panelHeight:'auto',onSelect:function(record){
	$.post('controller/checklistTypeController.php?action=getType',{id:record.id},function(res){
		$('#header1').val(res);
	});
	}" ></select></div>
			</div>

</form>
</div>