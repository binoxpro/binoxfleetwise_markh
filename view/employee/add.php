<div id="dlgEmployee" class="easyui-dialog" style="height:auto; width:50%; " closed='true' buttons='#button_barEmployee' modal="true">
<div class="col-lg-12">
<form id="frmEmployee" name="frmEmployee" method="post">
<table width="100%"><tr><td><label>EmployeeID:</label></td><td><input type="text" name="employeeID" value="" id="employeeID" style="width:70%;" /></td></tr>
<tr><td><label>Employee's Name:</label></td><td><input type="text"  name="employeeName" value="" id="employeeName" style="width:70%;" /></td></tr>
<tr><td><label>Contact:</label></td><td><input type="text"  name="contact" value="" id="contact" style="width:70%;" /></td></tr>
<tr><td><label>Email:</label></td><td><input type="text" name="email" id="email" class="easyui-validatebox" style="width:70%;" /></td></tr>
<tr><td><label>Position:</label></td><td><select name="position" id="position" class="easyui-combobox" data-options="url:'controller/positionController.php?action=view',valueField:'positionID',textField:'positionName',panelWidth:'130'" style="width:70%;"></select></td></tr>
</table>
</form>
</div>
</div>
</div>
 <div id="button_barEmployee">
<a href="#" class="btn btn-success" onclick=" saveEmployee()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgEmployee').dialog('close')"><i class="fa fa-times"></i>Cancel</a>
</div>
