<div class="col-lg-12">
<table id="dgEmployee" title="Company Employee" class="easyui-datagrid" style="height:700px;" url="controller/employeeController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarEmployee" pageSize="10" iconCls="fa fa-user fa-fw">
<thead>
<tr>
	<th field="employeeID" width="90">Employee ID</th>
    <th field="employeeName" width="90">Employee Name</th>
    <th field="contact" width="90">Contact</th>
    <th field="email" width="90">Email</th>
    <!--<th field="position" width="90">Position</th>-->
    <!--<th field="photo" width="90">Photo</th>-->
	
</tr>
</thead>

</table>
<div id="toolbarEmployee">
<a href="#" class="btn btn-primary" onclick="newEmployee()" ><i class="fa fa-plus-circle"></i>Add Employee</a>
<a href="#" class="btn btn-link" onclick="editEmployee()" ><i class="fa fa-pencil"></i>Edit Employee</a>
</div>
</div>
<?php
include_once('add.php');
?>
<script type="text/javascript">
function newEmployee(){
	$('#dlgEmployee').dialog('open').dialog('setTitle','Add An Employee:');
	$('#frmEmployee').form('clear');
	url='controller/employeeController.php?action=add';
}
function editEmployee(){
	var row=$('#dgEmployee').datagrid('getSelected');
	if(row){
	$('#dlgEmployee').dialog('open').dialog('setTitle','Edit An Employee:');
	$('#frmEmployee').form('load',row);
	url='controller/employeeController.php?action=update&id='+row.employeeID;
		
	}else{
		$.messager.show({title:'Info',msg:'select an employee  to edit'});
	}
}
function saveEmployee(){
		
		$.messager.progress();
	 $('#frmEmployee').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					  $.messager.progress('close');
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Employee Saved'});
						$('#dlgEmployee').dialog('close'); // close the dialog
						 $('#dgEmployee').datagrid('reload'); // reload the user
						 $.messager.progress('close');
		}
		
	}
}); 
		
}
function deleteEmployee(){
	$.messager.alert('Warning','This operation was not secure so it was turned off for  more information call help line:0781587081');
}
function addSystemRole(){
	var row=$('#dgEmployee').datagrid('getSelected');
	if(row){
	$('#dlgEmployee').dialog('open').dialog('setTitle','Edit An Employee:');
	$('#frmEmployee').form('load',row);
	url='controller/employeeController.php?action=update&id='+row.employeeID;
		
	}else{
		$.messager.show({title:'Info',msg:'select an employee to add system role'});
	}
	
}

</script>