<div class="col-lg-12">
<table id="dgDepartment" title="Company Product Lines" class="easyui-datagrid" style="height:700px;" url="controller/departmentController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarDepartment" pageSize="10" iconCls="fa fa-building">
<thead>
<tr>
	<th field="departmentID" width="90">Product ID</th>
    <th field="departmentName" width="90">Product Name</th>
   
	
</tr>
</thead>

</table>
<div id="toolbarDepartment">
<a href="#" class="btn btn-primary" onclick="newDepartment()" ><i class="fa fa-plus-circle"></i>Add Product</a>
<a href="#" class="btn btn-link" onclick="editDepartment()" ><i class="fa fa-pencil"></i>Edit Product</a>
<a href="#" class="btn btn-link" onclick="deleteDepartment()" ><i class="fa fa-minus-circle"></i>Delete Product</a>
<!--<a href="#" class="btn btn-link" onclick="addPosition()" ><i class="icon-share"></i>Add Positions</a>-->
</div>
</div>
<?php
include_once('add.php');
include_once('addPositon.php');
?>
<script type="text/javascript">
function newDepartment(){
	$('#dlgDepartment').dialog('open').dialog('setTitle','Add A Department:');
	$('#frmDepartment').form('clear');
	url='controller/departmentController.php?action=add';
}
function editDepartment(){
	var row=$('#dgDepartment').datagrid('getSelected');
	if(row){
	$('#dlgDepartment').dialog('open').dialog('setTitle','Edit A Department:');
	$('#frmDepartment').form('load',row);
	url='controller/departmentController.php?action=update&id='+row.departmentID;
		
	}else{
		$.messager.show({title:'Info',msg:'select a department to edit'});
	}
}
function saveDepartment(){
		
		$.messager.progress();
	 $('#frmDepartment').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Department Saved'});
						$('#dlgDepartment').dialog('close'); // close the dialog
						 $('#dgDepartment').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function deleteDepartment(){
	$.messager.alert('Warming','This operation is not secure so it was turn off  more information call help line:0781587081');
}
function addPosition(){
	
	var row=$('#dgDepartment').datagrid('getSelected');
	if(row){
	$('#dlgAddPosition').dialog('open').dialog('setTitle','Select Position add to Department:');
	$('#dgAddPosition').datagrid('reload',{id:row.departmentID});
	//$('#frmDepartment').form('load',row);
	url='controller/positionController.php?action=addDepartment&id='+row.departmentID;
		
	}else{
		$.messager.show({title:'Info',msg:'select a Department to add Position'});
	}

}
function addDepartment(){
	var ids=[];
	var str="";
	var rows=$('#dgAddPosition').datagrid('getSelections');
	for(var i=0;i<rows.length;i++){
		if(i+1<rows.length){
		str=str+rows[i].positionID+"-";
		}else{
			str=str+rows[i].positionID;
		}
	}
	
	$.post(url+"&positionID="+str,function(data){
		$('#dgAddPosition').datagrid('reload');
	});
}
</script>