<div id="dlgAddPosition" class="easyui-dialog" style="height:500px; width:500px; " closed='true' buttons='#button_barAddPosition'>
<table id="dgAddPosition" title="Positions" class="easyui-datagrid" style="height:300px;" url="controller/positionController.php?action=viewDepartment" pagination="true" rownumbers="true" fitColumns="true" singleSelect="false" toolbar="#toolbarAddPosition" pageSize="10">
<thead>
<tr>
	<th field="chk" checkbox="true">Add</th>
	<th field="positionID" width="90">Product ID</th>
    <th field="positionName" width="90">Product Name</th>
    <th field="allowance" width="90">Allowance</th>
    <th field="salary" width="90">Salary</th>
    <th field="other" width="90">Other</th>
</tr>
</thead>

</table>
<div id="toolbarAddPosition">
<a href="#" class="btn btn-primary" onclick="addDepartment()" ><i class="icon-plus-sign"></i>Add Positions</a>
<a href="#" class="btn btn-link" onclick="editPosition()" ><i class="icon-edit"></i>Edit Position</a>
<a href="#" class="btn btn-link" onclick="deletePoition()" ><i class="icon-minus-sign"></i>Delete Positon</a>
<!--<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="" >View Employee</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="" >View Sales</a>-->
</div>
</div>
 <div id="button_barAddPosition">
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgAddPosition').dialog('close')"><i class="icon-remove"></i>Close</a>
</div>