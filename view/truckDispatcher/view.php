<div class="col-lg-12" style="margin-top:5px; margin-bottom:5px;">
    <table id="dgDispatcher" title="Manage Dispatcher" style="height:auto;" url="controller/truckDispatcherController.php?action=view" pagination="true" toolbar="#toolbarAllocation" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-chart">
    	<thead>
        	<tr>
            <th field="id" width="150" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="mailingName" width="150">Customer</th>
            <th field="orderDate" width="100">Order Date</th>
            <th field="promiseDate" width="100">Promise Date</th>
            <th field="ordertypeId" width="50">Order Type Id</th>
            <th field="orderNumber" width="100">Order No</th>
            <th field="qtyOrder" width="100">Qty</th>
            <th field="truck" width="100" >Truck No</th>
            <th field="depotintime" width="100" data-options="editor:{type:'timespinner'}">Depot In Time</th>
            <th field="depotouttime" width="100" data-options="editor:{type:'timespinner'}">Depot Out Time</th>
            <th field="Dispatchcomment" width="100">Comment</th>
            <th field="eid" width="50" data-options="editor:{type:'text'}" hidden="true" >#</th>
            <th field="did" width="50" data-options="editor:{type:'text'}" hidden="true" >#</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarAllocation">
    	<!--<a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgDispatcher').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>Add</a>-->
        <a href="#" class="btn btn-primary btn-sm" onclick="saveDispatcher()"><i class="fa fa-pencil"></i>Save/Update</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgDispatcher').edatagrid('deleteRow');refreshGird()"><i class="fa fa-times-circle"></i>Delete</a>
       <!-- <a href="#" class="btn btn-link" onclick="exportToExcel()">Excel File</a>-->
    </div>
    </div>