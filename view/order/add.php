<div id="dlgCapacity" class="easyui-dialog" closed='true' buttons='#capacitybutton_bar' modal="true" style="width:870px; padding:5px;" >
<form id="frmCapacity" name="frmCapacity" method="post" >

    <table width="100%"ccellpadding="3" cellspacing="3">
        <tr><td>
        <label>Registration Number:</label></td><td>
        <select name="regNo" id="regNo" class="easyui-combobox form-control" style="height:auto; width:50%;" data-options="url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
        </select>
        </td></tr>
        <tr><td>
        <label>Trip Number:</label></td><td>
        <input name="tripNo" id="tripNo" value="" type="text" style="width:50%;"/>
        <a href="#" class="btn btn-primary btn-sm" onclick="searchTrip()"><i class="fa fa-search"></i>Previous Trip</a>
        </td></tr>
        <tr><td>
        <label>Loading Date:</label></td><td>
        <input type="text" name="creationDate" value="" id="creationDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:50%;height:30px;" />
        </td></tr>
        <tr><td>
        <label>Loading Time:</label></td><td>
        <input name="timeLoading" id="timeLoading" class="easyui-timespinner" data-option="min:'07:00',max:'06:00'" value="" type="text" style="width:50%; height:30px;"/>{Ranges from 07:00AM to 06:00PM}
        </td></tr>
      </table>
    <table class="table table-condensed">
    <tr class="info"><td><label>VPOWER</label></td><td><label>AGO</label></td><td><label>BIK</label></td><td>UGL</td></tr>
   		<tr><td>
        <input name="vpower" id="vpower" value="" type="text" style="width:70%;"/>
        </td><td>
        <input name="ago" id="ago" value="" type="text" style="width:70%;"/>
        </td>
        <td>
        <input name="bik" id="bik" value="" type="text" style="width:70%;"/>
        </td>
        <td>
        <input name="ugl" id="ugl" value="" type="ugl" style="width:70%;"/>
        </td>
        </tr>
     </table>
</form>
</div>
<div id="capacitybutton_bar">
<a href="#" class="btn btn-primary" onclick="saveCapacity()"><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger"><i class="fa fa-times"></i>Close</a>
</div>

