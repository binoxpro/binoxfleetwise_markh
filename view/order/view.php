   <div class="col-lg-12" style="margin-top:10px;">
    <table id="dgOrder" title="Manage Orders" style="height:auto;" url="controller/deliveryController.php?action=view" pagination="true" toolbar="#toolbarOrder" showFooter="true" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" iconCls="fa fa-book">
    	<thead>
        	<tr>
            <th field="id" width="150" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="mailingName" width="150">Customer</th>
           <!-- <th field="orderDate" width="100">Order Date</th>-->
            <th field="promiseDate" width="100">Load Date</th>
            <th field="ordertypeId" width="50">Order Type Id</th>
            <th field="orderNumber" width="100">Order No</th>
            <th field="qtyOrder" width="100">Qty</th>
            <th field="timein" width="100" >Depot In</th>
            <th field="timeout" width="100" >Dispatch Time</th>
            <th field="duration" width="100">Depot Duration</th>
            <th field="truck" width="100" >Truck No</th>
            <th field="deliveryDate" width="100" data-options="editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}"hidden="true">Delivery Date</th>
            <th field="deliveryTime" width="100" data-options="editor:{type:'timespinner'}" hidden="true">Delivery Time</th>
            <th field="did" width="50" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="deid" width="50" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="eid" width="50" data-options="editor:{type:'text'}" hidden="true" >#</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarOrder">
    	<a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgOrder').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>Add</a>
        <a href="#" class="btn btn-link" onclick="saveOrder()"><i class="fa fa-pencil"></i>Save</a>
        <a href="#" class="btn btn-link" onclick="javascript:$('#dgOrder').edatagrid('deleteRow');refreshGird()"><i class="fa fa-times-circle"></i>Delete</a>
        <a href="#" class="btn btn-link" onclick="exportToExcel()">Excel File</a>
        <a href="FuelTemplate.xlsx" class="btn btn-link" download="FuelTemplate"><i class="fa fa-download"></i>Download Template</a>
        <a href="admin.php?view=upload" class="btn btn-link">Upload Order</a>
        <a href="admin.php?view=uploadDelivery" class="btn btn-link">Upload Order(Delivered Orders)</a>
        <div>
        &nbsp;Starting<input type="text" name="startDate" value="" id="startDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />&nbsp;&nbsp;Ending<input type="text" name="endDate" value="" id="endDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />
            <a href="#" class="btn btn-primary btn-sm" onclick="viewSearchC()"><i class="fa fa-search"></i></a>
        </div>
        
        
    </div>
    </div>
    <?php
	require_once('addCustomer.php'); 
	//,options:{panelWidth:150,valueField:'cus',textField:'cus',url:'controller/orderController.php?action=viewCustomer2'}}
	?>
    
