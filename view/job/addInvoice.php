<div id="dlgInvoiceAdd" class="easyui-dialog" style="height:560px; width:800px; padding:10px; " closed='true' buttons='#button_barInvoiceAdd' modal='true'>
<form id="frmInvoiceAdd" name="frmInvoiceAdd" method="post">
<table cellspacing="4" style="width:100%;">
<tr><td><label>Job ID:</label></td><td><input type="text" name="jobNo" value="" id="jobNoInvoice" readonly="readonly"/></td></tr>
<tr><td><label>Client</label></td><td><input type="text" name="client" value="" id="clientInvoice"  onkeyup="getClient(this.value)" onfocus="getClient(this.value)" onblur="getClientLoss2()"/><div id="clientSelect"></div></td></tr>
<tr><td><label>Details:</label></td><td><textArea name="details" id="detailsInvoice"></textArea></td></tr>
<tr><td><label>Date:</label></td><td><input type="text" name="regDate" value="" id="regDateInvoice" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" /></td></tr>
<tr><td>
Income:</td><td><input type="text" name="income" id="incomeInvoice" value="" readonly /></td></tr>
<tr><td><label>Invoice No:</label></td><td><input type="text" name="budgetNo" value="" id="budgetNoInvoice" readonly="readonly" /></td></tr>
<tr><td><label>Product Line:</label></td><td><select name="product" id="productInvoice" class="easyui-combobox"  data-options="url:'controller/positionController.php?action=showDepartments',valueField:'departNo2',textField:'departName',panelHeight:'auto'" ></select> </td></tr>
</table>

</form>

<h4 class="text-success">Create Invoice Details:</h4>
<?php require_once('addInvoiceTax2.php');
require_once('addInvoiceItem.php');
 ?>
</div>
</div>
</div>
 <div id="button_barInvoiceAdd">
<a href="#" class="btn btn-success" onclick="setTotalInvoice();saveInvoiceJob()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgInvoiceAdd').dialog('close')"><i class="fa fa-times"></i>Close</a>
</div>
<script type="text/javascript">
//function getClient(x){
//		$.post('controller/analysisCodeController.php?action=getClient',{name:x},function(data){
//			$('#clientSelect').html(data);
//			document.getElementById('clientSelect').style="border:0px solid #000;";
//		});
//	}
//	
	//function loadData(x){
//		$('#clientInvoice').val(x);
//		$('#clientSelect').html('');
//		document.getElementById('clientSelect').style="border:0px solid #000;";	
//		
//	}
	function getClient(x){
		$.post('controller/analysisCodeController.php?action=getClient',{name:x},function(data){
			$('#clientSelect').html(data);
			document.getElementById('clientSelect').style="position:absolute;cursor:pointer;height:120px;overflow-y:scroll; background:#000; border-radius:4px;";
		});
	}
	function getClientLoss2()
	{
		//$('#clientSelect2').html("");
		if($('#clientInvoice').val()!=null ||$('#client2').val()==""){
		document.getElementById('clientSelect2').style="";
		}
	}
	
	function loadData(x){
		$('#clientInvoice').val(x);
		$('#clientSelect').html("");
		document.getElementById('clientSelect').style="";	
		
	}
	</script>