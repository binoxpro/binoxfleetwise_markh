<div class="col-lg-12">
<table id="dgJob" class="easyui-datagrid" style="height:600px; width:auto;" url="controller/jobController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarJob">
<thead>
<tr>
	<th field="jobNo" width="90">Project No</th>
    <th field="regDate" width="120">Date</th>
    <th field="client" width="90">Client Name</th>
    <th field="details" width="90">Description</th>
    <th field="issueType" width="90">Issues As</th>
    <th field="operationStatus" width="90">Status</th>
</tr>
</thead>
</table>
<div id="toolbarJob">
<a href="#" class="btn btn-primary" onclick="newBudgetJob()" ><i class="fa fa-file"></i>New Job(Budget)</a>
<a href="#" class="btn btn-link" onclick="newInvoiceJob()" ><i class="fa fa-file"></i>New Job(Proforma) </a>
<a href="#" class="btn btn-link" onclick="viewBudget()" ><i class="fa fa-file-word-o"></i>View Details</a>
<a href="#" class="btn btn-link" onclick="internalBudget()" ><i class="fa fa-history"></i>Attach Internal Budget</a>
<a href="#" class="btn btn-link" onclick="editBudgetJob()" ><i class="fa fa-edit"></i>Edit Job</a>
<a href="#" class="btn btn-link" onclick="reconciliation()" ><i class="fa fa-exclamation-circle"></i>Reconciliation</a>
<div class="btn-group">
<a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown" >More<span class="caret"></span></a>
<ul class="dropdown-menu" >
<li><a href="#" onclick="releaseFund()"><i class="fa fa-thumbs-up"></i>Release Funds</a></li>
<li><a href="#" onclick="accountability()"><i class="fa fa-briefcase"></i>Accountability</a></li>
</ul>
</div>
</div>

<?php
include_once('add.php');
include_once('addInvoice.php');
include_once('viewDetails.php');
include_once('viewTaxInvoice.php');
include_once('addLpo.php');
include_once('addInternal.php');
include_once('addRecepit.php');
include_once('reconciliation.php');
include_once('releaseFunds.php');
include_once('view/accountability/view.php');
?>
</div>
<script type="text/javascript">
var output;
function reconciliation(){
	if(Userrole=="finance"){
	var row=$('#dgJob').datagrid('getSelected');
	if(row)
	{
	$('#dlgRcon').dialog('open').dialog('setTitle','Reconciliation');
	$('#jobNoInternal2').val(row.jobNo);
	prepare_rcon();
	}else{
		$.messager.alert('Warning','Please select a job work on');
	}
	}else{
		$.messager.alert('Warning','You have no right to do this');
	}
}
function setDetails(bno){
	
		$.post('controller/budgetController.php?action=viewDetails&budgetNo='+bno,{actionv:'viewDetails',budgetNov:bno},function (response){
			$('#viewDetails').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsInternal(bno){
	
		$.post('controller/budgetController.php?action=viewDetails&budgetNo='+bno,{actionv:'viewDetails',budgetNov:bno},function (response){
			$('#viewDetailsInternal').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetails2(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno,typeInvoice:'Tax Invoice'},function (response){
			$('#viewDetails2').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsPorformaInvoice(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno,typeInvoice:'Proforma Invoice'},function (response){
			$('#viewDetails').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setDetailsPorformaInvoiceInternal(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno,typeInvoice:'Proforma Invoice'},function (response){
			$('#viewDetailsInternal').html(response);
			//internal
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function setBudgetNo(bno){
	
		$.post('controller/budgetController.php',{action:'showTaxInvoice',id:bno},function (response){
			$('#viewDetails2').html(response);
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getBudgetNox(bno){
	
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			//alert(rep\
			//output=response;
			
			$('#budgetNoLpo').val(response);
			setDetails(response);
			
		});
		
		//return output;
	
}
function getBudgetNoxx(bno){
	
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			//alert(rep\
			//output=response;
			
			//$('#budgetNoLpo').val(response);
			setDetailsInternal(response);
			//viewDetailsInternal
			
		});
		
		//return output;
	
}
function getBudgetNoForEdit(bno){
		$.post('controller/budgetController.php?action=defineBudgetNo&id='+bno,{actionx:'defineBudgetNox',idx:bno},function (response){
			$('#budgetNo').val(response);
			prepare_datagrid();
			
		});
}


function internalBudget()
{
	var row=$('#dgJob').datagrid('getSelected');
	if(row)
	{
		$('#dlgInternalBudget').dialog('open').dialog('setTitle','Create Internal Budget');
		
		$("#jobNoInternal").val(row.jobNo);
		prepare_Internaldatagrid();
		setTotalInternal();
		if(row.issueType=='budget'){
			getBudgetNoxx(row.jobNo);	
		}else{
			
			setDetailsPorformaInvoiceInternal(row.jobNo);
		}
		
	}else
	{
		$.messager.show({title:'Info',msg:'Select a job to define its internal budget'});
	}

}
function saveBudgetInternalBugdet(){
		url='controller/jobController.php?action=addInternal';
		$.messager.progress();
	 $('#frmBudgetInternal').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Internal Prepared'});
						//$('#dlgLpo').dialog('close'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


}
function newBudgetJob(){
	
	$('#dlgBudgetJob').dialog('open').dialog('setTitle','Create a budget job:');
	$('#frmBudgetJob').form('clear');
	 $.messager.progress({title:'loading',msg:'Connecting to server..'});
	getJobNo();
	 //$.messager.progress('close');
	$('#headerNo').val('');
	$('#itemName').val('');
	url='controller/jobController.php?action=add';
}

function newInvoiceJob(){
	$('#dlgInvoiceAdd').dialog('open').dialog('setTitle','Create A Invoice Job');
	loadingBar('Connecting to server');
	$('#frmInvoiceAdd').form('clear');
	
	getInvoiceNo()
	closeBar();
	//$('#headerNo').val('');
	url='controller/jobController.php?action=addInvoiceJob';
}
function editBudgetJob(){
	
	var row=$('#dgJob').datagrid('getSelected');
	if(row){
		if(row.operationStatus=='opened'){
			if(row.issueType=='budget'){
				$('#dlgBudgetJob').dialog('open').dialog('setTitle','Edit Out going budget  for::'+row.client);
				loadingBar('Loading Job information');
				$('#frmBudgetJob').form('load',row);
				getBudgetNoForEdit(row.jobNo);
				closeBar();
				url='controller/jobController.php?action=updateBudgetJob';	
			}else{
				$('#dlgInvoiceAdd').dialog('open').dialog('setTitle','Edit A Invoice Job');
				$('#frmInvoiceAdd').form('load',row);
				$('#jobNoInvoice').val(row.jobNo)
				getInvoiceNoForEdit(row.jobNo);
	
				url='controller/jobController.php?action=updateInvoiceJob';
				
			}
	
		}else{
		$.messager.show({title:'Info',msg:'A Job is not Editable To make it editable'});	
		}
	}else{
		$.messager.show({title:'Info',msg:'Please Select A Job to edit'});
	}
}

function progShow(){
	var prog="<div class='progress progress-striped active'><div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'><span class='sr-only'>Please Wait.......</span></div></div>";
	$('#viewDetails').html("");
	$('#viewDetails').html(prog);
}
function progShow2(){
	var prog="<div class='progress progress-striped active'><div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'><span class='sr-only'>Please Wait.....</span></div></div>";
	$('#viewDetails2').html("");
	$('#viewDetails2').html(prog);
}

function viewBudget(){
	
	var row=$('#dgJob').datagrid('getSelected');
	if(row){
		if(row.operationStatus=='opened' ){
		$('#dlgViewDetails').dialog('open').dialog('setTitle','View Job Detail '+row.client);
			progShow();
			if(row.issueType=='budget'){
				getBudgetNox(row.jobNo);	
				var budgetNo=$('#budgetNoLpo').val();
				$('#clientLpo').val(row.client);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				$('#lop_button').linkbutton('enable');
			}else{
				
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('enable');
			}
	
		}else if(row.operationStatus=='confirmed' || row.operationStatus=='paid' ){
			
			$('#dlgViewTaxInvoice').dialog('open').dialog('setTitle','Tax Invoice:: '+row.client);
			progShow2();
			setDetails2(row.jobNo);
			$('#jobNoLpo23').val(row.jobNo);
			$('#productLpo23').val(row.product);
			$('#clientLpo23').val(row.client);
			$('#lop_button2').linkbutton('disable');
	
		}else{
		
		$('#dlgViewDetails').dialog('open').dialog('setTitle','View Budget for:: '+row.client);
			progShow();
			if(row.issueType=='budget'){
				getBudgetNox(row.jobNo);	
				var budgetNo=$('#budgetNoLpo').val();
				
				$('#clientLpo').val(row.client);
				
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				
				$('#lop_button').linkbutton('disable');
			}else{
				
				
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('disable');
			}
			//$.messager.show({title:'Info',msg:'select a Job  to edit'});
		}
	}else{
		$.messager.show({title:'Info',msg:'select a Job  to to view edit'});
	}
}

function recevieLpo(){
	
		
	$('#dlgLpo').dialog('open').dialog('setTitle','Generate Tax Invoice');
	$('#frmLpo').form('clear');
	//prepare_datagrid();
	url='controller/budgetController.php?action=receviceLpo';
	$('#clientLpo2').val($('#clientLpo').val());
	$('#budgetNoLpo2').val($('#budgetNoLpo').val());
	$('#jobNoLpo2').val($('#jobNoLpo').val());
	$('#productLpo2').val($('#productLpo').val());
	
		
}
function receviePayment(){
	
	if(Userrole=="finance"){	
	$('#dlgRecepit').dialog('open').dialog('setTitle','Payment of an invoice');
	$('#frmRecepit').form('clear');
	
	url='controller/transactionController.php?action=payment';
	
	$('#jobid4').val($('#jobNoLpo23').val());
	$('#productRecepit').val($('#productLpo23').val());
	getAccountBal($('#clientLpo23').val());
	getOpenYearAndMonthRecepit()
	  }else{
		$.messager.alert('Message','You can not recevice a payment please inform the accountant about the payment or call 0781587081 support team');
	}
	
		
}
function getAccountBal(clientName)
{
	$.post('controller/transactionController.php?action=getAAcodeBal&clientName='+clientName,{actionx:'getAAcodeBal',clientNamex:clientName},function(response){
		alert(response);
		//alert(clientName);
		str=response.split('$');
		$('#balancebefore').numberbox('setValue',parseInt(str[0]));
		$('#credit').val(str[1]);
		$('#analcode').val(str[2]);
	});
}
function getOpenYearAndMonthRecepit(){
		$.post('controller/finanicalController.php',{action:'getYear'},function(data){
			//alert(data);
			var newstr=data.split('*');
			$('#year_codeRecepit').val(newstr[0]);
			$('#month_codeRecepit').val(newstr[1]);
		});
	}
	
function saveLpo(){
		
		loadingBar('Saving local purchase information');
	 $('#frmLpo').form('submit',{ url: url, 
	 onSubmit: function(){
		
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Lpo Required'});
						$('#dlgLpo').dialog('close'); // close the dialog
						 $('#dgJob').datagrid('reload');// reload the user
		}
		
	}
}); 
		
}

function saveBudgetJob(){
		
		loadingBar('Saving Proform invoice information');
	 $('#frmBudgetJob').form('submit',{ url: url, 
	 onSubmit: function(){
		
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Job Saved'});
						$('#dlgBudgetJob').dialog('close'); // close the dialog
						$('#dgJob').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function saveInvoiceJob(){
		//url='controller/jobController.php?action=add';
		url='controller/jobController.php?action=addInvoiceJob';
		$.messager.progress();
	 $('#frmInvoiceAdd').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Job Saved'});
						$('#dlgInvoiceAdd').dialog('close'); // close the dialog
						$('#dgJob').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function deleteJob(){
	$.messager.alert('Warning','This operation was not secure so it was turned off for  more information call help line:0781587081');
}
function getJobNo(){
	
		$.post('controller/jobController.php',{action:'setupJob'},function (response){
			var str;
			str=response.split('*');
			$('#budgetNo').val(str[0]);
			$('#jobNo').val(str[1]);
			prepare_datagrid();
			$.messager.progress('close');
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getJobNoInternal(){
	
		$.post('controller/jobController.php',{action:'setupJob'},function (response){
			var str;
			str=response.split('*');
			//$('#budgetNo').val(str[0]);
			//$('#jobNo').val(str[1]);
			prepare_datagrid();
			//prepare_datagrid2();
			//loadSummary();
		});
	
}

function getInvoiceNo(){
	
		$.post('controller/budgetController.php',{action:'setProforma'},function (response){
			var str;
			str=response.split('*');
			$('#budgetNoInvoice').val(str[0]);
			$('#jobNoInvoice').val(str[1]);
			prepare_datagridInvoice();
			//prepare_datagrid();
			//prepare_datagrid2();
			//loadSummary();
		});
	
}
function getInvoiceNoForEdit(id){
	
		$.post('controller/budgetController.php',{action:'loadState',id:id},function (response){
			var str;
			
			//$('#budgetNoInvoice').val(str[0]);
			$('#budgetNoInvoice').val(response);
			prepare_datagridInvoice();
			
		});
	
}


function editJob()
{
	var row=$('#dgJob').datagrid('getSelected');
	if(row){
		if(row.issueType=='budget'){
			
		$('#dlgViewDetails').dialog('open').dialog('setTitle','View Budget for:: '+row.client);
			//$('#viewDetails').html("");
			if(row.issueType=='budget'){
				
				
				
	
				
				getBudgetNox(row.jobNo);	//getBudgetNo(row.jobNo);
				var budgetNo=$('#budgetNoLpo').val();
				
				$('#clientLpo').val(row.client);
				
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				
				$('#lop_button').linkbutton('enable');
			}else{
				
				
				$('#clientLpo').val(row.client);
				$('#budgetNoLpo').val(row.budgetNo);
				$('#jobNoLpo').val(row.jobNo);
				$('#productLpo').val(row.product);
				setDetailsPorformaInvoice(row.jobNo);
				$('#lop_button').linkbutton('enable');
			}
	
		
		}
		
	}else{
		
	}
	
}

function releaseFund()
{
	
	var row=$('#dgJob').datagrid('getSelected');
	if(row)
	{
		if(row.operationStatus!='opened'){
			
		$('#dlgReleaseFund').dialog('open').dialog("setTitle",'Release to implement Bugdet');
		$('#frmReleaseFund').form('clear');
		$('#jobIdFund').val(row.jobNo);
		$('#departmentFund').val(row.product);
		$.messager.progress();
		$.post('controller/budgetController.php?action=getRelasedFunds',{jobIdFunds:row.jobNo},function(data){
			//alert(data);
			$('#invoiceNoFund').val(data.invoiceNo);
			$('#paidAmountFund').val(data.paidAmount);
			$('#totalFund').val(data.expense);
			$('#balanceFund').val(data.balance);
			url="controller/releaseFundsController.php?action=add";	
			$.messager.progress('close');
			
		},'json');
		}else{
		$.messager.show({title: 'Warning',msg: 'Your can not relase fund for any open job'});
			
		}
		
	}else
	{
		$.messager.show({title: 'Warning',msg: 'Please select a job'});
	}
}
function accountability(){
	var row=$('#dgJob').datagrid('getSelected');
	$.messager.progress();
	if(row)
	{
		//check if funds 
		$.post('controller/budgetController.php?action=getRelasedFundsForJob',{jobIdFunds:row.jobNo},function(data){
			$.messager.progress('close');
			//var amount=parseInt(data);
			//alert(data.paidAmount+"/"+data.status+"/"+data.DayOfForce);
			if(parseInt(data.paidAmount)>0){
				if(data.status=='on'){
				$('#dlgAccountability2').dialog('open').dialog('setTitle','Accountability');
				$('#dgAccountability').datagrid('load',{id:row.jobNo});
				$('#releaseFundAmount').val(data.paidAmount);
				}else
				{
					$.messager.alert('Message','The funds released for this project are locked call the accountant to open the accountability of funds');
				}
			}else{
				$.messager.alert('Message','No funds released or Fund ready Accounted for');
			}
		},'json');
	}else
	{
		$.messager.progress('close');
		$.messager.show({title: 'Warning',msg: 'Please select a job'});
	}
}

</script>