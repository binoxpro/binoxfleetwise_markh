<div id="dlgInvoiceItem" class="easyui-dialog" style="height:auto; width:50%; padding-top:10px; padding-left:10px;" closed='true' buttons='#button_barInvoiceItem' modal="true">
<form id="frmInvoiceItem" name="frmInvoiceItem" method="post">
<table width="100%" cellpadding="3" cellspacing="3"><tr><td>&nbsp;</td><td><input type="text" name="budgetNo" value="" id="budgetNoInvoice3" /></td></tr>
<!--<tr><td>Header:</td><td><input type="text" name="itemNamex" value="" id="itemName3" />&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-primary btn-circle" onclick="addHeader()"><i class="fa fa-plus-square-o fa-lg"></i></a></td></tr>-->
<tr><td><label>Description</label></td><td><textarea name="itemName" id="itemName4"></textarea></td></tr>
<tr><td><label>Quantity</label></td><td><input type="text" name="qty" value="" id="qty2" /></td></tr>
<tr><td><label>Rate</label></td><td><input type="text" name="unitPrice" value="" id="unitPrice2" /></td></tr>
<tr><td><label>No Days</label></td><td><input type="text" name="no_of_days" value="1" id="nodays2" onblur="getCal2(this.value)" /></td></tr>
<tr><td><label>Total</label></td><td><input type="text" name="total" value="" id="total2" /></td></tr>
</table>
</form>
</div>
 <div id="button_barInvoiceItem">
<a href="#" class="btn btn-success" onclick="saveInvoiceItem()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgInvoiceItem').dialog('close')"><i class="fa fa-times"></i>Close</a>
</div>
<script type="text/javascript">
function saveInvoiceItem(){
	saveForm("#frmInvoiceItem",url,"#dlgInvoiceItem");
	var budgetNo=$('#budgetNoInvoice3').val();
	$('#dgInvoiceTax').datagrid('reload',{budgetNo:budgetNo});
}
function getCal2(x)
{
	var qty=parseInt($('#qty2').val());	
	var rate=parseInt($('#unitPrice2').val());
	var noday=parseInt(x);
	var ans=qty*rate*noday;
	$('#total2').val(ans);
}
</script>