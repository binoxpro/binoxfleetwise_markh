<div id="dlgBudgetItem" class="easyui-dialog" style="height:auto; width:50%; padding-top:10px; padding-left:10px;" closed='true' buttons='#button_barBudgetItem' modal="true">
<form id="frmBudgetItem" name="frmBudgetItem" method="post">
<table width="100%" cellpadding="3" cellspacing="3"><tr><td>&nbsp;</td><td><input type="hidden" name="budgetNo" value="" id="budgetNo3" /><input type="hidden" name="headNo" value="" id="headerNo3" /></td></tr>
<tr><td>Header:</td><td><input type="text" name="itemNamex" value="" id="itemName3" />&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-primary btn-circle" onclick="addHeader()"><i class="fa fa-plus-square-o fa-lg"></i></a></td></tr>
<tr><td><label>Description</label></td><td><textarea name="itemName" id="itemName4"></textarea></td></tr>
<tr><td><label>Quantity</label></td><td><input type="text" name="qty" value="" id="qty" /></td></tr>
<tr><td><label>Rate</label></td><td><input type="text" name="unitPrice" value="" id="unitPrice" /></td></tr>
<tr><td><label>No Days</label></td><td><input type="text" name="nodays" value="1" id="nodays" onblur="getCal(this.value)" /></td></tr>
<tr><td><label>Total</label></td><td><input type="text" name="total" value="" id="total" /></td></tr>
</table>
</form>
</div>
 <div id="button_barBudgetItem">
<a href="#" class="btn btn-success" onclick="saveBudgetItem()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgBudgetItem').dialog('close')"><i class="icon-remove"></i>Close</a>
</div>
<?php
require_once('addBudgetHeader.php');
?>
<script type="text/javascript">
function addHeader(){
		$('#dlgBudgetHeader').dialog('open').dialog('setTitle','Add Budget Header');
		$('#frmBudgetHeader').form('clear');
		$('#budgetNo2').val($('#budgetNo3').val());
		
	}
function saveHeader(){
		var budgetNo=$('#budgetNo2').val();
		var itemName=$('#itemName2').val();
		if(itemName!=null || itemName!=''){
			$.messager.progress();
		$.post('controller/budgetController.php?action=addHeader&budgetNo='+budgetNo+"&itemName="+itemName,function(data){
			
			if(data==null){
				$.messager.progress('close');
				$.messager.alert('warning','Header');
			}else{
				$.messager.progress('close');
				$('#headerNo').val(data);
				$('#itemName').val(itemName);
				$('#headerNo3').val(data);
				$('#itemName3').val(itemName);
				$('#dlgBudgetHeader').dialog('close');
				//$('#dgBudget').edatagrid('reload');
			}
			
		});
		}else{
			$.messager.alert('Warning','supply the Header Item Name');
		}
}

function saveBudgetItem(){
	
	saveForm("#frmBudgetItem",url,"#dlgBudgetItem");
	var budgetNo=$('#budgetNo3').val();
	$('#dgBudget').datagrid('reload',{budgetNo:budgetNo});
}
function getCal(x)
{
	var qty=parseInt($('#qty').val());	
	var rate=parseInt($('#unitPrice').val());
	var noday=parseInt(x);
	var ans=qty*rate*noday;
	$('#total').val(ans);
}
</script>