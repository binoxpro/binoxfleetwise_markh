<div id="dlgReleaseFund" class="easyui-dialog" style="height:560px; width:800px; padding:3px;" closed='true' buttons='#button_barReleaseFund'>
<form id="frmReleaseFund" name="frmReleaseFund" method="post">
<table cellspacing="3" width="100%">
<tr><td><label>Job ID</label></td><td><input type="text" name="jobIdFund" value="" id="jobIdFund" readonly="readonly"  /></td></tr>
<tr><td><label>InvoiceNo</label></td><td><input type="text" name="invoiceNoFund" value="" id="invoiceNoFund" readonly="readonly" /></td></tr>
<tr><td><label>Total</label></td><td><input type="text" name="totalFund" value="" id="totalFund" readonly="readonly" /></td></tr>
<tr><td><label>Paid Amount</label></td><td><input type="text" name="paidAmountFund" value="" id="paidAmountFund" readonly="readonly" /></td></tr>
<tr><td><label>Details</label></td><td><textarea name="detailFund" cols="10" rows="5"></textarea></td></tr>
<tr><td><label>Amount</label></td><td><input type="text" name="amountFund" value="" id="amountFund" onblur="setFundBalance(this.value)" /></td></tr>
<tr><td><label>Balance</label></td><td><input type="text" name="balanceFund" value="" id="balanceFund" readonly="readonly" /></td></tr>
<tr><td><label>Days</label></td><td><input type="text" name="daysFund" value="" id="daysFund"/></td></tr>
<tr><td><label>Date</label></td><td><input type="text" name="dateFund" value="" id="dateFund" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" /></td></tr>
<tr><td><label>Payment Method</label></td><td><select name="paymentMethodFund" id="paymentMethodFund" class="easyui-combobox" style="height:auto;" ><option value="cash">Cash</option><option value="cheque">Cheque</option><option value="ETF">Electron Transfer</option></select></td></tr>
<tr><td><label>Ref No</label></td><td><input type="text" name="refNoFund" value="" id="refNoFund"/>{This is cheque No/Payment Voucher No}</td></tr>
<tr><td><label>Debit Account</label></td><td><select name="debitAccount" class="easyui-combogrid" data-options="panelWidth:250,idField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view',columns:[[{field:'account_code',title:'Account Code',width:50},{field:'account_name',title:'ACCOUNT',width:120},{field:'account_balance',title:'Current Balance',width:120}]]"></select></td></tr>
<tr><td>Given To:</td><td><select name="onFund" id="onFund" class="easyui-combogrid" data-options="panelWidth:250,idField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=viewCategory&id=staff',columns:[[{field:'analysisCode',title:'Code',width:50},{field:'analysisName',title:'Name',width:120}]]"></select></td></tr>

<tr><td><label>Credit Account</label></td><td><select name="creditAccount" class="easyui-combogrid" data-options="panelWidth:250,idField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view_cash_book_payment',columns:[[{field:'account_code',title:'Account Code',width:50},{field:'account_name',title:'ACCOUNT',width:120},{field:'account_balance',title:'Current Balance',width:120}]]"></select></td></tr>
<tr><td><label>Department</label></td><td><input type="text" name="departmentFund" value="" id="departmentFund" readonly="readonly" /> </td></tr>
</table>
</form>
</div>
 <div id="button_barReleaseFund">
<a href="#" class="btn btn-success" onclick="saveReleaseFund()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgReleaseFund').dialog('close')"><i class="icon-remove"></i>Close</a>
</div>
<script type="text/javascript">
function saveReleaseFund(){
		//url='controller/jobController.php?action=add';
		$.messager.progress();
	 $('#frmReleaseFund').form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Message',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Message',
					  msg: 'Fund release'});
						$('#dlgReleaseFund').dialog('close'); // close the dialog
						$('#dgJob').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
}
function setFundBalance(x){
	var bal=parseInt($('#totalFund').val())-(parseInt($('#paidAmountFund').val())+parseInt(x));
	$('#balanceFund').val(bal);
}

</script>