<table id="dgBudgetInternal" class="easyui-datagrid" title="Internal Budget" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true" fitcolumns="true"
			data-options="
				iconCls: 'icon-edit',
				toolbar: '#tbBudgetInternal',
                method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'Id',width:50,editor:{type:'text'}" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:320, editor:{type:'text'}">Description</th>
				<th data-options="field:'qty',width:30,
						editor:{
							type:'numberbox'
						}">QTY</th>
				<th data-options="field:'unitPrice',width:140,align:'right',editor:{type:'numberbox'}">Rate</th>
				<th data-options="field:'nodays',width:50,align:'right',editor:{type:'numberbox'}">No Days</th>
				<th data-options="field:'total',width:100,editor:{type:'numberbox'}">Total</th>
               
                <th data-options="field:'supplier',width:90,
						editor:{
							type:'combobox',
					options:{panelWidth:100,valueField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=viewCategory&id=sup'
                            }
						}">Supplier</th>
                        <th data-options="field:'contact',width:100">Contact</th>
                        <th data-options="field:'email',width:100">Email</th>
			</tr>
		</thead>
	</table>

	<div id="tbBudgetInternal" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-primary" onclick="javascript:$('#dgBudgetInternal').edatagrid('addRow')"><i class="icon-plus-sign"></i>Add Item</a>
		
		<a href="javascript:void(0)" class="btn btn-link" onclick="saveIt()"><i class="icon-ok-sign"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="deleteItem();setTotalInternal()"><i class="icon-remove"></i>Remove</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="approve();setTotalInternal()"><i class="icon-thumb-up"></i>Approve</a>
         <a href="#" class="btn btn-link" onclick="addSupplier()"><i class="icon-user"></i>New Supplier</a>
	</div>
	<?php 
	require_once('addCode.php'); 
	?>
	<script type="text/javascript">
	function approve(){
		if(Userrole=="all"){
		var row=$('#dgBudgetInternal').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Message','Are you sure you would like to approve this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=approveBudgetItemNew&id='+row.Id,function(result){
			if(result.success){
				$.messager.show({title:'Debug',msg:result.success});
				loadGrid();
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
		}else{
			$.messager.show({title:'Warning',msg:'You do not have rights to Approval'});
		}
	}
	function saveIt(){
		javascript:$('#dgBudgetInternal').edatagrid('saveRow');
		loadGrid();
		setTotalInternal();
	}
	
	function addSupplier()
	{
		$('#dlgSupplier').dialog('open').dialog('setTitle','Add Supplier');
		$('#frmSuppier').form('clear');
		url='controller/analysisCodeController.php?action=addSupplier';
		
	}
	function prepare_Internaldatagrid(){
			//var id=$('#budgetNo').val();
			var budgetNo=$('#jobNoInternal').val();
			
			
		
			$('#dgBudgetInternal').edatagrid({
				method:'get',
				url:'controller/budgetController.php?action=viewItemInternal&budgetNo='+budgetNo,
				saveUrl:'controller/budgetController.php?action=addItemInternal&budgetNo='+budgetNo,
				updateUrl:  'controller/budgetController.php?action=updateItemInternal&budgetNo='+budgetNo,
				onAdd:function(index,row){
					//set_valuesx(index);
					calcux(index);
				},
				onEdit:function(index,row){
					if(row.approved=='NO'){
					calcux(index);
					}else{
						loadGrid();
						$.messager.show({title:'Message',msg:'Please the budget Item is aready approved'});
					}
				}
			});
			
		}
	
	function loadGrid(){
		
		$("#dgBudgetInternal").edatagrid('reload');
		setTotalInternal();
		
	}
	
	function calcux(rowIndex){
		var editorsx=$("#dgBudgetInternal").edatagrid('getEditors',rowIndex);
		var qty=editorsx[2];
		var rates=editorsx[3];
		var noday=editorsx[4];
		var total=editorsx[5];
		noday.target.blur(function(){
		var qty1=qty.target.val();
		var rate1=rates.target.val();
		var noday1=noday.target.val();
		var totalcost=qty1*rate1*noday1;
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function set_valuesx(rowIndex){
		//var editorsx=$("#dgBudgetInternal").edatagrid('getEditors',rowIndex);
		//var editorsx=$("#dgBudgetInternal").edatagrid('getEditors',rowIndex);
		//var items=editors[0];
		//var qty=editorsx[0];
		//var rates=editorsx[2];
		//var noday=editorsx[3];
		//var total=editorsx[4];
		
		//qty.target.blur(function(){
			
			//var qty1=qty.target.val();
			//var rate1=1;rates.target.val();
			//var noday1=1;noday.target.val();
			//var totalcost=qty1*rate1*noday1;
			//alert(totalcost);
		//$(total.target).numberbox('setValue',totalcost);
		
			
		//});
		
		
	}
	function deleteItemInternal(){
		var row=$('#dgBudgetInternal').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItem&id='+row.subItemNo,function(result){
			if(result.success){
				
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	
function setTotalInternal(){
	var id=$('#jobNoInternal').val();
	$.post('controller/budgetController.php?action=viewTotalInternal&budgetNo='+id,function (data){
		if(data!=null){
		$('#incomeInternal').val(data);
		}
		
	});
}
    </script>