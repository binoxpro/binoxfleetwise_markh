<table id="dgInvoiceTax" class="easyui-datagrid" title="Proforma Invoice" style="height:auto;" pagination="true" rownumbers="true" fitcolumns="true"
			data-options="
				singleSelect: true,
				toolbar: '#tbInvoiceTax',
				method: 'get',
                showFooter:true,
                url:'controller/budgetController.php?action=viewInvoiceItem'
			">
		<thead>
			<tr>
            	<th data-options="field:'itemNo',width:50" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:350">Description</th>
				<th data-options="field:'qty',width:30">QTY</th>
				<th data-options="field:'unitPrice',width:140,align:'right'">Rate</th>
				<th data-options="field:'no_of_days',width:50,align:'right'">No Days</th>
				<th data-options="field:'total',width:100">Total</th>
               
			</tr>
		</thead>
	</table>

	<div id="tbInvoiceTax" style="height:auto">
    
		<a href="javascript:void(0)" class="btn btn-primary" onclick="addProformaInvoice()"><i class="fa fa-plus-circle"></i>Add</a>
		
		<a href="javascript:void(0)" class="btn btn-link" onclick="editProformaInvoice()"><i class="fa fa-edit"></i>Edit</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="deleteItemInvoice();setTotal();reloadDataGrid2()"><i class="icon-trash"></i>Remove Item</a><br />
        
	</div>
	<?php //require_once('addBudgetHeader.php'); ?>
	<script type="text/javascript">
	function addProformaInvoice(){
		
		$("#dlgInvoiceItem").dialog('open').dialog('setTitle','Add Invoice Item');
		$("#frmInvoiceItem").form('clear');
		//$("#itemName3").val($("#itemName").val());
		//$("#headerNo3").val($("#headerNo").val());
		$('#budgetNoInvoice3').val($('#budgetNoInvoice').val());
		url="controller/budgetController.php?action=addInvoiceItem";
		
	}
	function editProformaInvoice(){
		var row=$('#dgInvoiceTax').datagrid('getSelected');
		if(row){
			$("#dlgInvoiceItem").dialog('open').dialog('setTitle','Edit Invoice Item');
		$("#frmInvoiceItem").form('load',row);
		url='controller/budgetController.php?action=updateInvoiceItem&itemNo='+row.itemNo
		$('#budgetNoInvoice3').val($('#budgetNoInvoice').val());
		}else{
			
		}
	}
	function saveInvoiceTax34()
	{
		javascript:$('#dgInvoiceTax').edatagrid('saveRow');
		reloadDataGrid2();
	}
	
	function prepare_datagridInvoice(){
			var id=$('#budgetNoInvoice').val();
			var budgetNo=$('#budgetNoInvoice').val();
			$('#dgInvoiceTax').datagrid('reload',{budgetNo:budgetNo});
			
		
			//$('#dgInvoiceTax').edatagrid({
//				method:'get',
//				url: 'controller/budgetController.php?action=viewInvoiceItem&budgetNo='+budgetNo,
//				saveUrl:  'controller/budgetController.php?action=addInvoiceItem&budgetNo='+budgetNo,
//				updateUrl:  'controller/budgetController.php?action=updateInvoiceItem&budgetNo='+budgetNo,
//				onAdd:function(index,row){
//					set_values2(index);
//				},
//				onEdit:function(index,row){
//					calcu2(index);
//				}
//			});
			
		}
		
	function reloadDataGrid2(){
		var id=$('#budgetNoInvoice').val();
			var budgetNo=$('#budgetNoInvoice').val();
			$('#dgInvoiceTax').datagrid('reload',{budgetNo:budgetNo});
	}
	function calcu2(rowIndex){
		
		var editors=$('#dgInvoiceTax').edatagrid('getEditors',rowIndex);
		
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	
	}
	function set_values2(rowIndex){
		var editors=$('#dgInvoiceTax').edatagrid('getEditors',rowIndex);
		
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		noday.target.blur(function(){
			
		var qty1=qty.target.val();
		var rate1=rates.target.val();
		var noday1=noday.target.val();
		var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItemInvoice(){
		var row=$('#dgInvoiceTax').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItemInvoice&id='+row.itemNo,function(result){
			if(result.success){
				$('#dgInvoiceTax').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgInvoiceTax').datagrid('reload');	
}
function setTotalInvoice(){
	var id=$('#budgetNoInvoice').val();
	$.post('controller/budgetController.php?action=viewTotalInvoice&budgetNo='+id,function (data){
		if(data!=null){
		$('#incomeInvoice').val(data);
		}
		
	});
}
    </script>