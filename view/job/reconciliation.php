<div id="dlgRcon" class="easyui-dialog" style="height:500px; width:900px;" closed='true' buttons='#button_barRcon'>
<form id="frmBudgetInternal2" name="frmBudgetInternal2" method="post">
<table cellspacing="3" width="100%" >
<tr><td><label>Job ID:</label></td><td><input type="text" name="jobNo" value="" id="jobNoInternal2" /></td></tr>
<tr><td>Credit Account</td><td><select name="debitAccount" id="debitA" style="width:100px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view',columns:[[{field:'account_code',title:'Account Code',width:50},{field:'account_name',title:'ACCOUNT',width:120}]]"></select></td></tr>
<tr><td>Pend On:</td><td><select name="on" id="on" style="width:100px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=viewCategory&id=staff',columns:[[{field:'analysisCode',title:'Code',width:50},{field:'analysisName',title:'Name',width:120}]]"></select></td></tr>
<tr><td>Department:</td><td><select name="depart" id="depart" style="width:100px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=viewCategory&id=Pts',columns:[[{field:'analysisCode',title:'Code',width:50},{field:'analysisName',title:'Name',width:120}]]"></select></td></tr>
</table>
</form>
<h4>Reconciliation:</h4>
<?php require_once('pluginRecon.php'); ?>
</div>
</div>
</div>
 <div id="button_barRcon">
<a href="#" class="btn btn-success" onclick="saveRcon()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgRcon').dialog('close')"><i class="icon-remove"></i>Close</a>
</div>