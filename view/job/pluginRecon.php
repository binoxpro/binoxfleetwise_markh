<div style="margin:5px; width:100%;">
<table id="dgRcon" class="easyui-datagrid" title="Reconciliation" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbRconBug',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'Id',width:50,editor:{type:'text'}" hidden="true">Item No</th>
				<th data-options="field:'itemName',width:120, editor:{type:'text'}">Item</th>
				<th data-options="field:'total',width:100,
						editor:{
							type:'numberbox'
						}">Total</th>
				<th data-options="field:'accural',width:140,align:'right',editor:'numberbox'">Recepit Value</th>
                <th data-options="field:'acode',width:90,
						editor:{
							type:'combobox',
					options:{panelWidth:100,valueField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view'
                            }
						}">Debit Account</th>
                        <th data-options="field:'onwho',width:140,align:'right',editor:'text'">On</th>
                        <th data-options="field:'account',width:140,align:'right',editor:'text'">Account</th>
                        <th data-options="field:'datex',width:140,align:'right',editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}">Date</th>
                        <th data-options="field:'voucherNo',width:140,align:'right',editor:'text'">VoucherNo</th>
                        
			</tr>
		</thead>
	</table>

	<div id="tbRconBug" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-primary" onclick="javascript:$('#dgRcon').edatagrid('saveRow');reloadDataGridInternal()"><i class="icon-ok-sign"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="deleteItem();reloadSummary2()"><i class="icon-minus-sign"></i>Remove</a>
         
	</div>
    </div>
	<?php 
	require_once('addCode.php'); 
	?>
	<script type="text/javascript">
	
	function prepare_rcon(){
			//var id=$('#budgetNo').val();
			var budgetNo=$('#jobNoInternal2').val();
			$('#dgRcon').edatagrid({
				method:'get',
				url:'controller/budgetController.php?action=viewItemInternal&budgetNo='+budgetNo,
				saveUrl:'controller/budgetController.php?action=reconciliation&budgetNo='+budgetNo,
				updateUrl:'controller/budgetController.php?action=reconciliation&budgetNo='+budgetNo,
				onAdd:function(index,row){
					//set_valuesx(index);
					
				},
				onEdit:function(index,row){
					calcux2(index);
				}
			});
			
		}
	
	function reloadDataGridInternal(){
		$('#dgRcon').edatagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
	function calcux2(rowIndex){
		var editors=$('#dgRcon').edatagrid('getEditors',rowIndex);
		var headno=editors[5];
		var account=editors[6]
		var items=editors[3];
		items.target.blur(function(){
			var head_val=$('#on').combobox('getValue');
			
		$(headno.target).val(head_val);
		$(account.target).val($('#debitA').combobox('getValue'));
			
		});
		
		
		
	}
	function set_valuesx(rowIndex){
		var editors=$('#dgRcon').edatagrid('getEditors',rowIndex);
		var headno=editors[6];
		var items=editors[1];
		var qty=editors[2];
		var noday=editors[4]
		var rates=editors[3];
		var total=editors[5];
		items.target.blur(function(){
			var head_val=$('#headerNo').val();
			
		$(headno.target).val(head_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItemInternal(){
		var row=$('#dgRcon').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItem&id='+row.subItemNo,function(result){
			if(result.success){
				
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgRcon').datagrid('reload',{budegtNo:id});	
}
function setTotalInternal(){
	var id=$('#jobNoInternal').val();
	$.post('controller/budgetController.php?action=viewTotalInternal&budgetNo='+id,function (data){
		if(data!=null){
		$('#incomeInternal').val(data);
		}
		
	});
}
    </script>