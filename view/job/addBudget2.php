<table id="dgBudget" class="easyui-datagrid" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true" fitcolumns="true"
			data-options="
				iconCls:'icon-edit',
				singleSelect:true,
				toolbar:'#tbBudget',
				method:'get',
                showFooter:true,
                url:'controller/budgetController.php?action=viewItem'
			">
		<thead>
			<tr>
            	<th data-options="field:'subitemNo',width:50" hidden="true">Item No</th>
				<th field='itemName' width='350'>Description</th>
				<th data-options="field:'qty',width:30">QTY</th>
				<th data-options="field:'unitPrice',width:140,align:'right'">Rate</th>
				<th data-options="field:'nodays',width:50,align:'right'">No Days</th>
				<th data-options="field:'total',width:100">Total</th>
                <th data-options="field:'headNo',width:100" hidden="false">headerNo</th>
			</tr>
		</thead>
	</table>

	<div id="tbBudget" style="height:auto">
    <!--<a href="javascript:void(0)" class="btn btn-primary" onclick="addHeader()"><i class="icon-plus"></i>Add Header</a>-->
		<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="addBudgetItem()"><i class="fa fa-plus-sign"></i>Add Item</a>
		
		<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="editBudgetItem()"><i class="fa fa-edit-o"></i>Edit</a>
        <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="deleteItem()"><i class="icon-trash"></i>Remove</a><br />
        <input type="hidden" name="headerNo" id="headerNo" value="" readonly="readonly"  />&nbsp;&nbsp;&nbsp;<input type="hidden" name="itemNames" id="itemName" value="" readonly="readonly"/>
	</div>
	<?php 
		require_once('addBudgetItem.php');
	 ?>
	<script type="text/javascript">
	function editBudgetItem(){
		var row=$('#dgBudget').datagrid('getSelected');
		if(row){
			$("#dlgBudgetItem").dialog('open').dialog('setTitle','Add Invoice Item');
		$("#frmBudgetItem").form('load',row);
		url='controller/budgetController.php?action=updateItem&subItemNo='+row.subItemNo
		//$("#itemName3").val();
		//$("#headerNo3").val();
		//$('#budgetNo3').val();

		}else{
			
		}
	}
	function saveBudgetForm(){
		//javascript:$('#dgBudget').edatagrid('saveRow');
		//reloadDataGrid();
		//reloadSummary2();
	}
	function addBudgetItem(){
		$("#dlgBudgetItem").dialog('open').dialog('setTitle','Add Invoice Item');
		$("#frmBudgetItem").form('clear');
		$("#itemName3").val($("#itemName").val());
		$("#headerNo3").val($("#headerNo").val());
		$('#budgetNo3').val($('#budgetNo').val());
		url="controller/budgetController.php?action=addItem";
	}
	function prepare_datagrid(){
			var id=$('#budgetNo').val();
			var budgetNo=$('#budgetNo').val();
			
			$('#dgBudget').datagrid('reload',{budgetNo:budgetNo});
		
			//$('#dgBudget').edatagrid({
//				method:'get',
//				url: 'controller/budgetController.php?action=viewItem&budgetNo='+budgetNo,
//				saveUrl:  'controller/budgetController.php?action=addItem&budgetNo='+budgetNo,
//				updateUrl:  'controller/budgetController.php?action=updateItem&budgetNo='+budgetNo,
//				onAdd:function(index,row){
//					var head=$('#headerNo').val();
//					if(head!=null && head!=''){
//					set_values(index);
//					}else{
//						$.messager.alert('Warning','Supply the header catergory');
//						$('#dgBudget').edatagrid('reload');
//					}
//				},
//				onEdit:function(index,row){
//					calcu(index)
//				}
//				
//			});
			
		}
		function get_log_id(){
		/*
		$.post('controller/transactionController.php',{action:'addLog'},function(response){
			//alert(response);
			$('#log_id').val(response);
			var id=response;
			$('#dgBudget').edatagrid({
				url:'controller/transactionController.php?action=view_temp&log='+id,
				saveUrl: 'controller/transactionController.php?action=add_temp&log='+id,
				updateUrl: 'controller/transactionController.php?action=update_temp&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy_temp&log='+id
			});
			
		});
		*/
		
	}
	function reloadDataGrid(){
		$('#dgBudget').datagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
	function calcu(rowIndex){
		
		var editors=$('#dgBudget').edatagrid('getEditors',rowIndex);
		var datex=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	
	}
	function set_values(rowIndex){
		var editors=$('#dgBudget').edatagrid('getEditors',rowIndex);
		var headno=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		items.target.blur(function(){
			var head_val=$('#headerNo').val();
			//alert(head_val);
		$(headno.target).val(head_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItem(){
		var row=$('#dgBudget').datagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/budgetController.php?action=deleteItem&id='+row.subItemNo,function(result){
			if(result.success){
				//$('#dgBudget').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#budgetNo').val();
	$('#dgBudget').datagrid('reload',{budegtNo:id});	
	}
	
	function setTotal(){
	var id=$('#budgetNo').val();
	$.post('controller/budgetController.php?action=viewTotal&budgetNo='+id,function (data){
		if(data!=null){
		$('#income').val(data);
		}
		
	});
	}
    </script>