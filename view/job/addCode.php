<div id="dlgSupplier" class="easyui-dialog" style="height:300px; width:700px; " closed='true' buttons='#button_barSupplier'>
<div class="easyui-panel" title="Supplier" style="height:auto;">
<form id="frmSupplier" name="frmSupplier" method="post">
<table style="width:100%;">
<tr><td><label>Supplier Name:</label></td><td><input type="text"  name="analysisName" value="" id="analysisNamex" /></td></tr>
<tr><td><label>Contacts</label></td><td><input type="text" name="contact" value="" id="contactx" /></td></tr>
<tr><td><label>Email</label></td><td><input type="text" name="email" value="" id="email" /></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barSupplier">
<a href="#" class="btn btn-success" onclick="saveSupplier()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgSupplier').dialog('close')"><i class="icon-remove"></i>Cancel</a>
</div>
<script type="text/javascript">
function saveSupplier(){
		
		$.messager.progress();
	 $('#frmSupplier').form('submit',{ url: url, 
	 onSubmit: function(){
		
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Supplier Added'});
						$('#dlgSupplier').dialog('close'); // close the dialog
						 
		}
		
	}
}); 
		
}
</script>
