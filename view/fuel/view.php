    <div class="col-lg-12">
    <table id="dgFuel" title="Manage Fuel" class="easyui-datagrid" style="height:700px;" url="controller/fuelController.php?action=view" pagination="true" toolbar="#toolbarFuel" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<!--<th field="id" width="50">id</th>-->
            <th field="vehicleId" width="120">Vehicle</th>
            <th field="destination" width="150">Destination</th>
            <th field="qty" width="90">Litres</th>
            <th field="currency" width="100">Cost(UGSHS)</th>
            <th field="date" width="100">Date</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarFuel" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newFuel()"><i class="fa fa-plus-circle"></i>Add Fuel</a>
        <a href="#" class="btn btn-link btn-sm" onclick="editFuel()"><i class="fa fa-pencil"></i>Edit Fuel</a>
        <a href="#" class="btn btn-link btn-sm"><i class="fa fa-times-circle"></i>Delete Fuel</a>
        <a href="?view=upload_Fuel_Datasheet" class="btn btn-link btn-sm"><i class="fa fa-upload"></i>Upload Datasheet</a>
        <a href="#" class="btn btn-link btn-sm" onclick=""><i class="fa fa-download"></i>Download Datasheet</a><hr /> 
        <span>Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatec" style="width:150px;" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatec" style="width:150px;" />Vehicle:<select name="vehicleId" id="vehicleId" class="easyui-combobox" style="height:auto; width:30%;" data-options=       			   "url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select><a href="#" class="btn btn-link btn-sm" onclick="viewSearchC()"><i class="fa fa-search"></i>Search</a></span>
    </div>
    </div>
    <?php include_once('add.php'); ?>
    
