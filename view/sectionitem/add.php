<div id="col-lg-12" >
<form id="frmSectionitem" name="frmSectionitem" method="post">
<div id="col-lg-6">
    <div class="form-group">
    <label>Section:</label>
    <input  name="sectionId" id="sectionId" value="" type="text" class="easyui-combobox" style="width:130px;" data-options="url:'controller/sectionController.php?action=view',valueField:'id',textField:'sectionId',panelWidth:'130'"></option>
    </div>
    <div class="form-group">
    <label>Item:</label>
    <input class="form-control" name="itemName" id="itemName" value="" type="text"/>
    </div>
    <div class="form-group">
    <label>Description:</label>
    <input class="form-control" name="itemDescription" id="itemDescription" value="" type="text"/>
    </div>
    <div class="form-group">
    <label>Comment:</label>
    <textarea class="form-control" name="comment" id="comment" value="" type="text"></textarea>
    </div>
    <div class="form-group">
    <label>Parent:</label>
    <input class="form-control" name="parentId" id="parentId" value="" type="text"/>
    </div>
    
</div>
<a href="#" class="btn btn-primary">Save</a>
</form>
</div>

