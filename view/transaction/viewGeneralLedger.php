<script type="text/javascript">
function viewSearch(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	$('#dgGeneralLedger').datagrid('load',{startDate:d1,endDate:e1});
}

function viewSearchCompany(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	var row=$('#dgGeneralLedger').datagrid('getSelected');
	if(row){
	$('#dgGeneralLedger').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}

</script>
<div class="col-lg-12">
<table id="dgGeneralLedger" title="Performance Reports" class="easyui-datagrid" style="height:700px;" url="controller/transactionController.php?action=viewGeneralLedger" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarGenralLedger" showFooter="true" iconCls="fa fa-file-pdf-o fa-fw">
<thead>
<tr>
	<th field="id" width="50">Record No</th>
    <th field="datex" width="120">Date</th>
    <th field="details" width="250">Details</th>
    <th field="voucherNo" width="100">Ref</th>
    <th field="Dr" width="90">Dr</th>
	<th field="Cr" width="120" >Cr</th> 
    <th field="Bal" width="120" >Balance</th> 
</tr>
</thead>

</table>
<div id="toolbarGenralLedger">
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdate" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddate" />
<a href="#" class="btn btn-primary btn-sm" onclick="viewSearch()" ><i class="fa fa-search"></i>Search</a>&nbsp;&nbsp;
<a href="#" class="btn btn-link" onclick="viewSearchCompany()" ><i class="fa fa-file-o"></i>Ledger</a>

</div>
</div>
<?php

?>