<table id="dgDebitTransaction" class="easyui-edatagrid" title="Debit Transaction" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbDebitTransaction',
				fitcolumn:true
			">
		<thead>
			<tr>
				<th data-options="field:'trans_date',width:120, editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}">Trans Date</th>
				<th data-options="field:'account_code',width:100,
						formatter:function(value,row){
							return row.account_name;
						},
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view',onSelect:function(record){
                var account_code=$(this).combobox('getValue');
               /*alert(account_code);*/
                $.post('controller/accountController.php?action=account_status',{account_code:account_code},function(response){
                /*alert(response);*/
               $('#analysis_status').val(response); 
                }
                );
                }
                            }
						}">Account Name</th>
				<th data-options="field:'account_detail',width:140,align:'right',editor:'text'">Detail</th>
				<th data-options="field:'account_amount',width:100,align:'right',editor:'numberbox'">Amount</th>
				<th data-options="field:'trans_type',width:80,editor:{type:'combobox',options:{panelWidth:80,panelHeight:'auto',valueField:'trans_type',textField:'trans_value',url:'view/transaction/trans_type.json'
                            }}">Type</th>
                <th data-options="field:'subaccount',width:80,formatter:function(value,row){
							if(row.subaccount==''){
                            	return 'N/A';
                            }else{
                            	return row.subaccount;
                            }
						},editor:{type:'combobox',options:{panelWidth:250,valueField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=view_code',onSelect:function(record){
                var analysis=$('#analysis_status').val();
                
                if(analysis=='N'){
               	 $(this).combobox('setValue','');
               	 $.messager.show({title:'info',msg:'Debit Account Subaccount Not required'});
                
               	 }else if(analysis=='Y'){
                
                	}
                }
                }
           }">Sub Account</th>
			</tr>
		</thead>
	</table>

	<div id="tbDebitTransaction" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-primary" onclick="javascript:$('#dgDebitTransaction').edatagrid('addRow')"><i class="icon-plus-sign"></i>Add Item</a>
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgDebitTransaction').edatagrid('destroyRow')"><i class="icon-minus-sign"></i>Remove</a>
		<a href="javascript:void(0)" class="btn btn-link" onclick="saveY()"><i class="icon-ok-sign"></i>Save</a>
        <br />
        <span><input type="hidden" name="analysis_status" id="analysis_status" value=""  /><input type="hidden" name="totalz" id="totalz" value=""  /></span>
	</div>
	
	<script type="text/javascript">
	var total=0;
	function saveY(){
		
		//alert(totalx);
		
		javascript:$('#dgDebitTransaction').edatagrid('saveRow');
		getTransactionTotal();
		reloadDataGrid();
	}
	function prepare_datagrid(){
			//var guest_id=$('#guest_id').val();
			//var total_new=$('#total').val();
			get_log_id();
		/*
			$('#dgDebitTransaction').edatagrid({
				url: 'controller/transactionController.php?action=view_temp_transaction_data&log='+id,
				saveUrl: 'controller/transactionController.php?action=add&log='+id,
				updateUrl: 'controller/transactionController.php?action=update&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy&log='+id
			});
			*/
		}
		function getTransactionTotal()
		{
				//var totalx=0;
				var logId=$('#log_id').val();
				$.post('controller/transactionController.php',{action:'getTransactionTotal',logId:logId},function(response){
					$('#totalz').val(response);
					
					
					//alert(total);
				});
				//return total;
		}
		function get_log_id(){
		
		$.post('controller/transactionController.php',{action:'addLog'},function(response){
			//alert(response);
			$('#log_id').val(response);
			var id=response;
			$('#dgDebitTransaction').edatagrid({
				url:'controller/transactionController.php?action=view_temp&log='+id,
				saveUrl:'controller/transactionController.php?action=add_temp&log='+id,
				updateUrl:'controller/transactionController.php?action=update_temp&log='+id,
				destroyUrl:'controller/transactionController.php?action=destroy_temp&log='+id,
				onAdd:function(index,row){
					set_valueToGrid(index);
				},
				onEdit:function(index,row){
					
				}
			});
			
		});
		
	}
	//The hhh
	function set_valueToGrid(rowIndex){
		var editors=$('#dgDebitTransaction').edatagrid('getEditors',rowIndex);
		
		var trans_date=editors[0];
		var accountName=editors[1];
		var details=editors[2]
		var amount=editors[3];
		var type=editors[4];
		$(trans_date.target).datebox('setValue',$('#trans_date').datebox('getValue'));
		
		
		
	}
	//get finanical year and month
	function getFincnicalYear(){
		$.post('controller/finanicalController.php?action=getYear',{},function(data){
			
		});
	}
	function reloadDataGrid(){
		$('#dgDebitTransaction').datagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
    </script>