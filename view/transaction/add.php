<div id="dlgPaymentTransaction" class="easyui-dialog" style="height:600px; width:900px; padding:5px;  " closed='true' buttons='#button_bar'>

<form id="frmPaymentTransaction" name="frmPaymentTransaction" method="post">
<table width="100%">
<tr><td>YearCode:</td><td><input type="text" name="year_code" value="" id="year_code" readonly="readonly" style="background:#CCC; border:0px;width:80%;" /></td><td>Month Code:</td><td><input type="text" name="month_code" value="" id="month_code" readonly="readonly" style="background:#CCC; border:0px; width:80%;" /></td></tr>
<tr><td>Transaction Date:</td><td><input type="text" name="trans_date" value="" id="trans_date" class="easyui-datebox" data-options="formatter:myformatter3,parser:myparser" style="width:100%;" required /></td><td><a href="#" class="btn btn-primary" onclick="otherCurrency()" ><i class="icon-share"></i>Other Currency</a></td><td></td></tr>
<tr><td>Trans Log:</td><td><input type="text" name="log_id" id="log_id" value="" readonly="readonly" style="background:#CCC; border:0px; width:80%;" /></td><td>&nbsp;&nbsp;</td><td></td></tr>
<tr><td>Payee:</td><td><input type="text" name="trans_payee" value="" id="trans_payee" style="width:80%;" required /></td><td>Details:</td><td><input type="text" name="trans_detail" value="" id="trans_detail" style="width:80%;" required /></td></tr>
<tr><td>Amount:</td><td><input type="text" name="trans_amount" value="" id="trans_amount1" class="easyui-numberbox" style="width:80%;" required /></td><td>Payment Method</td><td><select name="payment_method" id="payment_method" class="easyui-combobox" style="width:100%;" ><option value="cash">Cash</option><option value="cheque">Cheque</option><option value="ETF">Electron Transfer</option></select></td></tr>
<tr><td>Voucher No:</td><td><input type="text" name="vtr_no" value="" id="vtr_no" style="width:80%;" /></td><td>Ref No:</td><td><input type="text" name="cheque_no" value="" id="cheque_no" style="width:80%;" /></td></tr>
<tr><td>Paying Account:</td><td><input type="hidden" name="header_account" value="" id="header_account" /><input type="text" name="account_n" value="" id="account_n" readonly="readonly" style="width:80%;" /></td><td></td><td></td></tr>
</table>
</form>

<!--Editable datagrid--><div style="padding-left:5px; padding-right:5px;" >
<?php
require('debitAccount.php');
?>

</div>
</div>
 <div id="button_bar">
<a href="#" class="btn btn-success" onclick="getTransactionTotal();saveTransaction()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgPaymentTransaction').dialog('close')"><i class="icon-remove"></i>Cancel</a>
</div>
