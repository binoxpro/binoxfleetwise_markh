<script type="text/javascript">
var searchn=-1;
var expandn=-1;
var acode=null;
function viewSearch(){
	var d1=$('#startdateCb').datebox('getValue');
	var e1=$('#enddateCb').datebox('getValue');
	searchn=0;
	
	$('#dgCashReport').datagrid('load',{startDate:d1,endDate:e1});
}

function viewSearchCompany(){
	var d1=$('#startdateCb').datebox('getValue');
	var e1=$('#enddateCb').datebox('getValue');
	var row=$('#dgCashReport').datagrid('getSelected');
	if(row){
	searchn=1;
	acode=row.accountCode;
	$('#dgCashReport').datagrid('load',{startDate:d1,endDate:e1,accountCode:row.accountCode});
	}else{
		$.messager.show({title:'Info',msg:'Select An Account to expand transaction'});
	}
}
function exportFile(){
	if(searchn==-1){
		$.post('controller/transactionController.php?action=viewCashBookReport',{a:'1'},function(res){
			JSONToCSVConvertor(res,'CashBook',true);
		});
	}else if(searchn==0)
	{
		var d1=$('#startdateCb').datebox('getValue');
		var e1=$('#enddateCb').datebox('getValue');
		$.post('controller/transactionController.php?action=viewCashBookReport',{startDate:d1,endDate:e1},function(res){
			JSONToCSVConvertor(res,'CashBook',true);
		});
	}else if(searchn==1)
	{
		var d1=$('#startdateCb').datebox('getValue');
		var e1=$('#enddateCb').datebox('getValue');
		//var row=$('#dgCashReport').datagrid('getSelected');
	if(acode!=null){
		$.post('controller/transactionController.php?action=viewCashBookReport',{startDate:d1,endDate:e1,accountCode:acode},function(res){
			JSONToCSVConvertor(res,'CashBook',true);
		});	
	}
	}
}
</script>
<div class="col-lg-12">
<table id="dgCashReport" title="Cash Book Report" class="easyui-datagrid" style="height:600px;" url="controller/transactionController.php?action=viewCashBookReport" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarCashBook" showFooter="true" iconCls="fa fa-file-pdf-o fa-fw">
<thead>
<tr>
	<th field="id" width="50">Record No</th>
    <th field="datex" width="120">Date</th>
    <th field="details" width="250">Details</th>
    <th field="voucherNo" width="100">Ref</th>
    <th field="Dr" width="90">Dr</th>
	<th field="Cr" width="120" >Cr</th> 
    <th field="Bal" width="120" >Balance</th> 
</tr>
</thead>

</table>
<div id="toolbarCashBook">
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdateCb" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddateCb" />
<a href="#" class="btn btn-primary" onclick="viewSearch()" ><i class="fa fa-search"></i>Search</a>&nbsp;&nbsp;

<a href="#" class="btn btn-link" onclick=" viewSearchCompany()" ><i class="fa fa-table"></i>View Detail</a>
<a href="#" class="btn btn-link" onclick="exportFile()" ><i class="fa fa-file-pdf-o"></i>Export</a>

</div>
</div>
<?php

?>