<table id="dgCreditTransaction" class="easyui-edatagrid" title="Credit Transaction" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbCreditTransaction',
				method: 'get'
			">
		<thead>
			<tr>
				<th data-options="field:'trans_date',width:120, editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}">Trans Date</th>
				<th data-options="field:'account_code',width:100,
						formatter:function(value,row){
							return row.account_name;
						},
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'account_code',textField:'account_name',url:'controller/accountController.php?action=view',onSelect:function(record){
                var account_code=$(this).combobox('getValue');
               /* alert(account_code);*/
                $.post('controller/accountController.php?action=account_status',{account_code:account_code},function(response){
               /* alert(response);*/
               $('#analysis_status1').val(response); 
                }
                );
                }
                            }
						}">Account Name</th>
				<th data-options="field:'account_detail',width:140,align:'right',editor:'text'">Detail</th>
				<th data-options="field:'account_amount',width:100,align:'right',editor:'numberbox'">Amount</th>
				<th data-options="field:'trans_type',width:80,editor:{type:'combobox',options:{panelWidth:80,panelHeight:'auto',valueField:'trans_type',textField:'trans_value',url:'view/transaction/trans_type.json'
                            }}">Type</th>
                <th data-options="field:'subaccount',width:80,editor:{type:'combobox',options:{panelWidth:250,valueField:'analysisCode',textField:'analysisName',url:'controller/analysisCodeController.php?action=view_code',onSelect:function(record){
                var analysis=$('#analysis_status1').val();
                
                if(analysis=='N'){
               	 $(this).combobox('setValue','');
               	 $.messager.show({title:'info',msg:'Debit Account Subaccount Not required'});
                
               	 }else if(analysis=='Y'){
                
                	}
                }
                }
           }">Sub Account</th>
			</tr>
		</thead>
	</table>

	<div id="tbCreditTransaction" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-primary" onclick="javascript:$('#dgCreditTransaction').edatagrid('addRow')"><i class="icon-plus-sign"></i>New Item</a>
		
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgCreditTransaction').edatagrid('saveRow');reloadDataGrid2()"><i class="icon-ok-sign"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgCreditTransaction').edatagrid('destroyRow')"><i class="icon-minus-sign"></i>Remove</a>
        <br />
        <span><input type="hidden" name="analysis_status" id="analysis_status1" value=""  /><input type="hidden" name="totaly" id="totaly" value=""  /></span>
	</div>
	
	<script type="text/javascript">
	function prepare_datagrid2(){
			//var guest_id=$('#guest_id').val();
			//var total_new=$('#total').val();
			get_log_id2();
		/*
			$('#dgDebitTransaction').edatagrid({
				url: 'controller/transactionController.php?action=view_temp_transaction_data&log='+id,
				saveUrl: 'controller/transactionController.php?action=add&log='+id,
				updateUrl: 'controller/transactionController.php?action=update&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy&log='+id
			});
			*/
		}
		function getTransactionTotal2()
		{
				//var totalx=0;
				var logId=$('#log_id1').val();
				$.post('controller/transactionController.php',{action:'getTransactionTotal',logId:logId},function(response){
					$('#totaly').val(response);
					
					
					//alert(total);
				});
				//return total;
		}
		function get_log_id2(){
		
		$.post('controller/transactionController.php',{action:'addLog'},function(response){
			//alert(response);
			$('#log_id1').val(response);
			var id=response;
			$('#dgCreditTransaction').edatagrid({
				url:'controller/transactionController.php?action=view_temp&log='+id,
				saveUrl: 'controller/transactionController.php?action=add_temp&log='+id,
				updateUrl: 'controller/transactionController.php?action=update_temp&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy_temp&log='+id,
				onAdd:function(index,row){
					set_valueToGrid2(index);
				},
				onEdit:function(index,row){
					
				}
			});
			
		});
		
	}
	//set the intial values of the grid
	function set_valueToGrid2(rowIndex){
		var editors=$('#dgCreditTransaction').edatagrid('getEditors',rowIndex);
		
		var trans_date=editors[0];
		var accountName=editors[1];
		var details=editors[2]
		var amount=editors[3];
		var type=editors[4];
		$(trans_date.target).datebox('setValue',$('#trans_date').datebox('getValue'));
		//alert("S:"+$('#trans_date').datebox('getValue'));
		
		
	}
	//get finanical year and month
	function getFincnicalYear2(){
		$.post('controller/finanicalController.php?action=getYear',{},function(data){
			
		});
	}
	function reloadDataGrid2(){
		$('#dgCreditTransaction').datagrid('reload');
	}
	function getAnalysisCode2($code){
		
	}
    </script>