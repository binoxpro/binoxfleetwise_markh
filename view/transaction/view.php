<script type="text/javascript">

</script>
<div class="col-lg-12">
<table id="dgTransaction" title="Cash Book Payment" class="easyui-datagrid" style="height:500px;" url="controller/accountController.php?action=view_cash_book_payment" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarTransaction" iconCls="fa fa-file-pdf-o fa-fw">
<thead>
<tr>
	<th field="account_code" width="90">Account Code</th>
    <th field="account_name" width="90">Account Name</th>
    <th field="account_type" width="90">Account Type</th>
    <th field="balance2" width="90" data-options="formatter:function(index,row){
    if(row.balance2==null){
    return 0.00;
    }else{
    return row.balance2;
    }
    
    }">Current Balance</th>
</tr>
</thead>

</table>
<div id="toolbarTransaction">
<a href="#" class="btn btn-primary" onclick="newTransaction()" ><i class="icon-circle-arrow-up"></i>Add Payment</a>
<a href="#" class="btn btn-link" onclick="newCashBookRecepit()" ><i class="icon-circle-arrow-down"></i>Add Recepit</a>
<!--<a href="#" class="btn btn-link" onclick="" >Other</a>-->
<!--<a href="#" class="easyui-linkbutton" iconCls="icon-print" plain="true" onclick="" >Other</a>-->
<!--<a href="#" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="" >More</a>-->
</div>
</div>
<?php
include_once('add.php');
include_once('addReceipt.php');
?>
<script type="text/javascript">
/*$(function(){	
		$('#dgTransaction').datagrid({
			rowStyler:function(index,row){
				if(parseFloat(row.balance2)<=0.00 || row.balance2==null){
					return 'background-color:#FFD5D5;';
				}else{
					
					return 'background-color:#CAE4FF;';
				}
			},
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					rowStyler:function(index,row){
				if(row.account_type=='ACA'){
					return 'background:#CAE4FF;';
				}else{
					
				}
			},
					url:'controller/transactionController.php?action=view_trans&cash_account='+row.account_code,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'',
					height:'auto',
					columns:[[{field:'header_id',title:'Trans No:',width:50},{field:'trans_date',title:'Date:',width:70},{field:'account_code',title:'Account Code',width:90},{field:'account_name',title:'Account Name',width:90},{field:'trans_detail',title:'Description',width:90},{field:'type2',title:'Credit',width:90,formatter:function(val,row){
				
			if(val=='C'){
				return row.trans_amount;
			}else{
				return '';
			}
			
			}},{field:'trans_type',title:'Debit',width:90,formatter:function(val,row){
				
			if(val=='D'){
				return row.trans_amount;
			}else{
				return '';
			}
			
			}}]],
					onResize:function(){
						$('#dgTransaction').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgTransaction').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgTransaction').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						//$('#dgTransaction').datagrid('expandRow',0);	
						}
		});
	});
	*/
	
	$(function(){	
		$('#dgTransaction').datagrid({
			rowStyler:function(index,row){
				if(parseFloat(row.balance2)<=0.00 || row.balance2==null){
					return 'background-color:#FFD5D5;';
				}else{
					
					return 'background-color:#CAE4FF;';
				}
			},
			view:detailview,
			detailFormatter:function(index,row){
				return '<div style="width:650px;padding:2px"><table class="ddv"></table></div>';
			},onExpandRow:function(index,row){
				var ddv=$(this).datagrid('getRowDetail',index).find('table.ddv');
				ddv.datagrid({
					rowStyler:function(index,row){
				if(row.account_type=='ACA'){
					return 'background:#CAE4FF;';
				}else{
					
				}
			},
					url:'controller/transactionController.php?action=view_trans&cash_account='+row.account_code,
					fitColumns:true,
					singleSelect:true,
					rownumber:true,
					loadMsg:'',
					height:'auto',
					columns:[[{field:'header_id',title:'Trans No:',width:50},{field:'trans_date',title:'Date:',width:70},{field:'account_code',title:'Account Code',width:90},{field:'account_name',title:'Account Name',width:90},{field:'trans_detail',title:'Description',width:90},{field:'type2',title:'Credit',width:90,formatter:function(val,row){
				
			if(val=='C'){
				return row.trans_amount;
			}else{
				return '';
			}
			
			}},{field:'trans_type',title:'Debit',width:90,formatter:function(val,row){
				
			if(val=='D'){
				return row.trans_amount;
			}else{
				return '';
			}
			
			}}]],
					onResize:function(){
						$('#dgTransaction').datagrid('fixDetailRowHeight',index);
					},
					onLoadSuccess:function(){
						//alert(index);
						setTimeout(function(){
							$('#dgTransaction').datagrid('fixDetailRowHeight',index);
							//alert(index);
							
						},0);
						}
					
					});
				$('#dgTransaction').datagrid('fixDetailRowHeight',index);
				
				//$('#dg').datagrid('collapseRow',1);
			},onLoadSuccess:function(){
						//$('#dgTransaction').datagrid('expandRow',0);	
						}
		});
	});

	
	
	function newTransaction(){
		
		var row=$('#dgTransaction').datagrid('getSelected');
		if(row){
			$('#dlgPaymentTransaction').dialog('open').dialog('setTitle','Cash Book Entry Screen');
			
			$('#frmPaymentTransaction').form('clear');
			//var id=get_log_id();
			//alert(id);
		//	get_log_id();
			getOpenYearAndMonth()
			prepare_datagrid();
			$('#header_account').val(row.account_code);
			$('#account_n').val(row.account_name);
			url='controller/transactionController.php?action=add'
			
		}else{
			$.messager.show({title:'Help',msg:'Please seect the cash account to credit For more help call:0781587081'});
		}
	}
	function saveTransaction(){
		var totalDiff=parseInt($('#totalz').val())-parseInt($('#trans_amount1').val());
		if(totalDiff==0){
		$.messager.progress();
	 $('#frmPaymentTransaction').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Academic Year Saved'});
						$('#dlgPaymentTransaction').dialog('close'); // close the dialog
						 $('#dgTransaction').datagrid('reload'); // reload the user
		}
		
	}
}); 
		
		}else{
			$.messager.show({title:'Help',msg:'Dedits should equal to credit'});
		}
	}
	function getOpenYearAndMonth(){
		$.post('controller/finanicalController.php',{action:'getYear'},function(data){
			//$.messager.alert(data);
			var newstr=data.split('*');
			$('#year_code').val(newstr[0]);
			$('#month_code').val(newstr[1]);
		});
	}
	function getOpenYearAndMonth2(){
		$.post('controller/finanicalController.php',{action:'getYear'},function(data){
			//alert(data);
			var newstr=data.split('*');
			$('#year_code1').val(newstr[0]);
			$('#month_code1').val(newstr[1]);
		});
	}
	function newCashBookRecepit(){
		
		var row=$('#dgTransaction').datagrid('getSelected');
		if(row){
			$('#dlgPaymentTransactionRecepit').dialog('open').dialog('setTitle','Cash Book Recepit Entry Screen');
			
			$('#frmPaymentTransactionRecepit').form('clear');
			//var id=get_log_id();
			//alert(id);
		//	get_log_id();
			getOpenYearAndMonth2()
			prepare_datagrid2();
			$('#header_account1').val(row.account_code);
			$('#account_n1').val(row.account_name);
			url='controller/transactionController.php?action=addRecepit'
			
		}else{
			$.messager.show({title:'Help',msg:'Please seect the cash account to credit For more help call:0781587081'});
		}
	
	}
	function saveRecepit(){
		//loadingBar("Saving information");
		var totalDiff=parseInt($('#totaly').val())-parseInt($('#trans_amount').val());
		if(totalDiff==0){
	 $('#frmPaymentTransactionRecepit').form('submit',{ url: url, 
	 onSubmit: function(){
		 return $(this).form('validate');
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Transaction Completed Successfully'});
						$('#dlgPaymentTransactionRecepit').dialog('close'); // close the dialog
						 $('#dgTransaction').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		}else
	{
		$.messager.show({title: 'Info',
					  msg: 'Complete the transaction'});
	}
	}
</script>