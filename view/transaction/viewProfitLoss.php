<script type="text/javascript">
function viewProfitLoss(){
	var d1=$('#startdate').datebox('getValue');
	var e1=$('#enddate').datebox('getValue');
	$('#dgProfitLoss').datagrid('load',{startDate:d1,endDate:e1});
}

</script>
<div class="col-lg-12">
<table id="dgProfitLoss" title="Profit and Loss Report" class="easyui-datagrid" style="height:700px;" url="controller/transactionController.php?action=viewProfitAndLoss" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarPL" showFooter="true" iconCls="fa fa-file-pdf-o fa-fw">
<thead>
<tr>
	<!--<th field="id" width="50">Record No</th>-->
    <th field="accountCode" width="120">Code</th>
    <th field="details" width="150">AccountName</th>
    <th field="Dr" width="90">Amount</th>
	<!--<th field="Cr" width="120" >Cr</th> -->
    <!--<th field="Bal" width="120" >Balance</th>-->
</tr>
</thead>

</table>
<div id="toolbarPL">
Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdate" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddate" />
<a href="#" class="btn btn-success btn-sm" onclick="viewProfitLoss()" ><i class="fa fa-search"></i>Search</a>

<a href="#" class="btn btn-link" onclick="" ><i class="fa fa-file-excel-o"></i>Export</a>
<a href="#" class="btn btn-link" onclick="" ><i class="fa fa-file-pdf-o"></i>Print</a>


</div>
</div>
<?php

?>