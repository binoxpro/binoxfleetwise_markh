    <table id="dgTrailer" title="Manage Trailer" class="easyui-datagrid" style="height:700px;" url="controller/trailerController.php?action=view" pagination="true" toolbar="#toolbarTrailer" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
            <th field="tractorHead" width="100">TractorHeader</th>
            <th field="trailerNo" width="100">Trailer</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarTrailer" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newTrailer()"><i class="fa fa-plus-circle"></i>Assign Trailer</a>
        <!--<a href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i>Edit Trailer</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Trailer</a>-->
    </div>
    <?php include_once('add.php'); ?>
    
