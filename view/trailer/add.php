<div id="dlgTrailer" class="easyui-dialog" closed='true' style="width:500px;" toolbar='#Trailerbutton_bar' modal="true" >
<form id="frmTrailer" name="frmTrailer" method="post">
<div class="col-lg-6" >
    <div class="form-group">
    <label>TractorHeader:</label>
    <select name="tractorheadId" id="tractorheadId" class="easyui-combobox" style="height:30px; width:100%;" data-options="url:'controller/vehicleController.php?action=view&type=',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
    </div>
    <div class="form-group">
        <label>Driver Name:</label>
        <input type="text" class="form-control" id="driverName"  name="driverName" readonly/>
    </div>
    <div class="form-group">
        <label>Driver's Contact:</label>
        <input type="text" class="form-control" id="driverContacts"  name="driverContacts" readonly/>
    </div>
    <div class="form-group">
    <label>Trailer:</label>
    <select name="trailerId" id="trailerId" class="easyui-combobox" style="height:30px; width:100%;" data-options="url:'controller/vehicleController.php?action=view&type=',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
    </div>

</div>
</form>
</div>
<div id="Trailerbutton_bar">
<a href="#" class="btn btn-primary" onclick="saveTrailer()">Save</a>
<a href="#" class="btn btn-primary">Close</a>
</div>

