<div id="dlgtireRepair" class="easyui-dialog" closed="true" style="width:80%;max-width:800px;padding:10px;height:400px; " toolbar="#tireRepairbutton" modal="true" >
<form id="frmtireRepair" name="frmtire" method="post">

<table id="dgActualInvoice" class="easyui-datagrid" title="Tire Maintanence Cost" style="height:auto; border-right:1px solid #66B3FF;" pagination="true" rownumbers="true"
			data-options="
				singleSelect: true,
				toolbar: '#tbActualInvoice',
				method: 'get',
                showFooter:true,
                fitColumns:true,
                iconCls:'fa fa-paper-plane'
			">
		<thead>
			<tr>
				<th data-options="field:'details',width:120, editor:{type:'text'}">Details</th>
				<th data-options="field:'cost',width:70,
						editor:{
							type:'numberbox',options:{precision:0,groupSeparator:','}
						}">Cost(USH)</th>
                <th data-options="field:'creationDate',width:100,editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}">Date</th>
			</tr>
		</thead>
	</table>

	<div id="tbActualInvoice" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgActualInvoice').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>New Item</a>&nbsp;&nbsp;
		
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgActualInvoice').edatagrid('saveRow');reloadDataGrid();reloadDataGrid();reloadSummary2()"><i class="fa fa-save"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="deleteItem();reloadSummary2();reloadDataGrid()"><i class="fa fa-times"></i>Remove</a>
        
	</div>
    </form>
 </div>
 <div id="tireRepairbutton">
&nbsp;&nbsp;<a href="#" class="btn btn-primary" onclick="repairedtire()"><i class="fa fa-times"></i>Repair Done</a>
</div>
 
    
	
	
	