<table id="dgActualExpense" class="easyui-datagrid" title="Expenses" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbActualExpense',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'idno',width:50" hidden="true">Expense No</th>
				<th data-options="field:'details',width:120, editor:{type:'text'}">Expense</th>
				<th data-options="field:'amount',width:100,
						editor:{
							type:'numberbox'
						}">Amount</th>
			</tr>
		</thead>
	</table>

	<div id="tbActualExpense" style="height:auto">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="javascript:$('#dgActualExpense').edatagrid('addRow')">Append</a>
		
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="javascript:$('#dgActualExpense').edatagrid('saveRow');reloadDataGrid2();reloadSummary()">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="deleteExpense();reloadSummary()">Remove</a>
	</div>
	
	<script type="text/javascript">

	function prepare_datagrid2(){
			var id=$('#invoiceNo').val();
			var jobid=$('#jobid').val();
			
			
		
			$('#dgActualExpense').edatagrid({
				method:'get',
				url: 'controller/invoiceController.php?action=view_expense_data&Expense_no='+id+"&jobid="+jobid,
				saveUrl: 'controller/invoiceController.php?action=addExpense&Expense_no='+id+"&jobid="+jobid,
				updateUrl: 'controller/invoiceController.php?action=updateExpense&Expense_no='+id+"&jobid="+jobid
			});
			
		}
		
	function reloadDataGrid2(){
		$('#dgActualExpense').edatagrid('reload');
	}
	
	
	function deleteExpense(){
		var row=$('#dgActualExpense').edatagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this Expense',function(r){
			if(r){
				$.post('controller/invoiceController.php',{id:row.idno,action:'deleteExpenseItem'},function(result){
			if(result.success){
				$('#dgActualExpense').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	
    </script>