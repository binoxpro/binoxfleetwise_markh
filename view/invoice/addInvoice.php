<table id="dgActualInvoice" class="easyui-datagrid" title="Proform Invoice" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				toolbar: '#tbActualInvoice',
				method: 'get',
                showFooter:true
			">
		<thead>
			<tr>
            	<th data-options="field:'idno',width:50" hidden="true">Item No</th>
				<th data-options="field:'item',width:120, editor:{type:'text'}">Item</th>
				<th data-options="field:'qty',width:100,
						editor:{
							type:'numberbox'
						}">Quantity</th>
				<th data-options="field:'rate',width:140,align:'right',editor:'numberbox'">Rate</th>
				<th data-options="field:'noday',width:100,align:'right',editor:'numberbox'">No Days</th>
				<th data-options="field:'total',width:100,editor:{type:'numberbox'}">Total</th>
                <th data-options="field:'datex',width:100,editor:{type:'text'}" hidden="true">Date</th>
			</tr>
		</thead>
	</table>

	<div id="tbActualInvoice" style="height:auto">
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:false" onclick="javascript:$('#dgActualInvoice').edatagrid('addRow')">Append</a>
		
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="javascript:$('#dgActualInvoice').edatagrid('saveRow');reloadDataGrid();reloadSummary2()">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="deleteItem();reloadSummary2()">Remove</a>
	</div>
	
	<script type="text/javascript">

	function prepare_datagrid(){
			var id=$('#invoiceNo').val();
			var jobid=$('#jobid').val();
			
			
		
			$('#dgActualInvoice').edatagrid({
				method:'get',
				url: 'controller/invoiceController.php?action=view_invoice_data&invoice_no='+id+"&jobid="+jobid,
				saveUrl: 'controller/invoiceController.php?action=addInvoice&invoice_no='+id+"&jobid="+jobid,
				updateUrl: 'controller/invoiceController.php?action=updateInvoice&invoice_no='+id+"&jobid="+jobid,
				onAdd:function(index,row){
					var date_var=$('#datex').datebox('getValue');
					if(date_var!=null && date_var!=''){
					set_values(index)
					}else{
					$.messager.alert('DVI System','Supply the invoice date');	
					$('#dgActualInvoice').edatagrid('reload');
					}
				},
				onEdit:function(index,row){
					calcu(index)
				}
			});
			
		}
		function get_log_id(){
		/*
		$.post('controller/transactionController.php',{action:'addLog'},function(response){
			//alert(response);
			$('#log_id').val(response);
			var id=response;
			$('#dgActualInvoice').edatagrid({
				url:'controller/transactionController.php?action=view_temp&log='+id,
				saveUrl: 'controller/transactionController.php?action=add_temp&log='+id,
				updateUrl: 'controller/transactionController.php?action=update_temp&log='+id,
				destroyUrl: 'controller/transactionController.php?action=destroy_temp&log='+id
			});
			
		});
		*/
		
	}
	function reloadDataGrid(){
		$('#dgActualInvoice').edatagrid('reload');
	}
	function getAnalysisCode($code){
		
	}
	function calcu(rowIndex){
		
		var editors=$('#dgActualInvoice').edatagrid('getEditors',rowIndex);
		var datex=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		qty.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		rates.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		noday.target.bind('change',function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	
	}
	function set_values(rowIndex){
		var editors=$('#dgActualInvoice').edatagrid('getEditors',rowIndex);
		var datex=editors[5];
		var items=editors[0];
		var qty=editors[1];
		var noday=editors[3]
		var rates=editors[2];
		var total=editors[4];
		items.target.blur(function(){
			var date_val=$('#datex').datebox('getValue');
			
		$(datex.target).val(date_val);
			
		});
		noday.target.blur(function(){
			
			var qty1=qty.target.val();
			var rate1=rates.target.val();
			var noday1=noday.target.val();
			var totalcost=qty1*rate1*noday1;
			//alert('hi');
			//alert(totalcost);
		$(total.target).numberbox('setValue',totalcost);
		
			
		});
		
		
	}
	function deleteItem(){
		var row=$('#dgActualInvoice').edatagrid('getSelected');
		if(row){
			
		$.messager.confirm('Delete Alert','Are you sure you would like to delete this item',function(r){
			if(r){
				$.post('controller/invoiceController.php',{id:row.idno,action:'deleteInvoiceItem'},function(result){
			if(result.success){
				$('#dgActualInvoice').datagrid('reload'); 
			}else{
				$.messager.show({title:'Debug',msg:result.msg});
			}
				},'json');
				
			}
		});
	
			
		}else{
			$.messager.show({title:'Alert',msg:'Please select an item to delete'});
		}
	}
	function reloadSummary2(){
	var id=$('#jobid').val();
	$('#dgSummary').datagrid('reload',{jobid:id});	
}
    </script>