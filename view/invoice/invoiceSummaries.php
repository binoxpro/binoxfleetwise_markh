<table id="dgSummary" title="Invoice Summary Values" class="easyui-datagrid" style="height:auto;" url="controller/invoiceController.php?action=view_summary" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" >
<thead>
<tr>
    <th field="summary" width="90" >Summaries</th>
    <th field="amount" width="90">Total</th>
</tr>
</thead>

</table>
<script type="text/javascript">
function loadSummary(){
	var id=$('#jobid').val();
	$('#dgSummary').datagrid('load',{jobid:id});
}
function reloadSummary(){
	var id=$('#jobid').val();
	$('#dgSummary').datagrid('reload',{jobid:id});	
}
</script>