<script type="text/javascript">
function viewSearch(){
	
	var d1=$('#startdatev').datebox('getValue');
	var e1=$('#enddatev').datebox('getValue');
	var p1=$('#clientNamev').combobox('getValue');
	if(p1==null || p1==''){
		//alert('not');
	$('#dgInvoiceVat').datagrid('load',{startDate:d1,endDate:e1});
	}else{
		//alert('Ok:'+p1);
	$('#dgInvoiceVat').datagrid('load',{startDate:d1,endDate:e1,client:p1});
	}
}
</script>
<div class="col-lg-12">
<table id="dgInvoiceVat" title="Invoice Reports" class="easyui-datagrid" style="height:600px;" url="controller/budgetController.php?action=view_report_invoice_vat" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarInvoiceVat">
<thead>
<tr>
    <th field="jobid" width="90" data-options="formatter:function(val,row){
    if(row.jobID!=''||row.invoiceNo!=''){
        return row.jobID+'-'+row.invoiceNo;
        }
        }">Invoice No:</th>
    <th field="attn" width="90">To:</th>
    <th field="clientName" width="90">CompanyName</th>
    <th field="income" width="90">Total</th>
    <th field="vat" width="90">VAT</th>
    <th field="invoiceDate" width="90">Date</th>
	
</tr>
</thead>

</table>
<div id="toolbarInvoiceVat">

<span>Client Name:<select name="clientNamev" id="clientNamev" class="easyui-combobox" style="height:auto;" data-options="url:'controller/analysisCodeController.php?action=viewCategory&id=Dedit',valueField:'analysisName',textField:'analysisName',panelWidth:'120',panelHeight:'auto'" ></select> Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatev" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatev"  /></span>
<a href="#" class="btn btn-link" onclick="viewSearch()" ><i class="icon-search"></i>Search</a>
</div>
</div>
<?php

?>
<script type="text/javascript">
    
</script>