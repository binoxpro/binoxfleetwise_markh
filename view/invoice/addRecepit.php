<div id="dlgRecepit" class="easyui-dialog" style="height:350px; width:400px; padding:5px;" closed='true' buttons='#button_barw'>
<div class="easyui-panel" title="Payment Form">
<form id="frmRecepit" name="frmRecepit" method="post">
<table>
<tr><td>JobID:</td><td><input type="text" name="jobid4" value="" id="jobid4" readonly /></td></tr>

<tr><td>Balance Before:</td><td><input type="text" name="balancebefore" value=""  id='balancebefore' class="easyui-numberbox" required="true"/><input type="hidden" name="analcode" value="" id="analcode" readonly /><input type="hidden" name="credit" value="" id="credit" readonly /></td></tr>
<tr><td>Amount:</td><td><input type="text" name="amount" value="" class="easyui-numberbox" data-option="min:1,max:1000000000" required="true" onblur="showBal()" id="amount"/></td></tr>
<tr><td>Balance:</td><td><input type="text" name="bal" value="" onfocus="showBal()" id="bal"  readonly="readonly" /></td></tr>
<tr><td>Date:</td><td><input type="text" name="datex" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" /></td></tr>

<tr><td>Apply With Holding Tax</td><td><input type="radio" name="wht" value="y" onclick="displayTax()" />Yes:<input type="radio" name="wht" value="n" onclick="displayTax()" />No:</td></tr>
<tr id="fillTax"></tr>
<tr><td>Receving Account</td><td><select name="debitAccount" style="width:200px;" class="easyui-combogrid" data-options="panelWidth:250,idField:'ACODE',textField:'ANAME',url:'controller/accountController.php?action=view_cash_book_payment',columns:[[{field:'ACODE',title:'CODE',width:50},{field:'ANAME',title:'ACCOUNT',width:120},{field:'CBAL',title:'Current Balance',width:120}]]"></select></td></tr>

</table>
</form>
</div>
</div>
 <div id="button_barw">
<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecepit()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgRecepit').dialog('close')">Cancel</a>
</div>
<script type="text/javascript">
function showBal(){
	var balb=$('#balancebefore').numberbox('getValue');
	var x=$('#amount').numberbox('getValue')
	var total=parseInt(balb)-parseInt(x);
	$('#bal').val(total);
	
}
function displayTax(){
	
	var taxtest;
	var tax=document.getElementsByName("wht");
	//alert (tax.length);
	for(var i=0;i<tax.length;i++){
		if(tax.item(i).checked==true){
			taxtest=tax.item(i).value;
			if(taxtest=="y"){
				
				document.getElementById("fillTax").innerHTML="";
				var show="<td>WHT:&nbsp;&nbsp;</td><td><input type='hidden' name='whtaxes' value='280' readonly/>&nbsp;&nbsp;<input type='' name='whtamount' value='' id='whtamt' readonly/></td>";
				
				document.getElementById("fillTax").innerHTML=show;
				document.getElementById("whtamt").value=getTax();//this function is below
			}else{
				document.getElementById("fillTax").innerHTML="";
				var show="<td colspan='2'><p>WHT Not applied</p></td>";
				document.getElementById("fillTax").innerHTML=show;
			}
		}else{
		}
		
	}
	

}
function getTax(){
	var amount=parseInt($('#amount').numberbox('getValue'));
	var wht=0.06;
	var total=0;
	
	total=amount* wht;
	return total;
}
function saveRecepit(){
		
		
		$.messager.progress();
	 $('#frmRecepit').form('submit',{ url: url, 
	 onSubmit: function(){
		 $.messager.progress('close');
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress();
			 var result = eval('('+result+')');
				 if (result.msg){
					 $.messager.progress('close');
					  $.messager.show({title: 'Info',
					  msg: result.msg});
					} else {
						$.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Thank for payment us'});
						$('#dlgRecepit').dialog('close'); // close the dialog
						$('#dgInvoice').datagrid('reload');// reload the user
						$('#payoff').linkbutton('disable');
						$.messager.progress('close');
		}
		
		
	}
}); 
		

	}
</script>