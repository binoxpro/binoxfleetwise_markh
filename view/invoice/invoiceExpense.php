<div id="dlgInvoiceExpense" class="easyui-dialog" style="height:500px; width:800px; padding-right:5px; background:#FFF; " closed='true' buttons='#button_bar_expense'>
<div></div>
<table id="dgActualExpense2" class="easyui-datagrid" title="Expenses" style="height:auto; border-right:1px solid #6AB5FF;" pagination="true" rownumbers="true"
			data-options="
				iconCls: 'icon-sum',
				singleSelect: true,
				toolbar: '#tbActualExpense2',
                showFooter:true,
                fitColumns:true,
                url: 'controller/invoiceController.php?action=view_expense_data'
			">
		<thead>
			<tr>
            	<th data-options="field:'idno',width:50" hidden="true">Expense No</th>
				<th data-options="field:'details',width:120">Expense</th>
				<th data-options="field:'amount',width:100">Amount</th>
			</tr>
		</thead>
	</table>
<div id="tbActualExpense2"></div>

</div>
<div id="button_bar_expense">
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgInvoiceExpense').dialog('close')">Close</a>
</div>
<?php
//require_once('addRecepit.php');
?>
<script type="text/javascript">


</script>
