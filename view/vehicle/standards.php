    <table id="dgStandard" title="Manage Vehicle Standard" class="easyui-datagrid" style="height:700px;" url="controller/standardController.php?action=view" pagination="true" toolbar="#toolbarVehicleStandard" rownumbers="true"  singleSelect="true" iconCls="fa fa-car">
    	<thead data-options="frozen:true">
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="regNo" width="120">Registration Number</th>
         </tr>
         </thead>
         <thead>
         <tr>
            <th field="abs" width="150" data-options="formatter:formatFitted">ABS</th>
            <th field="bmc" width="200" data-options="formatter:formatComplies">24 Bolts man hole cover</th>
            <th field="obc" width="200" data-options="formatter:formatInstalled">OBC</th>
            <th field="sur" width="100" data-options="formatter:formatComplies">Side under run 500</th>
            <th field="rur" width="100" data-options="formatter:formatComplies">Rear Under run 500</th>
            <th field="fbsm" width="100" data-options="formatter:formatComplies">Front Blind Spot Mirror</th>
            <th field="sst" width="100" data-options="formatter:formatComplies">Spray Suppression Tires</th>
            <th field="sstt" width="100" data-options="formatter:formatComplies">Spray Suppression Trailer Tires</th>
            
            </tr>
        </thead> 
    </table>
    <div id="toolbarVehicleStandard" >
    	<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="newStandard()" ><i class="fa fa-plus-circle"></i>Add Standard</a>
        <a href="#" class="btn btn-primary btn-sm"  onclick="editStandard()"><i class="fa fa-pencil"></i>Edit Standard</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Vehicle</a>
    </div>
    <!-- Button trigger modal -->
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">New Standard for a Vehicle</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="col-lg-12" >
<form id="frmstandard" name="frmstandard" method="post" >
<div style="width:40%; margin-left:5%">
<div id="col-lg-6">
<div class="form-group">
    <label>Vehicle</label><br />
    <select name="vehicleId" id="vehicleId" class="easyui-combobox form-control" style="height:25px; width:230px; " data-options="url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
    </div>
    <div class="form-group">
    <label>ABS</label>
    <select class="form-control" name="abs" id="abs"><option value="Fitted">Fitted</option><option value="Not Fitted">Not Fitted</option></select>
    </div>
    <div class="form-group">
    <label>24 Bolt Man Hole Cover</label>
    <select class="form-control" name="bmc" id="bmc"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
    <div class="form-group">
    <label>OBC</label>
    <select class="form-control" name="obc" id="obc"><option value="Installed">Installed</option><option value="Not Installed">Not Installed</option></select>
    </div>
    <div class="form-group">
    <label>Side Underrun</label>
    <select class="form-control" name="sur" id="sur"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
     <div class="form-group">
    <label>Rear Underrun</label>
    <select class="form-control" name="rur" id="rur"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
    
    <div class="form-group">
    <label>Front Blind Spot Mirror</label>
    <select class="form-control" name="fbsm" id="fbsm"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
    
    <div class="form-group">
    <label>Spray Suppression Tire</label>
    <select class="form-control" name="sst" id="sst"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
    
    <div class="form-group">
    <label>Spray Suppression Trailer Tire</label>
    <select class="form-control" name="sstt" id="sstt"><option value="Complies">Complies</option><option value="Does not Comply">Does n't Comply</option></select>
    </div>
    
</div>
</div>    
</form>
</div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="#" class="btn btn-primary" onclick="saveStandard()">Save</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
    
