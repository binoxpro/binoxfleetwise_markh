    <table id="dgVehicle" title="Manage Vehicle" class="easyui-datagrid" style="height:700px;" url="controller/vehicleController.php?action=view" pagination="true" toolbar="#toolbarVehicle" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<th field="id" width="50">id</th>
            <th field="regNo" width="120">Registration Number</th>
            <th field="configuration" width="150">Configuration</th>
            <th field="model" width="90">Modal</th>
            <th field="axel" width="80">Axel</th>
            <th field="cardNumber" width="100">Card Number</th>
            <th field="countryMake" width="100">Country</th>
            <th field="type" width="100">Type</th>
            <th field="capacty" width="100">Capacity</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarVehicle" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newVehicle()"><i class="fa fa-plus-circle"></i>Add Vehicle</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="editVehicle()"><i class="fa fa-pencil"></i>Edit Vehicle</a>
        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-times-circle"></i>Delete Vehicle</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="assignDriver()"><i class="fa fa-times-circle"></i>Assign Driver</a>
    </div>
    <?php include_once('add.php'); ?>
    <div id="dlgAssignVehicle" class="easyui-dialog" closed='true' style="width:500px; padding:4px;" buttons='#Assignvehiclebutton_bar' modal="true">
            <form name="frmAssign" id="frmAssign" method="post">
            <div class="form-group">
            <label>Vehicle Registration Number:</label>
            <select name="regNo3" id="regNo3" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
            </div>
            <div class="form-group">
            <label>Driver Name</label>
            <select name="driverId3" id="driverId3" class="easyui-combobox form-control" style="height:30px; width:100%;" data-options="url:'controller/driverController.php?action=view',valueField:'id',textField:'firstName',panelWidth:'120',panelHeight:'auto'" ></select>
            </div>
            </form>
    </div>
    <div id="Assignvehiclebutton_bar">
    <a href="#" class="btn btn-primary" onclick="saveAssignVehicle()">Save</a>
    <a href="#" class="btn btn-primary">Close</a>
    </div>
    
