<div id="dlgVehicle" class="easyui-dialog" closed='true' style="width:800px;" toolbar='#vehiclebutton_bar' modal="true" >
<form id="frmVehicle" name="frmVehicle" method="post">
    <div class="col-lg-6">
        <div class="form-group" >
        <label>Registration Number:</label>
        <input class="form-control" name="regNo" id="regNo" value="" type="text"/>
        </div>
        <div class="form-group">
        <label>Configuration:</label>
        <input class="form-control" name="configuration" id="configuration" value="" type="text"/>
        </div>
        <div class="form-group">
        <label>Model:</label>
        <input class="form-control" name="model" id="model" value="" type="text"/>
        </div>
        <div class="form-group" >
        <label>Axel:</label>
        <input class="form-control" name="axel" id="axel" value="" type="text"/>
        </div>

    </div>
    <div class="col-lg-6">
        <div class="form-group" >
            <label>Card Number:</label>
            <input class="form-control" name="cardNumber" id="cardNumber" value="" type="text"/>
        </div>
        <div class="form-group" >
        <label>Country Make:</label>
        <input class="form-control" name="countryMake" id="countryMake" value="" type="text"/>
        </div>
        <div class="form-group" >
        <label>Capacity:</label>
        <input class="form-control" name="capacty" id="capacty" value="" type="text"/>
        </div>

        <div class="form-group" >
        <label>Vehicle Type Id:</label>
        <select name="vehicleTypeId" id="vehicleTypeId" class="easyui-combobox" style="height:30px; width:100%;" data-options="url:'controller/vehicleTypeController.php?action=view',valueField:'id',textField:'vehicleType',panelWidth:'120',panelHeight:'auto'" ></select>
        </select>
        </div>
    </div>
</form>
</div>
<div id="vehiclebutton_bar">
<a href="#" class="btn btn-primary" onclick="saveVehicle()">Save</a>
<a href="#" class="btn btn-primary">Close</a>
</div>

