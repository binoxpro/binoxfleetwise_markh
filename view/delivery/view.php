<div class="col-lg-12" style="margin-top:5px; margin-bottom:5px;">
    <table id="dgDelivery" title="Manage Deliveries" style="height:auto;" url="controller/deliveryController.php?action=view" pagination="true" toolbar="#toolbarDelivery" rownumbers="true" fitColumns="true" singleSelect="true" nowrap="false" iconCls="fa fa-table">
    	<thead>
        	<tr>
            <!--hidden="true"-->
            <th field="id" width="150" data-options="editor:{type:'text'}" hidden="true" >#</th>
            <th field="mailingName" width="150">Customer</th>
            <!--<th field="orderDate" width="100">Order Date</th>-->
            <th field="promiseDate" width="100">Load Date</th>
            <th field="ordertypeId" width="50">Order Type</th>
            <th field="orderNumber" width="100">Order No</th>
            <th field="qtyOrder" width="100">Qty</th>
            <th field="timein" width="100" data-options="editor:{type:'timespinner'}" >Depot In</th>
            <th field="timeout" width="100" data-options="editor:{type:'timespinner'}" >Dispatch Time</th>
            <th field="duration" width="100">Depot Duration</th>
            <th field="truck" width="100" >Truck No</th>
            <th field="deliveryDate" width="120" data-options="editor:{type:'datebox',options:{formatter:myformatter2,parser:myparser}}">Delivery Date</th>
            <th field="deliveryTime" width="100" data-options="editor:{type:'timespinner'}">Delivery Time</th>
            <th field="did" width="50" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="deid" width="50" data-options="editor:{type:'text'}" hidden="true">#</th>
            <th field="eid" width="50" data-options="editor:{type:'text'}" hidden="true" >#</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarDelivery">
    	<!--<a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgDispatcher').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>Add</a>-->
        <a href="#" class="btn btn-primary btn-sm" onclick="saveDelivery()"><i class="fa fa-pencil"></i>Save/Update</a>
        <a href="#" class="btn btn-primary btn-sm" onclick="javascript:$('#dgDelivery').edatagrid('deleteRow');refreshGird()"><i class="fa fa-times-circle"></i>Delete</a>
       <!-- <a href="#" class="btn btn-link" onclick="exportToExcel()">Excel File</a>-->
       <div>
        &nbsp;Starting<input type="text" name="startDate" value="" id="startDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />&nbsp;&nbsp;Ending<input type="text" name="endDate" value="" id="endDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:20%;height:25px;" />
            <a href="#" class="btn btn-primary btn-sm" onclick="viewSearchC()"><i class="fa fa-search"></i></a>
        </div>
    </div>
    </div>