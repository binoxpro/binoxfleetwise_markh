<div id="dlgOthercostManagement" class="easyui-dialog" closed='true' style="width:800px;" buttons='#OthercostManagementbutton_bar' modal="true" >
<form id="frmOthercostManagement" name="frmOthercostManagement" method="post">
<table style="width:90%; margin-top:10px; margin-left:5%; height:200px;">
<tr><td>
    <label>Vehicle:</label>
    </td><td>
    <select name="vehicleId" id="vehicleId" class="easyui-combobox" style="height:30px; width:250px;" data-options=       			   "url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select>
    </td></tr><tr><td>
    <label>Destination</label>
    </td><td>
    <textarea name="destination" id="destination" value="" class="form-control"  style="width:250px;" ></textarea>
    </td></tr>
    <tr><td>
    <label>Amount:</label>
    </td><td>
    <input name="amount" id="amount" value="" class="form-control" type="text" style="width:250px;"/>
    </td></tr>
    <tr><td>
    <label>Description:</label>
    </td><td>
    <textarea name="description" id="description" class="form-control" value=""  style="width:250px;" ></textarea>
    </td></tr>
    <tr><td>
    <label>Date:</label>
    </td><td>
    <input name="creationDate" id="creationDate" value="" type="text" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:150px;"/>
    </td></tr>
</table>
</form>
</div>
<div id="OthercostManagementbutton_bar">
<a href="#" class="btn btn-success" onclick="saveOthercostManagement()">Save</a>
<a href="#" class="btn btn-danger">Close</a>
</div>

