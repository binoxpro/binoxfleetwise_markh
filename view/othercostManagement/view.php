    <div class="col-lg-12">
    <table id="dgOthercostManagement" title="Manage OthercostManagement" class="easyui-datagrid" style="height:500px;" url="controller/othercostManagementController.php?action=view" pagination="true" toolbar="#toolbarOthercostManagement" rownumbers="true" fitColumns="true" singleSelect="true" iconCls="fa fa-car">
    	<thead>
        	<tr>
        	<!--<th field="id" width="50">id</th>-->
            <th field="Truck" width="120">Truck</th>
            <th field="Destination" width="150">Destination</th>
            <th field="Description" width="150">Description</th>
            <th field="Amount" width="90">Amount</th>
            <th field="Date" width="90">Date</th>
            </tr>
        </thead> 
    </table>
    <div id="toolbarOthercostManagement" class="panel-footer">
    	<a href="#" class="btn btn-primary btn-sm" onclick="newOthercostManagement()"><i class="fa fa-plus-circle"></i>Add </a>
        <a href="#" class="btn btn-link btn-sm" onclick="editOthercostManagement()"><i class="fa fa-pencil"></i>Edit</a>
        <a href="#" class="btn btn-link btn-sm"><i class="fa fa-times-circle"></i>Delete</a><a href="admin.php?view=upload_other_cost" class="btn btn-link">Upload Datasheet</a><a href="otherCost.xlsx" class="btn btn-link" download="otherCost"><i class="fa fa-download"></i>Download Template</a><hr/>
         <span>Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="startdatec" style="width:150px;" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" required="true" id="enddatec" style="width:150px;" />Vehicle:<select name="vehicleId" id="vehicleId" class="easyui-combobox" style="height:auto; width:30%;" data-options=       			   "url:'controller/vehicleController.php?action=view',valueField:'id',textField:'regNo',panelWidth:'120',panelHeight:'auto'" ></select><a href="#" class="btn btn-link btn-sm" onclick="viewSearchC()"><i class="fa fa-search"></i>Search</a></span>
    </div>
    </div>
    <?php include_once('add.php'); ?>
    
