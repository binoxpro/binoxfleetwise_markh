<div id="dlgSpareMgt" class="easyui-dialog" style="height:auto; width:700px; " closed='true' modal='true' buttons='#button_barspareMgt'>

<form id="frmSparePart" name="frmSparePart" method="post">
<table cellpadding="0" cellspacing="5" style="width:80%; height:450px;">
<tr><td>
<label>Number Plate:</label></td><td><select id="regNo" name="regNo" class="easyui-combobox" style="width:355px;height:30px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(){

}"></select></td></tr>
<tr><td>
<label>Date Of Repair:</label></td><td><input type="text"  name="dateofrepair" value="" id="dateofrepair" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:355px;height:30px;" /></td></tr>
<tr><td>
<label>Type Of Repair</label></td><td><input type="text"  name="typeofrepair" value="" id="typeofrepair" class="form-control" style="width:355px;" /></td></tr>
<tr><td>
<label>Spare Part</label></td><td><input type="text"  name="partUsed" value="" id="partUsed" class="form-control" style="width:355px;" /></td></tr>
<tr><td>
<label>Cost:</label></td><td><input type="text"  name="cost" value="" id="cost" class="form-control" style="width:355px;" /></td></tr>
<tr><td>
<label>Comment</label></td><td><textarea name="comment" id="comment"  class="form-control" style="width:355px;"></textarea></td></tr>
</table>
</form>

</div>
 <div id="button_barspareMgt">
<a href="#" class="btn btn-primary" onclick="saveSpareManagement()" ><i class="fa fa-check"></i>Save</a>
<a href="#" class="btn btn-danger" iconCls="icon-cancel" onclick="javascript:$('#dlgSpareMgt').dialog('close')"><i class="fa fa-times"></i>Close</a>
</div>
<script type="text/javascript">


function saveSpareManagement()
{
	
		
		
		
		//$.messager.progress();
	 $('#frmSparePart').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			//$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  //$.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 //$.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Tyre information added'});
						$('#dlgSpareMgt').dialog('close'); // close the dialog
						 $('#dgSparePartManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}
</script>
