<table id="dgSparePartManagement" title="Spare Parts Management" class="easyui-datagrid" style="height:500px;" url="controller/spareController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarSpareManagement">
<thead>
<tr>
	<th field="NumberPlate" width="90">Number Plate</th>
    <th field="DateOfRepair" width="90">Date Of Repair</th>
    <th field="TypeOfRepair" width="90">Type Of Repair</th>
    <th field="SparePart" width="90">Spare Part</th>
    <th field="Cost" width="120">Cost</th>
    <th field="Comment" width="90">Comment</th>
</tr>
</thead>

</table>
<div id="toolBarSpareManagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="newSpare()" >Add</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editSpare()" >Edit</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="loadGrid()" >Reload All</a><a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="searchSpareDetailsRange()" >Search</a>
<span style="margin-left:5%;">Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" id="startdateSp" style="width:100%;" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" id="enddateSp" style="width:100%;" />Select Track:<select id="numberPlateSearchSpare" name="numberPlateSearchSpare" class="easyui-combobox" style="width:100px;height:25px;" data-options="url:'controller/trackController.php?action=viewTrack',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){
searchSpareDetails($('#numberPlateSearchSpare').combobox('getValue'));
}"></select></span>

</div>

<?php
include_once('add.php');
?>
<script type="text/javascript">
function searchSpareDetails(searchVal)
{
	var sd=$('#startdateSp').datebox('getValue');
	var ed=$('#enddateSp').datebox('getValue');
	if((sd!=null && ed!=null) && (sd!="" && ed!=""))
	{
		$('#dgSparePartManagement').datagrid('load',{numberPlate:searchVal,startDate:sd,endDate:ed});
	}else{
		$('#dgSparePartManagement').datagrid('load',{numberPlate:searchVal});
	}
}
function searchSpareDetailsRange()
{
	var sd=$('#startdateSp').datebox('getValue');
	var ed=$('#enddateSp').datebox('getValue');
	if((sd!=null && ed!=null) && (sd!="" && ed!=""))
	{
		$('#dgSparePartManagement').datagrid('load',{view:'x',startDate:sd,endDate:ed});
	}else{
		$.messager.show({title:'Warning',msg:'Please supply the date'});
	}
}
function loadGrid()
{
	$('#dgSparePartManagement').datagrid('load',{});
}
function newSpare(){
		
		
			$('#dlgSpareMgt').dialog('open').dialog('setTitle','Spare Parts Management');
			
			$('#frmSparePart').form('clear');
			
			url='controller/spareController.php?action=add';
			
	}
	
	
	
	function editSpare(){
		var row=$('#dgSparePartManagement').datagrid('getSelected');
		if(row){
			$('#dlgSpareMgt').dialog('open').dialog('setTitle','Edit Spare Parts');
			$('#frmSparePart').form('load',row);
			url='controller/spareController.php?action=update&id='+row.Id;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select to  edit'});
		}
		
	}
	
	function saveTrack(){
		
		
		
		$.messager.progress();
	 $('#frmTrack').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Truck successfully added'});
						$('#dlgTrack').dialog('close'); // close the dialog
						 $('#dgSparePartManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	
	
	
</script>