<div id="dlgSpareMgt" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barspareMgt'>
<div class="easyui-panel" title="Spare Parts Management" style="height:400px;">
<form id="frmSparePart" name="frmSparePart" method="post">
<table cellpadding="0" cellspacing="5" style="width:80%;">
<tr><td>
<label>Number Plate:</label></td><td><select id="numberPlatesS" name="NumberPlate" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/trackController.php?action=view',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){

}"></select></td></tr>
<tr><td>
<label>Date Of Repair:</label></td><td><input type="text"  name="DateOfRepair" value="" id="dor" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Type Of Repair</label></td><td><input type="text"  name="TypeOfRepair" value="" id="tor" /></td></tr>
<tr><td>
<label>Spare Part</label></td><td><input type="text"  name="SparePart" value="" id="sparepart" /></td></tr>
<tr><td>
<label>Cost:</label></td><td><input type="text"  name="Cost" value="" id="cost" /></td></tr>
<tr><td>
<label>Comment</label></td><td><textarea name="Comment" id="Comment" cols="15" rows="4"></textarea></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barspareMgt">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveSpareManagement()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgSpareMgt').dialog('close')">Close</a>
</div>
<script type="text/javascript">


function saveSpareManagement()
{
	
		
		
		
		$.messager.progress();
	 $('#frmSparePart').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Tyre information added'});
						$('#dlgSpareMgt').dialog('close'); // close the dialog
						 $('#dgSparePartManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}
</script>
