<table id="dgSparePartManagement" title="Spare Parts Management" class="easyui-datagrid" style="height:500px;" url="controller/spareController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarSpareManagement">
<thead>
<tr>

	<th field="regNo" width="90">Number Plate</th>
    <th field="partUsed" width="90">Part</th>
    <th field="cost" width="90">Cost</th>
    <th field="quantity" width="90">Quantity</th>
    <th field="dateofrepair" width="120">Date</th>
    <th field="typeofrepair" width="90">Type of repair</th>
    <th field="jobcardId" width="90">Has JobCard</th>
</tr>
</thead>

</table>
<div id="toolBarSpareManagement">
<a href="#" class="btn btn-primary"   onclick="newSpare()" ><i class="fa fa-plus-circle"></i>Add</a>
<a href="#" class="btn btn-link"  onclick="editSpare()" ><i class="fa fa-edit"></i>Edit</a>
<a href="#" class="btn btn-link"  onclick="deleteSpare()" ><i class="fa fa-times"></i>Delete</a>
<a href="#" class="btn btn-link" admin.php?view=upload_spare_data  onclick="loadGrid()" ><i class="fa fa-refresh"></i>Reload All</a>
<a href="admin.php?view=upload_spare_data" class="btn btn-link" ><i class="fa fa-upload"></i>Upload</a>
<br/>
<span style="margin-left:1%;">Start Date:<input type="text" name="startdate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" id="startdateSp" style="width:150px;" />End Date:<input type="text" name="enddate" value="" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" id="enddateSp" style="width:150px;" /><a href="#" class="btn btn-link" onclick="searchSpareDetailsRange()" ><i class="fa fa-search"></i>Search</a>Select Track:<select id="numberPlateSearchSpare" name="numberPlateSearchSpare" class="easyui-combobox" style="width:100px;height:25px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(rec){
	$('#dgSparePartManagement').datagrid('load',{vehicleId:rec.id});
}"></select></span>

</div>

<?php
include_once('add.php');
?>
<script type="text/javascript">
function searchSpareDetails(searchVal)
{
	var sd=$('#startdateSp').datebox('getValue');
	var ed=$('#enddateSp').datebox('getValue');
	if((sd!=null && ed!=null) && (sd!="" && ed!=""))
	{
		$('#dgSparePartManagement').datagrid('load',{numberPlate:searchVal,startDate:sd,endDate:ed});
	}else{
		$('#dgSparePartManagement').datagrid('load',{numberPlate:searchVal});
	}
}
function searchSpareDetailsRange()
{
	var sd=$('#startdateSp').datebox('getValue');
	var ed=$('#enddateSp').datebox('getValue');
	if((sd!=null && ed!=null) && (sd!="" && ed!=""))
	{
		$('#dgSparePartManagement').datagrid('load',{view:'x',startDate:sd,endDate:ed});
	}else{
		$.messager.show({title:'Warning',msg:'Please supply the date'});
	}
}
function loadGrid()
{
	$('#dgSparePartManagement').datagrid('load',{});
}
function newSpare(){
		
		
			$('#dlgSpareMgt').dialog('open').dialog('setTitle','Spare Parts Management');
			
			$('#frmSparePart').form('clear');
			
			url='controller/spareController.php?action=add';
			
	}
	
	
	
	function editSpare(){
		var row=$('#dgSparePartManagement').datagrid('getSelected');
		if(row){
			$('#dlgSpareMgt').dialog('open').dialog('setTitle','Edit Spare Parts');
			$('#frmSparePart').form('load',row);
			url='controller/spareController.php?action=update&id='+row.id;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select to  edit'});
		}
		
	}
	
	
	function deleteSpare(){
		var row=$('#dgSparePartManagement').datagrid('getSelected');
		if(row){
			$.messager.confirm('Confirm', 'Are sure you would like to delete is Information', function(r){
				if (r){
			//$('#dlgSpareMgt').dialog('open').dialog('setTitle','Edit Spare Parts');
			//$('#frmSparePart').form('load',row);
			url='controller/spareController.php?action=delete&id='+row.id;
			$.post(url,{},function(data){
				$.messager.show({title:'Message',msg:'Spare part Information deleted'});
				$('#dgSparePartManagement').datagrid('reload');
			})
				}});
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select a spare to delete'});
		}
		
	}
	
	function saveTrack(){
		
		
		
		$.messager.progress();
	 $('#frmTrack').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Truck successfully added'});
						$('#dlgTrack').dialog('close'); // close the dialog
						 $('#dgSparePartManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	}
	
	
	
</script>