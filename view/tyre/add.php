<div id="dlgTyre" class="easyui-dialog" style="height:auto; width:80%;max-width:800px;padding:10px " modal='true' closed='true' buttons='#button_barTyreMaanagement'>

<form id="frmTyre" name="frmTyre" method="post">
<div class="col-lg-6">
<div class='form-group' style='' >
<label>Number Plate:</label><br/><select id="numberPlatex2" name="numberPlate" class="easyui-combobox" style="width:350px;height:30px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(){
	getCurrentMileage($(this).combobox('getValue'),'#currentReading');
}"></select>
</div>
<div class='form-group' style='' >
<label>Current Mileage</label><br/><input type="text"  name="currentReading" value="" style="width:350px;height:30px;" id="currentReading" class="easyui-numberbox" />
</div>
<div class='form-group' style='' >
<label>Serial Number</label><br/><input type="text"  name="serialNumber" class='form-control'  value="" id="serialNumber" /><!--<select id="serialNumber" name="serialNumber" class="easyui-combobox" style="width:350px;height:30px;" data-options="url:'controller/stockController.php?action=viewInStock',textField:'serialNumber',valueField:'id',onSelect:function(rec){
	setBrand(rec.id);
}"></select>-->
</div>
<div class='form-group' style='' >
<label>Position:</label><br /><select id="tyrePosition" name="tyreDetails" class="easyui-combobox" style="width:350px;height:30px;" data-options="url:'controller/tyreTypeController.php?action=view',textField:'tyretypeName',valueField:'id',onSelect:function(){
}"></select>
</div>
<div class='form-group' style='' >
<label>Brand</label><input type="text"  name="brand" class='form-control'  value="" id="brand" />
</div>
<div class='form-group' style='' >
<label>Fixed As</label><br/><select name="fixedAs" id="fixedAs" style="width:355px;height:30px;"><option value="NEW">NEW</option><option value="RETREAT">RETREAT</option></select>
</div>
<div class='form-group' style='' >
<label>Cost Price</label><input name="cost" class='form-control' id="cost" type="text" />
</div>
</div>
<div class="col-lg-6">
<div class='form-group' style='' >
<label>Fixing Date:</label><br/><input type="text"  name="fixingDate" value="" id="fixingDate" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:355px;height:30px;" /></div>
<div class='form-group' style='' >
<label>Odometer Reading</label><br/><input type="text"  name="Odometer" value="" id="Odometer"class="easyui-numberbox" onfocus="javascript:$(this).numberbox('setValue',$('#currentReading').numberbox('getValue'))" style="width:355px;height:30px;" />
</div>
<div class='form-group' style='' >
<label>Expected Km</label><br/><input type="text"  name="expectedKm" value="" id="expectedKm"  onblur="setRemovalMileage($('#Odometer').numberbox('getValue'),$(this).val())" style="width:350px;height:30px;" />
</div>
<div class='form-group' style='' >
<label>Removal Mileage</label><br/><input type="text"  name="removalMileage" style="width:350px;height:30px;" value="" id="removalMileage" class="easyui-numberbox" />
</div>
<div class='form-group' style='' >
<label>Tread Depth</label><input type="text"  name="treadDepth" class='form-control' value="" id="treadDepth" />
</div>
<div class='form-group' style='' >
<label>Comment</label><textarea name="comment" id="comment" class='form-control' cols="15" rows="4"></textarea>
</div>
</div>

</form>


</div>
 <div id="button_barTyreMaanagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveTyreManagement()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgTyre').dialog('close')">Close</a>
</div>


<!--Tyre management  -->
<div id="dlgMileageExtension" class="easyui-dialog" style="height:auto; width:800px; " closed='true' buttons='#button_barExtensionMileage'>
<div class="easyui-panel" title="Service Management" style="height:300px;">
<form id="frmExtenisonMileage" name="frmExtenisonMileage" method="post">
<table cellpadding="0" cellspacing="5" style="width:80%;">
<tr><td>
<label>Removal Mileage:</label></td><td><input type="text"  name="rm" value="" id="rm" /></td></tr>
<tr><td>
<label>Extension Mileage</label></td><td><input type="text"  name="em" value="" id="em" /></td></tr>
</table>
</form>
</div>
</div>
<div id="button_barExtensionMileage">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveExtension()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgMileageExtension').dialog('close')">Close</a>
</div>

