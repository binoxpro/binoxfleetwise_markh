<div class="col-lg-12">
<table id="dgTyreManagement" title="Tyre Management" class="easyui-datagrid" style="height:500px;" url="controller/tyreController.php?action=view" pagination="true" rownumbers="true" singleSelect="true" nowrap="false" toolbar="#toolBarTyreManagement">
<thead data-options="frozen:true">
<tr>
	<th field="regNo" width="120">Vehicle</th>
    <th field="position" width="50">Posn.</th>
    <th field="brand" width="90">Brand</th>
    <th field="serialNumber" width="90">Sno.</th>
</tr>
</thead>
<thead>
    <tr>
    <th field="cost" width="120">Cost</th>
    <th field="fixedAs" width="120">New/Retread</th>
    <th field="fixingDate" width="120">Date Fitted</th>
    <th field="Odometer" width="120">Fitting(Kms)</th>
    <th field="expectedKm" width="120">Expected Kms</th>
    <th field="currentReading" width="120">Current(Km)</th>
    <th field="remaining" width="150">Remaining Kms</th>
    <th field="removalMileage" width="120">Removal Km</th>
    <th field="costperkm" width="120">CPK</th>
    <th field="kmsDone" width="120">Kms Done</th>
    <th field="currentCost" width="120">Current Tyre Cost</th>
    </tr>
</thead>

</table>
<div id="toolBarTyreManagement">
<a href="#" class="btn btn-primary" onclick="newTyre()" ><i class="fa fa-plus-circle"></i><strong>Add</strong></a>
<a href="#" class="btn btn-link" onclick="editTyre()" ><i class="fa fa-pencil"></i><strong>Edit</strong></a>
<a href="#" class="btn btn-link" onclick="deleteTyre()" ><i class="fa fa-times"></i><strong>Delete</strong></a>
<a href="#" class="btn btn-link"  onclick="treadDepth()" ><i class="fa fa-edit"></i>Tread Depth</a>
<a href="#" class="btn btn-link" onclick="openExtenison()" ><i class="fa fa-edit"></i>Extenison</a>
<a href="controller/tyreReport.php" class="btn btn-link" target="_blank"  ><i class="fa fa-file-pdf-o"></i><strong>PDF Report</strong></a>
<a href="#" class="btn btn-link" onclick="exportToExcel()" ><i class="fa fa-download"></i><strong>Download</strong></a>
<a href="admin.php?view=upload_tyre_data" class="btn btn-link" ><i class="fa fa-upload"></i><strong>Upload Data</strong></a><br /><span style="float:right">Vehicle:<select id="numberPlateSearchTyre" name="numberPlateSearchTyre" class="easyui-combobox" style="width:205px;height:25px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(){
searchTyreDetails($('#numberPlateSearchTyre').combobox('getValue'));
}"></select></span>
</div>
</div>
<?php
include_once('add.php');
include_once('update.php');
?>
<div id="dlgTreadDepth" class="easyui-dialog" style="height:auto; width:500px; " modal='true' closed='true' buttons='#button_barTreadDepth'>
<form id="frmTreadDepth" name="frmTyre" method="post">

<div class='form-group' style='' >
<label>Tread Depth(mm)</label><br/><input type="text"  name="treadDepth" value="" id="treadDepth" class="form-control" />
<input type="hidden"  name="stockId" value="" id="stockId" class="" />
</div>

</form>

</div>
<div id="button_barTreadDepth">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveTreadDepth()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgTreadDepth').dialog('close')">Close</a>
</div>
