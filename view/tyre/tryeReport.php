<?php
require_once '../../services/tyreService.php';
require_once'../../services/pdfDocument.php';

		$tyreService=new TyreService();
		$pdf=new Pdf();
		$pdf->reportTitle="Tyre Management Report";
	
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$header=array('Vehicle','Tyre Position','Brand','Serial No','Fixing Date','Odometer','Expected Km','Current','Removal','Comment');
		//$header2=array('Client','Date','JobNo');
		//$pdf->headerTable($header2,$jobService->view_ajob());
		$pdf->FancyTable($header,$tyreService->view());
		$pdf->Output();

?>