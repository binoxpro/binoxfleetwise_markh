<div class="col-lg-12">
<table id="dgAccount" title="Chart of Accounts" class="easyui-datagrid" style="height:auto;" url="controller/accountController.php?action=view" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolbarAccount" pageSize="10" iconCls="fa fa-table">
<thead>
<tr>
	<th field="account_code" width="90">Account Code</th>
    <th field="account_name" width="90">Account Name</th>
    <th field="account_type" width="90" formatter="accountTypeFormatter">Account Type</th>
    <th field="account_balance" width="90">Opening Balance</th>
</tr>
</thead>

</table>
<div id="toolbarAccount">
<a href="#" class="btn btn-primary"  onclick="newAccount()"><i class="fa fa-plus-circle"></i>Add Account</a>
<a href="#" class="btn btn-link"  onclick="editAccount()"><i class="fa fa-edit"></i>Edit Account</a>
<a href="#" class="btn btn-link" onclick="" ><i class="fa fa-times-circle"></i>Delete Account</a>
<!--<a href="#" class="btn btn-link" iconCls="icon-print" plain="true" onclick="" >View Legder</a>-->
<!--<a href="#" class="btn btn-link" iconCls="icon-help" plain="true" onclick="" >More</a>-->
</div>
</div>
<?php
include_once('add.php');
?>
