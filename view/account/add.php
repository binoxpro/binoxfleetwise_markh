<div id="dlgAccount" class="easyui-dialog" style="height:auto; width:500px; padding:5px; " closed='true' buttons='#button_bar' modal="true">
<form id="frmAccount" name="frmAccount" method="post">
<table width="100%"><tr><td><label>Account Code:</label></td><td><input type="text" name="account_code" value="" id="account_code" /></td></tr>
<tr><td><label>Account Name:</label></td><td><input type="text"  name="account_name" value="" id="account_name"/></td></tr>
<tr><td><label>Account Type</label></td><td><select name="account_type" id="account_type" class="easyui-combobox" data-options="url:'view/account/accountType.json',valueField:'id',textField:'text',panelWidth:'130'"></select></td></tr>
<tr><td><label>Account Balance:</label></td><td><input type="text" name="account_balance" value="0" id="account_balance" required/></td></tr>
<tr><td><label>Has Sub Account:</label></td><td><select name="analysis_required" id="analysis_required" class="easyui-combobox" ><option value="N">NO</option><option value="Y">YES</option></select></td></tr>
</table>
<!--<div><input type="text" name="action" value="" id="action" /></div>-->
</form>
</div>
</div>
 <div id="button_bar">
<a href="#" class="btn btn-success" onclick="saveAccount()" ><i class="icon-ok"></i>Save</a>
<a href="#" class="btn btn-danger" onclick="javascript:$('#dlgAccount').dialog('close')"><i class="icon-remove"></i>Cancel</a>
</div>
