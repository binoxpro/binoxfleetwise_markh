<?php if(isset($_REQUEST['view'])){ if($_REQUEST['view']=="complete_jobcard_items"){?>
<div class="col-lg-12">
    <a href="#" onclick="completeJobcard()" class="btn btn-success btn-lg">Complete Job card.</a>&nbsp;&nbsp;<input hidden="hidden" type="text" value='' id="partrequestformid" name="partrequestformid">
</div>
<?php }else{?>
    <div class="col-lg-12">
        <a href="#" onclick="submittostore()" class="btn btn-success btn-lg">Send to Store/</a>&nbsp;&nbsp;<input hidden="hidden" type="text" value='' id="partrequestformid" name="partrequestformid">
    </div>
<?php } }?>
<div class="col-lg-12">

    <h3 class="text-center">MARKH INVESTMENTS LIMITED COMPANY</h3>

    <h3 class="text-center"><span class="label label-primary">WORKSHOP JOB CARD</span> </h3>
    <div id="jobcardHeader">

    </div>
<?php
if($_REQUEST['view']=='complete_jobcard_items') {
    ?>
    <h3 class="text-center">WORK TO BE CARRIED OUT</h3>
    <!--<div id="jobcarditemDetails">-->
    <div><h4>Key:</h4></div>
    <div><span style="background-color: yellow; height: 5px; border: 1px solid black;padding-left: 25px;">.</span>:Pending Job</div>
    <div><span style="background-color: greenyellow; height: 5px; border: 1px solid black;padding-left: 25px;">.</span>:Completed</div>

    <div>
        <table id="dgJobcardItemDetails" class="easyui-datagrid" title="Job Card Details" style="height:auto; border-right:1px solid #66B3FF;" pagination="true" rownumbers="true"
               data-options="
				singleSelect: true,
				toolbar: '#tbJobcardItemDetails',
				method: 'get',
                showFooter:true,
                fitColumns:true,
                iconCls:'fa fa-paper-plane',
                rowStyler: function(index,row){
					if (row.status =='Pending'){
						return 'background-color:yellow;color:black;font-weight:bold;';
					}else
					{
					    return 'background-color:lightgreen;color:black;font-weight:bold;';
					}
				}
			">
            <thead>
            <tr>
                <th data-options="field:'id',width:120, editor:{type:'text'}" hidden="true">No.</th>
                <th data-options="field:'details',width:120, editor:{type:'text'}">Work to be carried out</th>
                <th

                        data-options="field:'initials',width:100,

						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'fullName',textField:'fullName',url:'controller/employeeController.php?action=viewComboType&str=Mechanics'
                }

						}"
                >Mechanics</th>
                <th data-options="field:'repairReview',width:120">Work Carried Out</th>

            </tr>
            </thead>
        </table>

        <div id="tbJobcardItemDetails" style="height:auto">
            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>New Details</a>&nbsp;&nbsp;
            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('saveRow')"><i class="fa fa-save"></i>Save</a>
            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('destroyRow')"><i class="fa fa-times"></i>Remove</a>
            <a href="javascript:void(0)" class="btn btn-link" onclick="workcarriedout()"><i class="fa fa-cogs"></i>Work carried out</a>
        </div>
    </div>
    <!--<div id="partsUsed">-->
    <div>
        <h3 class="text-center">Spare Parts</h3>
        <table id="dgPartUsed" class="easyui-datagrid" title="Job Parts" style="height:auto; border-right:1px solid #66B3FF;" pagination="true" rownumbers="true"
               data-options="
				singleSelect: true,
				toolbar: '#tbPartUsed',
				method: 'get',
                showFooter:true,
                fitColumns:true,
                iconCls:'fa fa-paper-plane',
                nowrap:false

			">
            <thead>
            <tr>
                <th data-options="field:'id',width:120,height:100, editor:{type:'text',options:{multiline:true}}" hidden="true" >No.</th>
                <th
                        data-options="field:'partUsedId',width:100,
                    formatter:function(value,row){
                        return row.partName;
                    },
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'id',textField:'itemName',url:'controller/itemController.php?action=viewCombo'
                }

						}"


                >Spare Part</th>
                <!--<th
                        data-options="field:'supplierId',width:100,
                    formatter:function(value,row){
                        return row.supplierName;
                    },
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'supplierCode',textField:'supplierName',url:'controller/supplierController.php?action=viewComboBox'
                }

						}"


                >Supplier</th>-->
                <th data-options="field:'partNumber',width:120,height:100">PART No.</th>
                <th data-options="field:'quantity',width:70,
						editor:{
							type:'numberbox',options:{precision:0,groupSeparator:','}
						}">QTY</th>
                <!--<th data-options="field:'cost',width:120, editor:{
							type:'numberbox',options:{precision:0,groupSeparator:','}
						}">Cost</th>-->
                <!--<th data-options="field:'total',width:120,height:100">Amount</th>-->
                <th data-options="field:'supplied',width:120,height:100,formatter:formatterCellStyle">Used/Supplied</th>

            </tr>
            </thead>
        </table>

        <div id="tbPartUsed" style="height:auto">
            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>New Details</a>&nbsp;&nbsp;

            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('saveRow')"><i class="fa fa-save"></i>Save</a>
            <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('destroyRow')"><i class="fa fa-times"></i>Remove</a>
            <a href="javascript:void(0)" class="btn btn-link" onclick="editpartSupply()">Spare Supplied</a>
        </div>

    </div>
    </div>
    <div id="dlgpartSupply" class="easyui-dialog" closed="true" style="width:90%; padding:5px;max-width:800px;" toolbar="#partSupplybutton" modal="true" data-options="onResize:function(){$(this).dialog('center')}" >
        <form id="frmpartSupply" name="frmpartSupply" method="post">
            <div class="col-lg-6">
                <div class='form-group' style='' ><label>ID:</label><input name='id' value='' id='id' class='form-control' type='text' readonly /> </div>
                <div class='form-group' style='' ><label>Spare Part:</label><input name='partName' value='' id='partName' class='form-control' type='text' /></div>
                <div class='form-group' style='' ><label>Supplier</label><input name='supplierName' value='' id='supplierName' class='form-control' type='text' readonly /></div>
                <div class='form-group' style='' ><label>Supplier Code</label><input name='supplierId' value='' id='supplierCode' class='form-control' type='text' readonly /> </div>
            </div>
            <div class="col-lg-6">
                <div class='form-group' style='' ><label>Invoice No.</label><input name='invoiceNo' value='' id='invoiceNo' class='form-control' type='text' /> </div>
                <div class='form-group' style='' ><label>Delivery Note No</label><input name='deliveryNoteNo' value='' id='deliveryNoteNo' class='form-control' type='text' /> </div>
                <div class='form-group' style='' ><label>Delivery Date</label><input name='deliveryDate' value='' id='deliveryDate' class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%; height: 30px;" /> </div>
                <div class='form-group' style='' ><label>Qty</label><input name='qty' value='' id='qty' class='form-control' type='text' /> </div>
                <div class='form-group' style='' ><label>Unit Price</label><input name='unitprice' value='' id='unitprice' class='form-control' type='text' /></div>
            </div>
        </form>
    </div>
    <div id="partSupplybutton">
        <a href="#" class="btn btn-primary" onclick="savepartSupply()">Save</a>&nbsp;&nbsp;
        <a href="#" class="btn btn-primary" onclick="javascript:$('#dlgpartSupply').dialog('close')">Close</a>
    </div>
    <div id="dlglabourPayment" class="easyui-dialog" closed="true" style="width:800px; padding:5px;" toolbar="#labourPaymentbutton" modal="true" >
        <form id="frmlabourPayment" name="frmlabourPayment" method="post">
            <div class="col-lg-6">
                <div class='form-group' style='' ><label>Repair Comments</label><textarea name='repairReview' id='repairReview' class='form-control' ></textarea> </div>
                <div class='form-group' style='' ><label>Status</label><Select name='status'  id='status' class='form-control'><option value="Pending">Pending</option><option value="Fixed">Fixed</option></Select></div>
            </div>

        </form>
    </div>
    <div id="labourPaymentbutton">
        <a href="#" class="btn btn-primary" onclick="savelabourPayment()">Save</a>&nbsp;&nbsp;
        <a href="#" class="btn btn-primary" onclick="javascript:$('#dlglabourPayment').dialog('close')">Close</a>
    </div>




<?php
}else{
?>

<h3 class="text-center">WORK TO BE CARRIED OUT</h3>
<!--<div id="jobcarditemDetails">-->
<div>
<table id="dgJobcardItemDetails" class="easyui-datagrid" title="Job Card Details" style="height:auto; border-right:1px solid #66B3FF;" pagination="true" rownumbers="true"
			data-options="
				singleSelect: true,
				toolbar: '#tbJobcardItemDetails',
				method: 'get',
                showFooter:true,
                fitColumns:true,
                iconCls:'fa fa-paper-plane'
			">
		<thead>
			<tr>
                <th data-options="field:'id',width:120, editor:{type:'text'}" hidden="true">No.</th>
				<th data-options="field:'details',width:120, editor:{type:'text'}">Work Descrpiton</th>
                <th

                        data-options="field:'initials',width:100,

						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'fullName',textField:'fullName',url:'controller/employeeController.php?action=viewComboType&str=Mechanics'
                }

						}"
                >Mechanics</th>

			</tr>
		</thead>
	</table>

	<div id="tbJobcardItemDetails" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>New Details</a>&nbsp;&nbsp;
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('saveRow')"><i class="fa fa-save"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgJobcardItemDetails').edatagrid('destroyRow')"><i class="fa fa-times"></i>Remove</a>
		<!--<a href="javascript:void(0)" class="btn btn-link" onclick="payLabour()">Pay Labour</a>-->
	</div>
</div>
<!--<div id="partsUsed">-->
<div>
<h3 class="text-center">Spare Parts</h3>
    <table id="dgPartUsed" class="easyui-datagrid" title="Job Parts" style="height:auto; border-right:1px solid #66B3FF;" pagination="true" rownumbers="true"
           data-options="
				singleSelect: true,
				toolbar: '#tbPartUsed',
				method: 'get',
                showFooter:true,
                fitColumns:true,
                iconCls:'fa fa-paper-plane',
                nowrap:false

			">
        <thead>
        <tr>
            <th data-options="field:'id',width:120,height:100, editor:{type:'text',options:{multiline:true}}" hidden="true" >No.</th>
            <th data-options="field:'prfitemId',width:120,height:100, editor:{type:'text',options:{multiline:true}}" hidden="true" >PrfitemId</th>
            <th data-options="field:'partNumber',width:120,height:100">PART No.</th>
            <th
                    data-options="field:'partUsedId',width:100,
                    formatter:function(value,row)
                    {
                        return row.partName;
                    },
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'id',textField:'itemName',url:'controller/itemController.php?action=viewCombo'
                }

						}"


            >Spare Part</th>
            <th
                    data-options="field:'uomId',width:100,
                    formatter:function(value,row){
                        return row.uom;
                    },
						editor:{
							type:'combobox',
					options:{panelWidth:250,valueField:'id',textField:'uom',url:'controller/uomController.php?action=viewCombo'
                }

						}"


            >UOM</th>
            <!--<th
                    data-options="field:'supplierId',width:100,
                formatter:function(value,row){
                    return row.supplierName;
                },
                    editor:{
                        type:'combobox',
                options:{panelWidth:250,valueField:'supplierCode',textField:'supplierName',url:'controller/supplierController.php?action=viewComboBox'
            }

                    }"


            >Supplier</th>-->

            <th data-options="field:'quantity',width:70,
						editor:{
							type:'numberbox',options:{precision:0,groupSeparator:','}
						}">QTY</th>
            <!--<th data-options="field:'cost',width:120, editor:{
                        type:'numberbox',options:{precision:0,groupSeparator:','}
                    }">Cost</th>-->
            <!--<th data-options="field:'total',width:120,height:100">Amount</th>-->
            <th data-options="field:'issuedQty',width:120,height:100">Qty Issued</th>

        </tr>
        </thead>
    </table>

	<div id="tbPartUsed" style="height:auto">
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('addRow')"><i class="fa fa-plus-circle"></i>New Details</a>&nbsp;&nbsp;
		
		<a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('saveRow')"><i class="fa fa-save"></i>Save</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="javascript:$('#dgPartUsed').edatagrid('destroyRow')"><i class="fa fa-times"></i>Remove</a>
        <a href="javascript:void(0)" class="btn btn-link" onclick="newitem()">Add Inventory Item(If an item does not exist on part list)</a>
	</div>

</div>
</div>
<div id="dlgpartSupply" class="easyui-dialog" closed="true" style="width:90%; padding:5px;max-width:800px;" toolbar="#partSupplybutton" modal="true" data-options="onResize:function(){$(this).dialog('center')}" >
	<form id="frmpartSupply" name="frmpartSupply" method="post">
		<div class="col-lg-6">
			<div class='form-group' style='' ><label>ID:</label><input name='id' value='' id='id' class='form-control' type='text' readonly /> </div>
			<div class='form-group' style='' ><label>Spare Part:</label><input name='partName' value='' id='partName' class='form-control' type='text' /></div>
			<div class='form-group' style='' ><label>Supplier</label><input name='supplierName' value='' id='supplierName' class='form-control' type='text' readonly /></div>
			<div class='form-group' style='' ><label>Supplier Code</label><input name='supplierId' value='' id='supplierCode' class='form-control' type='text' readonly /> </div>
		</div>
		<div class="col-lg-6">
			<div class='form-group' style='' ><label>Invoice No.</label><input name='invoiceNo' value='' id='invoiceNo' class='form-control' type='text' /> </div>
			<div class='form-group' style='' ><label>Delivery Note No</label><input name='deliveryNoteNo' value='' id='deliveryNoteNo' class='form-control' type='text' /> </div>
			<div class='form-group' style='' ><label>Delivery Date</label><input name='deliveryDate' value='' id='deliveryDate' class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%; height: 30px;" /> </div>
			<div class='form-group' style='' ><label>Qty</label><input name='qty' value='' id='qty' class='form-control' type='text' /> </div>
			<div class='form-group' style='' ><label>Unit Price</label><input name='unitprice' value='' id='unitprice' class='form-control' type='text' /></div>
		</div>
	</form>
</div>
<div id="partSupplybutton">
	<a href="#" class="btn btn-primary" onclick="savepartSupply()">Save</a>&nbsp;&nbsp;
	<a href="#" class="btn btn-primary" onclick="javascript:$('#dlgpartSupply').dialog('close')">Close</a>
</div>
<div id="dlglabourPayment" class="easyui-dialog" closed="true" style="width:800px; padding:5px;" toolbar="#labourPaymentbutton" modal="true" >
	<form id="frmlabourPayment" name="frmlabourPayment" method="post">
		<div class="col-lg-6">
			<div class='form-group' style='' ><label>NO.</label><input name='labourId' value='' id='labourId' class='form-control' type='text' /> </div>
			<div class='form-group' style='' ><label>Labour</label><input name='details' value='' id='details' class='form-control' type='text' /></div>
			<div class='form-group' style='' ><label>Amount</label><input name='amount' value='' id='amount' class='form-control' type='text' /></div>
		</div>
		<div class="col-lg-6">
			<div class='form-group' style='' ><label>Date</label><input name='paymentDate' value='' id='paymentDate' class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:100%; height: 30px;" /> </div>
			<div class='form-group' style='' ><label>Funding Account</label><select name='sourceAccount' id='sourceAccount' class="easyui-combobox form-control" style="width:100%;height:30px;" data-options="url:'controller/chartOfaccountController.php?action=viewComboType&accountType=2200',textField:'AccountName',valueField:'AccountCode'" ></select> </div>
			<div class='form-group' style='' ><label>Source Ref/Check No</label><input name='sourceRef' value='' id='sourceRef' class='form-control' type="text" /> </div>
		</div>
	</form>
</div>
<div id="labourPaymentbutton">
	<a href="#" class="btn btn-primary" onclick="savelabourPayment()">Save</a>&nbsp;&nbsp;
	<a href="#" class="btn btn-primary" onclick="javascript:$('#dlglabourPayment').dialog('close')">Close</a>
</div>
<?php
    }
    ?>
<!--The item dialog --->
<div id="dlgitem" class="easyui-dialog" closed="true" style="width:800px; padding:5px;" toolbar="#itembutton" modal="true" >
    <form id="frmitem" name="frmitem" method="post">
        <div class="col-lg-6">
            <div class='form-group' style='' ><label>PartNo:</label><input name='partNo' value='' id='partNo' class='form-control' type='text' /> </div>

            <div class='form-group' style='' ><label>serialNo:</label><input name='serialNo' value='' id='serialNo' class='form-control' type='text' /> </div>

            <div class='form-group' style='' ><label>Item Name:</label><input name='itemName' value='' id='itemName' class='form-control' type='text' /> </div>

            <div class='form-group' style='' ><label>Category:</label><select name='itemCategoryId' id='itemCategoryId' class="easyui-combobox form-control" style="height:40px; width:100%;" data-options="url:'controller/itemcategoryController.php?action=viewCombo',valueField:'id',textField:'itemCategory',panelWidth:'120',panelHeight:'auto',required:'true'" ></select> </div>
        </div>
        <div class="col-lg-6">
            <div class='form-group' style='' ><label>U.O.M:</label><select name='uomId' id='uomId' class="easyui-combobox form-control" style="height:40px; width:100%;" data-options="url:'controller/uomController.php?action=viewCombo',valueField:'id',textField:'uom',panelWidth:'120',panelHeight:'auto'"></select> </div>

            <div class='form-group' style='' ><label>Level Alert:</label><input name='levelAlert' value='' id='levelAlert' class='form-control' type='text' /> </div>

            <div class='form-group' style='' ><label>Bin:</label><input name='bin' value='' id='bin' class='form-control' type='text' /> </div>

            <div class='form-group' style='' ><label>Status:</label><select name='isActive' id='isActive' class='form-control'><option value="1">Active</option><option value="0">Deactive</option> </select> </div>
        </div>
    </form>
</div>
<div id="itembutton">
    <a href="#" class="btn btn-primary" onclick="saveitem()">Save</a>&nbsp;&nbsp;
    <a href="#" class="btn btn-primary" onclick="javascript:$('#dlgitem').dialog('close')">Close</a>
</div>





