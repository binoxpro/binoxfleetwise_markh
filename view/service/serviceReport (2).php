<?php
require_once '../../services/serviceTrackService.php';
require_once'../../services/pdfDocumentservice.php';

		$serviceTrack=new ServiceTrackService();
		$pdf=new Pdf();
		$pdf->reportTitle="Service Management Report";
	
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$header=array('Vehicle','Previous Reading','Next Reading','Interval','Reminder','Service','Type Of Service');
		$pdf->FancyTable($header,$serviceTrack->view());
		$pdf->Output('service.pdf');

?>