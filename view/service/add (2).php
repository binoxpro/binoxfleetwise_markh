<div id="dlgServiceManagement" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barServiceManagement'>
<div class="easyui-panel" title="Service Management" style="height:400px;">
<form id="frmServiceMaintainance" name="frmServiceMaintainance" method="post">
<table cellpadding="0" cellspacing="5" style="width:80%;">
<tr><td>
<label>Number Plate:</label></td><td><select id="numberPlatex" name="numberPlate" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/trackController.php?action=viewTrack',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){
setPreviousReading($('#numberPlatex').combobox('getValue'));
}"></select></td></tr>
<tr><td>
<label>Previous Reading:</label></td><td><input type="text"  name="PreviousDate" value="" id="previousDate" class="easyui-numberbox" style="width:255px;height:25px;"  /></td></tr>
<tr><td>
<label>Interval:</label></td><td><input type="text"  name="IntervalPeriod" value="" id="intervalx" class="easyui-numberbox" onblur="nextReading()" /></td></tr>
<tr><td>
<label>Next Reading:</label></td><td><input type="text"  name="NextDate" value="" id="nextDate" class="easyui-numberbox" style="width:255px;height:25px;" /></td></tr>

<tr><td>
<label>Reminder:</label></td><td><input type="text"  name="Reminder" value="" id="reminder" /></td></tr>
<tr><td>
<label>Service:</label></td><td><select id="service" name="Service" class="easyui-combobox" style="width:255px;height:25px;"><option value="Major">Major</option><option value="Medium">Medium</option><option value="Minor">Minor</option></select></td></tr>
<tr><td>
<label>Type of Service:</label></td><td><textarea name="TypeOfService" id="typeofservice" cols="15" rows="4"></textarea></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barServiceManagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-save" onclick="saveServiceManagement()" >Save</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgServiceManagement').dialog('close')">Close</a>
</div>
<script type="text/javascript">
function nextReading(){
	var id=parseInt($('#previousDate').numberbox('getValue'));
	//$.post('controller/serviceTrackController.php?action=getNextReading',{id:id},function(res){
	//$('#nextDate').datebox('setValue',res);
		
		
	//});
	var inter=parseInt($('#intervalx').val());
	$('#nextDate').numberbox('setValue',(id+inter));
	
	
}
function setPreviousReading(id)
{
	$.post('controller/serviceTrackController.php?action=getPreviousReading',{id:id},function(res){
	$('#previousDate').numberbox('setValue',res);
		
		
	});
	
}
function saveServiceManagement()
{
	
		
		
		
		$.messager.progress();
	 $('#frmServiceMaintainance').form('submit',{ url: url, 
	 onSubmit: function(){
		if(!$(this).form('validate')){
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		
		 },
		 success: function(result){
			 //$.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.progress('close');
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						 $.messager.progress('close');
						$.messager.show({title: 'Info',
					  msg: 'Service information add'});
						$('#dlgServiceManagement').dialog('close'); // close the dialog
						 $('#dgServiceManagement').datagrid('reload'); // reload the user
		}
		
	}
}); 
		

		
	
}
</script>
