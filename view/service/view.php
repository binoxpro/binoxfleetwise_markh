<div class="col-lg-12" style="margin-top:15px; margin-bottom:15px;">
<table id="dgServiceManagement" title="Service Management" class="easyui-datagrid" style="height:500px;" url="controller/serviceTrackController.php?action=manage" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarServiceManagement">
<thead>
<tr>
	<th field="regNo" width="90">Vehicle</th>
    <th field="km_reading" width="90">Previous</th>
    <th field="next_reading" width="90">Next</th>
    <th field="IntervalPeriod" width="90">Interval</th>
    <th field="Reminder" width="120">Reminder</th>
    <th field="Service" width="90">Service</th>
    <th field="TypeOfService" width="200">Type Of Service</th>
    <th field="currentReading" width="200">CurrentReading</th>
</tr>
</thead>

</table>
<div id="toolBarServiceManagement">
<a href="#" class="btn btn-primary" onclick="newServiceMaintance()" ><i class="fa fa-plus-circle"></i>Add</a>
<a href="#" class="btn btn-link" onclick="editServiceMaintance()" ><i class="fa fa-pencil"></i>Edit</a>
<a href="#" class="btn btn-link" onclick="loadGrid()" ><i class="fa fa-redo"></i>Reload All</a>
<a href="admin.php?view=upload_service_data" class="btn btn-link" ><i class="fa fa-upload"></i>Upload Data</a>
<span style="float:right;">Vehicle&nbsp;&nbsp;<select id="numberPlateSearchService" name="numberPlateSearchService" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(rec){
$('#dgServiceManagement').datagrid('load',{numberPlate:rec.id});
}"></select></span>
</div>
</div>
<?php
include_once('add.php');
?>
