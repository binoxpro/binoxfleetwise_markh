<div id="dlgServiceManagement" class="easyui-dialog" style="height:500px; width:700px; " closed='true' buttons='#button_barServiceManagement' modal='true'>
<div class="easyui-panel" title="Service Management" style="height:400px;">
<form id="frmServiceMaintainance" name="frmServiceMaintainance" method="post">
<table cellpadding="5" cellspacing="5" style="width:80%;">
<tr><td>
<label>Service Date:</label></td><td><input type="text"  name="reg_date" value="" id="reg_date" class="easyui-datebox" data-options="formatter:myformatter2,parser:myparser" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Number Plate:</label></td><td><select id="numberPlatex" name="numberPlate" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/vehicleController.php?action=view',textField:'regNo',valueField:'id',onSelect:function(){
setPreviousReading($('#numberPlatex').combobox('getValue'));
}"></select></td></tr>
<tr><td>
<label>Previous Reading:</label></td><td><input type="text"  name="km_reading" value="" id="km_readingx" class="easyui-numberbox" style="width:255px;height:25px;"  /></td></tr>
<tr><td>
<label>Interval:</label></td><td><input type="text"  name="IntervalPeriod" value="" id="intervalx" onblur="nextReading()" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Next Reading:</label></td><td><input type="text"  name="next_reading" value="" id="next_readingx" class="easyui-numberbox" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Reminder:</label></td><td><input type="text"  name="Reminder" value="" id="Reminder" style="width:255px;height:25px;" /></td></tr>
<tr><td>
<label>Service:</label></td><td><select id="Service" name="Service" class="easyui-combobox" style="width:255px;height:25px;"><option value="Major">Major</option><option value="Medium">Medium</option><option value="Minor">Minor</option></select></td></tr>
<tr><td>
<label>Type of Service:</label></td><td><textarea name="TypeOfService" id="TypeOfService" cols="15" rows="4" style="width:255px;"></textarea></td></tr>
</table>
</form>
</div>
</div>
 <div id="button_barServiceManagement">
<a href="#" class="btn btn-primary"  onclick="saveServiceManagement()" >Save</a>
<a href="#" class="btn btn-danger" iconCls="icon-cancel" onclick="javascript:$('#dlgServiceManagement').dialog('close')">Close</a>
</div>
<script type="text/javascript">
</script>
