
<table id="dgServiceManagement" title="Service Management" class="easyui-datagrid" style="height:500px;" url="controller/serviceTrackController.php?action=manage" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#toolBarServiceManagement">
<thead>
<tr>
	<th field="numberPlate" width="90">Vehicle</th>
    <th field="PreviousDate" width="90">Previous</th>
    <th field="NextDate" width="90">Next</th>
    <th field="IntervalPeriod" width="90">Interval</th>
    <th field="Reminder" width="120">Reminder</th>
    <th field="Service" width="90">Service</th>
    <th field="TypeOfService" width="200">Type Of Service</th>
    <th field="currentReading" width="200">CurrentReading</th>
</tr>
</thead>

</table>
<div id="toolBarServiceManagement">
<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="newServiceMaintance()" >Add</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editServiceMaintance()" >Edit</a>
<a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="loadGrid()" >Reload All</a>
<span style="margin-left:20%;">Select Track:<select id="numberPlateSearchService" name="numberPlateSearchService" class="easyui-combobox" style="width:255px;height:25px;" data-options="url:'controller/trackController.php?action=viewTrack',textField:'numberPlate',valueField:'numberPlate',onSelect:function(){
searchSeviceDetails($('#numberPlateSearchService').combobox('getValue'));
}"></select></span>
</div>

<?php
include_once('add.php');
?>
<script type="text/javascript" >
function loadGrid()
{
	$('#dgServiceManagement').datagrid('load',{});
}
function searchSeviceDetails(searchVal)
{
	$('#dgServiceManagement').datagrid('load',{numberPlate:searchVal});
}
function newServiceMaintance(){
		
		
			$('#dlgServiceManagement').dialog('open').dialog('setTitle','Service Management');
			
			$('#frmServiceMaintainance').form('clear');
			
			url='controller/serviceTrackController.php?action=addServiceMaintainance';
				
	}
	
	
	function editServiceMaintance(){
		var row=$('#dgServiceManagement').datagrid('getSelected');
		if(row){
			$('#dlgServiceManagement').dialog('open').dialog('setTitle','Edit service');
			$('#frmServiceMaintainance').form('load',row);
			url='controller/serviceTrackController.php?action=updateServiceMaintance&id='+row.serviceID;
			//prepare_datagrid2();
		}else{
			$.messager.show({title:'Warning',msg:'Please select the a vechile edit'});
		}
		
	}
	
	
		

		
	
	$(function(){
	$('#dgServiceManagement').datagrid({
	rowStyler:function(index,row){
		var nextR=parseInt(row.NextDate);
		var reminder=parseInt(row.Reminder);
		var current=parseInt(row.currentReading);
		//alert(nextR+"/"+current);
		var diff=nextR-current
		//alert(diff+":"+reminder);
		if((nextR-current)<=reminder){
			return 'background-color:#9B0000;color:#FFF;';
		}else if((nextR-current)<=(reminder+500)&&(nextR-current)>(reminder)){
			return 'background-color:#FF702B; text-decoration:blink;';
		}
		
	}
	
	});
	});
	
	
</script>