<?php 
require_once('header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>R-Fleet|Version 2.0</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/bootstrap/easyui.css" />
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/icon.css" /> 
   	<link rel="stylesheet" media="screen" href="lib/handsontable/dist/jquery.handsontable.full.css">
	<link rel="stylesheet" href="lib/signtures/assets/jquery.signaturepad.css">
    <link rel="stylesheet" href="lib/datepicker/css/datepicker.css" type="text/css">



    

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
	/* *, *:before, *:after{
-webkit-box-sizing:content-box;
-moz-box-sizing:content-box;
box-sizing:content-box;
}*/
.datagrid-editable-input,.textbox-text{
	color:#0061C1;
}
td input{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
td select{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
easyui-datebox{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
	</style>
</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-wrench"></i><span>REFIX</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/user.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?php echo $_SESSION['username']; ?></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                            <li>
                            <a><i class="fa fa-folder-open-o"></i>Depot Activities<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                               <!--<li>
                                    <a href="?view=vehicle_Load">Load Vehicle</a>
                                </li>-->

                                <li>
                                    <a href="?view=Manage_Order_Dispatch">Dispatch Order(Work order)</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Delivery_New">Trip Completion</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Order_New">WorkOrder Management</a>
                                </li>
                                <li>
                                    <a href="?view=manage_customer_complaints">Customer Complaints</a>
                                </li>
                                <li>
                                    <a href="?view=manage_customer_list">Customer List</a>
                                </li>
                                <li>
                                    <a href="?view=trip_Number_manger">Trip Number Manager</a>
                                </li>
                                <li>
                                    <a href="?view=manage_trip_type">Trip Type</a>
                                </li>
                                
                                </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            <li>
                            <a><i class="fa fa-folder-open-o"></i>Operations<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                               <!--<li>
                                    <a href="?view=vehicle_Load">Load Vehicle</a>
                                </li>-->
                                
                                <li>
                                    <a href="?view=Manage_Order">Order</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Delivery">Delivery Order</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Other_Cost_Management">OtherCost  To Manage</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Fuel">Fuel Management</a>
                                </li>
                                 <li>
                                    <a href="?view=Manage_Odometer">Weekly Odometer Reading</a>
                                </li>
                                </ul>
                            <!-- /.nav-second-level -->
                        </li>
                                <li>
                                    <a><i class="fa fa-file-pdf-o"></i>Operation Report<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                    	<li>
                                            <a href="?view=Utilisation_Report">Capacity Utilisation</a>
                                        </li>
                                        <li>
                                            <a href="?view=Time_Utilisation_Report">Time Utilisation</a>
                                        </li>
                                        <li>
                                            <a href="?view=Running_Cost_Report">Running Cost Report</a>
                                        </li>
                                        <li>
                                            <a href="?view=Utilisation_Graphy">Utilisation Graph</a>
                                        </li>
                                        <li>
                                            <a href="?view=Total_Order_Delivered">Total Order_Delivered</a>
                                        </li>
                                        <li>
                                            <a href="?view=Products_Delivered">Products_Delivered</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="openDia()">Vehicle Km Graph</a>
                                        </li>
                                    </ul> 
                                </li>
                             
                        <!--Inspection Click-->
                        <li>
                            <a><i class="fa fa-binoculars"></i>IOV<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                            	<li>
                                    <a href="?view=checkList_download">Download Check List</a>
                                </li>
                                <li>
                                    <a href="?view=IOV_Inspection">Inspection Vehicle</a>
                                </li>
                                <li>
                                    <a href="?view=Inspection_List">Inspection List</a>
                                </li>
                                <li>
                                    <a href="?view=Create_JobCard">Create Job Card</a>
                                </li>
                               <!-- <li>
                                    <a href="?view=Edit_JobCard">Edit Job Card</a>
                                </li>-->
                                <li>
                                    <a href="?view=Follow_Up_JobCard">Follow Up Job Card</a>
                                </li>
                                 <li>
                                    <a href="?view=Complete_JobCard">Repair Report</a>
                                </li>
                                </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-file-pdf-o"></i>IOV Reports<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="?view=Vehicle_History_Report">Vehicle History</a>
                                        </li>
                                        <li>
                                            <a href="?view=Parts_used_Report">Parts Used/Month</a>
                                        </li>
                                        
                                    </ul>
                                    <!-- /.nav-third-level -->
                               </li>
                                <li>
                                    <a><i class="fa fa-cogs"></i>IOV Config<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="?view=xxx">Check list Type</a>
                                        </li>
                                        <li>
                                            <a href="?view=Setting_Check_List_Section">Check List Section</a>
                                        </li>
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>

                            
                        
                                <!--<li>
                                    <a href="?view=Manage_Tyre">Tyre Management</a>
                                </li>-->
                                <!--<li>
                                    <a href="#"><i class="fa fa-cogs"></i>Settings<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                         <li>
                                    <a href="?view=Manage_Tyre_Type">Tyre Position</a>
                                		</li>
                                    </ul>
                                   
                                </li>-->
                                 <li>
                                    <a><i class="fa fa-truck"></i>Tire Inventory<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                         <li>
                                            <a href="?view=Purchase_Tire">Purchase Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=Mount_Tire">DisMount/Mount Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=retread_return_list">Retread Return list</a>
                                		</li>
                                        <li>
                                            <a href="?view=scrapped_tire">Scrapped Tire</a>
                                		</li>
                                        <li>
                                            <a href="?view=scrapped_tire">Tire Disposal</a>
                                		</li>
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a><i class="fa fa-cog"></i>Mounted Tire Mgt<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="?view=tire_inspection">Tire Inspection</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_inspection_history">Tire Inspection History</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_monitoring">Tire Monitoring</a>
                                		</li>
                                         <li>
                                            <a href="?view=tire_Rotate">Rotate Tire/Transfer Tire</a>
                                		</li>
                                        
                                        <li>
                                            <a href="?view=warning_Tire">Warning Tires</a>
                                		</li>
                                        
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a><i class="fa fa-cogs"></i>Tire Reports<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="?view=tire_summary">Tire Summary</a>
                                		</li>
                                        <li>
                                            <a href="?view=mounted_tire_report">Mounted Tire Report</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_brand_summary">Tire Brand Summary</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_history_by_Vehicle">Tire History(Truck)</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_history_by_tire">Tire History(Tire)</a>
                                		</li>
                                        <li>
                                            <a href="?view=tire_inventory_listing">Tire Inventory</a>
                                		</li>
                                        
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                                <li>
                                    <a><i class="fa fa-cogs"></i>Tire Config<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                         <li>
                                    <a href="?view=Manage_Tyre_Type">Tire Position</a>
                                		</li>
                                        <li>
                                    <a href="?view=Manage_Tire_size">Tire Size</a>
                                		</li>
                                        <li>
                                    <a href="?view=Manage_Tire_Pattern">Tire Pattern</a>
                                		</li>


                                    </ul>
                                   
                                </li>
                            
                        <li>
                            <a><i class="fa fa-wrench"></i>Service Management<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                               <!-- <li>
                                    <a href="?view=Service_Management">Service Management</a>
                                </li>-->
                                <li>
                                    <a href="?view=Service_Management_New">Service Management(New)</a>
                                </li>
                                <li>
                                    <a href="?view=Spare_Parts_Management">Spare Parts</a>
                                </li>
                                </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-cogs"></i>Service Config<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                    <li>
                                    <a href="?view=System_Administration_CheckList_Management">CheckList Management</a>
                                    </li>
                                    <li>
                                        <a href="?view=System_Administration_CheckListType_Management">CheckListType Management</a>
                                    </li>
                                     <li>
                                        <a href="?view=Manage_Sevrice_Type">Service Setup</a>
                                    </li>
                                    
                                     <li>
                                        <a href="?view=Manage_Model">Setup Model</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Component">Setup Component</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_service_type">Setup Service Type</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Action_Notes">Setup Action Note</a>
                                    </li>
                                    <li>
                                        <a href="?view=Manage_Service_Interval">Setup Service Interval</a>
                                    </li>
                                        
                                    </ul>
                                   <!-- /.nav-third-level-->
                                </li>
                            
                        <li>
                        <a><i class="fa fa-truck"></i>Vehicle Record<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                <li>
                                    <a href="?view=System_Administration_Vehicle_Management">Vehicle</a>
                                </li>
                                <li>
                                    <a href="?view=Vehicle_Standards_Management">Vehicle Standard</a>
                                </li>
                                <li>
                                    <a href="?view=licence_Management">Licence Management</a>
                                </li>
                                </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-cogs"></i>Vehicle Config<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                <li>
                                    <a href="?view=System_Administration_Vehicle_Types">Vechile Type Management</a>
                                </li>
                                <li>
                                    <a href="?view=System_Administration_Trailer_Management">Trailer Management</a>
                                </li>
                                </ul>
                                </li>
							
                        <li>
                            <a><i class="fa fa-users"></i>System Admin<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                               <li>
                                    <a href="?view=Manage_Other_Cost">OtherCost Management</a>
                                </li>
                                <li>
                                    <a href="?view=Manage_Access_Control">User Management</a>
                                </li>                                
                            </ul>
                            
                        </li>
                        <li>
                            <a><i class="fa fa-user-md"></i>Driver Record<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                <li>
                                    <a href="?view=Driver_Registration">Register Driver(Old Version)</a>
                                </li>
                                <!-- <li>
                                    <a href="?view=Driver_Registration_New">Register Driver(New)</a>
                                </li>-->
                                <li>
                                    <a href="?view=Driver_Records">Drivers Records(New)</a>
                                </li>
                              <!--  <li>
                                    <a href="?view=Management_Of_Driver">Manage Driver</a>
                                </li> -->
                                <li>
                                    <a href="?view=Manage_of_License">Manage License</a>
                                </li>
                               <!-- <li>
                                    <a href="?view=Manage_Driver_Training">Manage Driver's Training</a>
                                </li>-->
                                <li>
                                    <a href="?view=Manage_Driver_Training_new">Manage Driver's Training(New)</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                            </ul>
                        </div>
                        

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/user.png" alt=""><?php echo $_SESSION['username']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="javascript:;">Change Password</a>
                                    </li>
                                    
                                    <li>
                                        <a href="help.html">Help</a>
                                    </li>
                                    <li><a href="controller/usercontroller.php?action=logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- note notification can go here-->
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										$header2="";
										$header3="";
										for($i=0;$i<sizeof($header);$i++){
											if($i<2){
											$header2.= strtoupper($header[$i])."&nbsp;";
											}else{
											$header3.=$header[$i]."&nbsp;";	
											}
										}
										echo "<span>".$header2."</span>:<span>".$header3."</span>";
									}else{
										echo "DashBoard";
									}
			
							?></h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:auto;">
                                <div class="x_title">
                                    <h2><!--<i class="fa fa-cogs"></i>-->
                            <?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										for($i=0;$i<sizeof($header);$i++){
											echo strtoupper( $header[$i])."&nbsp;";
										}
									}else{
										echo "DashBoard";
									}
			
							?></h2>
                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                
                                    <?php require_once('controller/dispatcher.php'); ?>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">REFIX-SOFTWARE &copy; code360 Data Solution <a>code360 Data Solution</a>. |
                            <span class="lead"> <i class="fa fa-cogs"></i>FST Limited</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <?php require_once('view/odometer/kmgraphgenerator.php'); ?>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>

    <script src="js/custom.js"></script>

    
    <script type="text/javascript" src="lib/external/jquery/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="lib/external/jquerylib/jquery.edatagrid.js"></script>
	<script type="text/javascript" src="lib/external/jquerylib/datagrid-detailview.js"></script>
    <!--<script src="lib/signtures/assets/jquery.min.js"></script>-->
	<script src="lib/signtures/jquery.signaturepad.min.js"></script>
	<script src="lib/signtures/assets/json2.min.js"></script>
    
	<script type="text/javascript" src="lib/js/scriptEngine.js"></script>
	<script type="text/javascript" src="lib/external/chart/Chart.js"></script>
    <!-- moris js -->
    
    <?php require_once('controller/scriptController.php'); ?>
    <script type="text/javascript">
	function loadingBar(message){
	$.messager.progress({title:'loading',msg:message});
	}

	function closeBar()
	{
	$.messager.progress('close');
	}
	
	function saveForm(formName,url,dialogName,datagridName){
		//url=url;
		$.messager.progress();
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Warning',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						$(dialogName).dialog('close');
						$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


}

function saveFormUrl(formName,url,urlx){
		//url=url;
		$.messager.progress({title:'Submitting',msg:'Please wait connecting to server in progress...'});
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.show({title: 'Error',
					  msg: result.msg});
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						//$(dialogName).dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
						 window.location=urlx+'&id='+result.x;
		}
		
	}
}); 

}
function openDia(){
	$('#dlgGraphGenerator').dialog('open').dialog('setTitle','Km Graph Generator');
	$('#frmGraphGenerator').form('clear');
	//newTrip();
	//url='controller/capacityController.php?action=add';
}
function generateGraph()
{
	var vehicleId=$('#geneId').combobox('getValue');
	location.href='admin.php?view=Vehicle_Kilometer_Graph&id='+vehicleId;
	
}

function notifyMe() {
  if (!("Notification" in window)) {
   // alert("This browser does not support desktop notification");
  }
  else if (Notification.permission === "granted") {
	  $.post('controller/orderController.php?action=orderNotification',{x:'0'},function(data){
		  
	  
        var options = {
                body: data,
                icon: "image2.png",
                dir : "ltr"
             };
          var notification = new Notification("System Alerts",options);
	  });
  }
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }
   
      if (permission === "granted") {
        var options = {
              body: "This is the body of the notification",
              icon: "image2.jpg",
              dir : "ltr"
          };
        var notification = new Notification("System Alerts",options);
      }
    });
  }
}
//var myVar=setInterval(function(){notifyMe()},150000);
//var myVar=setTimeout(function(){notifyMe()}, 10);
$(function (){
	$('.tdate').datepicker({format:'yyyy-mm-dd'});
	
});

</script>

</body>

</html>