<?php 
 require_once('../services/dao.php');
class emailNotification extends DAO{
private $id;
private $name;
private $email;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getname(){
 return $this->name;
}
public function getemail(){
 return $this->email;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setname($name){
  $this->name=$name;
}
public function setemail($email){
  $this->email=$email;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>