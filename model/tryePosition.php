<?php
include_once('../services/dao.php');
class TyrePosition extends DAO{
	private $Id;
	private $positionDetails;
	private $isActive;
	
	
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
	
	public function setId($Id){
		$this->Id=$Id;
	}
	public function setPositionDetails($positionDetails){
		$this->positionDetails=$positionDetails;
	}
	
	public function setIsActive($isActive){
		$this->isActive=$isActive;
	}
	
	public function getId(){
		return $this->Id;
	}
	public function getPositionDetails(){
		return $this->positionDetails;
	}
	public function getIsActive(){
		return $this->isActive;
	}
	
	public function save(){
		 
	}
    public function delete(){
		
	}
    public function update(){
		
	}
    public function view(){
		
	}
    public function view_query($sql){
		
	}
}

?>