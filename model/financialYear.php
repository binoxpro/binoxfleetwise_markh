<?php 
 require_once('../services/dao.php');
class financialYear extends DAO{
private $id;
private $startDate;
private $endDate;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getstartDate()
		{
 		return $this->startDate;
}

		public function getendDate()
		{
 		return $this->endDate;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setstartDate($startDate)
		{
		  $this->startDate=$startDate;
		}

		public function setendDate($endDate)
		{
		  $this->endDate=$endDate;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>