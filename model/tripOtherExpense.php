<?php 
 require_once('../services/dao.php');
class TripOtherExpense extends DAO{
private $id;
private $tripNo;
private $expenseId;
private $Amount;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function gettripNo()
		{
 		return $this->tripNo;
}

		public function getexpenseId()
		{
 		return $this->expenseId;
}

		public function getAmount()
		{
 		return $this->Amount;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function settripNo($tripNo)
		{
		  $this->tripNo=$tripNo;
		}

		public function setexpenseId($expenseId)
		{
		  $this->expenseId=$expenseId;
		}

		public function setAmount($Amount)
		{
		  $this->Amount=$Amount;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>