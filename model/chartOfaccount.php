<?php 
 require_once('../services/dao.php');
class chartOfaccount extends DAO{
private $AccountCode;
private $AccountName;
private $AccountTypeCode;
private $IsActive;
private $ParentAccountCode;
		function __construct()
		{
 		parent::__construct();
		}

		public function getAccountCode()
		{
 		return $this->AccountCode;
}

		public function getAccountName()
		{
 		return $this->AccountName;
}

		public function getAccountTypeCode()
		{
 		return $this->AccountTypeCode;
}

		public function getIsActive()
		{
 		return $this->IsActive;
}

		public function getParentAccountCode()
		{
 		return $this->ParentAccountCode;
}

		public function setAccountCode($AccountCode)
		{
		  $this->AccountCode=$AccountCode;
		}

		public function setAccountName($AccountName)
		{
		  $this->AccountName=$AccountName;
		}

		public function setAccountTypeCode($AccountTypeCode)
		{
		  $this->AccountTypeCode=$AccountTypeCode;
		}

		public function setIsActive($IsActive)
		{
		  $this->IsActive=$IsActive;
		}

		public function setParentAccountCode($ParentAccountCode)
		{
		  $this->ParentAccountCode=$ParentAccountCode;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>