<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class JobCard extends DAO {
    private $id;
    private $jobNo;
    private $creationDate;
    private $vehicleId;
    private $rectified;
    private $dateCompleted;
	private $time;
	private $odometer;
	private $checkListId;
	private $comment;
	private $labourCost;
	private $labourHour;
    private $garrage;
    private $fuelLevel;
    private $driverName;
    private $createdBy;
    private $approvedBy;
    private $approvedDate;




    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getjobNo() {
        return $this->jobNo;
    }

    public function getcreationDate() {
        return $this->creationDate;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function getrectified() {
        return $this->rectified;
    }
	 public function getdateCompleted() {
        return $this->dateCompleted;
    }
	public function gettime(){
		return $this->time;
	}
	public function getodometer(){
		return $this->odometer;
	}
	
	public function getcheckListId(){
		return $this->checkListId;
	}
	
	public function getcomment(){
		return $this->comment;
	}

	public function getlabourCost() {
        return $this->labourCost;
    }
	
	public function getlabourHour() {
        return $this->labourHour;
    }

    public function getgarrage() {
        return $this->garrage;
    }


    public function getfuelLevel()
    {
        return $this->fuelLevel;
    }

    public function getdriverName()
    {
        return $this->driverName;
    }

    public function getcreatedBy()
    {
        return $this->createdBy;
    }

    public function getapprovedBy()
    {
        return $this->approvedBy;
    }

    public function getapprovedDate()
    {
        return $this->approvedDate;
    }



    public function setid($id) {
        $this->id = $id;
    }

    public function setjobNo($jobNo) {
        $this->jobNo = $jobNo;
    }

    public function setdate($date) {
        $this->creationDate = $date;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }

    public function setrectified($rectified) {
        $this->rectified = $rectified;
    }
	public function setdateCompleted($dateCompleted) {
        $this->dateCompleted = $dateCompleted;
    }
	
	public function setodometer($odometer){
		$this->odometer=$odometer;
	}
	public function settime($time) {
        $this->time = $time;
    }
	
	
	public function setChecklistId($checkListId) {
        $this->checkListId = $checkListId;
    }
	
	public function setComment($comment)
    {
        $this->comment = $comment;
    }
	
	public function setLabourCost($labourCost)
    {
        $this->labourCost = $labourCost;
    }
	
	public function setLabourHour($labourHour)
    {
        $this->labourHour = $labourHour;
    }

    public function setgarrage($garrage)
    {
        $this->garrage = $garrage;
    }

    public function setfuelLevel($fuelLevel)
    {
        $this->fuelLevel = $fuelLevel;
    }

    public function setDriverName($driverName)
    {
        $this->driverName = $driverName;
    }

    public function setcreatedBy($createdBy)
    {
        $this->createdBy=$createdBy;
    }

    public function setapprovedBy($approvedBy)
    {
        return $this->approvedBy=$approvedBy;
    }

    public function setapprovedDate($approvedDate)
    {
        return $this->approvedDate=$approvedDate;
    }


    public function delete()
    {
        
    }

    public function save()
    {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
