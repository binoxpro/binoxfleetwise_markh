<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class TruckStatus extends DAO {
    private $id;
    private $truckNo;
    private $truckCapacity;
    private $status;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function gettruckNo() {
        return $this->truckNo;
    }

    public function gettruckCapacity() {
        return $this->truckCapacity;
    }

    public function getstatus() {
        return $this->status;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function settruckNo($truckNo) {
        $this->truckNo = $truckNo;
    }

    public function settruckCapacity($truckCapacity) {
        $this->truckCapacity = $truckCapacity;
    }

    public function setstatus($status) {
        $this->status = $status;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
