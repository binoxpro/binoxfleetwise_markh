<?php 
 require_once('../services/dao.php');
class tireInspectionHeader extends DAO{
private $id;
private $vehicleId;
private $lastReading;
private $currentReading;
private $creationDate;
private $nextReading;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getvehicleId(){
 return $this->vehicleId;
}
public function getlastReading(){
 return $this->lastReading;
}
public function getcurrentReading(){
 return $this->currentReading;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function getisActive(){
 return $this->isActive;
}
public function getNextReading(){
 return $this->nextReading;
}

public function setid($id){
  $this->id=$id;
}
public function setvehicleId($vehicleId){
  $this->vehicleId=$vehicleId;
}
public function setlastReading($lastReading){
  $this->lastReading=$lastReading;
}
public function setcurrentReading($currentReading){
  $this->currentReading=$currentReading;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setNextReading($nextReading){
  $this->nextReading=$nextReading;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>