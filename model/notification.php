<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Notification extends DAO {
    private $id;
    private $section;
	private $to;
    private $cc;
	private $msg;
	private $headerNotif;
	private $sent;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getcc() {
        return $this->cc;
    }
	public function getsection() {
        return $this->section;
    }
	public function getto() {
        return $this->to;
    }
	public function getmsg(){
		return $this->msg;
	}
	public function getheader(){
		return $this->headerNotif;
	}
	public function getsent(){
		return $this->sent;
	}

    public function setId($id) {
        $this->id = $id;
    }
	public function setsection($section) {
        $this->section = $section;
    }
	
	public function setcc($cc) {
        $this->cc = $cc;
    }
	
	public function setto($to) {
        $this->to=$to;
    }
	public function setmsg($msg){
		$this->msg=$msg;
	}
	public function setheaderNotif($headerNotif){
		$this->headerNotif=$headerNotif;
	}
	public function setsent($sent){
		$this->sent=$sent;
	}
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
