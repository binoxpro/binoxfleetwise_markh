<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Allocation extends DAO {
    private $id;
    private $truckId;
    private $customer;
    private $orderId;
    private $ordertypeId;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function gettruckId() {
        return $this->truckId;
    }

    public function getcustomer() {
        return $this->customer;
    }

    public function getorderId() {
        return $this->orderId;
    }

    public function getordertypeId() {
        return $this->ordertypeId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function settruckId($truckId) {
        $this->truckId = $truckId;
    }

    public function setcustomer($customer) {
        $this->customer = $customer;
    }

    public function setorderId($orderId) {
        $this->orderId = $orderId;
    }

    public function setordertypeId($ordertypeId) {
        $this->ordertypeId = $ordertypeId;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
