<?php 
 require_once('../services/dao.php');
class driverNew extends DAO{
private $id;
private $firstName;
private $lastName;
private $dob;
private $contact;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getfirstName(){
 return $this->firstName;
}
public function getlastName(){
 return $this->lastName;
}
public function getdob(){
 return $this->dob;
}
public function getcontact(){
 return $this->contact;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setfirstName($firstName){
  $this->firstName=$firstName;
}
public function setlastName($lastName){
  $this->lastName=$lastName;
}
public function setdob($dob){
  $this->dob=$dob;
}
public function setcontact($contact){
  $this->contact=$contact;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>