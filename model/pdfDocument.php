<?php
require_once('../lib/fpdf17/fpdf.php');
class Pdf extends FPDF{
	public function Header(){
		
		$this->Image('dvibanner.jpg',0,6,45,28);
    // Arial bold 15
    	$this->SetFont('Times','',15);
    // Move to the right
    	$this->Cell(40);
		$this->Ln(15);
		$this->addressBlock();
		$this->SetX(10);
		$this->SetFont('Times','',10);
		$this->SetTextColor(200);
		$this->Cell(0,10,'Page '.$this->PageNo()."of ".$this->AliasNbPages.'',0,0,'L');
		$this->Ln(5);
    // Title
    	
    // Line break
	/*
    	$this->Ln(15);
		$this->Cell(40);
		$this->Cell(80,10,'Pro Invoice',1,0,'C');
		$this->Ln(10);
		//$this->Cell(10);
		$this->Cell(0,10,'TIN NO:10003555222',0,0);
		$this->Ln(10);
		*/
	}
	public function addressBlock(){
	$this->SetXY(10,10);
    // Arial italic 8
    $this->SetFont('Helvetica','B',9);
	$this->SetTextColor(55,28,0);
    // Page number
   // $this->Cell(0,10,'Page '.$this->PageNo().'',0,0,'C');
	//$this->Ln();
	//Suite 407, Diamond Trust Building Plot 18/19 Kampala Road +256(0)71269967 info@dvi-uganda.com  www.dvi-uganda.com
	 $this->Cell(0,10,'Diamond Trust Building, Suite 407',0,0,'R');
	 $this->Ln(4);
	 
	 $this->Cell(0,10,'Plot 18/19 Kampala Road ...............',0,0,'R');
	 $this->Ln(4);
	 $this->Cell(0,10,'Email:info@dvi-uganda.com..........',0,0,'R');
	 $this->Ln(4);
	 $this->Cell(0,10,'Tel:+256(0)71269967........................',0,0,'R');
	 $this->Ln(4);
	  $this->SetFont('Helvetica','B',10);
	  $this->SetTextColor(0,99,198);
	 $this->Cell(0,10,'Website:www.dvi-uganda.com',0,0,'R');	
	 $this->Ln(10);
	}

	public function Footer(){
		$this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
	$this->SetTextColor(120,120,120);
    // Page number
    
	//$this->Ln();
	 $this->Cell(0,0,'Suite 407, Diamond Trust Building Plot 18/19 Kampala Road +256(0)71269967 info@dvi-uganda.com  www.dvi-uganda.com ',0,0,'C');
	}
	
	function FancyTable($header, $data){
    // Colors, line width and bold font
	  $this->Ln(10);
    $this->SetFillColor(255,255,47);
    $this->SetTextColor(0,0,0);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.1);
    $this->SetFont('Helvetica','B',10);
    // Header
    $w = array(60, 20, 40, 30,45);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],10,$header[$i],1,0,'L',true);
    $this->Ln();
    // Color and font restoration
    $this->SetFillColor(204,235,255);
    $this->SetTextColor(0);
    $this->SetFont('Helvetica','',9);
    // Data
    $fill = false;
	$total=0;
	$numRows=sizeof($data);
	$i=0;
    foreach($data as $row)
    {
        $this->Cell($w[0],5,$row[2],'LR',0,'L',$fill);
        $this->Cell($w[1],5,$row[3],'LR',0,'L',$fill);
        $this->Cell($w[2],5,number_format($row[4],0),'LR',0,'R',$fill);
        $this->Cell($w[3],5,number_format($row[5],0),'LR',0,'R',$fill);
		$this->Cell($w[4],5,number_format(($row[3]*$row[4]*$row[5]),0),'LR',0,'R',$fill);
        $this->Ln();
		$this->Cell($w[0],5,$row[8],'LR',0,'L',$fill);
        $this->Cell($w[1],5,"",'LR',0,'L',$fill);
        $this->Cell($w[2],5,"",'LR',0,'R',$fill);
        $this->Cell($w[3],5,"",'LR',0,'R',$fill);
		$this->Cell($w[4],5,"",'LR',0,'R',$fill);
        $this->Ln();
        $fill = $fill;
		$total=$total+($row[3]*$row[4]*$row[5]);
		 if($numRows<=1){
			$this->Cell($w[0],5,'','LR',0,'L',$fill);
			$this->Cell($w[1],5,'','LR',0,'L',$fill);
        	$this->Cell($w[2],5,'','LR',0,'L',$fill);
       		$this->Cell($w[3],5,'','LR',0,'R',$fill);
			$this->Cell($w[4],5,'','LR',0,'R',$fill);
        	$this->Ln();
			$this->Cell($w[0],5,'','LR',0,'L',$fill);
			$this->Cell($w[1],5,'','LR',0,'L',$fill);
        	$this->Cell($w[2],5,'','LR',0,'L',$fill);
       		$this->Cell($w[3],5,'','LR',0,'R',$fill);
			$this->Cell($w[4],5,'','LR',0,'R',$fill);
			$this->Ln();
			$this->Cell($w[0],5,'','LR',0,'L',$fill);
			$this->Cell($w[1],5,'','LR',0,'L',$fill);
        	$this->Cell($w[2],5,'','LR',0,'L',$fill);
       		$this->Cell($w[3],5,'','LR',0,'R',$fill);
			$this->Cell($w[4],5,'','LR',0,'R',$fill);
			$this->Ln();
			$this->Cell($w[0],5,'','LR',0,'L',$fill);
			$this->Cell($w[1],5,'','LR',0,'L',$fill);
        	$this->Cell($w[2],5,'','LR',0,'L',$fill);
       		$this->Cell($w[3],5,'','LR',0,'R',$fill);
			$this->Cell($w[4],5,'','LR',0,'R',$fill);
			$this->Ln();
			$this->Cell($w[0],5,'','LR',0,'L',$fill);
			$this->Cell($w[1],5,'','LR',0,'L',$fill);
        	$this->Cell($w[2],5,'','LR',0,'L',$fill);
       		$this->Cell($w[3],5,'','LR',0,'R',$fill);
			$this->Cell($w[4],5,'','LR',0,'R',$fill);
			$this->Ln();
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln(1);
			//$fill = !$fill;
		}
		if($i==($numRows-1)&& $numRows==2)
		{
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
        	$this->Ln();
			// $fill = $fill;
			
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			// $fill = $fill;
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			 //$fill = $fill;
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			// $fill = $fill;
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln(1);
			// $fill = $fill;
		}
		
		if($i==($numRows-1)&& $numRows==3)
		{
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
        	$this->Ln();
			 //$fill = !$fill;
			
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			 //$fill = !$fill;
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			 //$fill = !$fill;
			$this->Cell($w[0],10,'','LR',0,'L',$fill);
			$this->Cell($w[1],10,'','LR',0,'L',$fill);
        	$this->Cell($w[2],10,'','LR',0,'L',$fill);
       		$this->Cell($w[3],10,'','LR',0,'R',$fill);
			$this->Cell($w[4],10,'','LR',0,'R',$fill);
			$this->Ln();
			 //$fill = !$fill;
			
		}
		
		$i=$i+1;
    }
    // Closing line
	 $this->Cell(array_sum($w),0,'','T');
	  $this->Ln();
	  //$this->Cell(array_sum($w),0,'','T');
	 //$this->Ln();
	 $this->SetFont('Helvetica','B',10);
	 $this->Cell($w[0],10,'E&O.E','LR',0,'L',$fill);
     $this->Cell($w[1],10,'','LR',0,'L',$fill);
     $this->Cell($w[2],10,'','LR',0,'R',$fill);
     $this->Cell($w[3],10,'Grand Total','LR',0,'R',$fill);
	 $this->Cell($w[4],10,number_format($total,0),'LR',0,'R',$fill);
	 $this->Ln();
     $this->Cell(array_sum($w),0,'','T');
	 $this->Ln(5);
	  $this->Cell(0,10,"Amount in words:".$this->number_to_word($total)." shilings",0,array_sum($w)+4);
	  $this->SetFont('Helvetica','B',10);
	  $this->SetTextColor(204,0,0);
	  //$this->SetFillColor(255,255,47);
	   $this->Cell(0,5,"Note:" ,1,0,'L');
	   $this->Ln(5);
	    $this->SetFont('Helvetica','',10);
	    $this->Cell(0,10,"1. Set up shall be a day before the Event" ,2,0);
		$this->Ln(5);
	    $this->Cell(0,10,"2. Cost includes Transport, Delivery, setup and technical support" ,3,0);
		$this->Ln(5);
	    $this->Cell(0,10,"3. Payment within 14 days from the day of that Event with a local purchase order prior" ,4,0);
		$this->Ln(5);
		 //$this->SetFont('Helvetica','',10);
		// $this->SetTextColor(204,0,0);
		// $this->Cell(0,10,"NB: This is a system generated invoice powered by Code 360 Data Solution |+256{0}781587081 " ,0,0);
	  // $this->Ln(5);
		 //$this->Cell(0,10,"4. Payment within 14 days from the data of that Event with a local" ,4,0);
	}
	function headerTable($header,$data,$title,$tin){
		
		$this->SetFont('Times','B',15);
		$this->Ln(15);
		$this->Cell(40);
		$this->Cell(80,10,$title,0,0,'C');
		$this->Ln(10);
		//$this->Cell(10);
		$this->SetTextColor(200);
		$this->SetFont('Courier','B',10);
		$this->Cell(0,10,$tin,0,0,'R');
		$this->Ln(10);
		
    // Colors, line width and bold font
	 $this->Ln(10);
   $this->SetFillColor(255,255,47);
    $this->SetTextColor(0,0,0);
    $this->SetDrawColor(0,0,0);
    $this->SetLineWidth(.05);
    $this->SetFont('Helvetica','B',10);
    // Header
    $w = array(50,60,30,55);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],8,$header[$i],1,0,'L',true);
    $this->Ln();
    // Color and font restoration
    //$this->SetFillColor(204,235,255);
    $this->SetTextColor(0);
    $this->SetFont('Times','B',10);
    // Data
    $fill = false;
	$total=0;
	
    foreach($data as $row)
    {
		$name1='';
		$name2='';
		$nameArray=array();
		$yourName=$row[2];
		$len=strlen($yourName);
		if($len>10)
		{
			$nameArray=explode(' ',$yourName);
			$name1=$nameArray[0];
			
			if(sizeof($nameArray)>2){
				$name2=$nameArray[1]." ".$nameArray[2];	
			}else{
				$name2=$nameArray[1];
			}
		}else
		{
			$name1=$yourName;
		}
        $this->Cell($w[0],6,'Mr/Ms:'.$name1,'LR',0,'L',$fill);
		$this->Cell($w[1],6,$row[3],'LR',0,'L',$fill);
        $this->Cell($w[2],6,$row[9],'LR',0,'L',$fill);
		$this->SetTextColor(255,0,0);
        $this->Cell($w[3],6,$row[1],'LR',0,'L',$fill);
		$this->SetTextColor(0);
		//$this->Cell($w[4],6,'100035','LR',0,'R',$fill);
        $this->Ln();
		$fill = !$fill;
		if($name2!='')
		{
			$this->Cell($w[0],7,$name2,'LR',0,'L',$fill);
			$this->Cell($w[1],7,'','LR',0,'L',$fill);
        	$this->Cell($w[2],7,'','LR',0,'L',$fill);
       		$this->Cell($w[3],7,'','LR',0,'L',$fill);
		
        	$this->Ln();
			$fill = !$fill;
		}
       
		
    }
    // Closing line
	 $this->Cell(array_sum($w),0,'','T');
	
	 
	  
	
	}
	/*
	function loadheader($job){
		require('../utilities/db/dbcon.php');
		$array=array();
		$sql2="select * from tempinvoice where jobid='$job'";
		try{
			$rs=$conn->query($sql2);
			
			foreach($rs as $row){
				array_push($array,$row);
			}
		}catch(PDOException $e){
			echo $e->getMessage();
		}
		return $array;
	}
	
	function loadheader2($job){
		require('../utilities/db/dbcon.php');
		$array=array();
		$sql2="select x1.attn,x1.company,x2.datex,x2.invoiceNo from tbl_porformadetails as x1 , tbl_invoicecreate as x2 where x1.jobid=x2.details and x1.jobid='$job'";
		try{
			$rs=$conn->query($sql2);
			
			foreach($rs as $row){
				array_push($array,$row);
			}
		}catch(PDOException $e){
			echo $e->getMessage();
		}
		return $array;
	}
	
	
*/
    function str_replace_last( $search , $replace , $str ) {
        if( ( $pos = strrpos( $str , $search ) ) !== false ) {
            $search_length  = strlen( $search );
            $str    = substr_replace( $str , $replace , $pos , $search_length );
        }
        return $str;
    }


    function number_to_word( $num) {
        $num    = ( string ) ( ( int ) $num );
       
        if( ( int ) ( $num ) && ctype_digit( $num ) )
        {
            $words  = array( );
           
            $num    = str_replace( array( ',' , ' ' ) , '' , trim( $num ) );
           
            $list1  = array('','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen');
           $list2  = array('','ten','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety','hundred');
           
            $list3  = array('','thousand','million','billion','trillion','quadrillion','quintillion','sextillion','septillion','octillion','nonillion','decillion','undecillion','duodecillion','tredecillion','quattuordecillion','quindecillion','sexdecillion','septendecillion','octodecillion','novemdecillion','vigintillion');
           
            $num_length = strlen( $num );
            $levels = ( int ) ( ( $num_length + 2 ) / 3 );
            $max_length = $levels * 3;
            $num    = substr( '00'.$num , -$max_length );
            $num_levels = str_split( $num , 3 );
           
            foreach( $num_levels as $num_part )
            {
                $levels--;
                $hundreds   = ( int ) ( $num_part / 100 );
                $hundreds   = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds ==1 ? '' : 's' ) . ' ' : '' );
                $tens       = ( int ) ( $num_part % 100 );
                $singles    = '';
               
                if( $tens < 20 )
                {
                    $tens   = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
                }
                else
                {
                    $tens   = ( int ) ( $tens / 10 );
                    $tens   = ' ' . $list2[$tens] . ' ';
                    $singles    = ( int ) ( $num_part % 10 );
                    $singles    = ' ' . $list1[$singles] . ' ';
                }
                $words[]    = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
            }
           
            $commas = count( $words );
           
            if( $commas > 1 )
            {
                $commas = $commas - 1;
            }
           
            $words  = implode( ', ' , $words );
           
            //Some Finishing Touch
            //Replacing multiples of spaces with one space
            $words  = trim( str_replace( ' ,' , ',' , $this->trim_all( ucwords( $words ) ) ) , ', ' );
            if( $commas )
            {
                $words  = $this->str_replace_last( ',' , ' and' , $words );
            }
           
            return $words;
        }
        else if( ! ( ( int ) $num ) )
        {
            return 'Zero';
        }
        return '';
    }

	public function trim_all( $str , $what = NULL , $with = ' ' ){
        if( $what === NULL )
        {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space
           
            $what   = "\\x00-\\x20";    //all white-spaces and control chars
        }
       
        return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
    }
}
/*
$pdf=new Pdf();
//$pdf->AliasNbPages();
$pdf->AddPage();
//$pdf->SetFont('Times','I',12);
$array=array();
$array2=array();
$id=$_REQUEST['id'];
$array=$pdf->loadheader($id);
$array2=$pdf->loadheader2($id);

$header=array('item','qty','rate','No Days','Total');
$header2=array('Invoice TO','Date','InoviceNo');
$pdf->headerTable($header2,$array2);

$pdf->FancyTable($header,$array);

	
$pdf->Output();
*/		

?>