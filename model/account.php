<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Account extends DAO {
    private $account_code;
    private $account_name;
    private $account_type;
    private $analysis_require;
    private $account_balance;
    private $analysis_required;
    private $account_status;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getAccount_code() {
        return $this->account_code;
    }

    public function getAccount_name() {
        return $this->account_name;
    }

    public function getAccount_type() {
        return $this->account_type;
    }

    public function getAnalysis_require() {
        return $this->analysis_require;
    }

    public function getAccount_balance() {
        return $this->account_balance;
    }

    public function getAnalysis_required() {
        return $this->analysis_required;
    }

    public function getAccount_status() {
        return $this->account_status;
    }

    public function setAccount_code($account_code) {
        $this->account_code = $account_code;
    }

    public function setAccount_name($account_name) {
        $this->account_name = $account_name;
    }

    public function setAccount_type($account_type) {
        $this->account_type = $account_type;
    }

    public function setAnalysis_require($analysis_require) {
        $this->analysis_require = $analysis_require;
    }

    public function setAccount_balance($account_balance) {
        $this->account_balance = $account_balance;
    }

    public function setAnalysis_required($analysis_required) {
        $this->analysis_required = $analysis_required;
    }

    public function setAccount_status($account_status) {
        $this->account_status = $account_status;
    }

    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
