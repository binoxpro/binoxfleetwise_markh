<?php 
 require_once('../services/dao.php');
class jci extends DAO{
private $id;
private $details;
private $timeTaken;
private $initials;
private $jobcardId;
private $repairReview;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getdetails()
		{
 		return $this->details;
}

		public function gettimeTaken()
		{
 		return $this->timeTaken;
}

		public function getinitials()
		{
 		return $this->initials;
}

		public function getjobcardId()
		{
 		return $this->jobcardId;
}

		public function getrepairReview()
		{
 		return $this->repairReview;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setdetails($details)
		{
		  $this->details=$details;
		}

		public function settimeTaken($timeTaken)
		{
		  $this->timeTaken=$timeTaken;
		}

		public function setinitials($initials)
		{
		  $this->initials=$initials;
		}

		public function setjobcardId($jobcardId)
		{
		  $this->jobcardId=$jobcardId;
		}

		public function setrepairReview($repairReview)
		{
		  $this->repairReview=$repairReview;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>