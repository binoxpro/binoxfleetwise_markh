<?php 
 require_once('../services/dao.php');
class vehicleNew extends DAO{
        private $id;
        private $regNo;
        private $yom;
        private $modelId;
        private $dop;
        private $chaseNumber;
        private $engineCapacityCC;
        private $vehicleTypeId;
        private $tonnage;
        private $color;
        private $status;
        private $fuel;
        private $purpose;
        private $hasTrailer;
        private $isTrailer;

		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getregNo()
		{
 		return $this->regNo;
}

		public function getyom()
		{
 		return $this->yom;
}

		public function getmodelId()
		{
 		return $this->modelId;
}

		public function getdop()
		{
 		return $this->dop;
}

		public function getchaseNumber()
		{
 		return $this->chaseNumber;
}

		public function getengineCapacityCC()
		{
 		return $this->engineCapacityCC;
}

		public function getvehicleTypeId()
		{
 		return $this->vehicleTypeId;
}

		public function gettonnage()
		{
 		return $this->tonnage;
}

    /**
     * @return mixed
     */
    public function getHasTrailer()
    {
        return $this->hasTrailer;
    }

    /**
     * @param mixed $hasTrailer
     */
    public function setHasTrailer($hasTrailer)
    {
        $this->hasTrailer = $hasTrailer;
    }

    /**
     * @return mixed
     */
    public function getIsTrailer()
    {
        return $this->isTrailer;
    }

    /**
     * @param mixed $isTrailer
     */
    public function setIsTrailer($isTrailer)
    {
        $this->isTrailer = $isTrailer;
    }





    public function getcolor()
    {
        return $this->color;
    }
    public function getstatus()
    {
        return $this->status;
    }
    public function getfuel()
    {
        return $this->fuel;
    }

    public function getpurpose()
    {
        return $this->purpose;
    }

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setregNo($regNo)
		{
		  $this->regNo=$regNo;
		}

		public function setyom($yom)
		{
		  $this->yom=$yom;
		}

		public function setmodelId($modelId)
		{
		  $this->modelId=$modelId;
		}

		public function setdop($dop)
		{
		  $this->dop=$dop;
		}

		public function setchaseNumber($chaseNumber)
		{
		  $this->chaseNumber=$chaseNumber;
		}

		public function setengineCapacityCC($engineCapacityCC)
		{
		  $this->engineCapacityCC=$engineCapacityCC;
		}

		public function setvehicleTypeId($vehicleTypeId)
		{
		  $this->vehicleTypeId=$vehicleTypeId;
		}

		public function settonnage($tonnage)
		{
		  $this->tonnage=$tonnage;
		}

    public function setcolor($color)
    {
        $this->color=$color;
    }

    public function setstatus($status)
    {
        $this->status=$status;
    }
    public function setfuel($fuel)
    {
        $this->fuel=$fuel;
    }
    public function setpurpose($purpose)
    {
        $this->purpose=$purpose;
    }

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>