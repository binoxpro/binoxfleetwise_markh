<?php 
 require_once('../services/dao.php');
class exceptionLog extends DAO{
private $id;
private $message;
private $creationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getmessage(){
 return $this->message;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function setid($id){
  $this->id=$id;
}
public function setmessage($message){
  $this->message=$message;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>