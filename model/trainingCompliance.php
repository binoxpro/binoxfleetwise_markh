<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class TrainingCompliance extends DAO {
    private $id;
    private $trainingId;
    private $iusseDate;
    private $expireDate;
    private $isActive;
    private $parentId;
	private $driverId;
	private $status;
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function gettrainingId() {
        return $this->trainingId;
    }

    public function getiusseDate() {
        return $this->iusseDate;
    }

    public function getexpireDate() {
        return $this->expireDate;
    }

    public function getisActive() {
        return $this->isActive;
    }
	 public function getparentId() {
        return $this->parentId;
    }
	public function getdriverId(){
		return $this->driverId;
	}
	public function getStatus(){
		return $this->status;
	}
	
	
    public function setid($id) {
        $this->id = $id;
    }

    public function settrainingId($trainingId) {
        $this->trainingId = $trainingId;
    }

    public function setIssueDate($issueDate) {
        $this->iusseDate = $issueDate;
    }

    public function setexpireDate($expireDate) {
        $this->expireDate = $expireDate;
    }

    public function setisActive($isActive) {
        $this->isActive = $isActive;
    }
	public function setparentId($parentId) {
        $this->parentId = $parentId;
    }
	
	public function setodometer($odometer){
		$this->odometer=$odometer;
	}
	public function setdriverId($driverId) {
        $this->driverId = $driverId;
    }
	public function setStatus($status) {
        $this->status = $status;
    }
	
	
	


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
