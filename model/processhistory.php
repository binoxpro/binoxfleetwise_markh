<?php 
 require_once('../services/dao.php');
class processhistory extends DAO{
private $id;
private $processId;
private $creationDate;
private $createdBy;
private $completedDate;
private $completedBy;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getprocessId()
		{
 		return $this->processId;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getcreatedBy()
		{
 		return $this->createdBy;
}

		public function getcompletedDate()
		{
 		return $this->completedDate;
}

		public function getcompletedBy()
		{
 		return $this->completedBy;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setprocessId($processId)
		{
		  $this->processId=$processId;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setcreatedBy($createdBy)
		{
		  $this->createdBy=$createdBy;
		}

		public function setcompletedDate($completedDate)
		{
		  $this->completedDate=$completedDate;
		}

		public function setcompletedBy($completedBy)
		{
		  $this->completedBy=$completedBy;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>