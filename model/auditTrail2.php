<?php 
 require_once('services/dao2.php');
class auditTrail extends DAO{
		private $id;
		private $userId;
		private $url;
		private $creationdate;
		private $timeCreation;
		private $Ipadderess;

		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getuserId()
		{
 		return $this->userId;
}

		public function geturl()
		{
 		return $this->url;
}

		public function getcreationdate()
		{
 		return $this->creationdate;
}

		public function gettimeCreation()
		{
 			return $this->timeCreation;
		}

	public function getIpadderess()
	{
		if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
				$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
				return trim($addr[0]);
			} else {
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}
		else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}
		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setuserId($userId)
		{
		  $this->userId=$userId;
		}

		public function seturl($url)
		{
		  $this->url=$url;
		}

		public function setcreationdate($creationdate)
		{
		  $this->creationdate=$creationdate;
		}

		public function settimeCreation($timeCreation)
		{
		  $this->timeCreation=$timeCreation;
		}

		public function setIpadderess($Ipadderess)
		{
		  $this->Ipadderess=$Ipadderess;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>