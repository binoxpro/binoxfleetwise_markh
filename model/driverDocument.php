<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class DriverDocument extends DAO {
    private $id;
    private $employeeId;
    private $documentType;
    private $issueDate;
    private $expiryDate;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getemployeeId() {
        return $this->employeeId;
    }

    public function getdocumentType() {
        return $this->documentType;
    }

    public function getissueDate() {
        return $this->issueDate;
    }

    public function getexpiryDate() {
        return $this->expiryDate;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setemployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    public function setdocumentType($documentType) {
        $this->documentType = $documentType;
    }

    public function setissueDate($issueDate) {
        $this->issueDate = $issueDate;
    }

    public function setexpiryDate($expiryDate) {
        $this->expiryDate = $expiryDate;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
