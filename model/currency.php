<?php 
 require_once('../services/dao.php');
class currency extends DAO{
private $id;
private $symbol;
private $default;
private $rate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getsymbol(){
 return $this->symbol;
}
public function getdefault(){
 return $this->default;
}
public function getrate(){
 return $this->rate;
}
public function setid($id){
  $this->id=$id;
}
public function setsymbol($symbol){
  $this->symbol=$symbol;
}
public function setdefault($default){
  $this->default=$default;
}
public function setrate($rate){
  $this->rate=$rate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>