<?php 
 require_once('../services/dao.php');
class lopdelivery extends DAO{
private $id;
private $purfItemId;
private $quantity;
private $passed1;
private $passed2;
private $deliveryDate;
private $taxNo;
private $deliveryNo;
private $recordedBy;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getpurfItemId()
		{
 		return $this->purfItemId;
}

		public function getquantity()
		{
 		return $this->quantity;
}

		public function getpassed1()
		{
 		return $this->passed1;
}

		public function getpassed2()
		{
 		return $this->passed2;
}

		public function getdeliveryDate()
		{
 		return $this->deliveryDate;
}

		public function gettaxNo()
		{
 		return $this->taxNo;
}

		public function getdeliveryNo()
		{
 		return $this->deliveryNo;
}

		public function getrecordedBy()
		{
 		return $this->recordedBy;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setpurfItemId($purfItemId)
		{
		  $this->purfItemId=$purfItemId;
		}

		public function setquantity($quantity)
		{
		  $this->quantity=$quantity;
		}

		public function setpassed1($passed1)
		{
		  $this->passed1=$passed1;
		}

		public function setpassed2($passed2)
		{
		  $this->passed2=$passed2;
		}

		public function setdeliveryDate($deliveryDate)
		{
		  $this->deliveryDate=$deliveryDate;
		}

		public function settaxNo($taxNo)
		{
		  $this->taxNo=$taxNo;
		}

		public function setdeliveryNo($deliveryNo)
		{
		  $this->deliveryNo=$deliveryNo;
		}

		public function setrecordedBy($recordedBy)
		{
		  $this->recordedBy=$recordedBy;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>