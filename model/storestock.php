<?php 
 require_once('../services/dao.php');
class storestock extends DAO{
private $id;
private $itemId;
private $quantity;
private $unitPrice;
private $stocktype;
private $creationDate;
private $recordedBy;
private $receivedBy;
private $supplierId;
private $checkedBy;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getitemId()
		{
 		return $this->itemId;
}

		public function getquantity()
		{
 		return $this->quantity;
}

		public function getunitPrice()
		{
 		return $this->unitPrice;
}

		public function getstocktype()
		{
 		return $this->stocktype;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getrecordedBy()
		{
 		return $this->recordedBy;
}

		public function getreceivedBy()
		{
 		return $this->receivedBy;
}

		public function getsupplierId()
		{
 		return $this->supplierId;
}

		public function getcheckedBy()
		{
 		return $this->checkedBy;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setitemId($itemId)
		{
		  $this->itemId=$itemId;
		}

		public function setquantity($quantity)
		{
		  $this->quantity=$quantity;
		}

		public function setunitPrice($unitPrice)
		{
		  $this->unitPrice=$unitPrice;
		}

		public function setstocktype($stocktype)
		{
		  $this->stocktype=$stocktype;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setrecordedBy($recordedBy)
		{
		  $this->recordedBy=$recordedBy;
		}

		public function setreceivedBy($receivedBy)
		{
		  $this->receivedBy=$receivedBy;
		}

		public function setsupplierId($supplierId)
		{
		  $this->supplierId=$supplierId;
		}

		public function setcheckedBy($checkedBy)
		{
		  $this->checkedBy=$checkedBy;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>