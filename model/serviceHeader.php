<?php 
 require_once('../services/dao.php');
class serviceHeader extends DAO{
private $id;
private $creationDate;
private $serviceIntervalId;
private $kmReading;
private $nextReading;
private $reminderReading;
private $isActive;
private $vehicleId;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function getserviceIntervalId(){
 return $this->serviceIntervalId;
}
public function getkmReading(){
 return $this->kmReading;
}
public function getnextReading(){
 return $this->nextReading;
}
public function getreminderReading(){
 return $this->reminderReading;
}
public function getisActive(){
 return $this->isActive;
}
public function getvehicleId(){
 return $this->vehicleId;
}
public function setid($id){
  $this->id=$id;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function setserviceIntervalId($serviceIntervalId){
  $this->serviceIntervalId=$serviceIntervalId;
}
public function setkmReading($kmReading){
  $this->kmReading=$kmReading;
}
public function setnextReading($nextReading){
  $this->nextReading=$nextReading;
}
public function setreminderReading($reminderReading){
  $this->reminderReading=$reminderReading;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setvehicleId($vehicleId){
  $this->vehicleId=$vehicleId;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>