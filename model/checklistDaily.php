<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class ChecklistDaily extends DAO {
    private $id;
    private $sectionitemId;
    private $status;
    private $checklistId;
    private $vehicleId;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getsectionitemId() {
        return $this->sectionitemId;
    }

    public function getstatus() {
        return $this->status;
    }

    public function getchecklistId() {
        return $this->checklistId;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setsectionitemId($sectionitemId) {
        $this->sectionitemId = $sectionitemId;
    }

    public function setstatus($status) {
        $this->status = $status;
    }

    public function setchecklistId($checklistId) {
        $this->checklistId = $checklistId;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
