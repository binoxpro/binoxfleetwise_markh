<?php
include_once('../services/DAO.php');
class Spare extends DAO{
	
	private $id;
	private $numberPlate;
	private $dateOfRepair;
	private $typeOfRepair;
	private $sparePart;
	private $cost;
	private $comment;
	
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
        public function getid() {
            return $this->id;
        }

        public function getNumberPlate() {
            return $this->numberPlate;
        }

        public function getdateOfRepair() {
            return $this->dateOfRepair;
        }

        public function gettypeOfRepair() {
            return $this->typeOfRepair;
        }

       
		public function getsparePart() {
            return $this->sparePart;
        }
		public function getcost() {
            return $this->cost;
        }
		public function getcomment() {
            return $this->comment;
        }
		
        public function setid($id) {
            $this->id = $id;
        }

        public function setNumberPlate($numberPlate) {
            $this->numberPlate = $numberPlate;
        }

        public function setdateOfRepair($dateOfRepair) {
            $this->dateOfRepair = $dateOfRepair;
        }

        public function settypeOfRepair($typeOfRepair) {
            $this->typeOfRepair = $typeOfRepair;
        }

        
		public function setsparePart($sparePart){
			$this->sparePart=$sparePart;
		}
		public function setcost($cost){
			$this->cost=$cost;
		}
		public function setcomment($comment){
			$this->comment=$comment;
		}
		
        public function save(){
		 
	}
    public function delete(){
		
	}
    public function update(){
		
	}
    public function view(){
		
	}
    public function view_query($sql){
		
	}

}

?>