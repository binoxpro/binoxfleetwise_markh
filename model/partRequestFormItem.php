<?php 
 require_once('../services/dao.php');
class partRequestFormItem extends DAO{
private $id;
private $itemId;
private $itemDescription;
private $uomId;
private $quantity;
private $quantityIssued;
private $dateIssued;
private $issuedBy;
private $receviedBy;
private $comment;
private $status;
private $prfId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getitemId()
		{
 		return $this->itemId;
}

		public function getitemDescription()
		{
 		return $this->itemDescription;
}

		public function getuomId()
		{
 		return $this->uomId;
}

		public function getquantity()
		{
 		return $this->quantity;
}

		public function getquantityIssued()
		{
 		return $this->quantityIssued;
}

		public function getdateIssued()
		{
 		return $this->dateIssued;
}

		public function getissuedBy()
		{
 		return $this->issuedBy;
}

		public function getreceviedBy()
		{
 		return $this->receviedBy;
}

		public function getcomment()
		{
 		return $this->comment;
}

		public function getstatus()
		{
 		return $this->status;
}

		public function getprfId()
		{
 		return $this->prfId;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setitemId($itemId)
		{
		  $this->itemId=$itemId;
		}

		public function setitemDescription($itemDescription)
		{
		  $this->itemDescription=$itemDescription;
		}

		public function setuomId($uomId)
		{
		  $this->uomId=$uomId;
		}

		public function setquantity($quantity)
		{
		  $this->quantity=$quantity;
		}

		public function setquantityIssued($quantityIssued)
		{
		  $this->quantityIssued=$quantityIssued;
		}

		public function setdateIssued($dateIssued)
		{
		  $this->dateIssued=$dateIssued;
		}

		public function setissuedBy($issuedBy)
		{
		  $this->issuedBy=$issuedBy;
		}

		public function setreceviedBy($receviedBy)
		{
		  $this->receviedBy=$receviedBy;
		}

		public function setcomment($comment)
		{
		  $this->comment=$comment;
		}

		public function setstatus($status)
		{
		  $this->status=$status;
		}

		public function setprfId($prfId)
		{
		  $this->prfId=$prfId;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>