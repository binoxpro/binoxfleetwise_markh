<?php 
 require_once('../services/dao.php');
class tireMaintenanceCost extends DAO{
private $id;
private $tireId;
private $cost;
private $details;
private $creationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettireId(){
 return $this->tireId;
}
public function getcost(){
 return $this->cost;
}
public function getdetails(){
 return $this->details;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function setid($id){
  $this->id=$id;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function setcost($cost){
  $this->cost=$cost;
}
public function setdetails($details){
  $this->details=$details;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>