<?php 
 require_once('../services/dao.php');
class workOrder extends DAO{
	private $id;
	private $tripNo;
	private $townFrom;
	private $townTo;
	private $distance;
	private $DeliveryNo;
	private $PONo;
	private $Quantity;
	private $Bags;
	private $Rate;
	private $Amount;
	private $product;
	private $CustomerId;
	private $Claimed;
	private $WhtApplied;
	private $WhtValue;
	private $CreationDate;
	private $IsActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function gettripNo()
		{
 		return $this->tripNo;
}

		public function getDeliveryNo()
		{
 		return $this->DeliveryNo;
}

		public function getPONo()
		{
 		return $this->PONo;
}

		public function getQuantity()
		{
 		return $this->Quantity;
}

		public function getBags()
		{
 		return $this->Bags;
}

		public function getRate()
		{
 		return $this->Rate;
}

		public function getAmount()
		{
 		return $this->Amount;
}

		public function getCustomerId()
		{
 		return $this->CustomerId;
}

		public function getClaimed()
		{
 		return $this->Claimed;
}

		public function getWhtApplied()
		{
 		return $this->WhtApplied;
}

		public function getWhtValue()
		{
 		return $this->WhtValue;
}

		public function getCreationDate()
		{
 		return $this->CreationDate;
}

		public function getIsActive()
		{
 		return $this->IsActive;
}
		public function getProduct()
		{
			return $this->product;
		}

		public function gettownFrom()
		{
			return $this->townFrom;
		}

		public function gettownTo()
		{
			return $this->townTo;
		}

		public function getDistance()
		{
			return $this->distance;
		}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function settripNo($tripNo)
		{
		  $this->tripNo=$tripNo;
		}

		public function setDeliveryNo($DeliveryNo)
		{
		  $this->DeliveryNo=$DeliveryNo;
		}

		public function setPONo($PONo)
		{
		  $this->PONo=$PONo;
		}

		public function setQuantity($Quantity)
		{
		  $this->Quantity=$Quantity;
		}

		public function setBags($Bags)
		{
		  $this->Bags=$Bags;
		}

		public function setRate($Rate)
		{
		  $this->Rate=$Rate;
		}

		public function setAmount($Amount)
		{
		  $this->Amount=$Amount;
		}

		public function setCustomerId($CustomerId)
		{
		  $this->CustomerId=$CustomerId;
		}

		public function setClaimed($Claimed)
		{
		  $this->Claimed=$Claimed;
		}

		public function setWhtApplied($WhtApplied)
		{
		  $this->WhtApplied=$WhtApplied;
		}

		public function setWhtValue($WhtValue)
		{
		  $this->WhtValue=$WhtValue;
		}

		public function setCreationDate($CreationDate)
		{
		  $this->CreationDate=$CreationDate;
		}

		public function setIsActive($IsActive)
		{
		  $this->IsActive=$IsActive;
		}

		public function setProduct($product)
		{
			$this->product=$product;
		}

		public function settownFrom($townFrom)
		{
			$this->townFrom=$townFrom;
		}

		public function settownTo($townTo)
		{
			$this->townTo=$townTo;
		}
		public function setDistance($distance)
		{
			$this->distance=$distance;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>