<?php 
 require_once('../services/dao.php');
class retreated extends DAO{
private $id;
private $stockId;
private $kmDone;
private $expectedKm;
private $cost;
private $creationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getstockId(){
 return $this->stockId;
}
public function getkmDone(){
 return $this->kmDone;
}
public function getexpectedKm(){
 return $this->expectedKm;
}
public function getcost(){
 return $this->cost;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function setid($id){
  $this->id=$id;
}
public function setstockId($stockId){
  $this->stockId=$stockId;
}
public function setkmDone($kmDone){
  $this->kmDone=$kmDone;
}
public function setexpectedKm($expectedKm){
  $this->expectedKm=$expectedKm;
}
public function setcost($cost){
  $this->cost=$cost;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>