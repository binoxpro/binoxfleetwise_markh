<?php 
 require_once('../services/dao.php');
class employee extends DAO{
	private $empNo;
	private $firstName;
	private $lastName;
	private $sex;
	private $martialStatus;
	private $dob;
	private $town;
	private $district;
	private $nationID;
	private $mobile1;
	private $mobile2;
	private $nikname;
	private $contact;
	private  $photo;
		function __construct()
		{
 		parent::__construct();
		}

		public function getempNo()
		{
 		return $this->empNo;
}

		public function getfirstName()
		{
 		return $this->firstName;
}

		public function getlastName()
		{
 		return $this->lastName;
}

		public function getsex()
		{
 		return $this->sex;
}

		public function getmartialStatus()
		{
 		return $this->martialStatus;
}

		public function getdob()
		{
 		return $this->dob;
}

		public function gettown()
		{
 		return $this->town;
}

		public function getdistrict()
		{
 		return $this->district;
}

		public function getnationID()
		{
 		return $this->nationID;
}

		public function getmobile1()
		{
 		return $this->mobile1;
}

		public function getmobile2()
		{
 		return $this->mobile2;
}

		public function getnikname()
		{
 		return $this->nikname;
}

		public function getcontact()
		{
 		return $this->contact;
}

		public function setempNo($empNo)
		{
		  $this->empNo=$empNo;
		}

		public function getphoto()
		{
			return $this->photo;
		}

		public function setfirstName($firstName)
		{
		  $this->firstName=$firstName;
		}

		public function setlastName($lastName)
		{
		  $this->lastName=$lastName;
		}

		public function setsex($sex)
		{
		  $this->sex=$sex;
		}

		public function setmartialStatus($martialStatus)
		{
		  $this->martialStatus=$martialStatus;
		}

		public function setdob($dob)
		{
		  $this->dob=$dob;
		}

		public function settown($town)
		{
		  $this->town=$town;
		}

		public function setdistrict($district)
		{
		  $this->district=$district;
		}

		public function setnationID($nationID)
		{
		  $this->nationID=$nationID;
		}

		public function setmobile1($mobile1)
		{
		  $this->mobile1=$mobile1;
		}

		public function setmobile2($mobile2)
		{
		  $this->mobile2=$mobile2;
		}

		public function setnikname($nikname)
		{
		  $this->nikname=$nikname;
		}

		public function setcontact($contact)
		{
		  $this->contact=$contact;
		}
	public function setphoto($photo)
	{
		$this->photo=$photo;
	}

		public function save()
		{
		}

		public function update()
		{
		}
		public function view()
		{
		}
		public function delete()
		{
		}
		public function view_query($sql)
		{
		}
} 
 ?>