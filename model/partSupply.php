<?php 
 require_once('../services/dao.php');
class partSupply extends DAO{
private $id;
private $invoiceNo;
private $deliveryNoteNo;
private $deliveryDate;
private $partUsedId;
private $qty;
private $unitprice;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getinvoiceNo()
		{
 		return $this->invoiceNo;
}

		public function getdeliveryNoteNo()
		{
 		return $this->deliveryNoteNo;
}

		public function getdeliveryDate()
		{
 		return $this->deliveryDate;
}

		public function getpartUsedId()
		{
 		return $this->partUsedId;
}

		public function getqty()
		{
 		return $this->qty;
}

		public function getunitprice()
		{
 		return $this->unitprice;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setinvoiceNo($invoiceNo)
		{
		  $this->invoiceNo=$invoiceNo;
		}

		public function setdeliveryNoteNo($deliveryNoteNo)
		{
		  $this->deliveryNoteNo=$deliveryNoteNo;
		}

		public function setdeliveryDate($deliveryDate)
		{
		  $this->deliveryDate=$deliveryDate;
		}

		public function setpartUsedId($partUsedId)
		{
		  $this->partUsedId=$partUsedId;
		}

		public function setqty($qty)
		{
		  $this->qty=$qty;
		}

		public function setunitprice($unitprice)
		{
		  $this->unitprice=$unitprice;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>