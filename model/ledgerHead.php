<?php
session_start();
 require_once('../services/dao.php');
class ledgerHead extends DAO{
private $id;
private $transactionDate;
private $monthId;
private $amount;
private $ipAddress;
private $userId;
private $postingDate;
private $postingTime;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function gettransactionDate()
		{
 		return $this->transactionDate;
}

		public function getmonthId()
		{
 		return $this->monthId;
}

		public function getamount()
		{
 		return $this->amount;
}

		public function getipAddress()
		{
			if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
				if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
					$addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
					return trim($addr[0]);
				} else {
					return $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
			}
			else {
				return $_SERVER['REMOTE_ADDR'];
			}
		}

		public function getuserId()
		{
 		return $_SESSION['userId'];
}

		public function getpostingDate()
		{
 		return date('Y-m-d');
}

		public function getpostingTime()
		{
 		return date('H:i:s');
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function settransactionDate($transactionDate)
		{
		  $this->transactionDate=$transactionDate;
		}

		public function setmonthId($monthId)
		{
		  $this->monthId=$monthId;
		}

		public function setamount($amount)
		{
		  $this->amount=$amount;
		}

		public function setipAddress($ipAddress)
		{
		  $this->ipAddress=$ipAddress;
		}

		public function setuserId($userId)
		{
		  $this->userId=$userId;
		}

		public function setpostingDate($postingDate)
		{
		  $this->postingDate=$postingDate;
		}

		public function setpostingTime($postingTime)
		{
		  $this->postingTime=$postingTime;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>