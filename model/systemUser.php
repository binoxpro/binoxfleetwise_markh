<?php 
 require_once('../services/dao.php');
class systemUser extends DAO{
private $id;
private $firstName;
private $lastName;
private $dob;
private $contact;
private $email;
private $isActive;
private $username;
private $password;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getfirstName()
		{
 		return $this->firstName;
}

		public function getlastName()
		{
 		return $this->lastName;
}

		public function getdob()
		{
 		return $this->dob;
}

		public function getcontact()
		{
 		return $this->contact;
}

		public function getemail()
		{
 		return $this->email;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function getusername()
		{
 		return $this->username;
}

		public function getpassword()
		{
 		return $this->password;
}



	public function setid($id)
		{
		  $this->id=$id;
		}

		public function setfirstName($firstName)
		{
		  $this->firstName=$firstName;
		}

		public function setlastName($lastName)
		{
		  $this->lastName=$lastName;
		}

		public function setdob($dob)
		{
		  $this->dob=$dob;
		}

		public function setcontact($contact)
		{
		  $this->contact=$contact;
		}

		public function setemail($email)
		{
		  $this->email=$email;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function setusername($username)
		{
		  $this->username=$username;
		}

		public function setpassword($password)
		{
		  $this->password=$password;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>