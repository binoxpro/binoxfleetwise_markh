<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Tyre extends DAO {
    private $id;
    private $vehicleId;
    private $tyretypeId;
	private $tyreName;
	private $costTyre;
    private $fixon;
    private $fixDate;
	private $removalon;
	private $isActive;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function gettyretypeId() {
        return $this->tyretypeId;
    }

    public function getfixon() {
        return $this->fixon;
    }

    public function getfixDate() {
        return $this->fixDate;
    }
	public function getremovalon() {
        return $this->removalon;
    }
	public function getisActive(){
		return $this->isActive;
	}
	
	public function getTyreName(){
		return $this->tyreName;
	}
	
	public function getCostTyre(){
		return $this->costTyre;
	}

    public function setid($id) {
        $this->id = $id;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }

    public function settyretypeId($tyretypeId) {
        $this->tyretypeId = $tyretypeId;
    }

    public function setfixon($fixon) {
        $this->fixon = $fixon;
    }

    public function setfixDate($fixDate) {
        $this->fixDate = $fixDate;
    }
	public function setremovalon($removalon) {
        $this->removalon = $removalon;
    }
	public function setisActive($isActive){
		$this->isActive=$isActive;
	}
	
	public function setTyreName($tyreName){
		$this->tyreName=$tyreName;
	}
	
	public function setCostTyre($costTyre){
		$this->costTyre=$costTyre;
	}


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
