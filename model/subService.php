<?php 
 require_once('../services/dao.php');
class subService extends DAO{
private $id;
private $componentScheduleId;
private $serviceHeaderId;
private $comment;
private $done;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getcomponentScheduleId(){
 return $this->componentScheduleId;
}
public function getserviceHeaderId(){
 return $this->serviceHeaderId;
}
public function getcomment(){
 return $this->comment;
}
public function getdone(){
 return $this->done;
}
public function setid($id){
  $this->id=$id;
}
public function setcomponentScheduleId($componentScheduleId){
  $this->componentScheduleId=$componentScheduleId;
}
public function setserviceHeaderId($serviceHeaderId){
  $this->serviceHeaderId=$serviceHeaderId;
}
public function setcomment($comment){
  $this->comment=$comment;
}
public function setdone($done){
  $this->done=$done;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>