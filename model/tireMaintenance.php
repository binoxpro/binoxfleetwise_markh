<?php 
 require_once('../services/dao.php');
class tireMaintenance extends DAO{
private $id;
private $tireId;
private $conditionM;
private $comment;
private $creationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettireId(){
 return $this->tireId;
}
public function getconditionM(){
 return $this->conditionM;
}
public function getcomment(){
 return $this->comment;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function setid($id){
  $this->id=$id;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function setconditionM($conditionM){
  $this->conditionM=$conditionM;
}
public function setcomment($comment){
  $this->comment=$comment;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>