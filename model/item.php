<?php 
 require_once('../services/dao.php');
class item extends DAO{
private $id;
private $partNo;
private $serialNo;
private $itemName;
private $itemCategoryId;
private $uomId;
private $levelAlert;
private $bin;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getpartNo()
		{
 		return $this->partNo;
}

		public function getserialNo()
		{
 		return $this->serialNo;
}

		public function getitemName()
		{
 		return $this->itemName;
}

		public function getitemCategoryId()
		{
 		return $this->itemCategoryId;
}

		public function getuomId()
		{
 		return $this->uomId;
}

		public function getlevelAlert()
		{
 		return $this->levelAlert;
}

		public function getbin()
		{
 		return $this->bin;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setpartNo($partNo)
		{
		  $this->partNo=$partNo;
		}

		public function setserialNo($serialNo)
		{
		  $this->serialNo=$serialNo;
		}

		public function setitemName($itemName)
		{
		  $this->itemName=$itemName;
		}

		public function setitemCategoryId($itemCategoryId)
		{
		  $this->itemCategoryId=$itemCategoryId;
		}

		public function setuomId($uomId)
		{
		  $this->uomId=$uomId;
		}

		public function setlevelAlert($levelAlert)
		{
		  $this->levelAlert=$levelAlert;
		}

		public function setbin($bin)
		{
		  $this->bin=$bin;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>