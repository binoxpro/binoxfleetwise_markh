<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class PartsUsed extends DAO {
    private $id;
    private $partusedId;
    private $quantity;
    private $cost;
    private $jobcardId;
	private $dateofrepair;
	private $typeofrepair;
	private $numberPlate;
    private $supplierId;
    private $supplied;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getpartused() {
        return $this->partusedId;
    }

    public function getquantity() {
        return $this->quantity;
    }

    public function getcost() {
        return $this->cost;
    }

    public function getjobcardId() {
        return $this->jobcardId;
    }
	
	public function getDateofRepair() {
        return $this->dateofrepair;
    }
	public function gettypeofRepair() {
        return $this->typeofrepair;
    }
	
	public function getnumberPlate() {
        return $this->numberPlate;
    }

    public function getsupplierId() {
        return $this->supplierId;
    }
    public function getsupplied()
    {
        return $this->supplied;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setpartused($partusedId) {
        $this->partusedId = $partusedId;
    }

    public function setquantity($quantity) {
        $this->quantity = $quantity;
    }

    public function setcost($cost) {
        $this->cost = $cost;
    }

    public function setjobcardId($jobcardId) {
        $this->jobcardId = $jobcardId;
    }
	
	public function setDateOfRepair($dateOfRepair) {
        $this->dateofrepair = $dateOfRepair;
    }
	public function setTypeOfRepair($typeOfRepair) {
        $this->typeofrepair= $typeOfRepair;
    }
	
	public function setnumberPlate($numberPlate) {
        $this->numberPlate = $numberPlate;
    }
    public function setsupplierId($supplierId) {
        $this->supplierId= $supplierId;
    }
    public function setsupplied($supplied) {
        $this->supplied= $supplied;
    }
	


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
