<?php 
 require_once('../services/dao.php');
class tireWear extends DAO{
private $id;
private $tireWear;
private $comment;
private $CreationDate;
private $tireId;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettireWear(){
 return $this->tireWear;
}
public function getcomment(){
 return $this->comment;
}
public function getCreationDate(){
 return $this->CreationDate;
}
public function gettireId(){
 return $this->tireId;
}
public function setid($id){
  $this->id=$id;
}
public function settireWear($tireWear){
  $this->tireWear=$tireWear;
}
public function setcomment($comment){
  $this->comment=$comment;
}
public function setCreationDate($CreationDate){
  $this->CreationDate=$CreationDate;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>