<?php 
 require_once('../services/dao.php');
class fuelOrder extends DAO{
private $id;
private $orderNo;
private $tripNo;
private $invoiceNo;
private $creationDate;
private $paid;
private $checked;
private $posted;
private $supplierId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getorderNo()
		{
 		return $this->orderNo;
}

		public function gettripNo()
		{
 		return $this->tripNo;
}

		public function getinvoiceNo()
		{
 		return $this->invoiceNo;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getpaid()
		{
 		return $this->paid;
}

		public function getchecked()
		{
 		return $this->checked;
}

		public function getposted()
		{
 		return $this->posted;
}

		public function getsupplierId()
		{
 		return $this->supplierId;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setorderNo($orderNo)
		{
		  $this->orderNo=$orderNo;
		}

		public function settripNo($tripNo)
		{
		  $this->tripNo=$tripNo;
		}

		public function setinvoiceNo($invoiceNo)
		{
		  $this->invoiceNo=$invoiceNo;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setpaid($paid)
		{
		  $this->paid=$paid;
		}

		public function setchecked($checked)
		{
		  $this->checked=$checked;
		}

		public function setposted($posted)
		{
		  $this->posted=$posted;
		}

		public function setsupplierId($supplierId)
		{
		  $this->supplierId=$supplierId;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>