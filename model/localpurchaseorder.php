<?php 
 require_once('../services/dao.php');
class localpurchaseorder extends DAO{
private $id;
private $supplieId;
private $creationDate;
private $createdBy;
private $prfId;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getsupplieId()
		{
 		return $this->supplieId;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getcreatedBy()
		{
 		return $this->createdBy;
}

		public function getprfId()
		{
 		return $this->prfId;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setsupplieId($supplieId)
		{
		  $this->supplieId=$supplieId;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setcreatedBy($createdBy)
		{
		  $this->createdBy=$createdBy;
		}

		public function setprfId($prfId)
		{
		  $this->prfId=$prfId;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>