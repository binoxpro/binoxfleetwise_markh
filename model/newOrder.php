<?php 
 require_once('../services/dao.php');
class newOrder extends DAO{
private $id;
private $customerName;
private $orderDate;
private $loadDate;
private $orderNo;
private $ulg;
private $ago;
private $bik;
private $vpower;
private $depotTimeIn;
private $depotTimeOut;
private $vehicleId;
private $deliveryDate;
private $deliveryTime;
private $tripNo;
private $Revenue;
private $Expense;
private $totalProduct;
private $systDate;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getcustomerName(){
 return $this->customerName;
}
public function getorderDate(){
 return $this->orderDate;
}
public function getloadDate(){
 return $this->loadDate;
}
public function getorderNo(){
 return $this->orderNo;
}
public function getulg(){
 return $this->ulg;
}
public function getago(){
 return $this->ago;
}
public function getbik(){
 return $this->bik;
}
public function getvpower(){
 return $this->vpower;
}
public function getdepotTimeIn(){
 return $this->depotTimeIn;
}
public function getdepotTimeOut(){
 return $this->depotTimeOut;
}
public function getvehicleId(){
 return $this->vehicleId;
}
public function getdeliveryDate(){
 return $this->deliveryDate;
}
public function getdeliveryTime(){
 return $this->deliveryTime;
}
public function gettripNo(){
 return $this->tripNo;
}
public function getRevenue(){
 return $this->Revenue;
}
public function getExpense(){
 return $this->Expense;
}
public function gettotalProduct(){
 return $this->totalProduct;
}
public function getsystDate(){
 return $this->systDate;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setcustomerName($customerName){
  $this->customerName=$customerName;
}
public function setorderDate($orderDate){
  $this->orderDate=$orderDate;
}
public function setloadDate($loadDate){
  $this->loadDate=$loadDate;
}
public function setorderNo($orderNo){
  $this->orderNo=$orderNo;
}
public function setulg($ulg){
  $this->ulg=$ulg;
}
public function setago($ago){
  $this->ago=$ago;
}
public function setbik($bik){
  $this->bik=$bik;
}
public function setvpower($vpower){
  $this->vpower=$vpower;
}
public function setdepotTimeIn($depotTimeIn){
  $this->depotTimeIn=$depotTimeIn;
}
public function setdepotTimeOut($depotTimeOut){
  $this->depotTimeOut=$depotTimeOut;
}
public function setvehicleId($vehicleId){
  $this->vehicleId=$vehicleId;
}
public function setdeliveryDate($deliveryDate){
  $this->deliveryDate=$deliveryDate;
}
public function setdeliveryTime($deliveryTime){
  $this->deliveryTime=$deliveryTime;
}
public function settripNo($tripNo){
  $this->tripNo=$tripNo;
}
public function setRevenue($Revenue){
  $this->Revenue=$Revenue;
}
public function setExpense($Expense){
  $this->Expense=$Expense;
}
public function settotalProduct($totalProduct){
  $this->totalProduct=$totalProduct;
}
public function setsystDate($systDate){
  $this->systDate=$systDate;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>