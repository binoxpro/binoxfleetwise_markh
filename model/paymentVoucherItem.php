<?php 
 require_once('../services/dao.php');
class paymentVoucherItem extends DAO{
private $id;
private $payVoucherId;
private $particular;
private $amount;
private $accountCode;
private $individualCode;
private $costCenter;
private $tripNo;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getpayVoucherId()
		{
 		return $this->payVoucherId;
}

		public function getparticular()
		{
 		return $this->particular;
}

		public function getamount()
		{
 		return $this->amount;
}

		public function getaccountCode()
		{
 		return $this->accountCode;
}

		public function getindividualCode()
		{
 		return $this->individualCode;
}

		public function getcostCenter()
		{
 		return $this->costCenter;
}

		public function gettripNo()
		{
 		return $this->tripNo;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setpayVoucherId($payVoucherId)
		{
		  $this->payVoucherId=$payVoucherId;
		}

		public function setparticular($particular)
		{
		  $this->particular=$particular;
		}

		public function setamount($amount)
		{
		  $this->amount=$amount;
		}

		public function setaccountCode($accountCode)
		{
		  $this->accountCode=$accountCode;
		}

		public function setindividualCode($individualCode)
		{
		  $this->individualCode=$individualCode;
		}

		public function setcostCenter($costCenter)
		{
		  $this->costCenter=$costCenter;
		}

		public function settripNo($tripNo)
		{
		  $this->tripNo=$tripNo;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>