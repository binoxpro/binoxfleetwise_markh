<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class OtherCostManagement extends DAO {
    private $id;
    private $description;
	private $amount;
	private $creationDate;
    private $vehicleId;
	private $destination;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }
	public function getothercostId() {
        return $this->othercostId;
    }
	public function getamount() {
        return $this->amount;
    }
	public function getCreationDate() {
        return $this->creationDate;
    }
	
	public function getDestination() {
        return $this->destination;
    }
	
	public function getDescription() {
        return $this->description;
    }

    public function setId($id) {
        $this->id = $id;
    }
	public function setothercostId($othercostId) {
        $this->othercostId = $othercostId;
    }
	
	public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }
	
	public function setamount($amount) {
        $this->amount=$amount;
    }
	
	public function setCreationDate($creationDate) {
        $this->creationDate=$creationDate;
    }
	
	public function setDescription($description) {
        $this->description=$description;
    }
	
	public function setDestination($destination) {
        $this->destination=$destination;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
