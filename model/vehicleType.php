<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class VehicleType extends DAO {
    private $id;
    private $vehicleType;
   
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getvehicleType() {
        return $this->vehicleType;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setvehicleType($vehicleType) {
        $this->vehicleType = $vehicleType;
    }
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
