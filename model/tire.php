<?php 
 require_once('../services/dao.php');
class tire extends DAO{
private $id;
private $serialNumber;
private $tireBand;
private $tirePatternId;
private $tireSizeId;
private $retreadDepth;
private $maxmiumPressure;
private $tireType;
private $cost;
private $kms;
private $status;
private $isActive;
private $parentId;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getserialNumber(){
 return $this->serialNumber;
}
public function gettireBand(){
 return $this->tireBand;
}
public function gettirePatternId(){
 return $this->tirePatternId;
}
public function gettireSizeId(){
 return $this->tireSizeId;
}
public function getretreadDepth(){
 return $this->retreadDepth;
}
public function getmaxmiumPressure(){
 return $this->maxmiumPressure;
}
public function gettireType(){
 return $this->tireType;
}
public function getcost(){
 return $this->cost;
}
public function getkms(){
 return $this->kms;
}
public function getstatus(){
 return $this->status;
}
public function getisActive(){
 return $this->isActive;
}
public function getParentId(){
 return $this->parentId;
}
public function setid($id){
  $this->id=$id;
}
public function setserialNumber($serialNumber){
  $this->serialNumber=$serialNumber;
}
public function settireBand($tireBand){
  $this->tireBand=$tireBand;
}
public function settirePatternId($tirePatternId){
  $this->tirePatternId=$tirePatternId;
}
public function settireSizeId($tireSizeId){
  $this->tireSizeId=$tireSizeId;
}
public function setretreadDepth($retreadDepth){
  $this->retreadDepth=$retreadDepth;
}
public function setmaxmiumPressure($maxmiumPressure){
  $this->maxmiumPressure=$maxmiumPressure;
}
public function settireType($tireType){
  $this->tireType=$tireType;
}
public function setcost($cost){
  $this->cost=$cost;
}
public function setkms($kms){
  $this->kms=$kms;
}
public function setstatus($status){
  $this->status=$status;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setParentId($parentId){
  $this->parentId=$parentId;
}

public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>