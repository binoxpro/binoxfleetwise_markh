<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Section extends DAO {
    private $id;
    private $name;
	private $checkListTypeId;
    private $isActive;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getisActive() {
        return $this->isActive;
    }
	public function getname() {
        return $this->name;
    }
	public function getCheckListTypeId() {
        return $this->checkListTypeId;
    }

    public function setid($id) {
        $this->id = $id;
    }
	public function setname($name) {
        $this->name = $name;
    }
	
	public function setisActive($isActive) {
        $this->isActive = $isActive;
    }
	
	public function setCheckListTypeId($checkListTypeId) {
        $this->checkListTypeId=$checkListTypeId;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
