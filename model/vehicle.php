<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Vehicle extends DAO {
    private $id;
    private $regNo;
    private $configuration;
    private $model;
    private $yom;
	private $chaseNumber;
    private $countryMake;
	private $vehicletypeId;
	private $capacity;
	//private $
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getregNo() {
        return $this->regNo;
    }

    public function getconfiguration() {
        return $this->configuration;
    }

    public function getmodel() {
        return $this->model;
    }

    public function getaxel() {
        return $this->axel;
    }
	
	public function getcapacity() {
        return $this->capacity;
    }
	
	public function getcardNumber() {
        return $this->cardNumber;
    }
	public function getcountryMake() {
        return $this->countryMake;
    }
	public function getvehicletypeId() {
        return $this->vehicletypeId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setregNo($regNo) {
        $this->regNo = $regNo;
    }

    public function setconfiguration($configuration) {
        $this->configuration = $configuration;
    }

    public function setmodel($model) {
        $this->model = $model;
    }

    public function setaxel($axel) {
        $this->axel = $axel;
    }
	
	public function setcapacity($capacity) {
        $this->capacity=$capacity;
    }
	
	public function setcardNumber($cardNumber) {
        $this->cardNumber = $cardNumber;
    }
	public function setcountryMake($countryMake) {
        $this->countryMake = $countryMake;
    }
	public function setvehicletypeId($vehicletypeId) {
        $this->vehicletypeId = $vehicletypeId;
    }




    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
