<?php
include_once('../services/dao.php');
class License extends DAO{
	
	private $id;
	private $numberPlate;
	private $thirdPartyDOI;
	private $thirdPartyDOEg;
	private $caliberationDOI;
	private $caliberationDOE;
	private $hsse;
	private $reminder;
	private $service;
	private $typeOfService;
	private $regDate;
	private $fDoi;
	private $fDoe;
	private $pDoi;
	private $pDoe;
    private $isActive;
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
        public function getid() {
            return $this->id;
        }

        public function getNumberPlate() {
            return $this->numberPlate;
        }

        public function getthirdPartyDOI() {
            return $this->thirdPartyDOI;
        }

        public function getthirdPartyDOEg() {
            return $this->thirdPartyDOEg;
        }

        public function getRegDate() {
            return $this->regDate;
        }
		public function getcaliberationDOI() {
            return $this->caliberationDOI;
        }
		public function getcaliberationDOE() {
            return $this->caliberationDOE;
        }
		public function gethsse() {
            return $this->hsse;
        }
		public function getFDoi() {
            return $this->fDoi;
        }
		public function getFDoe() {
            return $this->fDoe;
        }
		public function getPDoi() {
            return $this->pDoi;
        }
		public function getPDoe() {
            return $this->pDoe;
        }
		public function getTypeOfService() {
            return $this->typeOfService;
        }
        public function getIsActive() {
            return $this->isActive;
        }

        public function setid($id) {
            $this->id = $id;
        }

        public function setNumberPlate($numberPlate) {
            $this->numberPlate = $numberPlate;
        }

        public function setthirdPartyDOI($thirdPartyDOI) {
            $this->thirdPartyDOI = $thirdPartyDOI;
        }

        public function setthirdPartyDOEg($thirdPartyDOEg) {
            $this->thirdPartyDOEg = $thirdPartyDOEg;
        }

        public function setRegDate($regDate) {
            $this->regDate = $regDate;
        }
		public function setcaliberationDOI($caliberationDOI){
			$this->caliberationDOI=$caliberationDOI;
		}
		public function setcaliberationDOE($caliberationDOE){
			$this->caliberationDOE=$caliberationDOE;
		}
		public function sethsse($hsse){
			$this->hsse=$hsse;
		}
		public function setFDoi($fdoi){
			$this->fDoi=$fdoi;
		}
		public function setFDoe($fdoe){
			$this->fDoe=$fdoe;
		}
		public function setPDoi($pDoi){
			$this->pDoi=$pDoi;
		}
		public function setPDoe($pdoe){
			$this->pDoe=$pdoe;
		}
        public function setIsActive($isActive){
			$this->isActive=$isActive;
		}
        public function save(){
		 
	   }
        public function delete(){
    		
    	}
        public function update(){
    		
    	}
        public function view(){
    		
    	}
        public function view_query($sql){
    		
    	}

}

?>