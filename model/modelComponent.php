<?php 
 require_once('../services/dao.php');
class modelComponent extends DAO{
private $id;
private $modelId;
private $componentId;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getmodelId(){
 return $this->modelId;
}
public function getcomponentId(){
 return $this->componentId;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setmodelId($modelId){
  $this->modelId=$modelId;
}
public function setcomponentId($componentId){
  $this->componentId=$componentId;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>