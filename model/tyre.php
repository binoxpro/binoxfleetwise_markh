<?php
include_once('../services/dao.php');
class Tyre extends DAO{
	
	private $Id;
	private $numberPlate;
	private $tyrePosition;
	private $brand;
	private $serialNumber;
	private $fixingDate;
	private $odometer;
	private $expectedKm;
	private $currentMileage;
	private $removalMileage;
	private $comment;
	private $cost;
	private $isActive;
	private $fixedAs;
	private $removalDate;
	private $stockId;
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
        public function getId() {
            return $this->Id;
        }

        public function getNumberPlate() {
            return $this->numberPlate;
        }

        public function gettyrePosition() {
            return $this->tyrePosition;
        }

        public function getbrand() {
            return $this->brand;
        }

        public function getcomment() {
            return $this->comment;
        }
		public function getserialNumber() {
            return $this->serialNumber;
        }
		public function getfixingDate() {
            return $this->fixingDate;
        }
		public function getodometer() {
            return $this->odometer;
        }
		public function getexpectedKm() {
            return $this->expectedKm;
        }
		public function getcurrentMileage() {
            return $this->currentMileage;
        }
		
		public function getremovalMileage() {
            return $this->removalMileage;
        }
		
		public function getCost() {
            return $this->cost;
        }
		
		public function getIsActive() {
            return $this->isActive;
        }
		
		public function getFixedAs() {
            return $this->fixedAs;
        }
		
		public function getremovalDate() {
            return $this->removalDate;
        }
		public function getStockId() {
            return $this->stockId;
        }

        public function setId($Id){
            $this->Id = $Id;
        }

        public function setNumberPlate($numberPlate) {
            $this->numberPlate = $numberPlate;
        }

        public function settyrePosition($tyrePosition) {
            $this->tyrePosition = $tyrePosition;
        }

        public function setbrand($brand) {
            $this->brand = $brand;
        }

        public function setcomment($comment) {
            $this->comment = $comment;
        }
		public function setserialNumber($serialNumber){
			$this->serialNumber=$serialNumber;
		}
		public function setfixingDate($fixingDate){
			$this->fixingDate=$fixingDate;
		}
		public function setodometer($odometer){
			$this->odometer=$odometer;
		}
		public function setexpectedKm($expectedKm){
			$this->expectedKm=$expectedKm;
		}
		public function setcurrentMileage($currentMileage){
			$this->currentMileage=$currentMileage;
		}
		public function setremovalMileage($tos){
			$this->removalMileage=$tos;
		}
		public function setCost($cost){
			$this->cost=$cost;
		}
		
		public function setIsActive($isActive){
			$this->isActive=$isActive;
		}
		
		public function setFixedAs($fixedAs){
			$this->fixedAs=$fixedAs;
		}
		
		public function setremovalDate($removalDate){
			$this->removalDate=$removalDate;
		}
		
		public function setStockId($stockId){
			$this->stockId=$stockId;
		}
		
        public function save(){
		 
	}
    public function delete(){
		
	}
    public function update(){
		
	}
    public function view(){
		
	}
    public function view_query($sql){
		
	}

}

?>