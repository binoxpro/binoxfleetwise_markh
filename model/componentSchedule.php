<?php 
 require_once('../services/dao.php');
class componentSchedule extends DAO{
private $id;
private $serviceIntervalId;
private $componentModelId;
private $actionNoteId;
private $creationDate;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getserviceIntervalId(){
 return $this->serviceIntervalId;
}
public function getcomponentModelId(){
 return $this->componentModelId;
}
public function getactionNoteId(){
 return $this->actionNoteId;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setserviceIntervalId($serviceIntervalId){
  $this->serviceIntervalId=$serviceIntervalId;
}
public function setcomponentModelId($componentModelId){
  $this->componentModelId=$componentModelId;
}
public function setactionNoteId($actionNoteId){
  $this->actionNoteId=$actionNoteId;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>