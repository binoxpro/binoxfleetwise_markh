<?php 
 require_once('../services/dao.php');
class partUsedTest extends DAO{
private $id;
private $numberplate;
private $partused;
private $quantity;
private $cost;
private $jobcardId;
private $dateofrepair;
private $typeofrepair;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getnumberplate(){
 return $this->numberplate;
}
public function getpartused(){
 return $this->partused;
}
public function getquantity(){
 return $this->quantity;
}
public function getcost(){
 return $this->cost;
}
public function getjobcardId(){
 return $this->jobcardId;
}
public function getdateofrepair(){
 return $this->dateofrepair;
}
public function gettypeofrepair(){
 return $this->typeofrepair;
}
public function setid($id){
  $this->id=$id;
}
public function setnumberplate($numberplate){
  $this->numberplate=$numberplate;
}
public function setpartused($partused){
  $this->partused=$partused;
}
public function setquantity($quantity){
  $this->quantity=$quantity;
}
public function setcost($cost){
  $this->cost=$cost;
}
public function setjobcardId($jobcardId){
  $this->jobcardId=$jobcardId;
}
public function setdateofrepair($dateofrepair){
  $this->dateofrepair=$dateofrepair;
}
public function settypeofrepair($typeofrepair){
  $this->typeofrepair=$typeofrepair;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>