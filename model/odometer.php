<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Odometer extends DAO {
    private $id;
    private $vehicleId;
	private $reading;
	private $creationDate;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }
	
	public function getvehicleId() {
        return $this->vehicleId;
    }
	
	public function getreading() {
        return $this->reading;
    }
	
	public function getCreationDate() {
        return $this->creationDate;
    }

    public function setId($id) {
        $this->id = $id;
    }
	
	public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }
	
	public function setreading($reading) {
        $this->reading=$reading;
    }
	
	public function setCreationDate($creationDate) {
        $this->creationDate=$creationDate;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
