<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Licence extends DAO {
    private $id;
    private $licenceNo;
    private $issueDate;
    private $expiryDate;
    private $licenceClass;
    private $parentId;
	private $isActive;
	private $driverId;
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getlicenceNo() {
        return $this->licenceNo;
    }

    public function getissueDate() {
        return $this->issueDate;
    }

    public function getexpiryDate() {
        return $this->expiryDate;
    }

    public function getlicenceClass() {
        return $this->licenceClass;
    }
	 public function getparentId() {
        return $this->parentId;
    }
	public function getisActive(){
		return $this->isActive;
	}
	public function getdriverId(){
		return $this->driverId;
	}
	
	
	
    public function setid($id) {
        $this->id = $id;
    }

    public function setlicenceNo($licenceNo) {
        $this->licenceNo = $licenceNo;
    }

    public function setIssueDate($date) {
        $this->issueDate = $date;
    }

    public function setexpiryDate($expiryDate) {
        $this->expiryDate = $expiryDate;
    }

    public function setlicenceClass($licenceClass) {
        $this->licenceClass = $licenceClass;
    }
	public function setparentId($parentId) {
        $this->parentId = $parentId;
    }
	
	public function setdriverId($driverId){
		$this->driverId=$driverId;
	}
	public function setisActive($isActive) {
        $this->isActive = $isActive;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
