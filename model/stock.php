<?php 
 require_once('../services/dao.php');
class stock extends DAO{
private $id;
private $brand;
private $serialNumber;
private $cost;
private $status;
private $retreadstatus;
private $creationDate;
private $treadDepth;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getbrand(){
 return $this->brand;
}
public function getserialNumber(){
 return $this->serialNumber;
}
public function getcost(){
 return $this->cost;
}
public function getstatus(){
 return $this->status;
}
public function getretreadstatus(){
 return $this->retreadstatus;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function gettreadDepth(){
 return $this->treadDepth;
}
public function setid($id){
  $this->id=$id;
}
public function setbrand($brand){
  $this->brand=$brand;
}
public function setserialNumber($serialNumber){
  $this->serialNumber=$serialNumber;
}
public function setcost($cost){
  $this->cost=$cost;
}
public function setstatus($status){
  $this->status=$status;
}
public function setretreadstatus($retreadstatus){
  $this->retreadstatus=$retreadstatus;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function settreadDepth($treadDepth){
  $this->treadDepth=$treadDepth;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>