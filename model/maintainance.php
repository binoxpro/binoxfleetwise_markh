<?php
include_once('../services/dao.php');
class Maintainance extends DAO{
	private $mID;
	private $workID;
	private $numberPlate;
	private $reg_date;
	private $partUsed;
	private $partID;
	
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
	
	public function getMID() {
            return $this->mID;
        }

        public function getWorkID() {
            return $this->workID;
        }

        public function getNumberPlate() {
            return $this->numberPlate;
        }

        public function getReg_date() {
            return $this->reg_date;
        }

        public function getPartUsed() {
            return $this->partUsed;
        }

        public function getPartID() {
            return $this->partID;
        }

        public function setMID($mID) {
            $this->mID = $mID;
        }

        public function setWorkID($workID) {
            $this->workID = $workID;
        }

        public function setNumberPlate($numberPlate) {
            $this->numberPlate = $numberPlate;
        }

        public function setReg_date($reg_date) {
            $this->reg_date = $reg_date;
        }

        public function setPartUsed($partUsed) {
            $this->partUsed = $partUsed;
        }

        public function setPartID($partID) {
            $this->partID = $partID;
        }

        	
	 public function save(){
		 
	 }
    public function delete(){
		
	}
    public function update(){
		
	}
    public function view(){
		
	}
    public function view_query($sql){
		
	}
}

?>