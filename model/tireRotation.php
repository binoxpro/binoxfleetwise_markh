<?php 
 require_once('../services/dao.php');
class tireRotation extends DAO{
private $id;
private $tireId;
private $fromPosition;
private $toPosition;
private $tireHoldId;
private $kmsDone;
private $inspectionDetailsId;
private $CreationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettireId(){
 return $this->tireId;
}
public function getfromPosition(){
 return $this->fromPosition;
}
public function gettoPosition(){
 return $this->toPosition;
}
public function gettireHoldId(){
 return $this->tireHoldId;
}
public function getkmsDone(){
 return $this->kmsDone;
}
public function getinspectionDetailsId(){
 return $this->inspectionDetailsId;
}
public function getCreationDate(){
 return $this->CreationDate;
}
public function setid($id){
  $this->id=$id;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function setfromPosition($fromPosition){
  $this->fromPosition=$fromPosition;
}
public function settoPosition($toPosition){
  $this->toPosition=$toPosition;
}
public function settireHoldId($tireHoldId){
  $this->tireHoldId=$tireHoldId;
}
public function setkmsDone($kmsDone){
  $this->kmsDone=$kmsDone;
}
public function setinspectionDetailsId($inspectionDetailsId){
  $this->inspectionDetailsId=$inspectionDetailsId;
}
public function setCreationDate($CreationDate){
  $this->CreationDate=$CreationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>