<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class OtherCost extends DAO {
    private $id;
    private $costName;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }
	public function getcostName() {
        return $this->costName;
    }

    public function setId($id) {
        $this->id = $id;
    }
	public function setcostName($costName) {
        $this->costName = $costName;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
