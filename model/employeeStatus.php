<?php 
 require_once('../services/dao.php');
class employeeStatus extends DAO{
private $id;
private $empNo;
private $positionId;
private $startDate;
private $endDate;
private $contractType;
private $payrateId;
private $NssfRequired;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getempNo()
		{
 		return $this->empNo;
}

		public function getpositionId()
		{
 		return $this->positionId;
}

		public function getstartDate()
		{
 		return $this->startDate;
}

		public function getendDate()
		{
 		return $this->endDate;
}

		public function getcontractType()
		{
 		return $this->contractType;
}

		public function getpayrateId()
		{
 		return $this->payrateId;
}

		public function getNssfRequired()
		{
 		return $this->NssfRequired;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setempNo($empNo)
		{
		  $this->empNo=$empNo;
		}

		public function setpositionId($positionId)
		{
		  $this->positionId=$positionId;
		}

		public function setstartDate($startDate)
		{
		  $this->startDate=$startDate;
		}

		public function setendDate($endDate)
		{
		  $this->endDate=$endDate;
		}

		public function setcontractType($contractType)
		{
		  $this->contractType=$contractType;
		}

		public function setpayrateId($payrateId)
		{
		  $this->payrateId=$payrateId;
		}

		public function setNssfRequired($NssfRequired)
		{
		  $this->NssfRequired=$NssfRequired;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>