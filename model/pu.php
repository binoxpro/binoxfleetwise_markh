<?php 
 require_once('../services/dao.php');
class pu extends DAO{
private $id;
private $numberplate;
private $partusedId;
private $quantity;
private $cost;
private $jobcardId;
private $dateofrepair;
private $typeofrepair;
private $supplierId;
private $supplied;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getnumberplate()
		{
 		return $this->numberplate;
}

		public function getpartusedId()
		{
 		return $this->partusedId;
}

		public function getquantity()
		{
 		return $this->quantity;
}

		public function getcost()
		{
 		return $this->cost;
}

		public function getjobcardId()
		{
 		return $this->jobcardId;
}

		public function getdateofrepair()
		{
 		return $this->dateofrepair;
}

		public function gettypeofrepair()
		{
 		return $this->typeofrepair;
}

		public function getsupplierId()
		{
 		return $this->supplierId;
}

		public function getsupplied()
		{
 		return $this->supplied;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setnumberplate($numberplate)
		{
		  $this->numberplate=$numberplate;
		}

		public function setpartusedId($partusedId)
		{
		  $this->partusedId=$partusedId;
		}

		public function setquantity($quantity)
		{
		  $this->quantity=$quantity;
		}

		public function setcost($cost)
		{
		  $this->cost=$cost;
		}

		public function setjobcardId($jobcardId)
		{
		  $this->jobcardId=$jobcardId;
		}

		public function setdateofrepair($dateofrepair)
		{
		  $this->dateofrepair=$dateofrepair;
		}

		public function settypeofrepair($typeofrepair)
		{
		  $this->typeofrepair=$typeofrepair;
		}

		public function setsupplierId($supplierId)
		{
		  $this->supplierId=$supplierId;
		}

		public function setsupplied($supplied)
		{
		  $this->supplied=$supplied;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>