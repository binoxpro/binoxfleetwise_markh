<?php 
 require_once('../services/dao.php');
class Trip extends DAO{
private $TripNumber;
private $TripTypeId;
private $VehicleId;
private $DriverId;
private $Status;
private $TripStartDate;
private $TripEndDate;
private $WorkOrderAttached;
private $Approved;
		function __construct()
		{
 		parent::__construct();
		}

		public function getTripNumber()
		{
 		return $this->TripNumber;
}

		public function getTripTypeId()
		{
 		return $this->TripTypeId;
}

		public function getVehicleId()
		{
 		return $this->VehicleId;
}

		public function getDriverId()
		{
 		return $this->DriverId;
}

		public function getStatus()
		{
 		return $this->Status;
}

		public function getTripStartDate()
		{
 		return $this->TripStartDate;
}

		public function getTripEndDate()
		{
 		return $this->TripEndDate;
}

		public function getWorkOrderAttached()
		{
 		return $this->WorkOrderAttached;
}

		public function getApproved()
		{
 		return $this->Approved;
}

		public function setTripNumber($TripNumber)
		{
		  $this->TripNumber=$TripNumber;
		}

		public function setTripTypeId($TripTypeId)
		{
		  $this->TripTypeId=$TripTypeId;
		}

		public function setVehicleId($VehicleId)
		{
		  $this->VehicleId=$VehicleId;
		}

		public function setDriverId($DriverId)
		{
		  $this->DriverId=$DriverId;
		}

		public function setStatus($Status)
		{
		  $this->Status=$Status;
		}

		public function setTripStartDate($TripStartDate)
		{
		  $this->TripStartDate=$TripStartDate;
		}

		public function setTripEndDate($TripEndDate)
		{
		  $this->TripEndDate=$TripEndDate;
		}

		public function setWorkOrderAttached($WorkOrderAttached)
		{
		  $this->WorkOrderAttached=$WorkOrderAttached;
		}

		public function setApproved($Approved)
		{
		  $this->Approved=$Approved;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>