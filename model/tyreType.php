<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class TyreType extends DAO {
    private $id;
    private $tyretypeName;
    private $tyreIndex;
    private $rotationPosition;
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }
	public function gettyretypeName() {
        return $this->tyretypeName;
    }
    public function gettyreIndex() {
        return $this->tyreIndex;
    }
    public function getrotationName() {
        return $this->rotationPosition;
    }

    public function setId($id) {
        $this->id = $id;
    }
	public function settyretypeName($tyretypeName) {
        $this->tyretypeName = $tyretypeName;
    }
    
    public function settyreIndex($tyreIndex) {
        $this->tyreIndex = $tyreIndex;
    }
    public function setrotationPosition($rotationPosition) {
        $this->rotationPosition = $rotationPosition;
    }
    
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
