<?php 
 require_once('../services/dao.php');
class localpurchaseorderitem extends DAO{
private $id;
private $prfItemId;
private $qty;
private $rate;
private $currencyCode;
private $isActive;
private $creationDate;
private $lopId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getprfItemId()
		{
 		return $this->prfItemId;
}

		public function getqty()
		{
 		return $this->qty;
}

		public function getrate()
		{
 		return $this->rate;
}

		public function getcurrencyCode()
		{
 		return $this->currencyCode;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}
    public function getlopId()
    {
        return $this->lopId;
    }

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setprfItemId($prfItemId)
		{
		  $this->prfItemId=$prfItemId;
		}

		public function setqty($qty)
		{
		  $this->qty=$qty;
		}

		public function setrate($rate)
		{
		  $this->rate=$rate;
		}

		public function setcurrencyCode($currencyCode)
		{
		  $this->currencyCode=$currencyCode;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

    public function setlopId($lopId)
    {
        $this->lopId=$lopId;
    }

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>