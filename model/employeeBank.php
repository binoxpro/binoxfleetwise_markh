<?php 
 require_once('../services/dao.php');
class employeeBank extends DAO{
private $id;
private $empNo;
private $bank;
private $branch;
private $accountName;
private $accountNumber;
private $nssfNumber;
private $Isactive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getempNo()
		{
 		return $this->empNo;
}

		public function getbank()
		{
 		return $this->bank;
}

		public function getbranch()
		{
 		return $this->branch;
}

		public function getaccountName()
		{
 		return $this->accountName;
}

		public function getaccountNumber()
		{
 		return $this->accountNumber;
}

		public function getnssfNumber()
		{
 		return $this->nssfNumber;
}

		public function getIsactive()
		{
 		return $this->Isactive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setempNo($empNo)
		{
		  $this->empNo=$empNo;
		}

		public function setbank($bank)
		{
		  $this->bank=$bank;
		}

		public function setbranch($branch)
		{
		  $this->branch=$branch;
		}

		public function setaccountName($accountName)
		{
		  $this->accountName=$accountName;
		}

		public function setaccountNumber($accountNumber)
		{
		  $this->accountNumber=$accountNumber;
		}

		public function setnssfNumber($nssfNumber)
		{
		  $this->nssfNumber=$nssfNumber;
		}

		public function setIsactive($Isactive)
		{
		  $this->Isactive=$Isactive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>