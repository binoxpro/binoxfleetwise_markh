<?php 
 require_once('../services/dao.php');
class vehiclenewm extends DAO{
private $id;
private $regNo;
private $yom;
private $modelId;
private $dop;
private $chaseNumber;
private $enginecapacitycc;
private $vehicleTypeId;
private $tonnage;
private $color;
private $status;
private $fuel;
private $purpose;
private $isTrailer;
private $hasTrailer;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getregNo()
		{
 		return $this->regNo;
}

		public function getyom()
		{
 		return $this->yom;
}

		public function getmodelId()
		{
 		return $this->modelId;
}

		public function getdop()
		{
 		return $this->dop;
}

		public function getchaseNumber()
		{
 		return $this->chaseNumber;
}

		public function getenginecapacitycc()
		{
 		return $this->enginecapacitycc;
}

		public function getvehicleTypeId()
		{
 		return $this->vehicleTypeId;
}

		public function gettonnage()
		{
 		return $this->tonnage;
}

		public function getcolor()
		{
 		return $this->color;
}

		public function getstatus()
		{
 		return $this->status;
}

		public function getfuel()
		{
 		return $this->fuel;
}

		public function getpurpose()
		{
 		return $this->purpose;
}

		public function getisTrailer()
		{
 		return $this->isTrailer;
}

		public function gethasTrailer()
		{
 		return $this->hasTrailer;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setregNo($regNo)
		{
		  $this->regNo=$regNo;
		}

		public function setyom($yom)
		{
		  $this->yom=$yom;
		}

		public function setmodelId($modelId)
		{
		  $this->modelId=$modelId;
		}

		public function setdop($dop)
		{
		  $this->dop=$dop;
		}

		public function setchaseNumber($chaseNumber)
		{
		  $this->chaseNumber=$chaseNumber;
		}

		public function setenginecapacitycc($enginecapacitycc)
		{
		  $this->enginecapacitycc=$enginecapacitycc;
		}

		public function setvehicleTypeId($vehicleTypeId)
		{
		  $this->vehicleTypeId=$vehicleTypeId;
		}

		public function settonnage($tonnage)
		{
		  $this->tonnage=$tonnage;
		}

		public function setcolor($color)
		{
		  $this->color=$color;
		}

		public function setstatus($status)
		{
		  $this->status=$status;
		}

		public function setfuel($fuel)
		{
		  $this->fuel=$fuel;
		}

		public function setpurpose($purpose)
		{
		  $this->purpose=$purpose;
		}

		public function setisTrailer($isTrailer)
		{
		  $this->isTrailer=$isTrailer;
		}

		public function sethasTrailer($hasTrailer)
		{
		  $this->hasTrailer=$hasTrailer;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>