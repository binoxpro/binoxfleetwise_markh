<?php 
 require_once('../services/dao.php');
class subActivity extends DAO{
private $id;
private $name;
private $link;
private $activityId;
private $isActive;
private $orderIndex;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getname(){
 return $this->name;
}
public function getlink(){
 return $this->link;
}
public function getactivityId(){
 return $this->activityId;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setname($name){
  $this->name=$name;
}
public function setlink($link){
  $this->link=$link;
}
public function setactivityId($activityId){
  $this->activityId=$activityId;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setorderIndex($orderIndex){
  $this->orderIndex=$orderIndex;
}
public function getOrderIndex()
{
    return $this->orderIndex;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>