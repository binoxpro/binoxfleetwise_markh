<?php 
 require_once('../services/dao.php');
class employmentHistory extends DAO{
private $id;
private $empNo;
private $company;
private $title;
private $years;
private $contact;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getempNo()
		{
 		return $this->empNo;
}

		public function getcompany()
		{
 		return $this->company;
}

		public function gettitle()
		{
 		return $this->title;
}

		public function getyears()
		{
 		return $this->years;
}

		public function getcontact()
		{
 		return $this->contact;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setempNo($empNo)
		{
		  $this->empNo=$empNo;
		}

		public function setcompany($company)
		{
		  $this->company=$company;
		}

		public function settitle($title)
		{
		  $this->title=$title;
		}

		public function setyears($years)
		{
		  $this->years=$years;
		}

		public function setcontact($contact)
		{
		  $this->contact=$contact;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>