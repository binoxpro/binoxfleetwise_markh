<?php 
 require_once('../services/dao.php');
class tireBrand extends DAO{
private $id;
private $brand;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getbrand(){
 return $this->brand;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setbrand($brand){
  $this->brand=$brand;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>