<?php 
 require_once('../services/dao.php');
class soldscrap extends DAO{
private $id;
private $scrapId;
private $amount;
private $supplier;
private $creationDate;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getscrapId(){
 return $this->scrapId;
}
public function getamount(){
 return $this->amount;
}
public function getsupplier(){
 return $this->supplier;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function setid($id){
  $this->id=$id;
}
public function setscrapId($scrapId){
  $this->scrapId=$scrapId;
}
public function setamount($amount){
  $this->amount=$amount;
}
public function setsupplier($supplier){
  $this->supplier=$supplier;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>