<?php 
 require_once('../services/dao.php');
class paymentVoucher extends DAO{
private $id;
private $sourceAccount;
private $preparedBy;
private $AuthorisedBy;
private $Posted;
private $transactionDate;
private $creationDate;
private $sourceRef;
private $approved;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getsourceAccount()
		{
 		return $this->sourceAccount;
}

		public function getpreparedBy()
		{
 		return $this->preparedBy;
}

		public function getAuthorisedBy()
		{
 		return $this->AuthorisedBy;
}

		public function getPosted()
		{
 		return $this->Posted;
}

		public function gettransactionDate()
		{
 		return $this->transactionDate;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getsourceRef()
		{
 		return $this->sourceRef;
}

		public function getapproved()
		{
 		return $this->approved;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setsourceAccount($sourceAccount)
		{
		  $this->sourceAccount=$sourceAccount;
		}

		public function setpreparedBy($preparedBy)
		{
		  $this->preparedBy=$preparedBy;
		}

		public function setAuthorisedBy($AuthorisedBy)
		{
		  $this->AuthorisedBy=$AuthorisedBy;
		}

		public function setPosted($Posted)
		{
		  $this->Posted=$Posted;
		}

		public function settransactionDate($transactionDate)
		{
		  $this->transactionDate=$transactionDate;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setsourceRef($sourceRef)
		{
		  $this->sourceRef=$sourceRef;
		}

		public function setapproved($approved)
		{
		  $this->approved=$approved;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>