<?php 
 require_once('../services/dao.php');
class driverVehicle extends DAO{
private $id;
private $driverId;
private $vehicleId;
private $assignmentDate;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getdriverId()
		{
 		return $this->driverId;
}

		public function getvehicleId()
		{
 		return $this->vehicleId;
}

		public function getassignmentDate()
		{
 		return $this->assignmentDate;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setdriverId($driverId)
		{
		  $this->driverId=$driverId;
		}

		public function setvehicleId($vehicleId)
		{
		  $this->vehicleId=$vehicleId;
		}

		public function setassignmentDate($assignmentDate)
		{
		  $this->assignmentDate=$assignmentDate;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>