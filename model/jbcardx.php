<?php 
 require_once('../services/dao.php');
class jbcardx extends DAO{
private $id;
private $jobNo;
private $creationDate;
private $vehicleId;
private $rectified;
private $dateCompleted;
private $odometer;
private $time;
private $checklistId;
private $comment;
private $labourCost;
private $labourHour;
private $garrage;
private $fuelLevel;
private $driverName;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getjobNo()
		{
 		return $this->jobNo;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getvehicleId()
		{
 		return $this->vehicleId;
}

		public function getrectified()
		{
 		return $this->rectified;
}

		public function getdateCompleted()
		{
 		return $this->dateCompleted;
}

		public function getodometer()
		{
 		return $this->odometer;
}

		public function gettime()
		{
 		return $this->time;
}

		public function getchecklistId()
		{
 		return $this->checklistId;
}

		public function getcomment()
		{
 		return $this->comment;
}

		public function getlabourCost()
		{
 		return $this->labourCost;
}

		public function getlabourHour()
		{
 		return $this->labourHour;
}

		public function getgarrage()
		{
 		return $this->garrage;
}

		public function getfuelLevel()
		{
 		return $this->fuelLevel;
}

		public function getdriverName()
		{
 		return $this->driverName;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setjobNo($jobNo)
		{
		  $this->jobNo=$jobNo;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setvehicleId($vehicleId)
		{
		  $this->vehicleId=$vehicleId;
		}

		public function setrectified($rectified)
		{
		  $this->rectified=$rectified;
		}

		public function setdateCompleted($dateCompleted)
		{
		  $this->dateCompleted=$dateCompleted;
		}

		public function setodometer($odometer)
		{
		  $this->odometer=$odometer;
		}

		public function settime($time)
		{
		  $this->time=$time;
		}

		public function setchecklistId($checklistId)
		{
		  $this->checklistId=$checklistId;
		}

		public function setcomment($comment)
		{
		  $this->comment=$comment;
		}

		public function setlabourCost($labourCost)
		{
		  $this->labourCost=$labourCost;
		}

		public function setlabourHour($labourHour)
		{
		  $this->labourHour=$labourHour;
		}

		public function setgarrage($garrage)
		{
		  $this->garrage=$garrage;
		}

		public function setfuelLevel($fuelLevel)
		{
		  $this->fuelLevel=$fuelLevel;
		}

		public function setdriverName($driverName)
		{
		  $this->driverName=$driverName;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>