<?php 
 require_once('../services/dao.php');
class client extends DAO{
private $id;
private $companyName;
private $street;
private $PostalAddress;
private $City;
private $Phone;
private $Email;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getcompanyName()
		{
 		return $this->companyName;
}

		public function getstreet()
		{
 		return $this->street;
}

		public function getPostalAddress()
		{
 		return $this->PostalAddress;
}

		public function getCity()
		{
 		return $this->City;
}

		public function getPhone()
		{
 		return $this->Phone;
}

		public function getEmail()
		{
 		return $this->Email;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setcompanyName($companyName)
		{
		  $this->companyName=$companyName;
		}

		public function setstreet($street)
		{
		  $this->street=$street;
		}

		public function setPostalAddress($PostalAddress)
		{
		  $this->PostalAddress=$PostalAddress;
		}

		public function setCity($City)
		{
		  $this->City=$City;
		}

		public function setPhone($Phone)
		{
		  $this->Phone=$Phone;
		}

		public function setEmail($Email)
		{
		  $this->Email=$Email;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>