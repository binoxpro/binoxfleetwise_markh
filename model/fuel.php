<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Fuel extends DAO {
    private $id;
    private $vehicleId;
	private $distance;
    private $qty;
	private $rate;
	private $destination;
	private $creationDate;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getqty() {
        return $this->qty;
    }
	public function getvehicleId() {
        return $this->vehicleId;
    }
	public function getdistance() {
        return $this->distance;
    }
	
	public function getRate() {
        return $this->rate;
    }
	
	public function getCreationDate() {
        return $this->creationDate;
    }
	
	public function getDestination() {
        return $this->destination;
    }

    public function setId($id) {
        $this->id = $id;
    }
	public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }
	
	public function setqty($qty) {
        $this->qty = $qty;
    }
	
	public function setdistance($distance) {
        $this->distance=$distance;
    }
	
	public function setRate($rate) {
        $this->rate=$rate;
    }
	public function setCreationDate($creationDate) {
        $this->creationDate=$creationDate;
    }
	
	public function setDestination($destination) {
        $this->destination=$destination;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
