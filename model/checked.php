<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Checked extends DAO {
    private $id;
    private $vehicleId;
    private $datex;
    private $timex;
    private $driverName;
	private $representative;
    private $comment;
	private $status;
	private $checklisytTypeId;
    private $vehicleConditionOk;
    private $defectdonotcorrection;
    private $defectscorrected;
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function getdate() {
        return $this->datex;
    }

    public function gettime() {
        return $this->timex;
    }

    public function getdriverName() {
        return $this->driverName;
    }
	
	public function getrepresentative() {
        return $this->representative;
    }
	public function getcomment() {
        return $this->comment;
    }
	
	public function getstatus() {
        return $this->status;
    }
	
	public function getcheckListTypeId() {
        return $this->checklisytTypeId;
    }

    public function getvehicleCoditionOk() {
        return $this->vehicleConditionOk;
    }
    public function getdefectdonotcorrection()
    {
        return $this->defectdonotcorrection;
    }
    public function getdefectscorrected()
    {
        return $this->defectscorrected;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }

    public function setdate($date) {
        $this->datex = $date;
    }

    public function settime($time) {
        $this->timex = $time;
    }

    public function setdriverName($driverName) {
        $this->driverName = $driverName;
    }
	
	public function setrepresentative($representative) {
        $this->representative = $representative;
    }
	
	public function setcomment($comment) {
        $this->comment = $comment;
    }
	
	public function setstatus($status) {
        $this->status = $status;
    }
	
	public function setCheckListTypeId($status) {
        $this->checklisytTypeId = $status;
    }
    public function setvehicleCoditionOk($vco) {
        $this->vehicleConditionOk = $vco;
    }
    public function setdefectscorrected($dc) {
        $this->defectscorrected = $dc;
    }
    public function setdefectdonotcorrection($dnc)
    {
        $this->defectdonotcorrection = $dnc;
    }

	

    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
