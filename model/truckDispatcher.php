<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class TruckDispatcher extends DAO {
    private $id;
    private $timeinDepo;
    private $timeoutDepo;
    private $dispatcher;
	private $allocationId;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function gettimeinDepo() {
        return $this->timeinDepo;
    }

    public function gettimeoutDepo() {
        return $this->timeoutDepo;
    }

    public function getdispatcher() {
        return $this->dispatcher;
    }
	
	public function getAllocationId() {
        return $this->allocationId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function settimeinDepo($timeinDepo) {
        $this->timeinDepo = $timeinDepo;
    }

    public function settimeoutDepo($timeoutDepo) {
        $this->timeoutDepo = $timeoutDepo;
    }

    public function setdispatcher($dispatcher) {
        $this->dispatcher = $dispatcher;
    }
	
	public function setAllocationId($allocationId) {
        $this->allocationId = $allocationId;
    }



    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
