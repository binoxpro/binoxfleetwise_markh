<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Trailer extends DAO {
    private $id;
    private $tractorheadId;
	private $trailerId;
    private $isActive;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getId() {
        return $this->id;
    }

    public function getIsActive() {
        return $this->isActive;
    }
	public function getTractorHeadId() {
        return $this->tractorheadId;
    }
	public function getTrailerId() {
        return $this->trailerId;
    }

    public function setId($id) {
        $this->id = $id;
    }
	public function setTractorHeadId($tractorheadId) {
        $this->tractorheadId = $tractorheadId;
    }
	
	public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }
	
	public function setTrailerId($trailerId) {
        $this->trailerId=$trailerId;
    }
	
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
