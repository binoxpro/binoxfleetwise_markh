<?php 
 require_once('../services/dao.php');
class generalLedger extends DAO{
private $id;
private $ledgerHeadId;
private $accountCode;
private $narrative;
private $reference;
private $amount;
private $transactionType;
private $costCentreId;
private $individualNo;
private $tripNo;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getledgerHeadId()
		{
 		return $this->ledgerHeadId;
}

		public function getaccountCode()
		{
 		return $this->accountCode;
}

		public function getnarrative()
		{
 		return $this->narrative;
}

		public function getreference()
		{
 		return $this->reference;
}

		public function getamount()
		{
 		return $this->amount;
}

		public function gettransactionType()
		{
 		return $this->transactionType;
}

		public function getcostCentreId()
		{
 		return $this->costCentreId;
}

		public function getindividualNo()
		{
 		return $this->individualNo;
}

		public function gettripNo()
		{
 		return $this->tripNo;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setledgerHeadId($ledgerHeadId)
		{
		  $this->ledgerHeadId=$ledgerHeadId;
		}

		public function setaccountCode($accountCode)
		{
		  $this->accountCode=$accountCode;
		}

		public function setnarrative($narrative)
		{
		  $this->narrative=$narrative;
		}

		public function setreference($reference)
		{
		  $this->reference=$reference;
		}

		public function setamount($amount)
		{
		  $this->amount=$amount;
		}

		public function settransactionType($transactionType)
		{
		  $this->transactionType=$transactionType;
		}

		public function setcostCentreId($costCentreId)
		{
		  $this->costCentreId=$costCentreId;
		}

		public function setindividualNo($individualNo)
		{
		  $this->individualNo=$individualNo;
		}

		public function settripNo($tripNo)
		{
		  $this->tripNo=$tripNo;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>