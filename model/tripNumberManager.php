<?php 
 require_once('../services/dao.php');
class tripNumberManager extends DAO{
private $id;
private $tripNo;
private $tripTypeId;
private $OrderNo;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettripNo(){
 return $this->tripNo;
}
public function gettripTypeId(){
 return $this->tripTypeId;
}
public function getOrderNo(){
 return $this->OrderNo;
}
public function setid($id){
  $this->id=$id;
}
public function settripNo($tripNo){
  $this->tripNo=$tripNo;
}
public function settripTypeId($tripTypeId){
  $this->tripTypeId=$tripTypeId;
}
public function setOrderNo($OrderNo){
  $this->OrderNo=$OrderNo;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>