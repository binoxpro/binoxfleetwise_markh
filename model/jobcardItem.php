<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class JobcardItem extends DAO {
    private $id;
    private $details;
    private $timeTaken;
    private $initials;
    private $jobcardId;
    private $status;
    private $repairReview;

    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getdetails() {
        return $this->details;
    }

    public function gettimeTaken() {
        return $this->timeTaken;
    }

    public function getinitials() {
        return $this->initials;
    }

    public function getjobcardId() {
        return $this->jobcardId;
    }

    public function getstatus()
    {
        return $this->status;
    }

    public function getrepairReview()
    {
        return $this->repairReview;
    }




    public function setid($id) {
        $this->id = $id;
    }

    public function setdetails($details) {
        $this->details = $details;
    }

    public function settimeTaken($timeTaken) {
        $this->timeTaken = $timeTaken;
    }

    public function setinitials($initials) {
        $this->initials = $initials;
    }

    public function setjobcardId($jobcardId) {
        $this->jobcardId = $jobcardId;
    }

    public function setrepairReview($repairReview) {
        $this->repairReview = $repairReview;
    }

    public function setstatus($status) {
        $this->status = $status;
    }

    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
