<?php 
 require_once('../services/dao.php');
class serviceInterval extends DAO{
private $id;
private $intervalValue;
private $serviceTypeId;
private $modal;
private $isActive;
private $nextServiceType;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getintervalValue(){
 return $this->intervalValue;
}
public function getserviceTypeId(){
 return $this->serviceTypeId;
}
public function getmodal(){
 return $this->modal;
}
public function getisActive(){
 return $this->isActive;
}
public function getnextServiceType(){
 return $this->nextServiceType;
}
public function setid($id){
  $this->id=$id;
}
public function setintervalValue($intervalValue){
  $this->intervalValue=$intervalValue;
}
public function setserviceTypeId($serviceTypeId){
  $this->serviceTypeId=$serviceTypeId;
}
public function setmodal($modal){
  $this->modal=$modal;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setnextServiceType($nextServiceType){
  $this->nextServiceType=$nextServiceType;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>