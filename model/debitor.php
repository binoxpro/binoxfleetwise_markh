<?php 
 require_once('../services/dao.php');
class debitor extends DAO{
private $id;
private $contactPerson;
private $companyName;
private $building;
private $town;
private $district;
private $country;
private $tel;
private $email;
private $creditPeriod;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getcontactPerson()
		{
 		return $this->contactPerson;
}

		public function getcompanyName()
		{
 		return $this->companyName;
}

		public function getbuilding()
		{
 		return $this->building;
}

		public function gettown()
		{
 		return $this->town;
}

		public function getdistrict()
		{
 		return $this->district;
}

		public function getcountry()
		{
 		return $this->country;
}

		public function gettel()
		{
 		return $this->tel;
}

		public function getemail()
		{
 		return $this->email;
}

		public function getcreditPeriod()
		{
 		return $this->creditPeriod;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setcontactPerson($contactPerson)
		{
		  $this->contactPerson=$contactPerson;
		}

		public function setcompanyName($companyName)
		{
		  $this->companyName=$companyName;
		}

		public function setbuilding($building)
		{
		  $this->building=$building;
		}

		public function settown($town)
		{
		  $this->town=$town;
		}

		public function setdistrict($district)
		{
		  $this->district=$district;
		}

		public function setcountry($country)
		{
		  $this->country=$country;
		}

		public function settel($tel)
		{
		  $this->tel=$tel;
		}

		public function setemail($email)
		{
		  $this->email=$email;
		}

		public function setcreditPeriod($creditPeriod)
		{
		  $this->creditPeriod=$creditPeriod;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>