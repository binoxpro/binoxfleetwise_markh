<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class EmmployeeDetails extends DAO {
    private $id;
    private $position;
    private $employeeId;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }
	public function getposition() {
        return $this->position;
    }
	public function getemployeeId() {
        return $this->employeeId;
    }

    public function setid($id) {
        $this->id = $id;
    }
	public function setposition($position) {
        $this->position = $position;
    }
	 public function setemployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }

}
