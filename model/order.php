<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Order extends DAO {
    private $id;
    private $mailingName;
    private $orderDate;
    private $promiseDate;
    private $orderNo;
	private $orderType;
	private $qtyOrder;
    private $statusOrder;
	private $orderStatus;
	private $itemCode;
	private $orderNumber;
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getmailingName() {
        return $this->mailingName;
    }

    public function getorderDate() {
        return $this->orderDate;
    }

    public function getpromiseDate() {
        return $this->promiseDate;
    }
	
	public function getitemCode() {
        return $this->itemCode;
    }

    public function getorderNo() {
        return $this->orderNo;
    }
	public function getqtyOrder() {
        return $this->qtyOrder;
    }
	
	public function getcapacity() {
        return $this->capacity;
    }
	
	public function getorderType() {
        return $this->orderType;
    }
	
	public function getstatusOrder() {
        return $this->statusOrder;
    }
	
	public function getorderStatus() {
        return $this->orderStatus;
    }

	public function getorderNumber() {
        return $this->orderNumber;
    }
	
    public function setid($id) {
        $this->id = $id;
    }

    public function setmailingName($mailingName) {
        $this->mailingName = $mailingName;
    }

    public function setorderDate($orderDate) {
        $this->orderDate = $orderDate;
    }

    public function setpromiseDate($promiseDate) {
        $this->promiseDate = $promiseDate;
    }

    public function setorderNo($orderNo) {
        $this->orderNo = $orderNo;
    }
	public function setqtyOrder($qtyOrder) {
        $this->qtyOrder = $qtyOrder;
    }
	
	public function setcapacity($capacity) {
        $this->capacity=$capacity;
    }
	
	public function setorderType($orderType) {
        $this->orderType = $orderType;
    }
	public function setstatusOrder($statusOrder) {
        $this->statusOrder = $statusOrder;
    }
	public function setorderStatus($orderStatus) {
        $this->orderStatus = $orderStatus;
    }
	
	public function setitemCode($itemCode) {
        $this->itemCode = $itemCode;
    }
	
	public function setorderNumber($orderNumber) {
        $this->orderNumber = $orderNumber;
    }




    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
