<?php 
 require_once('../services/dao.php');
class invoice extends DAO{
private $id;
private $creationDate;
private $confirmed;
private $currencyId;
private $customerId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getcreationDate()
		{
 		return $this->creationDate;
}

		public function getconfirmed()
		{
 		return $this->confirmed;
}

		public function getcurrencyId()
		{
 		return $this->currencyId;
}

		public function getcustomerId()
		{
 		return $this->customerId;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setcreationDate($creationDate)
		{
		  $this->creationDate=$creationDate;
		}

		public function setconfirmed($confirmed)
		{
		  $this->confirmed=$confirmed;
		}

		public function setcurrencyId($currencyId)
		{
		  $this->currencyId=$currencyId;
		}

		public function setcustomerId($customerId)
		{
		  $this->customerId=$customerId;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>