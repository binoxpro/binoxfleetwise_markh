<?php 
 require_once('../services/dao.php');
class partRequestForm extends DAO{
private $id;
private $section;
private $vehicleId;
private $requestedBy;
private $requestDate;
private $status;
private $jobcardId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getsection()
		{
 		return $this->section;
}

		public function getvehicleId()
		{
 		return $this->vehicleId;
}

		public function getrequestedBy()
		{
 		return $this->requestedBy;
}

		public function getrequestDate()
		{
 		return $this->requestDate;
}

		public function getstatus()
		{
 		return $this->status;
}

		public function getjobcardId()
		{
 		return $this->jobcardId;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setsection($section)
		{
		  $this->section=$section;
		}

		public function setvehicleId($vehicleId)
		{
		  $this->vehicleId=$vehicleId;
		}

		public function setrequestedBy($requestedBy)
		{
		  $this->requestedBy=$requestedBy;
		}

		public function setrequestDate($requestDate)
		{
		  $this->requestDate=$requestDate;
		}

		public function setstatus($status)
		{
		  $this->status=$status;
		}

		public function setjobcardId($jobcardId)
		{
		  $this->jobcardId=$jobcardId;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>