<?php 
 require_once('../services/dao.php');
class truckModel extends DAO{
private $id;
private $modelName;
private $makeId;
private $yearManufacture;
private $country;
private $isActive;

		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getmodelName()
		{
 		return $this->modelName;
}

		public function getmakeId()
		{
 		return $this->makeId;
}

		public function getyearManufacture()
		{
 		return $this->yearManufacture;
}

		public function getcountry()
		{
 		return $this->country;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setmodelName($modelName)
		{
		  $this->modelName=$modelName;
		}

		public function setmakeId($makeId)
		{
		  $this->makeId=$makeId;
		}

		public function setyearManufacture($yearManufacture)
		{
		  $this->yearManufacture=$yearManufacture;
		}

		public function setcountry($country)
		{
		  $this->country=$country;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>