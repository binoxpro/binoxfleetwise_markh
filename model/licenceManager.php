<?php 
 require_once('../services/dao.php');
class licenceManager extends DAO{
private $id;
private $vehicleId;
private $startDate;
private $expireDate;
private $licenceCost;
private $status;
private $licenceName;
private $supplierId;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getvehicleId()
		{
 		return $this->vehicleId;
}

		public function getstartDate()
		{
 		return $this->startDate;
}

		public function getexpireDate()
		{
 		return $this->expireDate;
}

		public function getlicenceCost()
		{
 		return $this->licenceCost;
}

		public function getstatus()
		{
 		return $this->status;
}

		public function getlicenceName()
		{
 		return $this->licenceName;
}

		public function getsupplierId()
		{
 		return $this->supplierId;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setvehicleId($vehicleId)
		{
		  $this->vehicleId=$vehicleId;
		}

		public function setstartDate($startDate)
		{
		  $this->startDate=$startDate;
		}

		public function setexpireDate($expireDate)
		{
		  $this->expireDate=$expireDate;
		}

		public function setlicenceCost($licenceCost)
		{
		  $this->licenceCost=$licenceCost;
		}

		public function setstatus($status)
		{
		  $this->status=$status;
		}

		public function setlicenceName($licenceName)
		{
		  $this->licenceName=$licenceName;
		}

		public function setsupplierId($supplierId)
		{
		  $this->supplierId=$supplierId;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>