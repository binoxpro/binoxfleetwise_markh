<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Delivery extends DAO {
    private $id;
    private $DeliveryDate;
    private $DeliveryTime;
	private $allocationId;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getDeliveryDate() {
        return $this->DeliveryDate;
    }

    public function getDeliveryTime() {
        return $this->DeliveryTime;
    }

	public function getAllocationId() {
        return $this->allocationId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setDeliveryDate($DeliveryDate) {
        $this->DeliveryDate = $DeliveryDate;
    }

    public function setDeliveryTime($DeliveryTime) {
        $this->DeliveryTime = $DeliveryTime;
    }
	
	public function setAllocationId($allocationId) {
        $this->allocationId = $allocationId;
    }



    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
