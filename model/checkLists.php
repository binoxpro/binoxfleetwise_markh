<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class CheckLists extends DAO {
    private $id;
    private $driverId;
    private $date;
    private $employeeId;
    private $checklisttypeId;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getdriverId() {
        return $this->driverId;
    }

    public function getdate() {
        return $this->date;
    }

    public function getemployeeId() {
        return $this->employeeId;
    }

    public function getchecklisttypeId() {
        return $this->checklisttypeId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setdriverId($driverId) {
        $this->driverId = $driverId;
    }

    public function setdate($date) {
        $this->date = $date;
    }

    public function setemployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    public function setchecklisttypeId($checklisttypeId) {
        $this->checklisttypeId = $checklisttypeId;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
