<?php 
 require_once('../services/dao.php');
class tireHold extends DAO{
private $id;
private $vehicleId;
private $positionId;
private $tireId;
private $fixingDate;
private $fixingOdometer;
private $expectedKms;
private $removalKms;
private $actualKms;
private $creationDate;
private $comment;
private $isActive;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function getvehicleId(){
 return $this->vehicleId;
}
public function getpositionId(){
 return $this->positionId;
}
public function gettireId(){
 return $this->tireId;
}
public function getfixingDate(){
 return $this->fixingDate;
}
public function getfixingOdometer(){
 return $this->fixingOdometer;
}
public function getexpectedKms(){
 return $this->expectedKms;
}
public function getremovalKms(){
 return $this->removalKms;
}
public function getactualKms(){
 return $this->actualKms;
}
public function getcreationDate(){
 return $this->creationDate;
}
public function getcomment(){
 return $this->comment;
}
public function getisActive(){
 return $this->isActive;
}
public function setid($id){
  $this->id=$id;
}
public function setvehicleId($vehicleId){
  $this->vehicleId=$vehicleId;
}
public function setpositionId($positionId){
  $this->positionId=$positionId;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function setfixingDate($fixingDate){
  $this->fixingDate=$fixingDate;
}
public function setfixingOdometer($fixingOdometer){
  $this->fixingOdometer=$fixingOdometer;
}
public function setexpectedKms($expectedKms){
  $this->expectedKms=$expectedKms;
}
public function setremovalKms($removalKms){
  $this->removalKms=$removalKms;
}
public function setactualKms($actualKms){
  $this->actualKms=$actualKms;
}
public function setcreationDate($creationDate){
  $this->creationDate=$creationDate;
}
public function setcomment($comment){
  $this->comment=$comment;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>