<?php 
 require_once('../services/dao.php');
class accountType extends DAO{
private $PrefixCode;
private $AccountTypeName;
private $AccountBalanceType;
private $ParentPrefixCode;
private $IsActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getPrefixCode()
		{
 		return $this->PrefixCode;
}

		public function getAccountTypeName()
		{
 		return $this->AccountTypeName;
}

		public function getAccountBalanceType()
		{
 		return $this->AccountBalanceType;
}

		public function getParentPrefixCode()
		{
 		return $this->ParentPrefixCode;
}

		public function getIsActive()
		{
 		return $this->IsActive;
}

		public function setPrefixCode($PrefixCode)
		{
		  $this->PrefixCode=$PrefixCode;
		}

		public function setAccountTypeName($AccountTypeName)
		{
		  $this->AccountTypeName=$AccountTypeName;
		}

		public function setAccountBalanceType($AccountBalanceType)
		{
		  $this->AccountBalanceType=$AccountBalanceType;
		}

		public function setParentPrefixCode($ParentPrefixCode)
		{
		  $this->ParentPrefixCode=$ParentPrefixCode;
		}

		public function setIsActive($IsActive)
		{
		  $this->IsActive=$IsActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>