<?php 
 require_once('../services/dao.php');
class fuelOrderItem extends DAO{
private $id;
private $fuelOrderId;
private $fuelProductId;
private $litres;
private $rate;
private $isActive;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getfuelOrderId()
		{
 		return $this->fuelOrderId;
}

		public function getfuelProductId()
		{
 		return $this->fuelProductId;
}

		public function getlitres()
		{
 		return $this->litres;
}

		public function getrate()
		{
 		return $this->rate;
}

		public function getisActive()
		{
 		return $this->isActive;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setfuelOrderId($fuelOrderId)
		{
		  $this->fuelOrderId=$fuelOrderId;
		}

		public function setfuelProductId($fuelProductId)
		{
		  $this->fuelProductId=$fuelProductId;
		}

		public function setlitres($litres)
		{
		  $this->litres=$litres;
		}

		public function setrate($rate)
		{
		  $this->rate=$rate;
		}

		public function setisActive($isActive)
		{
		  $this->isActive=$isActive;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>