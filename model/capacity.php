<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class CapacityUtilisation extends DAO {
    private $id;
    private $vehicleId;
    private $tripId;
    private $vpower;
    private $ago;
	private $bik;
	private $ugl;
    private $creationDate;
	private $timeLoading;
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function gettripId() {
        return $this->tripId;
    }

    public function getvpower() {
        return $this->vpower;
    }

    public function getago() {
        return $this->ago;
    }
	public function getugl() {
        return $this->ugl;
    }
	
	public function getcapacity() {
        return $this->capacity;
    }
	
	public function getbik() {
        return $this->bik;
    }
	public function getcreationDate() {
        return $this->creationDate;
    }
	public function gettimeLoading() {
        return $this->timeLoading;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }

    public function settripId($tripId) {
        $this->tripId = $tripId;
    }

    public function setvpower($vpower) {
        $this->vpower = $vpower;
    }

    public function setago($ago) {
        $this->ago = $ago;
    }
	public function setugl($ugl) {
        $this->ugl = $ugl;
    }
	
	public function setcapacity($capacity) {
        $this->capacity=$capacity;
    }
	
	public function setbik($bik) {
        $this->bik = $bik;
    }
	public function setcreationDate($creationDate) {
        $this->creationDate = $creationDate;
    }
	public function settimeLoading($timeLoading) {
        $this->timeLoading = $timeLoading;
    }




    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
