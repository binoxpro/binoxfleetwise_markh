<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class Standards extends DAO {
    private $id;
    private $abs;
    private $bmc;
    private $vehicleId;
    private $obc;
    private $sur;
	private $rur;
	private $fbsm;
	private $sst;
	private $sstt;
	private $isActive;
	private $parentId;
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getabs() {
        return $this->abs;
    }

    public function getbmc() {
        return $this->bmc;
    }

    public function getvehicleId() {
        return $this->vehicleId;
    }

    public function getobc() {
        return $this->obc;
    }
	 public function getsur() {
        return $this->sur;
    }
	public function getrur(){
		return $this->rur;
	}
	public function getfbsm(){
		return $this->fbsm;
	}
	
	public function getsst(){
		return $this->sst;
	}
	
	public function getsstt(){
		return $this->sstt;
	}

	public function getisActive() {
        return $this->isActive;
    }
	
	public function getparentId() {
        return $this->parentId;
    }
	
    public function setid($id) {
        $this->id = $id;
    }

    public function setabs($abs) {
        $this->abs = $abs;
    }

    public function setbmc($date) {
        $this->bmc = $date;
    }

    public function setvehicleId($vehicleId) {
        $this->vehicleId = $vehicleId;
    }

    public function setobc($obc) {
        $this->obc = $obc;
    }
	public function setsur($sur) {
        $this->sur = $sur;
    }
	
	public function setfbsm($fbsm){
		$this->fbsm=$fbsm;
	}
	public function setrur($rur) {
        $this->rur = $rur;
    }
	
	
	public function setsst($sst) {
        $this->sst = $sst;
    }
	
	public function setsstt($sstt) {
        $this->sstt = $sstt;
    }
	
	public function setisActive($isActive) {
        $this->isActive = $isActive;
    }
	
	public function setparentId($parentId) {
        $this->parentId = $parentId;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
