<?php 
 require_once('../services/dao.php');
class trainingTracker extends DAO{
private $id;
private $trainingId;
private $issueDate;
private $expiryDate;
private $isActive;
private $driverId;
private $status;
private $parentId;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettrainingId(){
 return $this->trainingId;
}
public function getissueDate(){
 return $this->issueDate;
}
public function getexpiryDate(){
 return $this->expiryDate;
}
public function getisActive(){
 return $this->isActive;
}
public function getdriverId(){
 return $this->driverId;
}
public function getstatus(){
 return $this->status;
}
public function getparentId(){
 return $this->parentId;
}
public function setid($id){
  $this->id=$id;
}
public function settrainingId($trainingId){
  $this->trainingId=$trainingId;
}
public function setissueDate($issueDate){
  $this->issueDate=$issueDate;
}
public function setexpiryDate($expiryDate){
  $this->expiryDate=$expiryDate;
}
public function setisActive($isActive){
  $this->isActive=$isActive;
}
public function setdriverId($driverId){
  $this->driverId=$driverId;
}
public function setstatus($status){
  $this->status=$status;
}
public function setparentId($parentId){
  $this->parentId=$parentId;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>