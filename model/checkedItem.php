<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class CheckedItem extends DAO {
    private $id;
    private $checkListId;
    private $sectionItemId;
    private $corrected;
    private $inspectorsComment;
    
	
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }

    public function getcheckListId() {
        return $this->checkListId;
    }

    public function getsectionItemId() {
        return $this->sectionItemId;
    }

    public function getCorrected() {
        return $this->corrected;
    }

    public function getdriverName() {
        return $this->driverName;
    }
    public function getinspectorsComment() {
        return $this->inspectorsComment;
    }
	
    public function setid($id) {
        $this->id = $id;
    }

    public function setcheckListId($checkListId) {
        $this->checkListId = $checkListId;
    }

    public function setsectionItemId($date) {
        $this->sectionItemId = $date;
    }

    public function setcorrected($time) {
    $this->corrected = $time;
}
    public function setinspectorsComment($inspectorsComment) {
        $this->inspectorsComment = $inspectorsComment;
    }

    
    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
