<?php 
 require_once('../services/dao.php');
class tireTreadDepth extends DAO{
private $id;
private $treadDepth;
private $tireId;
private $CreationDate;
private $odometerreading;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettreadDepth(){
 return $this->treadDepth;
}
public function gettireId(){
 return $this->tireId;
}
public function getCreationDate(){
 return $this->CreationDate;
}
public function getodometerreading(){
 return $this->odometerreading;
}
public function setid($id){
  $this->id=$id;
}
public function settreadDepth($treadDepth){
  $this->treadDepth=$treadDepth;
}
public function settireId($tireId){
  $this->tireId=$tireId;
}
public function setCreationDate($CreationDate){
  $this->CreationDate=$CreationDate;
}
public function setodometerreading($odometerreading){
  $this->odometerreading=$odometerreading;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>