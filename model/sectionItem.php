<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Account
 *
 * @author plussoft
 */
include_once('../services/dao.php');
class SectionItem extends DAO {
    private $id;
	private $itemName;
    private $itemDescription;
    private $checklistId;
    private $comment;
    private $sectionId;
    
    
    function __construct() {
        parent::__construct();
    }
    
    public function getid() {
        return $this->id;
    }
    public function getitemName() {
        return $this->itemName;
    }
    
	public function getitemDescription() {
        return $this->itemDescription;
    }

    public function getchecklistId() {
        return $this->checklistId;
    }

    public function getcomment() {
        return $this->comment;
    }

    public function getsectionId() {
        return $this->sectionId;
    }

    public function setid($id) {
        $this->id = $id;
    }

    public function setitemDescription($itemDescription) {
        $this->itemDescription = $itemDescription;
    }

    public function setchecklistId($checklistId) {
        $this->checklistId = $checklistId;
    }

    public function setcomment($comment) {
        $this->comment = $comment;
    }

    public function setsectionId($sectionId) {
        $this->sectionId = $sectionId;
    }
	public function setItemName($sectionId) {
        $this->itemName = $sectionId;
    }


    public function delete() {
        
    }

    public function save() {
        
    }

    public function update() {
        
    }

    public function view() {
        
    }

    public function view_query($sql) {
        
    }


        //put your code here
}
