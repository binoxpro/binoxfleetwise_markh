<?php 
 require_once('../services/dao.php');
class workorderCargo extends DAO{
private $id;
private $cargoTypeId;
private $workorderId;
private $cargoDetails;
private $qty;
private $Rate;
private $Total;
private $Vat;
		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getcargoTypeId()
		{
 		return $this->cargoTypeId;
}

		public function getworkorderId()
		{
 		return $this->workorderId;
}

		public function getcargoDetails()
		{
 		return $this->cargoDetails;
}

		public function getqty()
		{
 		return $this->qty;
}

		public function getRate()
		{
 		return $this->Rate;
}

		public function getTotal()
		{
 		return $this->Total;
}

		public function getVat()
		{
 		return $this->Vat;
}

		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setcargoTypeId($cargoTypeId)
		{
		  $this->cargoTypeId=$cargoTypeId;
		}

		public function setworkorderId($workorderId)
		{
		  $this->workorderId=$workorderId;
		}

		public function setcargoDetails($cargoDetails)
		{
		  $this->cargoDetails=$cargoDetails;
		}

		public function setqty($qty)
		{
		  $this->qty=$qty;
		}

		public function setRate($Rate)
		{
		  $this->Rate=$Rate;
		}

		public function setTotal($Total)
		{
		  $this->Total=$Total;
		}

		public function setVat($Vat)
		{
		  $this->Vat=$Vat;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>