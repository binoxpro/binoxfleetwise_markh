<?php 
 require_once('../services/dao.php');
class supplier extends DAO{
private $supplierCode;
private $supplierName;
private $address;
private $paymentTerm;
private $accountNumber;
 function __construct(){
 parent::__construct();
}
public function getsupplierCode(){
 return $this->supplierCode;
}
public function getsupplierName(){
 return $this->supplierName;
}
public function getaddress(){
 return $this->address;
}
public function getpaymentTerm(){
 return $this->paymentTerm;
}
public function getaccountNumber(){
 return $this->accountNumber;
}
public function setsupplierCode($supplierCode){
  $this->supplierCode=$supplierCode;
}
public function setsupplierName($supplierName){
  $this->supplierName=$supplierName;
}
public function setaddress($address){
  $this->address=$address;
}
public function setpaymentTerm($paymentTerm){
  $this->paymentTerm=$paymentTerm;
}
public function setaccountNumber($accountNumber){
  $this->accountNumber=$accountNumber;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>