<?php 
 require_once('../services/dao.php');
class auditTrail extends DAO{
		private $id;
		private $userId;
		private $url;
		private $creationdate;
		private $timeCreation;
		private $Ipadderess;

		function __construct()
		{
 		parent::__construct();
		}

		public function getid()
		{
 		return $this->id;
}

		public function getuserId()
		{
 		return $this->userId;
}

		public function geturl()
		{
 		return $this->url;
}

		public function getcreationdate()
		{
 		return $this->creationdate;
}

		public function gettimeCreation()
		{
 			return $this->timeCreation;
		}

	public function getIpadderess()
	{
		$client  = $_SERVER['HTTP_CLIENT_IP'];
		$forward = $_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}

		return $ip;
	}
		public function setid($id)
		{
		  $this->id=$id;
		}

		public function setuserId($userId)
		{
		  $this->userId=$userId;
		}

		public function seturl($url)
		{
		  $this->url=$url;
		}

		public function setcreationdate($creationdate)
		{
		  $this->creationdate=$creationdate;
		}

		public function settimeCreation($timeCreation)
		{
		  $this->timeCreation=$timeCreation;
		}

		public function setIpadderess($Ipadderess)
		{
		  $this->Ipadderess=$Ipadderess;
		}

		public function save()
		{
} 

		public function update()
		{
} 
public function view()
		{
} 
public function delete()
		{
} 
public function view_query($sql)
		{
} 
} 
 ?>