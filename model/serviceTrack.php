<?php
include_once('../services/dao.php');
class ServiceTrack extends DAO{
	
	private $serviceID;
	private $numberPlate;
	private $kmReading;
	private $nextReading;
	private $previousDate;
	private $nextDate;
	private $interval;
	private $reminder;
	private $service;
	private $typeOfService;
	private $regDate;
	
	public function __construct(){
		//connect to the database
		parent::__construct();
	}
        public function getServiceID() {
            return $this->serviceID;
        }

        public function getNumberPlate() {
            return $this->numberPlate;
        }

        public function getKmReading() {
            return $this->kmReading;
        }

        public function getNextReading() {
            return $this->nextReading;
        }

        public function getRegDate() {
            return $this->regDate;
        }
		public function getPreviousDate() {
            return $this->previousDate;
        }
		public function getNextDate() {
            return $this->nextDate;
        }
		public function getInterval() {
            return $this->interval;
        }
		public function getReminder() {
            return $this->reminder;
        }
		public function getService() {
            return $this->service;
        }
		public function getTypeOfService() {
            return $this->typeOfService;
        }

        public function setServiceID($serviceID) {
            $this->serviceID = $serviceID;
        }

        public function setNumberPlate($numberPlate) {
            $this->numberPlate = $numberPlate;
        }

        public function setKmReading($kmReading) {
            $this->kmReading = $kmReading;
        }

        public function setNextReading($nextReading) {
            $this->nextReading = $nextReading;
        }

        public function setRegDate($regDate) {
            $this->regDate = $regDate;
        }
		public function setPreviousDate($previousDate){
			$this->previousDate=$previousDate;
		}
		public function setNextDate($nextDate){
			$this->nextDate=$nextDate;
		}
		public function setInterval($interval){
			$this->interval=$interval;
		}
		public function setReminder($reminder){
			$this->reminder=$reminder;
		}
		public function setService($service){
			$this->service=$service;
		}
		public function setTypeOfservice($tos){
			$this->typeOfService=$tos;
		}
        public function save(){
		 
	}
    public function delete(){
		
	}
    public function update(){
		
	}
    public function view(){
		
	}
    public function view_query($sql){
		
	}

}

?>