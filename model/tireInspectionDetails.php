<?php 
 require_once('../services/dao.php');
class tireInspectionDetails extends DAO{
private $id;
private $tireHoldId;
private $treadDepth;
private $pressure;
private $sidewall;
private $tread;
private $mech;
private $mismatchedDual;
private $valveCondition;
private $rimCondition;
private $inspectionHeaderId;
private $generalRemarks;
 function __construct(){
 parent::__construct();
}
public function getid(){
 return $this->id;
}
public function gettireHoldId(){
 return $this->tireHoldId;
}
public function gettreadDepth(){
 return $this->treadDepth;
}
public function getpressure(){
 return $this->pressure;
}
public function getsidewall(){
 return $this->sidewall;
}
public function gettread(){
 return $this->tread;
}
public function getmech(){
 return $this->mech;
}
public function getmismatchedDual(){
 return $this->mismatchedDual;
}
public function getvalveCondition(){
 return $this->valveCondition;
}
public function getrimCondition(){
 return $this->rimCondition;
}
public function getinspectionHeaderId(){
 return $this->inspectionHeaderId;
}
public function getgeneralRemarks(){
 return $this->generalRemarks;
}
public function setid($id){
  $this->id=$id;
}
public function settireHoldId($tireHoldId){
  $this->tireHoldId=$tireHoldId;
}
public function settreadDepth($treadDepth){
  $this->treadDepth=$treadDepth;
}
public function setpressure($pressure){
  $this->pressure=$pressure;
}
public function setsidewall($sidewall){
  $this->sidewall=$sidewall;
}
public function settread($tread){
  $this->tread=$tread;
}
public function setmech($mech){
  $this->mech=$mech;
}
public function setmismatchedDual($mismatchedDual){
  $this->mismatchedDual=$mismatchedDual;
}
public function setvalveCondition($valveCondition){
  $this->valveCondition=$valveCondition;
}
public function setrimCondition($rimCondition){
  $this->rimCondition=$rimCondition;
}
public function setinspectionHeaderId($inspectionHeaderId){
  $this->inspectionHeaderId=$inspectionHeaderId;
}
public function setgeneralRemarks($generalRemarks){
  $this->generalRemarks=$generalRemarks;
}
public function save()
{
} 
public function update()
{
} 
public function view()
{
} 
public function delete()
{
} 
public function view_query($sql)
{
} 
} 
 ?>