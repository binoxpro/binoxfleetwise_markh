<?php 
require_once('header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MARKH INVESTMENTS FMS.104</title>

    <!-- Bootstrap -->
    <link href="vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendor/nprogress/nprogress.css" rel="stylesheet">

    <link href="vendor/iCheck/skins/flat/green.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/bootstrap/easyui.css" />
    <link rel="stylesheet" type="text/css" href="lib/external/jquery/themes/icon.css" />
    <link rel="stylesheet" media="screen" href="lib/handsontable/dist/jquery.handsontable.full.css">
    <link rel="stylesheet" href="lib/signtures/assets/jquery.signaturepad.css">
    <link rel="stylesheet" href="lib/datepicker/css/datepicker.css" type="text/css">
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">


    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->

    <!--<link href="fonts/css/font-awesome.min.css" rel="stylesheet">-->


    <!-- Custom styling plus plugins -->

    <!--<link href="css/icheck/flat/green.css" rel="stylesheet">-->
    <!--<link href="css/progressbar/bootstrap-progressbar-3.3.0.css" rel="stylesheet">-->






    <!-- NProgress -->
    <!--<link href="css/nprogress/nprogress.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">-->





    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<style type="text/css">
	/* *, *:before, *:after{
-webkit-box-sizing:content-box;
-moz-box-sizing:content-box;
box-sizing:content-box;
}*/
.datagrid-editable-input,.textbox-text{
	color:#0061C1;
}
td input{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
td select{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
.easyui-datebox{
	height:30px;
	width:30%;
	border:1px solid #CCC;
	border-radius:3px;
	font-size:16px;
	margin-top:10px;
	margin-bottom:10px;
}
	</style>
</head>


<body class="nav-md">

    <div class="container body">
        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="admin.php" class="site_title"><i class="fa fa-wrench"></i><span>BINOX</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/user.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?php echo $_SESSION['username']; ?></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                    <?php require_once('controller/sideBarMenu.php');?>
                            </ul>
                        </div>
                        

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/user.png" alt=""><?php echo "<p id='usernamefor'>" .$_SESSION['username']."</p>"; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="javascript:;">Change Password</a>
                                    </li>
                                    
                                    <li>
                                        <a href="help.html">Help</a>
                                    </li>
                                    <li>
                                        <a href="controller/usercontroller.php?action=logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- note notification can go here-->
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->

                <div class="right_col" role="main" >
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										$header2="";
										$header3="";
										for($i=0;$i<sizeof($header);$i++){
											if($i<2){
											$header2.= strtoupper($header[$i])."&nbsp;";
											}else{
											$header3.=$header[$i]."&nbsp;";	
											}
										}
										echo "<span>".$header2."</span>:<span>".$header3."</span>";
									}else{
										echo "DashBoard";
									}
			
							?></h3>
                        </div>

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:auto;">
                                <div class="x_title">
                                    <h2><!--<i class="fa fa-cogs"></i>-->
                            <?php
									if(isset($_REQUEST['view'])){
										$header=explode("_",$_REQUEST['view']);
										for($i=0;$i<sizeof($header);$i++){
											echo strtoupper( $header[$i])."&nbsp;";
										}
									}else{
										echo "DashBoard";
									}
			
							?></h2>
                                    <!--<ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>-->
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                
                                    <?php require_once('controller/dispatcher.php'); ?>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        <p class="pull-right">Binox Africa Software &copy; <a> Fleetwise Solution</a>. |
                            <span class="lead">Licenced to <i class="fa fa-cogs">MARKH INVESTMENT</i></span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->


            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <?php //require_once('view/odometer/kmgraphgenerator.php'); ?>
    <!-- jQuery -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendor/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendor/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="vendor/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!--type head-->
    <script src="lib/external/typeahead/bootstrap3-typeahead.min.js"></script>

    <script src="js/chartjs/chart.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="vendor/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="lib/external/jquery/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="lib/datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="lib/external/jquerylib/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="lib/external/jquerylib/datagrid-detailview.js"></script>
    <script src="lib/signtures/jquery.signaturepad.min.js"></script>
    <script src="lib/signtures/assets/json2.min.js"></script>
    <script type="text/javascript" src="lib/js/scriptEngine.js"></script>
    <script type="text/javascript" src="lib/external/chart/Chart.js"></script>
    <!-- Custom Theme Scripts -->

    <script src="js/custom.min.js"></script>
    <?php require_once('controller/scriptController.php'); ?>


    <!--<script src="js/jquery.min.js"></script>-->
    <!--<script src="js/bootstrap.min.js"></script>-->

    <!-- chart js -->

    <!-- bootstrap progress js -->
    <!--<script src="js/progressbar/bootstrap-progressbar.min.js"></script>-->

    <!-- icheck -->



    <!--<script src="vendor/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="lib/external/jquery/jquery.easyui.min.js"></script>-->



    <!--<script src="lib/signtures/assets/jquery.min.js"></script>-->

    

    <!-- moris js -->
    

    <script type="text/javascript">
	function loadingBar(message){
	$.messager.progress({title:'loading',msg:message});
	}

	function closeBar()
	{
	$.messager.progress('close');
	}
	
	function saveForm(formName,url,dialogName,datagridName){
		//url=url;
		$.messager.progress({title:'Submitting',msg:'Please wait connecting to server in progress...'});
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
                     $.messager.alert('Error',
                         result.msg,'error');
					} else {
						$.messager.show({title: 'Info',
					  msg: 'Successfully completed'});
						$(dialogName).dialog('close');
						$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
		}
		
	}
}); 
		


}

function saveFormUrl(formName,url,urlx){
		//url=url;
		$.messager.progress({title:'Submitting',msg:'Please wait connecting to server in progress...'});
	 $(formName).form('submit',{ url: url, 
	 onSubmit: function(){
		// $.messager.alert('info',$(this).form('validate'));
		if($(this).form('validate')==false)
		{
			$.messager.progress('close');
		}
		 return $(this).form('validate');
		 },
		 success: function(result){
			 $.messager.progress('close');
			 var result = eval('('+result+')');
				 if (result.msg){
					  $.messager.alert('Error',
					   result.msg,'error');
					} else {
						$.messager.show({title: 'Info',
					  msg: 'successfully completed'});
						//$(dialogName).dialog('close');
						//$(datagridName).datagrid('reload'); // close the dialog
						 // reload the user
						 window.location=urlx+'&id='+result.x;
		}
		
	}
}); 

}
    function saveFormUrl2(formName,url,urlx){
        //url=url;
        $.messager.progress({title:'Submitting',msg:'Please wait connecting to server in progress...'});
        $(formName).form('submit',{ url: url,
            onSubmit: function(){
                // $.messager.alert('info',$(this).form('validate'));
                if($(this).form('validate')==false)
                {
                    $.messager.progress('close');
                }
                return $(this).form('validate');
            },
            success: function(result){
                $.messager.progress('close');
                var result = eval('('+result+')');
                if (result.msg){
                    $.messager.alert({title: 'Error',
                        msg: result.msg});
                } else {
                    $.messager.show({title: 'Info',
                        msg: 'successfully completed'});
                    //$(dialogName).dialog('close');
                    //$(datagridName).datagrid('reload'); // close the dialog
                    // reload the user
                    window.location=urlx;
                }

            }
        });

    }
function openDia(){
	$('#dlgGraphGenerator').dialog('open').dialog('setTitle','Km Graph Generator');
	$('#frmGraphGenerator').form('clear');
	//newTrip();
	//url='controller/capacityController.php?action=add';
}
function generateGraph()
{
	var vehicleId=$('#geneId').combobox('getValue');
	location.href='admin.php?view=Vehicle_Kilometer_Graph&id='+vehicleId;
	
}

function notifyMe() {
  if (!("Notification" in window)) {
   // alert("This browser does not support desktop notification");
  }
  else if (Notification.permission === "granted") {
	  $.post('controller/orderController.php?action=orderNotification',{x:'0'},function(data){
		  
	  
        var options = {
                body: data,
                icon: "image2.png",
                dir : "ltr"
             };
          var notification = new Notification("System Alerts",options);
	  });
  }
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }
   
      if (permission === "granted") {
        var options = {
              body: "This is the body of the notification",
              icon: "image2.jpg",
              dir : "ltr"
          };
        var notification = new Notification("System Alerts",options);
      }
    });
  }
}
//var myVar=setInterval(function(){notifyMe()},150000);
//var myVar=setTimeout(function(){notifyMe()}, 10);
$(function (){
	$('.tdate').datepicker({format:'yyyy-mm-dd'});
	
});
    function sendFileToServer(fileName,fileParaName,url)
    {
        loadingBar('Please wait.....');
        formdata = new FormData();
        if($(fileName).prop('files').length > 0)
        {
            file =$(fileName).prop('files')[0];
            formdata.append(fileParaName, file);
        }
        jQuery.ajax({
            url:  url,
            type: 'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                closeBar();
                alert(result);

                // if all is well
                // play the audio file
            }
        });


    }

</script>

</body>

</html>